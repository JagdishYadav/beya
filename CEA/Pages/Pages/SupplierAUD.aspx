﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SupplierAUD.aspx.vb" Inherits="CEA.Screen1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="PageTitle">
    SupplierAUD
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
                <div class="large-6 columns">
                    <label>
                        Supplier ID</label>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </div>
                 
            </div>
    <asp:Panel ID="Panel1" runat="server" CssClass="panel">
    
            <div class="row">
            <div class="large-6 columns">
                    <label>
                        Name (English)</label>
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </div>
                <div class="large-6 columns">
                    <label>
                        Name (Arabic)</label>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                </div></div>
                <div class="row">
            <div class="large-12 columns">
                    <label>
                       Address</label>
                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                </div>
                </div>
                <div class="row">
            <div class="large-6 medium-6 columns">
                    <label>
                        P.O. Box</label>
                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                
                    <label>
                        City</label>
                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                     <label>
                        Zip Code</label>
                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                     <label>
                        Country</label>
                    <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                </div>
                <div class="large-6 medium-6 columns">
                    <asp:Panel ID="Panel2" runat="server" CssClass="panel">
                    <div class="panelTitle">Fast Contect</div>
                    <label>
                        Phone No.</label>
                    <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                    <label>
                        Fax No.</label>
                    <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
                    <label>
                        Email ID.</label>
                    <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
                    </asp:Panel>
                </div>
                </div>
                
                </asp:Panel>
                <div class="row">
                <div class="large-9 columns">
                </div>
                <div class="large-3 columns">
                    <asp:Button ID="Button1" runat="server" Text="Done" CssClass="small button" />
                    <asp:Button ID="Button2" runat="server" Text="Cencel" CssClass="small button alert" />
                </div>
</asp:Content>


