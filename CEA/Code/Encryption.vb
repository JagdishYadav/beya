﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq
Imports System.Security.Cryptography
Imports System.Xml
Imports System.Text
Imports System.IO

Public Class Encryption
    Private Shared key As Byte() = {}
    Private Shared IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
    Private Shared EncryptionKey As String = "!5623a#d"
    Public Shared Function Decrypt(Input As String) As String
        Input = Input.Replace(" ", "+")
        Dim inputByteArray(Input.Length) As Byte
        Try
            key = System.Text.Encoding.UTF8.GetBytes(EncryptionKey.Substring(0, 8))
            Dim des As New DESCryptoServiceProvider()
            inputByteArray = Convert.FromBase64String(Input)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim encoding As Encoding = encoding.UTF8
            Dim sDecrypt = Encoding.GetString(ms.ToArray())
            Return sDecrypt
        Catch ex As Exception
            Return "Invalid"
        End Try
    End Function
    Public Shared Function Encrypt(Input As String) As String
        Input = Input.Replace(" ", "+")
        Dim inputByteArray(Input.Length) As Byte
        Try
            key = System.Text.Encoding.UTF8.GetBytes(EncryptionKey.Substring(0, 8))
            Dim des As New DESCryptoServiceProvider()
            inputByteArray = Encoding.UTF8.GetBytes(Input)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim sEncrypt As String = Convert.ToBase64String(ms.ToArray())
            Return sEncrypt
        Catch ex As Exception
            Return ""
        End Try
    End Function
End Class
