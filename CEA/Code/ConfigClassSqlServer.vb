﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web

Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient

Imports System.Collections
Imports System.Web.Security
Imports System.IO
Imports System.Web.UI
Imports System.Globalization
Imports System.Text

Public Class ConfigClassSqlServer
    Dim strConnectionName As String = ""
    Dim sSessionId As String = ""
    Dim strUserName As String = ""
    Dim sTicketData As String = ""
    Public Function Login(strUserName As String, strPassword As String, strConnectionString As String, lblErrorMessage As Label, strCompID As String, is_Supplier As String) As String
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings(strConnectionString).ConnectionString
        sqlConnection.Open()
        If (is_Supplier = "YES") Then
            sqlCommand.CommandText = "SELECT u.UserID, Password, m.RoleID, u.ID, 0, 1, 0 FROM Users u INNER JOIN User_Role_Mapping m ON u.ID = m.UserID WHERE u.UserID = '" + strUserName + "' AND m.RoleID = 99"
        Else
            sqlCommand.CommandText = "SELECT u.UserID, Password, m.RoleID, u.ID, c.ID, c.ClientTypeID, ISNULL(c.IsTemp,0)IsTemp FROM Users u INNER JOIN User_Role_Mapping m ON u.ID = m.UserID INNER JOIN Company c ON c.ID = u.CompID WHERE u.UserID = '" + strUserName + "' AND CONVERT(DATETIME,STUFF(STUFF(STUFF(c.ExpiryDate,9,0,' '),12,0,':'),15,0,':')) > getdate() AND u.CompID = '" + strCompID + "' AND m.RoleID <> 99 "
        End If

        Dim sqlReader As SqlDataReader = sqlCommand.ExecuteReader()
        If (sqlReader.HasRows()) Then
            Dim strRoles As String = String.Empty
            While (sqlReader.Read())
                If (strPassword = Encryption.Decrypt(sqlReader.GetValue(1).ToString())) Then
                    sSessionId = HttpContext.Current.Session.SessionID
                    strUserName = sqlReader.GetValue(0).ToString()
                    sTicketData = sSessionId + "|" + strUserName + "|" + sqlReader.GetValue(2).ToString() + "|" + sqlReader.GetValue(3).ToString() + "|" + sqlReader.GetValue(4).ToString() + "|" + sqlReader.GetValue(5).ToString() + "|" + sqlReader.GetValue(6).ToString()
                    strRoles = sqlReader.GetValue(4).ToString() + ","
                Else
                    sTicketData = "NotFound"
                End If
            End While
            If (strRoles.Length > 0) Then
                strRoles = strRoles.Substring(0, strRoles.Length - 1)
                sTicketData = sTicketData + "|" + strRoles
            End If
            sqlReader.Close()
        Else
            sTicketData = "NotFound"
        End If
        sqlConnection.Close()
        Return sTicketData
    End Function
    Public Function GetUserModules(strUserName As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT r.NameE, m.Module_ID FROM Users u INNER JOIN User_Role_Mapping mp ON mp.UserID = u.ID INNER JOIN User_Role r ON r.ID = mp.RoleID INNER JOIN UserRole_Module_Mapping m ON m.User_Role_ID = mp.RoleID WHERE u.UserID = '" + strUserName + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetSupplierByID(strSupplierID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT s.UserID, NameE, NameA, Address, POBox, City, ZipCode, Country, Phone, Fax, s.EmailID, ApprovedBy, Other, RefNo, s.UserID, s.Pass FROM Suppliers s WHERE s.UserID = '" + strSupplierID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function CreateSupplier(ByVal strUserID As String, ByVal NameE As String, ByVal NameA As String, ByVal Address As String, ByVal POBox As String, ByVal City As String, ByVal ZipCode As String, ByVal Country As String, ByVal Phone As String, ByVal Fax As String, ByVal EmailID As String, ByVal strApprovedBy As String, ByVal strOther As String, ByVal strRefNo As String, ByVal strPass As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "SELECT ID FROM Suppliers WHERE UserID = '" + strUserID + "' "
        If (sqlCommand.ExecuteScalar() IsNot Nothing) Then 'Record already exist with this User ID
            retVal = 1
        Else
            sqlCommand.CommandText = "INSERT INTO Suppliers(UserID, NameE, NameA, Address, POBox, City, ZipCode, Country, Phone, Fax, EmailID, ApprovedBy, Other, RefNo, Pass) VALUES ('" + strUserID + "', '" + NameE + "', '" + NameA + "', '" + Address + "', '" + POBox + "', '" + City + "', '" + ZipCode + "', '" + Country + "', '" + Phone + "', '" + Fax + "', '" + EmailID + "', '" + strApprovedBy + "', '" + strOther + "', '" + strRefNo + "', '" + strPass + "') "
            sqlCommand.ExecuteNonQuery()
        End If
        sqlConnection.Close()
        Return retVal
    End Function
    Public Function UpdateSupplier(strUserID As String, NameE As String, NameA As String, Address As String, POBox As String, City As String, ZipCode As String, Country As String, Phone As String, Fax As String, EmailID As String, strApprovedBy As String, strOther As String, strrefNo As String, strPass As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "UPDATE Suppliers SET NameE = '" + NameE + "', NameA = '" + NameA + "', Address = '" + Address + "', POBox = '" + POBox + "', City = '" + City + "', ZipCode = '" + ZipCode + "', Country = '" + Country + "', Phone = '" + Phone + "', Fax = '" + Fax + "', EmailID = '" + EmailID + "', ApprovedBy = '" + strApprovedBy + "', Other = '" + strOther + "', RefNo = '" + strrefNo + "', Pass = '" + strPass + "' WHERE UserID = '" + strUserID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function UpdateSupplierUserID(ID As String, strUserID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "UPDATE Suppliers SET UserID = '" + strUserID + "' WHERE ID = '" + ID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetUsers() As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT UserID, Password, User_Role_ID, NameE As CategoryNameE, NameA As CategoryNameA FROM Users LEFT JOIN User_Role ON User_Role_ID = ID"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i).Item(1) = Encryption.Decrypt(dt.Rows(i).Item(1).ToString())
        Next
        sqlConnection.Close()
        Return dt
    End Function
    Public Function SearchUsers(strUserID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT u.UserID, MAX(Password) As Password, MAX(mp.RoleID), MAX(NameE) As CategoryNameE, MAX(NameA) As CategoryNameA, u.ID, MAX(u.FirstName) + ' ' + MAX(u.LastName) As Name, MAX(u.Email) Email FROM Users u INNER JOIN User_Role_Mapping mp ON mp.UserID = u.ID LEFT JOIN User_Role r ON mp.RoleID = r.ID WHERE (u.UserID LIKE '%" + strUserID + "%' OR u.FirstName LIKE '%" + strUserID + "%' OR u.LastName LIKE '%" + strUserID + "%') GROUP BY u.UserID,u.ID"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i).Item(1) = Encryption.Decrypt(dt.Rows(i).Item(1).ToString())
        Next
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetApproved_Agencies() As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID, NameE, NameA, '' RefNo FROM Approval_Agencies"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProduct_Specification() As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Product_Specification"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function DeleteUser(UserID As String, strUserName As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "DELETE FROM Users WHERE ID = '" + UserID + "' DELETE FROM User_Role_Mapping WHERE UserID = '" + UserID + "' DELETE FROM Supplier_Quot WHERE SuppID IN(SELECT ID FROM Suppliers WHERE UserID = '" + strUserName + "')  DELETE FROM Suppliers WHERE UserID = '" + strUserName + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function DeleteUserBySupplier(strSupplierID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "DELETE FROM Users WHERE ID = (SELECT UserID FROM Suppliers WHERE ID = '" + strSupplierID + "') DELETE FROM User_Role_Mapping WHERE UserID = (SELECT UserID FROM Suppliers WHERE ID = '" + strSupplierID + "') "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetUserByID(strUserID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT u.UserID, Password, mp.RoleID, NameE As CategoryNameE, NameA As CategoryNameA, u.FirstName, u.LastName, u.Email FROM Users u INNER JOIN User_Role_Mapping mp ON mp.UserID = u.ID LEFT JOIN User_Role r ON mp.RoleID = r.ID WHERE u.UserID = '" + strUserID + "'"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        If dt.Rows.Count > 0 Then
            dt.Rows(0).Item(1) = Encryption.Decrypt(dt.Rows(0).Item(1).ToString())
        End If
        sqlConnection.Close()
        Return dt
    End Function
    Public Function UpdateUser(UserID As String, Password As String, strFirstName As String, strLastName As String, strEmail As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        Dim psw As String = Encryption.Encrypt(Password)
        sqlCommand.CommandText = "UPDATE Users SET Password = '" + psw + "', FirstName = '" + strFirstName + "', LastName = '" + strLastName + "', Email = '" + strEmail + "'  WHERE ID = '" + UserID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlCommand.CommandText = "DELETE FROM User_Role_Mapping WHERE UserID = '" + UserID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function UpdateUserByID(strID As String, UserID As String, Password As String, strFirstName As String, strLastName As String, strEmail As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        Dim psw As String = Encryption.Encrypt(Password)
        sqlCommand.CommandText = "UPDATE Users SET UserID = '" + UserID + "', Password = '" + psw + "', FirstName = '" + strFirstName + "', LastName = '" + strLastName + "', Email = '" + strEmail + "'  WHERE ID = (SELECT UserID FROM Suppliers WHERE ID = '" + strID + "') "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function CreateUser(UserID As String, Password As String, strFirstName As String, strLastName As String, strEmail As String, strCompID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "SELECT UserID FROM Users WHERE UserID = '" + UserID + "' AND CompID = '" + strCompID + "' "
        If (sqlCommand.ExecuteScalar() IsNot Nothing) Then 'Record already exist with this ID
            retVal = -1
        Else
            Dim psw As String = Encryption.Encrypt(Password)
            sqlCommand.CommandText = "INSERT INTO Users(UserID, Password,FirstName,LastName,Email,CompID) VALUES ('" + UserID + "', '" + psw + "', '" + strFirstName + "', '" + strLastName + "', '" + strEmail + "', '" + strCompID + "' ) SELECT ID FROM Users WHERE ID = (SELECT MAX(ID) FROM Users) "
            retVal = sqlCommand.ExecuteScalar()
        End If
        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetEquipmentByCode(strCode As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Code, DescE, DescA, Unit, OperationCost, EquipmentDepCost, EquipmentDailyCost, RentalCostD, RentalCostM FROM Equipment WHERE Code = '" + strCode + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function CreateEquipment(Code As String, DescE As String, DescA As String, Unit As String, OperationCost As String, EquipmentDepCost As String, EquipmentDailyCost As String, RentalCostD As String, RentalCostM As String, strCompID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "SELECT Code FROM Equipment WHERE Code = '" + Code + "' AND CompID = '" + strCompID + "' "
        If (sqlCommand.ExecuteScalar() IsNot Nothing) Then 'Record already exist with this Code
            retVal = 1
        Else
            Dim dHourlyRate As Double = (Double.Parse(EquipmentDailyCost) / 8)
            sqlCommand.CommandText = "INSERT INTO Equipment(Code, DescE, DescA, Unit, OperationCost, EquipmentDepCost, EquipmentDailyCost, RentalCostD, RentalCostM, HourlyRate, CompID) VALUES ('" + Code + "', '" + DescE + "', '" + DescA + "', '" + Unit + "', '" + OperationCost + "', '" + EquipmentDepCost + "', '" + EquipmentDailyCost + "', '" + RentalCostD + "', '" + RentalCostM + "', " + dHourlyRate.ToString() + ", '" + strCompID + "' ) "
            sqlCommand.ExecuteNonQuery()
        End If
        sqlConnection.Close()
        Return retVal
    End Function
    Public Function UpdateEquipment(Code As String, DescE As String, DescA As String, Unit As String, OperationCost As String, EquipmentDepCost As String, EquipmentDailyCost As String, RentalCostD As String, RentalCostM As String, strCompID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        Dim dHourlyRate As Double = (Double.Parse(EquipmentDailyCost) / 8)
        sqlCommand.CommandText = "UPDATE Equipment SET DescE = '" + DescE + "', DescA = '" + DescA + "', Unit = '" + Unit + "', OperationCost = '" + OperationCost + "', EquipmentDepCost = '" + EquipmentDepCost + "', EquipmentDailyCost = '" + EquipmentDailyCost + "', RentalCostD = '" + RentalCostD + "', RentalCostM = '" + RentalCostM + "', HourlyRate = " + dHourlyRate.ToString() + " WHERE Code = '" + Code + "' AND CompID = '" + strCompID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetLabourByCode(strCode As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Code, ProfE, ProfA, GenExp, AvgSalary, Housing, Food, MedInsurance, SecInsurance, Ticket, EOContract, Iqama, Nationality FROM Labour WHERE Code = '" + strCode + "' AND CompID = '" + strCompID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function CreateLabour(Code As String, ProfE As String, ProfA As String, GenExp As String, AvgSalary As String, Housing As String, Food As String, MedInsurance As String, SecInsurance As String, Ticket As String, EOContract As String, Iqama As String, Nationality As String, HourlyRate As String, strCompID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "SELECT Code FROM Labour WHERE Code = '" + Code + "' AND CompID = '" + strCompID + "' "
        If (sqlCommand.ExecuteScalar() IsNot Nothing) Then 'Record already exist with this Code
            retVal = 1
        Else
            sqlCommand.CommandText = "INSERT INTO Labour(Code, ProfE, ProfA, GenExp, AvgSalary, Housing, Food, MedInsurance, SecInsurance, Ticket, EOContract, Iqama, Nationality, HourlyRate, CompID) VALUES ('" + Code + "', '" + ProfE + "', '" + ProfA + "', '" + GenExp + "', '" + AvgSalary + "', '" + Housing + "', '" + Food + "', '" + MedInsurance + "', '" + SecInsurance + "', '" + Ticket + "', '" + EOContract + "', '" + Iqama + "', '" + Nationality + "', '" + HourlyRate + "', '" + strCompID + "' ) "
            sqlCommand.ExecuteNonQuery()
        End If
        sqlConnection.Close()
        Return retVal
    End Function
    Public Function UpdateLabour(Code As String, ProfE As String, ProfA As String, GenExp As String, AvgSalary As String, Housing As String, Food As String, MedInsurance As String, SecInsurance As String, Ticket As String, EOContract As String, Iqama As String, Nationality As String, HourlyRate As String, strCompID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "UPDATE Labour SET ProfE = '" + ProfE + "', ProfA = '" + ProfA + "', GenExp = '" + GenExp + "', AvgSalary = '" + AvgSalary + "', Housing = '" + Housing + "', Food = '" + Food + "', MedInsurance = '" + MedInsurance + "', SecInsurance = '" + SecInsurance + "', Ticket = '" + Ticket + "', EOContract = '" + EOContract + "', Iqama = '" + Iqama + "', Nationality = '" + Nationality + "', HourlyRate = '" + HourlyRate + "' WHERE Code = '" + Code + "' AND CompID = '" + strCompID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetSuppliers(strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt, dt1 As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT s.UserID, s.NameE, s.NameA, Address, POBox, City, ZipCode, Country, Phone, Fax, EmailID, s.ApprovedBy, s.Other, '' As ApprovedByE, '' As ApprovedByA, s.UserID, ID FROM Suppliers s WHERE s.NameE LIKE '%" + strSearch + "%' OR s.NameA LIKE '%" + strSearch + "%' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Approval_Agencies"
        sqlDataAdapter.Fill(dt1)

        Dim strApprovedByE, strApprovedByA As String
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim arrApprovedBy As String() = dt.Rows(i).Item(11).ToString().Split(",")
            If (arrApprovedBy.Length > 0 And arrApprovedBy(0).Length > 0) Then
                For j As Integer = 0 To arrApprovedBy.Length - 1
                    For k As Integer = 0 To dt1.Rows.Count - 1
                        If (arrApprovedBy(j) = dt1.Rows(k).Item(0).ToString()) Then

                            If (dt1.Rows(k).Item(0).ToString() = "99") Then
                                strApprovedByE = strApprovedByE + dt.Rows(i).Item(12).ToString() + ","
                                strApprovedByA = strApprovedByA + dt.Rows(i).Item(12).ToString() + ","
                            Else
                                strApprovedByE = strApprovedByE + dt1.Rows(k).Item(1).ToString() + ","
                                strApprovedByA = strApprovedByA + dt1.Rows(k).Item(2).ToString() + ","
                            End If
                        End If
                    Next
                Next

                If (strApprovedByE.Length > 0) Then
                    strApprovedByE = strApprovedByE.Substring(0, strApprovedByE.Length - 1)
                End If
                If (strApprovedByA.Length > 0) Then
                    strApprovedByA = strApprovedByA.Substring(0, strApprovedByA.Length - 1)
                End If
                dt.Rows(i).Item(13) = strApprovedByE
                dt.Rows(i).Item(14) = strApprovedByA
                strApprovedByE = ""
                strApprovedByA = ""
            End If
        Next

        sqlConnection.Close()
        Return dt
    End Function
    Public Function DeleteSupplier(strSupplierID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "DELETE FROM Suppliers WHERE ID = '" + strSupplierID + "' DELETE FROM Supplier_Quot WHERE SuppID = '" + strSupplierID + "'"
        sqlCommand.ExecuteNonQuery()
        sqlConnection.Close()
        Return retVal
    End Function
    Public Function DeleteCrewStructure(strCode As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "DELETE FROM ProductCrewStructure WHERE ProductID = '" + strCode + "'  UPDATE MainHierarchy1 SET CrewCode = 0 WHERE CrewCode = '" + strCode + "' "
        sqlCommand.ExecuteNonQuery()
        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetLabours(strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Code, ProfE, ProfA, GenExp, AvgSalary, Housing, Food, MedInsurance, SecInsurance, Ticket, EOContract, Iqama, c.NameE, c.NameA, cast(HourlyRate as numeric(8,2))HourlyRate FROM Labour l LEFT JOIN Country c ON c.ID = l.Nationality WHERE ProfE LIKE '%" + strSearch + "%' OR ProfA LIKE '%" + strSearch + "%'"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetCrewStrucrureAll(strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT MIN(ID)ID, ProductID, MIN(DailyOutput)DailyOutput, MIN(HourlyOutput)HourlyOutput, MIN(ManHour)ManHour, MIN(EquipHour)EquipHour FROM ProductCrewStructure WHERE ProductID LIKE '%" + strSearch + "%' GROUP BY ProductID"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetLaboursByCountry(strSearch As String, strCountryID As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Code, ProfE, ProfA FROM Labour l WHERE (ProfE LIKE '%" + strSearch + "%' OR ProfA LIKE '%" + strSearch + "%') AND l.Nationality = '" + strCountryID + "' AND l.CompID = '" + strCompID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetLabourByID(strCode As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Code, ProfE, ProfA, GenExp, AvgSalary, Housing, Food, MedInsurance, SecInsurance, Ticket, EOContract, Iqama, Nationality, HourlyRate FROM Labour WHERE Code = '" + strCode + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function DeleteLabour(strCode As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "DELETE FROM Labour WHERE Code = '" + strCode + "' "
        sqlCommand.ExecuteNonQuery()
        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetSuppliersList(strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Suppliers WHERE NameE LIKE '%" + strSearch + "%' OR NameA LIKE '%" + strSearch + "%' ORDER BY NameE, NameA"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetEquipments(strSearch As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Code, DescE, DescA, Unit, OperationCost, EquipmentDepCost, EquipmentDailyCost, RentalCostD, RentalCostM, HourlyRate FROM Equipment WHERE CompID = '" + strCompID + "' AND DescE LIKE '%" + strSearch + "%' OR DescA LIKE '%" + strSearch + "%'"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function DeleteEquipment(strCode As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "DELETE FROM Equipment WHERE Code = '" + strCode + "' "
        sqlCommand.ExecuteNonQuery()
        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetProducts(strMapID As String, strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt, dt1 As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT DISTINCT p.ID, p.MapID, p.NameE, p.NameA, p.PropE, p.PropA, p.DescE, p.DescA, p.Unit, p.ManHour, p.EquipHour, p.ApprovedBy, Other, '' As ApprovedByE, '' As ApprovedByA, Specification, Other_Spec FROM ProductDetails p WHERE MapID = '" + strMapID + "' AND (p.NameE LIKE '%" + strSearch + "%' OR p.NameA LIKE '%" + strSearch + "%') "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Approval_Agencies"
        sqlDataAdapter.Fill(dt1)

        Dim strApprovedByE, strApprovedByA As String
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim arrApprovedBy As String() = dt.Rows(i).Item(11).ToString().Split(",")
            If (arrApprovedBy.Length > 0 And arrApprovedBy(0).Length > 0) Then
                For j As Integer = 0 To arrApprovedBy.Length - 1
                    For k As Integer = 0 To dt1.Rows.Count - 1
                        If (arrApprovedBy(j) = dt1.Rows(k).Item(0).ToString()) Then

                            If (dt1.Rows(k).Item(0).ToString() = "99") Then
                                strApprovedByE = strApprovedByE + dt.Rows(i).Item(12).ToString() + ","
                                strApprovedByA = strApprovedByA + dt.Rows(i).Item(12).ToString() + ","
                            Else
                                strApprovedByE = strApprovedByE + dt1.Rows(k).Item(1).ToString() + ","
                                strApprovedByA = strApprovedByA + dt1.Rows(k).Item(2).ToString() + ","
                            End If
                        End If
                    Next
                Next

                If (strApprovedByE.Length > 0) Then
                    strApprovedByE = strApprovedByE.Substring(0, strApprovedByE.Length - 1)
                End If
                If (strApprovedByA.Length > 0) Then
                    strApprovedByA = strApprovedByA.Substring(0, strApprovedByA.Length - 1)
                End If
                dt.Rows(i).Item(13) = strApprovedByE
                dt.Rows(i).Item(14) = strApprovedByE
                strApprovedByE = ""
                strApprovedByA = ""
            End If
        Next

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProducts_2(strMapID As String, strSearch As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt, dt1 As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT DISTINCT p.ID, p.MapID, p.NameE, p.NameA, p.PropE, p.PropA, p.DescE, p.DescA, p.Unit, p.ManHour, p.EquipHour, p.ApprovedBy, Other, '' As ApprovedByE, '' As ApprovedByA, Specification, Other_Spec FROM ProductDetails p WHERE MapID = '" + strMapID + "' AND CompID = '" + strCompID + "' AND (p.NameE LIKE '%" + strSearch + "%' OR p.NameA LIKE '%" + strSearch + "%' OR p.MapID LIKE '%" + strSearch + "%') AND (Status = 1 OR Status IS NULL)"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Approval_Agencies"
        sqlDataAdapter.Fill(dt1)

        Dim strApprovedByE, strApprovedByA As String
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim arrApprovedBy As String() = dt.Rows(i).Item(11).ToString().Split(",")
            If (arrApprovedBy.Length > 0 And arrApprovedBy(0).Length > 0) Then
                For j As Integer = 0 To arrApprovedBy.Length - 1
                    For k As Integer = 0 To dt1.Rows.Count - 1
                        If (arrApprovedBy(j) = dt1.Rows(k).Item(0).ToString()) Then

                            If (dt1.Rows(k).Item(0).ToString() = "99") Then
                                strApprovedByE = strApprovedByE + dt.Rows(i).Item(12).ToString() + ","
                                strApprovedByA = strApprovedByA + dt.Rows(i).Item(12).ToString() + ","
                            Else
                                strApprovedByE = strApprovedByE + dt1.Rows(k).Item(1).ToString() + ","
                                strApprovedByA = strApprovedByA + dt1.Rows(k).Item(2).ToString() + ","
                            End If
                        End If
                    Next
                Next

                If (strApprovedByE.Length > 0) Then
                    strApprovedByE = strApprovedByE.Substring(0, strApprovedByE.Length - 1)
                End If
                If (strApprovedByA.Length > 0) Then
                    strApprovedByA = strApprovedByA.Substring(0, strApprovedByA.Length - 1)
                End If
                dt.Rows(i).Item(13) = strApprovedByE
                dt.Rows(i).Item(14) = strApprovedByE
                strApprovedByE = ""
                strApprovedByA = ""
            End If
        Next

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetAssociatedSuppliers(strProdID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT SupplierID, NameE, NameA, MinPrice, MaxPrice FROM AssociatedSuppliers WHERE ProductID = '" + strProdID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetAssociatedCrewStructures(strProdID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT c.Lab_Equip, c.Code, case c.Lab_Equip WHEN 1 THEN l.ProfE else e.DescE end As DescE, case c.Lab_Equip WHEN 1 THEN l.ProfA else e.DescA end As DescA, c.Man_Day FROM ProductCrewStructure c LEFT JOIN Labour l ON l.Code = c.Code LEFT JOIN Equipment e ON e.Code = c.Code WHERE c.ProductID = '" + strProdID + "'"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetCrewStructureCodesAll(strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT DISTINCT ProductID FROM ProductCrewStructure WHERE CompID = '" + strCompID + "' ORDER BY ProductID ASC"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetMainHierarchy() As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT IDCode,Level1,Level2,Level3,Level4,Level5,Level6,Level7,Level8 FROM MainHierarchy"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetMainHierarchyA() As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT IDCode,Level1A,Level2A,Level3A,Level4A,Level5A,Level6A,Level7A,Level8A FROM MainHierarchy"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function DeleteProductByID(strID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "DELETE FROM ProductDetails WHERE ID = '" + strID + "' "
        sqlCommand.ExecuteNonQuery()
        sqlConnection.Close()
        Return retVal
    End Function
    Public Function Save_Update_Project(strProjectID As String, strProjectName As String, strCreationDate As String, strCreatedBy As String, strStatus As String, strCompID As String, strLangID As String, strProjectType As String) As Integer
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim retVal As String = "0"

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        If (strProjectID.Length > 0) Then
            sqlCommand.CommandText = "UPDATE Project SET Name = '" + strProjectName + "', Status = '" + strStatus + "' WHERE ID = '" + strProjectID + "' "
            sqlCommand.ExecuteNonQuery()
            retVal = strProjectID
        Else
            sqlCommand.CommandText = "SELECT ID FROM Project WHERE Name = '" + strProjectName + "' AND CompID = '" + strCompID + "' "
            If (sqlCommand.ExecuteScalar() IsNot Nothing) Then 'Record already exist with this project name
                retVal = "-1"
            Else
                sqlCommand.CommandText = "INSERT INTO Project(Name,CreationDate,CreatedBy,Status,CompID, LangID, ProjectType) VALUES ('" + strProjectName + "', '" + strCreationDate + "', '" + strCreatedBy + "', '" + strStatus + "', '" + strCompID + "', '" + strLangID + "', '" + strProjectType + "') SELECT MAX(ID) FROM Project"
                retVal = sqlCommand.ExecuteScalar().ToString()
            End If
        End If
        sqlConnection.Close()
        Return retVal
    End Function
    Public Function UpdateProject(strProjName As String, strBuildingName As String, strUserIDs As String, strLevel1IDs As String, strLevel2IDs As String, strLevel3IDs As String, strLevel4IDs As String, strLevel5IDs As String, strLevel6IDs As String, strLevel7IDs As String, strLevel8IDs As String, strCreationDate As String, strCreatedBy As String) As Integer
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim retVal As Integer = 0

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "UPDATE Project SET User_IDs = '" + strUserIDs + "', Level1_IDs = '" + strLevel1IDs + "', Level2_IDs = '" + strLevel2IDs + "', Level3_IDs = '" + strLevel3IDs + "', Level4_IDs = '" + strLevel4IDs + "', Level5_IDs = '" + strLevel5IDs + "', Level6_IDs = '" + strLevel6IDs + "', Level7_IDs = '" + strLevel7IDs + "', Level8_IDs = '" + strLevel8IDs + "' WHERE Name = '" + strProjName + "'"
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function UpdateCostLineItem(strLineNo As String, strCrewCode As String, strPercent As String, strCost As String, strMaterialCost As String, strUnit As String, strDO As String) As Integer
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim retVal As Integer = 0
        Dim dt As New DataTable()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        'calculation
        Dim HO As Double = 0
        Dim MH As Double = 0
        Dim EH As Double = 0

        HO = Convert.ToDouble(strDO) / 8

        sqlCommand.CommandText = "SELECT ISNULL(SUM(ISNULL(Man_Day, 0)), 0)FROM ProductCrewStructure WHERE ProductID = '" + strCrewCode + "' AND Lab_Equip = 1"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        If (dt.Rows.Count > 0) Then
            MH = Convert.ToDouble(dt.Rows(0).Item(0).ToString()) / HO
        End If

        dt.Clear()
        sqlCommand.CommandText = "SELECT ISNULL(SUM(ISNULL(Man_Day, 0)), 0) FROM ProductCrewStructure WHERE ProductID = '" + strCrewCode + "' AND Lab_Equip = 2"
        sqlDataAdapter.SelectCommand = sqlCommand
        sqlDataAdapter.Fill(dt)

        If (dt.Rows.Count > 0) Then
            EH = Convert.ToDouble(dt.Rows(0).Item(0).ToString()) / HO
        End If

        HO = Math.Round(HO, 2)
        MH = Math.Round(MH, 2)
        EH = Math.Round(EH, 2)

        sqlCommand.CommandText = "UPDATE MainHierarchy1 SET CrewCode = '" + strCrewCode + "',  IsPercent = '" + strPercent + "', Cost = " + strCost + ", MaterialCost = '" + strMaterialCost + "', Unit = '" + strUnit + "', DO = '" + strDO + "', HO = '" + HO.ToString() + "', MH = '" + MH.ToString() + "', EH = '" + EH.ToString() + "' WHERE No = '" + strLineNo + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetProjects(strUserID As String, strSearch As String, strCompID As String, strUserName As String, strUserRole As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT * FROM Project WHERE LEN(BuildingName) > 0 AND Name LIKE '%" + strSearch + "%' AND CompID = '" + strCompID + "' AND Status = 0 ORDER BY Name"
        'sqlCommand.CommandText = "SELECT MAX(ID)ID,Name, Max(BuildingName)BuildingName,Max(User_IDs)User_IDs, MAX(Categories)Categories, Max(CreationDate)CreationDate,Max(CreatedBy)CreatedBy,Min(Status)Status,Max(CompID)CompID, Min(IsComplete)IsComplete, Max(LangID)LangID,Max(ProjectType)ProjectType FROM Project WHERE LEN(BuildingName) > 0 AND Name LIKE '%" + strSearch + "%' AND CompID = '" + strCompID + "' AND Status = 0 GROUP BY Name ORDER BY IsComplete"

        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)
        If (dt.Rows.Count > 0) Then

            If (strUserRole <> "4" Or strUserRole <> "888" Or strUserRole <> "999") Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    dt.Rows(i).Item(5) = DateTime.ParseExact(dt.Rows(i).Item(5), "yyyyMMddHHmmss", Nothing).ToString()
                    Dim arrList As String() = dt.Rows(i).Item(3).ToString().Split(",")
                    'If (arrList.Contains(strUserID) = False And dt.Rows(i).Item(6).ToString() <> strUserName) Then
                    If (arrList.Contains(strUserID) = False) Then
                        dt.Rows(i).Delete()
                    End If
                Next
            End If
            
            For i As Integer = 0 To dt.Rows.Count - 1
                If (i > 0) Then
                    If (dt.Rows(i).RowState <> DataRowState.Deleted And dt.Rows(i - 1).RowState <> DataRowState.Deleted) Then
                        If (dt.Rows(i).Item(1).ToString() = dt.Rows(i - 1).Item(1).ToString()) Then
                            dt.Rows(i).Delete()
                        End If
                    End If
                End If
            Next
        End If
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetAllProjects(strSearch As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT MAX(ID)ID,Name, Max(BuildingName)BuildingName,Max(User_IDs)User_IDs, MAX(Categories)Categories, Max(CreationDate)CreationDate,Max(CreatedBy)CreatedBy,Min(Status)Status,Max(CompID)CompID, Min(IsComplete)IsComplete, Max(LangID)LangID,Max(ProjectType)ProjectType FROM Project WHERE LEN(BuildingName) > 0 AND ((Status = 1 AND Name LIKE '%" + strSearch + "%') OR (CompID = '" + strCompID + "' AND Name LIKE '%" + strSearch + "%')) AND IsComplete = 1 GROUP BY Name"

        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i).Item(5) = DateTime.ParseExact(dt.Rows(i).Item(5), "yyyyMMddHHmmss", Nothing).ToString()
            If (i > 0) Then
                If (dt.Rows(i - 1).RowState <> DataRowState.Deleted And dt.Rows(i).Item(1).ToString() = dt.Rows(i - 1).Item(1).ToString()) Then
                    dt.Rows(i - 1).Delete()
                End If
            End If
        Next

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetAllProjectsOfCurrentCompany(strSearch As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        'sqlCommand.CommandText = "SELECT MAX(ID)ID,Name, Max(BuildingName)BuildingName,Max(User_IDs)User_IDs, MAX(Categories)Categories, Max(CreationDate)CreationDate,Max(CreatedBy)CreatedBy,Min(Status)Status,Max(CompID)CompID, Min(IsComplete)IsComplete, Max(LangID)LangID,Max(ProjectType)ProjectType FROM Project WHERE LEN(BuildingName) > 0 AND CompID = '" + strCompID + "' AND Name LIKE '%" + strSearch + "%' AND IsComplete = 1 AND Status = 0 GROUP BY Name"
        sqlCommand.CommandText = "SELECT * FROM Project WHERE LEN(BuildingName) > 0 AND CompID = '" + strCompID + "' AND Name LIKE '%" + strSearch + "%' AND IsComplete = 1 AND Status = 0 ORDER BY Name"

        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i).Item(5) = DateTime.ParseExact(dt.Rows(i).Item(5), "yyyyMMddHHmmss", Nothing).ToString()
            If (i > 0) Then
                If (dt.Rows(i - 1).RowState <> DataRowState.Deleted And dt.Rows(i).Item(1).ToString() = dt.Rows(i - 1).Item(1).ToString()) Then
                    dt.Rows(i - 1).Delete()
                End If
            End If
        Next

        For i As Integer = 0 To dt.Rows.Count - 1
            If (i > 0) Then
                If (dt.Rows(i).RowState <> DataRowState.Deleted And dt.Rows(i - 1).RowState <> DataRowState.Deleted) Then
                    If (dt.Rows(i).Item(1).ToString() = dt.Rows(i - 1).Item(1).ToString()) Then
                        dt.Rows(i).Delete()
                    End If
                End If
            End If
        Next

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetAllTemplateProjects(strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT MAX(ID)ID,Name, Max(BuildingName)BuildingName,Max(User_IDs)User_IDs, MAX(Categories)Categories, Max(CreationDate)CreationDate,Max(CreatedBy)CreatedBy,Min(Status)Status,Max(CompID)CompID, Min(IsComplete)IsComplete, Max(LangID)LangID,Max(ProjectType)ProjectType FROM Project WHERE LEN(BuildingName) > 0 AND Name LIKE '%" + strSearch + "%' AND IsComplete = 1 AND Status = 1 GROUP BY Name"

        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i).Item(5) = DateTime.ParseExact(dt.Rows(i).Item(5), "yyyyMMddHHmmss", Nothing).ToString()
            If (i > 0) Then
                If (dt.Rows(i - 1).RowState <> DataRowState.Deleted And dt.Rows(i).Item(1).ToString() = dt.Rows(i - 1).Item(1).ToString()) Then
                    dt.Rows(i - 1).Delete()
                End If
            End If
        Next

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetAllProjectByID(strProjID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT * FROM Project WHERE ID = '" + strProjID + "'"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i).Item(5) = DateTime.ParseExact(dt.Rows(i).Item(5), "yyyyMMddHHmmss", Nothing).ToString()
        Next

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetCostLineItemData(strLineNo As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT CrewCode, IsPercent, Cost, MaterialCost, Unit, Do FROM MainHierarchy1 WHERE No = '" + strLineNo + "' AND CrewCode IS NOT NULL"

        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function Update_Building_Name(strBuildingName_Old As String, strProjName As String, strBuildingName_New As String, strProjID As String) As String
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim retVal As String = "0"

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        If (strBuildingName_Old.Length = 0) Then
            sqlCommand.CommandText = "UPDATE Project SET BuildingName = '" + strBuildingName_New + "' WHERE Name = '" + strProjName + "' "
            sqlCommand.ExecuteNonQuery()
            retVal = strBuildingName_New
        Else
            sqlCommand.CommandText = "SELECT ID FROM Project WHERE Name = '" + strProjName + "' AND BuildingName = '" + strBuildingName_New + "' AND ID <> '" + strProjID + "' "
            If (sqlCommand.ExecuteScalar() IsNot Nothing) Then 'Record already exist with this building name
                retVal = "-1"
            Else
                sqlCommand.CommandText = "UPDATE Project SET BuildingName = '" + strBuildingName_New + "' WHERE Name = '" + strProjName + "' AND BuildingName = '" + strBuildingName_Old + "'  "
                sqlCommand.ExecuteNonQuery()
                retVal = strBuildingName_New
            End If
        End If

        sqlConnection.Close()
        Return retVal
    End Function
    'Public Function GetBuildingsByProject(strProjectName As String, strUserID As String) As DataTable
    Public Function GetBuildingsByProject(strProjectName As String, strCompID As String, strPojectType As String, strRole As String, strUserID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable()
        'Dim strID As String = strUserID + ","
        'Dim strID1 As String = "," + strUserID
        'Dim strID2 As String = "," + strUserID + ","

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        'sqlCommand.CommandText = "SELECT DISTINCT ID, Name, BuildingName FROM Project WHERE Name = '" + strProjectName + "' AND (User_IDs = '" + strUserID + "' OR User_IDs LIKE '" + strID + "%' OR User_IDs LIKE '%" + strID1 + "' OR User_IDs LIKE '%" + strID2 + "%')"
        sqlCommand.CommandText = "SELECT ID, Name, BuildingName, User_IDs FROM Project WHERE Name = '" + strProjectName + "' AND CompID = '" + strCompID + "' ORDER BY BuildingName"

        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        If (strPojectType = "Open") Then
            If (strRole <> "4" Or strRole <> "888" Or strRole <> "999") Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim arrList As String() = dt.Rows(i).Item(3).ToString().Split(",")
                    If (arrList.Contains(strUserID) = False) Then
                        dt.Rows(i).Delete()
                    End If
                Next
            End If
        End If

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetAllRoles() As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM User_Role WHERE ID NOT IN(99, 888, 999,555,777,666)"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetUserRoles() As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM User_Role WHERE ID NOT IN(888)"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetUserRolesByID(strUserID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT RoleID FROM User_Role_Mapping WHERE UserID = '" + strUserID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function InsertUserRoles(UserID As String, RoleID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "INSERT INTO User_Role_Mapping(UserID, RoleID) VALUES ('" + UserID + "', '" + RoleID + "') "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function DeleteUserRoles(UserID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "DELETE FROM User_Role_Mapping WHERE UserID = '" + UserID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetAllCategories(strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT DISTINCT No + '  ' + NameE As NameE, No + '  ' + NameA As NameA, NameE As EngName, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo is null or Len(ParentNo) = 0 AND CompID = '" + strCompID + "' ORDER BY No ASC"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function InsertRole_Categories(strRoleID As String, strCategories As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "DELETE FROM Role_Cat_Assignment WHERE RoleID = '" + strRoleID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlCommand.CommandText = "INSERT INTO Role_Cat_Assignment(RoleID, Categories) VALUES ('" + strRoleID + "', '" + strCategories + "') "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function Insert_Report_Role_Mapping(strRoleID As String, strCategories As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "DELETE FROM Report_Role_Mapping WHERE RoleID = '" + strRoleID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlCommand.CommandText = "INSERT INTO Report_Role_Mapping(RoleID, ReportIDs) VALUES ('" + strRoleID + "', '" + strCategories + "') "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetCategoriesByRole(strRoleID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Categories FROM Role_Cat_Assignment WHERE RoleID = '" + strRoleID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetReport_Role_Mapping(strRoleID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ReportIDs FROM Report_Role_Mapping WHERE RoleID = '" + strRoleID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProjectUsers(strProjName As String, strBuildingName As String, strCompID As String) As String()
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable
        Dim arrUsers As String()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT User_IDs FROM Project WHERE Name = '" + strProjName + "' AND BuildingName  = '" + strBuildingName + "' AND CompID = '" + strCompID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        If (dt.Rows.Count > 0) Then
            arrUsers = dt.Rows(0).Item(0).ToString().Split(",")
        End If
        sqlConnection.Close()
        Return arrUsers
    End Function
    Public Function GetAllUsers(strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT u.ID, MAX(u.UserID) UserID FROM Users u INNER JOIN User_Role_Mapping r ON r.UserID = u.ID WHERE r.RoleID NOT IN(99,888,999,555,777) AND u.CompID = '" + strCompID + "' Group BY u.ID "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function InsertProjectUsers(strUserIDs As String, strProjName As String, strBuildingName As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "UPDATE Project SET User_IDs = '" + strUserIDs + "' WHERE Name = '" + strProjName + "' AND BuildingName = '" + strBuildingName + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetUserCategories(strUserID As String) As String
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable
        Dim arrCategories As String()
        Dim strRetValue = String.Empty

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Categories FROM User_Role_Mapping m LEFT JOIN Role_Cat_Assignment a ON a.RoleID = m.RoleID WHERE m.UserID = '" + strUserID + "' AND Len(Categories) > 1 "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        If (dt.Rows.Count > 0) Then
            arrCategories = dt.Rows(0).Item(0).ToString().Split("}")
            For i As Integer = 0 To arrCategories.Length - 1
                strRetValue = strRetValue + "'" + arrCategories(i).ToString() + "'" + ","
            Next
        End If
        strRetValue = strRetValue.Substring(0, strRetValue.Length - 1)
        sqlConnection.Close()
        Return strRetValue
    End Function
    Public Function GetProjectCostLineItems(strProjectName As String, strBuildingName As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Categories FROM Project WHERE Name = '" + strProjectName + "' AND BuildingName = '" + strBuildingName + "' AND CompID = '" + strCompID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()

        Return dt
    End Function


    Public Function InsertProjectCategories(strCategories As String, strPojectID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "UPDATE Project SET Categories = '" + strCategories + "' WHERE ID = '" + strPojectID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetProjectCategories(strProjID As String) As String()
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable
        Dim arrCategories As String()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Categories FROM Project WHERE ID = '" + strProjID + "'  "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        If (dt.Rows.Count > 0) Then
            arrCategories = dt.Rows(0).Item(0).ToString().Split("~")
        End If
        sqlConnection.Close()
        Return arrCategories
    End Function
    Public Function CreateProjectFromExisting(strExistingProjName As String, strNewProjName As String, strUserName As String, strCompID As String, strProjID As String, strStatus As String, strLangID As String, strProjectType As String) As Integer
        Dim retVal As Integer = 0
        Dim dt, dt1, dt2, dt3, dt4, dt5 As New DataTable()
        Dim strItemID As String = String.Empty
        Dim strProjectID As String = String.Empty
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "DELETE FROM Project WHERE Name = '" + strNewProjName + "' "
        sqlCommand.ExecuteNonQuery()

        sqlCommand.CommandText = "SELECT * FROM Project WHERE Name = '" + strExistingProjName + "' AND CompID = '" + strCompID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        If (dt.Rows.Count > 0) Then

            For k As Integer = 0 To dt.Rows.Count - 1
                sqlCommand.CommandText = "INSERT INTO Project(Name, BuildingName, User_IDs, Categories, CreationDate, CreatedBy, Status, CompID, LangID, ProjectType, IsComplete) VALUES ('" + strNewProjName + "', '" + dt.Rows(k).Item(2).ToString() + "', '" + dt.Rows(k).Item(3).ToString() + "', '" + dt.Rows(k).Item(4).ToString() + "', '" + Now.ToString("yyyyMMddHHmmss") + "', '" + strUserName + "', '" + strStatus + "', '" + strCompID + "', '" + strLangID + "', '" + strProjectType + "', 1) SELECT MAX(ID) FROM Project"
                strProjectID = sqlCommand.ExecuteScalar().ToString()

                dt1.Clear()
                sqlCommand.CommandText = "SELECT ProjectID, SelectedItem_Path, Status, LangID, ID FROM Items_Selection WHERE ProjectID = '" + dt.Rows(k).Item(0).ToString() + "' "
                sqlDataAdapter.Fill(dt1)

                For i As Integer = 0 To dt1.Rows.Count - 1
                    sqlCommand.CommandText = "INSERT INTO Items_Selection(ProjectID, SelectedItem_Path, Status, LangID) VALUES ('" + strProjectID + "', '" + dt1.Rows(i).Item(1).ToString() + "', '" + dt1.Rows(i).Item(2).ToString() + "', '" + dt1.Rows(i).Item(3).ToString() + "') SELECT MAX(ID) FROM Items_Selection"
                    strItemID = sqlCommand.ExecuteScalar().ToString()

                    dt2.Clear()
                    sqlCommand.CommandText = "SELECT Prod_ID, Qty FROM Item_Products WHERE Item_ID = '" + dt1.Rows(i).Item(4).ToString() + "' "
                    sqlDataAdapter.Fill(dt2)

                    For j As Integer = 0 To dt2.Rows.Count - 1
                        sqlCommand.CommandText = "INSERT INTO Item_Products(Item_ID, Prod_ID, Qty) VALUES ('" + strItemID + "', '" + dt2.Rows(j).Item(0).ToString() + "', '" + dt2.Rows(j).Item(1).ToString() + "') "
                        sqlCommand.ExecuteNonQuery()
                    Next

                    dt3.Clear()
                    If (strProjectType = "9") Then
                        sqlCommand.CommandText = "SELECT ItemNo, Div, BOQ_No, NameE, NameA, Unit, Qty, BOQ_LineNo FROM Project_BOQ_Data WHERE projectID = '" + dt1.Rows(i).Item(0).ToString() + "' "
                        sqlDataAdapter.Fill(dt3)

                        For j As Integer = 0 To dt3.Rows.Count - 1
                            sqlCommand.CommandText = "INSERT INTO Project_BOQ_Data(ProjectID, ItemNo, Div, BOQ_No, NameE, NameA, Unit, Qty, BOQ_LineNo) VALUES ('" + strProjectID + "', '" + dt3.Rows(j).Item(0).ToString() + "', '" + dt3.Rows(j).Item(1).ToString() + "', '" + dt3.Rows(j).Item(2).ToString() + "', '" + dt3.Rows(j).Item(3).ToString() + "', '" + dt3.Rows(j).Item(4).ToString() + "', '" + dt3.Rows(j).Item(5).ToString() + "', '" + dt3.Rows(j).Item(6).ToString() + "', '" + dt3.Rows(j).Item(7).ToString() + "') "
                            sqlCommand.ExecuteNonQuery()
                        Next

                        'copy boq mapping data
                        dt4.Clear()
                        sqlCommand.CommandText = "SELECT BOQLineNo, LineItemCode, Qty, FilePath FROM Project_BOQ_LineItem_Mapping WHERE projectID = '" + dt1.Rows(i).Item(0).ToString() + "' "
                        sqlDataAdapter.Fill(dt4)

                        For j As Integer = 0 To dt4.Rows.Count - 1
                            sqlCommand.CommandText = "INSERT INTO Project_BOQ_LineItem_Mapping(ProjectID, BOQLineNo, LineItemCode, Qty, FilePath) VALUES ('" + strProjectID + "', '" + dt4.Rows(j).Item(0).ToString() + "', '" + dt4.Rows(j).Item(1).ToString() + "', '" + dt4.Rows(j).Item(2).ToString() + "', '" + dt4.Rows(j).Item(3).ToString() + "' ) "
                            sqlCommand.ExecuteNonQuery()
                        Next
                    Else
                        sqlCommand.CommandText = "SELECT ItemNo, Qty, Supp_Quot_ID, DataType FROM Project_CostLineItems WHERE projectID = '" + dt1.Rows(i).Item(0).ToString() + "' "
                        sqlDataAdapter.Fill(dt3)

                        For j As Integer = 0 To dt3.Rows.Count - 1
                            sqlCommand.CommandText = "INSERT INTO Project_CostLineItems(ProjectID, ItemNo, Qty, Supp_Quot_ID, DataType) VALUES ('" + strProjectID + "', '" + dt3.Rows(j).Item(0).ToString() + "', '" + dt3.Rows(j).Item(1).ToString() + "', '" + dt3.Rows(j).Item(2).ToString() + "', '" + dt3.Rows(j).Item(3).ToString() + "') "
                            sqlCommand.ExecuteNonQuery()
                        Next
                    End If
                    'copy project crew structure data
                    dt5.Clear()
                    sqlCommand.CommandText = "SELECT Code, Man_Day, DailyOutput, HourlyOutput, ManHour, EquipHour, Lab_Equip, CrewCode FROM Project_CrewStructure WHERE projectID = '" + dt1.Rows(i).Item(0).ToString() + "' "
                    sqlDataAdapter.Fill(dt5)

                    For j As Integer = 0 To dt5.Rows.Count - 1
                        sqlCommand.CommandText = "INSERT INTO Project_CrewStructure(ProjectID, Code, Man_Day, DailyOutput, HourlyOutput, ManHour, EquipHour, Lab_Equip, CrewCode) VALUES ('" + strProjectID + "', '" + dt5.Rows(j).Item(0).ToString() + "', '" + dt5.Rows(j).Item(1).ToString() + "', '" + dt5.Rows(j).Item(2).ToString() + "', '" + dt5.Rows(j).Item(3).ToString() + "', '" + dt5.Rows(j).Item(4).ToString() + "', '" + dt5.Rows(j).Item(5).ToString() + "', '" + dt5.Rows(j).Item(6).ToString() + "', '" + dt5.Rows(j).Item(7).ToString() + "') "
                        sqlCommand.ExecuteNonQuery()
                    Next
                Next
            Next
        End If
        sqlConnection.Close()
        Return strProjectID
    End Function
    Public Function Insert_Selected_Items(strItemPath As String, strProjID As String, strStatus As String, strLangID As String, strCategoryID As String) As String
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "INSERT INTO Items_Selection(ProjectID, SelectedItem_Path, Status, LangID, CategoryID) VALUES ('" + strProjID + "', '" + strItemPath + "', '" + strStatus + "', '" + strLangID + "', '" + strCategoryID + "') "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function Delete_Selected_Items(strProjID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "DELETE FROM Items_Selection WHERE ProjectID = '" + strProjID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetProject_Items_Selection(strProjID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT i.ID, i.SelectedItem_Path, s.StatusE, s.StatusA FROM Items_Selection i LEFT JOIN Item_Status s ON i.Status = s.ID WHERE ProjectID = '" + strProjID + "'"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function Get_Item_Data(strID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID, SelectedItem_Path, LangID, CategoryID FROM Items_Selection WHERE ID = '" + strID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function Insert_Item_Products(strItemID As String, strProductID As String, strQty As String)
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "INSERT INTO Item_Products(Item_ID, Prod_ID, Qty) VALUES('" + strItemID + "', '" + strProductID + "', '" + strQty + "')"
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function DeleteItem(strID As String, strProjectID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "DELETE FROM Item_Products WHERE Item_ID = '" + strID + "' "
        sqlCommand.ExecuteNonQuery()
        sqlCommand.CommandText = "DELETE FROM Items_Selection WHERE ID = '" + strID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlCommand.CommandText = "SELECT COUNT(*) FROM Items_Selection WHERE ProjectID = '" + strProjectID + "' AND Status = 0"
        If (sqlCommand.ExecuteScalar().ToString() = "0") Then
            sqlCommand.CommandText = "UPDATE Project SET IsComplete = 1 WHERE ID = '" + strProjectID + "'  "
            sqlCommand.ExecuteNonQuery()
        End If

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function Get_Item_Products(strProjID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Prod_ID, Qty FROM Item_Products WHERE Item_ID = '" + strProjID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function DeleteItem_Products(strItemID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "DELETE FROM Item_Products WHERE Item_ID = '" + strItemID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlCommand.CommandText = "UPDATE Items_Selection SET Status = 1 WHERE ID = '" + strItemID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetProject_Items_Selection(strProjID As String, strPath As String) As String
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim retVal As String = String.Empty
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT SelectedItem_Path FROM Items_Selection WHERE ProjectID = '" + strProjID + "' AND SelectedItem_Path = '" + strPath + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        If (dt.Rows.Count > 0) Then
            retVal = dt.Rows(0).Item(0).ToString()
        End If

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function Get_Item_Products_Final(strItemID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT i.ID, MAX(MapID)MapID, MAX(NameE)NameE, MAX(NameA)NameA, MAX(PropE)PropE, MAX(PropA)PropA, MAX(DescE)DescE, MAX(DescA)DescA, MAX(Unit)Unit, MAX(ManHour)ManHour, MAX(EquipHour)EquipHour, MAX(Qty)Qty,MAX(p.ID) ProdID FROM Item_Products i INNER JOIN ProductDetails p ON i.Prod_ID = p.ID WHERE i.Item_ID = '" + strItemID + "' GROUP BY i.ID"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function DeleteItem_Product(strID As String, strItemID As String, strProjectID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "DELETE FROM Item_Products WHERE ID = '" + strID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlCommand.CommandText = "SELECT COUNT(*) FROM Item_Products WHERE Item_ID = '" + strItemID + "'"
        If (sqlCommand.ExecuteScalar().ToString() = "0") Then
            sqlCommand.CommandText = "UPDATE Items_Selection SET Status = 0 WHERE ID = '" + strItemID + "'  "
            sqlCommand.ExecuteNonQuery()
            sqlCommand.CommandText = "UPDATE Project SET IsComplete = 0 WHERE ID = '" + strProjectID + "'  "
            sqlCommand.ExecuteNonQuery()
        End If

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function Get_Pending_List(strProjID As String) As String
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim retVal As String = String.Empty

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT COUNT(*) FROM Items_Selection WHERE ProjectID = '" + strProjID + "' AND Status = 0"
        retVal = sqlCommand.ExecuteScalar().ToString()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetProductByID(strID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt, dt1 As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT *, '' As ApprovedByE, '' As ApprovedByA FROM ProductDetails WHERE ID = '" + strID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Approval_Agencies"
        sqlDataAdapter.Fill(dt1)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProductBySupplierID(strSuppID As String, strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt, dt1 As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT DISTINCT p.*, '' As ApprovedByE, '' As ApprovedByA FROM ProductDetails p LEFT JOIN MainHierarchy1 m ON p.MapID = m.No WHERE SupplierID = '" + strSuppID + "' AND p.CompID = 0 AND (p.NameE LIKE '%" + strSearch + "%' OR p.NameA LIKE '%" + strSearch + "%' OR m.NameE LIKE '%" + strSearch + "%' OR m.NameA LIKE '%" + strSearch + "%' OR p.MapID LIKE '%" + strSearch + "%') AND p.Status IN(0,2)"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Approval_Agencies"
        sqlDataAdapter.Fill(dt1)

        Dim strApprovedByE, strApprovedByA As String
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim arrApprovedBy As String() = dt.Rows(i).Item(13).ToString().Split(",")
            If (arrApprovedBy.Length > 0 And arrApprovedBy(0).Length > 0) Then
                For j As Integer = 0 To arrApprovedBy.Length - 1
                    For k As Integer = 0 To dt1.Rows.Count - 1
                        If (arrApprovedBy(j) = dt1.Rows(k).Item(0).ToString()) Then

                            If (dt1.Rows(k).Item(0).ToString() = "99") Then
                                strApprovedByE = strApprovedByE + dt.Rows(i).Item(14).ToString() + ","
                                strApprovedByA = strApprovedByA + dt.Rows(i).Item(14).ToString() + ","
                            Else
                                strApprovedByE = strApprovedByE + dt1.Rows(k).Item(1).ToString() + ","
                                strApprovedByA = strApprovedByA + dt1.Rows(k).Item(2).ToString() + ","
                            End If
                        End If
                    Next
                Next

                If (strApprovedByE.Length > 0) Then
                    strApprovedByE = strApprovedByE.Substring(0, strApprovedByE.Length - 1)
                End If
                If (strApprovedByA.Length > 0) Then
                    strApprovedByA = strApprovedByA.Substring(0, strApprovedByA.Length - 1)
                End If
                dt.Rows(i).Item(23) = strApprovedByE
                dt.Rows(i).Item(24) = strApprovedByA
                strApprovedByE = ""
                strApprovedByA = ""
            End If
        Next

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProductBySupplier(SuppIDFrom As String, SuppIDTo As String, strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt, dt1 As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT DISTINCT *, '' As ApprovedByE, '' As ApprovedByA FROM ProductDetails WHERE (SupplierID BETWEEN " + SuppIDFrom + " AND " + SuppIDTo + ") AND (NameE LIKE '%" + strSearch + "%' OR NameA LIKE '%" + strSearch + "%') AND Status = 0"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Approval_Agencies"
        sqlDataAdapter.Fill(dt1)

        Dim strApprovedByE, strApprovedByA As String
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim arrApprovedBy As String() = dt.Rows(i).Item(13).ToString().Split(",")
            If (arrApprovedBy.Length > 0 And arrApprovedBy(0).Length > 0) Then
                For j As Integer = 0 To arrApprovedBy.Length - 1
                    For k As Integer = 0 To dt1.Rows.Count - 1
                        If (arrApprovedBy(j) = dt1.Rows(k).Item(0).ToString()) Then

                            If (dt1.Rows(k).Item(0).ToString() = "99") Then
                                strApprovedByE = strApprovedByE + dt.Rows(i).Item(14).ToString() + ","
                                strApprovedByA = strApprovedByA + dt.Rows(i).Item(14).ToString() + ","
                            Else
                                strApprovedByE = strApprovedByE + dt1.Rows(k).Item(1).ToString() + ","
                                strApprovedByA = strApprovedByA + dt1.Rows(k).Item(2).ToString() + ","
                            End If
                        End If
                    Next
                Next

                If (strApprovedByE.Length > 0) Then
                    strApprovedByE = strApprovedByE.Substring(0, strApprovedByE.Length - 1)
                End If
                If (strApprovedByA.Length > 0) Then
                    strApprovedByA = strApprovedByA.Substring(0, strApprovedByA.Length - 1)
                End If
                dt.Rows(i).Item(23) = strApprovedByE
                dt.Rows(i).Item(24) = strApprovedByA
                strApprovedByE = ""
                strApprovedByA = ""
            End If
        Next

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetAllSuppliers() As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT DISTINCT p.SupplierID As ID, u.FirstName + ' ' + u.LastName As Name FROM ProductDetails p INNER JOIN Users u ON p.SupplierID = u.ID "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProduct_Image_Data(strID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ImageFile, DocFile FROM ProductDetails WHERE ID = '" + strID + "'  "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetAssociatedSuppliers(strProdID As String, strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT SupplierID, NameE, NameA, MinPrice, MaxPrice FROM AssociatedSuppliers WHERE ProductID = '" + strProdID + "' AND (NameE LIKE '%" + strSearch + "%' OR NameA LIKE '%" + strSearch + "%') "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetAssociatedCrewStructures(strProdID As String, strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Code, DescE, DescA, ProductionRate, HourlyRate FROM ProductCrewStructure WHERE ProductID = '" + strProdID + "' AND (CODE LIKE '%" + strSearch + "%' OR DESCE LIKE '%" + strSearch + "%' OR DESCA LIKE '%" + strSearch + "%') "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProducts_1(strMapID As String, strSearch As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt, dt1, dt2 As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT DISTINCT p.ID, p.MapID, p.NameE, p.NameA, p.PropE, p.PropA, p.DescE, p.DescA, p.Unit, p.ManHour, p.EquipHour, p.ApprovedBy, Other, '' As ApprovedByE, '' As ApprovedByA, Specification, Other_Spec, '' As SpecificationE, '' As SpecificationA, RefNo FROM ProductDetails p WHERE MapID = '" + strMapID + "' AND CompID = '" + strCompID + "' AND (p.NameE LIKE '%" + strSearch + "%' OR p.NameA LIKE '%" + strSearch + "%' OR p.ID LIKE '%" + strSearch + "%'  OR p.MapID LIKE '%" + strSearch + "%') AND (Status = 1 OR Status IS NULL)"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Approval_Agencies"
        sqlDataAdapter.Fill(dt1)

        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Product_Specification"
        sqlDataAdapter.Fill(dt2)

        Dim strApprovedByE, strApprovedByA As String
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim arrApprovedBy As String() = dt.Rows(i).Item(11).ToString().Split(",")
            If (arrApprovedBy.Length > 0 And arrApprovedBy(0).Length > 0) Then
                For j As Integer = 0 To arrApprovedBy.Length - 1
                    For k As Integer = 0 To dt1.Rows.Count - 1
                        If (arrApprovedBy(j) = dt1.Rows(k).Item(0).ToString()) Then

                            If (dt1.Rows(k).Item(0).ToString() = "99") Then
                                strApprovedByE = strApprovedByE + dt.Rows(i).Item(12).ToString() + ","
                                strApprovedByA = strApprovedByA + dt.Rows(i).Item(12).ToString() + ","
                            Else
                                strApprovedByE = strApprovedByE + dt1.Rows(k).Item(1).ToString() + ","
                                strApprovedByA = strApprovedByA + dt1.Rows(k).Item(2).ToString() + ","
                            End If
                        End If
                    Next
                Next

                If (strApprovedByE.Length > 0) Then
                    strApprovedByE = strApprovedByE.Substring(0, strApprovedByE.Length - 1)
                End If
                If (strApprovedByA.Length > 0) Then
                    strApprovedByA = strApprovedByA.Substring(0, strApprovedByA.Length - 1)
                End If
                dt.Rows(i).Item(13) = strApprovedByE
                dt.Rows(i).Item(14) = strApprovedByE
                strApprovedByE = ""
                strApprovedByA = ""
            End If
        Next

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim arrApprovedBy As String() = dt.Rows(i).Item(15).ToString().Split(",")
            If (arrApprovedBy.Length > 0 And arrApprovedBy(0).Length > 0) Then
                For j As Integer = 0 To arrApprovedBy.Length - 1
                    For k As Integer = 0 To dt2.Rows.Count - 1
                        If (arrApprovedBy(j) = dt2.Rows(k).Item(0).ToString()) Then

                            If (dt2.Rows(k).Item(0).ToString() = "99") Then
                                strApprovedByE = strApprovedByE + dt.Rows(i).Item(16).ToString() + ","
                                strApprovedByA = strApprovedByA + dt.Rows(i).Item(16).ToString() + ","
                            Else
                                strApprovedByE = strApprovedByE + dt2.Rows(k).Item(1).ToString() + ","
                                strApprovedByA = strApprovedByA + dt2.Rows(k).Item(2).ToString() + ","
                            End If
                        End If
                    Next
                Next

                If (strApprovedByE.Length > 0) Then
                    strApprovedByE = strApprovedByE.Substring(0, strApprovedByE.Length - 1)
                End If
                If (strApprovedByA.Length > 0) Then
                    strApprovedByA = strApprovedByA.Substring(0, strApprovedByA.Length - 1)
                End If
                dt.Rows(i).Item(17) = strApprovedByE
                dt.Rows(i).Item(18) = strApprovedByE
                strApprovedByE = ""
                strApprovedByA = ""
            End If
        Next

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProducts_BySupplier(strMapID As String, strSearch As String, strSupplierID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt, dt1, dt2 As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT DISTINCT p.ID, p.MapID, p.NameE, p.NameA, p.PropE, p.PropA, p.DescE, p.DescA, p.Unit, p.ManHour, p.EquipHour, p.ApprovedBy, Other, '' As ApprovedByE, '' As ApprovedByA, Specification, Other_Spec, '' As SpecificationE, '' As SpecificationA, RefNo FROM ProductDetails p WHERE TempMapID = '" + strMapID + "' AND (p.NameE LIKE '%" + strSearch + "%' OR p.NameA LIKE '%" + strSearch + "%' OR p.ID LIKE '%" + strSearch + "%' OR p.TempMapID LIKE '%" + strSearch + "%') AND SupplierID = '" + strSupplierID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Approval_Agencies"
        sqlDataAdapter.Fill(dt1)

        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Product_Specification"
        sqlDataAdapter.Fill(dt2)

        Dim strApprovedByE, strApprovedByA As String
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim arrApprovedBy As String() = dt.Rows(i).Item(11).ToString().Split(",")
            If (arrApprovedBy.Length > 0 And arrApprovedBy(0).Length > 0) Then
                For j As Integer = 0 To arrApprovedBy.Length - 1
                    For k As Integer = 0 To dt1.Rows.Count - 1
                        If (arrApprovedBy(j) = dt1.Rows(k).Item(0).ToString()) Then

                            If (dt1.Rows(k).Item(0).ToString() = "99") Then
                                strApprovedByE = strApprovedByE + dt.Rows(i).Item(12).ToString() + ","
                                strApprovedByA = strApprovedByA + dt.Rows(i).Item(12).ToString() + ","
                            Else
                                strApprovedByE = strApprovedByE + dt1.Rows(k).Item(1).ToString() + ","
                                strApprovedByA = strApprovedByA + dt1.Rows(k).Item(2).ToString() + ","
                            End If
                        End If
                    Next
                Next

                If (strApprovedByE.Length > 0) Then
                    strApprovedByE = strApprovedByE.Substring(0, strApprovedByE.Length - 1)
                End If
                If (strApprovedByA.Length > 0) Then
                    strApprovedByA = strApprovedByA.Substring(0, strApprovedByA.Length - 1)
                End If
                dt.Rows(i).Item(13) = strApprovedByE
                dt.Rows(i).Item(14) = strApprovedByE
                strApprovedByE = ""
                strApprovedByA = ""
            End If
        Next

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim arrApprovedBy As String() = dt.Rows(i).Item(15).ToString().Split(",")
            If (arrApprovedBy.Length > 0 And arrApprovedBy(0).Length > 0) Then
                For j As Integer = 0 To arrApprovedBy.Length - 1
                    For k As Integer = 0 To dt2.Rows.Count - 1
                        If (arrApprovedBy(j) = dt2.Rows(k).Item(0).ToString()) Then

                            If (dt2.Rows(k).Item(0).ToString() = "99") Then
                                strApprovedByE = strApprovedByE + dt.Rows(i).Item(16).ToString() + ","
                                strApprovedByA = strApprovedByA + dt.Rows(i).Item(16).ToString() + ","
                            Else
                                strApprovedByE = strApprovedByE + dt2.Rows(k).Item(1).ToString() + ","
                                strApprovedByA = strApprovedByA + dt2.Rows(k).Item(2).ToString() + ","
                            End If
                        End If
                    Next
                Next

                If (strApprovedByE.Length > 0) Then
                    strApprovedByE = strApprovedByE.Substring(0, strApprovedByE.Length - 1)
                End If
                If (strApprovedByA.Length > 0) Then
                    strApprovedByA = strApprovedByA.Substring(0, strApprovedByA.Length - 1)
                End If
                dt.Rows(i).Item(17) = strApprovedByE
                dt.Rows(i).Item(18) = strApprovedByE
                strApprovedByE = ""
                strApprovedByA = ""
            End If
        Next

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetCountries() As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Country"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function Create_Company_User(strUserName As String, strCompName As String, strCountryID As String, strPass As String, strEmail As String, strExpiryDate As String, strClientType As String, strPhone As String, strFax As String, strPOBox As String, strzipCode As String, strWebsite As String, strStatus As String, strFileName As String, strTemp As String)
        Dim retVal As Integer = 0
        Dim strCompID As String = String.Empty
        Dim strUserID As String = String.Empty
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim sqlTrans As SqlTransaction = Nothing

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        'sqlCommand.CommandText = "SELECT UserID FROM Users WHERE UserID = '" + strUserName + "' "
        'If (sqlCommand.ExecuteScalar() IsNot Nothing) Then
        '    retVal = 1
        'Else
        '    sqlCommand.CommandText = "INSERT INTO Company(Name, CountryID, UsersCount, ExpiryDate, ClientTypeID, Phone, fax, POBox, ZipCode, Website, Status, FileName, Email) VALUES('" + strCompName + "', '" + strCountryID + "', 5, '" + strExpiryDate + "', '" + strClientType + "', '" + strPhone + "', '" + strFax + "', '" + strPOBox + "', '" + strzipCode + "', '" + strWebsite + "', '" + strStatus + "', '" + strFileName + "', '" + strEmail + "') SELECT MAX(ID) FROM Company"
        '    strCompID = sqlCommand.ExecuteScalar.ToString()

        '    sqlCommand.CommandText = "INSERT INTO Users(UserID, Password, Email, CompID) VALUES('" + strUserName + "', '" + Encryption.Encrypt(strPass) + "', '" + strEmail + "', '" + strCompID + "') SELECT MAX(ID) FROM Users"
        '    strCompID = sqlCommand.ExecuteScalar.ToString()

        '    sqlCommand.CommandText = "INSERT INTO User_Role_Mapping(UserID, RoleID) VALUES('" + strCompID + "', 888) "
        '    sqlCommand.ExecuteNonQuery()
        'End If
        sqlTrans = sqlConnection.BeginTransaction()
        sqlCommand.Transaction = sqlTrans

        Try

        
        sqlCommand.CommandText = "SELECT ID FROM Company WHERE Name = '" + strCompName + "' AND CountryID = '" + strCountryID + "' "
        If (sqlCommand.ExecuteScalar() IsNot Nothing) Then
            retVal = 1
        Else
                sqlCommand.CommandText = "INSERT INTO Company(Name, CountryID, UsersCount, ExpiryDate, ClientTypeID, Phone, Fax, POBox, ZipCode, Website, Status, FileName, Email, IsTemp) VALUES('" + strCompName + "', '" + strCountryID + "', 5, '" + strExpiryDate + "', '" + strClientType + "', '" + strPhone + "', '" + strFax + "', '" + strPOBox + "', '" + strzipCode + "', '" + strWebsite + "', '" + strStatus + "', '" + strFileName + "', '" + strEmail + "', '" + strTemp + "') SELECT MAX(ID) FROM Company"
            strCompID = sqlCommand.ExecuteScalar.ToString()

                sqlCommand.CommandText = "INSERT INTO Users(UserID, Password, Email, CompID, FirstName, LastName) VALUES('" + strUserName + "', '" + Encryption.Encrypt(strPass) + "', '" + strEmail + "', '" + strCompID + "', '" + strCompName + "' , ' ') SELECT MAX(ID) FROM Users"
                strUserID = sqlCommand.ExecuteScalar.ToString()

                sqlCommand.CommandText = "INSERT INTO User_Role_Mapping(UserID, RoleID) VALUES('" + strUserID + "', 888) "
            sqlCommand.ExecuteNonQuery()

            sqlCommand.CommandText = "sp_CopyCompanyData"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("CompID", strCompID)
            sqlCommand.ExecuteNonQuery()
        End If

        sqlTrans.Commit()
            'sqlConnection.Close()
            Return retVal
        Catch ex As Exception
            sqlTrans.Rollback()
            Return 1
        Finally
            sqlConnection.Close()
        End Try
    End Function
    Public Function GetLimitedUserRoles() As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM User_Role WHERE ID NOT IN(99,999,888,555,777,666)"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function SearchLimitedUsers(strUserID As String, strCompanyID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT u.UserID, MAX(Password) As Password, MAX(mp.RoleID), MAX(NameE) As CategoryNameE, MAX(NameA) As CategoryNameA, u.ID, MAX(u.FirstName) + ' ' + MAX(u.LastName) As Name, MAX(u.Email) Email FROM Users u INNER JOIN User_Role_Mapping mp ON mp.UserID = u.ID LEFT JOIN User_Role r ON mp.RoleID = r.ID WHERE (u.UserID LIKE '%" + strUserID + "%' OR u.FirstName LIKE '%" + strUserID + "%' OR u.LastName LIKE '%" + strUserID + "%') AND CompID = '" + strCompanyID + "' GROUP BY u.UserID,u.ID"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i).Item(1) = Encryption.Decrypt(dt.Rows(i).Item(1).ToString())
        Next
        sqlConnection.Close()
        Return dt
    End Function
    Public Function AddNewBuilding(strProjectName As String, strBuildingName As String, strProjectID As String, strCreationDate As String, strCreatedBy As String, strlangID As String) As String
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim retVal As String = "0"
        Dim dt As New DataTable()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "SELECT ID FROM Project WHERE BuildingName = '" + strBuildingName + "' AND Name = '" + strProjectName + "'"
        If (sqlCommand.ExecuteScalar() IsNot Nothing) Then 'Record already exist with this building name
            retVal = "-1"
        Else
            sqlCommand.CommandText = "SELECT Status, CompID FROM Project WHERE ID = '" + strProjectID + "' "
            Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
            sqlDataAdapter.Fill(dt)

            sqlCommand.CommandText = "INSERT INTO Project(Name,BuildingName,CreationDate,CreatedBy,Status,CompID,LangID) VALUES ('" + strProjectName + "', '" + strBuildingName + "', '" + strCreationDate + "', '" + strCreatedBy + "', '" + dt.Rows(0).Item(0).ToString() + "', '" + dt.Rows(0).Item(1).ToString() + "', '" + strlangID + "') SELECT MAX(ID) FROM Project"
            retVal = sqlCommand.ExecuteScalar().ToString()
        End If

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function CheckBuildingExistence(strProjectName As String, strBuildingName As String, strProjectID As String, strCreationDate As String, strCreatedBy As String, strlangID As String, strCompID As String) As String
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim retVal As String = "0"
        Dim dt As New DataTable()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "SELECT ID FROM Project WHERE BuildingName = '" + strBuildingName + "' AND Name = '" + strProjectName + "' AND CompID = '" + strCompID + "' "
        If (sqlCommand.ExecuteScalar() IsNot Nothing) Then 'Record already exist with this building name
            retVal = "-1"
        Else
            sqlCommand.CommandText = "SELECT Status, CompID, ProjectType, ISNULL(BuildingName,0)BName FROM Project WHERE ID = '" + strProjectID + "' "
            Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
            sqlDataAdapter.Fill(dt)

            If (dt.Rows.Count > 0) Then
                If (dt.Rows(0).Item(3).ToString() = "0") Then
                    sqlCommand.CommandText = "DELETE FROM Project WHERE ID = '" + strProjectID + "' "
                    sqlCommand.ExecuteNonQuery()
                End If
            End If

            sqlCommand.CommandText = "INSERT INTO Project(Name,BuildingName,CreationDate,CreatedBy,Status,CompID,LangID,IsComplete,ProjectType) VALUES ('" + strProjectName + "', '" + strBuildingName + "', '" + strCreationDate + "', '" + strCreatedBy + "', '" + dt.Rows(0).Item(0).ToString() + "', '" + dt.Rows(0).Item(1).ToString() + "', '" + strlangID + "', 0, '" + dt.Rows(0).Item(2).ToString() + "') SELECT MAX(ID) FROM Project"
            retVal = sqlCommand.ExecuteScalar().ToString()
        End If

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function SetProjectStatus(strProjectID As String) As String
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim retVal As String = "0"

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        'sqlCommand.CommandText = "SELECT ID FROM Items_Selection WHERE ProjectID = '" + strProjectID + "' AND Status = 0"
        'If (sqlCommand.ExecuteScalar() Is Nothing) Then
        '    sqlCommand.CommandText = "UPDATE Project SET IsComplete = 1 WHERE ID = '" + strProjectID + "'  "
        '    sqlCommand.ExecuteNonQuery()
        'End If   pehle ka code ha

        sqlCommand.CommandText = "UPDATE Project SET IsComplete = 1 WHERE ID = '" + strProjectID + "'  "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function Delete_All_Item_Products(strItemID As String, strProjectID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "DELETE FROM Item_Products WHERE Item_ID = '" + strItemID + "' "
        sqlCommand.ExecuteNonQuery()
        sqlCommand.CommandText = "UPDATE Items_Selection SET Status = 0 WHERE ID = '" + strItemID + "' "
        sqlCommand.ExecuteNonQuery()
        sqlCommand.CommandText = "UPDATE Project SET IsComplete = 0 WHERE ID = '" + strProjectID + "'  "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetAllReports() As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Reports"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetReportsByRole(strRoleID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable
        Dim strIDs As String = ""

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "SELECT ReportIDs FROM Report_Role_Mapping WHERE RoleID = '" + strRoleID + "' "
        If (sqlCommand.ExecuteScalar() IsNot Nothing) Then
            strIDs = sqlCommand.ExecuteScalar().ToString()

            sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Reports WHERE ID IN( " + strIDs + ") "
            Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
            sqlDataAdapter.Fill(dt)
        End If
        sqlConnection.Close()
        Return dt
    End Function
    Public Function Get_Project_Items(strProjID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID FROM Items_Selection WHERE ProjectID = '" + strProjID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function Get_Full_Report_Data(strProjID As String, strUserName As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT DISTINCT '" + strUserName + "' As UserName, p.ID, p.NameE, p.DescE, p.PropE, i.Qty, p.ImageFile,p.DocFile, p.Unit, p.ManHour, p.EquipHour FROM Items_Selection s INNER JOIN Item_Products i ON s.ID =  i.Item_ID INNER JOIN ProductDetails p ON p.ID =  i.Prod_ID WHERE s.ProjectID = '" + strProjID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProductPath(strProductID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT p.TempMapID, m.NameE, m.NameA FROM ProductDetails p INNER JOIN MainHierarchy1 m ON m.No = p.TempMapID WHERE p.ID = '" + strProductID + "'"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetClientTypes() As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM ClientType"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function Insert_Clinet_Type(strNameE As String, strNameA As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "SELECT COUNT(*) FROM ClientType WHERE NameE = '" + strNameE + "' AND NameA = '" + strNameA + "' "
        If (sqlCommand.ExecuteScalar().ToString() = "0") Then
            sqlCommand.CommandText = "INSERT INTO ClientType(NameE, NameA) VALUES ('" + strNameE + "', '" + strNameA + "') "
            sqlCommand.ExecuteNonQuery()
        Else
            retVal = "-1"
        End If
        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetProductCrewStructure_Labour(strProductID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT c.ID, c.Code, l.ProfE, l.ProfA, c.Man_Day, DailyOutput, HourlyOutput, ManHour, EquipHour, l.Nationality FROM ProductCrewStructure c LEFT JOIN Labour l ON l.Code = c.Code WHERE c.Lab_Equip = 1 AND c.ProductID = '" + strProductID + "' ORDER BY l.ProfE, l.ProfA"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProductivityHistory(ByVal strCode As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT TOP(5) ID, Code, DateTime, DO FROM ProductivityHistory WHERE Code = '" + strCode + "' ORDER BY DateTime DESC"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProductCrewStructure_Equipment(strProductID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT c.ID, c.Code, e.DescE, e.DescA, c.Man_Day, DailyOutput, HourlyOutput, ManHour, EquipHour FROM ProductCrewStructure c LEFT JOIN Equipment e ON c.Code = e.Code WHERE c.Lab_Equip = 2 AND c.ProductID = '" + strProductID + "' ORDER BY e.DescE, e.DescA"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetCrewStructureByID(strCode As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT TOP(1) c.ID, c.Code, c.NameE, c.NameA, c.DescE, c.DescA, c.Unit, l.Nationality, c.DailyOutput, c.HourlyOutput, c.ManHour, c.EquipHour FROM CrewStructure c LEFT JOIN Labour l ON l.Code = c.CrewCode WHERE c.Code = '" + strCode + "' AND Lab_Equip = 1"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetCrewStructureLabour(strCode As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT c.ID, c.CrewCode As Code, l.ProfE, l.ProfA, c.Man_Day, c.DailyOutput, c.HourlyOutput, c.ManHour, c.EquipHour, l.Nationality FROM CrewStructure c LEFT JOIN Labour l ON l.Code = c.CrewCode AND l.CompID = 0 WHERE c.Lab_Equip = 1 AND c.Code = '" + strCode + "' ORDER BY l.ProfE, l.ProfA"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetCreStructureEquipment(strCode As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT c.ID, c.CrewCode As Code, e.DescE, e.DescA, c.Man_Day, DailyOutput, HourlyOutput, ManHour, EquipHour FROM CrewStructure c LEFT JOIN Equipment e ON e.Code = c.CrewCode AND e.CompID = 0 WHERE c.Lab_Equip = 2 AND c.Code = '" + strCode + "' ORDER BY e.DescE, e.DescA"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetAllProductivities(strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT DISTINCT Code, NameE, NameA, DescE, DescA, DailyOutput FROM CrewStructure WHERE Code LIKE '%" + strSearch + "%' OR  NameE LIKE '%" + strSearch + "%' OR NameA LIKE '%" + strSearch + "%'"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProductivities() As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        'sqlCommand.CommandText = "SELECT DISTINCT Code, Code + '  ' + NameE As NameE, Code + '  ' + NameA As NameA FROM CrewStructure"
        sqlCommand.CommandText = "SELECT DISTINCT Code, Code As NameE, Code As NameA FROM CrewStructure"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProductivityLaboursByCode(ByVal strCode As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        'sqlCommand.CommandText = "SELECT c.ID, l.Code, l.ProfE, l.ProfA, c.Man_Day, l.Nationality, c.DailyOutput, c.MaterialCost FROM CrewStructure c LEFT JOIN Labour l ON l.Code = c.CrewCode WHERE c.Code = '" + strCode + "' AND c.Lab_Equip = 1"
        sqlCommand.CommandText = "SELECT c.ID, c.CrewCode As Code, l.ProfE, l.ProfA, c.Man_Day, DailyOutput, HourlyOutput, ManHour, EquipHour, l.Nationality FROM CrewStructure c LEFT JOIN Labour l ON l.Code = c.CrewCode AND l.CompID = '" + strCompID + "' WHERE c.Code = '" + strCode + "' AND c.Lab_Equip = 1"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProductivityEquipmentsByCode(ByVal strCode As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT c.ID, c.CrewCode As Code, e.DescE, e.DescA, c.Man_Day, c.DailyOutput FROM CrewStructure c LEFT JOIN Equipment e ON e.Code = c.CrewCode AND e.CompID = '" + strCompID + "' WHERE c.Code = '" + strCode + "' AND c.Lab_Equip = 2 "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProjectCreatedByUser(strProjectID As String) As String
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim strCreatedBy As String = ""

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT CreatedBy FROM Project WHERE ID = '" + strProjectID + "' "
        strCreatedBy = sqlCommand.ExecuteScalar().ToString()
       
        sqlConnection.Close()
        Return strCreatedBy
    End Function
    Public Function GetAllCompanies() As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT DISTINCT ID, Name FROM Company"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetCompanyByName(strCompanyName As String) As String
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim strRetVal As String = "-1"

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID FROM Company WHERE Name = '" + strCompanyName + "' AND Status = 0"

        If (sqlCommand.ExecuteScalar() IsNot Nothing) Then
            strRetVal = sqlCommand.ExecuteScalar().ToString()
        End If
        sqlConnection.Close()
        Return strRetVal
    End Function
    Public Function GetCompanyByID(strCompanyID As String) As String
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim strRetVal As String = "-1"

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID FROM Company WHERE ID = '" + strCompanyID + "' AND Status = 0"

        If (sqlCommand.ExecuteScalar() IsNot Nothing) Then
            strRetVal = sqlCommand.ExecuteScalar().ToString()
        End If
        sqlConnection.Close()
        Return strRetVal
    End Function
    Public Function DeleteHistory(ByVal strID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "DELETE FROM ProductivityHistory WHERE ID = '" + strID + "' "
        sqlCommand.ExecuteNonQuery()
        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetProductivityDocs(ByVal strCode As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID, Code, Name, DateTime, Ext FROM ProductivityDocuments WHERE Code = '" + strCode + "' ORDER BY DateTime DESC"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i).Item(3) = DateTime.ParseExact(dt.Rows(i).Item(3), "yyyyMMddHHmmss", Nothing).ToString()
        Next
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetReferenceData(ByVal strSearch As String, ByVal strCode As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT * FROM History WHERE LineNum = '" + strCode + "' AND ( Source LIKE '%" + strSearch + "%' OR Code LIKE '%" + strSearch + "%' OR DescE LIKE '%" + strSearch + "%' OR DescA LIKE '%" + strSearch + "%') "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProjectCostLineItems(strProjectID As String, strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT MAX(li.ID)ID, li.ItemNo LineItemNo, mh.NameE LineItemNameE, mh.NameA LineItemNameA, mh.CrewCode, li.Qty, CASE mh.IsPercent WHEN 0 THEN mh.Cost ELSE mh.Cost + '%' END Cost FROM Project_CostLineItems li LEFT JOIN Project_CrewStructure cs on cs.ProjectID = li.ProjectID INNER JOIN MainHierarchy1 mh on mh.[No] = li.ItemNo WHERE li.ProjectID = '" + strProjectID + "' AND (li.ItemNo LIKE '%" + strSearch + "%' OR mh.NameE LIKE '%" + strSearch + "%' OR mh.NameA LIKE '%" + strSearch + "%') GROUP BY li.ItemNo, mh.NameE, mh.NameA, mh.CrewCode, li.Qty, mh.IsPercent, mh.Cost"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function SaveProjectData(strProjectID As String, strCostLineNo As String, strCompID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim dt As New DataTable()
        Dim dtCSExisting As New DataTable()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "SELECT ProductID,Man_Day,DO,HO,MH,EH,Lab_Equip,Code FROM ProductCrewStructure INNER JOIN MainHierarchy1 ON MainHierarchy1.CrewCode = ProductID AND MainHierarchy1.CompID = '" + strCompID + "'  WHERE No = '" + strCostLineNo + "' AND ProductCrewStructure.CompID = '" + strCompID + "'"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dtCSExisting)

        For i As Integer = 0 To dtCSExisting.Rows.Count - 1
            sqlCommand.CommandText = "SELECT COUNT(*) FROM Project_CrewStructure WHERE ProjectID = '" + strProjectID + "' AND CrewCode = '" + dtCSExisting.Rows(i).Item(7).ToString() + "' AND Lab_Equip = '" + dtCSExisting.Rows(i).Item(6).ToString() + "' "
            If (sqlCommand.ExecuteScalar().ToString().Equals("0")) Then
                sqlCommand.CommandText = "INSERT INTO Project_CrewStructure(ProjectID,Code,Man_Day,DailyOutput,HourlyOutput,ManHour,EquipHour,Lab_Equip,CrewCode) SELECT '" + strProjectID + "',  '" + dtCSExisting.Rows(i).Item(0).ToString() + "', '" + dtCSExisting.Rows(i).Item(1).ToString() + "', '" + dtCSExisting.Rows(i).Item(2).ToString() + "', '" + dtCSExisting.Rows(i).Item(3).ToString() + "', '" + dtCSExisting.Rows(i).Item(4).ToString() + "', '" + dtCSExisting.Rows(i).Item(5).ToString() + "', '" + dtCSExisting.Rows(i).Item(6).ToString() + "', '" + dtCSExisting.Rows(i).Item(7).ToString() + "' "
                sqlCommand.ExecuteNonQuery()
            End If
        Next

        sqlCommand.CommandText = "SELECT COUNT(*) FROM Project_CostLineItems WHERE ProjectID = '" + strProjectID + "' AND ItemNo = '" + strCostLineNo + "' "
        If (sqlCommand.ExecuteScalar().ToString().Equals("0")) Then
            sqlCommand.CommandText = "INSERT INTO Project_CostLineItems(ProjectID, ItemNo) VALUES('" + strProjectID + "', '" + strCostLineNo + "') "
            sqlCommand.ExecuteNonQuery()
        End If

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function UpdateProject_CostLineItems(strProjectID As String, strCostLineNo As String, strQty As String) As Integer
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim retVal As Integer = 0

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "UPDATE Project_CostLineItems SET Qty = '" + strQty + "' WHERE ProjectID = '" + strProjectID + "' AND ItemNo = '" + strCostLineNo + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetProjectCrewStructure_Labour(strProjectID As String, strCrewCode As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT c.ID, c.Code, l.ProfE, l.ProfA, c.Man_Day, DailyOutput, HourlyOutput, ManHour, EquipHour, l.Nationality, c.CrewCode FROM Project_CrewStructure c LEFT JOIN Labour l ON l.Code = c.CrewCode WHERE c.Lab_Equip = 1 AND c.ProjectID = '" + strProjectID + "' AND c.Code = '" + strCrewCode + "' AND l.CompID = '" + strCompID + "' ORDER BY l.ProfE, l.ProfA"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProjectCrewStructure_Equipment(strProjectID As String, strCrewCode As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT c.ID, c.Code, e.DescE, e.DescA, c.Man_Day, DailyOutput, HourlyOutput, ManHour, EquipHour, c.CrewCode FROM Project_CrewStructure c LEFT JOIN Equipment e ON c.CrewCode = e.Code WHERE c.Lab_Equip = 2 AND c.ProjectID = '" + strProjectID + "' AND c.Code = '" + strCrewCode + "' AND e.CompID = '" + strCompID + "' ORDER BY e.DescE, e.DescA"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProjectCostLineItemsFROMBOQ(strProjectID As String, strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT MAX(li.ID)ID, li.LineItemCode LineItemNo, mh.NameE LineItemNameE, mh.NameA LineItemNameA, mh.CrewCode, li.Qty, CASE mh.IsPercent WHEN 0 THEN mh.Cost ELSE mh.Cost + '%' END Cost FROM Project_BOQ_LineItem_Mapping li LEFT JOIN Project_CrewStructure cs on cs.ProjectID = li.ProjectID INNER JOIN MainHierarchy1 mh on mh.[No] = li.LineItemCode WHERE li.ProjectID = '" + strProjectID + "' AND (li.LineItemCode LIKE '%" + strSearch + "%' OR mh.NameE LIKE '%" + strSearch + "%' OR mh.NameA LIKE '%" + strSearch + "%') GROUP BY li.LineItemCode, mh.NameE, mh.NameA, mh.CrewCode, li.Qty, mh.IsPercent, mh.Cost"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function UpdateProject_CostLineItems_BOQ(strProjectID As String, strCostLineNo As String, strQty As String) As Integer
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim retVal As Integer = 0

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "UPDATE Project_BOQ_LineItem_Mapping SET Qty = '" + strQty + "' WHERE ProjectID = '" + strProjectID + "' AND LineItemCode = '" + strCostLineNo + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    'Public Function GetLineItem_DailyOutput(strCSCode As String) As DataTable
    '    Dim sqlConnection As New SqlConnection()
    '    Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
    '    Dim dt As New DataTable

    '    sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
    '    sqlConnection.Open()
    '    sqlCommand.CommandText = "SELECT ISNULL(DO, 0)DO FROM MainHierarchy1 WHERE CrewCode = '" + strCSCode + "' "
    '    Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

    '    sqlDataAdapter.Fill(dt)
    '    sqlConnection.Close()
    '    Return dt
    'End Function
    Public Function Get_BOQ_Mappings(strProjID As String, strLineNo As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "select m8.No, m8.NameE, m8.NameA, m8.ParentNo, m7.No, m7.NameE,m7.NameA, m7.ParentNo, m6.No, m6.NameE, m6.NameA, m6.ParentNo, m5.No, m5.NameE, m5.NameA, m5.ParentNo, m4.No, m4.NameE, m4.NameA, m4.ParentNo, m3.No, m3.NameE, m3.NameA, m3.ParentNo, m2.No, m2.NameE, m2.NameA, m2.ParentNo, m1.No, m1.NameE, m1.NameA, m1.ParentNo FROM MainHierarchy1 m1 left join mainhierarchy1 m2 on m1.ParentNo = m2.No left join mainhierarchy1 m3 on m2.ParentNo = m3.No left join mainhierarchy1 m4 on m3.ParentNo = m4.No left join mainhierarchy1 m5 on m4.ParentNo = m5.No left join mainhierarchy1 m6 on m5.ParentNo = m6.No left join mainhierarchy1 m7 on m6.ParentNo = m7.No left join mainhierarchy1 m8 on m7.ParentNo = m8.No where m1.No IN(SELECT LineItemCode FROM Project_BOQ_LineItem_Mapping WHERE ProjectID = '" + strProjID + "' AND BOQLineNo = '" + strLineNo + "' ) "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function Delete_BOQ_Mappings(strProjectID As String, strBOQLineNo As String, strLineCode As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "DELETE FROM Project_BOQ_LineItem_Mapping WHERE ProjectID = '" + strProjectID + "' AND BOQLineNo = '" + strBOQLineNo + "' AND LineItemCode = '" + strLineCode + "' "
        sqlCommand.CommandText = sqlCommand.CommandText + " DELETE FROM Project_BOQ_Data WHERE ProjectID = '" + strProjectID + "' AND BOQ_LineNo = '" + strBOQLineNo + "' AND ItemNo = '" + strLineCode + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetCompaniesAll(strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID, Name, UsersCount, ExpiryDate, Phone, Fax, POBox, ZipCode, WebSite, Status, Email FROM Company WHERE Name LIKE '%" + strSearch + "%' OR Phone LIKE '%" + strSearch + "%'"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function DeleteCompany(strID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlTrans As SqlTransaction = Nothing
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        Try
            sqlTrans = sqlConnection.BeginTransaction()
            sqlCommand.Transaction = sqlTrans

            sqlCommand.CommandText = "sp_DeleteCompanyData"
            sqlCommand.CommandType = CommandType.StoredProcedure
            sqlCommand.Parameters.AddWithValue("CompID", strID)
            sqlCommand.ExecuteNonQuery()

            sqlTrans.Commit()
            Return retVal
        Catch ex As Exception
            sqlTrans.Rollback()
        Finally
            sqlConnection.Close()
        End Try
    End Function
    Public Function GetCompaniesByID(strID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT c.ID, c.Name, c.UsersCount, c.ExpiryDate, c.Phone, c.Fax, c.POBox, c.ZipCode, c.WebSite, c.Status, c.CountryID, c.ClientTypeID, c.Email, c.IsTemp, u.userID, u.Password FROM Company c INNER JOIN users u on c.id = u.compid INNER JOIN User_Role_Mapping m on u.ID = m.UserID WHERE c.ID = '" + strID + "' AND (m.RoleID = 888 OR m.RoleID = 999)"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function UpdateCompany(strID As String, strCompName As String, strCountryID As String, strEmail As String, strExpiryDate As String, strClientType As String, strPhone As String, strFax As String, strPOBox As String, strzipCode As String, strWebsite As String, strStatus As String, strFileName As String, strTemp As String, strNewuserName As String, strPassword As String)
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "UPDATE Company SET Email = '" + strEmail + "', ExpiryDate = '" + strExpiryDate + "', Phone = '" + strPhone + "', Fax = '" + strFax + "', POBox = '" + strPOBox + "', ZipCode = '" + strzipCode + "', WebSite = '" + strWebsite + "', Status = '" + strStatus + "', CountryID = '" + strCountryID + "', ClientTypeID = '" + strClientType + "', FileName = '" + strFileName + "', IsTemp = '" + strTemp + "' WHERE ID = '" + strID + "'  UPDATE Users Set UserID = '" + strNewuserName + "', Password = '" + Encryption.Encrypt(strPassword) + "' WHERE ID = (SELECT TOP(1) u.ID FROM Users u INNER JOIN User_Role_Mapping m ON m.UserID = u.ID WHERE u.CompID = '" + strID + "' ) "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetCompanyLogo(strCompanyID As String) As String
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "SELECT FileName FROM Company WHERE ID = '" + strCompanyID + "' "
        Dim strFileName As String = sqlCommand.ExecuteScalar().ToString()

        sqlConnection.Close()
        Return strFileName
    End Function
    Public Function GetLaboursByCompanyID(strSearch As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Code, ProfE, ProfA, GenExp, AvgSalary, Housing, Food, MedInsurance, SecInsurance, Ticket, EOContract, Iqama, c.NameE, c.NameA, cast(HourlyRate as numeric(8,2))HourlyRate FROM Labour l LEFT JOIN Country c ON c.ID = l.Nationality WHERE CompID = '" + strCompID + "' AND (Code LIKE '%" + strSearch + "%' OR ProfE LIKE '%" + strSearch + "%' OR ProfA LIKE '%" + strSearch + "%' )"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetEquipmentByCodeAndCompany(strCode As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Code, DescE, DescA, Unit, OperationCost, EquipmentDepCost, EquipmentDailyCost, RentalCostD, RentalCostM FROM Equipment WHERE Code = '" + strCode + "' AND CompID = '" + strCompID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetEquipmentsByCompany(strSearch As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Code, DescE, DescA, Unit, OperationCost, EquipmentDepCost, EquipmentDailyCost, RentalCostD, RentalCostM, HourlyRate FROM Equipment WHERE CompID = '" + strCompID + "' AND (Code LIKE '%" + strSearch + "%' OR DescE LIKE '%" + strSearch + "%' OR DescA LIKE '%" + strSearch + "%') "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function UpdateCostLineItemByCompany(strLineNo As String, strCrewCode As String, strPercent As String, strCost As String, strMaterialCost As String, strUnit As String, strDO As String, strCompID As String) As Integer
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim retVal As Integer = 0
        Dim dt As New DataTable()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        'calculation
        Dim HO As Double = 0
        Dim MH As Double = 0
        Dim EH As Double = 0

        HO = Convert.ToDouble(strDO) / 8

        sqlCommand.CommandText = "SELECT ISNULL(SUM(ISNULL(Man_Day, 0)), 0)FROM ProductCrewStructure WHERE ProductID = '" + strCrewCode + "' AND Lab_Equip = 1 AND CompID = '" + strCompID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        If (dt.Rows.Count > 0) Then
            MH = Convert.ToDouble(dt.Rows(0).Item(0).ToString()) / HO
        End If

        dt.Clear()
        sqlCommand.CommandText = "SELECT ISNULL(SUM(ISNULL(Man_Day, 0)), 0) FROM ProductCrewStructure WHERE ProductID = '" + strCrewCode + "' AND Lab_Equip = 2 AND CompID = '" + strCompID + "'"
        sqlDataAdapter.SelectCommand = sqlCommand
        sqlDataAdapter.Fill(dt)

        If (dt.Rows.Count > 0) Then
            EH = Convert.ToDouble(dt.Rows(0).Item(0).ToString()) / HO
        End If

        HO = Math.Round(HO, 2)
        MH = Math.Round(MH, 2)
        EH = Math.Round(EH, 2)

        sqlCommand.CommandText = "UPDATE MainHierarchy1 SET CrewCode = '" + strCrewCode + "',  IsPercent = '" + strPercent + "', Cost = " + strCost + ", MaterialCost = '" + strMaterialCost + "', Unit = '" + strUnit + "', DO = '" + strDO + "', HO = '" + HO.ToString() + "', MH = '" + MH.ToString() + "', EH = '" + EH.ToString() + "' WHERE No = '" + strLineNo + "' AND CompID = '" + strCompID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function DeleteProductByIDAndCompany(strID As String, strCompID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "DELETE FROM ProductDetails WHERE ID = '" + strID + "' AND CompID = '" + strCompID + "' "
        sqlCommand.ExecuteNonQuery()
        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetProduct_Image_Data_ByCompany(strID As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ImageFile, DocFile FROM ProductDetails WHERE ID = '" + strID + "' AND CompID = '" + strCompID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetAssociatedSuppliersByCompany(strProdID As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT SupplierID, NameE, NameA, MinPrice, MaxPrice FROM AssociatedSuppliers WHERE ProductID = '" + strProdID + "' AND CompID = '" + strCompID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetAssociatedCrewStructuresByCompany(strProdID As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT c.Lab_Equip, c.Code, case c.Lab_Equip WHEN 1 THEN l.ProfE else e.DescE end As DescE, case c.Lab_Equip WHEN 1 THEN l.ProfA else e.DescA end As DescA, c.Man_Day FROM ProductCrewStructure c LEFT JOIN Labour l ON l.Code = c.Code LEFT JOIN Equipment e ON e.Code = c.Code WHERE c.ProductID = '" + strProdID + "' AND CompID = '" + strCompID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProductCrewStructure_LabourByCompany(strProductID As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT c.ID, c.Code, l.ProfE, l.ProfA, c.Man_Day, DailyOutput, HourlyOutput, ManHour, EquipHour, l.Nationality FROM ProductCrewStructure c LEFT JOIN Labour l ON l.Code = c.Code AND l.CompID = '" + strCompID + "' WHERE c.Lab_Equip = 1 AND c.ProductID = '" + strProductID + "' AND c.CompID = '" + strCompID + "' ORDER BY l.ProfE, l.ProfA"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProductCrewStructure_EquipmentByCompany(strProductID As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT c.ID, c.Code, e.DescE, e.DescA, c.Man_Day, DailyOutput, HourlyOutput, ManHour, EquipHour FROM ProductCrewStructure c LEFT JOIN Equipment e ON c.Code = e.Code AND e.CompID = '" + strCompID + "' WHERE c.Lab_Equip = 2 AND c.ProductID = '" + strProductID + "' AND c.CompID = '" + strCompID + "' ORDER BY e.DescE, e.DescA"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProductivityHistoryByCompany(ByVal strCode As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT TOP(5) ID, Code, DateTime, DO FROM ProductivityHistory WHERE Code = '" + strCode + "' AND CompID = '" + strCompID + "' ORDER BY DateTime DESC"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetLaboursByCountryAndCompany(strSearch As String, strCountryID As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Code, ProfE, ProfA FROM Labour l WHERE (ProfE LIKE '%" + strSearch + "%' OR ProfA LIKE '%" + strSearch + "%') AND l.Nationality = '" + strCountryID + "' AND l.CompID = '" + strCompID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetEquipmentsByCompany2(strSearch As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT Code, DescE, DescA, Unit, OperationCost, EquipmentDepCost, EquipmentDailyCost, RentalCostD, RentalCostM, HourlyRate FROM Equipment WHERE DescE LIKE '%" + strSearch + "%' OR DescA LIKE '%" + strSearch + "%' AND CompID = '" + strCompID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function DeleteHistoryByCompany(ByVal strID As String, strCompID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "DELETE FROM ProductivityHistory WHERE ID = '" + strID + "' AND CompID = '" + strCompID + "' "
        sqlCommand.ExecuteNonQuery()
        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetProductivityDocsByCompany(ByVal strCode As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ID, Code, Name, DateTime, Ext FROM ProductivityDocuments WHERE Code = '" + strCode + "' AND CompID = '" + strCompID + "' ORDER BY DateTime DESC"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i).Item(3) = DateTime.ParseExact(dt.Rows(i).Item(3), "yyyyMMddHHmmss", Nothing).ToString()
        Next
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetReferenceDataByCompany(ByVal strSearch As String, ByVal strCode As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT * FROM History WHERE LineNum = '" + strCode + "' AND CompID = '" + strCompID + "' AND ( Source LIKE '%" + strSearch + "%' OR Code LIKE '%" + strSearch + "%' OR DescE LIKE '%" + strSearch + "%' OR DescA LIKE '%" + strSearch + "%') "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProjectCostLineItemsFROMBOQByCompany(strProjectID As String, strSearch As String, strCompanyID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT MAX(li.ID)ID, li.LineItemCode LineItemNo, mh.NameE LineItemNameE, mh.NameA LineItemNameA, mh.CrewCode, li.Qty, CASE mh.IsPercent WHEN 0 THEN mh.Cost ELSE mh.Cost + '%' END Cost FROM Project_BOQ_LineItem_Mapping li LEFT JOIN Project_CrewStructure cs on cs.ProjectID = li.ProjectID INNER JOIN MainHierarchy1 mh on mh.[No] = li.LineItemCode WHERE li.ProjectID = '" + strProjectID + "' AND (li.LineItemCode LIKE '%" + strSearch + "%' OR mh.NameE LIKE '%" + strSearch + "%' OR mh.NameA LIKE '%" + strSearch + "%') AND mh.CompID = '" + strCompanyID + "' GROUP BY li.LineItemCode, mh.NameE, mh.NameA, mh.CrewCode, li.Qty, mh.IsPercent, mh.Cost"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function GetProjectCostLineItemsByCompany(strProjectID As String, strSearch As String, strCompID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT MAX(li.ID)ID, li.ItemNo LineItemNo, mh.NameE LineItemNameE, mh.NameA LineItemNameA, mh.CrewCode, li.Qty, CASE mh.IsPercent WHEN 0 THEN mh.Cost ELSE mh.Cost + '%' END Cost FROM Project_CostLineItems li LEFT JOIN Project_CrewStructure cs on cs.ProjectID = li.ProjectID INNER JOIN MainHierarchy1 mh on mh.[No] = li.ItemNo WHERE li.ProjectID = '" + strProjectID + "' AND (li.ItemNo LIKE '%" + strSearch + "%' OR mh.NameE LIKE '%" + strSearch + "%' OR mh.NameA LIKE '%" + strSearch + "%') AND mh.CompID = '" + strCompID + "' GROUP BY li.ItemNo, mh.NameE, mh.NameA, mh.CrewCode, li.Qty, mh.IsPercent, mh.Cost"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function CreateSupplierUser(UserID As String, Password As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

         Dim psw As String = Encryption.Encrypt(Password)
        sqlCommand.CommandText = "INSERT INTO Users(UserID, Password) VALUES ('" + UserID + "', '" + psw + "') SELECT ID FROM Users WHERE ID = (SELECT MAX(ID) FROM Users) "
        retVal = sqlCommand.ExecuteScalar()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetSupplierByUserID(strUserID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT s.ID, NameE, NameA, Address, POBox, City, ZipCode, Country, Phone, Fax, s.EmailID, ApprovedBy, Other, RefNo, s.UserID, s.Pass FROM Suppliers s WHERE s.UserID = '" + strUserID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function DeleteSupplierUser(strSupplierID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "DELETE FROM User_Role_Mapping WHERE UserID = (SELECT ID FROM Users WHERE UserID = '" + strSupplierID + "') DELETE FROM Users WHERE userID =  '" + strSupplierID + "' "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function UpdateCostLineItemByCompany_BySupplier(strLineNo As String, strCrewCode As String, strPercent As String, strCost As String, strMaterialCost As String, strUnit As String, strDO As String, strCompID As String, strSuppID As String, strFileName As String) As Integer
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim retVal As Integer = 0
        Dim dt As New DataTable()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        'calculation
        Dim HO As Double = 0
        Dim MH As Double = 0
        Dim EH As Double = 0

        HO = Convert.ToDouble(strDO) / 8

        sqlCommand.CommandText = "SELECT ISNULL(SUM(ISNULL(Man_Day, 0)), 0)FROM ProductCrewStructure WHERE ProductID = '" + strCrewCode + "' AND Lab_Equip = 1 AND CompID = '" + strCompID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        If (dt.Rows.Count > 0) Then
            MH = Convert.ToDouble(dt.Rows(0).Item(0).ToString()) / HO
        End If

        dt.Clear()
        sqlCommand.CommandText = "SELECT ISNULL(SUM(ISNULL(Man_Day, 0)), 0) FROM ProductCrewStructure WHERE ProductID = '" + strCrewCode + "' AND Lab_Equip = 2 AND CompID = '" + strCompID + "'"
        sqlDataAdapter.SelectCommand = sqlCommand
        sqlDataAdapter.Fill(dt)

        If (dt.Rows.Count > 0) Then
            EH = Convert.ToDouble(dt.Rows(0).Item(0).ToString()) / HO
        End If

        HO = Math.Round(HO, 2)
        MH = Math.Round(MH, 2)
        EH = Math.Round(EH, 2)

        sqlCommand.CommandText = "DELETE FROM Supplier_Quot WHERE CostLineItemNo = '" + strLineNo + "' AND SuppID = (SELECT ID FROM Suppliers WHERE UserID = '" + strSuppID + "' )   INSERT INTO Supplier_Quot (CostLineItemNo, CrewCode, IsPercent, Cost, MaterialCost, Unit, DO, HO, MH, EH, SuppID, FileName) VALUES('" + strLineNo + "', '" + strCrewCode + "', '" + strPercent + "', '" + strCost + "', '" + strMaterialCost + "', '" + strUnit + "', '" + strDO + "', '" + HO.ToString() + "', '" + MH.ToString() + "', '" + EH.ToString() + "', (SELECT ID FROM Suppliers WHERE UserID = '" + strSuppID + "' ), '" + strFileName + "' )"
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function Get_Supplier_Quotation(strCostLineNo As String, strSuppID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT * FROM Supplier_Quot WHERE CostLineItemNo = '" + strCostLineNo + "' AND SuppID = (SELECT ID FROM Suppliers WHERE UserID = '" + strSuppID + "' )"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function Get_Supplier_Quotes(strSuppID As String, strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "SELECT s.*, NameE, NameA FROM Supplier_Quot s LEFT JOIN MainHierarchy1 ON No = CostLineItemNo AND CompID = 0 WHERE SuppID = (SELECT ID FROM Suppliers WHERE UserID = '" + strSuppID + "' ) AND (CostLineItemNo LIKE '%" + strSearch + "%' OR s.CrewCode LIKE '%" + strSearch + "%' OR s.Unit LIKE '%" + strSearch + "%') "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function Delete_Supplier_Quotation(strID As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "DELETE FROM Supplier_Quot WHERE ID = '" + strID + "' "
        sqlCommand.ExecuteNonQuery()
        sqlConnection.Close()
        Return retVal
    End Function
    Public Function Get_Supplier_Quotation_ByID(strID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT * FROM Supplier_Quot WHERE ID = '" + strID + "'"
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()
        Return dt
    End Function
    Public Function Get_AllSuppliers() As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA FROM Suppliers "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function Get_Supplier_Quotes_ByRange(strFromSuppID As String, strToSuppID As String, strSearch As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "SELECT s.*, NameE, NameA FROM Supplier_Quot s LEFT JOIN MainHierarchy1 ON CostLineItemNo = No And CompID = 0 WHERE SuppID BETWEEN '" + strFromSuppID + "' AND '" + strToSuppID + "' AND (CostLineItemNo LIKE '%" + strSearch + "%' OR s.CrewCode LIKE '%" + strSearch + "%' OR s.Unit LIKE '%" + strSearch + "%') "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dt)

        sqlConnection.Close()
        Return dt
    End Function
    Public Function SaveProjectData_1(strProjectID As String, strCostLineNo As String, strCompID As String, strSuppQuotID As String, strDataType As String) As Integer
        Dim retVal As Integer = 0
        Dim sqlConnection As New SqlConnection()
        Dim dt As New DataTable()
        Dim dtCSExisting As New DataTable()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        If (strDataType = "0") Then
            sqlCommand.CommandText = "SELECT ProductID,Man_Day,DO,HO,MH,EH,Lab_Equip,Code FROM ProductCrewStructure INNER JOIN MainHierarchy1 ON MainHierarchy1.CrewCode = ProductID AND MainHierarchy1.CompID = '" + strCompID + "'  WHERE No = '" + strCostLineNo + "' AND ProductCrewStructure.CompID = '" + strCompID + "'"
        ElseIf (strDataType = "1") Then
            sqlCommand.CommandText = "SELECT ProductID,Man_Day,DO,HO,MH,EH,Lab_Equip,Code FROM ProductCrewStructure INNER JOIN Supplier_Quot ON Supplier_Quot.CrewCode = ProductID AND Supplier_Quot.SuppID = '" + strSuppQuotID + "'  WHERE Supplier_Quot.CostLineItemNo = '" + strCostLineNo + "' AND ProductCrewStructure.CompID = 0"
        End If

        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
        sqlDataAdapter.Fill(dtCSExisting)

        For i As Integer = 0 To dtCSExisting.Rows.Count - 1
            sqlCommand.CommandText = "SELECT COUNT(*) FROM Project_CrewStructure WHERE ProjectID = '" + strProjectID + "' AND CrewCode = '" + dtCSExisting.Rows(i).Item(7).ToString() + "' AND Lab_Equip = '" + dtCSExisting.Rows(i).Item(6).ToString() + "' "
            If (sqlCommand.ExecuteScalar().ToString().Equals("0")) Then
                sqlCommand.CommandText = "INSERT INTO Project_CrewStructure(ProjectID,Code,Man_Day,DailyOutput,HourlyOutput,ManHour,EquipHour,Lab_Equip,CrewCode) SELECT '" + strProjectID + "',  '" + dtCSExisting.Rows(i).Item(0).ToString() + "', '" + dtCSExisting.Rows(i).Item(1).ToString() + "', '" + dtCSExisting.Rows(i).Item(2).ToString() + "', '" + dtCSExisting.Rows(i).Item(3).ToString() + "', '" + dtCSExisting.Rows(i).Item(4).ToString() + "', '" + dtCSExisting.Rows(i).Item(5).ToString() + "', '" + dtCSExisting.Rows(i).Item(6).ToString() + "', '" + dtCSExisting.Rows(i).Item(7).ToString() + "' "
                sqlCommand.ExecuteNonQuery()
            End If
        Next

        'sqlCommand.CommandText = "SELECT COUNT(*) FROM Project_CostLineItems WHERE ProjectID = '" + strProjectID + "' AND ItemNo = '" + strCostLineNo + "' "
        'If (sqlCommand.ExecuteScalar().ToString().Equals("0")) Then
        '    sqlCommand.CommandText = "INSERT INTO Project_CostLineItems(ProjectID, ItemNo, Supp_Quot_ID) VALUES('" + strProjectID + "', '" + strCostLineNo + "' , '" + strSuppQuotID + "') "
        '    sqlCommand.ExecuteNonQuery()
        'End If

        sqlCommand.CommandText = "INSERT INTO Project_CostLineItems(ProjectID, ItemNo, Supp_Quot_ID, DataType) VALUES('" + strProjectID + "', '" + strCostLineNo + "' , '" + strSuppQuotID + "', '" + strDataType + "') "
        sqlCommand.ExecuteNonQuery()

        sqlConnection.Close()
        Return retVal
    End Function
    Public Function GetProjectCostLineItems_1(strProjectID As String) As DataTable
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim dt As New DataTable
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        sqlCommand.CommandText = "SELECT ItemNo, Supp_Quot_ID, DataType FROM Project_CostLineItems WHERE ProjectID = '" + strProjectID + "' "
        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        sqlDataAdapter.Fill(dt)
        sqlConnection.Close()

        Return dt
    End Function
End Class

