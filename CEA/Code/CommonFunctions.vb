﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Globalization
Imports System.Web.UI.WebControls

Public Class CommonFunctions
    Public Shared Function ValidateInput(strInput As String) As Boolean
        Dim sInvChar() As Char = {"=", "<", ">", "%", "&", ";", "?"}
        If (strInput.IndexOfAny(sInvChar) > -1) Then
            Return False
        Else
            Return True
        End If
    End Function
End Class
