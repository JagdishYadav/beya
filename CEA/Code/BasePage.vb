﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.IO

Namespace FMS.Culture
    Public Class BasePage
        Inherits System.Web.UI.Page
        Protected Overrides Sub InitializeCulture()
            Dim cultureCookie As HttpCookie = Request.Cookies("Culture")
            Dim cultureCode As String
            If cultureCookie IsNot Nothing Then
                cultureCode = cultureCookie.Value
            Else
                cultureCode = Nothing
            End If
            If (String.IsNullOrEmpty(cultureCode) = False) Then

                Page.UICulture = cultureCode
                Page.Culture = cultureCode

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US")
                Thread.CurrentThread.CurrentUICulture = New CultureInfo(cultureCode)
            End If
        End Sub
    End Class
End Namespace




