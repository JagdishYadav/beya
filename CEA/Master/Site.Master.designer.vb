﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Site

    '''<summary>
    '''Head1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Head1 As Global.System.Web.UI.HtmlControls.HtmlHead

    '''<summary>
    '''MenuStyleSheet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents MenuStyleSheet As Global.System.Web.UI.HtmlControls.HtmlLink

    '''<summary>
    '''HeadContent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HeadContent As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''MasterPageBodyTag control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents MasterPageBodyTag As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Button2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Button2 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''compLogo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents compLogo As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''Welcome control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Welcome As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ulGlobalDropdown control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ulGlobalDropdown As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''loginAs control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents loginAs As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Logout control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Logout As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''LinkButton1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LinkButton1 As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lblUserRole control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblUserRole As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Menu1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Menu1 As Global.System.Web.UI.WebControls.Menu

    '''<summary>
    '''MiddlePart control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents MiddlePart As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''PageTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PageTitle As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''MainContent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents MainContent As Global.System.Web.UI.WebControls.ContentPlaceHolder

    '''<summary>
    '''hfMenuLanguage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfMenuLanguage As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfHideMenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfHideMenu As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Button1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Button1 As Global.System.Web.UI.WebControls.Button
End Class
