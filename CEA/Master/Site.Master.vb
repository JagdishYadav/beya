﻿Imports System.Threading
Imports System.Globalization
Public Class Site
    Inherits System.Web.UI.MasterPage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        MenuStyleSheet.Attributes.Add("href", "../css/Style.css")
        If (IsPostBack = False) Then
            Button2.Attributes.Add("style", "display:none")
            Button1.Attributes.Add("style", "display:none")
            Dim dt As New DataTable()
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim user As System.Security.Principal.IPrincipal = HttpContext.Current.User

            If (Session("CompanyLogo") Is Nothing) Then
                Dim objSqlConfig As New ConfigClassSqlServer()
                Dim strFileName As String = objSqlConfig.GetCompanyLogo(Session("CompanyID"))
                compLogo.ImageUrl = "~/Upload/CompanyLogo/" + strFileName
                Session("CompanyLogo") = strFileName
            Else
                compLogo.ImageUrl = "~/Upload/CompanyLogo/" + Session("CompanyLogo").ToString()
            End If


            If (Page.UICulture = "Arabic") Then
                Menu1.Style.Add("text-align", "right")
                Menu1.Style.Add("direction", "rtl")
                Menu1.Style.Add("float", "right")

            Else
                'ContentTable.Style.Add("text-align", "left")
                'ContentTable.Style.Add("direction", "ltr")
            End If
            If (Session("UserName") IsNot Nothing) Then
                Welcome.Text = Session("UserName").ToString()
            End If

            If (Session("UserName") IsNot Nothing) Then
                Dim configClass As New ConfigClassSqlServer()
                dt = configClass.GetUserModules(Session("UserName").ToString())
                If (dt.Rows.Count > 0) Then
                    loginAs.Text = "You are login as " + dt.Rows(0).Item(0).ToString()
                ElseIf (Session("User_Role_ID") IsNot Nothing And Session("User_Role_ID") = "99") Then
                    loginAs.Text = "You are login as Supplier"
                ElseIf (Session("User_Role_ID") IsNot Nothing And Session("User_Role_ID") = "555") Then
                    loginAs.Text = "You are login as Data Controller"
                ElseIf (Session("User_Role_ID") IsNot Nothing And Session("User_Role_ID") = "777") Then
                    loginAs.Text = "You are login as Specification Writer"
                ElseIf (Session("User_Role_ID") IsNot Nothing And Session("User_Role_ID") = "666") Then
                    loginAs.Text = "You are login as Productivity Estimation Engineer"
                End If

            End If
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If (Page.UICulture = "Arabic") Then
            Logout.Text = "تسجيل الخروج"
            LinkButton1.Text = "English"
            hfMenuLanguage.Value = "Arabic"
            MenuStyleSheet.Attributes.Add("href", "../css/ArMenu.css")
        Else
            LinkButton1.Text = "العربية"
            Logout.Text = "Logout"
            hfMenuLanguage.Value = "English"
        End If
    End Sub
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        Dim senderLink As New LinkButton
        If (Page.UICulture = "English" Or Page.UICulture = "English (United States)" Or Page.UICulture = "English (United Kingdom)") Then
            senderLink.CommandArgument = "ar"
        Else
            senderLink.CommandArgument = "en-US"
        End If

        'Save Current Culture in Cookie- will be used in InitializeCulture in BasePage
        Response.Cookies.Add(New HttpCookie("Culture", senderLink.CommandArgument))
        Response.Redirect(Request.Url.AbsolutePath)
    End Sub
    Protected Sub Logout_Click(sender As Object, e As EventArgs) Handles Logout.Click
        Dim strURL As String = ""

        If (Session("OriginalCompanyID") IsNot Nothing) Then
            strURL = "../Login.aspx?CompID=" + Session("OriginalCompanyID").ToString()
        ElseIf (Session("CompanyID") IsNot Nothing) Then
            strURL = "../Login.aspx?CompID=" + Session("CompanyID").ToString()
        End If

        If (Session("IS_SUPPLIER") IsNot Nothing And Session("IS_SUPPLIER") = "YES") Then
            strURL = "../Supplier"
        End If
        Session.Clear()
        Session.RemoveAll()
        Session.Abandon()
        Response.Redirect(strURL)
    End Sub
    Protected Sub Menu1_MenuItemClick(sender As Object, e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu1.MenuItemClick
        If (e.Item.Value = "3" And Request.RawUrl.ToString() <> "/Admin/LevelEntry.aspx") Then
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "Showopoup();", True)
        Else
            Response.Redirect("~/Admin/LevelEntry.aspx")
        End If
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Response.Redirect("~/Admin/LevelEntry.aspx")
    End Sub
End Class