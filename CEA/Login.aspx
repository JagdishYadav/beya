﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="CEA.Login1" %>

<!doctype html>
<html class="no-js" lang="en-US">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!--[if gte IE 9]>
  <style type="text/css">
    .loginform {
       filter: none;
    }
    
  </style>
<![endif]-->
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="CSS/Style.css" rel="stylesheet" type="text/css" />
    <script src="js/vendor/modernizr.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <%--<div class="large-12">
            <header>
                <div class="row">

                    <div class="row">


                        <img src="Images/SystemLogo.png" style="float: left" /><h2 style="text-align: right;">Cost Estimation System</h2>
                    </div>





                </div>
            </header>
        </div>--%>

        <section class="loginPageWrapper justify-content-center align-items-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-7 col-lg-5">
                        <div class="_header text-center">
                            <div class="Loginlogo"></div>
                            <h1>Welcome to <br /> Cost Estimation System</h1>
                        </div>
                        <div class="formWrap card">
                            <div class="card-body">
                                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="text-center mb-4">
                                            <h5>Sign In</h5>
                                            <p class="smallText">Login to access your account.</p>
                                        </div>
                                        <div class="text-center errorBlock">
                                            <asp:Label ID="lblErrorMessage" runat="server" Font-Size="Small" ForeColor="Red"></asp:Label>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label Text="Username" runat="server" />
                                            <asp:TextBox ID="Username" runat="server" placeholder="Username" Width="100%"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label Text="Password" runat="server" />
                                            <asp:TextBox ID="Password" runat="server" placeholder="Password" TextMode="Password" Width="100%"></asp:TextBox>
                                        </div>
                                        <div class="form-group mb-0">
                                            <asp:Button ID="Button1" runat="server" Text="Signin" CssClass="btn btn-primary btn-block" />
                                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="right" Style="margin-top: 15px;">Sign Up</asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="text-center mt-4 _footer">
                            <p>Version 1.0 © 2014. All Rights Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </form>
</body>
</html>
