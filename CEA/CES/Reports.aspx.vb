﻿
Public Class Reports
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                'Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                'Menuhide.Value = "close"
                Dim objSqlConfig As New ConfigClassSqlServer()
                ddlReports.DataSource = objSqlConfig.GetReportsByRole(Session("User_Role_ID"))
                ddlReports.DataValueField = "ID"
                If (UICulture = "Arabic") Then
                    div1.Style.Add("direction", "rtl")
                    div1.Style.Add("text-align", "right")
                    ddlReports.DataTextField = "NameA"
                Else
                    ddlReports.DataTextField = "NameE"
                End If
                ddlReports.DataBind()
                ddlReports.SelectedIndex = 0

                If (Session("User_Role_ID") = "999") Then
                    Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                    Dim menuItems As MenuItemCollection = Menu1.Items
                    If (Session("User_Role_ID").ToString().Contains("999")) Then
                        If (menuItems.Count > 15) Then
                            menuItems.RemoveAt(12)
                            menuItems.RemoveAt(12)
                            menuItems.RemoveAt(12)
                            menuItems.RemoveAt(12)
                        End If
                    ElseIf (Session("User_Role_ID").ToString().Contains("888")) Then
                        If (menuItems.Count > 5) Then
                            menuItems.RemoveAt(0)
                            menuItems.RemoveAt(1)
                            menuItems.RemoveAt(2)
                            menuItems.RemoveAt(2)
                            menuItems.RemoveAt(5)
                            menuItems.RemoveAt(5)
                            menuItems.RemoveAt(5)
                            menuItems.RemoveAt(5)
                            menuItems.RemoveAt(5)
                            menuItems.RemoveAt(5)
                            menuItems.RemoveAt(5)
                        End If
                    End If
                Else
                    Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                    Menuhide.Value = "close"
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If (ddlReports.SelectedIndex = 0) Then
            Session("ReportID") = "1"
        ElseIf (ddlReports.SelectedIndex = 1) Then
            Session("ReportID") = "2"
        ElseIf (ddlReports.SelectedIndex = 2) Then
            Session("ReportID") = "3"
        ElseIf (ddlReports.SelectedIndex = 3) Then
            Session("ReportID") = "4"
        ElseIf (ddlReports.SelectedIndex = 4) Then
            Session("ReportID") = "5"
        ElseIf (ddlReports.SelectedIndex = 5) Then
            Session("ReportID") = "6"
        ElseIf (ddlReports.SelectedIndex = 6) Then
            Session("ReportID") = "7"
        ElseIf (ddlReports.SelectedIndex = 7) Then
            Session("ReportID") = "8"
        ElseIf (ddlReports.SelectedIndex = 8) Then
            Session("ReportID") = "9"
        ElseIf (ddlReports.SelectedIndex = 9) Then
            Session("ReportID") = "10"
        ElseIf (ddlReports.SelectedIndex = 10) Then
            Session("ReportID") = "11"
        ElseIf (ddlReports.SelectedIndex = 11) Then
            Session("ReportID") = "12"
        ElseIf (ddlReports.SelectedIndex = 12) Then
            Session("ReportID") = "13"
        ElseIf (ddlReports.SelectedIndex = 13) Then
            Session("ReportID") = "14"
        ElseIf (ddlReports.SelectedIndex = 14) Then
            Session("ReportID") = "15"
        End If
        Response.Redirect("ReportViewer.aspx?ID=1")
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'Response.Redirect("Selection_List.aspx")
        Response.Redirect("ProjectCostLineItems.aspx")
    End Sub
End Class