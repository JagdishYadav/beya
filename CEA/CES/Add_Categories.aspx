﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Add_Categories.aspx.vb" Inherits="CEA.Add_Level1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

    <script type="text/javascript">
        //$(function () {
        //    $('#sidr').hide();
        //});

        function RadioCheck(rb) {

            var gv = document.getElementById("<%=grdData.ClientID%>");
            var rbs = gv.getElementsByTagName("input");
            var nodeNo;

            for (var i = 0; i < rbs.length; i++) {

                if (rbs[i].type == "radio") {

                    if (rbs[i].checked && rbs[i] != rb) {

                        rbs[i].checked = false;
                        break;
                    }
                }
            }

            var treeViewData = window["<%=tvLevels.ClientID%>" + "_Data"];
            if (treeViewData.selectedNodeID.value != "") {
                var selectedNode = document.getElementById(treeViewData.selectedNodeID.value);
                var text = selectedNode.innerHTML;
                nodeNo = text.split(" ")[0];
            }

            var suppID;
            var DataType = "1";
            var gridIndex = 1;

            if (gv.rows.length <= 2) {
                DataType = "0";
            }

            for (var j = 0; j < rbs.length; j++) {

                if (rbs[j].type == "hidden" && rbs[j - 1].type == "radio" && rbs[j - 1].checked) {
                    suppID = rbs[j].value;

                    gridIndex = j - gridIndex;
                    if (gv.rows[gridIndex].cells[7].innerHTML == "System Default" || gv.rows[gridIndex].cells[7].innerHTML == "نظام افتراضي") {
                        DataType = "0";
                    }
                }

                else if (j > 2)
                    gridIndex = j - gridIndex;
            }

            var hiddenField = document.getElementById("<%= hdnSuppIDs.ClientID%>").value;
            var varArray = hiddenField.split(",");
            var newValue = "";

            for (var i = 0; i < varArray.length; i++) {
                if (varArray[i].length > 0) {
                    var innerArray = varArray[i].split(":")
                    if (innerArray[0] == nodeNo) {
                        innerArray[1] = suppID;
                        innerArray[2] = DataType;
                    }
                    newValue = newValue + innerArray[0] + ":" + innerArray[1] + ":" + innerArray[2] + ","
                }
            }

            if (newValue.length == 0) {
                newValue = newValue + nodeNo + ":" + suppID + ":" + DataType + ","
            }

            document.getElementById("<%= hdnSuppIDs.ClientID%>").value = newValue;
        }

        var _isInitialLoad = true;
        function pageLoad(sender, args) {
            if (_isInitialLoad) {
                _isInitialLoad = false;
                document.getElementById("<%= Button8.ClientID %>").click();
            }
        }
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProjSaved") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SelectCategory") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SaveFirst") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorSelectUser") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "PleaseSelectOption") %>';
            }

            alert(msgstring);
        }
        function DisableNextButton() {
            document.getElementById('<%= Button2.ClientID %>').disabled = true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <div class="row">
        <div class="large-4 medium-4 column">
            <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, CostLineItems %>'></asp:Label>
        </div>
        <div class="large-8 medium-8 column">
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <asp:Label ID="Label3" runat="server" Text="" SkinID="RightLabel"></asp:Label>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <script type="text/javascript">
        var xPos, yPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xPos = $get('divTreeViewScrollTo').scrollLeft;
            yPos = $get('divTreeViewScrollTo').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('divTreeViewScrollTo').scrollLeft = xPos;
            $get('divTreeViewScrollTo').scrollTop = yPos;
        }
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel2" runat="server" CssClass="">
                <div class="row justify-content-between">
                    <div class="col-12 col-md-auto">
                        <asp:Label ID="lblProjectName" runat="server" Text="ProjectName" SkinID="Controllabel" CssClass="control-label mb-4 mt-2 d-block"></asp:Label>
                    </div>
                    <div class="col-12 col-md-auto">
                        <div class="text-right mb-3 buttonActionGroup">
                            <div class="btn-group commanButtonGroup mb-3">
                                <asp:Button ID="Button9" runat="server" Text='<%$ Resources:Resource, Previous %>' CssClass="btn btn-primary btn-min-120" />
                                <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="btn btn-primary btn-min-120" Visible="false" />
                                <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Next %>' CssClass="btn btn-primary btn-min-120" />
                            </div>
                        </div>
                    </div>
                </div>
                <%-- <div class="row">
                <div class="large-6 medium-6 column">
                    <asp:Label ID="Label1" runat="server" Text="Select level 1"></asp:Label>
                </div>
            </div>--%>
                <div class="row">
                    <div id="divTreeViewScrollTo" class="col-12 col-lg-4" style="overflow: auto; height: 400px;">
                        <%--<asp:Panel ID="Panel1" runat="server" CssClass="panel" style="overflow:auto;height:400px;">--%>
                        <%-- <div class="large-7 medium-7 columns" id="div1" runat="server">
                <div style="width:562px; min-height:314px">--%>

                        <asp:TreeView ID="tvLevels" runat="server" Width="100%" LineImagesFolder="~/TreeLineImages"
                            Visible="false" ShowLines="True" ShowCheckBoxes="None" NodeWrap="true">
                        </asp:TreeView>

                        <%-- </div>
                </div>--%>
                        <%--</asp:Panel>--%>
                    </div>

                    <div class="col-12 col-lg-8">
                        <asp:Panel ID="Panel3" runat="server" CssClass="card" Style="overflow: auto; height: 400px;">
                            <div class="card-body p-0">
                                <div class="table-responsive tableLayoutWrap altboarder">
                                    <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                        AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="250" DataKeyNames="ID" SelectedRowStyle-BackColor="Yellow">
                                        <Columns>
                                            <asp:BoundField DataField="No" SortExpression="No" HeaderText='<%$ Resources:Resource, LineNumber %>'
                                                ItemStyle-Width="150px" />
                                            <asp:BoundField DataField="NameE" SortExpression="NameE" HeaderText='<%$ Resources:Resource, Desc %>'
                                                ItemStyle-Width="300px" />
                                            <asp:BoundField DataField="NameA" SortExpression="NameA" HeaderText='<%$ Resources:Resource, Desc %>'
                                                ItemStyle-Width="300px" />
                                            <asp:BoundField DataField="Unit" SortExpression="Unit" HeaderText='<%$ Resources:Resource, Unit %>'
                                                ItemStyle-Width="100px" />
                                            <asp:BoundField DataField="CrewCode" SortExpression="CrewCode" HeaderText='<%$ Resources:Resource, CrewCode %>'
                                                ItemStyle-Width="100px" />
                                            <asp:BoundField DataField="DO" SortExpression="DO" HeaderText='<%$ Resources:Resource, DO %>'
                                                ItemStyle-Width="100px" />
                                            <asp:BoundField DataField="Cost" SortExpression="Cost" HeaderText='<%$ Resources:Resource, Cost %>' ItemStyle-Width="100px" />
                                            <asp:BoundField DataField="MaterialCost" SortExpression="MaterialCost" HeaderText='<%$ Resources:Resource, MaterialCost %>' ItemStyle-Width="100px" />
                                            <asp:BoundField DataField="SuppNameE" SortExpression="SuppNameE" HeaderText='<%$ Resources:Resource, Supplier %>' ItemStyle-Width="100px" />
                                            <asp:BoundField DataField="SuppNameA" SortExpression="SuppNameA" HeaderText='<%$ Resources:Resource, Supplier %>' ItemStyle-Width="100px" />
                                            <asp:TemplateField Visible="false">
                                                <ItemTemplate>
                                                    <asp:RadioButton ID="RadioButton1" runat="server" onclick="RadioCheck(this);" />
                                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Eval("ID")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Button ID="Button8" runat="server" Text="" />
                    </div>
                </div>
            </asp:Panel>
            <asp:HiddenField ID="hdnSuppIDs" runat="server" Value="" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <script>
        $(document).ready(function () {
            $(".bodyWrapper .bodyContainer").addClass('p-0');
            $("#headerWrapper .toggleNavButton").hide();
        });

        $(document).ready(function () { $("#sidr").show(); });
    </script>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MiddlePart">
    <%--<div class="row">
        <div class="large-12 medium-12 column">
            <div class="outerDivBtn">
                <div class="BTNdiv">
                    <asp:Button ID="Button9" runat="server" Text='<%$ Resources:Resource, Previous %>' CssClass="backBTN" />
                    <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave" Visible="false" />
                    <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Next %>' CssClass="BTNnext" />
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>


