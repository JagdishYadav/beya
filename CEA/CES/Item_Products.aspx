﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Item_Products.aspx.vb" Inherits="CEA.Cat_Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProjSaved") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SelectProduct") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SaveFirst") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorNumeric") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorQty") %>';
            }

            alert(msgstring);
        }
        function DisableNextButton() {
            document.getElementById('<%= Button3.ClientID %>').disabled = true; 
        }

        $(document).ready(function () {
            $(".bodyWrapper .bodyContainer").addClass('p-0');
            $("#headerWrapper .toggleNavButton").hide();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, ProductAttachedWith %>'></asp:Label>
    <asp:Label ID="lblSelectedItem" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel2" runat="server" CssClass="panel">
                <div class="row">
                    <asp:Label ID="lblProjectName" runat="server" CssClass="ProjectName"></asp:Label>
                </div>
                <br />
                <div class="row">
                    <asp:Panel ID="Panel1" runat="server" CssClass="panel">
                    <div class="row">
                        <div class="large-6 medium-6 column">
                            <asp:Label ID="Label15" runat="server" Text='<%$ Resources:Resource, Search %>' Width="100%"></asp:Label>
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="left" Width="70%">
                            </asp:TextBox><asp:Button ID="Button5" runat="server" Text="Search" CssClass="BTNSearch" />
                        </div>
                        </div>
                        <div style="width: 100%; height: 500px; overflow: scroll">
                            <asp:GridView ID="grdProducts" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                EmptyDataText="No Data" Width="100%" PageSize="25" DataKeyNames="ID" HeaderStyle-HorizontalAlign ="Center">
                                <Columns>
                                    <asp:BoundField DataField="MapID" HeaderText="MapID" ItemStyle-Width="0px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="NameE" SortExpression="NameE" HeaderText='<%$ Resources:Resource, NameE %>'
                                        ItemStyle-Width="25%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="NameA" SortExpression="NameA" HeaderText='<%$ Resources:Resource, NameA %>'
                                        ItemStyle-Width="25%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="PropE" SortExpression="PropE" HeaderText='<%$ Resources:Resource, FeatE %>'
                                        ItemStyle-Width="25%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="PropA" SortExpression="PropA" HeaderText='<%$ Resources:Resource, FeatA %>'
                                        ItemStyle-Width="25%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="DescE" SortExpression="DescE" HeaderText='<%$ Resources:Resource, DescriptionE %>'
                                        ItemStyle-Width="25%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="DescA" SortExpression="DescA" HeaderText='<%$ Resources:Resource, DescriptionA %>'
                                        ItemStyle-Width="25%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="Unit" SortExpression="Unit" HeaderText='<%$ Resources:Resource, Unit %>'
                                        ItemStyle-Width="5%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="ManHour" SortExpression="ManHour" HeaderText='<%$ Resources:Resource, ManHour %>'
                                        ItemStyle-Width="5%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="EquipHour" SortExpression="EquipHour" HeaderText='<%$ Resources:Resource, EquipHour %>'
                                        ItemStyle-Width="5%" ItemStyle-Wrap="true" />
                                    <asp:TemplateField HeaderText='<%$ Resources:Resource, Select %>' ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" OnCheckedChanged="CheckBox1_CheckedChanged" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText='<%$ Resources:Resource, Qty %>' ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" MaxLength="6" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                    <div class="row">
                        <div class="large-12 medium-12 column">
                    <div class="outerDivBtn">
                        <div class="BTNdiv">
                            <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Previous %>'
                                CssClass="backBTN"  />
                            <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave"
                                />
                            <asp:Button ID="Button3" runat="server" Text='<%$ Resources:Resource, Next %>' CssClass="BTNnext"
                                />
                        </div></div></div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
