﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Reports.aspx.vb" Inherits="CEA.Reports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, Reports %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="div1" runat="server" class="row">
                <div class="col-12">
                    <div class="row justify-content-between">
                        <div class="col-12 col-md-5 col-lg-7 order-md-12">
                            <div class="text-right mb-3 buttonActionGroup">
                                <div class="btn-group commanButtonGroup mb-3 mt-3">
                                    <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Previous %>' CssClass="btn btn-primary btn-min-120" />
                                    <asp:Button ID="Button5" runat="server" Text='<%$ Resources:Resource, Show %>' CssClass="btn btn-primary btn-min-120" />
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-7 col-lg-5 order-md-1">
                            <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, SelectReport %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="ddlReports"></asp:Label>
                            <asp:DropDownList ID="ddlReports" runat="server" SkinID="FormControl" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <script>
        $(document).ready(function () {
            $(".bodyWrapper .bodyContainer").addClass('p-0');
            $("#headerWrapper .toggleNavButton").hide();
        });

        $(document).ready(function () { $("#sidr").show(); });
    </script>
</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MiddlePart">
    <%--<div class="row">
        <div class="large-12 medium-12 column">
            <div class="outerDivBtn">
                <div class="BTNdiv">
                    <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Previous %>' CssClass="backBTN rightbtn" />
                    <asp:Button ID="Button5" runat="server" Text='<%$ Resources:Resource, Show %>' CssClass="BTNView" />
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>
