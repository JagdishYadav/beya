﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Product_Suppliers.aspx.vb" Inherits="CEA.Product_Suppliers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
     <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
     <script type="text/javascript">

            $(function () {
                $('#sidr').hide();
            });

         $(document).ready(function () {
             $(".bodyWrapper .bodyContainer").addClass('p-0');
             $("#headerWrapper .toggleNavButton").hide();
         });
     </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="large-12 medium-12 columns">
                    <h5>
                        <asp:Label ID="lblManpower" runat="server" Text='<%$ Resources:Resource, AssociatedSupplierList %>'></asp:Label></h5>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <asp:Panel ID="Panel1" runat="server" CssClass="panel">
                        <%--<div class="row">
                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="backBTN" Text='<%$ Resources:Resource, Back %>'></asp:LinkButton>
                            <div class="large-10 medium-10 column" id="header2" runat="server">
                                <h3 id="header1" runat="server" style="text-align: left;">
                                    <asp:Label ID="lblHeader" runat="server" Text='<%$ Resources:Resource, AssociatedSupplierList %>'></asp:Label></h3>
                                <h3 id="h1" runat="server" style="text-align: right;">
                                </h3>
                            </div>
                        </div>--%>
                        <div class="row">
                            <div class="large-6 medium-6 column">
                                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="backBTN" Text='<%$ Resources:Resource, Back %>'></asp:LinkButton>
                                <asp:Label ID="Label15" runat="server" Text='<%$ Resources:Resource, Search %>' Width="100%"></asp:Label>
                                <asp:TextBox ID="txtSearch" runat="server" CssClass="left" Width="70%"></asp:TextBox><asp:Button
                                    ID="Button1" runat="server" Text="Search" CssClass="BTNSearch" />
                            </div>
                        </div>
                        <div style="width: 100%; height: 650px; overflow: scroll">
                            <asp:GridView ID="grdSuppliers" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="15" HeaderStyle-HorizontalAlign ="Center">
                                <Columns>
                                    <asp:BoundField DataField="SupplierID" SortExpression="SupplierID" HeaderText='<%$ Resources:Resource, Code %>'
                                        ItemStyle-Width="20%" />
                                    <asp:BoundField DataField="NameE" SortExpression="NameE" HeaderText='<%$ Resources:Resource, NameE %>'
                                        ItemStyle-Width="30%" />
                                    <asp:BoundField DataField="NameA" SortExpression="NameA" HeaderText='<%$ Resources:Resource, NameA %>'
                                        ItemStyle-Width="30%" />
                                    <asp:BoundField DataField="MinPrice" SortExpression="MinPrice" HeaderText='<%$ Resources:Resource, MinPrice %>'
                                        ItemStyle-Width="10%" />
                                    <asp:BoundField DataField="MaxPrice" SortExpression="MaxPrice" HeaderText='<%$ Resources:Resource, MaxPrice %>'
                                        ItemStyle-Width="10%" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
