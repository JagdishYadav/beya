﻿Public Class ProjectCostLineItems
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Session("User_Role_ID") = "5" Or Session("User_Role_ID") = "6" Or Session("User_Role_ID") = "155" Or Session("User_Role_ID") = "166" Or Session("User_Role_ID") = "177") Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "Disable", "DisableNextButton();", True)
        End If
        If (IsPostBack = False) Then
            Session("ProjectSaved") = "0"
            'Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
            'Menuhide.Value = "close"
            Dim linkButton As LinkButton = Master.FindControl("LinkButton1")
            linkButton.Visible = False
            lblProjectName.Text = Session("ProjectName") + " \ " + Session("BuildingName")
            If (Request.IsAuthenticated) Then
                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                End If
                If (UICulture = "Arabic") Then
                    grdItems.Columns(1).Visible = False
                Else
                    grdItems.Columns(2).Visible = False
                End If
                Load_First_TimeDataGrid()
            Else
                Response.Redirect("../Login.aspx")
            End If
            If (Session("IsTemp").ToString().Equals("1")) Then
                grdItems.Enabled = False
            End If

            If (Session("User_Role_ID") = "999") Then
                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (Session("User_Role_ID").ToString().Contains("999")) Then
                    If (menuItems.Count > 15) Then
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                    End If
                ElseIf (Session("User_Role_ID").ToString().Contains("888")) Then
                    If (menuItems.Count > 5) Then
                        menuItems.RemoveAt(0)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                    End If
                End If
            Else
                Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                Menuhide.Value = "close"
            End If
        End If
    End Sub
    Private Sub Load_First_TimeDataGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New System.Data.DataTable

        If (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
            dt = objSqlConfig.GetProjectCostLineItemsFROMBOQByCompany(Session("ProjectID").ToString(), txtSearch.Text.Trim(), Session("CompanyID"))
        Else
            dt = objSqlConfig.GetProjectCostLineItemsByCompany(Session("ProjectID").ToString(), txtSearch.Text.Trim(), Session("CompanyID"))
        End If

        grdItems.DataSource = dt
        grdItems.DataBind()

        For i As Integer = 0 To grdItems.Rows.Count - 1
            Dim txtbox As TextBox = grdItems.Rows(i).FindControl("txtQty")
            txtbox.Text = dt.Rows(i).Item(5).ToString()
        Next
    End Sub
    Protected Sub txtQty_TextChanged(sender As Object, e As EventArgs)
        Session("ProjectSaved") = "0"
    End Sub
    Private Sub LoadGridView()
        Dim objSqlConfig As New ConfigClassSqlServer()
        If (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
            grdItems.DataSource = objSqlConfig.GetProjectCostLineItemsFROMBOQByCompany(Session("ProjectID").ToString(), txtSearch.Text.Trim(), Session("CompanyID"))
        Else
            grdItems.DataSource = objSqlConfig.GetProjectCostLineItemsByCompany(Session("ProjectID").ToString(), txtSearch.Text.Trim(), Session("CompanyID"))
        End If
        grdItems.DataBind()
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        LoadGridView()
    End Sub
    Protected Sub grdItems_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdItems.Sorting
        If (ViewState("SortState") IsNot Nothing And ViewState("SortState") = SortDirection.Ascending) Then
            ViewState("SortDirection") = "asc"
            ViewState("SortState") = 1
        Else
            ViewState("SortDirection") = "desc"
            ViewState("SortState") = 0
        End If
        ViewState("SortExp") = e.SortExpression
        FillGrid()
    End Sub
    Private Sub FillGrid()
        Dim Data As System.Data.DataTable = grdItems.DataSource
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp") Is Nothing) Then
                If ViewState("SortDirection").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp") & " " & "desc"
                End If

                grdItems.DataSource = dv
                grdItems.DataBind()
            End If
        End If
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
            Response.Redirect("ClientBOQ.aspx")
        Else
            Response.Redirect("Add_Categories.aspx")
        End If
    End Sub
    Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Button5_Click(sender, Nothing)
        If (Session("IsTemp").ToString().Equals("1")) Then
            Response.Redirect("Reports.aspx")
        Else
            If (Session("ProjectSaved") = "1") Then
                Response.Redirect("Reports.aspx")
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
            End If
        End If
    End Sub
    'Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
    '    Session("BuildingName") = ""
    '    Session("ProjectSaved") = "0"
    '    If (Session("Project_Type").Equals("Open") = False) Then
    '        Session("AddBuilding") = "1"
    '    End If
    '    Response.Redirect("Add_Building.aspx")
    'End Sub
    'Protected Sub LinkButton2_Click(sender As Object, e As EventArgs) Handles LinkButton2.Click
    '    Response.Redirect("Selection_List.aspx")
    'End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim Qty As Double

        For i As Integer = 0 To grdItems.Rows.Count - 1
            Dim txtbox As TextBox = grdItems.Rows(i).FindControl("txtQty")
            If (txtbox.Text.Length = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('5');", True)
                txtbox.Focus()
                Return
            ElseIf (Double.TryParse(txtbox.Text, Qty) = False) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
                txtbox.Focus()
                Return
            End If
            If (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
                objSqlConfig.UpdateProject_CostLineItems_BOQ(Session("ProjectID").ToString(), HttpUtility.HtmlDecode(grdItems.Rows(i).Cells.Item(0).Text.ToString), txtbox.Text.Trim())
            Else
                objSqlConfig.UpdateProject_CostLineItems(Session("ProjectID").ToString(), HttpUtility.HtmlDecode(grdItems.Rows(i).Cells.Item(0).Text.ToString), txtbox.Text.Trim())
            End If
        Next

        objSqlConfig.SetProjectStatus(Session("ProjectID"))
        Session("ProjectSaved") = "1"
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
    End Sub
    Protected Sub grdItems_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdItems.SelectedIndexChanged
        If (grdItems.SelectedIndex >= 0) Then
            Session("LineItemCrewCode") = HttpUtility.HtmlDecode(grdItems.SelectedRow.Cells(3).Text)
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "Showopoup('Project_CrewStructure.aspx');", True)
        End If
    End Sub
End Class