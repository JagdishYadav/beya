﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="ProjectCostLineItems.aspx.vb" Inherits="CEA.ProjectCostLineItems" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript">
        //$(function () {
        //    $('#sidr').hide();
        //});
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProjSaved") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SelectProduct") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SaveFirst") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorNumeric") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorQty") %>';
            }

            alert(msgstring);
        }
        function DisableNextButton() {
            document.getElementById('<%= Button4.ClientID %>').disabled = true;
        }
        function Showopoup(url) {

            params = 'width=' + screen.width;
            params += ', height=' + screen.height;
            params += ', top=0, left=0'
            params += ', fullscreen=yes';
            params += ', menubar=no, status=no, scrollbars=yes,titlebar=no, toolbar=no,directories=no';
            var win = window.open('', 'Crew Structure', params);
            win.location.href = url
            win = null;
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="lblTitle" runat="server" Text='<%$ Resources:Resource, SelectedItems %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="">
                <div class="row justify-content-between">
                    <div class="col-12 col-md-auto">
                        <asp:Label ID="lblProjectName" runat="server" Text="Label" CssClass="label"></asp:Label>
                    </div>
                    <div class="col-12 col-md-auto">
                        <div class="text-right mb-3 buttonActionGroup">
                            <div class="btn-group commanButtonGroup mb-3">
                                <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Previous %>' CssClass="btn btn-primary btn-min-120" />
                                <asp:Button ID="Button5" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="btn btn-primary btn-min-120" Visible="false" />
                                <asp:Button ID="Button4" runat="server" Text='<%$ Resources:Resource, Next %>' CssClass="btn btn-primary btn-min-120" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="row">
        <div class="large-12 medium-12 columns">
            <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
        </div>
    </div>--%>
                <div class="row">
                    <div class="col-12 col-md-7 col-lg-5">
                        <%--<asp:Label ID="Label15" runat="server" Text="" Width="100%"></asp:Label>--%>
                        <div class="input-group mb-4 commanSearchGroup">
                            <asp:TextBox ID="txtSearch" runat="server" SkinID="FormControl" CssClass="form-control" placeholder="Search"></asp:TextBox>
                            <div class="input-group-append">
                                <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Search %>' CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive tableLayoutWrap altboarder">
                            <asp:GridView ID="grdItems" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                AllowSorting="true" EmptyDataText="No Data" Width="100%" DataKeyNames="ID"
                                RowStyle-Height="20px" HeaderStyle-HorizontalAlign="Center">
                                <Columns>
                                    <asp:BoundField DataField="LineItemNo" SortExpression="LineItemNo" HeaderText='<%$ Resources:Resource, LineItemNo %>'
                                        ItemStyle-Width="50px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="LineItemNameE" SortExpression="LineItemNameE" HeaderText='<%$ Resources:Resource, LineItemName %>'
                                        ItemStyle-Width="100px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="LineItemNameA" SortExpression="LineItemNameA" HeaderText='<%$ Resources:Resource, LineItemName %>'
                                        ItemStyle-Width="100px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="CrewCode" SortExpression="CrewCode" HeaderText='<%$ Resources:Resource, CrewStructure %>'
                                        ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="Cost" SortExpression="Cost" HeaderText='<%$ Resources:Resource, ItemCost %>'
                                        ItemStyle-Width="80px" ItemStyle-Wrap="true" />
                                    <asp:TemplateField HeaderText='<%$ Resources:Resource, Qty %>' ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Width="80">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtQty" runat="server" MaxLength="6" OnTextChanged="txtQty_TextChanged"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:CommandField ShowCancelButton="false" ShowDeleteButton="false" ShowEditButton="false"
                                        ShowInsertButton="false" ItemStyle-Width="60px" ShowSelectButton="true" SelectText='<%$ Resources:Resource, ViewCrew %>' />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>

                <%--<div class="large-12 medium-12 column">--%>
                <%--<table width="100%" border="0" id="table1" runat="server">
                    <tr>
                        <td class="style3">
                            <asp:LinkButton ID="LinkButton1" runat="server" Text='<%$ Resources:Resource, GotoStart %>'
                                Font-Underline="True"></asp:LinkButton>
                        </td>
                        <td class="style2">
                            <asp:LinkButton ID="LinkButton2" runat="server" Text='<%$ Resources:Resource, GotoYourSelectionList %>'
                                Font-Underline="True"></asp:LinkButton>
                        </td>
                        <%-- <td>
                        <asp:LinkButton ID="LinkButton3" runat="server" Text='<%$ Resources:Resource, GetSavedSelectionList %>' Font-Underline="True"></asp:LinkButton>
                    </td>--%>
                <%--</tr>
                </table>--%>
                <%--</div>--%>
                <%--</asp:Content>--%>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script>
        $(document).ready(function () {
            $(".bodyWrapper .bodyContainer").addClass('p-0');
            $("#headerWrapper .toggleNavButton").hide();
        });

        $(document).ready(function () { $("#sidr").show(); });
    </script>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MiddlePart">
    <%--<div class="row">
        <div class="large-12 medium-12 column">
            <div class="outerDivBtn">
                <div class="BTNdiv">
                    <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Previous %>' CssClass="backBTN" />
                    <asp:Button ID="Button5" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave" Visible="false" />
                    <asp:Button ID="Button4" runat="server" Text='<%$ Resources:Resource, Next %>' CssClass="BTNnext" />
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>
