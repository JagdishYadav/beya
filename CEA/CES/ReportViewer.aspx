﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReportViewer.aspx.vb"
    Inherits="CEA.ReportViewer" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../CSS/ReportView.css" rel="stylesheet" type="text/css" />
    <script>
        document.oncontextmenu = new Function("return false");
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <%--<section class="content" style="margin-top:10px">--%>
    <div class="panel">
        <div class="row">
            <div class="large-5 middle-5 column">
                <asp:Button ID="lnkReports" runat="server" Text='<%$ Resources:Resource, Report %>' CssClass="BTNReport" />
                <asp:Button ID="lnkSelection" runat="server" CssClass="BTNList" Text='<%$ Resources:Resource, ItemSelectionList %>' />
                <asp:Button ID="lnkHome" runat="server" CssClass="BTNHome" Text='<%$ Resources:Resource, Home %>' />

            </div>
        </div>
        <div class="row">
            <div class="large-6  middle-6 column">
                <br />
                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, ExportReportTo %>'></asp:Label>
            </div>
        </div>
        <div class="row">
            <div class="large-6  middle-6 column">
                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="left" Width="40%">
                    <asp:ListItem Value="0">Excel Format</asp:ListItem>
                    <asp:ListItem Value="1">PDF Format</asp:ListItem>
                    <asp:ListItem Value="2">Word Format</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Export %>'
                    CssClass="BTNExport" />
            </div>
        </div>
        <div class="row">
            <div class="large-12 middle-12 column ">
                <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
                    ToolPanelView="ParameterPanel" HasExportButton="False" HasSearchButton="False" />
            </div>
        </div>
    </div>
    <%--</section>--%>
    </form>
</body>
</html>
