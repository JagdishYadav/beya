﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Project_CrewStructure.aspx.vb" Inherits="CEA.Project_CrewStructure" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#sidr').hide();
        });
        function DoPostBack() {
            document.getElementById('<%= ddlCountry.ClientID %>').focus();
            document.getElementById('<%= txtDO.ClientID %>').focus();
            //alert(txtDO.value);
            //document.getElementById('<%= hdnDoValue.ClientID %>').value = txtDO.value;
            //document.getElementById('<%= Button9999.ClientID %>').click();
            return false;
        }
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DataSaved") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "LabourAlreadyExist") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "EquipAlreadyExist") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorFileFormat") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DocAdded") %>';
            }
            else if (msgno == 6) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NoFile") %>';
            }
            else if (msgno == 7) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SaveProductivity") %>';
            }
            else if (msgno == 8) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorNumeric") %>';
            }
            else if (msgno == 9) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "CrewMissing") %>';
            }
            alert(msgstring);
        }
    </script>
    <style>
        .panelDiv {
            min-height: 115px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label14" runat="server" Text='<%$ Resources:Resource, CrewStructure %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel4" runat="server" Visible="true" CssClass="projectCrewStructureWrapper">
                <div class="row">
                    <div class="col-12">
                        <div class="text-right mb-3 buttonActionGroup">
                            <div class="btn-group commanButtonGroup mb-3">
                                <asp:Button ID="btnSave" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="btn btn-primary btn-min-120" ValidationGroup="Save" />
                                <asp:Button ID="Button8" runat="server" Text='<%$ Resources:Resource, Cancel %>' CssClass="btn btn-primary btn-min-120" />
                                <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Close %>' CssClass="btn btn-primary btn-min-120" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-lg-6 mb-4">
                                <div class="panelDiv columnDiv">
                                    <div class="p-2">
                                        <div class="row form-group">
                                            <div class="col-12 col-md-5">
                                                <asp:Label ID="Label15" runat="server" Text='<%$ Resources:Resource, SelectNat %>' Font-Bold="True" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="ddlCountry"></asp:Label>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" SkinID="FormControl" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-12 col-md-5">
                                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, Code %>' Font-Bold="True" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtCode"></asp:Label>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <%--<asp:Label ID="lblProdCode" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>--%>
                                                <asp:TextBox ID="txtCode" runat="server" Font-Bold="True" MaxLength="20" ReadOnly="true" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorCode %>'
                                                    ControlToValidate="txtCode" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>

                                            </div>
                                        </div>
                                    </div>
                                    <%-- <div class="row">
                            <div class="large-3 medium-3 columns bold">
                                <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, ItemName %>'
                                    Font-Bold="True" ForeColor="Red"></asp:Label>
                            </div>
                            <div class="large-9 medium-9 column">
                                <asp:Label ID="lblProdName" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                <asp:Label ID="lblProdName2" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                            </div>
                        </div>--%>
                                    <%--<div class="row">
                            <div class="large-3 medium-3 columns bold">
                                <asp:Label ID="Label5" runat="server" Text='<%$ Resources:Resource, ItemUnit %>'
                                    Font-Bold="True" ForeColor="Red"></asp:Label>
                            </div>
                            <div class="large-9 medium-9 column">
                                <asp:Label ID="lblProdUnit" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                            </div>
                        </div>--%>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6 mb-4">
                                <div class="panelDiv columnDiv">
                                    <div class="p-2">
                                        <div class="row form-group">
                                            <div class="col-12 col-md-5">
                                                <asp:Label ID="Label8" runat="server" Text='<%$ Resources:Resource, DailyOutput %>' Font-Bold="True" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtDO"></asp:Label>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <asp:TextBox ID="txtDO" runat="server" MaxLength="10" AutoPostBack="True" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="frvDO" runat="server" ErrorMessage='<%$ Resources:Resource, MissingDailyOutput %>'
                                                    ControlToValidate="txtDO" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="reAvgSal" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                                    SetFocusOnError="true" ControlToValidate="txtDO" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                                    Display="None" ValidationGroup="Save">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-12 col-md-5">
                                                <asp:Label ID="Label7" runat="server" Text='<%$ Resources:Resource, HourlyOutput %>'></asp:Label>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <asp:Label ID="lblHO" runat="server" Text="Value" Font-Bold="True"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-12 col-md-5">
                                                <asp:Label ID="Label10" runat="server" Text='<%$ Resources:Resource, ManHour %>'></asp:Label>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <asp:Label ID="lblMO" runat="server" Text="Value" Font-Bold="True"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-12 col-md-5">
                                                <asp:Label ID="Label12" runat="server" Text='<%$ Resources:Resource, EquipHour %>'></asp:Label>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <asp:Label ID="lblEH" runat="server" Text="Value" Font-Bold="True"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--<div class="row">
        <div class="large-6 medium-6 column">
                        <asp:Label ID="Label18" runat="server" Text="Search" Width="100%"></asp:Label>
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="left" Width="60%"></asp:TextBox><asp:Button
                            ID="Button5" runat="server" Text="Search" CssClass="BTNSearch" />
                    </div>
                     <div class="large-6 medium-6 column">
                        <asp:Label ID="Label19" runat="server" Text="Search" Width="100%"></asp:Label>
                        <asp:TextBox ID="TextBox1" runat="server" CssClass="left" Width="60%"></asp:TextBox><asp:Button
                            ID="Button1" runat="server" Text="Search" CssClass="BTNSearch" />
                    </div>
    </div>--%>
                        <div class="row" id="abc" runat="server">
                            <div class="col-12 col-lg-6" id="grd1" runat="server">
                                <asp:Label ID="Label16" runat="server" Text='<%$ Resources:Resource, LaboursList %>' SkinID="Controllabel" CssClass="control-label mb-3 font-weight-bold" AssociatedControlID="grdLabour"></asp:Label>

                                <div class="table-responsive tableLayoutWrap altboarder">
                                    <asp:GridView ID="grdLabour" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        EmptyDataText="No Data" Width="100%" PageSize="10" DataKeyNames="Code" ShowFooter="True"
                                        HeaderStyle-Height="30px" RowStyle-Height="25px" FooterStyle-BackColor="ActiveBorder"
                                        FooterStyle-Height="25px" AllowSorting="false" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <asp:TemplateField HeaderText='<%$ Resources:Resource, Prof %>'>
                                                <ItemTemplate>
                                                    <asp:Label ID="l1" runat="server" Text='<% # Eval("ProfE")%>' />
                                                    <asp:Label ID="l2" runat="server" Text='<% # Eval("ProfA")%>' />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:DropDownList ID="ddlLabE" runat="server" SkinID="FormControl" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </FooterTemplate>
                                                <EditItemTemplate>
                                                    <asp:Label ID="lblLabEdit" runat="server" Text='<% # Eval("CrewCode")%>' Visible="false" />
                                                    <asp:DropDownList ID="ddlLabFE" runat="server" SkinID="FormControl" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText='<%$ Resources:Resource, Man %>'>
                                                <ItemTemplate>
                                                    <asp:Label ID="l3" runat="server" Text='<% # Eval("Man_Day")%>' />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtMDF" runat="server" MaxLength="10" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfv114" runat="server" ErrorMessage='<%$ Resources:Resource, MissingMan %>'
                                                        ControlToValidate="txtMDF" SetFocusOnError="true" ValidationGroup="SaveLab" Display="None"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="reAvgSal1" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                                        SetFocusOnError="true" ControlToValidate="txtMDF" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                                        Display="None" ValidationGroup="SaveLab">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </asp:RegularExpressionValidator>
                                                </FooterTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtMD" runat="server" MaxLength="10" Text='<% # Eval("Man_Day")%>' SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfv119" runat="server" ErrorMessage='<%$ Resources:Resource, MissingMan %>'
                                                        ControlToValidate="txtMD" SetFocusOnError="true" ValidationGroup="SaveLabE" Display="None"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="re2345" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                                        SetFocusOnError="true" ControlToValidate="txtMD" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                                        Display="None" ValidationGroup="SaveLabE"></asp:RegularExpressionValidator>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                <FooterTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" Text='<%$ Resources:Resource, Save %>'
                                                        OnClick="LinkButton1_Click" ValidationGroup="SaveLab"></asp:LinkButton>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="LinkButton8" runat="server" CommandName="Update" Text='<%$ Resources:Resource, Update %>'
                                                        ItemStyle-Width="25px" ValidationGroup="SaveLabE"></asp:LinkButton>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton8" runat="server" CommandName="Edit" Text='<%$ Resources:Resource, Edit %>'
                                                        ItemStyle-Width="25px"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="LinkButton9" runat="server" CommandName="Cancel" Text='<%$ Resources:Resource, Cancel %>'
                                                        ItemStyle-Width="25px"></asp:LinkButton>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton9" runat="server" CommandName="Delete" Text='<%$ Resources:Resource, Delete %>'
                                                        ItemStyle-Width="25px"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="ActiveBorder" Height="30px"></FooterStyle>
                                        <HeaderStyle Height="30px"></HeaderStyle>
                                        <RowStyle Height="25px"></RowStyle>
                                    </asp:GridView>
                                </div>
                            </div>

                            <div class="col-12 col-lg-6">
                                <asp:Label ID="Label17" runat="server" Text='<%$ Resources:Resource, EquipmentList %>' SkinID="Controllabel" CssClass="control-label mb-3 font-weight-bold" AssociatedControlID="grdEquip"></asp:Label>

                                <div class="table-responsive tableLayoutWrap altboarder">
                                    <asp:GridView ID="grdEquip" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        EmptyDataText="No Data" Width="100%" PageSize="10" DataKeyNames="Code" ShowFooter="True"
                                        HeaderStyle-Height="30px" RowStyle-Height="25px" FooterStyle-BackColor="ActiveBorder"
                                        FooterStyle-Height="25px" AllowSorting="false" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <asp:TemplateField HeaderText='<%$ Resources:Resource, Desc %>'>
                                                <ItemTemplate>
                                                    <asp:Label ID="l11" runat="server" Text='<% # Eval("DescE")%>' />
                                                    <asp:Label ID="l12" runat="server" Text='<% # Eval("DescA")%>' />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:DropDownList ID="ddlEquipE" runat="server" SkinID="FormControl" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </FooterTemplate>
                                                <EditItemTemplate>
                                                    <asp:Label ID="lblEqEdit" runat="server" Text='<% # Eval("CrewCode")%>' Visible="false" />
                                                    <asp:DropDownList ID="ddlEquipFE" runat="server" SkinID="FormControl" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText='<%$ Resources:Resource, Day %>'>
                                                <ItemTemplate>
                                                    <asp:Label ID="l13" runat="server" Text='<% # Eval("Man_Day")%>' />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtMDF1" runat="server" MaxLength="10" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfv1134" runat="server" ErrorMessage='<%$ Resources:Resource, MissingDay %>'
                                                        ControlToValidate="txtMDF1" SetFocusOnError="true" ValidationGroup="SaveEq" Display="None"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="reAvgSal2" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                                        SetFocusOnError="true" ControlToValidate="txtMDF1" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                                        Display="None" ValidationGroup="SaveEq">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </asp:RegularExpressionValidator>
                                                </FooterTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtMD1" runat="server" MaxLength="50" Text='<% # Eval("Man_Day")%>' SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfv1319" runat="server" ErrorMessage='<%$ Resources:Resource, MissingDay %>'
                                                        ControlToValidate="txtMD1" SetFocusOnError="true" ValidationGroup="SaveEqE" Display="None"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="rgexp897" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                                        SetFocusOnError="true" ControlToValidate="txtMD1" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                                        Display="None" ValidationGroup="SaveEqE">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </asp:RegularExpressionValidator>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                <FooterTemplate>
                                                    <asp:LinkButton ID="LinkButton11" runat="server" Text='<%$ Resources:Resource, Save %>'
                                                        OnClick="LinkButton11_Click" ValidationGroup="SaveEq"></asp:LinkButton>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="LinkButton81" runat="server" CommandName="Update" Text='<%$ Resources:Resource, Update %>'
                                                        ItemStyle-Width="25px" ValidationGroup="SaveEqE"></asp:LinkButton>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton82" runat="server" CommandName="Edit" Text='<%$ Resources:Resource, Edit %>'
                                                        ItemStyle-Width="25px"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="LinkButton91" runat="server" CommandName="Cancel" Text='<%$ Resources:Resource, Cancel %>'
                                                        ItemStyle-Width="25px"></asp:LinkButton>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton92" runat="server" CommandName="Delete" Text='<%$ Resources:Resource, Delete %>'
                                                        ItemStyle-Width="25px"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </asp:Panel>


            <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
                ShowMessageBox="true" ValidationGroup="Save" DisplayMode="BulletList" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveLab" DisplayMode="BulletList" />
            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveLabE" DisplayMode="BulletList" />
            <asp:ValidationSummary ID="ValidationSummary3" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveEq" DisplayMode="BulletList" />
            <asp:ValidationSummary ID="ValidationSummary4" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveEqE" DisplayMode="BulletList" />

            <asp:ValidationSummary ID="ValidationSummary5" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveHistoryE" DisplayMode="BulletList" />
            <asp:ValidationSummary ID="ValidationSummary6" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveHistoryF" DisplayMode="BulletList" />




            <asp:Button ID="Button9999" runat="server" Text="" />
            <asp:HiddenField ID="hdnDoValue" runat="server" />
        </ContentTemplate>

    </asp:UpdatePanel>


    <script>
        $(document).ready(function () {
            $(".bodyWrapper .bodyContainer").addClass('p-0');
            $("#headerWrapper .toggleNavButton").hide();
        });
    </script>
</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MiddlePart">
    <%--<div class="row">
        <div class="large-12 medium-12 column">
            <div class="outerDivBtn">
                <div class="BTNdiv">
                    <asp:Button ID="btnSave" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave" ValidationGroup="Save" />
                    <asp:Button ID="Button8" runat="server" Text='<%$ Resources:Resource, Cancel %>' CssClass="BTNCancel" />
                    <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Close %>' CssClass="BTNclose" />
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>
