﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/PopUpMaster.Master"
    CodeBehind="BOQ_Mapping.aspx.vb" Inherits="CEA.BOQ_Mapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style>
        .TreeViewStyle
        {
            margin-top:50px;
        }
        .EmptyRowStyle td
        {
            height: 296px;
        }
         #divTreeViewScrollTo a
        {
            display:none;
        }
        #divTreeViewScrollTo td a
        {
            display:inline;
        }
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>

    <script type="text/javascript">
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProjSaved") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "UnMappingSuccess") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SelectItemUnmap") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SelectLeafNodeUnmap")%>';
            }
            alert(msgstring);
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="lblHeader" runat="server" Text='<%$ Resources:Resource, BOQUNMAPPING %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
   
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <script type="text/javascript">
        var xPos, yPos, xPos1, yPos1;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xPos = $get('scrollDiv').scrollLeft;
            yPos = $get('scrollDiv').scrollTop;

            xPos1 = $get('divTreeViewScrollTo').scrollLeft;
            yPos1 = $get('divTreeViewScrollTo').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('scrollDiv').scrollLeft = xPos;
            $get('scrollDiv').scrollTop = yPos;

            $get('divTreeViewScrollTo').scrollLeft = xPos1;
            $get('divTreeViewScrollTo').scrollTop = yPos1;
        }

        $(document).ready(function () {
            $(".bodyWrapper .bodyContainer").addClass('p-0');
            $("#headerWrapper .toggleNavButton").hide();
        });
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <asp:Panel ID="Panel1" runat="server">
                <div class="row">
                    <div id="scrollDiv" class="large-5 medium-5 columns height350" style="padding-top:50px" >
                        <asp:GridView ID="grdBOQ" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                            AllowSorting="false" EmptyDataText="No Data" Width="100%" PageSize="3000" DataKeyNames="Div"
                            SelectedRowStyle-BackColor="Yellow" ShowHeaderWhenEmpty="true" HeaderStyle-Height = "47px" HeaderStyle-HorizontalAlign ="Center">
                            <Columns>
                                <asp:BoundField DataField="Div" HeaderText='<%$ Resources:Resource, Div1 %>' ItemStyle-Width="2%" />
                                <asp:BoundField DataField="No" HeaderText='<%$ Resources:Resource, LineItemNo %>'
                                    ItemStyle-Width="11%" />
                                <asp:BoundField DataField="NameE" HeaderText='<%$ Resources:Resource, NameEng %>'
                                    ItemStyle-Width="38%" />
                                <asp:BoundField DataField="NameA" HeaderText='<%$ Resources:Resource, NameAra %>'
                                    ItemStyle-Width="38%" />
                                <asp:BoundField DataField="Unit" HeaderText='<%$ Resources:Resource, Unit %>' ItemStyle-Width="8%" />
                                <asp:BoundField DataField="Qty" HeaderText='<%$ Resources:Resource, Qty1 %>' ItemStyle-Width="3%" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="large-2 medium-2 columns height350">
                        <asp:Button ID="Button3" runat="server" Text='<%$ Resources:Resource, Unmap %>' CssClass="BTNExport btnMiddle top160" />
                    </div>
                    <div id="divTreeViewScrollTo" class="large-5 medium-5 columns height350">
                        <asp:TreeView ID="tvLevels" runat="server" LineImagesFolder="~/TreeLineImages" ShowLines="True"
                            ShowCheckBoxes="None" LeafNodeStyle-BackColor="Yellow" NodeWrap = "true">
                        </asp:TreeView>
                    </div>
                </div>
                
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <br />
    <br />
    <br />

     <div class="large-12 medium-12 column" style="left:40%">
            <div class="outerDivBtn">
                <div class="BTNdiv">
                <asp:Button ID="btnSave" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave rightbtn" />
                       
                         <asp:Button ID="btnCancel" runat="server" Text='<%$ Resources:Resource, Cancel %>' CssClass="BTNCancel middlebtn" />
                      
                         <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Close %>' CssClass="BTNclose" />
                             </div>
            </div>
        </div>
</asp:Content>


 
    