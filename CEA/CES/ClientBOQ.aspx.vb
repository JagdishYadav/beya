﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.Office.Interop
Imports System.Data.OleDb
Imports System.Drawing
Imports Telerik.Web.UI
Imports System.Web.Services

Public Class ClientBOQ
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            Session("ProjectSaved") = "0"
            'Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
            'Menuhide.Value = "close"

            hdnCompID.Value = Session("CompanyID")
            hdnClientTypeID.Value = Session("ClientTypeID")
            hdnLangID.Value = "0"

            'RadTreeView1.Attributes.Add("onclick", "addNodes()")

            Dim linkButton As LinkButton = Master.FindControl("LinkButton1")
            linkButton.Visible = False
            lblProjectName.Text = Session("ProjectName") + " \ " + Session("BuildingName")
            If (Request.IsAuthenticated) Then
                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    hdnLangID.Value = "0"

                    grdData.Columns(1).Visible = False
                    grdData.Columns(2).Visible = True
                    grdData.Columns(8).Visible = False
                    grdData.Columns(9).Visible = True
                Else
                    grdData.Columns(1).Visible = True
                    grdData.Columns(2).Visible = False
                    grdData.Columns(8).Visible = True
                    grdData.Columns(9).Visible = False
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If

            Button100.Attributes.Add("style", "display:none")
            Button200.Attributes.Add("style", "display:none")
            Button4.Attributes.Add("style", "display:none")
            Button1.Attributes("onclick") = "return CheckPreConditions();"

            'load system components in tree view
            LoadTreeViewData()
            'RadTreeView1.Visible = False
            'Dim thread As New Thread(AddressOf LoadTreeThread)
            'thread.Start()
            'thread.Abort()

            grdBOQ.DataSource = New List(Of String)
            grdBOQ.DataBind()

            Dim dt As New System.Data.DataTable
            dt.Columns.Add("ProjectID")
            dt.Columns.Add("BOQLineNo")
            dt.Columns.Add("LineItemCode")
            dt.Columns.Add("Qty")
            dt.Columns.Add("grid1")
            dt.Columns.Add("grid2")
            dt.Columns.Add("grid3")
            dt.Columns.Add("grid4")
            dt.Columns.Add("grid5")
            dt.Columns.Add("grid6")

            Session("MappedData") = dt

            If (Session("Project_Type").Equals("Open")) Then

                Dim dtProjectMappedData As New System.Data.DataTable
                Dim sqlConnection As New SqlConnection()
                Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
                sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                sqlConnection.Open()

                Dim dtFilePath As New System.Data.DataTable
                sqlCommand.CommandText = "SELECT TOP(1) FilePath FROM Project_BOQ_LineItem_Mapping WHERE ProjectID = '" + Session("ProjectID") + "' "
                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dtFilePath)

                Session("SuppliersValues") = Nothing

                If (dtFilePath.Rows.Count > 0) Then
                    Dim strFileName As String = dtFilePath.Rows(0).Item(0).ToString()

                    ImportFromExcel(strFileName)

                    sqlCommand.CommandText = "SELECT bd.ProjectID, bd.BOQ_LineNo BOQLineNo, bd.ItemNo LineItemCode, bd.Qty, bd.Div, bd.BOQ_No, bd.NameE, bd.NameA, bd.Unit, bd.Qty, cli.ItemNo, cli.Supp_Quot_ID, cli.DataType FROM Project_BOQ_Data bd LEFT JOIN Project_CostLineItems cli ON cli.ProjectID = bd.ProjectID AND bd.ItemNo = cli.ItemNo WHERE bd.ProjectID = '" + Session("ProjectID").ToString() + "' "
                    sqlDataAdapter.Fill(dtProjectMappedData)

                    Dim strHiddenValue As String = ""
                    For i As Integer = 0 To dtProjectMappedData.Rows.Count - 1
                        Dim rowIndex As Integer = Convert.ToInt32(dtProjectMappedData.Rows(i).Item(1).ToString())
                        rowIndex = rowIndex - 1
                        grdBOQ.Rows(rowIndex).Cells(0).BackColor = Drawing.Color.Green
                        grdBOQ.Rows(rowIndex).Cells(1).BackColor = Drawing.Color.Green
                        grdBOQ.Rows(rowIndex).Cells(2).BackColor = Drawing.Color.Green
                        grdBOQ.Rows(rowIndex).Cells(3).BackColor = Drawing.Color.Green
                        grdBOQ.Rows(rowIndex).Cells(4).BackColor = Drawing.Color.Green
                        grdBOQ.Rows(rowIndex).Cells(5).BackColor = Drawing.Color.Green

                        strHiddenValue = strHiddenValue + dtProjectMappedData.Rows(i).Item(10).ToString() + ":" + dtProjectMappedData.Rows(i).Item(11).ToString() + ":" + dtProjectMappedData.Rows(i).Item(12).ToString() + ","
                    Next

                    hdnSuppIDs.Value = strHiddenValue
                    Session("SuppliersValues") = strHiddenValue

                    If (dtProjectMappedData.Rows.Count > 0) Then
                        ViewState("BackColor") = grdBOQ.Rows(0).BackColor
                    End If

                    'dtProjectMappedData.Columns.Add("grid1")
                    'dtProjectMappedData.Columns.Add("grid2")
                    'dtProjectMappedData.Columns.Add("grid3")
                    'dtProjectMappedData.Columns.Add("grid4")
                    'dtProjectMappedData.Columns.Add("grid5")
                    'dtProjectMappedData.Columns.Add("grid6")

                    Session("MappedData") = dtProjectMappedData
                    Session("ProjectSaved") = "1"
                End If

                sqlConnection.Close()

            End If
            If (Session("IsTemp").ToString().Equals("1")) Then
                RadTreeView1.Enabled = False
                grdBOQ.Enabled = False
                uplImage.Enabled = False
            End If

            If (Session("User_Role_ID") = "999") Then
                Dim Menu1 As WebControls.Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (Session("User_Role_ID").ToString().Contains("999")) Then
                    If (menuItems.Count > 15) Then
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                    End If
                ElseIf (Session("User_Role_ID").ToString().Contains("888")) Then
                    If (menuItems.Count > 5) Then
                        menuItems.RemoveAt(0)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                    End If
                End If
            Else
                Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                Menuhide.Value = "close"
            End If

        End If

        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim arrValues(10) As String
        arrValues(0) = ""
        arrValues(1) = ""
        arrValues(2) = ""
        arrValues(3) = ""
        arrValues(4) = ""
        arrValues(5) = ""
        arrValues(6) = ""
        arrValues(7) = ""
        arrValues(8) = ""
        arrValues(9) = ""

        Dim dt1 As New DataTable()
        dt1.Columns.Add("ID")
        dt1.Columns.Add("No")
        dt1.Columns.Add("NameE")
        dt1.Columns.Add("NameA")
        dt1.Columns.Add("Unit")
        dt1.Columns.Add("CrewCode")
        dt1.Columns.Add("DO")
        dt1.Columns.Add("Cost")
        dt1.Columns.Add("MaterialCost")
        dt1.Columns.Add("SuppNameE")
        dt1.Columns.Add("SuppNameA")
        dt1.Columns.Add("ID1")

        dt1.Rows.Add(arrValues)
        grdData.DataSource = dt1
        grdData.DataBind()

        For i As Integer = 0 To grdData.Rows.Count - 1
            grdData.Rows(i).Cells(11).Style.Add("display", "none")
        Next

    End Sub
    'Private Sub LoadTreeViewData()
    '    Dim strClientTypeID As String = "1"
    '    If (Session("ClientTypeID") IsNot Nothing) Then
    '        strClientTypeID = Session("ClientTypeID")
    '    End If

    '    Dim objSqlConfig As New ConfigClassSqlServer()
    '    Dim strCategories As String = objSqlConfig.GetUserCategories(Session("UserID"))

    '    'If (hdnInitialLoad.Value.ToString().Equals("1")) Then
    '    Dim sqlConnection As New SqlConnection()
    '    Dim node As RadTreeNode = Nothing
    '    'Dim NodeLevel1, NodeLevel2, NodeLevel3, NodeLevel4, NodeLevel5, NodeLevel6, NodeLevel7 As RadTreeNode
    '    'Dim NodeLevel1 As RadTreeNode
    '    RadTreeView1.Nodes.Clear()

    '    Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
    '    sqlCommand.CommandTimeout = 120000
    '    'Dim r1, r2, r3, r4, r5, r6, r7, r8 As SqlDataReader
    '    Dim r1 As SqlDataReader
    '    'Dim dt1, dt2, dt3, dt4, dt5, dt6, dt7, dt8 As New System.Data.DataTable
    '    Dim dt1 As New System.Data.DataTable

    '    sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
    '    sqlConnection.Open()

    '    If (UICulture = "Arabic") Then
    '        sqlCommand.CommandText = "SELECT DISTINCT ID,  NameA, NameE, No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No + '  ' + NameE IN ( " + strCategories + " ) ORDER BY No ASC"
    '        r1 = sqlCommand.ExecuteReader()
    '        dt1.Load(r1)
    '        r1.Close()

    '        For Each dr1 As DataRow In dt1.Rows
    '            If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
    '                node = New RadTreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
    '                node.Target = dr1.Item(2).ToString()
    '                'node.ImageUrl = dr1.Item(3).ToString()
    '                node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
    '                RadTreeView1.Nodes.Add(node)
    '                'NodeLevel1 = node

    '                'sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                'r2 = sqlCommand.ExecuteReader()
    '                'dt2.Clear()
    '                'dt2.Load(r2)
    '                'r2.Close()
    '                'For Each dr2 As DataRow In dt2.Rows
    '                '    If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
    '                '        node = New RadTreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
    '                '        node.Target = dr2.Item(2).ToString()
    '                '        'node.ImageUrl = dr2.Item(3).ToString() 
    '                '        node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
    '                '        NodeLevel1.ChildNodes.Add(node)
    '                '        NodeLevel2 = node

    '                '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '        r3 = sqlCommand.ExecuteReader()
    '                '        dt3.Clear()
    '                '        dt3.Load(r3)
    '                '        r3.Close()
    '                '        For Each dr3 As DataRow In dt3.Rows
    '                '            If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
    '                '                node = New RadTreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
    '                '                node.Target = dr3.Item(2).ToString()
    '                '                'node.ImageUrl = dr3.Item(3).ToString()
    '                '                node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
    '                '                NodeLevel2.ChildNodes.Add(node)
    '                '                NodeLevel3 = node

    '                '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                r4 = sqlCommand.ExecuteReader()
    '                '                dt4.Clear()
    '                '                dt4.Load(r4)
    '                '                r4.Close()

    '                '                For Each dr4 As DataRow In dt4.Rows
    '                '                    If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
    '                '                        node = New RadTreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
    '                '                        node.Target = dr4.Item(2).ToString()
    '                '                        'node.ImageUrl = dr4.Item(3).ToString()
    '                '                        node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
    '                '                        NodeLevel3.ChildNodes.Add(node)
    '                '                        NodeLevel4 = node

    '                '                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                        r5 = sqlCommand.ExecuteReader()
    '                '                        dt5.Clear()
    '                '                        dt5.Load(r5)
    '                '                        r5.Close()
    '                '                        For Each dr5 As DataRow In dt5.Rows
    '                '                            If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
    '                '                                node = New RadTreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
    '                '                                node.Target = dr5.Item(2).ToString()
    '                '                                'node.ImageUrl = dr5.Item(3).ToString()
    '                '                                node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
    '                '                                NodeLevel4.ChildNodes.Add(node)
    '                '                                NodeLevel5 = node

    '                '                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                                r6 = sqlCommand.ExecuteReader()
    '                '                                dt6.Clear()
    '                '                                dt6.Load(r6)
    '                '                                r6.Close()
    '                '                                For Each dr6 As DataRow In dt6.Rows
    '                '                                    If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
    '                '                                        node = New RadTreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
    '                '                                        node.Target = dr6.Item(2).ToString()
    '                '                                        'node.ImageUrl = dr6.Item(3).ToString()
    '                '                                        node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
    '                '                                        NodeLevel5.ChildNodes.Add(node)
    '                '                                        NodeLevel6 = node

    '                '                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                                        r7 = sqlCommand.ExecuteReader()
    '                '                                        dt7.Clear()
    '                '                                        dt7.Load(r7)
    '                '                                        r7.Close()
    '                '                                        For Each dr7 As DataRow In dt7.Rows
    '                '                                            If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
    '                '                                                node = New RadTreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
    '                '                                                node.Target = dr7.Item(2).ToString()
    '                '                                                'node.ImageUrl = dr7.Item(3).ToString()
    '                '                                                node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
    '                '                                                NodeLevel6.ChildNodes.Add(node)
    '                '                                                NodeLevel7 = node

    '                '                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                                                r8 = sqlCommand.ExecuteReader()
    '                '                                                dt8.Clear()
    '                '                                                dt8.Load(r8)
    '                '                                                r8.Close()
    '                '                                                For Each dr8 As DataRow In dt8.Rows
    '                '                                                    If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
    '                '                                                        node = New RadTreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
    '                '                                                        node.Target = dr8.Item(2).ToString()
    '                '                                                        'node.ImageUrl = dr8.Item(3).ToString()
    '                '                                                        node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
    '                '                                                        NodeLevel7.ChildNodes.Add(node)
    '                '                                                    End If
    '                '                                                Next
    '                '                                            End If
    '                '                                        Next
    '                '                                    End If
    '                '                                Next
    '                '                            End If
    '                '                        Next
    '                '                    End If
    '                '                Next
    '                '            End If
    '                '        Next
    '                '    End If
    '                'Next
    '            End If
    '        Next

    '    Else
    '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No + '  ' + NameE IN ( " + strCategories + " ) ORDER BY No ASC"
    '        r1 = sqlCommand.ExecuteReader()
    '        dt1.Load(r1)
    '        r1.Close()

    '        For Each dr1 As DataRow In dt1.Rows
    '            If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
    '                node = New RadTreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
    '                node.Target = dr1.Item(2).ToString()
    '                'node.ImageUrl = dr1.Item(3).ToString()
    '                node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
    '                RadTreeView1.Nodes.Add(node)
    '                'NodeLevel1 = node

    '                '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '        r2 = sqlCommand.ExecuteReader()
    '                '        dt2.Clear()
    '                '        dt2.Load(r2)
    '                '        r2.Close()
    '                '        For Each dr2 As DataRow In dt2.Rows
    '                '            If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
    '                '                node = New RadTreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
    '                '                node.Target = dr2.Item(2).ToString()
    '                '                'node.ImageUrl = dr2.Item(3).ToString()
    '                '                node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
    '                '                NodeLevel1.ChildNodes.Add(node)
    '                '                NodeLevel2 = node

    '                '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                r3 = sqlCommand.ExecuteReader()
    '                '                dt3.Clear()
    '                '                dt3.Load(r3)
    '                '                r3.Close()
    '                '                For Each dr3 As DataRow In dt3.Rows
    '                '                    If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
    '                '                        node = New RadTreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
    '                '                        node.Target = dr3.Item(2).ToString()
    '                '                        'node.ImageUrl = dr3.Item(3).ToString()
    '                '                        node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
    '                '                        NodeLevel2.ChildNodes.Add(node)
    '                '                        NodeLevel3 = node

    '                '                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                        r4 = sqlCommand.ExecuteReader()
    '                '                        dt4.Clear()
    '                '                        dt4.Load(r4)
    '                '                        r4.Close()

    '                '                        For Each dr4 As DataRow In dt4.Rows
    '                '                            If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
    '                '                                node = New RadTreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
    '                '                                node.Target = dr4.Item(2).ToString()
    '                '                                'node.ImageUrl = dr4.Item(3).ToString()
    '                '                                node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
    '                '                                NodeLevel3.ChildNodes.Add(node)
    '                '                                NodeLevel4 = node

    '                '                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                                r5 = sqlCommand.ExecuteReader()
    '                '                                dt5.Clear()
    '                '                                dt5.Load(r5)
    '                '                                r5.Close()
    '                '                                For Each dr5 As DataRow In dt5.Rows
    '                '                                    If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
    '                '                                        node = New RadTreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
    '                '                                        node.Target = dr5.Item(2).ToString()
    '                '                                        'node.ImageUrl = dr5.Item(3).ToString()
    '                '                                        node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
    '                '                                        NodeLevel4.ChildNodes.Add(node)
    '                '                                        NodeLevel5 = node

    '                '                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                                        r6 = sqlCommand.ExecuteReader()
    '                '                                        dt6.Clear()
    '                '                                        dt6.Load(r6)
    '                '                                        r6.Close()
    '                '                                        For Each dr6 As DataRow In dt6.Rows
    '                '                                            If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
    '                '                                                node = New RadTreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
    '                '                                                node.Target = dr6.Item(2).ToString()
    '                '                                                'node.ImageUrl = dr6.Item(3).ToString()
    '                '                                                node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
    '                '                                                NodeLevel5.ChildNodes.Add(node)
    '                '                                                NodeLevel6 = node

    '                '                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                                                r7 = sqlCommand.ExecuteReader()
    '                '                                                dt7.Clear()
    '                '                                                dt7.Load(r7)
    '                '                                                r7.Close()
    '                '                                                For Each dr7 As DataRow In dt7.Rows
    '                '                                                    If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
    '                '                                                        node = New RadTreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
    '                '                                                        node.Target = dr7.Item(2).ToString()
    '                '                                                        'node.ImageUrl = dr7.Item(3).ToString()
    '                '                                                        node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
    '                '                                                        NodeLevel6.ChildNodes.Add(node)
    '                '                                                        NodeLevel7 = node

    '                '                                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                                                        r8 = sqlCommand.ExecuteReader()
    '                '                                                        dt8.Clear()
    '                '                                                        dt8.Load(r8)
    '                '                                                        r8.Close()
    '                '                                                        For Each dr8 As DataRow In dt8.Rows
    '                '                                                            If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
    '                '                                                                node = New RadTreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
    '                '                                                                node.Target = dr8.Item(2).ToString()
    '                '                                                                'node.ImageUrl = dr8.Item(3).ToString()
    '                '                                                                node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
    '                '                                                                NodeLevel7.ChildNodes.Add(node)
    '                '                                                            End If
    '                '                                                        Next
    '                '                                                    End If
    '                '                                                Next
    '                '                                            End If
    '                '                                        Next
    '                '                                    End If
    '                '                                Next
    '                '                            End If
    '                '                        Next
    '                '                    End If
    '                '                Next
    '                '            End If
    '                '        Next
    '            End If
    '        Next
    '    End If

    '    RadTreeView1.Visible = True
    '    sqlConnection.Close()
    '    RadTreeView1.CollapseAll()
    '    'End If
    '    'ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "HideDiv();", True)
    'End Sub
    Private Sub LoadTreeViewData()
        Dim strClientTypeID As String = "1"
        If (Session("ClientTypeID") IsNot Nothing) Then
            strClientTypeID = Session("ClientTypeID")
        End If

        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim strCategories As String = objSqlConfig.GetUserCategories(Session("UserID"))

        'If (hdnInitialLoad.Value.ToString().Equals("1")) Then
        Dim sqlConnection As New SqlConnection()
        Dim node As RadTreeNode = Nothing
        Dim NodeLevel1, NodeLevel2, NodeLevel3, NodeLevel4, NodeLevel5, NodeLevel6, NodeLevel7 As RadTreeNode
        'Dim NodeLevel1 As RadTreeNode
        RadTreeView1.Nodes.Clear()

        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlCommand.CommandTimeout = 120000
        Dim r1, r2, r3, r4, r5, r6, r7, r8 As SqlDataReader
        'Dim r1 As SqlDataReader
        Dim dt1, dt2, dt3, dt4, dt5, dt6, dt7, dt8 As New System.Data.DataTable
        'Dim dt1 As New System.Data.DataTable

        Try

            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
            sqlConnection.Open()

            If (UICulture = "Arabic") Then
                sqlCommand.CommandText = "SELECT DISTINCT ID,  NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE (MainHierarchy1.ParentNo iS NULL OR Len(MainHierarchy1.ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No + '  ' + NameE IN ( " + strCategories + " ) AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                r1 = sqlCommand.ExecuteReader()
                dt1.Load(r1)
                r1.Close()

                For Each dr1 As DataRow In dt1.Rows
                    If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
                        node = New RadTreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
                        node.Target = dr1.Item(2).ToString()
                        'node.ImageUrl = dr1.Item(3).ToString()
                        node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
                        node.Checkable = False
                        RadTreeView1.Nodes.Add(node)
                        'NodeLevel1 = node

                        'sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE MainHierarchy1.ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                        'r2 = sqlCommand.ExecuteReader()
                        'dt2.Clear()
                        'dt2.Load(r2)
                        'r2.Close()
                        'For Each dr2 As DataRow In dt2.Rows
                        '    If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
                        '        node = New RadTreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
                        '        node.Target = dr2.Item(2).ToString()
                        '        'node.ImageUrl = dr2.Item(3).ToString() 
                        '        node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
                        '        node.Checkable = False
                        '        NodeLevel1.Nodes.Add(node)
                        '        NodeLevel2 = node

                        '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE MainHierarchy1.ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                        '        r3 = sqlCommand.ExecuteReader()
                        '        dt3.Clear()
                        '        dt3.Load(r3)
                        '        r3.Close()
                        '        For Each dr3 As DataRow In dt3.Rows
                        '            If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
                        '                node = New RadTreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
                        '                node.Target = dr3.Item(2).ToString()
                        '                'node.ImageUrl = dr3.Item(3).ToString()
                        '                node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
                        '                node.Checkable = False
                        '                NodeLevel2.Nodes.Add(node)
                        '                NodeLevel3 = node

                        '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE MainHierarchy1.ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                        '                r4 = sqlCommand.ExecuteReader()
                        '                dt4.Clear()
                        '                dt4.Load(r4)
                        '                r4.Close()

                        '                For Each dr4 As DataRow In dt4.Rows
                        '                    If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
                        '                        node = New RadTreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
                        '                        node.Target = dr4.Item(2).ToString()
                        '                        'node.ImageUrl = dr4.Item(3).ToString()
                        '                        node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
                        '                        node.Checkable = False
                        '                        NodeLevel3.Nodes.Add(node)
                        '                        NodeLevel4 = node

                        '                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE MainHierarchy1.ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                        '                        r5 = sqlCommand.ExecuteReader()
                        '                        dt5.Clear()
                        '                        dt5.Load(r5)
                        '                        r5.Close()
                        '                        For Each dr5 As DataRow In dt5.Rows
                        '                            If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
                        '                                node = New RadTreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
                        '                                node.Target = dr5.Item(2).ToString()
                        '                                'node.ImageUrl = dr5.Item(3).ToString()
                        '                                node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
                        '                                node.Checkable = False
                        '                                NodeLevel4.Nodes.Add(node)
                        '                                NodeLevel5 = node

                        '                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE MainHierarchy1.ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                        '                                r6 = sqlCommand.ExecuteReader()
                        '                                dt6.Clear()
                        '                                dt6.Load(r6)
                        '                                r6.Close()
                        '                                For Each dr6 As DataRow In dt6.Rows
                        '                                    If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
                        '                                        node = New RadTreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
                        '                                        node.Target = dr6.Item(2).ToString()
                        '                                        'node.ImageUrl = dr6.Item(3).ToString()
                        '                                        node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
                        '                                        node.Checkable = False
                        '                                        NodeLevel5.Nodes.Add(node)
                        '                                        NodeLevel6 = node

                        '                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE MainHierarchy1.ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                        '                                        r7 = sqlCommand.ExecuteReader()
                        '                                        dt7.Clear()
                        '                                        dt7.Load(r7)
                        '                                        r7.Close()
                        '                                        For Each dr7 As DataRow In dt7.Rows
                        '                                            If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
                        '                                                node = New RadTreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
                        '                                                node.Target = dr7.Item(2).ToString()
                        '                                                'node.ImageUrl = dr7.Item(3).ToString()
                        '                                                node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
                        '                                                node.Checkable = False
                        '                                                NodeLevel6.Nodes.Add(node)
                        '                                                NodeLevel7 = node

                        '                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE MainHierarchy1.ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                        '                                                r8 = sqlCommand.ExecuteReader()
                        '                                                dt8.Clear()
                        '                                                dt8.Load(r8)
                        '                                                r8.Close()
                        '                                                For Each dr8 As DataRow In dt8.Rows
                        '                                                    If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
                        '                                                        node = New RadTreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
                        '                                                        node.Target = dr8.Item(2).ToString()
                        '                                                        'node.ImageUrl = dr8.Item(3).ToString()
                        '                                                        node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
                        '                                                        node.Checkable = False
                        '                                                        NodeLevel7.Nodes.Add(node)
                        '                                                    End If
                        '                                                Next
                        '                                            End If
                        '                                        Next
                        '                                    End If
                        '                                Next
                        '                            End If
                        '                        Next
                        '                    End If
                        '                Next
                        '            End If
                        '        Next
                        '    End If
                        'Next
                    End If
                Next

            Else
                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE (MainHierarchy1.ParentNo iS NULL OR Len(MainHierarchy1.ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No + '  ' + NameE IN ( " + strCategories + " ) AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                r1 = sqlCommand.ExecuteReader()
                dt1.Load(r1)
                r1.Close()

                For Each dr1 As DataRow In dt1.Rows
                    If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
                        node = New RadTreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
                        node.Target = dr1.Item(2).ToString()
                        'node.ImageUrl = dr1.Item(3).ToString()
                        node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
                        node.Checkable = False
                        RadTreeView1.Nodes.Add(node)
                        'NodeLevel1 = node

                        'sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE MainHierarchy1.ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                        'r2 = sqlCommand.ExecuteReader()
                        'dt2.Clear()
                        'dt2.Load(r2)
                        'r2.Close()
                        'For Each dr2 As DataRow In dt2.Rows
                        '    If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
                        '        node = New RadTreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
                        '        node.Target = dr2.Item(2).ToString()
                        '        'node.ImageUrl = dr2.Item(3).ToString()
                        '        node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
                        '        node.Checkable = False
                        '        NodeLevel1.Nodes.Add(node)
                        '        NodeLevel2 = node

                        '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE MainHierarchy1.ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                        '        r3 = sqlCommand.ExecuteReader()
                        '        dt3.Clear()
                        '        dt3.Load(r3)
                        '        r3.Close()
                        '        For Each dr3 As DataRow In dt3.Rows
                        '            If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
                        '                node = New RadTreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
                        '                node.Target = dr3.Item(2).ToString()
                        '                'node.ImageUrl = dr3.Item(3).ToString()
                        '                node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
                        '                node.Checkable = False
                        '                NodeLevel2.Nodes.Add(node)
                        '                NodeLevel3 = node

                        '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE MainHierarchy1.ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                        '                r4 = sqlCommand.ExecuteReader()
                        '                dt4.Clear()
                        '                dt4.Load(r4)
                        '                r4.Close()

                        '                For Each dr4 As DataRow In dt4.Rows
                        '                    If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
                        '                        node = New RadTreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
                        '                        node.Target = dr4.Item(2).ToString()
                        '                        'node.ImageUrl = dr4.Item(3).ToString()
                        '                        node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
                        '                        node.Checkable = False
                        '                        NodeLevel3.Nodes.Add(node)
                        '                        NodeLevel4 = node

                        '                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE MainHierarchy1.ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                        '                        r5 = sqlCommand.ExecuteReader()
                        '                        dt5.Clear()
                        '                        dt5.Load(r5)
                        '                        r5.Close()
                        '                        For Each dr5 As DataRow In dt5.Rows
                        '                            If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
                        '                                node = New RadTreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
                        '                                node.Target = dr5.Item(2).ToString()
                        '                                'node.ImageUrl = dr5.Item(3).ToString()
                        '                                node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
                        '                                node.Checkable = False
                        '                                NodeLevel4.Nodes.Add(node)
                        '                                NodeLevel5 = node

                        '                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE MainHierarchy1.ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                        '                                r6 = sqlCommand.ExecuteReader()
                        '                                dt6.Clear()
                        '                                dt6.Load(r6)
                        '                                r6.Close()
                        '                                For Each dr6 As DataRow In dt6.Rows
                        '                                    If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
                        '                                        node = New RadTreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
                        '                                        node.Target = dr6.Item(2).ToString()
                        '                                        'node.ImageUrl = dr6.Item(3).ToString()
                        '                                        node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
                        '                                        node.Checkable = False
                        '                                        NodeLevel5.Nodes.Add(node)
                        '                                        NodeLevel6 = node

                        '                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE MainHierarchy1.ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                        '                                        r7 = sqlCommand.ExecuteReader()
                        '                                        dt7.Clear()
                        '                                        dt7.Load(r7)
                        '                                        r7.Close()
                        '                                        For Each dr7 As DataRow In dt7.Rows
                        '                                            If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
                        '                                                node = New RadTreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
                        '                                                node.Target = dr7.Item(2).ToString()
                        '                                                'node.ImageUrl = dr7.Item(3).ToString()
                        '                                                node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
                        '                                                node.Checkable = False
                        '                                                NodeLevel6.Nodes.Add(node)
                        '                                                NodeLevel7 = node

                        '                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 INNER JOIN (SELECT ParentNo FROM MainHierarchy1)xxx ON MainHierarchy1.No = xxx.ParentNo WHERE MainHierarchy1.ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                        '                                                r8 = sqlCommand.ExecuteReader()
                        '                                                dt8.Clear()
                        '                                                dt8.Load(r8)
                        '                                                r8.Close()
                        '                                                For Each dr8 As DataRow In dt8.Rows
                        '                                                    If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
                        '                                                        node = New RadTreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
                        '                                                        node.Target = dr8.Item(2).ToString()
                        '                                                        'node.ImageUrl = dr8.Item(3).ToString()
                        '                                                        node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
                        '                                                        node.Checkable = False
                        '                                                        NodeLevel7.Nodes.Add(node)
                        '                                                    End If
                        '                                                Next
                        '                                            End If
                        '                                        Next
                        '                                    End If
                        '                                Next
                        '                            End If
                        '                        Next
                        '                    End If
                        '                Next
                        '            End If
                        '        Next
                        '    End If
                        'Next
                    End If
                Next
            End If

            RadTreeView1.Visible = True
            sqlConnection.Close()
            RadTreeView1.CollapseAllNodes()

        Catch ex As Exception
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "alert('Could not load. connection to database server is lost...');", True)
        End Try
        'End If
        'ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "HideDiv();", True)
    End Sub
    'Protected Sub RadTreeView1_SelectedNodeChanged(sender As Object, e As EventArgs) Handles RadTreeView1.SelectedNodeChanged
    '    Dim node As RadTreeNode
    '    'If (RadTreeView1.Nodes.Count > 0 And RadTreeView1.SelectedNode.Text <> "" And RadTreeView1.SelectedNode.ChildNodes.Count = 0) Then
    '    If (RadTreeView1.SelectedNode.ChildNodes.Count = 0) Then

    '        Try

    '            Dim strClientTypeID As String = "1"
    '            If (Session("ClientTypeID") IsNot Nothing) Then
    '                strClientTypeID = Session("ClientTypeID")
    '            End If

    '            Dim dt As New System.Data.DataTable
    '            Dim arrList() As String = RadTreeView1.SelectedNode.ToolTip.Split(",")
    '            Dim strNodeNumber As String = arrList(arrList.Length - 1)
    '            Dim strLevelNo As Integer = Integer.Parse(arrList(0))
    '            Dim sqlConnection As New SqlConnection()

    '            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
    '            sqlConnection.Open()
    '            Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()

    '            If (UICulture = "Arabic") Then
    '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 m1 WHERE m1.ParentNo = '" + strNodeNumber + "' AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
    '            Else
    '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 m1 WHERE m1.ParentNo = '" + strNodeNumber + "' AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
    '            End If
    '            Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
    '            sqlDataAdapter.Fill(dt)

    '            strLevelNo = strLevelNo + 1
    '            'For i As Integer = 0 To dt.Rows.Count - 1
    '            '    If Not (IsDBNull(dt.Rows(i).Item(0).ToString())) And Not (Trim(dt.Rows(i).Item(0).ToString())) = "" Then
    '            '        node = New RadTreeNode(dt.Rows(i).Item(3).ToString() + " " + dt.Rows(i).Item(1).ToString())
    '            '        node.Target = dt.Rows(i).Item(2).ToString()
    '            '        node.ToolTip = strLevelNo.ToString() + "," + dt.Rows(i).Item(0).ToString() + "," + dt.Rows(i).Item(3).ToString()
    '            '        RadTreeView1.SelectedNode.ChildNodes.Add(node)

    '            '        node.ShowCheckBox = True
    '            '        'If (dt.Rows(i).Item(4).ToString() = "0") Then
    '            '        '    node.ShowCheckBox = True
    '            '        'End If
    '            '    End If
    '            'Next

    '            For Each row In dt.Rows
    '                If Not (IsDBNull(row.Item(0).ToString())) And Not (Trim(row.Item(0).ToString())) = "" Then
    '                    node = New RadTreeNode(row.Item(3).ToString() + " " + row.Item(1).ToString())
    '                    node.Target = row.Item(2).ToString()
    '                    node.ToolTip = strLevelNo.ToString() + "," + row.Item(0).ToString() + "," + row.Item(3).ToString()
    '                    RadTreeView1.SelectedNode.ChildNodes.Add(node)

    '                    node.ShowCheckBox = True
    '                    'If (dt.Rows(i).Item(4).ToString() = "0") Then
    '                    '    node.ShowCheckBox = True
    '                    'End If
    '                End If
    '            Next

    '            For i As Integer = 0 To grdBOQ.Rows.Count - 1
    '                If (grdBOQ.Rows(i).Cells(0).BackColor <> Color.Green) Then
    '                    grdBOQ.Rows(i).Cells(0).BackColor = Color.White
    '                    grdBOQ.Rows(i).Cells(1).BackColor = Color.White
    '                    grdBOQ.Rows(i).Cells(2).BackColor = Color.White
    '                    grdBOQ.Rows(i).Cells(3).BackColor = Color.White
    '                    grdBOQ.Rows(i).Cells(4).BackColor = Color.White
    '                    grdBOQ.Rows(i).Cells(5).BackColor = Color.White
    '                End If
    '            Next

    '            sqlConnection.Close()

    '        Catch ex As Exception
    '            Dim str As String = ex.Message
    '        End Try
    '        'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ScrollToSelectedNode", "ScrollToSelectedNode();", True)
    '    End If

    '    If (hdnVal.Value.Length > 0 Or IsNumeric(hdnVal.Value) = True) Then
    '        Dim Index As Integer = Convert.ToInt32(hdnVal.Value)
    '        If (grdBOQ.Rows(Index - 1).Cells(0).BackColor <> Color.Green) Then
    '            grdBOQ.Rows(Index - 1).Cells(0).BackColor = Color.Yellow
    '            grdBOQ.Rows(Index - 1).Cells(1).BackColor = Color.Yellow
    '            grdBOQ.Rows(Index - 1).Cells(2).BackColor = Color.Yellow
    '            grdBOQ.Rows(Index - 1).Cells(3).BackColor = Color.Yellow
    '            grdBOQ.Rows(Index - 1).Cells(4).BackColor = Color.Yellow
    '            grdBOQ.Rows(Index - 1).Cells(5).BackColor = Color.Yellow
    '        End If
    '    End If
    'End Sub
    Protected Sub LinkButton2_Click(sender As Object, e As EventArgs) Handles LinkButton2.Click
        Dim strFileName As String = "ExcelFile"
        Dim sDateTimeNow As String = DateTime.Now.ToString("yyyyMMddHHmmss")

        If (uplImage.HasFile) Then
            Dim fileExt As String = System.IO.Path.GetExtension(uplImage.FileName).ToLower()
            If (fileExt = ".xls" Or fileExt = ".xlsx") Then

                'ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowDiv();", True)
                strFileName = strFileName + "_" + sDateTimeNow + fileExt

                If (System.IO.File.Exists("../Upload/Excel/" + strFileName)) Then
                    System.IO.File.Delete("../Upload/Excel/" + strFileName)
                End If
                uplImage.SaveAs(Server.MapPath("../Upload/Excel/" + strFileName))

                ImportFromExcel(strFileName)

                'For Each row As GridViewRow In grdBOQ.Rows
                '     row.Attributes.Add("onclick", "setSelected(this);")
                'Next
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('14');", True)
                'ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "HideDiv();", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('13');", True)
                Return
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('13');", True)
            Return
        End If
    End Sub
    'Protected Sub grdBOQ_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBOQ.PageIndexChanging
    '    grdBOQ.SelectedIndex = -1
    '    If (ViewState("BOQDATA") IsNot Nothing) Then
    '        grdBOQ.PageIndex = e.NewPageIndex
    '        grdBOQ.DataSource = ViewState("BOQDATA")
    '        grdBOQ.DataBind()
    '    End If

    '    Dim dtProjectMappedData As System.Data.DataTable = Session("MappedData")

    '    If (dtProjectMappedData IsNot Nothing) Then
    '        For i As Integer = 0 To dtProjectMappedData.Rows.Count - 1
    '            Dim rowIndex As Integer = Convert.ToInt32(dtProjectMappedData.Rows(i).Item(1).ToString())
    '            Dim iMaxIndex As Integer = ((grdBOQ.PageIndex + 1) * 15)
    '            Dim iMinIndex As Integer = iMaxIndex - 14
    '            If (rowIndex >= iMinIndex And rowIndex <= iMaxIndex) Then
    '                If (rowIndex <= 15) Then
    '                    rowIndex = rowIndex - 1
    '                Else
    '                    rowIndex = (rowIndex - ((grdBOQ.PageIndex * 15) + 1))
    '                End If

    '                grdBOQ.Rows(rowIndex).Cells(1).BackColor = Drawing.Color.Green
    '                grdBOQ.Rows(rowIndex).Cells(2).BackColor = Drawing.Color.Green
    '                grdBOQ.Rows(rowIndex).Cells(3).BackColor = Drawing.Color.Green
    '                grdBOQ.Rows(rowIndex).Cells(4).BackColor = Drawing.Color.Green
    '                grdBOQ.Rows(rowIndex).Cells(5).BackColor = Drawing.Color.Green
    '                grdBOQ.Rows(rowIndex).Cells(6).BackColor = Drawing.Color.Green
    '            End If
    '        Next
    '    End If
    'End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'If (RadTreeView1.SelectedNode Is Nothing) Then
        '    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('17');", True)
        '    Return
        'End If
        'If (RadTreeView1.CheckedNodes.Count = 0) Then
        '    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('18');", True)
        '    Return
        'End If
        'If (hdnVal1.Value.Length = 0 Or IsNumeric(hdnVal1.Value) = False) Then
        '    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('19');", True)
        '    Return
        'End If
        'If (hdnVal.Value.Length = 0 Or IsNumeric(hdnVal.Value) = False) Then
        '    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('16');", True)
        '    Return
        'Else
        For i As Integer = 0 To grdBOQ.Rows.Count - 1
            If (grdBOQ.Rows(i).Cells(0).BackColor <> Color.Green) Then
                grdBOQ.Rows(i).Cells(0).BackColor = Color.White
                grdBOQ.Rows(i).Cells(1).BackColor = Color.White
                grdBOQ.Rows(i).Cells(2).BackColor = Color.White
                grdBOQ.Rows(i).Cells(3).BackColor = Color.White
                grdBOQ.Rows(i).Cells(4).BackColor = Color.White
                grdBOQ.Rows(i).Cells(5).BackColor = Color.White
            End If
        Next

        Dim Index As Integer = Convert.ToInt32(hdnVal.Value)
        If (grdBOQ.Rows(Index - 1).Cells(0).BackColor <> Color.Green) Then
            grdBOQ.Rows(Index - 1).Cells(0).BackColor = Color.Yellow
            grdBOQ.Rows(Index - 1).Cells(1).BackColor = Color.Yellow
            grdBOQ.Rows(Index - 1).Cells(2).BackColor = Color.Yellow
            grdBOQ.Rows(Index - 1).Cells(3).BackColor = Color.Yellow
            grdBOQ.Rows(Index - 1).Cells(4).BackColor = Color.Yellow
            grdBOQ.Rows(Index - 1).Cells(5).BackColor = Color.Yellow
        End If
        'End If

        Dim dt As System.Data.DataTable = Session("MappedData")
        'Dim dt1 As System.Data.DataTable = ViewState("BOQDATA")
        Dim dr As DataRow
        Dim drarr() As DataRow
        Dim isnodata As Boolean = False

        'Dim lineNo As Integer = (grdBOQ.SelectedIndex + 1) + (grdBOQ.PageIndex * 15)
        Dim lineNo As Integer = hdnVal.Value
        Dim strLineNo = lineNo.ToString()

        For i As Integer = 0 To RadTreeView1.CheckedNodes.Count - 1

            'Dim strLineItemCode As String = RadTreeView1.SelectedNode.ToolTip.Split(",")(2).ToString()
            Dim strLineItemCode As String = RadTreeView1.CheckedNodes(i).ToolTip.Split(",")(2).ToString()
            Dim strFilterExpression As String = "BOQLineNo = " + strLineNo + " and " + "LineItemCode = " + "'" + strLineItemCode + "'"
            Dim rowFound As Integer = 0

            If (dt Is Nothing) Then
                isnodata = True
            Else
                drarr = dt.Select(strFilterExpression)
                rowFound = drarr.Length
            End If

            If (isnodata = True Or rowFound = 0) Then
                dr = dt.NewRow()
                dr("ProjectID") = Session("ProjectID")
                dr("BOQLineNo") = lineNo 'dt1.Rows(grdBOQ.SelectedIndex).Item(0).ToString()
                dr("LineItemCode") = strLineItemCode
                'txtQty.Text = txtQty.Text.Replace(",", "")
                'dr("Qty") = txtQty.Text
                hdnVal1.Value = hdnVal1.Value.Replace(",", "")
                'dr("Qty") = hdnVal1.Value
                dr("Qty") = txtQty.Text.Trim()

                grdBOQ.SelectedIndex = (Convert.ToInt32(hdnVal.Value) - 1)

                dr(4) = HttpUtility.HtmlDecode(grdBOQ.SelectedRow.Cells(0).Text.Trim())
                dr(5) = HttpUtility.HtmlDecode(grdBOQ.SelectedRow.Cells(1).Text.Trim())
                dr(6) = HttpUtility.HtmlDecode(grdBOQ.SelectedRow.Cells(2).Text.Trim())
                dr(7) = HttpUtility.HtmlDecode(grdBOQ.SelectedRow.Cells(3).Text.Trim())
                dr(8) = HttpUtility.HtmlDecode(grdBOQ.SelectedRow.Cells(4).Text.Trim())
                'dr(9) = HttpUtility.HtmlDecode(grdBOQ.SelectedRow.Cells(6).Text.Trim())

                dt.Rows.Add(dr)
            End If
        Next

        For i As Integer = 0 To RadTreeView1.CheckedNodes.Count - 1
            If (RadTreeView1.CheckedNodes.Count > 0) Then
                RadTreeView1.CheckedNodes(i).Checked = False
                i = i - 1
            End If
        Next
        'RadTreeView1.CheckedNodes.Clear()

        Session("MappedData") = dt
        ViewState("BackColor") = grdBOQ.SelectedRow.BackColor

        grdBOQ.SelectedRow.Cells(0).BackColor = Drawing.Color.Green
        grdBOQ.SelectedRow.Cells(1).BackColor = Drawing.Color.Green
        grdBOQ.SelectedRow.Cells(2).BackColor = Drawing.Color.Green
        grdBOQ.SelectedRow.Cells(3).BackColor = Drawing.Color.Green
        grdBOQ.SelectedRow.Cells(4).BackColor = Drawing.Color.Green
        grdBOQ.SelectedRow.Cells(5).BackColor = Drawing.Color.Green
        hdnMappedRowIndex.Value = grdBOQ.SelectedRow.RowIndex + 1

        Button2_Click(sender, Nothing)

        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('21');", True)
        'End If
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        'Dim sqlTransaction As SqlTransaction = Nothing
        Try
            Dim dt As System.Data.DataTable = Session("MappedData")
            Dim objSqlConfig As New ConfigClassSqlServer()

            If (dt IsNot Nothing And dt.Rows.Count > 0) Then
                Dim sqlConnection As New SqlConnection()
                Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
                sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                sqlConnection.Open()

                'sqlTransaction = sqlConnection.BeginTransaction()
                'sqlCommand.Transaction = sqlTransaction
                sqlCommand.CommandText = "DELETE FROM Project_BOQ_LineItem_Mapping WHERE ProjectID = '" + Session("ProjectID") + "'  DELETE FROM Project_CrewStructure WHERE ProjectID = '" + Session("ProjectID") + "' DELETE FROM Project_BOQ_Data WHERE ProjectID = '" + Session("ProjectID") + "' "
                sqlCommand.ExecuteNonQuery()

                'new deletion lines for supplier quot changes
                sqlCommand.CommandText = "DELETE FROM Project_CostLineItems WHERE ProjectID = '" + Session("ProjectID").ToString() + "' "
                sqlCommand.ExecuteNonQuery()

                'Dim index As Integer = -1
                'Dim previousIndex As Integer = -9
                Dim arrValues() As String
                Dim strValue As String = ""
                Dim strDataType As String = ""

                Session("SuppliersValues") = hdnSuppIDs.Value
                arrValues = Session("SuppliersValues").ToString().Split(",")
                For i As Integer = 0 To dt.Rows.Count - 1

                    For j As Integer = 0 To arrValues.Length - 1
                        If (arrValues(j).ToString().Length > 0 And dt.Rows(i).ItemArray(2).ToString() = arrValues(j).Split(":")(0)) Then
                            strValue = arrValues(j).Split(":")(1)
                            strDataType = arrValues(j).Split(":")(2)
                            Exit For
                        End If
                    Next

                    sqlCommand.CommandText = "INSERT INTO Project_BOQ_LineItem_Mapping(ProjectID, BOQLineNo, LineItemCode, Qty, FilePath, Supp_Quot_ID, DataType) VALUES ('" + Session("ProjectID") + "', '" + dt.Rows(i).Item(1).ToString() + "', '" + dt.Rows(i).Item(2).ToString() + "', '" + dt.Rows(i).Item(3).ToString() + "', '" + ViewState("ExcelFileName").ToString() + "', '" + strValue + "', '" + strDataType + "')"
                    sqlCommand.ExecuteNonQuery()

                    'index = Convert.ToInt32(dt.Rows(i).Item(1).ToString())
                    'index = (index / (grdBOQ.PageIndex + 1)) - 1

                    'If (index <> previousIndex) Then

                    '    sqlCommand.CommandText = "INSERT INTO Project_BOQ_Data(ProjectID, ItemNo, Div, BOQ_No, NameE, NameA, Unit, Qty, BOQ_LineNo) VALUES ('" + Session("ProjectID") + "', '" + dt.Rows(i).Item(2).ToString() + "', '" + dt.Rows(i).Item(4).ToString() + "', '" + dt.Rows(i).Item(5).ToString() + "', '" + dt.Rows(i).Item(6).ToString() + "', N'" + dt.Rows(i).Item(7).ToString() + "', '" + dt.Rows(i).Item(8).ToString() + "',  '" + dt.Rows(i).Item(9).ToString() + "', '" + dt.Rows(i).Item(1).ToString() + "')"
                    '    sqlCommand.ExecuteNonQuery()

                    '    previousIndex = index
                    'End If


                    sqlCommand.CommandText = "INSERT INTO Project_BOQ_Data(ProjectID, ItemNo, Div, BOQ_No, NameE, NameA, Unit, Qty, BOQ_LineNo) VALUES ('" + Session("ProjectID") + "', '" + dt.Rows(i).Item(2).ToString() + "', '" + dt.Rows(i).Item(4).ToString() + "', '" + dt.Rows(i).Item(5).ToString() + "', '" + dt.Rows(i).Item(6).ToString() + "', N'" + dt.Rows(i).Item(7).ToString() + "', '" + dt.Rows(i).Item(8).ToString() + "', '" + dt.Rows(i).Item(3).ToString() + "', '" + dt.Rows(i).Item(1).ToString() + "')"
                    sqlCommand.ExecuteNonQuery()

                    objSqlConfig.SaveProjectData_1(Session("ProjectID").ToString(), dt.Rows(i).Item(2).ToString(), Session("CompanyID"), strValue, strDataType)
                Next

                'For i As Integer = 0 To RadTreeView1.Nodes.Count - 1
                '    For j As Integer = 0 To RadTreeView1.Nodes(i).GetAllNodes().Count - 1
                '        RadTreeView1.Nodes(i).GetAllNodes(j).ch()
                '    Next
                '    RadTreeView1.Nodes(i).Checkable = False
                'Next
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)

                Session("ProjectSaved") = "1"
                'sqlTransaction.Commit()
                sqlConnection.Close()
            Else
                Dim sqlConnection As New SqlConnection()
                Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
                sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                sqlConnection.Open()

                sqlCommand.CommandText = "DELETE FROM Project_BOQ_LineItem_Mapping WHERE ProjectID = '" + Session("ProjectID") + "'  DELETE FROM Project_CrewStructure WHERE ProjectID = '" + Session("ProjectID") + "'  DELETE FROM Project_BOQ_Data WHERE ProjectID = '" + Session("ProjectID") + "' DELETE FROM Project_CostLineItems WHERE ProjectID = '" + Session("ProjectID").ToString() + "' "
                sqlCommand.ExecuteNonQuery()

                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
                sqlConnection.Close()

                Session("ProjectSaved") = "0"
            End If

        Catch ex As Exception
            'sqlTransaction.Rollback()
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "alert('Could not save. connection to database server is lost...'); DisableAllCheckboxes();", True)
        End Try
    End Sub
    'Protected Sub grdBOQ_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdBOQ.SelectedIndexChanged
    '    If (grdBOQ.SelectedIndex >= 0) Then
    '        txtQty.Text = grdBOQ.SelectedRow.Cells(6).Text
    '        'Dim dtProjectMappedData As New System.Data.DataTable
    '        'dtProjectMappedData = Session("MappedData")

    '        'For j As Integer = 0 To dtProjectMappedData.Rows.Count - 1
    '        '    If (dtProjectMappedData.Rows(j).Item(1).ToString() = (grdBOQ.SelectedRow.RowIndex + 1).ToString()) Then
    '        '        txtQty.Text = grdBOQ.SelectedRow.Cells(3).Text
    '        '        For i As Integer = 0 To RadTreeView1.Nodes.Count - 1
    '        '            SearchFromTree(RadTreeView1.Nodes(i), dtProjectMappedData.Rows(j).Item(2).ToString())
    '        '        Next
    '        '    End If
    '        'Next
    '    End If
    'End Sub
    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        'If (grdBOQ.SelectedRow Is Nothing) Then
        '    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('16');", True)
        '    Return
        'End If

        'Dim dt As System.Data.DataTable = Session("MappedData")
        'Dim dr() As DataRow

        'Dim lineNo As Integer = grdBOQ.SelectedIndex + 1
        'Dim strLineNo = lineNo.ToString()

        'dr = dt.Select("BOQLineNo = " + strLineNo)
        'For i As Integer = 0 To dr.Length - 1
        '    dt.Rows.Remove(dr(i))
        'Next

        'Session("MappedData") = dt
        'If (ViewState("BackColor") IsNot Nothing) Then
        '    grdBOQ.SelectedRow.Cells(1).BackColor = ViewState("BackColor")
        '    grdBOQ.SelectedRow.Cells(2).BackColor = ViewState("BackColor")
        '    grdBOQ.SelectedRow.Cells(3).BackColor = ViewState("BackColor")
        'End If
    End Sub
    'Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
    '    Dim xlApp As Microsoft.Office.Interop.Excel.Application
    '    Dim xlBook As Microsoft.Office.Interop.Excel.Workbook
    '    xlApp = CType(CreateObject("Excel.Application"), Microsoft.Office.Interop.Excel.Application)
    '    xlApp.Visible = True
    '    xlBook = xlApp.Workbooks.Open(MapPath("../Upload/Excel/ExcelFile.xlsx"))
    'End Sub
    Private Sub ImportFromExcel(strFileName As String)

        Dim conStr As String = ""
        Dim Extension As String = Path.GetExtension(Server.MapPath("../Upload/Excel/" + strFileName))

        Select Case Extension
            Case ".xls"
                conStr = ConfigurationManager.ConnectionStrings("Excel03ConString").ConnectionString()
                Exit Select
            Case ".xlsx"
                conStr = ConfigurationManager.ConnectionStrings("Excel07ConString").ConnectionString()
                Exit Select
        End Select

        conStr = String.Format(conStr, Server.MapPath("../Upload/Excel/" + strFileName), "Yes")

        Dim connExcel As New OleDbConnection(conStr)
        Dim cmdExcel As New OleDbCommand()
        Dim oda As New OleDbDataAdapter()
        Dim dt, dt1 As New System.Data.DataTable

        cmdExcel.Connection = connExcel
        connExcel.Open()

        Dim dtExcelSchema As System.Data.DataTable

        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
        Dim SheetName As String = ""

        For i As Integer = 0 To dtExcelSchema.Rows.Count - 1
            'Dim index As Integer = dtExcelSchema.Rows(i)("TABLE_NAME").ToString().IndexOf("$")
            'Dim strName As String = dtExcelSchema.Rows(i)("TABLE_NAME").ToString().Substring(index, dtExcelSchema.Rows(i)("TABLE_NAME").ToString().Length - index)
            If (dtExcelSchema.Rows(i)("TABLE_NAME").ToString().Contains("$") = False) Then
                'If (strName.Length > 2) Then
                dtExcelSchema.Rows(i).Delete()
            End If
        Next

        Dim d As Double = 0
        For i As Integer = 0 To dtExcelSchema.Rows.Count - 1
            If (dtExcelSchema.Rows(i).RowState <> DataRowState.Deleted) Then
                SheetName = dtExcelSchema.Rows(i)("TABLE_NAME").ToString()
                cmdExcel.CommandText = "SELECT * From [" & SheetName & "]"
                oda.SelectCommand = cmdExcel
                If (dt.Rows.Count = 0) Then
                    oda.Fill(dt)
                Else
                    dt1.Columns.Clear()
                    dt1.Clear()
                    oda.Fill(dt1)

                    If (dt1.Columns.Count = 11) Then
                        If (dt1.Rows.Count > 0) Then
                            For Each row As DataRow In dt1.Rows
                                If (row.Item(7).ToString().Length > 0 And Double.TryParse(row.Item(7).ToString(), d) = True) Then
                                    dt.ImportRow(row)
                                End If
                            Next

                        End If
                    ElseIf (dt1.Columns.Count = 6) Then
                        If (dt1.Rows.Count > 0) Then
                            For Each row As DataRow In dt1.Rows
                                If (row.Item(4).ToString().Length > 0 And Double.TryParse(row.Item(4).ToString(), d) = True) Then
                                    dt.ImportRow(row)
                                End If
                            Next

                        End If

                    End If

                End If
            End If
        Next

        connExcel.Close()

        If (dt.Columns.Count = 11) Then

            dt.Columns.RemoveAt(4)
            dt.Columns.RemoveAt(4)
            dt.Columns.RemoveAt(4)
            dt.Columns.RemoveAt(5)
            dt.Columns.RemoveAt(6)

            For i As Integer = 0 To dt.Rows.Count - 1
                If (dt.Rows(i).Item(4).ToString().Length = 0 Or Double.TryParse(dt.Rows(i).Item(4).ToString(), d) = False) Then
                    dt.Rows(i).Delete()
                End If
            Next

            dt.Columns.Add("LineNo")
            Dim iLineNo As Integer = 1

            For i As Integer = 0 To dt.Rows.Count - 1
                If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                    dt.Rows(i).Item("LineNo") = iLineNo.ToString()
                    iLineNo = iLineNo + 1
                End If
            Next

            dt.Columns(0).ColumnName = "Div"
            dt.Columns(1).ColumnName = "NameE"
            dt.Columns(2).ColumnName = "Unit"
            dt.Columns(3).ColumnName = "No"
            dt.Columns(4).ColumnName = "Qty"
            dt.Columns(5).ColumnName = "NameA"

            'Dim dtFirstPageData As New System.Data.DataTable
            'dtFirstPageData = dt.Clone()
            'Dim dataRow As DataRow
            'For i As Integer = 0 To 14
            '    dataRow = dt.Rows(i)
            '    dtFirstPageData.ImportRow(dataRow)
            'Next

            'ViewState("RowNumber") = 15
            grdBOQ.DataSource = dt
            grdBOQ.DataBind()

            ViewState("BOQDATA") = dt
            ViewState("ExcelFileName") = strFileName

            Dim dtMappedData As System.Data.DataTable = Session("MappedData")
            dtMappedData.Clear()
            Session("MappedData") = dtMappedData

        ElseIf (dt.Columns.Count = 6) Then

            dt.Columns.Add("NameA")
            For i As Integer = 1 To dt.Rows.Count - 1 Step 2
                If ((i + 1) <= dt.Rows.Count - 1) Then
                    dt.Rows(i + 1).Item("NameA") = dt.Rows(i).Item(2).ToString()
                    dt.Rows(i + 1).Item(4) = dt.Rows(i).Item(4).ToString()
                    dt.Rows(i + 1).Item(3) = dt.Rows(i).Item(3).ToString()
                End If
            Next

            For i As Integer = 0 To dt.Rows.Count - 1
                If ((dt.Rows(i).Item(4).ToString().Length = 0 Or IsNumeric(dt.Rows(i).Item(4).ToString()) = False) Or dt.Rows(i).Item(6).ToString().Length = 0) Then
                    dt.Rows(i).Delete()
                End If
            Next

            dt.Columns.Add("LineNo")
            Dim iLineNo As Integer = 1

            For i As Integer = 0 To dt.Rows.Count - 1
                If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                    dt.Rows(i).Item("LineNo") = iLineNo.ToString()
                    iLineNo = iLineNo + 1
                End If
            Next

            dt.Columns(0).ColumnName = "No"
            dt.Columns(1).ColumnName = "Div"
            dt.Columns(2).ColumnName = "NameE"
            dt.Columns(3).ColumnName = "Unit"
            dt.Columns(4).ColumnName = "Qty"

            'Dim dtFirstPageData As New System.Data.DataTable
            'Dim dataRow As DataRow
            'For i As Integer = 0 To 14
            '    dataRow = dt.Rows(i)
            '    dtFirstPageData.ImportRow(dataRow)
            'Next

            grdBOQ.DataSource = dt
            grdBOQ.DataBind()

            ViewState("BOQDATA") = dt
            ViewState("ExcelFileName") = strFileName

            Dim dtMappedData As System.Data.DataTable = Session("MappedData")
            dtMappedData.Clear()
            Session("MappedData") = dtMappedData
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "alert('Invalid file format...');", True)
        End If
        For Each row As GridViewRow In grdBOQ.Rows
            row.Attributes.Add("onclick", "setSelected(this);")
        Next

        grdBOQ.RowStyle.CssClass = "grid-row"
        grdBOQ.AlternatingRowStyle.CssClass = "alt-row"
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If (Session("IsTemp").ToString().Equals("1")) Then
            Response.Redirect("ProjectCostLineItems.aspx")
        Else
            Dim dt As System.Data.DataTable = Session("MappedData")
            If (dt IsNot Nothing And dt.Rows.Count = 0) Then
                Session("ProjectSaved") = "0"
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('20');", True)
                Return
            End If
            If (Session("ProjectSaved") = "0") Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
            Else
                Response.Redirect("ProjectCostLineItems.aspx")
            End If
        End If

    End Sub
    Protected Sub lnkViewDoc_Click(sender As Object, e As EventArgs) Handles lnkViewDoc.Click
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "document.getElementById('ctl00_MainContent_Button100').click();", True)
    End Sub
    Protected Sub lnkDownload_Click(sender As Object, e As EventArgs) Handles lnkDownload.Click
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "document.getElementById('ctl00_MainContent_Button200').click();", True)
    End Sub
    Protected Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        If (Session("Project_Type").ToString() = "Open") Then
            Response.Redirect("Add_Building.aspx")
        Else
            Response.Redirect("User_Selection.aspx")
        End If
    End Sub
    Protected Sub Button100_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button100.Click
        ViewDocument("")
    End Sub
    Protected Sub Button200_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button200.Click
        ViewDocument("SampleClientBOQ.xlsx")
    End Sub
    Private Sub ViewDocument(strFileName As String)
        If (strFileName.Length > 1) Then
            Dim strFile As String = Server.MapPath("../Upload/Excel/" + strFileName)
            Dim content As Byte() = File.ReadAllBytes(strFile)
            Dim context As HttpContext = HttpContext.Current
            Dim strAttachment As String = "attachment; filename=" + strFile

            context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            context.Response.BinaryWrite(content)
            context.Response.AppendHeader("Content-Disposition", strAttachment)
            context.Response.End()
        Else
            Dim dt As New System.Data.DataTable
            Dim sqlConnection As New SqlConnection()
            Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
            Dim da As New SqlDataAdapter()
            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
            sqlConnection.Open()

            sqlCommand.CommandText = "SELECT FilePath FROM Project_BOQ_LineItem_Mapping WHERE ProjectID = '" + Session("ProjectID") + "' "
            da.SelectCommand = sqlCommand
            da.Fill(dt)

            If (dt.Rows.Count > 0) Then
                Dim strFile As String = Server.MapPath("../Upload/Excel/" + dt.Rows(0).Item(0).ToString())
                Dim content As Byte() = File.ReadAllBytes(strFile)
                Dim context As HttpContext = HttpContext.Current
                Dim strAttachment As String = "attachment; filename=" + strFile

                context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                context.Response.BinaryWrite(content)
                context.Response.AppendHeader("Content-Disposition", strAttachment)
                context.Response.End()
            End If

            sqlConnection.Close()
        End If
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "DisableAllCheckboxes();", True)
    End Sub
    'Private Sub LoadTreeThread()
    '    Dim strClientTypeID As String = "1"
    '    If (Session("ClientTypeID") IsNot Nothing) Then
    '        strClientTypeID = Session("ClientTypeID")
    '    End If

    '    'If (hdnInitialLoad.Value.ToString().Equals("1")) Then
    '    Dim sqlConnection As New SqlConnection()
    '    Dim node As RadTreeNode = Nothing
    '    Dim NodeLevel1, NodeLevel2, NodeLevel3, NodeLevel4, NodeLevel5, NodeLevel6, NodeLevel7 As RadTreeNode
    '    'Dim NodeLevel1 As RadTreeNode
    '    RadTreeView1.Nodes.Clear()

    '    Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
    '    sqlCommand.CommandTimeout = 120000
    '    Dim r1, r2, r3, r4, r5, r6, r7, r8 As SqlDataReader
    '    'Dim r1 As SqlDataReader
    '    Dim dt1, dt2, dt3, dt4, dt5, dt6, dt7, dt8 As New System.Data.DataTable
    '    'Dim dt1 As New System.Data.DataTable

    '    sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
    '    sqlConnection.Open()

    '    If (UICulture = "Arabic") Then
    '        sqlCommand.CommandText = "SELECT DISTINCT ID,  NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '        r1 = sqlCommand.ExecuteReader()
    '        dt1.Load(r1)
    '        r1.Close()

    '        For Each dr1 As DataRow In dt1.Rows
    '            If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
    '                node = New RadTreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
    '                node.Target = dr1.Item(2).ToString()
    '                'node.ImageUrl = dr1.Item(3).ToString()
    '                node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
    '                RadTreeView1.Nodes.Add(node)
    '                'NodeLevel1 = node

    '                'sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                'r2 = sqlCommand.ExecuteReader()
    '                'dt2.Clear()
    '                'dt2.Load(r2)
    '                'r2.Close()
    '                'For Each dr2 As DataRow In dt2.Rows
    '                '    If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
    '                '        node = New RadTreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
    '                '        node.Target = dr2.Item(2).ToString()
    '                '        'node.ImageUrl = dr2.Item(3).ToString() 
    '                '        node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
    '                '        NodeLevel1.ChildNodes.Add(node)
    '                '        NodeLevel2 = node

    '                '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '        r3 = sqlCommand.ExecuteReader()
    '                '        dt3.Clear()
    '                '        dt3.Load(r3)
    '                '        r3.Close()
    '                '        For Each dr3 As DataRow In dt3.Rows
    '                '            If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
    '                '                node = New RadTreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
    '                '                node.Target = dr3.Item(2).ToString()
    '                '                'node.ImageUrl = dr3.Item(3).ToString()
    '                '                node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
    '                '                NodeLevel2.ChildNodes.Add(node)
    '                '                NodeLevel3 = node

    '                '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                r4 = sqlCommand.ExecuteReader()
    '                '                dt4.Clear()
    '                '                dt4.Load(r4)
    '                '                r4.Close()

    '                '                For Each dr4 As DataRow In dt4.Rows
    '                '                    If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
    '                '                        node = New RadTreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
    '                '                        node.Target = dr4.Item(2).ToString()
    '                '                        'node.ImageUrl = dr4.Item(3).ToString()
    '                '                        node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
    '                '                        NodeLevel3.ChildNodes.Add(node)
    '                '                        NodeLevel4 = node

    '                '                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                        r5 = sqlCommand.ExecuteReader()
    '                '                        dt5.Clear()
    '                '                        dt5.Load(r5)
    '                '                        r5.Close()
    '                '                        For Each dr5 As DataRow In dt5.Rows
    '                '                            If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
    '                '                                node = New RadTreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
    '                '                                node.Target = dr5.Item(2).ToString()
    '                '                                'node.ImageUrl = dr5.Item(3).ToString()
    '                '                                node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
    '                '                                NodeLevel4.ChildNodes.Add(node)
    '                '                                NodeLevel5 = node

    '                '                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                                r6 = sqlCommand.ExecuteReader()
    '                '                                dt6.Clear()
    '                '                                dt6.Load(r6)
    '                '                                r6.Close()
    '                '                                For Each dr6 As DataRow In dt6.Rows
    '                '                                    If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
    '                '                                        node = New RadTreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
    '                '                                        node.Target = dr6.Item(2).ToString()
    '                '                                        'node.ImageUrl = dr6.Item(3).ToString()
    '                '                                        node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
    '                '                                        NodeLevel5.ChildNodes.Add(node)
    '                '                                        NodeLevel6 = node

    '                '                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                                        r7 = sqlCommand.ExecuteReader()
    '                '                                        dt7.Clear()
    '                '                                        dt7.Load(r7)
    '                '                                        r7.Close()
    '                '                                        For Each dr7 As DataRow In dt7.Rows
    '                '                                            If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
    '                '                                                node = New RadTreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
    '                '                                                node.Target = dr7.Item(2).ToString()
    '                '                                                'node.ImageUrl = dr7.Item(3).ToString()
    '                '                                                node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
    '                '                                                NodeLevel6.ChildNodes.Add(node)
    '                '                                                NodeLevel7 = node

    '                '                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                '                                                r8 = sqlCommand.ExecuteReader()
    '                '                                                dt8.Clear()
    '                '                                                dt8.Load(r8)
    '                '                                                r8.Close()
    '                '                                                For Each dr8 As DataRow In dt8.Rows
    '                '                                                    If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
    '                '                                                        node = New RadTreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
    '                '                                                        node.Target = dr8.Item(2).ToString()
    '                '                                                        'node.ImageUrl = dr8.Item(3).ToString()
    '                '                                                        node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
    '                '                                                        NodeLevel7.ChildNodes.Add(node)
    '                '                                                    End If
    '                '                                                Next
    '                '                                            End If
    '                '                                        Next
    '                '                                    End If
    '                '                                Next
    '                '                            End If
    '                '                        Next
    '                '                    End If
    '                '                Next
    '                '            End If
    '                '        Next
    '                '    End If
    '                'Next
    '            End If
    '        Next

    '    Else
    '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '        r1 = sqlCommand.ExecuteReader()
    '        dt1.Load(r1)
    '        r1.Close()

    '        For Each dr1 As DataRow In dt1.Rows
    '            If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
    '                node = New RadTreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
    '                node.Target = dr1.Item(2).ToString()
    '                'node.ImageUrl = dr1.Item(3).ToString()
    '                node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
    '                RadTreeView1.Nodes.Add(node)
    '                NodeLevel1 = node

    '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                r2 = sqlCommand.ExecuteReader()
    '                dt2.Clear()
    '                dt2.Load(r2)
    '                r2.Close()
    '                For Each dr2 As DataRow In dt2.Rows
    '                    If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
    '                        node = New RadTreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
    '                        node.Target = dr2.Item(2).ToString()
    '                        'node.ImageUrl = dr2.Item(3).ToString()
    '                        node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
    '                        NodeLevel1.ChildNodes.Add(node)
    '                        NodeLevel2 = node

    '                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                        r3 = sqlCommand.ExecuteReader()
    '                        dt3.Clear()
    '                        dt3.Load(r3)
    '                        r3.Close()
    '                        For Each dr3 As DataRow In dt3.Rows
    '                            If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
    '                                node = New RadTreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
    '                                node.Target = dr3.Item(2).ToString()
    '                                'node.ImageUrl = dr3.Item(3).ToString()
    '                                node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
    '                                NodeLevel2.ChildNodes.Add(node)
    '                                NodeLevel3 = node

    '                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                                r4 = sqlCommand.ExecuteReader()
    '                                dt4.Clear()
    '                                dt4.Load(r4)
    '                                r4.Close()

    '                                For Each dr4 As DataRow In dt4.Rows
    '                                    If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
    '                                        node = New RadTreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
    '                                        node.Target = dr4.Item(2).ToString()
    '                                        'node.ImageUrl = dr4.Item(3).ToString()
    '                                        node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
    '                                        NodeLevel3.ChildNodes.Add(node)
    '                                        NodeLevel4 = node

    '                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                                        r5 = sqlCommand.ExecuteReader()
    '                                        dt5.Clear()
    '                                        dt5.Load(r5)
    '                                        r5.Close()
    '                                        For Each dr5 As DataRow In dt5.Rows
    '                                            If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
    '                                                node = New RadTreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
    '                                                node.Target = dr5.Item(2).ToString()
    '                                                'node.ImageUrl = dr5.Item(3).ToString()
    '                                                node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
    '                                                NodeLevel4.ChildNodes.Add(node)
    '                                                NodeLevel5 = node

    '                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                                                r6 = sqlCommand.ExecuteReader()
    '                                                dt6.Clear()
    '                                                dt6.Load(r6)
    '                                                r6.Close()
    '                                                For Each dr6 As DataRow In dt6.Rows
    '                                                    If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
    '                                                        node = New RadTreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
    '                                                        node.Target = dr6.Item(2).ToString()
    '                                                        'node.ImageUrl = dr6.Item(3).ToString()
    '                                                        node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
    '                                                        NodeLevel5.ChildNodes.Add(node)
    '                                                        NodeLevel6 = node

    '                                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                                                        r7 = sqlCommand.ExecuteReader()
    '                                                        dt7.Clear()
    '                                                        dt7.Load(r7)
    '                                                        r7.Close()
    '                                                        For Each dr7 As DataRow In dt7.Rows
    '                                                            If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
    '                                                                node = New RadTreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
    '                                                                node.Target = dr7.Item(2).ToString()
    '                                                                'node.ImageUrl = dr7.Item(3).ToString()
    '                                                                node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
    '                                                                NodeLevel6.ChildNodes.Add(node)
    '                                                                NodeLevel7 = node

    '                                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                                                                r8 = sqlCommand.ExecuteReader()
    '                                                                dt8.Clear()
    '                                                                dt8.Load(r8)
    '                                                                r8.Close()
    '                                                                For Each dr8 As DataRow In dt8.Rows
    '                                                                    If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
    '                                                                        node = New RadTreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
    '                                                                        node.Target = dr8.Item(2).ToString()
    '                                                                        'node.ImageUrl = dr8.Item(3).ToString()
    '                                                                        node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
    '                                                                        NodeLevel7.ChildNodes.Add(node)
    '                                                                    End If
    '                                                                Next
    '                                                            End If
    '                                                        Next
    '                                                    End If
    '                                                Next
    '                                            End If
    '                                        Next
    '                                    End If
    '                                Next
    '                            End If
    '                        Next
    '                    End If
    '                Next
    '            End If
    '        Next
    '    End If

    '    RadTreeView1.Visible = True
    '    sqlConnection.Close()
    '    RadTreeView1.CollapseAll()
    '    'End If
    '    'ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "HideDiv();", True)
    'End Sub
    Protected Sub grdBOQ_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdBOQ.RowDeleting
        Dim dt As New System.Data.DataTable
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim rowIndex As Integer = e.RowIndex

        rowIndex = rowIndex + ((grdBOQ.PageIndex * 15) + 1)

        dt = objSqlConfig.Get_BOQ_Mappings(Session("ProjectID"), (rowIndex).ToString())

        Session("Div") = HttpUtility.HtmlDecode(grdBOQ.Rows(e.RowIndex).Cells(1).Text)
        Session("No") = HttpUtility.HtmlDecode(grdBOQ.Rows(e.RowIndex).Cells(2).Text)
        Session("NameE") = HttpUtility.HtmlDecode(grdBOQ.Rows(e.RowIndex).Cells(3).Text)
        Session("NameA") = HttpUtility.HtmlDecode(grdBOQ.Rows(e.RowIndex).Cells(4).Text)
        Session("Unit") = HttpUtility.HtmlDecode(grdBOQ.Rows(e.RowIndex).Cells(5).Text)
        Session("Qty") = HttpUtility.HtmlDecode(grdBOQ.Rows(e.RowIndex).Cells(6).Text)
        Session("BOQLineNo") = rowIndex

        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "Showopoup('BOQ_Mapping.aspx');", True)
    End Sub
    Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        'Response.Redirect("ClientBOQ.aspx")
        Dim dtProjectMappedData As New System.Data.DataTable
        dtProjectMappedData = Session("MappedData")
        hdnSuppIDs.Value = Session("SuppliersValues").ToString()

        'If (ViewState("BackColor") IsNot Nothing) Then
        '    For i As Integer = 0 To grdBOQ.Rows.Count - 1
        '        'grdBOQ.Rows(i).Cells(1).BackColor = ViewState("BackColor")
        '        'grdBOQ.Rows(i).Cells(2).BackColor = ViewState("BackColor")
        '        'grdBOQ.Rows(i).Cells(3).BackColor = ViewState("BackColor")
        '        'grdBOQ.Rows(i).Cells(4).BackColor = ViewState("BackColor")
        '        'grdBOQ.Rows(i).Cells(5).BackColor = ViewState("BackColor")
        '        'grdBOQ.Rows(i).Cells(6).BackColor = ViewState("BackColor")

        '        grdBOQ.Rows(i).Cells(1).BackColor = Color.White
        '        grdBOQ.Rows(i).Cells(2).BackColor = Color.White
        '        grdBOQ.Rows(i).Cells(3).BackColor = Color.White
        '        grdBOQ.Rows(i).Cells(4).BackColor = Color.White
        '        grdBOQ.Rows(i).Cells(5).BackColor = Color.White
        '        grdBOQ.Rows(i).Cells(6).BackColor = Color.White
        '    Next
        'End If

        For i As Integer = 0 To grdBOQ.Rows.Count - 1
            grdBOQ.Rows(i).Cells(0).BackColor = Color.White
            grdBOQ.Rows(i).Cells(1).BackColor = Color.White
            grdBOQ.Rows(i).Cells(2).BackColor = Color.White
            grdBOQ.Rows(i).Cells(3).BackColor = Color.White
            grdBOQ.Rows(i).Cells(4).BackColor = Color.White
            grdBOQ.Rows(i).Cells(5).BackColor = Color.White
        Next
        For i As Integer = 0 To dtProjectMappedData.Rows.Count - 1
            Dim rowIndex As Integer = Convert.ToInt32(dtProjectMappedData.Rows(i).Item(1).ToString())
            rowIndex = rowIndex - 1
            grdBOQ.Rows(rowIndex).Cells(0).BackColor = Drawing.Color.Green
            grdBOQ.Rows(rowIndex).Cells(1).BackColor = Drawing.Color.Green
            grdBOQ.Rows(rowIndex).Cells(2).BackColor = Drawing.Color.Green
            grdBOQ.Rows(rowIndex).Cells(3).BackColor = Drawing.Color.Green
            grdBOQ.Rows(rowIndex).Cells(4).BackColor = Drawing.Color.Green
            grdBOQ.Rows(rowIndex).Cells(5).BackColor = Drawing.Color.Green
        Next

        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "DisableAllCheckboxes('');", True)
    End Sub
    <WebMethod()> _
    Public Shared Function GetNodesData(nodeNumber As String, compID As Integer, langID As Integer, clientType As Integer) As String
        Dim obj = New ClientBOQ()
        Dim query As String = "[sp_GetNodesData]"
        Dim cmd As New SqlCommand(query)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@NodeNo", nodeNumber)
        cmd.Parameters.AddWithValue("@CompID", compID)
        cmd.Parameters.AddWithValue("@LangID", langID)
        cmd.Parameters.AddWithValue("@ClientTypeID", clientType)
        Return GetData(cmd, nodeNumber).GetXml()
    End Function
    <WebMethod()> _
    Public Shared Function GetNodesData1(nodeNumber As String, compID As Integer) As String
        Dim obj = New ClientBOQ()
        Dim query As String = "[sp_GetNodesData1]"
        Dim cmd As New SqlCommand(query)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@NodeNo", nodeNumber)
        cmd.Parameters.AddWithValue("@CompID", compID)
        Return GetData(cmd, nodeNumber).GetXml()
    End Function
    Private Shared Function GetData(cmd As SqlCommand, nodeNo As String) As DataSet
        Dim strConnString As String = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        Using con As New SqlConnection(strConnString)
            Using sda As New SqlDataAdapter()
                cmd.Connection = con
                sda.SelectCommand = cmd
                Using ds As New DataSet()
                    sda.Fill(ds, "Nodes")
                    Return ds
                End Using
            End Using
        End Using
    End Function
    Protected Sub grdBOQ_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdBOQ.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            For Each link As LinkButton In e.Row.Cells(6).Controls.OfType(Of LinkButton)()
                link.Attributes("onclick") = "return checkMapping(this);"
            Next
        End If
    End Sub
    Private Function GetSelectedRecord() As String
        Dim retValue = "-1"
        For i As Integer = 0 To grdData.Rows.Count - 1
            Dim rb As RadioButton = DirectCast(grdData.Rows(i).Cells(10).FindControl("RadioButton1"), RadioButton)
            If rb IsNot Nothing Then
                If rb.Checked Then
                    Dim hf As HiddenField = DirectCast(grdData.Rows(i).Cells(10).FindControl("HiddenField1"), HiddenField)
                    If hf IsNot Nothing Then
                        retValue = hf.Value
                    End If
                    If (grdData.Rows(i).Cells(8).Text = "System Default" Or grdData.Rows(i).Cells(8).Text = "نظام افتراضي") Then
                        retValue = retValue + ":" + "0"
                    Else
                        retValue = retValue + ":" + "1"
                    End If
                    Exit For
                End If
            End If
        Next
        Return retValue
    End Function
    Private Sub SetSelectedRecord(strID As String)
        For i As Integer = 0 To grdData.Rows.Count - 1
            Dim rb As RadioButton = DirectCast(grdData.Rows(i).Cells(10).FindControl("RadioButton1"), RadioButton)
            If rb IsNot Nothing Then
                Dim hf As HiddenField = DirectCast(grdData.Rows(i).Cells(10).FindControl("HiddenField1"), HiddenField)
                If hf IsNot Nothing Then
                    If hf.Value.Equals(strID) Then
                        rb.Checked = True
                        Exit For
                    Else
                        rb.Checked = False
                    End If
                End If
            End If
        Next
    End Sub
    Private Sub ShowSuppliersData()
        Dim dt As New DataTable()
        Dim sqlConnection As New SqlConnection()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim sqlDataAdapter As New SqlDataAdapter()
        If (RadTreeView1.SelectedNode.Nodes.Count = 0) Then
            grdData.Columns(8).Visible = True
            grdData.Columns(9).Visible = True
            grdData.Columns(10).Visible = True

            If (UICulture = "Arabic") Then
                grdData.Columns(8).Visible = False
            Else
                grdData.Columns(9).Visible = False
            End If

            Dim strCostLineItemNo As String = RadTreeView1.SelectedNode.ToolTip.Split(",")(2)
            sqlCommand.CommandText = "SELECT s.ID, s.CostLineItemNo No, MainHierarchy1.NameE, MainHierarchy1.NameA, s.Unit, s.CrewCode, s.DO, s.Cost, s.MaterialCost, sup.NameE SuppNameE, sup.NameA SuppNameA FROM Supplier_Quot s LEFT JOIN Suppliers sup ON s.SuppID = sup.ID LEFT JOIN MainHierarchy1 ON s.CostLineItemNo = No AND CompID = 0 WHERE CostLineItemNo = '" + strCostLineItemNo + "' UNION ALL SELECT m.ID, m.[No] No, m.NameE, m.NameA, m.Unit, m.CrewCode, m.DO, m.Cost, m.MaterialCost, 'System Default' SuppNameE, 'نظام افتراضي' SuppNameA FROM MainHierarchy1 m WHERE m.CompID = '" + Session("CompanyID") + "' AND No = '" + strCostLineItemNo + "' "
            sqlDataAdapter.SelectCommand = sqlCommand
            sqlDataAdapter.Fill(dt)

            If (dt.Rows.Count > 0) Then
                grdData.DataSource = dt
                grdData.DataBind()

                Dim rb As RadioButton = DirectCast(grdData.Rows(0).Cells(10).FindControl("RadioButton1"), RadioButton)
                rb.Checked = True
                ViewState("Lines_Data") = dt
            Else
                grdData.DataSource = Nothing
                grdData.DataBind()
            End If
        Else
            grdData.Columns(8).Visible = False
            grdData.Columns(9).Visible = False
            grdData.Columns(10).Visible = False
        End If

        sqlConnection.Close()
    End Sub
    'Protected Sub RadTreeView1_NodeCheck(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadTreeNodeEventArgs) Handles RadTreeView1.NodeCheck
    '    Session("ProjectSaved") = "0"
    '    Dim strValue = String.Empty
    '    Dim arrValue() As String
    '    Dim strExistingvalues As String = ""
    '    Dim strNewValue As String = ""
    '    Dim iIndex As Integer = -1

    '    e.Node.Selected = True
    '    ShowSuppliersData()

    '    Dim index As Integer = 0
    '    If (e.Node.Checked = True) Then
    '        'If (ViewState("Selected_Supplier_Quotes") IsNot Nothing) Then
    '        If (hdnSuppIDs.Value.Length > 0) Then

    '            arrValue = hdnSuppIDs.Value.Split(",")
    '            For i As Integer = 0 To arrValue.Length - 1
    '                If (e.Node.ToolTip.Split(",")(2).ToString() = arrValue(i).Split(":")(0)) Then
    '                    index = 1
    '                End If
    '            Next
    '            If (index = 0) Then
    '                'strExistingvalues = ViewState("Selected_Supplier_Quotes").ToString()
    '                strExistingvalues = hdnSuppIDs.Value
    '                strValue = strExistingvalues + e.Node.ToolTip.Split(",")(2).ToString() + ":"
    '                strValue = strValue + GetSelectedRecord() + ","
    '                'ViewState("Selected_Supplier_Quotes") = strValue
    '                hdnSuppIDs.Value = strValue
    '            End If

    '        Else
    '            strValue = e.Node.ToolTip.Split(",")(2).ToString() + ":"
    '            strValue = strValue + GetSelectedRecord() + ","
    '            'ViewState("Selected_Supplier_Quotes") = strValue
    '            hdnSuppIDs.Value = strValue
    '        End If
    '    Else
    '        'If (ViewState("Selected_Supplier_Quotes") IsNot Nothing) Then
    '        If (hdnSuppIDs.Value.Length > 0) Then
    '            'strValue = ViewState("Selected_Supplier_Quotes").ToString()
    '            strValue = hdnSuppIDs.Value
    '            arrValue = strValue.Split(",")
    '            For i As Integer = 0 To arrValue.Length - 1
    '                If (e.Node.ToolTip.Split(",")(2).ToString() = arrValue(i).Split(":")(0)) Then
    '                    iIndex = i
    '                End If
    '            Next
    '            For i As Integer = 0 To arrValue.Length - 1
    '                If (i <> iIndex And arrValue(i).ToString().Length > 0) Then
    '                    strNewValue = strNewValue + arrValue(i).ToString() + ","
    '                End If
    '            Next
    '            'ViewState("Selected_Supplier_Quotes") = strNewValue
    '            hdnSuppIDs.Value = strNewValue
    '        End If
    '    End If
    'End Sub
    'Protected Sub RadTreeView1_NodeClick(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadTreeNodeEventArgs) Handles RadTreeView1.NodeClick

    '    Dim strClientTypeID As String = "1"
    '    If (Session("ClientTypeID") IsNot Nothing) Then
    '        strClientTypeID = Session("ClientTypeID")
    '    End If

    '    Dim dt As New System.Data.DataTable
    '    Dim arrList() As String = RadTreeView1.SelectedNode.ToolTip.Split(",")
    '    Dim strNodeNumber As String = arrList(arrList.Length - 1)
    '    Dim strLevelNo As Integer = Integer.Parse(arrList(0))
    '    Dim sqlConnection As New SqlConnection()

    '    sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
    '    sqlConnection.Open()
    '    Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()

    '    If (UICulture = "Arabic") Then
    '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No, Unit, CrewCode, DO, Cost, MaterialCost, '' SuppNameE, '' SuppNameA FROM MainHierarchy1 WHERE ParentNo = '" + strNodeNumber + "' AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
    '    Else
    '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No, Unit, CrewCode, DO, Cost, MaterialCost, '' SuppNameE, '' SuppNameA FROM MainHierarchy1 WHERE ParentNo = '" + strNodeNumber + "' AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
    '    End If
    '    Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
    '    sqlDataAdapter.Fill(dt)

    '    grdData.DataSource = dt
    '    grdData.DataBind()
    '    ViewState("Lines_Data") = dt

    '    ShowSuppliersData()

    '    Dim arrValue() As String = hdnSuppIDs.Value.Split(",")
    '    For i As Integer = 0 To arrValue.Length - 1
    '        If (arrValue(i).ToString().Length > 0 And RadTreeView1.SelectedNode.ToolTip.Split(",")(2) = arrValue(i).Split(":")(0).ToString()) Then
    '            SetSelectedRecord(arrValue(i).Split(":")(1).ToString())
    '        End If
    '    Next

    '    sqlConnection.Close()
    'End Sub
    Private Sub FillGrid()
        Dim Data As DataTable = ViewState("Lines_Data")
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp") Is Nothing) Then
                If ViewState("SortDirection").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp") & " " & "desc"
                End If

                grdData.DataSource = dv
                grdData.DataBind()
            End If
        End If
    End Sub
    Protected Sub grdData_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdData.Sorting
        If (ViewState("SortState") IsNot Nothing And ViewState("SortState") = SortDirection.Ascending) Then
            ViewState("SortDirection") = "asc"
            ViewState("SortState") = 1
        Else
            ViewState("SortDirection") = "desc"
            ViewState("SortState") = 0
        End If
        ViewState("SortExp") = e.SortExpression
        FillGrid()
    End Sub
End Class