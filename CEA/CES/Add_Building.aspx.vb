﻿Public Class Add_Building
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            'Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
            'Menuhide.Value = "close"
            Dim linkButton As LinkButton = Master.FindControl("LinkButton1")
            linkButton.Visible = False
            Session("ProjectSaved") = "0"
            lblProjectName.Text = Session("ProjectName")
            If (Request.IsAuthenticated) Then
                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
            If (Session("BuildingName") IsNot Nothing) Then
                txtBuildingName.Text = Session("BuildingName")
                Session("ProjectSaved") = "1"
            End If
            LoadGrid()

            txtBuildingName.ReadOnly = True
            If (Session("User_Role_ID") = "4" Or Session("User_Role_ID") = "999" Or Session("User_Role_ID") = "888") Then
                txtBuildingName.ReadOnly = False
            End If

            If (Session("IsTemp").ToString().Equals("1")) Then
                txtBuildingName.ReadOnly = True
            End If

            If (Session("User_Role_ID") = "999") Then
                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (Session("User_Role_ID").ToString().Contains("999")) Then
                    If (menuItems.Count > 15) Then
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                    End If
                ElseIf (Session("User_Role_ID").ToString().Contains("888")) Then
                    If (menuItems.Count > 5) Then
                        menuItems.RemoveAt(0)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                    End If
                End If
            Else
                Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                Menuhide.Value = "close"
            End If
            txtBuildingName.Focus()
        End If
    End Sub
    Protected Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtBuildingName.TextChanged
        Session("ProjectSaved") = "0"
    End Sub
    Protected Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        If (Session("User_Role_ID").ToString() = "999") Then
            Response.Redirect("~/Admin/Template_Project.aspx")
        Else
            Response.Redirect("Project_Home.aspx")
        End If
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim retValue As String = String.Empty
        Dim strlangID As String = "1"
        If (UICulture = "Arabic") Then
            strlangID = 2
        End If
        If (txtBuildingName.Text.Trim.Length = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
            txtBuildingName.Focus()
        Else
            Dim strCreationDate As String = Now.ToString("yyyyMMddHHmmss")
            'If (Session("AddBuilding") Is Nothing) Then
            '    If (Session("BuildingName") Is Nothing) Then
            '        retValue = objSqlConfig.Update_Building_Name("", Session("ProjectName"), txtBuildingName.Text.Trim(), "")
            '        'Else
            '        'retValue = objSqlConfig.Update_Building_Name(Session("BuildingName"), Session("ProjectName"), txtBuildingName.Text.Trim(), Session("ProjectID"))
            '    End If
            'Else
            '    'Dim strCreationDate As String = Now.ToString("yyyyMMddHHmmss")
            '    retValue = objSqlConfig.AddNewBuilding(Session("ProjectName"), txtBuildingName.Text.Trim(), Session("ProjectID"), strCreationDate, Session("UserName"), strlangID)
            '    If (retValue <> "-1") Then
            '        Session("ProjectID") = retValue
            '    End If
            'End If

            'If (retValue = "-1") Then
            '    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
            '    Session("ProjectSaved") = "0"
            'Else
            '    'ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
            '    Session("BuildingName") = txtBuildingName.Text.Trim()
            '    Session("ProjectSaved") = "1"
            '    Session("AddBuilding") = Nothing
            'End If

            'new code to add new building when project type is open
            'If (Session("Project_Type") = "Open") Then

            retValue = objSqlConfig.CheckBuildingExistence(Session("ProjectName"), txtBuildingName.Text.Trim(), Session("ProjectID"), strCreationDate, Session("UserName"), strlangID, Session("CompanyID"))

            If (retValue <> "-1") Then
                Session("ProjectID") = retValue
                Session("BuildingName") = txtBuildingName.Text.Trim()
                Session("AddBuilding") = Nothing
                grdBuildings.SelectedIndex = -1
            Else
                Session("BuildingName") = txtBuildingName.Text.Trim()
                For i As Integer = 0 To grdBuildings.Rows.Count - 1
                    If (txtBuildingName.Text.Trim() = HttpUtility.HtmlDecode(grdBuildings.Rows(i).Cells(1).Text.Trim())) Then
                        grdBuildings.SelectedIndex = i
                        Session("ProjectID") = grdBuildings.SelectedDataKey(0).ToString()
                        Exit For
                    End If
                Next
            End If
            'End If

            'If (Session("ProjectSaved") = "1") Then
            Session("ProjectSaved") = "1"
            LoadGrid()
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
            'End If
        End If
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If (Session("IsTemp").ToString().Equals("0") Or Session("Project_Type") <> "Open") Then
            Button1_Click(sender, Nothing)
        End If
        If (Session("IsTemp").ToString().Equals("1")) Then
            Response.Redirect("User_Selection.aspx")
        Else
            If (Session("ProjectSaved").Equals("0")) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
            Else
                If (Session("Project_Type") = "Open") Then
                    Dim objSqlConfig As New ConfigClassSqlServer()
                    If (objSqlConfig.GetProjectCreatedByUser(Session("ProjectID")) = Session("UserName")) Then
                        Response.Redirect("User_Selection.aspx")
                    Else
                        If (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
                            Response.Redirect("ClientBOQ.aspx")
                        Else
                            Response.Redirect("Add_Categories.aspx")
                        End If
                    End If
                Else
                    Response.Redirect("User_Selection.aspx")
                End If
            End If
        End If
    End Sub
    Private Sub LoadGrid()
        grdBuildings.Enabled = True
        txtBuildingName.ReadOnly = False
        Dim objSqlConfig As New ConfigClassSqlServer()
        grdBuildings.DataSource = Nothing
        grdBuildings.DataBind()

        'grdBuildings.DataSource = objSqlConfig.GetBuildingsByProject(Session("ProjectName"), Session("UserID"))
        grdBuildings.DataSource = objSqlConfig.GetBuildingsByProject(Session("ProjectName"), Session("CompanyID"), Session("Project_Type"), Session("User_Role_ID"), Session("UserID"))
        grdBuildings.DataBind()

        Button1.Enabled = True
        If (Session("Project_Type").ToString().Equals("New") Or Session("Project_Type").ToString().Equals("BOQ")) Then
            grdBuildings.Enabled = False
            grdBuildings.Visible = False
        ElseIf (Session("Project_Type").ToString().Equals("Open")) Then
            'txtBuildingName.ReadOnly = True
            grdBuildings.Visible = True
            'Button1.Enabled = False
        ElseIf (Session("Project_Type").ToString().Equals("Existing")) Then
            txtBuildingName.ReadOnly = False
            grdBuildings.Visible = True
        End If
        If (Session("AddBuilding") IsNot Nothing) Then
            txtBuildingName.ReadOnly = False
            Button1.Enabled = True
        End If
    End Sub
    Protected Sub grdBuildings_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdBuildings.SelectedIndexChanged
        Session("ProjectID") = grdBuildings.SelectedDataKey(0).ToString()
        If (Session("Project_Type").ToString().Equals("Open")) Then
            'txtBuildingName.Text = grdBuildings.SelectedRow.Cells(1).Text
            Session("ProjectSaved") = "1"
            'Session("BuildingName") = txtBuildingName.Text
        ElseIf (Session("Project_Type").ToString().Equals("Existing")) Then
            txtBuildingName.Text = grdBuildings.SelectedRow.Cells(1).Text
        End If
        txtBuildingName.Text = grdBuildings.SelectedRow.Cells(1).Text
        Session("BuildingName") = txtBuildingName.Text
    End Sub
End Class