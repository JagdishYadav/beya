﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Selection_List.aspx.vb" Inherits="CEA.Selection_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#sidr').hide();
        });
        function ShowConfirm() {
            var strConfirm = '<%=GetGlobalResourceObject("Resource", "ConfirmDelete") %>';
            return confirm(strConfirm);
        }
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Success") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "UpdateSuccess") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteSuccess") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorAlreadyExist") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "PendingList") %>';
            }

            alert(msgstring);
        }
        function DisableNextButton() {
            document.getElementById('<%= Button3.ClientID %>').disabled = true;
        }

        $(document).ready(function () {
            $(".bodyWrapper .bodyContainer").addClass('p-0');
            $("#headerWrapper .toggleNavButton").hide();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, SelectionList %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel2" runat="server" CssClass="panel">
                <div class="row">
                    <asp:Label ID="lblProjectName" runat="server" CssClass="ProjectName"></asp:Label>
                </div>
                <br />
                <div class="row">
                    <asp:Panel ID="Panel1" runat="server" CssClass="panel">
                        <div style="width: 100%; height: 300px; overflow: scroll">
                            <asp:GridView ID="grdSelection" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                EmptyDataText="No Data" Width="100%" PageSize="20" DataKeyNames="ID" HeaderStyle-HorizontalAlign ="Center">
                                <Columns>
                                    <asp:BoundField DataField="SelectedItem_Path" HeaderText='<%$ Resources:Resource, SelectedItem %>'
                                        ItemStyle-Width="80%" />
                                    <asp:CommandField ItemStyle-Width="10%" ShowSelectButton="true" SelectText='<%$ Resources:Resource, ViewProducts %>' />
                                    <asp:CommandField ItemStyle-Width="5%" ShowDeleteButton="true" SelectText='<%$ Resources:Resource, Delete %>' />
                                    <asp:BoundField DataField="StatusE" HeaderText='<%$ Resources:Resource, Status %>'
                                        ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="StatusA" HeaderText='<%$ Resources:Resource, Status %>'
                                        ItemStyle-Width="5%" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                </div>
                <br />
                <br />
                <div class="row">
                    <div class="large-4 medium-4 column middle">
                        <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Previous %>'
                            CssClass="backBTN"  />
                        <asp:Button ID="Button3" runat="server" Text='<%$ Resources:Resource, Report %>' CssClass="BTNReport"
                            />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
