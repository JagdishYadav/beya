﻿Public Class Selection_List
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Session("User_Role_ID") = "7" Or Session("User_Role_ID") = "8" Or Session("User_Role_ID") = "9" Or Session("User_Role_ID") = "10" Or Session("User_Role_ID") = "11" Or Session("User_Role_ID") = "188") Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "Disable", "DisableNextButton();", True)
        End If
        If (IsPostBack = False) Then
            'Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
            'Menuhide.Value = "close"
            Dim linkButton As LinkButton = Master.FindControl("LinkButton1")
            linkButton.Visible = False
            lblProjectName.Text = Session("ProjectName") + " \ " + Session("BuildingName")
            If (Request.IsAuthenticated) Then
                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    'Button3.Style.Add("direction", "rtl")
                    'Button3.Style.Add("float", "right")
                    Button1.Style.Add("direction", "rtl")
                    Button1.Style.Add("float", "right")
                End If
                grdSelection.Columns(3).Visible = True
                grdSelection.Columns(4).Visible = True
                If (UICulture = "Arabic") Then
                    grdSelection.Columns(3).Visible = False
                Else
                    grdSelection.Columns(4).Visible = False
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
            LoadDataGrid()
        End If
    End Sub
    Private Sub LoadDataGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        grdSelection.DataSource = objSqlConfig.GetProject_Items_Selection(Session("ProjectID").ToString())
        grdSelection.DataBind()
    End Sub
    Protected Sub grdSelection_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdSelection.SelectedIndexChanged
        Session("Selected_Item_ID") = grdSelection.SelectedDataKey("ID").ToString()
        Response.Redirect("Item_Products.aspx")
    End Sub
    Protected Sub grdSelection_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdSelection.RowDeleting
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim retVal As Integer = objSqlConfig.DeleteItem(grdSelection.DataKeys(e.RowIndex).Value.ToString(), Session("ProjectID"))

        If (retVal = 0) Then
            grdSelection.DataSource = objSqlConfig.GetProject_Items_Selection(Session("ProjectID").ToString())
            grdSelection.DataBind()
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)

            Dim strSelectedCategories As String = String.Empty
            For i As Integer = 0 To grdSelection.Rows.Count - 1
                strSelectedCategories = strSelectedCategories + grdSelection.Rows(i).Cells(0).Text + "}"
            Next
            If (strSelectedCategories.Length > 0) Then
                strSelectedCategories = strSelectedCategories.Substring(0, strSelectedCategories.Length - 1)
            End If
            objSqlConfig.InsertProjectCategories(strSelectedCategories, Session("ProjectID").ToString())
        End If
    End Sub
    Protected Sub grdSelection_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdSelection.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            For Each link As LinkButton In e.Row.Cells(2).Controls.OfType(Of LinkButton)()
                If link.CommandName = "Delete" Then
                    link.Attributes("onclick") = "if (ShowConfirm() == false) { return false; };"
                End If
            Next
        End If
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Response.Redirect("Add_Categories.aspx")
    End Sub
    'Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
    'Dim objSqlConfig As New ConfigClassSqlServer()
    'If (objSqlConfig.Get_Pending_List(Session("ProjectID").ToString()).ToString() <> "0") Then
    '    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('5');", True)
    'Else
    '    'Move forward
    'End 

    'Move Forward to reports 
    'End Sub
    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Response.Redirect("Reports.aspx")
    End Sub
    Protected Sub grdSelection_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdSelection.PageIndexChanging
        grdSelection.PageIndex = e.NewPageIndex
        LoadDataGrid()
    End Sub
End Class