﻿Public Class Final_ProductList
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            Session("ProjectSaved") = "0"
            Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
            Menuhide.Value = "close"
            Dim linkButton As LinkButton = Master.FindControl("LinkButton1")
            linkButton.Visible = False
            lblProjectName.Text = Session("ProjectName") + " \ " + Session("BuildingName")
            lblSelectedItem.Text = Session("Selected_Item_Name")
            btnEmpty.Attributes("onclick") = "if (ShowConfirm() == false) { return false; };"
            If (Request.IsAuthenticated) Then
                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    Button3.Style.Add("direction", "rtl")
                    Button3.Style.Add("float", "right")
                    Button1.Style.Add("direction", "rtl")
                    Button1.Style.Add("float", "right")
                    table1.Style.Add("direction", "rtl")
                    table1.Style.Add("float", "right")
                End If
                grdProducts.Columns(0).Visible = False
                If (UICulture = "Arabic") Then
                    grdProducts.Columns(1).Visible = False
                    grdProducts.Columns(3).Visible = False
                    grdProducts.Columns(5).Visible = False
                Else
                    grdProducts.Columns(2).Visible = False
                    grdProducts.Columns(4).Visible = False
                    grdProducts.Columns(6).Visible = False
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
            LoadDataGrid()
        End If
    End Sub
    Private Sub LoadDataGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        grdProducts.DataSource = objSqlConfig.Get_Item_Products_Final(Session("Selected_Item_ID").ToString())
        grdProducts.DataBind()
    End Sub
    Protected Sub grdProducts_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdProducts.RowDeleting
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim retVal As Integer = objSqlConfig.DeleteItem_Product(grdProducts.DataKeys(e.RowIndex).Value.ToString(), Session("Selected_Item_ID"), Session("ProjectID"))
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('5');", True)
        LoadDataGrid()
    End Sub
    Protected Sub grdProducts_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdProducts.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            For Each link As LinkButton In e.Row.Cells(11).Controls.OfType(Of LinkButton)()
                If link.CommandName = "Delete" Then
                    link.Attributes("onclick") = "if (ShowConfirm() == false) { return false; };"
                End If
            Next
        End If
    End Sub
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        Session("BuildingName") = ""
        Session("ProjectSaved") = "0"
        If (Session("Project_Type").Equals("Open") = False) Then
            Session("AddBuilding") = "1"
        End If
        Response.Redirect("Add_Building.aspx")
    End Sub
    Protected Sub LinkButton2_Click(sender As Object, e As EventArgs) Handles LinkButton2.Click
        Response.Redirect("Selection_List.aspx")
    End Sub
    'Protected Sub LinkButton3_Click(sender As Object, e As EventArgs) Handles LinkButton3.Click
    '    Response.Redirect("Selection_List.aspx")
    'End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Response.Redirect("Item_Products.aspx")
    End Sub
    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        'Dim objSqlConfig As New ConfigClassSqlServer()
        'If (objSqlConfig.Get_Pending_List(Session("ProjectID").ToString()).ToString() <> "0") Then
        '    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
        'Else
        '    'Move Forward to reports 
        'End If
        Response.Redirect("Reports.aspx")
    End Sub
    Protected Sub grdProducts_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdProducts.SelectedIndexChanged
        Response.Redirect("Product_Suppliers.aspx?ID=" + grdProducts.SelectedDataKey("ProdID").ToString())
    End Sub
    Protected Sub grdProducts_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdProducts.RowEditing
        Session("CameFromCES") = "1"
        Session("ProductCode") = grdProducts.DataKeys(e.NewEditIndex)("ProdID").ToString()
        Session("ProductName") = grdProducts.Rows(e.NewEditIndex).Cells(1).Text
        Session("ProductNameAra") = grdProducts.Rows(e.NewEditIndex).Cells(2).Text
        Session("ProductUnit") = grdProducts.Rows(e.NewEditIndex).Cells(7).Text
        Session("ProductCode") = grdProducts.DataKeys(e.NewEditIndex).Values(1).ToString()
        Response.Redirect("../Admin/CrewStructure_New.aspx")
    End Sub
    Protected Sub btnEmpty_Click(sender As Object, e As EventArgs) Handles btnEmpty.Click
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim retVal As Integer = objSqlConfig.Delete_All_Item_Products(Session("Selected_Item_ID"), Session("ProjectID"))
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('7');", True)
        LoadDataGrid()
    End Sub
End Class