﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Cat_Products
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Session("User_Role_ID") = "7" Or Session("User_Role_ID") = "8" Or Session("User_Role_ID") = "9" Or Session("User_Role_ID") = "10" Or Session("User_Role_ID") = "11" Or Session("User_Role_ID") = "188") Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "Disable", "DisableNextButton();", True)
        End If
        If (IsPostBack = False) Then
            Session("ProjectSaved") = "0"
            Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
            Menuhide.Value = "close"
            Dim linkButton As LinkButton = Master.FindControl("LinkButton1")
            linkButton.Visible = False
            lblProjectName.Text = Session("ProjectName") + " \ " + Session("BuildingName")
            If (Request.IsAuthenticated) Then
                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    Button3.Style.Add("direction", "rtl")
                    Button3.Style.Add("float", "right")
                    Button1.Style.Add("direction", "rtl")
                    Button1.Style.Add("float", "right")
                    Button2.Style.Add("direction", "rtl")
                    Button2.Style.Add("float", "right")
                End If
                grdProducts.Columns(0).Visible = False
                If (UICulture = "Arabic") Then
                    grdProducts.Columns(1).Visible = False
                    grdProducts.Columns(3).Visible = False
                    grdProducts.Columns(5).Visible = False
                Else
                    grdProducts.Columns(2).Visible = False
                    grdProducts.Columns(4).Visible = False
                    grdProducts.Columns(6).Visible = False
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
            Load_First_TimeDataGrid()
        End If
    End Sub
    Private Sub Load_First_TimeDataGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt, dt1 As New System.Data.DataTable
        Dim strIDCode As String = String.Empty
        dt = objSqlConfig.Get_Item_Data(Session("Selected_Item_ID").ToString())
        If (dt.Rows.Count > 0) Then

            Dim sqlConnection As New SqlConnection()
            Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
            sqlConnection.Open()

            grdProducts.DataSource = objSqlConfig.GetProducts(dt.Rows(0).Item(3).ToString(), "")
            grdProducts.DataBind()

            dt1 = objSqlConfig.Get_Item_Products(Session("Selected_Item_ID").ToString())

            For j As Integer = 0 To dt1.Rows.Count - 1
                For i As Integer = 0 To grdProducts.Rows.Count - 1
                    If (dt1.Rows(j).Item(0).ToString() = grdProducts.DataKeys(i).Value.ToString()) Then
                        Dim cbox As CheckBox = grdProducts.Rows(i).FindControl("CheckBox1")
                        Dim txtbox As TextBox = grdProducts.Rows(i).FindControl("TextBox1")
                        cbox.Checked = True
                        txtbox.Text = dt1.Rows(j).Item(1).ToString()
                    End If
                Next
            Next
            ViewState("Products_Grid") = grdProducts.DataSource
            ViewState("MapID") = dt.Rows(0).Item(3).ToString()
        End If
    End Sub
    Private Sub LoadGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New System.Data.DataTable
        grdProducts.DataSource = ViewState("Products_Grid")
        grdProducts.DataBind()

        dt = objSqlConfig.Get_Item_Products(Session("Selected_Item_ID").ToString())

        For j As Integer = 0 To dt.Rows.Count - 1
            For i As Integer = 0 To grdProducts.Rows.Count - 1
                If (dt.Rows(j).Item(0).ToString() = grdProducts.DataKeys(i).Value.ToString()) Then
                    Dim cbox As CheckBox = grdProducts.Rows(i).FindControl("CheckBox1")
                    Dim txtbox As TextBox = grdProducts.Rows(i).FindControl("TextBox1")
                    cbox.Checked = True
                    txtbox.Text = dt.Rows(j).Item(1).ToString()
                End If
            Next
        Next
    End Sub
    Protected Sub grdProducts_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdProducts.PageIndexChanging
        grdProducts.PageIndex = e.NewPageIndex
        LoadGrid()
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New System.Data.DataTable

        grdProducts.DataSource = objSqlConfig.GetProducts(ViewState("MapID").ToString(), txtSearch.Text)
        grdProducts.DataBind()

        dt = objSqlConfig.Get_Item_Products(Session("Selected_Item_ID").ToString())
        For j As Integer = 0 To dt.Rows.Count - 1
            For i As Integer = 0 To grdProducts.Rows.Count - 1
                If (dt.Rows(j).Item(0).ToString() = grdProducts.DataKeys(i).Value.ToString()) Then
                    Dim cbox As CheckBox = grdProducts.Rows(i).FindControl("CheckBox1")
                    Dim txtbox As TextBox = grdProducts.Rows(i).FindControl("TextBox1")
                    cbox.Checked = True
                    txtbox.Text = dt.Rows(j).Item(1).ToString()
                End If
            Next
        Next
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Response.Redirect("Selection_List.aspx")
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim strSelectedProducts As String = String.Empty
        Dim Qty As Double
        Dim count As Integer = 0

        For i As Integer = 0 To grdProducts.Rows.Count - 1
            Dim cbox As CheckBox = grdProducts.Rows(i).FindControl("CheckBox1")
            If (cbox.Checked) Then
                count = 1
                Dim txtbox As TextBox = grdProducts.Rows(i).FindControl("TextBox1")
                If (txtbox.Text.Length = 0) Then
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('5');", True)
                    txtbox.Focus()
                    Return
                ElseIf (Double.TryParse(txtbox.Text, Qty) = False) Then
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
                    txtbox.Focus()
                    Return
                End If
            End If
        Next

        If (count = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
        Else
            objSqlConfig.DeleteItem_Products(Session("Selected_Item_ID").ToString())
            For i As Integer = 0 To grdProducts.Rows.Count - 1
                Dim cbox As CheckBox = grdProducts.Rows(i).FindControl("CheckBox1")
                If (cbox.Checked) Then
                    Dim txtbox As TextBox = grdProducts.Rows(i).FindControl("TextBox1")
                    objSqlConfig.Insert_Item_Products(Session("Selected_Item_ID").ToString(), grdProducts.DataKeys(i).Value.ToString(), txtbox.Text.Trim())
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
                    Session("ProjectSaved") = "1"
                End If
            Next
            objSqlConfig.SetProjectStatus(Session("ProjectID"))
        End If
    End Sub
    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If (Session("ProjectSaved").Equals("0")) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
        Else
            Session("Selected_Item_Name") = lblSelectedItem.Text
            Response.Redirect("Final_ProductList.aspx")
        End If
    End Sub
    Protected Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs)
        Session("ProjectSaved") = "0"
    End Sub
    Protected Sub TextBox1_TextChanged(sender As Object, e As EventArgs)
        Session("ProjectSaved") = "0"
    End Sub
End Class