﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.IO

Public Class ReportViewer
    Inherits System.Web.UI.Page
    Dim reportDoc As New ReportDocument()
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'If (IsPostBack) Then
        Try
            If (Request.QueryString("ID") IsNot Nothing) Then
                Session("ReportDoc") = Nothing
            End If
            If (Session("ReportDoc") IsNot Nothing) Then
                CrystalReportViewer1.ReportSource = Session("ReportDoc")
                reportDoc = Session("ReportDoc")
            End If

            Dim reportPath As String
            Dim strServerName As String = ConfigurationManager.AppSettings("DBServer")
            Dim strDatabase As String = ConfigurationManager.AppSettings("Database")
            Dim strUserID As String = ConfigurationManager.AppSettings("UserID")
            Dim strPswd As String = ConfigurationManager.AppSettings("Password")

            If (Session("ReportID") = "1") Then
                Dim parmField As New ParameterField()
                Dim parmField1 As New ParameterField()
                Dim parmField2 As New ParameterField()
                Dim parmField3 As New ParameterField()
                Dim paramFields As New ParameterFields()
                Dim paramDiscreteValue As New ParameterDiscreteValue()
                Dim paramDiscreteValue1 As New ParameterDiscreteValue()
                Dim paramDiscreteValue2 As New ParameterDiscreteValue()
                Dim paramDiscreteValue3 As New ParameterDiscreteValue()

                parmField.Name = "@UserName"
                paramDiscreteValue.Value = Session("UserName")
                parmField.CurrentValues.Add(paramDiscreteValue)
                paramFields.Add(parmField)

                parmField1.Name = "@ProjectID"
                paramDiscreteValue1.Value = Session("ProjectID")
                parmField1.CurrentValues.Add(paramDiscreteValue1)
                paramFields.Add(parmField1)

                'parmField3.Name = "@CompID"
                'paramDiscreteValue3.Value = Session("CompanyID")
                'parmField3.CurrentValues.Add(paramDiscreteValue3)
                'paramFields.Add(parmField3)

                If (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
                    parmField2.Name = "@ProjectType"
                    paramDiscreteValue2.Value = "2"
                    parmField2.CurrentValues.Add(paramDiscreteValue2)
                    paramFields.Add(parmField2)
                Else
                    parmField2.Name = "@ProjectType"
                    paramDiscreteValue2.Value = "1"
                    parmField2.CurrentValues.Add(paramDiscreteValue2)
                    paramFields.Add(parmField2)
                End If

                CrystalReportViewer1.ParameterFieldInfo = paramFields
                reportPath = Server.MapPath("~/Reports/TabularReportE.rpt")

                reportDoc.Load(reportPath)
                reportDoc.DataSourceConnections(0).SetConnection(strServerName, strDatabase, strUserID, strPswd)
                reportDoc.DataSourceConnections(0).SetLogon(strUserID, strPswd)
                reportDoc.SetDatabaseLogon(strUserID, strPswd, strServerName, strDatabase)
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = reportDoc
                Session("ReportDoc") = reportDoc.FileName

                'reportDoc.SetParameterValue("@UserName", Session("UserName"))
                'reportDoc.SetParameterValue("@ProjectID", Session("ProjectID"))
                'reportDoc.SetParameterValue("@CompID", Session("CompanyID"))
                'reportDoc.SetParameterValue("@ProjectType", "1")
                'reportDoc.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, "")

            ElseIf (Session("ReportID") = "2") Then

                Dim parmField As New ParameterField()
                Dim parmField1 As New ParameterField()
                Dim parmField2 As New ParameterField()
                Dim parmField3 As New ParameterField()
                Dim parmField4 As New ParameterField()
                Dim paramFields As New ParameterFields()
                Dim paramDiscreteValue As New ParameterDiscreteValue()
                Dim paramDiscreteValue1 As New ParameterDiscreteValue()
                Dim paramDiscreteValue2 As New ParameterDiscreteValue()
                Dim paramDiscreteValue3 As New ParameterDiscreteValue()
                Dim paramDiscreteValue4 As New ParameterDiscreteValue()

                parmField.Name = "@UserName"
                paramDiscreteValue.Value = Session("UserName")
                parmField.CurrentValues.Add(paramDiscreteValue)
                paramFields.Add(parmField)

                parmField1.Name = "@ProjectID"
                paramDiscreteValue1.Value = Session("ProjectID")
                parmField1.CurrentValues.Add(paramDiscreteValue1)
                paramFields.Add(parmField1)

                parmField3.Name = "@ProjectName"
                paramDiscreteValue3.Value = Session("ProjectName")
                parmField3.CurrentValues.Add(paramDiscreteValue3)
                paramFields.Add(parmField3)

                parmField4.Name = "@CompID"
                paramDiscreteValue4.Value = Session("CompanyID")
                parmField4.CurrentValues.Add(paramDiscreteValue4)
                paramFields.Add(parmField4)

                If (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
                    parmField2.Name = "@ProjectType"
                    paramDiscreteValue2.Value = "2"
                    parmField2.CurrentValues.Add(paramDiscreteValue2)
                    paramFields.Add(parmField2)
                Else
                    parmField2.Name = "@ProjectType"
                    paramDiscreteValue2.Value = "1"
                    parmField2.CurrentValues.Add(paramDiscreteValue2)
                    paramFields.Add(parmField2)
                End If

                CrystalReportViewer1.ParameterFieldInfo = paramFields
                reportPath = Server.MapPath("~/Reports/FullReportE.rpt")

                reportDoc.Load(reportPath)
                reportDoc.DataSourceConnections(0).SetConnection(strServerName, strDatabase, strUserID, strPswd)
                reportDoc.DataSourceConnections(0).SetLogon(strUserID, strPswd)
                reportDoc.SetDatabaseLogon(strUserID, strPswd, strServerName, strDatabase)
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = reportDoc
                Session("ReportDoc") = reportDoc.FileName

            ElseIf (Session("ReportID") = "3") Then
                Dim parmField As New ParameterField()
                Dim parmField1 As New ParameterField()
                Dim parmField2 As New ParameterField()
                Dim paramFields As New ParameterFields()
                Dim paramDiscreteValue As New ParameterDiscreteValue()
                Dim paramDiscreteValue1 As New ParameterDiscreteValue()
                Dim paramDiscreteValue2 As New ParameterDiscreteValue()

                parmField.Name = "@UserName"
                paramDiscreteValue.Value = Session("UserName")
                parmField.CurrentValues.Add(paramDiscreteValue)
                paramFields.Add(parmField)

                parmField1.Name = "@ProjectID"
                paramDiscreteValue1.Value = Session("ProjectID")
                parmField1.CurrentValues.Add(paramDiscreteValue1)
                paramFields.Add(parmField1)

                parmField2.Name = "@CompID"
                paramDiscreteValue2.Value = Session("CompanyID")
                parmField2.CurrentValues.Add(paramDiscreteValue2)
                paramFields.Add(parmField2)

                CrystalReportViewer1.ParameterFieldInfo = paramFields
                reportPath = Server.MapPath("~/Reports/MaterialCostReportE.rpt")

                reportDoc.Load(reportPath)
                reportDoc.DataSourceConnections(0).SetConnection(strServerName, strDatabase, strUserID, strPswd)
                reportDoc.DataSourceConnections(0).SetLogon(strUserID, strPswd)
                reportDoc.SetDatabaseLogon(strUserID, strPswd, strServerName, strDatabase)
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = reportDoc
                Session("ReportDoc") = reportDoc.FileName

            ElseIf (Session("ReportID") = "4") Then
                Dim parmField As New ParameterField()
                Dim parmField1 As New ParameterField()
                Dim parmField2 As New ParameterField()
                Dim paramFields As New ParameterFields()
                Dim paramDiscreteValue As New ParameterDiscreteValue()
                Dim paramDiscreteValue1 As New ParameterDiscreteValue()
                Dim paramDiscreteValue2 As New ParameterDiscreteValue()

                parmField.Name = "@UserName"
                paramDiscreteValue.Value = Session("UserName")
                parmField.CurrentValues.Add(paramDiscreteValue)
                paramFields.Add(parmField)

                parmField1.Name = "@ProjectID"
                paramDiscreteValue1.Value = Session("ProjectID")
                parmField1.CurrentValues.Add(paramDiscreteValue1)
                paramFields.Add(parmField1)

                parmField2.Name = "@CompID"
                paramDiscreteValue2.Value = Session("CompanyID")
                parmField2.CurrentValues.Add(paramDiscreteValue2)
                paramFields.Add(parmField2)

                CrystalReportViewer1.ParameterFieldInfo = paramFields
                reportPath = Server.MapPath("~/Reports/MaterialCost_CategoryWiseReportE.rpt")

                reportDoc.Load(reportPath)
                reportDoc.DataSourceConnections(0).SetConnection(strServerName, strDatabase, strUserID, strPswd)
                reportDoc.DataSourceConnections(0).SetLogon(strUserID, strPswd)
                reportDoc.SetDatabaseLogon(strUserID, strPswd, strServerName, strDatabase)
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = reportDoc
                Session("ReportDoc") = reportDoc.FileName

            ElseIf (Session("ReportID") = "5") Then
                Dim parmField As New ParameterField()
                Dim parmField1 As New ParameterField()
                Dim parmField2 As New ParameterField()
                Dim parmField3 As New ParameterField()
                Dim paramFields As New ParameterFields()
                Dim paramDiscreteValue As New ParameterDiscreteValue()
                Dim paramDiscreteValue1 As New ParameterDiscreteValue()
                Dim paramDiscreteValue2 As New ParameterDiscreteValue()
                Dim paramDiscreteValue3 As New ParameterDiscreteValue()

                parmField.Name = "@UserName"
                paramDiscreteValue.Value = Session("UserName")
                parmField.CurrentValues.Add(paramDiscreteValue)
                paramFields.Add(parmField)

                parmField1.Name = "@ProjectID"
                paramDiscreteValue1.Value = Session("ProjectID")
                parmField1.CurrentValues.Add(paramDiscreteValue1)
                paramFields.Add(parmField1)

                parmField2.Name = "@ProjectName"
                paramDiscreteValue2.Value = Session("ProjectName")
                parmField2.CurrentValues.Add(paramDiscreteValue2)
                paramFields.Add(parmField2)

                parmField3.Name = "@CompID"
                paramDiscreteValue3.Value = Session("CompanyID")
                parmField3.CurrentValues.Add(paramDiscreteValue3)
                paramFields.Add(parmField3)

                CrystalReportViewer1.ParameterFieldInfo = paramFields
                reportPath = Server.MapPath("~/Reports/CrewStuructureCostReportE.rpt")

                reportDoc.Load(reportPath)
                reportDoc.DataSourceConnections(0).SetConnection(strServerName, strDatabase, strUserID, strPswd)
                reportDoc.DataSourceConnections(0).SetLogon(strUserID, strPswd)
                reportDoc.SetDatabaseLogon(strUserID, strPswd, strServerName, strDatabase)
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = reportDoc
                Session("ReportDoc") = reportDoc.FileName

            ElseIf (Session("ReportID") = "6") Then
                Dim parmField As New ParameterField()
                Dim parmField1 As New ParameterField()
                Dim parmField2 As New ParameterField()
                Dim parmField3 As New ParameterField()
                Dim parmField4 As New ParameterField()
                Dim paramFields As New ParameterFields()
                Dim paramDiscreteValue As New ParameterDiscreteValue()
                Dim paramDiscreteValue1 As New ParameterDiscreteValue()
                Dim paramDiscreteValue2 As New ParameterDiscreteValue()
                Dim paramDiscreteValue3 As New ParameterDiscreteValue()
                Dim paramDiscreteValue4 As New ParameterDiscreteValue()

                parmField.Name = "@UserName"
                paramDiscreteValue.Value = Session("UserName")
                parmField.CurrentValues.Add(paramDiscreteValue)
                paramFields.Add(parmField)

                parmField1.Name = "@ProjectID"
                paramDiscreteValue1.Value = Session("ProjectID")
                parmField1.CurrentValues.Add(paramDiscreteValue1)
                paramFields.Add(parmField1)

                parmField3.Name = "@ProjectName"
                paramDiscreteValue3.Value = Session("ProjectName")
                parmField3.CurrentValues.Add(paramDiscreteValue3)
                paramFields.Add(parmField3)

                parmField4.Name = "@CompID"
                paramDiscreteValue4.Value = Session("CompanyID")
                parmField4.CurrentValues.Add(paramDiscreteValue4)
                paramFields.Add(parmField4)

                If (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
                    parmField2.Name = "@ProjectType"
                    paramDiscreteValue2.Value = "2"
                    parmField2.CurrentValues.Add(paramDiscreteValue2)
                    paramFields.Add(parmField2)
                Else
                    parmField2.Name = "@ProjectType"
                    paramDiscreteValue2.Value = "1"
                    parmField2.CurrentValues.Add(paramDiscreteValue2)
                    paramFields.Add(parmField2)
                End If

                CrystalReportViewer1.ParameterFieldInfo = paramFields
                reportPath = Server.MapPath("~/Reports/CrewStuructureCost_ItemwiseReportE.rpt")

                reportDoc.Load(reportPath)
                reportDoc.DataSourceConnections(0).SetConnection(strServerName, strDatabase, strUserID, strPswd)
                reportDoc.DataSourceConnections(0).SetLogon(strUserID, strPswd)
                reportDoc.SetDatabaseLogon(strUserID, strPswd, strServerName, strDatabase)
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = reportDoc
                Session("ReportDoc") = reportDoc.FileName

            ElseIf (Session("ReportID") = "7") Then
                Dim parmField As New ParameterField()
                Dim parmField1 As New ParameterField()
                Dim parmField2 As New ParameterField()
                Dim parmField3 As New ParameterField()
                Dim parmField4 As New ParameterField()
                Dim paramFields As New ParameterFields()
                Dim paramDiscreteValue As New ParameterDiscreteValue()
                Dim paramDiscreteValue1 As New ParameterDiscreteValue()
                Dim paramDiscreteValue2 As New ParameterDiscreteValue()
                Dim paramDiscreteValue3 As New ParameterDiscreteValue()
                Dim paramDiscreteValue4 As New ParameterDiscreteValue()

                parmField.Name = "@UserName"
                paramDiscreteValue.Value = Session("UserName")
                parmField.CurrentValues.Add(paramDiscreteValue)
                paramFields.Add(parmField)

                parmField1.Name = "@ProjectID"
                paramDiscreteValue1.Value = Session("ProjectID")
                parmField1.CurrentValues.Add(paramDiscreteValue1)
                paramFields.Add(parmField1)

                parmField3.Name = "@ProjectName"
                paramDiscreteValue3.Value = Session("ProjectName")
                parmField3.CurrentValues.Add(paramDiscreteValue3)
                paramFields.Add(parmField3)

                parmField4.Name = "@CompID"
                paramDiscreteValue4.Value = Session("CompanyID")
                parmField4.CurrentValues.Add(paramDiscreteValue4)
                paramFields.Add(parmField4)

                If (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
                    parmField2.Name = "@ProjectType"
                    paramDiscreteValue2.Value = "2"
                    parmField2.CurrentValues.Add(paramDiscreteValue2)
                    paramFields.Add(parmField2)
                Else
                    parmField2.Name = "@ProjectType"
                    paramDiscreteValue2.Value = "1"
                    parmField2.CurrentValues.Add(paramDiscreteValue2)
                    paramFields.Add(parmField2)
                End If

                CrystalReportViewer1.ParameterFieldInfo = paramFields
                reportPath = Server.MapPath("~/Reports/MasterReport.rpt")

                reportDoc.Load(reportPath)
                reportDoc.DataSourceConnections(0).SetConnection(strServerName, strDatabase, strUserID, strPswd)
                reportDoc.DataSourceConnections(0).SetLogon(strUserID, strPswd)
                reportDoc.SetDatabaseLogon(strUserID, strPswd, strServerName, strDatabase)
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = reportDoc
                Session("ReportDoc") = reportDoc.FileName

            ElseIf (Session("ReportID") = "8") Then
                Dim parmField As New ParameterField()
                Dim parmField1 As New ParameterField()
                'Dim parmField1 As New ParameterField()
                Dim paramFields As New ParameterFields()
                Dim paramDiscreteValue As New ParameterDiscreteValue()
                Dim paramDiscreteValue1 As New ParameterDiscreteValue()
                ' Dim paramDiscreteValue1 As New ParameterDiscreteValue()

                parmField.Name = "@UserName"
                paramDiscreteValue.Value = Session("UserName")
                parmField.CurrentValues.Add(paramDiscreteValue)
                paramFields.Add(parmField)

                CrystalReportViewer1.ParameterFieldInfo = paramFields
                reportPath = Server.MapPath("~/Reports/SuppliersReportE.rpt")

                reportDoc.Load(reportPath)
                reportDoc.DataSourceConnections(0).SetConnection(strServerName, strDatabase, strUserID, strPswd)
                reportDoc.DataSourceConnections(0).SetLogon(strUserID, strPswd)
                reportDoc.SetDatabaseLogon(strUserID, strPswd, strServerName, strDatabase)
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = reportDoc
                Session("ReportDoc") = reportDoc.FileName

            ElseIf (Session("ReportID") = "9") Then
                Dim parmField As New ParameterField()
                Dim parmField1 As New ParameterField()
                Dim parmField2 As New ParameterField()
                Dim paramFields As New ParameterFields()
                Dim paramDiscreteValue As New ParameterDiscreteValue()
                Dim paramDiscreteValue1 As New ParameterDiscreteValue()
                Dim paramDiscreteValue2 As New ParameterDiscreteValue()

                parmField.Name = "@UserName"
                paramDiscreteValue.Value = Session("UserName")
                parmField.CurrentValues.Add(paramDiscreteValue)
                paramFields.Add(parmField)

                parmField1.Name = "@ProjectID"
                paramDiscreteValue1.Value = Session("ProjectID")
                parmField1.CurrentValues.Add(paramDiscreteValue1)
                paramFields.Add(parmField1)

                parmField2.Name = "@CompID"
                paramDiscreteValue2.Value = Session("CompanyID")
                parmField2.CurrentValues.Add(paramDiscreteValue2)
                paramFields.Add(parmField2)

                CrystalReportViewer1.ParameterFieldInfo = paramFields
                reportPath = Server.MapPath("~/Reports/SuppliersProductReportE.rpt")

                reportDoc.Load(reportPath)
                reportDoc.DataSourceConnections(0).SetConnection(strServerName, strDatabase, strUserID, strPswd)
                reportDoc.DataSourceConnections(0).SetLogon(strUserID, strPswd)
                reportDoc.SetDatabaseLogon(strUserID, strPswd, strServerName, strDatabase)
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = reportDoc
                Session("ReportDoc") = reportDoc.FileName

            ElseIf (Session("ReportID") = "10") Then
                Dim parmField As New ParameterField()
                Dim parmField1 As New ParameterField()
                Dim parmField2 As New ParameterField()
                Dim parmField3 As New ParameterField()
                Dim paramFields As New ParameterFields()
                Dim paramDiscreteValue As New ParameterDiscreteValue()
                Dim paramDiscreteValue1 As New ParameterDiscreteValue()
                Dim paramDiscreteValue2 As New ParameterDiscreteValue()
                Dim paramDiscreteValue3 As New ParameterDiscreteValue()

                parmField.Name = "@UserName"
                paramDiscreteValue.Value = Session("UserName")
                parmField.CurrentValues.Add(paramDiscreteValue)
                paramFields.Add(parmField)

                parmField1.Name = "@ProjectID"
                paramDiscreteValue1.Value = Session("ProjectID")
                parmField1.CurrentValues.Add(paramDiscreteValue1)
                paramFields.Add(parmField1)

                parmField2.Name = "@ProjectName"
                paramDiscreteValue2.Value = Session("ProjectName")
                parmField2.CurrentValues.Add(paramDiscreteValue2)
                paramFields.Add(parmField2)

                parmField3.Name = "@CompID"
                paramDiscreteValue3.Value = Session("CompanyID")
                parmField3.CurrentValues.Add(paramDiscreteValue3)
                paramFields.Add(parmField3)

                CrystalReportViewer1.ParameterFieldInfo = paramFields
                reportPath = Server.MapPath("~/Reports/ManpowerReportE.rpt")

                reportDoc.Load(reportPath)
                reportDoc.DataSourceConnections(0).SetConnection(strServerName, strDatabase, strUserID, strPswd)
                reportDoc.DataSourceConnections(0).SetLogon(strUserID, strPswd)
                reportDoc.SetDatabaseLogon(strUserID, strPswd, strServerName, strDatabase)
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = reportDoc
                Session("ReportDoc") = reportDoc.FileName

            ElseIf (Session("ReportID") = "11") Then
                Dim parmField As New ParameterField()
                Dim parmField1 As New ParameterField()
                Dim parmField2 As New ParameterField()
                Dim parmField3 As New ParameterField()
                Dim parmField4 As New ParameterField()
                Dim paramFields As New ParameterFields()
                Dim paramDiscreteValue As New ParameterDiscreteValue()
                Dim paramDiscreteValue1 As New ParameterDiscreteValue()
                Dim paramDiscreteValue2 As New ParameterDiscreteValue()
                Dim paramDiscreteValue3 As New ParameterDiscreteValue()
                Dim paramDiscreteValue4 As New ParameterDiscreteValue()

                parmField.Name = "@UserName"
                paramDiscreteValue.Value = Session("UserName")
                parmField.CurrentValues.Add(paramDiscreteValue)
                paramFields.Add(parmField)

                parmField1.Name = "@ProjectID"
                paramDiscreteValue1.Value = Session("ProjectID")
                parmField1.CurrentValues.Add(paramDiscreteValue1)
                paramFields.Add(parmField1)

                parmField2.Name = "@ProjectName"
                paramDiscreteValue2.Value = Session("ProjectName")
                parmField2.CurrentValues.Add(paramDiscreteValue2)
                paramFields.Add(parmField2)

                parmField4.Name = "@CompID"
                paramDiscreteValue4.Value = Session("CompanyID")
                parmField4.CurrentValues.Add(paramDiscreteValue4)
                paramFields.Add(parmField4)

                If (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
                    parmField3.Name = "@ProjectType"
                    paramDiscreteValue3.Value = "2"
                    parmField3.CurrentValues.Add(paramDiscreteValue3)
                    paramFields.Add(parmField3)
                Else
                    parmField3.Name = "@ProjectType"
                    paramDiscreteValue3.Value = "1"
                    parmField3.CurrentValues.Add(paramDiscreteValue3)
                    paramFields.Add(parmField3)
                End If

                CrystalReportViewer1.ParameterFieldInfo = paramFields
                reportPath = Server.MapPath("~/Reports/ManPower_CatWiseReport.rpt")

                reportDoc.Load(reportPath)
                reportDoc.DataSourceConnections(0).SetConnection(strServerName, strDatabase, strUserID, strPswd)
                reportDoc.DataSourceConnections(0).SetLogon(strUserID, strPswd)
                reportDoc.SetDatabaseLogon(strUserID, strPswd, strServerName, strDatabase)
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = reportDoc
                Session("ReportDoc") = reportDoc.FileName

            ElseIf (Session("ReportID") = "12") Then
                Dim parmField As New ParameterField()
                Dim parmField1 As New ParameterField()
                Dim parmField2 As New ParameterField()
                Dim parmField3 As New ParameterField()
                Dim paramFields As New ParameterFields()
                Dim paramDiscreteValue As New ParameterDiscreteValue()
                Dim paramDiscreteValue1 As New ParameterDiscreteValue()
                Dim paramDiscreteValue2 As New ParameterDiscreteValue()
                Dim paramDiscreteValue3 As New ParameterDiscreteValue()

                parmField.Name = "@UserName"
                paramDiscreteValue.Value = Session("UserName")
                parmField.CurrentValues.Add(paramDiscreteValue)
                paramFields.Add(parmField)

                parmField1.Name = "@ProjectID"
                paramDiscreteValue1.Value = Session("ProjectID")
                parmField1.CurrentValues.Add(paramDiscreteValue1)
                paramFields.Add(parmField1)

                parmField2.Name = "@ProjectName"
                paramDiscreteValue2.Value = Session("ProjectName")
                parmField2.CurrentValues.Add(paramDiscreteValue2)
                paramFields.Add(parmField2)

                parmField3.Name = "@CompID"
                paramDiscreteValue3.Value = Session("CompanyID")
                parmField3.CurrentValues.Add(paramDiscreteValue3)
                paramFields.Add(parmField3)

                CrystalReportViewer1.ParameterFieldInfo = paramFields
                reportPath = Server.MapPath("~/Reports/EquipmentDetailsReport.rpt")

                reportDoc.Load(reportPath)
                reportDoc.DataSourceConnections(0).SetConnection(strServerName, strDatabase, strUserID, strPswd)
                reportDoc.DataSourceConnections(0).SetLogon(strUserID, strPswd)
                reportDoc.SetDatabaseLogon(strUserID, strPswd, strServerName, strDatabase)
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = reportDoc
                Session("ReportDoc") = reportDoc.FileName

            ElseIf (Session("ReportID") = "13") Then
                Dim parmField As New ParameterField()
                Dim parmField1 As New ParameterField()
                Dim parmField2 As New ParameterField()
                Dim parmField3 As New ParameterField()
                Dim parmField4 As New ParameterField()
                Dim paramFields As New ParameterFields()
                Dim paramDiscreteValue As New ParameterDiscreteValue()
                Dim paramDiscreteValue1 As New ParameterDiscreteValue()
                Dim paramDiscreteValue2 As New ParameterDiscreteValue()
                Dim paramDiscreteValue3 As New ParameterDiscreteValue()
                Dim paramDiscreteValue4 As New ParameterDiscreteValue()

                parmField.Name = "@UserName"
                paramDiscreteValue.Value = Session("UserName")
                parmField.CurrentValues.Add(paramDiscreteValue)
                paramFields.Add(parmField)

                parmField1.Name = "@ProjectID"
                paramDiscreteValue1.Value = Session("ProjectID")
                parmField1.CurrentValues.Add(paramDiscreteValue1)
                paramFields.Add(parmField1)

                parmField2.Name = "@ProjectName"
                paramDiscreteValue2.Value = Session("ProjectName")
                parmField2.CurrentValues.Add(paramDiscreteValue2)
                paramFields.Add(parmField2)

                parmField4.Name = "@CompID"
                paramDiscreteValue4.Value = Session("CompanyID")
                parmField4.CurrentValues.Add(paramDiscreteValue4)
                paramFields.Add(parmField4)

                If (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
                    parmField3.Name = "@ProjectType"
                    paramDiscreteValue3.Value = "2"
                    parmField3.CurrentValues.Add(paramDiscreteValue3)
                    paramFields.Add(parmField3)
                Else
                    parmField3.Name = "@ProjectType"
                    paramDiscreteValue3.Value = "1"
                    parmField3.CurrentValues.Add(paramDiscreteValue3)
                    paramFields.Add(parmField3)
                End If

                CrystalReportViewer1.ParameterFieldInfo = paramFields
                reportPath = Server.MapPath("~/Reports/Equipment_CatWiseReport.rpt")

                reportDoc.Load(reportPath)
                reportDoc.DataSourceConnections(0).SetConnection(strServerName, strDatabase, strUserID, strPswd)
                reportDoc.DataSourceConnections(0).SetLogon(strUserID, strPswd)
                reportDoc.SetDatabaseLogon(strUserID, strPswd, strServerName, strDatabase)
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = reportDoc
                Session("ReportDoc") = reportDoc.FileName

            ElseIf (Session("ReportID") = "14") Then
                Dim parmField As New ParameterField()
                Dim parmField1 As New ParameterField()
                Dim parmField2 As New ParameterField()
                Dim parmField3 As New ParameterField()
                Dim paramFields As New ParameterFields()
                Dim paramDiscreteValue As New ParameterDiscreteValue()
                Dim paramDiscreteValue1 As New ParameterDiscreteValue()
                Dim paramDiscreteValue2 As New ParameterDiscreteValue()
                Dim paramDiscreteValue3 As New ParameterDiscreteValue()

                parmField.Name = "@UserName"
                paramDiscreteValue.Value = Session("UserName")
                parmField.CurrentValues.Add(paramDiscreteValue)
                paramFields.Add(parmField)

                parmField1.Name = "@ProjectID"
                paramDiscreteValue1.Value = Session("ProjectID")
                parmField1.CurrentValues.Add(paramDiscreteValue1)
                paramFields.Add(parmField1)

                parmField3.Name = "@CompID"
                paramDiscreteValue3.Value = Session("CompanyID")
                parmField3.CurrentValues.Add(paramDiscreteValue3)
                paramFields.Add(parmField3)

                If (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
                    parmField2.Name = "@ProjectType"
                    paramDiscreteValue2.Value = "2"
                    parmField2.CurrentValues.Add(paramDiscreteValue2)
                    paramFields.Add(parmField2)
                Else
                    parmField2.Name = "@ProjectType"
                    paramDiscreteValue2.Value = "1"
                    parmField2.CurrentValues.Add(paramDiscreteValue2)
                    paramFields.Add(parmField2)
                End If


                CrystalReportViewer1.ParameterFieldInfo = paramFields
                reportPath = Server.MapPath("~/Reports/ProjectTotalCostReportE.rpt")

                reportDoc.Load(reportPath)
                reportDoc.DataSourceConnections(0).SetConnection(strServerName, strDatabase, strUserID, strPswd)
                reportDoc.DataSourceConnections(0).SetLogon(strUserID, strPswd)
                reportDoc.SetDatabaseLogon(strUserID, strPswd, strServerName, strDatabase)
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = reportDoc
                Session("ReportDoc") = reportDoc.FileName

            ElseIf (Session("ReportID") = "15") Then
                Dim parmField As New ParameterField()
                Dim parmField1 As New ParameterField()
                Dim parmField2 As New ParameterField()
                Dim parmField3 As New ParameterField()
                Dim paramFields As New ParameterFields()
                Dim paramDiscreteValue As New ParameterDiscreteValue()
                Dim paramDiscreteValue1 As New ParameterDiscreteValue()
                Dim paramDiscreteValue2 As New ParameterDiscreteValue()
                Dim paramDiscreteValue3 As New ParameterDiscreteValue()

                parmField.Name = "@UserName"
                paramDiscreteValue.Value = Session("UserName")
                parmField.CurrentValues.Add(paramDiscreteValue)
                paramFields.Add(parmField)

                parmField1.Name = "@ProjectID"
                paramDiscreteValue1.Value = Session("ProjectID")
                parmField1.CurrentValues.Add(paramDiscreteValue1)
                paramFields.Add(parmField1)

                parmField2.Name = "@ProjectName"
                paramDiscreteValue2.Value = Session("ProjectName")
                parmField2.CurrentValues.Add(paramDiscreteValue2)
                paramFields.Add(parmField2)

                CrystalReportViewer1.ParameterFieldInfo = paramFields
                reportPath = Server.MapPath("~/Reports/ClientBOQ_Mapping_Main.rpt")

                reportDoc.Load(reportPath)
                reportDoc.DataSourceConnections(0).SetConnection(strServerName, strDatabase, strUserID, strPswd)
                reportDoc.DataSourceConnections(0).SetLogon(strUserID, strPswd)
                reportDoc.SetDatabaseLogon(strUserID, strPswd, strServerName, strDatabase)
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
                CrystalReportViewer1.ReportSource = reportDoc
                Session("ReportDoc") = reportDoc.FileName
            End If

            'Session("ReportDoc") = reportDoc

        Catch ex As Exception
            'Label2.Text = ex.Message
            'Label3.Text = ex.InnerException.Message()
        End Try
        
        'End If
        'End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            CrystalReportViewer1.HasPrintButton = True
            Label1.Visible = True
            DropDownList1.Visible = True
            Button1.Visible = True
            If (Session("IsTemp").ToString().Equals("1")) Then
                CrystalReportViewer1.HasPrintButton = False
            ElseIf (Session("IsTemp").ToString().Equals("2")) Then
                Label1.Visible = False
                DropDownList1.Visible = False
                Button1.Visible = False
            ElseIf (Session("IsTemp").ToString().Equals("3")) Then
                CrystalReportViewer1.HasPrintButton = False
                Label1.Visible = False
                DropDownList1.Visible = False
                Button1.Visible = False
                'Me.ClientScript.RegisterStartupScript(Me.GetType(), "hideContext", "if (document.all){document.onselectstart=new Function ('return false');document.oncontextmenu=new Function ('event.cancelBubble=true; event.returnValue=false; return false;');}")
            End If

        End If
    End Sub
    Protected Sub lnkReports_Click(sender As Object, e As EventArgs) Handles lnkReports.Click
        Response.Redirect("Reports.aspx")
    End Sub
    Protected Sub lnkSelection_Click(sender As Object, e As EventArgs) Handles lnkSelection.Click
        If (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
            Response.Redirect("ClientBOQ.aspx")
        Else
            Response.Redirect("Add_Categories.aspx")
        End If
    End Sub
    Protected Sub lnkHome_Click(sender As Object, e As EventArgs) Handles lnkHome.Click
        If (Session("User_Role_ID") = "999") Then
            Response.Redirect("~/Admin/Template_Project.aspx")
        Else
            Response.Redirect("Project_Home.aspx")
        End If
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try

            Dim reportPath As String
            Dim dt As New System.Data.DataTable
            Dim sqlConnection As New SqlConnection()
            Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
            sqlConnection.Open()

            Dim projectType As Integer = 1
            If (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
                projectType = 2
            End If

            If (Session("ReportID") = "1") Then

                sqlCommand.CommandText = "sp_TabularReport"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
                sqlCommand.Parameters.AddWithValue("@ProjectID", Session("ProjectID"))
                sqlCommand.Parameters.AddWithValue("@ProjectType", projectType)
                sqlCommand.Connection = sqlConnection

                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)
                reportPath = Server.MapPath("~/Reports/TabularReportE.rpt")

            ElseIf (Session("ReportID") = "2") Then

                sqlCommand.CommandText = "sp_FullReportE"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
                sqlCommand.Parameters.AddWithValue("@ProjectID", Session("ProjectID"))
                sqlCommand.Parameters.AddWithValue("@ProjectName", Session("ProjectName"))
                sqlCommand.Parameters.AddWithValue("@ProjectType", projectType)
                sqlCommand.Connection = sqlConnection

                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)
                reportPath = Server.MapPath("~/Reports/FullReportE.rpt")

            ElseIf (Session("ReportID") = "3") Then

                sqlCommand.CommandText = "sp_MaterialCostReport"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
                sqlCommand.Parameters.AddWithValue("@ProjectID", Session("ProjectID"))
                sqlCommand.Connection = sqlConnection

                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)
                reportPath = Server.MapPath("~/Reports/MaterialCostReportE.rpt")

            ElseIf (Session("ReportID") = "4") Then

                sqlCommand.CommandText = "sp_MaterialCost_CategoryWise_Report"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
                sqlCommand.Parameters.AddWithValue("@ProjectID", Session("ProjectID"))
                sqlCommand.Connection = sqlConnection

                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)
                reportPath = Server.MapPath("~/Reports/MaterialCost_CategoryWiseReportE.rpt")

            ElseIf (Session("ReportID") = "5") Then

                sqlCommand.CommandText = "sp_CrewStructureCostReportE"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
                sqlCommand.Parameters.AddWithValue("@ProjectID", Session("ProjectID"))
                sqlCommand.Parameters.AddWithValue("@ProjectName", Session("ProjectName"))
                sqlCommand.Connection = sqlConnection

                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)
                reportPath = Server.MapPath("~/Reports/CrewStuructureCostReportE.rpt")

            ElseIf (Session("ReportID") = "6") Then

                sqlCommand.CommandText = "sp_CrewStructureCost_ItemWise_ReportE"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
                sqlCommand.Parameters.AddWithValue("@ProjectID", Session("ProjectID"))
                sqlCommand.Parameters.AddWithValue("@ProjectName", Session("ProjectName"))
                sqlCommand.Parameters.AddWithValue("@ProjectType", projectType)
                sqlCommand.Connection = sqlConnection

                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)
                reportPath = Server.MapPath("~/Reports/CrewStuructureCost_ItemwiseReportE.rpt")

            ElseIf (Session("ReportID") = "7") Then

                sqlCommand.CommandText = "sp_MasterReportE"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
                sqlCommand.Parameters.AddWithValue("@ProjectID", Session("ProjectID"))
                sqlCommand.Parameters.AddWithValue("@ProjectName", Session("ProjectName"))
                sqlCommand.Parameters.AddWithValue("@ProjectType", projectType)
                sqlCommand.Connection = sqlConnection

                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)
                reportPath = Server.MapPath("~/Reports/MasterReport.rpt")

            ElseIf (Session("ReportID") = "8") Then

                sqlCommand.CommandText = "sp_SuppliersReportE"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
                sqlCommand.Connection = sqlConnection

                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)
                reportPath = Server.MapPath("~/Reports/SuppliersReportE.rpt")

            ElseIf (Session("ReportID") = "9") Then

                sqlCommand.CommandText = "sp_SuppliersProductReport"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
                sqlCommand.Parameters.AddWithValue("@ProjectID", Session("ProjectID"))
                sqlCommand.Connection = sqlConnection

                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)
                reportPath = Server.MapPath("~/Reports/SuppliersProductReportE.rpt")

            ElseIf (Session("ReportID") = "10") Then

                sqlCommand.CommandText = "sp_ManpowerReportE"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
                sqlCommand.Parameters.AddWithValue("@ProjectID", Session("ProjectID"))
                sqlCommand.Parameters.AddWithValue("@ProjectName", Session("ProjectName"))
                sqlCommand.Connection = sqlConnection

                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)
                reportPath = Server.MapPath("~/Reports/ManpowerReportE.rpt")

            ElseIf (Session("ReportID") = "11") Then

                sqlCommand.CommandText = "sp_Manpower_ItemWise_ReportE"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
                sqlCommand.Parameters.AddWithValue("@ProjectID", Session("ProjectID"))
                sqlCommand.Parameters.AddWithValue("@ProjectName", Session("ProjectName"))
                sqlCommand.Parameters.AddWithValue("@ProjectType", projectType)
                sqlCommand.Connection = sqlConnection

                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)
                reportPath = Server.MapPath("~/Reports/ManPower_CatWiseReport.rpt")

            ElseIf (Session("ReportID") = "12") Then

                sqlCommand.CommandText = "sp_EquipmentDetailsReport"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
                sqlCommand.Parameters.AddWithValue("@ProjectID", Session("ProjectID"))
                sqlCommand.Parameters.AddWithValue("@ProjectName", Session("ProjectName"))
                sqlCommand.Connection = sqlConnection

                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)
                reportPath = Server.MapPath("~/Reports/EquipmentDetailsReport.rpt")

            ElseIf (Session("ReportID") = "13") Then

                sqlCommand.CommandText = "sp_EquipmentDetails_ItemWise"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
                sqlCommand.Parameters.AddWithValue("@ProjectID", Session("ProjectID"))
                sqlCommand.Parameters.AddWithValue("@ProjectName", Session("ProjectName"))
                sqlCommand.Parameters.AddWithValue("@ProjectType", projectType)
                sqlCommand.Connection = sqlConnection

                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)
                reportPath = Server.MapPath("~/Reports/Equipment_CatWiseReport.rpt")

            ElseIf (Session("ReportID") = "14") Then

                sqlCommand.CommandText = "sp_TotalProjectCost"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
                sqlCommand.Parameters.AddWithValue("@ProjectID", Session("ProjectID"))
                sqlCommand.Parameters.AddWithValue("@ProjectType", projectType)
                sqlCommand.Connection = sqlConnection

                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)
                reportPath = Server.MapPath("~/Reports/ProjectTotalCostReportE.rpt")

            ElseIf (Session("ReportID") = "15") Then

                sqlCommand.CommandText = "sp_ClientBOQMapping_Main"
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
                sqlCommand.Parameters.AddWithValue("@ProjectID", Session("ProjectID"))
                sqlCommand.Parameters.AddWithValue("@ProjectName", Session("ProjectName"))
                sqlCommand.Connection = sqlConnection

                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)
                reportPath = Server.MapPath("~/Reports/ClientBOQ_Mapping_Main.rpt")
            End If

            reportDoc.Load(reportPath)
            reportDoc.SetDataSource(dt)
            sqlConnection.Close()

            Select Case DropDownList1.SelectedItem.Value

                Case "0"
                    reportDoc.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, Response, True, "ProjectReport")
                Case "1"
                    reportDoc.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, True, "ProjectReport")
                Case "2"
                    reportDoc.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.WordForWindows, Response, True, "ProjectReport")

            End Select

            'If (Session("ReportDoc") IsNot Nothing) Then
            '    CrystalReportViewer1.ReportSource = Session("ReportDoc")
            '    reportDoc = Session("ReportDoc")
            'End If

        Catch ex As Exception
            'Label2.Text = ex.Message
            'Label3.Text = ex.InnerException.Message
        End Try
    End Sub
    Private Function LoadData() As System.Data.DataTable
        Dim Dt As New System.Data.DataTable()
        Dim objSqlConfig As New ConfigClassSqlServer()

        Dt = objSqlConfig.Get_Full_Report_Data(Session("ProjectID"), Session("UserName"))
        Dt.Columns.Add(New DataColumn("Image", System.Type.GetType("System.Byte[]")))

        For i As Integer = 0 To Dt.Rows.Count - 1
            Dt.Rows(i).Item(11) = ParseImage(Dt.Rows(i).Item(6).ToString())
        Next

        Return Dt
    End Function
    Private Function ParseImage(ByVal ImgName As String) As Byte()
        Dim Fs As FileStream
        Dim Bs As BinaryReader
        Dim Path As String

        Path = Server.MapPath("~")
        If (File.Exists(Path & "\Upload\Images\" & ImgName)) Then
            Fs = New FileStream(Path & "\Upload\Images\" & ImgName, FileMode.Open, FileAccess.Read)
        Else
            ImgName = "nopicture.gif"
            Fs = New FileStream(Path & "\Upload\Images\" & ImgName, FileMode.Open, FileAccess.Read)
        End If
        Bs = New BinaryReader(Fs)
        Dim imgbyte(Fs.Length) As Byte
        imgbyte = Bs.ReadBytes(Convert.ToInt32((Fs.Length)))
        Fs.Close()
        Bs.Close()

        Return imgbyte
    End Function

End Class