﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Add_Level1
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Session("User_Role_ID") = "7" Or Session("User_Role_ID") = "8" Or Session("User_Role_ID") = "9" Or Session("User_Role_ID") = "10" Or Session("User_Role_ID") = "11" Or Session("User_Role_ID") = "188") Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "Disable", "DisableNextButton();", True)
        End If
        If (IsPostBack = False) Then
            Session("ProjectSaved") = "0"
            'Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
            'Menuhide.Value = "close"
            Dim linkButton As LinkButton = Master.FindControl("LinkButton1")
            linkButton.Visible = False
            lblProjectName.Text = Session("ProjectName") + " / " + Session("BuildingName")

            tvLevels.PathSeparator = "$"

            If (Request.IsAuthenticated) Then
                If (UICulture = "Arabic") Then
                    '.Style.Add("direction", "rtl")
                    'Panel1.Style.Add("text-align", "right")
                    tvLevels.Style.Add("float", "right")
                    tvLevels.Style.Add("direction", "rtl")
                    tvLevels.Style.Add("text-align", "right")
                    Button9.Style.Add("direction", "rtl")
                    Button9.Style.Add("float", "right")
                    Button1.Style.Add("direction", "rtl")
                    Button1.Style.Add("float", "right")
                    Button2.Style.Add("direction", "rtl")
                    Button2.Style.Add("float", "right")

                    grdData.Columns(1).Visible = False
                    grdData.Columns(2).Visible = True
                Else
                    grdData.Columns(1).Visible = True
                    grdData.Columns(2).Visible = False
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
            Button8.Attributes.Add("style", "display:none")
            Dim languageSelect As HiddenField = Me.Page.Master.FindControl("hfMenuLanguage")
            If languageSelect.Value = "Arabic" Then
                tvLevels.LineImagesFolder = "~/TreeLineImagesAr"

            End If
            If (Session("IsTemp").ToString().Equals("1")) Then
                tvLevels.Enabled = False
            End If

            If (Session("User_Role_ID") = "999") Then
                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (Session("User_Role_ID").ToString().Contains("999")) Then
                    If (menuItems.Count > 15) Then
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                    End If
                ElseIf (Session("User_Role_ID").ToString().Contains("888")) Then
                    If (menuItems.Count > 5) Then
                        menuItems.RemoveAt(0)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                    End If
                End If
            Else
                Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                Menuhide.Value = "close"
            End If

            grdData.DataSource = Nothing
            grdData.DataBind()
        End If
    End Sub
    Private Sub LoadTreeViewData()
        Try

        Dim strClientTypeID As String = "1"
        If (Session("ClientTypeID") IsNot Nothing) Then
            strClientTypeID = Session("ClientTypeID")
        End If
        'If (hdnInitialLoad.Value.ToString().Equals("1")) Then
        Dim sqlConnection As New SqlConnection()
        Dim node As TreeNode = Nothing
        Dim NodeLevel1, NodeLevel2, NodeLevel3, NodeLevel4, NodeLevel5, NodeLevel6, NodeLevel7 As TreeNode
        tvLevels.Nodes.Clear()

        Dim objSqlConfig As New ConfigClassSqlServer()
            Dim strCategories As String = String.Empty
            If (Session("IsTemp").ToString().Equals("1")) Then
                strCategories = objSqlConfig.GetUserCategories("1")
            Else
                strCategories = objSqlConfig.GetUserCategories(Session("UserID"))
            End If
            Dim dtProjectitems As New System.Data.DataTable
            dtProjectitems = objSqlConfig.GetProjectCostLineItems(Session("ProjectName"), Session("BuildingName"), Session("CompanyID"))

            Dim arrProjectItems As String()
            Dim strLevel1, strlevel2, strlevel3, strlevel4, strLevel5, strlevel6, strlevel7, strlevel8 As String
            strLevel1 = ""
            strlevel2 = ""
            strlevel3 = ""
            strlevel4 = ""
            strLevel5 = ""
            strlevel6 = ""
            strlevel7 = ""
            strlevel8 = ""

            If (dtProjectitems.Rows.Count > 0) Then
                arrProjectItems = dtProjectitems.Rows(0).Item(0).ToString().Split("~")

                Dim arrItemNos As String()
                Dim Result1 As Match

                For j As Integer = 0 To arrProjectItems.Length - 1

                    arrItemNos = arrProjectItems(j).Split("$")
                    If (arrItemNos.Length > 0) Then
                        strLevel1 = strLevel1 + "'"
                        Result1 = System.Text.RegularExpressions.Regex.Match(arrItemNos(0), "\d+")
                        'Dim strResult As String = ""
                        'Dim myChars() As Char = arrItemNos(0).ToCharArray()
                        'For Each ch As Char In myChars
                        '    If Char.IsDigit(ch) Then
                        '        strResult = strResult + ch
                        '    End If
                        'Next
                        strLevel1 = strLevel1 + Result1.Value + "',"
                    End If
                    If (arrItemNos.Length > 1) Then
                        strlevel2 = strlevel2 + "'"
                        Result1 = System.Text.RegularExpressions.Regex.Match(arrItemNos(1), "\d+")
                        'Dim strResult As String = ""
                        'Dim myChars() As Char = arrItemNos(1).ToCharArray()
                        'For Each ch As Char In myChars
                        '    If Char.IsDigit(ch) Then
                        '        strResult = strResult + ch
                        '    End If
                        'Next
                        strlevel2 = strlevel2 + Result1.Value + "',"
                    End If
                    If (arrItemNos.Length > 2) Then
                        strlevel3 = strlevel3 + "'"
                        'Dim strResult As String = ""
                        'Dim myChars() As Char = arrItemNos(2).ToCharArray()
                        'For Each ch As Char In myChars
                        '    If Char.IsDigit(ch) Then
                        '        strResult = strResult + ch
                        '    End If
                        'Next
                        Result1 = System.Text.RegularExpressions.Regex.Match(arrItemNos(2), "\d+")
                        strlevel3 = strlevel3 + Result1.Value + "',"
                    End If
                    If (arrItemNos.Length > 3) Then
                        strlevel4 = strlevel4 + "'"
                        Result1 = System.Text.RegularExpressions.Regex.Match(arrItemNos(3), "\d+")
                        'Dim strResult As String = ""
                        'Dim myChars() As Char = arrItemNos(3).ToCharArray()
                        'For Each ch As Char In myChars
                        '    If Char.IsDigit(ch) Then
                        '        strResult = strResult + ch
                        '    End If
                        'Next
                        strlevel4 = strlevel4 + Result1.Value + "',"
                    End If
                    If (arrItemNos.Length > 4) Then
                        strLevel5 = strLevel5 + "'"
                        Result1 = System.Text.RegularExpressions.Regex.Match(arrItemNos(4), "\d+")
                        'Dim strResult As String = ""
                        'Dim myChars() As Char = arrItemNos(4).ToCharArray()
                        'For Each ch As Char In myChars
                        '    If Char.IsDigit(ch) Then
                        '        strResult = strResult + ch
                        '    End If
                        'Next
                        strLevel5 = strLevel5 + Result1.Value + "',"
                    End If
                    If (arrItemNos.Length > 5) Then
                        strlevel6 = strlevel6 + "'"
                        Result1 = System.Text.RegularExpressions.Regex.Match(arrItemNos(5), "\d+")
                        'Dim strResult As String = ""
                        'Dim myChars() As Char = arrItemNos(5).ToCharArray()
                        'For Each ch As Char In myChars
                        '    If Char.IsDigit(ch) Then
                        '        strResult = strResult + ch
                        '    End If
                        'Next
                        strlevel6 = strlevel6 + Result1.Value + "',"
                    End If
                    If (arrItemNos.Length > 6) Then
                        strlevel7 = strlevel7 + "'"
                        Result1 = System.Text.RegularExpressions.Regex.Match(arrItemNos(6), "\d+")
                        'Dim strResult As String = ""
                        'Dim myChars() As Char = arrItemNos(6).ToCharArray()
                        'For Each ch As Char In myChars
                        '    If Char.IsDigit(ch) Then
                        '        strResult = strResult + ch
                        '    End If
                        'Next
                        strlevel7 = strlevel7 + Result1.Value + "',"
                    End If
                    If (arrItemNos.Length > 7) Then
                        strlevel8 = strlevel8 + "'"
                        Result1 = System.Text.RegularExpressions.Regex.Match(arrItemNos(7), "\d+")
                        'Dim strResult As String = ""
                        'Dim myChars() As Char = arrItemNos(7).ToCharArray()
                        'For Each ch As Char In myChars
                        '    If Char.IsDigit(ch) Then
                        '        strResult = strResult + ch
                        '    End If
                        'Next
                        strlevel8 = strlevel8 + Result1.Value + "',"
                    End If

                Next

                If (strLevel1.Length > 0) Then
                    strLevel1 = strLevel1.Substring(0, strLevel1.Length - 1)
                Else
                    strLevel1 = "''"
                End If
                If (strlevel2.Length > 0) Then
                    strlevel2 = strlevel2.Substring(0, strlevel2.Length - 1)
                Else
                    strlevel2 = "''"
                End If
                If (strlevel3.Length > 0) Then
                    strlevel3 = strlevel3.Substring(0, strlevel3.Length - 1)
                Else
                    strlevel3 = "''"
                End If
                If (strlevel4.Length > 0) Then
                    strlevel4 = strlevel4.Substring(0, strlevel4.Length - 1)
                Else
                    strlevel4 = "''"
                End If
                If (strLevel5.Length > 0) Then
                    strLevel5 = strLevel5.Substring(0, strLevel5.Length - 1)
                Else
                    strLevel5 = "''"
                End If
                If (strlevel6.Length > 0) Then
                    strlevel6 = strlevel6.Substring(0, strlevel6.Length - 1)
                Else
                    strlevel6 = "''"
                End If
                If (strlevel7.Length > 0) Then
                    strlevel7 = strlevel7.Substring(0, strlevel7.Length - 1)
                Else
                    strlevel7 = "''"
                End If
                If (strlevel8.Length > 0) Then
                    strlevel8 = strlevel8.Substring(0, strlevel8.Length - 1)
                Else
                    strlevel8 = "''"
                End If

            End If

            Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
            sqlCommand.CommandTimeout = 920000
            Dim r1, r2, r3, r4, r5, r6, r7, r8 As SqlDataReader
            Dim dt1, dt2, dt3, dt4, dt5, dt6, dt7, dt8 As New System.Data.DataTable

            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
            sqlConnection.Open()

            If (UICulture = "Arabic") Then
                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No + '  ' + NameE IN ( " + strCategories + " ) AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                sqlCommand.CommandTimeout = 920000
                r1 = sqlCommand.ExecuteReader()
                dt1.Load(r1)
                r1.Close()

                For Each dr1 As DataRow In dt1.Rows
                    If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
                        node = New TreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
                        node.Target = dr1.Item(2).ToString()
                        'node.ImageUrl = dr1.Item(3).ToString()
                        node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
                        tvLevels.Nodes.Add(node)
                        NodeLevel1 = node

                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN(" + strlevel2 + ") AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                        sqlCommand.CommandTimeout = 920000
                        r2 = sqlCommand.ExecuteReader()
                        dt2.Clear()
                        dt2.Load(r2)
                        r2.Close()
                        For Each dr2 As DataRow In dt2.Rows
                            If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
                                node = New TreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
                                node.Target = dr2.Item(2).ToString()
                                'node.ImageUrl = dr2.Item(3).ToString() 
                                node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
                                NodeLevel1.ChildNodes.Add(node)
                                NodeLevel2 = node

                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN(" + strlevel3 + ") AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                                sqlCommand.CommandTimeout = 920000
                                r3 = sqlCommand.ExecuteReader()
                                dt3.Clear()
                                dt3.Load(r3)
                                r3.Close()
                                For Each dr3 As DataRow In dt3.Rows
                                    If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
                                        node = New TreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
                                        node.Target = dr3.Item(2).ToString()
                                        'node.ImageUrl = dr3.Item(3).ToString()
                                        node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
                                        NodeLevel2.ChildNodes.Add(node)
                                        NodeLevel3 = node

                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN(" + strlevel4 + ") AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                                        sqlCommand.CommandTimeout = 920000
                                        r4 = sqlCommand.ExecuteReader()
                                        dt4.Clear()
                                        dt4.Load(r4)
                                        r4.Close()

                                        For Each dr4 As DataRow In dt4.Rows
                                            If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
                                                node = New TreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
                                                node.Target = dr4.Item(2).ToString()
                                                'node.ImageUrl = dr4.Item(3).ToString()
                                                node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
                                                NodeLevel3.ChildNodes.Add(node)
                                                NodeLevel4 = node

                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN(" + strLevel5 + ") AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                                                sqlCommand.CommandTimeout = 920000
                                                r5 = sqlCommand.ExecuteReader()
                                                dt5.Clear()
                                                dt5.Load(r5)
                                                r5.Close()
                                                For Each dr5 As DataRow In dt5.Rows
                                                    If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
                                                        node = New TreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
                                                        node.Target = dr5.Item(2).ToString()
                                                        'node.ImageUrl = dr5.Item(3).ToString()
                                                        node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
                                                        NodeLevel4.ChildNodes.Add(node)
                                                        NodeLevel5 = node

                                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN(" + strlevel6 + ") AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                                                        sqlCommand.CommandTimeout = 920000
                                                        r6 = sqlCommand.ExecuteReader()
                                                        dt6.Clear()
                                                        dt6.Load(r6)
                                                        r6.Close()
                                                        For Each dr6 As DataRow In dt6.Rows
                                                            If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
                                                                node = New TreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
                                                                node.Target = dr6.Item(2).ToString()
                                                                'node.ImageUrl = dr6.Item(3).ToString()
                                                                node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
                                                                NodeLevel5.ChildNodes.Add(node)
                                                                NodeLevel6 = node

                                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN(" + strlevel7 + ") AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                                                                sqlCommand.CommandTimeout = 920000
                                                                r7 = sqlCommand.ExecuteReader()
                                                                dt7.Clear()
                                                                dt7.Load(r7)
                                                                r7.Close()
                                                                For Each dr7 As DataRow In dt7.Rows
                                                                    If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
                                                                        node = New TreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
                                                                        node.Target = dr7.Item(2).ToString()
                                                                        'node.ImageUrl = dr7.Item(3).ToString()
                                                                        node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
                                                                        NodeLevel6.ChildNodes.Add(node)
                                                                        NodeLevel7 = node

                                                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN(" + strlevel8 + ") AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                                                                        sqlCommand.CommandTimeout = 920000
                                                                        r8 = sqlCommand.ExecuteReader()
                                                                        dt8.Clear()
                                                                        dt8.Load(r8)
                                                                        r8.Close()
                                                                        For Each dr8 As DataRow In dt8.Rows
                                                                            If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
                                                                                node = New TreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
                                                                                node.Target = dr8.Item(2).ToString()
                                                                                'node.ImageUrl = dr8.Item(3).ToString()
                                                                                node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
                                                                                NodeLevel7.ChildNodes.Add(node)
                                                                            End If
                                                                        Next
                                                                    End If
                                                                Next
                                                            End If
                                                        Next
                                                    End If
                                                Next
                                            End If
                                        Next
                                    End If
                                Next
                            End If
                        Next


                    End If
                Next

            Else
                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No + '  ' + NameE IN ( " + strCategories + " ) AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                sqlCommand.CommandTimeout = 920000
                r1 = sqlCommand.ExecuteReader()
                dt1.Load(r1)
                r1.Close()

                For Each dr1 As DataRow In dt1.Rows
                    If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
                        node = New TreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
                        node.Target = dr1.Item(2).ToString()
                        'node.ImageUrl = dr1.Item(3).ToString()
                        node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
                        tvLevels.Nodes.Add(node)
                        NodeLevel1 = node

                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN(" + strlevel2 + ") AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"

                        'If (strlevel2.Length > 0) Then
                        '    sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN('" + strlevel2 + "') ORDER BY No ASC"
                        'Else
                        '    sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        'End If

                        sqlCommand.CommandTimeout = 920000
                        r2 = sqlCommand.ExecuteReader()
                        dt2.Clear()
                        dt2.Load(r2)
                        r2.Close()
                        For Each dr2 As DataRow In dt2.Rows
                            If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
                                node = New TreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
                                node.Target = dr2.Item(2).ToString()
                                'node.ImageUrl = dr2.Item(3).ToString()
                                node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
                                NodeLevel1.ChildNodes.Add(node)
                                NodeLevel2 = node

                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN(" + strlevel3 + ") AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                                'If (strlevel3.Length > 0) Then
                                '    sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN('" + strlevel3 + "') ORDER BY No ASC"
                                'Else
                                '    sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                                'End If

                                sqlCommand.CommandTimeout = 920000
                                r3 = sqlCommand.ExecuteReader()
                                dt3.Clear()
                                dt3.Load(r3)
                                r3.Close()
                                For Each dr3 As DataRow In dt3.Rows
                                    If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
                                        node = New TreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
                                        node.Target = dr3.Item(2).ToString()
                                        'node.ImageUrl = dr3.Item(3).ToString()
                                        node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
                                        NodeLevel2.ChildNodes.Add(node)
                                        NodeLevel3 = node

                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN(" + strlevel4 + ") AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                                        'If (strlevel4.Length > 0) Then
                                        '    sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN('" + strlevel4 + "') ORDER BY No ASC"
                                        'Else
                                        '    sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                                        'End If

                                        sqlCommand.CommandTimeout = 920000
                                        r4 = sqlCommand.ExecuteReader()
                                        dt4.Clear()
                                        dt4.Load(r4)
                                        r4.Close()

                                        For Each dr4 As DataRow In dt4.Rows
                                            If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
                                                node = New TreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
                                                node.Target = dr4.Item(2).ToString()
                                                'node.ImageUrl = dr4.Item(3).ToString()
                                                node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
                                                NodeLevel3.ChildNodes.Add(node)
                                                NodeLevel4 = node

                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN(" + strLevel5 + ") AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"

                                                'If (strLevel5.Length > 0) Then
                                                '    sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN('" + strLevel5 + "') ORDER BY No ASC"
                                                'Else
                                                '    sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                                                'End If

                                                sqlCommand.CommandTimeout = 920000
                                                r5 = sqlCommand.ExecuteReader()
                                                dt5.Clear()
                                                dt5.Load(r5)
                                                r5.Close()
                                                For Each dr5 As DataRow In dt5.Rows
                                                    If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
                                                        node = New TreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
                                                        node.Target = dr5.Item(2).ToString()
                                                        'node.ImageUrl = dr5.Item(3).ToString()
                                                        node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
                                                        NodeLevel4.ChildNodes.Add(node)
                                                        NodeLevel5 = node

                                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN(" + strlevel6 + ")  AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                                                        'If (strlevel6.Length > 0) Then
                                                        '    sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN('" + strlevel6 + "') ORDER BY No ASC"
                                                        'Else
                                                        '    sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                                                        'End If

                                                        sqlCommand.CommandTimeout = 920000
                                                        r6 = sqlCommand.ExecuteReader()
                                                        dt6.Clear()
                                                        dt6.Load(r6)
                                                        r6.Close()
                                                        For Each dr6 As DataRow In dt6.Rows
                                                            If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
                                                                node = New TreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
                                                                node.Target = dr6.Item(2).ToString()
                                                                'node.ImageUrl = dr6.Item(3).ToString()
                                                                node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
                                                                NodeLevel5.ChildNodes.Add(node)
                                                                NodeLevel6 = node

                                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN(" + strlevel7 + ") AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                                                                sqlCommand.CommandTimeout = 920000
                                                                r7 = sqlCommand.ExecuteReader()
                                                                dt7.Clear()
                                                                dt7.Load(r7)
                                                                r7.Close()
                                                                For Each dr7 As DataRow In dt7.Rows
                                                                    If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
                                                                        node = New TreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
                                                                        node.Target = dr7.Item(2).ToString()
                                                                        'node.ImageUrl = dr7.Item(3).ToString()
                                                                        node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
                                                                        NodeLevel6.ChildNodes.Add(node)
                                                                        NodeLevel7 = node

                                                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND No IN(" + strlevel8 + ") AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                                                                        sqlCommand.CommandTimeout = 920000
                                                                        r8 = sqlCommand.ExecuteReader()
                                                                        dt8.Clear()
                                                                        dt8.Load(r8)
                                                                        r8.Close()
                                                                        For Each dr8 As DataRow In dt8.Rows
                                                                            If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
                                                                                node = New TreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
                                                                                node.Target = dr8.Item(2).ToString()
                                                                                'node.ImageUrl = dr8.Item(3).ToString()
                                                                                node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
                                                                                NodeLevel7.ChildNodes.Add(node)
                                                                            End If
                                                                        Next
                                                                    End If
                                                                Next
                                                            End If
                                                        Next
                                                    End If
                                                Next
                                            End If
                                        Next
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If

            tvLevels.Visible = True
            sqlConnection.Close()
            tvLevels.ExpandAll()

            'End If
            'ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "HideDiv();", True)
        Catch ex As Exception
            Dim abc As String = ex.Message
        End Try
    End Sub
    Protected Sub tvLevels_SelectedNodeChanged(sender As Object, e As EventArgs) Handles tvLevels.SelectedNodeChanged
        Dim node As TreeNode

        If (tvLevels.Nodes.Count > 0 And tvLevels.SelectedNode.Text <> "") Then

            Dim strClientTypeID As String = "1"
            If (Session("ClientTypeID") IsNot Nothing) Then
                strClientTypeID = Session("ClientTypeID")
            End If

            Dim dt As New System.Data.DataTable
            Dim arrList() As String = tvLevels.SelectedNode.ToolTip.Split(",")
            Dim strNodeNumber As String = arrList(arrList.Length - 1)
            Dim strLevelNo As Integer = Integer.Parse(arrList(0))
            Dim sqlConnection As New SqlConnection()

            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
            sqlConnection.Open()
            Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()

            If (UICulture = "Arabic") Then
                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No, Unit, CrewCode, DO, Cost, MaterialCost, '' SuppNameE, '' SuppNameA FROM MainHierarchy1 WHERE ParentNo = '" + strNodeNumber + "' AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
            Else
                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No, Unit, CrewCode, DO, Cost, MaterialCost, '' SuppNameE, '' SuppNameA FROM MainHierarchy1 WHERE ParentNo = '" + strNodeNumber + "' AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
            End If
            Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
            sqlDataAdapter.Fill(dt)

            'tvLevels.SelectedNode.ChildNodes.Clear()

            grdData.DataSource = dt
            grdData.DataBind()
            ViewState("Lines_Data") = dt

            Label1.Text = tvLevels.SelectedNode.Text
            Label3.Text = "Total " + dt.Rows.Count.ToString() + " records found"

            strLevelNo = strLevelNo + 1
            Dim strNodeNo As String = ""
            Dim bNodeFound As Boolean = False
            For i As Integer = 0 To dt.Rows.Count - 1
                If Not (IsDBNull(dt.Rows(i).Item(0).ToString())) And Not (Trim(dt.Rows(i).Item(0).ToString())) = "" Then

                    For j = 0 To tvLevels.SelectedNode.ChildNodes.Count - 1
                        strNodeNo = tvLevels.SelectedNode.ChildNodes(j).ToolTip.Split(",")(2)
                        If (strNodeNo = Trim(dt.Rows(i).Item(3).ToString())) Then
                            bNodeFound = True
                        End If
                    Next

                    If (bNodeFound = False) Then
                        node = New TreeNode(dt.Rows(i).Item(3).ToString() + " " + dt.Rows(i).Item(1).ToString())
                        node.Target = dt.Rows(i).Item(2).ToString()
                        node.ToolTip = strLevelNo.ToString() + "," + dt.Rows(i).Item(0).ToString() + "," + dt.Rows(i).Item(3).ToString()
                        tvLevels.SelectedNode.ChildNodes.Add(node)
                    End If
                    bNodeFound = False
                End If
            Next

            If (dt.Rows.Count = 0) Then
                tvLevels.SelectedNode.ShowCheckBox = True
            End If

            ShowSuppliersData()

            Dim arrValue() As String = hdnSuppIDs.Value.Split(",")
            For i As Integer = 0 To arrValue.Length - 1
                If (arrValue(i).ToString().Length > 0 And tvLevels.SelectedNode.ToolTip.Split(",")(2) = arrValue(i).Split(":")(0).ToString()) Then
                    SetSelectedRecord(arrValue(i).Split(":")(1).ToString())
                End If
            Next
            tvLevels.SelectedNode.ExpandAll()

            sqlConnection.Close()
        End If
    End Sub
    Private Sub SelectExistingData()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        Dim strValue As String = ""
        Dim arrCategories As String() = objSqlConfig.GetProjectCategories(Session("ProjectID").ToString())
        For i As Integer = 0 To arrCategories.Length - 1
            Dim treeNode As TreeNode = tvLevels.FindNode(arrCategories(i))
            If (treeNode IsNot Nothing) Then
                treeNode.ShowCheckBox = True
                treeNode.Checked = True
                ExpandToRoot(treeNode)
            End If
        Next

        dt = objSqlConfig.GetProjectCostLineItems_1(Session("ProjectID").ToString())
        For i As Integer = 0 To dt.Rows.Count - 1
            strValue = strValue + dt.Rows(i).ItemArray(0).ToString() + ":" + dt.Rows(i).ItemArray(1).ToString() + ":" + dt.Rows(i).ItemArray(2).ToString() + ","
        Next

        'ViewState("Selected_Supplier_Quotes") = strValue
        hdnSuppIDs.Value = strValue
    End Sub
    Private Sub ExpandToRoot(node As TreeNode)
        node.Expand()
        If (node.Parent IsNot Nothing) Then
            ExpandToRoot(node.Parent)
        End If
    End Sub
    Protected Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        If (tvLevels.Nodes.Count = 0) Then
            LoadTreeViewData()
            SelectExistingData()
        End If
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim dt As New System.Data.DataTable
        If (tvLevels.CheckedNodes.Count = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
        Else
            Dim objSqlConfig As New ConfigClassSqlServer()
            Dim strSelectedCategories As String = String.Empty
            For i As Integer = 0 To tvLevels.CheckedNodes.Count - 1
                strSelectedCategories = strSelectedCategories + tvLevels.CheckedNodes(i).ValuePath + "~"
            Next

            strSelectedCategories = strSelectedCategories.Substring(0, strSelectedCategories.Length - 1)

            Dim strLangID As String = "1"
            If (UICulture = "Arabic") Then
                strLangID = "2"
            End If
            If (objSqlConfig.InsertProjectCategories(strSelectedCategories, Session("ProjectID").ToString()) = 0) Then

                objSqlConfig.Delete_Selected_Items(Session("ProjectID"))

                For i As Integer = 0 To tvLevels.CheckedNodes.Count - 1
                    If (objSqlConfig.GetProject_Items_Selection(Session("ProjectID").ToString(), tvLevels.CheckedNodes(i).ValuePath.ToString()).Length = 0) Then
                        objSqlConfig.Insert_Selected_Items(tvLevels.CheckedNodes(i).ValuePath, Session("ProjectID").ToString(), "0", strLangID, tvLevels.CheckedNodes(i).ToolTip.Split(",")(2))
                    End If
                Next

                Dim sqlConnection As New SqlConnection()
                Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
                sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                sqlConnection.Open()

                sqlCommand.CommandText = "SELECT ProjectID, ItemNo, Qty FROM Project_CostLineItems WHERE ProjectID = '" + Session("ProjectID").ToString() + "' "
                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)

                sqlCommand.CommandText = "DELETE FROM Project_CostLineItems WHERE ProjectID = '" + Session("ProjectID").ToString() + "' "
                sqlCommand.ExecuteNonQuery()

                Dim strItemNo As String = String.Empty
                Dim arrValues() As String
                Dim strValue As String = ""
                Dim strDataType As String = ""
                'arrValues = ViewState("Selected_Supplier_Quotes").ToString().Split(",")
                arrValues = hdnSuppIDs.Value.Split(",")
                For i As Integer = 0 To tvLevels.CheckedNodes.Count - 1
                    For j As Integer = 0 To arrValues.Length - 1
                        If (arrValues(j).ToString().Length > 0 And tvLevels.CheckedNodes(i).ToolTip.Split(",")(2) = arrValues(j).Split(":")(0)) Then
                            strValue = arrValues(j).Split(":")(1)
                            strDataType = arrValues(j).Split(":")(2)
                        End If
                    Next
                    strItemNo = tvLevels.CheckedNodes(i).ToolTip.Split(",")(2)
                    objSqlConfig.SaveProjectData_1(Session("ProjectID").ToString(), strItemNo, Session("CompanyID"), strValue, strDataType)
                Next

                For i As Integer = 0 To dt.Rows.Count - 1
                    If (dt.Rows(i).Item(2).ToString().Length > 0) Then
                        sqlCommand.CommandText = "UPDATE Project_CostLineItems SET Qty = '" + dt.Rows(i).Item(2).ToString() + "' WHERE ProjectID = '" + Session("ProjectID").ToString() + "' AND ItemNo = '" + dt.Rows(i).Item(1).ToString() + "' "
                        sqlCommand.ExecuteNonQuery()
                    End If
                Next

                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
                Session("ProjectSaved") = "1"
            End If
        End If
    End Sub
    Protected Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        If (Session("Project_Type").ToString() = "Open") Then
            Response.Redirect("Add_Building.aspx")
        Else
            Response.Redirect("User_Selection.aspx")
        End If
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Button1_Click(sender, Nothing)
        If (Session("IsTemp").ToString().Equals("1")) Then
            Response.Redirect("ProjectCostLineItems.aspx")
        Else
            If (Session("ProjectSaved") = "0") Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
            Else
                Response.Redirect("ProjectCostLineItems.aspx")
            End If
        End If
    End Sub
    Protected Sub tvLevels_TreeNodeCheckChanged(sender As Object, e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvLevels.TreeNodeCheckChanged
        Session("ProjectSaved") = "0"
        Dim strValue = String.Empty
        Dim arrValue() As String
        Dim strExistingvalues As String = ""
        Dim strNewValue As String = ""
        Dim iIndex As Integer = -1

        e.Node.Select()
        ShowSuppliersData()

        Dim index As Integer = 0
        If (e.Node.Checked = True) Then
            'If (ViewState("Selected_Supplier_Quotes") IsNot Nothing) Then
            If (hdnSuppIDs.Value.Length > 0) Then

                arrValue = hdnSuppIDs.Value.Split(",")
                For i As Integer = 0 To arrValue.Length - 1
                    If (e.Node.ToolTip.Split(",")(2).ToString() = arrValue(i).Split(":")(0)) Then
                        index = 1
                    End If
                Next
                If (index = 0) Then
                    'strExistingvalues = ViewState("Selected_Supplier_Quotes").ToString()
                    strExistingvalues = hdnSuppIDs.Value
                    strValue = strExistingvalues + e.Node.ToolTip.Split(",")(2).ToString() + ":"
                    strValue = strValue + GetSelectedRecord() + ","
                    'ViewState("Selected_Supplier_Quotes") = strValue
                    hdnSuppIDs.Value = strValue
                End If
                
            Else
                strValue = e.Node.ToolTip.Split(",")(2).ToString() + ":"
                strValue = strValue + GetSelectedRecord() + ","
                'ViewState("Selected_Supplier_Quotes") = strValue
                hdnSuppIDs.Value = strValue
            End If
        Else
            'If (ViewState("Selected_Supplier_Quotes") IsNot Nothing) Then
            If (hdnSuppIDs.Value.Length > 0) Then
                'strValue = ViewState("Selected_Supplier_Quotes").ToString()
                strValue = hdnSuppIDs.Value
                arrValue = strValue.Split(",")
                For i As Integer = 0 To arrValue.Length - 1
                    If (e.Node.ToolTip.Split(",")(2).ToString() = arrValue(i).Split(":")(0)) Then
                        iIndex = i
                    End If
                Next
                For i As Integer = 0 To arrValue.Length - 1
                    If (i <> iIndex And arrValue(i).ToString().Length > 0) Then
                        strNewValue = strNewValue + arrValue(i).ToString() + ","
                    End If
                Next
                'ViewState("Selected_Supplier_Quotes") = strNewValue
                hdnSuppIDs.Value = strNewValue
            End If
        End If
    End Sub
    Protected Sub grdData_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdData.Sorting
        If (ViewState("SortState") IsNot Nothing And ViewState("SortState") = SortDirection.Ascending) Then
            ViewState("SortDirection") = "asc"
            ViewState("SortState") = 1
        Else
            ViewState("SortDirection") = "desc"
            ViewState("SortState") = 0
        End If
        ViewState("SortExp") = e.SortExpression
        FillGrid()
    End Sub
    Private Sub FillGrid()
        Dim Data As DataTable = ViewState("Lines_Data")
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp") Is Nothing) Then
                If ViewState("SortDirection").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp") & " " & "desc"
                End If

                grdData.DataSource = dv
                grdData.DataBind()
            End If
        End If
    End Sub
    Private Function GetSelectedRecord() As String
        Dim retValue = "-1"
        For i As Integer = 0 To grdData.Rows.Count - 1
            Dim rb As RadioButton = DirectCast(grdData.Rows(i).Cells(10).FindControl("RadioButton1"), RadioButton)
            If rb IsNot Nothing Then
                If rb.Checked Then
                    Dim hf As HiddenField = DirectCast(grdData.Rows(i).Cells(10).FindControl("HiddenField1"), HiddenField)
                    If hf IsNot Nothing Then
                        retValue = hf.Value
                    End If
                    If (grdData.Rows(i).Cells(8).Text = "System Default" Or grdData.Rows(i).Cells(8).Text = "نظام افتراضي") Then
                        retValue = retValue + ":" + "0"
                    Else
                        retValue = retValue + ":" + "1"
                    End If
                    Exit For
                End If
            End If
        Next
        Return retValue
    End Function
    Private Sub SetSelectedRecord(strID As String)
        For i As Integer = 0 To grdData.Rows.Count - 1
            Dim rb As RadioButton = DirectCast(grdData.Rows(i).Cells(10).FindControl("RadioButton1"), RadioButton)
            If rb IsNot Nothing Then
                Dim hf As HiddenField = DirectCast(grdData.Rows(i).Cells(10).FindControl("HiddenField1"), HiddenField)
                If hf IsNot Nothing Then
                    If hf.Value.Equals(strID) Then
                        rb.Checked = True
                        Exit For
                    Else
                        rb.Checked = False
                    End If
                End If
            End If
        Next
    End Sub
    Private Sub ShowSuppliersData()
        Dim dt As New DataTable()
        Dim sqlConnection As New SqlConnection()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        Dim sqlDataAdapter As New SqlDataAdapter()
        If (tvLevels.SelectedNode.ChildNodes.Count = 0) Then
            grdData.Columns(8).Visible = True
            grdData.Columns(9).Visible = True
            grdData.Columns(10).Visible = True

            If (UICulture = "Arabic") Then
                grdData.Columns(8).Visible = False
            Else
                grdData.Columns(9).Visible = False
            End If

            Dim strCostLineItemNo As String = tvLevels.SelectedNode.ToolTip.Split(",")(2)
            sqlCommand.CommandText = "SELECT s.ID, s.CostLineItemNo No, MainHierarchy1.NameE, MainHierarchy1.NameA, s.Unit, s.CrewCode, s.DO, s.Cost, s.MaterialCost, sup.NameE SuppNameE, sup.NameA SuppNameA FROM Supplier_Quot s LEFT JOIN Suppliers sup ON s.SuppID = sup.ID LEFT JOIN MainHierarchy1 ON s.CostLineItemNo = No AND CompID = 0 WHERE CostLineItemNo = '" + strCostLineItemNo + "' UNION ALL SELECT m.ID, m.[No] No, m.NameE, m.NameA, m.Unit, m.CrewCode, m.DO, m.Cost, m.MaterialCost, 'System Default' SuppNameE, 'نظام افتراضي' SuppNameA FROM MainHierarchy1 m WHERE m.CompID = '" + Session("CompanyID") + "' AND No = '" + strCostLineItemNo + "' "
            sqlDataAdapter.SelectCommand = sqlCommand
            SqlDataAdapter.Fill(dt)

            If (dt.Rows.Count > 0) Then
                grdData.DataSource = dt
                grdData.DataBind()

                Dim rb As RadioButton = DirectCast(grdData.Rows(0).Cells(10).FindControl("RadioButton1"), RadioButton)
                rb.Checked = True
                ViewState("Lines_Data") = dt
            Else
                grdData.DataSource = Nothing
                grdData.DataBind()
            End If
        Else
            grdData.Columns(8).Visible = False
            grdData.Columns(9).Visible = False
            grdData.Columns(10).Visible = False
        End If

        sqlConnection.Close()
    End Sub

End Class