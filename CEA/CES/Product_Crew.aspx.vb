﻿Public Class Product_Crew
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                'Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                'Menuhide.Value = "close"
                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                End If
                If (Request.QueryString.Get("ID") IsNot Nothing) Then
                    LoadDataGrid_CrewStructure(Request.QueryString.Get("ID").ToString())
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
        Panel1.Width = grdCrew.Width

    End Sub
    Private Sub LoadDataGrid_CrewStructure(strProdID As String)
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New System.Data.DataTable
        dt = objSqlConfig.GetAssociatedCrewStructures(strProdID, txtSearch1.Text)
        If (dt.Rows.Count > 0) Then
            grdCrew.DataSource = dt
            grdCrew.DataBind()
            Session("CrewList") = dt
        Else
            grdCrew.DataSource = Nothing
            grdCrew.DataBind()
            Session("CrewList") = Nothing
        End If
    End Sub
    Protected Sub grdCrew_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdCrew.Sorting
        If (ViewState("SortState1") IsNot Nothing And ViewState("SortState1") = SortDirection.Ascending) Then
            ViewState("SortDirection1") = "asc"
            ViewState("SortState1") = 1
        Else
            ViewState("SortDirection1") = "desc"
            ViewState("SortState1") = 0
        End If
        ViewState("SortExp1") = e.SortExpression
        FillGrid_Equipment()
    End Sub
    Protected Sub grdCrew_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdCrew.PageIndexChanging
        grdCrew.PageIndex = e.NewPageIndex
        grdCrew.DataBind()
    End Sub
    Private Sub FillGrid_Equipment()
        Dim Data As System.Data.DataTable = grdCrew.DataSource
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp1") Is Nothing) Then
                If ViewState("SortDirection1").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp1") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp1") & " " & "desc"
                End If

                grdCrew.DataSource = dv
                grdCrew.DataBind()
            End If
        End If
    End Sub
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        Response.Redirect("Final_ProductList.aspx")
    End Sub
End Class