﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Final_ProductList.aspx.vb" Inherits="CEA.Final_ProductList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        function ShowConfirm() {
            var strConfirm = '<%=GetGlobalResourceObject("Resource", "ConfirmDelete") %>';
            return confirm(strConfirm);
        }
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "PendingList") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdAlreadyAdded") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdAdded") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorProdNotFound") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdDeleted") %>';
            }
            else if (msgno == 6) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdUpdated") %>';
            }
            else if (msgno == 7) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "AllProductsDeleted") %>';
            }
            alert(msgstring);
        }

        $(document).ready(function () {
            $(".bodyWrapper .bodyContainer").addClass('p-0');
            $("#headerWrapper .toggleNavButton").hide();
        });
    </script>
    <style type="text/css">
        .style2
        {
            width: 317px;
        }
        .style3
        {
            width: 248px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, ProductAttachedWith %>'></asp:Label>
    <asp:Label ID="lblSelectedItem" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel2" runat="server" CssClass="panel">
                <div class="row">
                    <asp:Panel ID="Panel1" runat="server" CssClass="panel">
                        <div class="row">
                            <asp:Label ID="lblProjectName" runat="server" CssClass="ProjectName"></asp:Label>
                        </div>
                        <br />
                        <div style="width: 100%; height: 550px; overflow: scroll">
                            <asp:GridView ID="grdProducts" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                EmptyDataText="No Data" Width="100%" PageSize="40" DataKeyNames="ID,ProdID" HeaderStyle-HorizontalAlign ="Center">
                                <Columns>
                                    <asp:BoundField DataField="MapID" HeaderText="MapID" ItemStyle-Width="0px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="NameE" SortExpression="NameE" HeaderText='<%$ Resources:Resource, NameE %>'
                                        ItemStyle-Width="25%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="NameA" SortExpression="NameA" HeaderText='<%$ Resources:Resource, NameA %>'
                                        ItemStyle-Width="25%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="PropE" SortExpression="PropE" HeaderText='<%$ Resources:Resource, FeatE %>'
                                        ItemStyle-Width="25%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="PropA" SortExpression="PropA" HeaderText='<%$ Resources:Resource, FeatA %>'
                                        ItemStyle-Width="25%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="DescE" SortExpression="DescE" HeaderText='<%$ Resources:Resource, DescriptionE %>'
                                        ItemStyle-Width="25%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="DescA" SortExpression="DescA" HeaderText='<%$ Resources:Resource, DescriptionA %>'
                                        ItemStyle-Width="25%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="Unit" SortExpression="Unit" HeaderText='<%$ Resources:Resource, Unit %>'
                                        ItemStyle-Width="5%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="ManHour" SortExpression="ManHour" HeaderText='<%$ Resources:Resource, ManHour %>'
                                        ItemStyle-Width="5%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="EquipHour" SortExpression="EquipHour" HeaderText='<%$ Resources:Resource, EquipHour %>'
                                        ItemStyle-Width="2%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="Qty" SortExpression="Qty" HeaderText='<%$ Resources:Resource, Qty %>'
                                        ItemStyle-Width="2%" ItemStyle-Wrap="true" />
                                    <asp:CommandField ItemStyle-Width="7%" ShowDeleteButton="true" SelectText='<%$ Resources:Resource, Delete %>' />
                                    <asp:CommandField ItemStyle-Width="7%" ShowSelectButton="true" SelectText='<%$ Resources:Resource, SupplierList %>' />
                                    <asp:CommandField ItemStyle-Width="7%" ShowEditButton="true" EditText='<%$ Resources:Resource, CrewList %>' />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                    <div class="row">
                        <div class="large-12 medium-12 column">
                    <div class="outerDivBtn">
                        <div class="BTNdiv">
                            <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Previous %>'
                                CssClass="backBTN" />
                            <asp:Button ID="btnEmpty" runat="server" Text='<%$ Resources:Resource, EmptyCartList %>'
                                CssClass="BTNDelete"  />
                            <asp:Button ID="Button3" runat="server" Text='<%$ Resources:Resource, Next %>' CssClass="BTNnext"
                               />
                        </div></div></div>
                    </div>
                    <div class="large-12 medium-12 column">
                        <table width="100%" border="0" id="table1" runat="server">
                            <tr>
                                <td class="style3">
                                    <asp:LinkButton ID="LinkButton1" runat="server" Text='<%$ Resources:Resource, GotoStart %>'
                                        Font-Underline="True"></asp:LinkButton>
                                </td>
                                <td class="style2">
                                    <asp:LinkButton ID="LinkButton2" runat="server" Text='<%$ Resources:Resource, GotoYourSelectionList %>'
                                        Font-Underline="True"></asp:LinkButton>
                                </td>
                                <%-- <td>
                        <asp:LinkButton ID="LinkButton3" runat="server" Text='<%$ Resources:Resource, GetSavedSelectionList %>' Font-Underline="True"></asp:LinkButton>
                    </td>--%>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
