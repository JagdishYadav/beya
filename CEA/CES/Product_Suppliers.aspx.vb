﻿Public Class Product_Suppliers
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                'Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                'Menuhide.Value = "close"
                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    'header1.Style.Add("float", "right")
                    'header2.Style.Add("float", "right")
                End If

                If (Request.QueryString.Get("ID") IsNot Nothing) Then
                    LoadDataGrid(Request.QueryString.Get("ID").ToString())
                End If
                Panel1.Width = grdSuppliers.Width
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
    End Sub
    Private Sub LoadDataGrid(strProdID)
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New System.Data.DataTable
        dt = objSqlConfig.GetAssociatedSuppliers(strProdID, txtSearch.Text)
        If (dt.Rows.Count > 0) Then
            grdSuppliers.DataSource = dt
            grdSuppliers.DataBind()
        Else
            grdSuppliers.DataSource = Nothing
            grdSuppliers.DataBind()
        End If
    End Sub
    Protected Sub grdSuppliers_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdSuppliers.Sorting
        If (ViewState("SortState") IsNot Nothing And ViewState("SortState") = SortDirection.Ascending) Then
            ViewState("SortDirection") = "asc"
            ViewState("SortState") = 1
        Else
            ViewState("SortDirection") = "desc"
            ViewState("SortState") = 0
        End If
        ViewState("SortExp") = e.SortExpression
        FillGrid()
    End Sub
    Private Sub FillGrid()
        Dim Data As System.Data.DataTable = grdSuppliers.DataSource
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp") Is Nothing) Then
                If ViewState("SortDirection").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp") & " " & "desc"
                End If

                grdSuppliers.DataSource = dv
                grdSuppliers.DataBind()
            End If
        End If
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        LoadDataGrid(Request.QueryString.Get("ID").ToString())
    End Sub
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        Response.Redirect("Final_ProductList.aspx")
    End Sub
End Class