﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="ClientBOQ.aspx.vb" Inherits="CEA.ClientBOQ" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .grid-row {
            background-color: white;
        }

        .alt-row {
            background-color: white;
        }

        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }

        .EmptyRowStyle td {
            height: 296px;
        }

        #divTreeViewScrollTo a {
            display: none;
        }

        #divTreeViewScrollTo td a {
            display: table-cell;
        }
    </style>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript">

        function RadioCheck(rb) {

            var gv = document.getElementById("<%=grdData.ClientID%>");
            var rbs = gv.getElementsByTagName("input");
            var nodeNo;
            var gridIndex = 1;

            for (var i = 0; i < rbs.length; i++) {

                if (rbs[i].type == "radio") {

                    if (rbs[i] == rb) {

                        if (i == 0)
                            gridIndex = 1;
                        else if (i == 2)
                            gridIndex = 2;
                        else if (i == 4)
                            gridIndex = 3;
                        else
                            gridIndex = (i - ((i / 2) - 1));
                    }
                    if (rbs[i].checked && rbs[i] != rb) {

                        rbs[i].checked = false;
                    }
                }
            }

            var treeView = $find("<%= RadTreeView1.ClientID %>");
            nodeNo = treeView.get_selectedNodes()[0].get_text().split(' ')[0];

            var suppID;
            var DataType = "1";

            if (gv.rows.length <= 2) {
                DataType = "0";
            }

            for (var j = 0; j < rbs.length; j++) {

                if (rbs[j].type == "radio" && rbs[j].checked) {

                    suppID = gv.rows[gridIndex].cells[9].innerHTML;

                    if (gv.rows[gridIndex].cells[7].innerHTML == "System Default" || gv.rows[gridIndex].cells[7].innerHTML == "نظام افتراضي") {
                        DataType = "0";
                    }
                }
            }

            var hiddenField = document.getElementById("<%= hdnSuppIDs.ClientID%>").value;
            var varArray = hiddenField.split(",");
            var newValue = "";

            for (var i = 0; i < varArray.length; i++) {
                if (varArray[i].length > 0) {
                    var innerArray = varArray[i].split(":")
                    if (innerArray[0] == nodeNo) {
                        innerArray[1] = suppID;
                        innerArray[2] = DataType;
                    }
                    newValue = newValue + innerArray[0] + ":" + innerArray[1] + ":" + innerArray[2] + ","
                }
            }

            //if (newValue.length == 0) {
            //    newValue = newValue + nodeNo + ":" + suppID + ":" + DataType + ","
            //}

            document.getElementById("<%= hdnSuppIDs.ClientID%>").value = newValue;
        }

        function updateHiddenField(sender, eventArgs) {
            var nodeNo = eventArgs.get_node().get_text().split(' ')[0];
            var isChecked = eventArgs.get_node().get_checked();
            var gv = document.getElementById("<%=grdData.ClientID%>");
            var rbs = gv.getElementsByTagName("input");
            var hiddenField = document.getElementById("<%= hdnSuppIDs.ClientID%>").value;
            var varArray = hiddenField.split(",");
            var newValue = "";
            var gridIndex = 1;
            var suppID;
            var DataType = "1";
            var index = -1;

            if (gv.rows.length <= 2) {
                DataType = "0";
            }

            for (var j = 0; j < rbs.length; j++) {

                if (rbs[j].type == "radio" && rbs[j].checked) {

                    if (j == 0)
                        gridIndex = 1;
                    else if (j == 2)
                        gridIndex = 2;
                    else if (j == 4)
                        gridIndex = 3;
                    else
                        gridIndex = (j - ((j / 2) - 1));

                    suppID = gv.rows[gridIndex].cells[9].innerHTML;
                    if (gv.rows[1].cells[7].innerHTML == "System Default" || gv.rows[1].cells[7].innerHTML == "نظام افتراضي") {
                        DataType = "0";
                    }
                }
            }

            if (isChecked == true) {

                var Found = 0;
                for (var i = 0; i < varArray.length; i++) {

                    if (varArray[i].length > 0) {
                        var innerArray = varArray[i].split(":")

                        if (innerArray[0] == nodeNo) {

                            innerArray[1] = suppID;
                            innerArray[2] = DataType;
                            Found = 1;
                        }

                        newValue = newValue + innerArray[0] + ":" + innerArray[1] + ":" + innerArray[2] + ","
                    }
                }

                if (newValue.length == 0 || Found == 0) {
                    newValue = newValue + nodeNo + ":" + suppID + ":" + DataType + ","
                }
            }

            else {

                for (var i = 0; i < varArray.length; i++) {
                    if (varArray[i].length > 0) {
                        var innerArray = varArray[i].split(":")
                        if (innerArray[0] == nodeNo) {
                            index = i;
                        }
                    }
                }
                for (var i = 0; i < varArray.length; i++) {
                    if (varArray[i].length > 0 && i != index) {
                        var innerArray = varArray[i].split(":")
                        newValue = newValue + innerArray[0] + ":" + innerArray[1] + ":" + innerArray[2] + ","
                    }
                }
            }

            document.getElementById("<%= hdnSuppIDs.ClientID%>").value = newValue;
        }

        function addNodes(sender, eventArgs) {
            treeView = $find("<%= RadTreeView1.ClientID %>");
            var selected = treeView.get_selectedNode();

            if (selected.get_allNodes().length == 0) {
                var NodeNumber = treeView.get_selectedNodes()[0].get_text().split(' ');
                GetNodes(NodeNumber[0]);

                if (selected.get_toolTip().split(',')[3] == "0") {
                    for (var i = 0; i < selected.get_parent().get_allNodes().length; i++) {
                        selected.get_parent().get_allNodes()[i].get_checkBoxElement().disabled = true;
                    }
                    selected.get_checkBoxElement().disabled = false;
                }
            }

            if (selected.get_toolTip().split(',')[3] == "0")
                GetNodes1(NodeNumber[0]);

            return true;
        }

        function GetNodes(nodeNumber) {
            var compID = document.getElementById('<%= hdnCompID.ClientID %>').value;
            var langID = document.getElementById('<%= hdnLangID.ClientID %>').value;
            var clientType = document.getElementById('<%= hdnClientTypeID.ClientID %>').value;

            $.ajax({
                type: "POST",
                url: "ClientBOQ.aspx/GetNodesData",
                data: '{nodeNumber: ' + nodeNumber + ',compID: ' + compID + ',langID: ' + langID + ',clientType: ' + clientType + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
        }

        function GetNodes1(nodeNumber) {
            var compID = document.getElementById('<%= hdnCompID.ClientID %>').value;
            var langID = document.getElementById('<%= hdnLangID.ClientID %>').value;
            var clientType = document.getElementById('<%= hdnClientTypeID.ClientID %>').value;

            $.ajax({
                type: "POST",
                url: "ClientBOQ.aspx/GetNodesData1",
                data: '{nodeNumber: ' + nodeNumber + ',compID: ' + compID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess1,
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
        }

        function OnSuccess(response) {

            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            var nodes = xml.find("Nodes");

            treeView = $find("<%= RadTreeView1.ClientID %>");
            var selected = treeView.get_selectedNode();
            var nodesCount = 0;
            $.each(nodes, function () {

                nodesCount = 1;
                var toolTip = '1,' + $(this).find("ID").text() + ',' + $(this).find("No").text() + ',' + $(this).find("Cnt").text();
                treeView.trackChanges();
                var newNode = new Telerik.Web.UI.RadTreeNode();
                newNode.set_text($(this).find("No").text() + ' ' + $(this).find("Name1").text());
                newNode.set_toolTip(toolTip);
                newNode.set_target($(this).find("Name2").text());
                selected.get_nodes().add(newNode);
                newNode.get_checkBoxElement().disabled = true;
                treeView.commitChanges();

                //if ($(this).find("Cnt").text() == "0") {
                //    var checkBoxElement = newNode.get_checkBoxElement();
                //    if (checkBoxElement != null) {
                //        checkBoxElement.disabled = true;
                //    }
                //}
                //else {
                //    var checkBoxElement = newNode.get_checkBoxElement();
                //    if (checkBoxElement != null) {
                //        checkBoxElement.disabled = true;
                //    }
                //}

            });

            //if (nodesCount == 0) {
            //    var NodeNumber1 = treeView.get_selectedNodes()[0].get_text().split(' ');
            //    GetNodes1(NodeNumber1[0]);
            //}

        };

        function OnSuccess1(response) {

            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            var nodes = xml.find("Nodes");
            var gridView = document.getElementById("<%=grdData.ClientID%>");
            var NodeNumber = treeView.get_selectedNodes()[0].get_text().split(' ')[0];

            if (NodeNumber != gridView.rows[1].cells[0].innerHTML.toString()) {
                var tbody = gridView.getElementsByTagName("tbody")[0];
                var row1 = tbody.getElementsByTagName("tr")[1];
                var row = row1.cloneNode(true);

                for (var k = 1; k <= gridView.rows.length; k++) {
                    gridView.deleteRow(k);
                }

                var supplierID = "-1";
                var index = 0;

                $.each(nodes, function () {

                    if (index == 0) {
                        supplierID = $(this).find("ID").text();
                        index = 99;
                    }

                    row.cells[0].innerHTML = $(this).find("No").text();
                    row.cells[1].innerHTML = $(this).find("NameE").text();
                    row.cells[2].innerHTML = $(this).find("Unit").text();
                    row.cells[3].innerHTML = $(this).find("CrewCode").text();
                    row.cells[4].innerHTML = $(this).find("DO").text();
                    row.cells[5].innerHTML = $(this).find("Cost").text();
                    row.cells[6].innerHTML = $(this).find("MaterialCost").text();
                    row.cells[7].innerHTML = $(this).find("SuppNameE").text();
                    row.cells[9].innerHTML = $(this).find("ID").text();
                    tbody.appendChild(row);
                    row = row1.cloneNode(true);
                });

                if (gridView.rows.length > 2 && gridView.rows[1].cells[9].innerHTML.toString() != supplierID)
                    gridView.deleteRow(1);

                var rbs = gridView.getElementsByTagName("input");
                for (var i = 0; i < rbs.length; i++) {

                    if (rbs[i].type == "radio") {

                        rbs[i].checked = true;
                        break;
                    }
                }
            }

        };

        function checkMapping(lnk) {
            var row = lnk.parentNode.parentNode;
            if (row.cells[0].style.backgroundColor != "green") {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NotMapped") %>';
                alert(msgstring);
                return false;
            }
            else {
                return true;
            }
        }
        function CheckPreConditions() {

            treeView = $find("<%= RadTreeView1.ClientID %>");
            var Qty = document.getElementById('<%= txtQty.ClientID %>').value;
            var rowSelected = document.getElementById('<%= hdnVal.ClientID %>').value;
            var retValue = 1;

            if (treeView.get_checkedNodes().length == 0) {
                retValue = 18;
            }
            if (Qty.length == 0 || isNaN(Qty)) {
                retValue = 19;
            }
            if (rowSelected.length == 0) {
                retValue = 16;
            }

            if (retValue == 18) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SelectLeafNode") %>';
                alert(msgstring);
                return false;
            }
            else if (retValue == 19) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NumericQty") %>';
                alert(msgstring);
                return false;
            }
            else if (retValue == 16) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SelectBOQ") %>';
                    alert(msgstring);
                    return false;
                }
                else {
                    return true;
                }
    }
    function ShowMessage(msgno) {
        var ctl = document.getElementById('<%=RadTreeView1.ClientID%>');
        var input = ctl.getElementsByTagName('input');
        for (i = 0; i < input.length; i++) {
            input[i].disabled = true;
        }

        if (msgno == -1) {
            var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
        }
        else if (msgno == 1) {
            var msgstring = '<%=GetGlobalResourceObject("Resource", "ProjSaved") %>';
        }
        else if (msgno == 2) {
            var msgstring = '<%=GetGlobalResourceObject("Resource", "ProjAlreadyExist") %>';
        }
        else if (msgno == 3) {
            var msgstring = '<%=GetGlobalResourceObject("Resource", "SaveFirst") %>';
        }
        else if (msgno == 4) {
            var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorProjName") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "PleaseSelectOption") %>';
            }
            else if (msgno == 13) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NotExcel") %>';
            }
            else if (msgno == 14) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Imported") %>';
            }
            else if (msgno == 15) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ImportFailure") %>';
            }
            else if (msgno == 16) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SelectBOQ") %>';
            }
            else if (msgno == 17) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SelectItem") %>';
            }
            else if (msgno == 18) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SelectLeafNode") %>';
            }
            else if (msgno == 19) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NumericQty") %>';
            }
            else if (msgno == 20) {

                var msgstring = '<%=GetGlobalResourceObject("Resource", "MapItem") %>';
            }
            else if (msgno == 21) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "MappingSuccess") %>';
            }
            else if (msgno == 22) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NotMapped") %>';
            }
            else if (msgno == 99) {
                var msgstring = "";
            }

    alert(msgstring);
}

function DisableAllCheckboxes() {
    var ctl = document.getElementById('<%=RadTreeView1.ClientID%>');
    var input = ctl.getElementsByTagName('input');
    for (i = 0; i < input.length; i++) {
        input[i].disabled = true;
    }
}
function Showopoup(url) {

    params = 'width=' + screen.width;
    params += ', height=' + screen.height;
    params += ', top=0, left=0'
    params += ', fullscreen=yes';
    params += ', menubar=no, status=no, scrollbars=yes,titlebar=no, toolbar=no,directories=no';
    var win = window.open('', 'Crew Structure', params);
    win.location.href = url
    win = null;
    return false;
}

var prevRow;
function setSelected(selRow) {
    //var tbl = document.getElementById("<%= grdBOQ.ClientID %>");

    if (selRow.cells[0].style.backgroundColor != "green") {
        selRow.cells[0].style.backgroundColor = "Yellow";
        selRow.cells[1].style.backgroundColor = "Yellow";
        selRow.cells[2].style.backgroundColor = "Yellow";
        selRow.cells[3].style.backgroundColor = "Yellow";
        selRow.cells[4].style.backgroundColor = "Yellow";
        selRow.cells[5].style.backgroundColor = "Yellow";
    }
    if (prevRow != null && prevRow.rowIndex != document.getElementById("<%= hdnMappedRowIndex.ClientID %>").value && prevRow.cells[0].style.backgroundColor != "green") {
                prevRow.cells[0].style.backgroundColor = "White";
                prevRow.cells[1].style.backgroundColor = "White";
                prevRow.cells[2].style.backgroundColor = "White";
                prevRow.cells[3].style.backgroundColor = "White";
                prevRow.cells[4].style.backgroundColor = "White";
                prevRow.cells[5].style.backgroundColor = "White";
            }

            prevRow = selRow;
    //            for (var i = 1; i < tbl.rows.length; i++) {
    //            
    //                if (selRow.rowIndex == i) {
    //                    selRow.cells[0].style.backgroundColor = "Yellow";
    //                    selRow.cells[1].style.backgroundColor = "Yellow";
    //                    selRow.cells[2].style.backgroundColor = "Yellow";
    //                    selRow.cells[3].style.backgroundColor = "Yellow";
    //                    selRow.cells[4].style.backgroundColor = "Yellow";
    //                    selRow.cells[5].style.backgroundColor = "Yellow";
    //                }   

    //                else {

    //                    tbl.rows[i].cells[0].style.backgroundColor = "White";
    //                    tbl.rows[i].cells[1].style.backgroundColor = "White";
    //                    tbl.rows[i].cells[2].style.backgroundColor = "White";
    //                    tbl.rows[i].cells[3].style.backgroundColor = "White";
    //                    tbl.rows[i].cells[4].style.backgroundColor = "White";
    //                    tbl.rows[i].cells[5].style.backgroundColor = "White";
    //                }
    //            }

            document.getElementById("<%= hdnVal.ClientID %>").value = selRow.rowIndex;
            document.getElementById("<%= hdnVal1.ClientID %>").value = selRow.cells[5].innerHTML;
            document.getElementById("<%= txtQty.ClientID %>").value = selRow.cells[5].innerHTML;
}

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, BOQMapping %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <script type="text/javascript">

        var xPos, yPos, xPos1, yPos1;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xPos = $get('scrollDiv').scrollLeft;
            yPos = $get('scrollDiv').scrollTop;

            xPos1 = $get('divTreeViewScrollTo').scrollLeft;
            yPos1 = $get('divTreeViewScrollTo').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('scrollDiv').scrollLeft = xPos;
            $get('scrollDiv').scrollTop = yPos;

            $get('divTreeViewScrollTo').scrollLeft = xPos1;
            $get('divTreeViewScrollTo').scrollTop = yPos1;
        }

    </script>
    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="panel">
                <div class="row">
                    <div class="large-6 medium-6 columns">
                        <asp:FileUpload ID="uplImage" runat="server" Width="250px" CssClass="left" />
                        <asp:LinkButton ID="LinkButton2" runat="server" CssClass="BTNImport" Text='<%$ Resources:Resource, ImportFromExcel %>'></asp:LinkButton>
                    </div>
                    <div class="large-6 medium-6 columns">
                        <asp:TextBox ID="txtQty" runat="server" Width="70px" CssClass="right"></asp:TextBox>
                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, Qty %>' CssClass="right"
                            Style="margin-right: 10px; margin-top: 10px;"></asp:Label>
                        <asp:LinkButton ID="lnkViewDoc" runat="server" Text='<%$ Resources:Resource, ViewClientBOQDoc %>'
                            CssClass="right BTNView smallbtn"></asp:LinkButton>
                        <asp:LinkButton ID="lnkDownload" runat="server" Text='<%$ Resources:Resource, DownloadDoc %>'
                            CssClass="right BTNImport smallbtn"></asp:LinkButton>
                    </div>
                </div>
                <asp:Label ID="Label2" runat="server" Text="Label" Width="800px" Visible = "false"></asp:Label>
                <div class="row">
                    <div id="scrollDiv" class="large-5 medium-5 columns height350" >
                        <asp:GridView ID="grdBOQ" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                            AllowSorting="false" EmptyDataText="No Data"
                            Width="100%" PageSize="99999" DataKeyNames="LineNo" SelectedRowStyle-BackColor="Yellow"
                            ShowHeaderWhenEmpty="true">
                            <Columns>
                                <asp:BoundField DataField="Div" HeaderText='<%$ Resources:Resource, Div1 %>'
                                    ItemStyle-Width="2%" />
                                <asp:BoundField DataField="No" HeaderText='<%$ Resources:Resource, LineItemNo %>'
                                    ItemStyle-Width="11%" />
                                <asp:BoundField DataField="NameE" HeaderText='<%$ Resources:Resource, NameEng %>'
                                    ItemStyle-Width="38%" />
                                <asp:BoundField DataField="NameA" HeaderText='<%$ Resources:Resource, NameAra %>'
                                    ItemStyle-Width="38%" />
                                <asp:BoundField DataField="Unit" HeaderText='<%$ Resources:Resource, Unit %>' ItemStyle-Width="8%" />
                                <asp:BoundField DataField="Qty" HeaderText='<%$ Resources:Resource, Qty1 %>' ItemStyle-Width="3%" />
                                <asp:CommandField ItemStyle-Width="5%" ShowDeleteButton="true" DeleteText='<%$ Resources:Resource, Mapping %>' />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="large-2 medium-2 columns height350">
                        <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Map %>' CssClass="BTNExport btnMiddle top160" />
                        <div class="clear">
                        </div>
                        <br />
                        <asp:Button ID="Button3" runat="server" Text='<%$ Resources:Resource, Unmap %>' CssClass="BTNCancel btnMiddle" Visible = "false" />
                    </div>
                    <div id="divTreeViewScrollTo" class="large-5 medium-5 columns height350">
                        <asp:TreeView ID="tvLevels" runat="server" LineImagesFolder="~/TreeLineImages" 
                            ShowLines="True" ShowCheckBoxes="None" Visible = "false" Height="344px">
                        </asp:TreeView>
                    </div>
                </div>
                <div class="row">
                     <div class="large-12 medium-12 column">
                    <div class="outerDivBtn">
                        <div class="BTNdiv">
                        <asp:Button ID="Button9" runat="server" Text='<%$ Resources:Resource, Previous %>'
                            CssClass="backBTN rightbtn" />
                            
                        <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave middlebtn" />
                        
                        <asp:Button ID="Button5" runat="server" Text='<%$ Resources:Resource, Next %>' CssClass="BTNnext" /></div>
                    </div></div>
                </div>
                <asp:Button ID="Button100" runat="server" />
                <asp:Button ID="Button200" runat="server" />
                <asp:Button ID="Button4" runat="server" Text="" />
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="LinkButton2"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="Button100" />
            <asp:PostBackTrigger ControlID="Button200" />
        </Triggers>
    </asp:UpdatePanel>--%>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <asp:Label ID="lblProjectName" runat="server" CssClass="ProjectName"></asp:Label>
            </div>
            <br />
            <asp:Panel ID="Panel1" runat="server" CssClass="panel">

                <asp:Label ID="Label2" runat="server" Text="Label" Width="800px" Visible="false"></asp:Label>
                <div class="row">
                    <div id="scrollDiv" class="large-4 medium-4 columns height350">
                        <asp:GridView ID="grdBOQ" runat="server" AutoGenerateColumns="false" EmptyDataText="No Data"
                            Width="100%" DataKeyNames="LineNo" ShowHeaderWhenEmpty="true" CssClass="grid-row" AlternatingRowStyle-CssClass="alt-row" HeaderStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundField DataField="Div" HeaderText='<%$ Resources:Resource, Div1 %>' ItemStyle-Width="2%" />
                                <asp:BoundField DataField="No" HeaderText='<%$ Resources:Resource, LineItemNo %>'
                                    ItemStyle-Width="11%" />
                                <asp:BoundField DataField="NameE" HeaderText='<%$ Resources:Resource, NameEng %>'
                                    ItemStyle-Width="38%" />
                                <asp:BoundField DataField="NameA" HeaderText='<%$ Resources:Resource, NameAra %>'
                                    ItemStyle-Width="38%" />
                                <asp:BoundField DataField="Unit" HeaderText='<%$ Resources:Resource, Unit %>' ItemStyle-Width="8%" />
                                <asp:BoundField DataField="Qty" HeaderText='<%$ Resources:Resource, Qty1 %>' ItemStyle-Width="3%" />
                                <asp:CommandField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center" ShowDeleteButton="true" DeleteText='<%$ Resources:Resource, Mapping %>' />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="large-1 medium-1 columns height350 smallwidth">
                        <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Map %>' CssClass="BTNExport btnMiddle top160" />
                        <div class="clear">
                        </div>
                        <br />
                        <asp:Button ID="Button3" runat="server" Text='<%$ Resources:Resource, Unmap %>' CssClass="BTNCancel btnMiddle"
                            Visible="false" />
                    </div>
                    <div id="divTreeViewScrollTo" class="large-3 medium-3 columns height350 width275">
                        <asp:Panel ID="Panel2" runat="server" CssClass="panel">
                            <%--<asp:TreeView ID="tvLevels" runat="server" LineImagesFolder="~/TreeLineImages" ShowLines="True"
                            ShowCheckBoxes="None" Visible="false" NodeWrap="true" Width="100%">
                        </asp:TreeView>--%>
                            <%-- <telerik:RadTreeView ID="RadTreeView1" runat="server" OnClientNodeClicked = "addNodes" CheckBoxes = "true">
                        </telerik:RadTreeView>--%>
                            <telerik:RadTreeView ID="RadTreeView1" runat="server" OnClientNodeClicked="addNodes" CheckBoxes="true" OnClientNodeChecked="updateHiddenField"></telerik:RadTreeView>
                        </asp:Panel>
                    </div>
                    <div id="div1" class="large-4 medium-4 columns height350" style="width: 522px;">
                        <asp:Panel ID="Panel3" runat="server" CssClass="panel" Style="padding-top: 23px; height: 400px;">
                            <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="250" DataKeyNames="ID" SelectedRowStyle-BackColor="Yellow">
                                <Columns>
                                    <asp:BoundField DataField="No" SortExpression="No" HeaderText='<%$ Resources:Resource, LineNumber %>'
                                        ItemStyle-Width="150px" />
                                    <asp:BoundField DataField="NameE" SortExpression="NameE" HeaderText='<%$ Resources:Resource, Desc %>'
                                        ItemStyle-Width="300px" />
                                    <asp:BoundField DataField="NameA" SortExpression="NameA" HeaderText='<%$ Resources:Resource, Desc %>'
                                        ItemStyle-Width="300px" />
                                    <asp:BoundField DataField="Unit" SortExpression="Unit" HeaderText='<%$ Resources:Resource, Unit %>'
                                        ItemStyle-Width="100px" />
                                    <asp:BoundField DataField="CrewCode" SortExpression="CrewCode" HeaderText='<%$ Resources:Resource, CrewCode %>'
                                        ItemStyle-Width="100px" />
                                    <asp:BoundField DataField="DO" SortExpression="DO" HeaderText='<%$ Resources:Resource, DO %>'
                                        ItemStyle-Width="100px" />
                                    <asp:BoundField DataField="Cost" SortExpression="Cost" HeaderText='<%$ Resources:Resource, Cost %>' ItemStyle-Width="90px" />
                                    <asp:BoundField DataField="MaterialCost" SortExpression="MaterialCost" HeaderText='<%$ Resources:Resource, MaterialCost %>' ItemStyle-Width="100px" />
                                    <asp:BoundField DataField="SuppNameE" SortExpression="SuppNameE" HeaderText='<%$ Resources:Resource, Supplier %>' ItemStyle-Width="90px" />
                                    <asp:BoundField DataField="SuppNameA" SortExpression="SuppNameA" HeaderText='<%$ Resources:Resource, Supplier %>' ItemStyle-Width="90px" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:RadioButton ID="RadioButton1" runat="server" onclick="RadioCheck(this);" />
                                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Eval("ID")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ID" />
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </div>
                </div>

                <asp:Button ID="Button100" runat="server" />
                <asp:Button ID="Button200" runat="server" />
                <asp:Button ID="Button4" runat="server" Text="" />
            </asp:Panel>

            <asp:HiddenField ID="hdnSuppIDs" runat="server" Value="" />
            <asp:HiddenField ID="hdnVal" runat="server" Value="" />
            <asp:HiddenField ID="hdnVal1" runat="server" Value="" />

            <asp:HiddenField ID="hdnCompID" runat="server" />
            <asp:HiddenField ID="hdnLangID" runat="server" />
            <asp:HiddenField ID="hdnClientTypeID" runat="server" />
            <asp:HiddenField ID="hdnMappedRowIndex" runat="server" Value="-1" />
        </ContentTemplate>
        <%-- <Triggers>
            <asp:PostBackTrigger ControlID="LinkButton2"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="Button100" />
            <asp:PostBackTrigger ControlID="Button200" />
        </Triggers>--%>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MiddlePart">

    <div class="row">
        <div class="large-12 medium-12 column">
            <div class="outerDivBtn">
                <div class="BTNdiv">
                    <asp:Button ID="Button9" runat="server" Text='<%$ Resources:Resource, Previous %>'
                        CssClass="backBTN rightbtn" />
                    <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave middlebtn" Visible="false" />
                    <asp:Button ID="Button5" runat="server" Text='<%$ Resources:Resource, Next %>' CssClass="BTNnext" />
                </div>
                <%--<asp:Button ID="Button6" runat="server" Text="Previous Page" CssClass="BTNnext" /></div>
                                <asp:Button ID="Button7" runat="server" Text="Next Page" CssClass="BTNnext" /></div>--%>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="large-6 medium-6 columns">
            <asp:FileUpload ID="uplImage" runat="server" Width="250px" CssClass="small button left " Style="margin: 1px 9px !important" />
            <asp:LinkButton ID="LinkButton2" runat="server" CssClass="BTNImport" Text='<%$ Resources:Resource, ImportFromExcel %>'></asp:LinkButton>
        </div>
        <div class="large-6 medium-6 columns">

            <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, Qty %>' CssClass="right"></asp:Label><asp:TextBox ID="txtQty" runat="server" Width="70px" CssClass="right"></asp:TextBox>
            <asp:LinkButton ID="lnkViewDoc" runat="server" Text='<%$ Resources:Resource, ViewClientBOQDoc %>'
                CssClass="right BTNView"></asp:LinkButton>
            <asp:LinkButton ID="lnkDownload" runat="server" Text='<%$ Resources:Resource, DownloadDoc %>'
                CssClass="right BTNImport"></asp:LinkButton>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $(".bodyWrapper .bodyContainer").addClass('p-0');
            $("#headerWrapper .toggleNavButton").hide();
        });
        $(document).ready(function () { $("#sidr").show(); });
    </script>
</asp:Content>

