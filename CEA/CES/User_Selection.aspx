﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="User_Selection.aspx.vb" Inherits="CEA.User_Selection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript">
        //$(function () {
        //    $('#sidr').hide();
        //});
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProjSaved") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "BuildingAlreadyExist") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SaveFirst") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorSelectUser") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "PleaseSelectOption") %>';
            }

            alert(msgstring);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, SelectUsers %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="">
                <div class="row justify-content-between">
                    <div class="col-12 col-md-auto">
                        <asp:Label ID="lblProjectName" runat="server" Text="ProjectName" CssClass="ProjectName mt-2 d-block font-weight-bold"></asp:Label>
                    </div>
                    <div class="col-12 col-md-auto">
                        <div class="text-right mb-3 buttonActionGroup">
                            <div class="btn-group commanButtonGroup mb-3">
                                <asp:Button ID="Button9" runat="server" Text='<%$ Resources:Resource, Previous %>' CssClass="btn btn-primary btn-min-120" />
                                <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="btn btn-primary btn-min-120" Visible="false" />
                                <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Next %>' CssClass="btn btn-primary btn-min-120" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive tableLayoutWrap altboarder">
                            <asp:GridView ID="grdusers" runat="server" AutoGenerateColumns="false" EmptyDataText="No Data"
                                Width="100%" PageSize="25" AllowPaging="true" DataKeyNames="ID" HeaderStyle-HorizontalAlign="Center">
                                <Columns>
                                    <asp:BoundField DataField="UserID" HeaderText='<%$ Resources:Resource, UserName %>'
                                        ItemStyle-Width="90%">
                                        <ItemStyle Width="90%" />
                                    </asp:BoundField>
                                    <asp:TemplateField ItemStyle-VerticalAlign="Middle" HeaderText='<%$ Resources:Resource, Select %>' ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" OnCheckedChanged="CheckBox1_CheckedChanged" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>

            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script>
        $(document).ready(function () {
            $(".bodyWrapper .bodyContainer").addClass('p-0');
            $("#headerWrapper .toggleNavButton").hide();
        });

        $(document).ready(function () { $("#sidr").show(); });
    </script>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MiddlePart">
    <%--<div class="row">
        <div class="large-12 medium-12 column">
            <div class="outerDivBtn">
                <div class="BTNdiv">
                    <asp:Button ID="Button9" runat="server" Text='<%$ Resources:Resource, Previous %>' CssClass="backBTN" />
                    <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave" Visible="false" />
                    <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Next %>' CssClass="BTNnext" />
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>

