﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Product_Crew.aspx.vb" Inherits="CEA.Product_Crew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#sidr').hide();
        });
        function ShowMessage(msgno) {
            if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorSelectProf") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProfAlreadyAdded") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProfAdded") %>';
            }

            alert(msgstring);
        }

        $(document).ready(function () {
            $(".bodyWrapper .bodyContainer").addClass('p-0');
            $("#headerWrapper .toggleNavButton").hide();
        });
    </script>
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Crew Structure
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="large-12 medium-12 columns">
                    <h5>
                        <asp:Label ID="lblManpower" runat="server" Text='<%$ Resources:Resource, AssociatedCrewList %>'></asp:Label></h5>
                </div>
            </div>
            <asp:Panel ID="Panel1" runat="server" CssClass="panel">
                <div class="row">
                    <div class="large-6 medium-6 column">
                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="backBTN" Text='<%$ Resources:Resource, Back %>'></asp:LinkButton>
                        <asp:Label ID="Label15" runat="server" Text='<%$ Resources:Resource, Search %>' Width="100%"></asp:Label>
                        <asp:TextBox ID="txtSearch1" runat="server" CssClass="left" Width="70%"></asp:TextBox><asp:Button
                            ID="Button5" runat="server" Text="Search" CssClass="BTNSearch" />
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 medium-12 columns">
                        <div style="width: 100%; height: 650px; overflow: scroll">
                            <asp:GridView ID="grdCrew" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="15" DataKeyNames="Code" HeaderStyle-HorizontalAlign ="Center">
                                <Columns>
                                    <asp:BoundField DataField="Code" SortExpression="Code" HeaderText='<%$ Resources:Resource, Code %>'
                                        ItemStyle-Width="45px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="DescE" SortExpression="DescE" HeaderText='<%$ Resources:Resource, DescriptionE %>'
                                        ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="DescA" SortExpression="DescA" HeaderText='<%$ Resources:Resource, DescriptionA %>'
                                        ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="ProductionRate" SortExpression="ProductionRate" HeaderText='<%$ Resources:Resource, ProductionRate %>'
                                        ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="HourlyRate" SortExpression="HourlyRate" HeaderText='<%$ Resources:Resource, HourlyRate %>'
                                        ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
