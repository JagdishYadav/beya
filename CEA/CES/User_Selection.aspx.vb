﻿Public Class User_Selection
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            'Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
            'Menuhide.Value = "close"
            Dim linkButton As LinkButton = Master.FindControl("LinkButton1")
            linkButton.Visible = False
            Session("ProjectSaved") = "0"
            lblProjectName.Text = Session("ProjectName") + " / " + Session("BuildingName")
            If (Request.IsAuthenticated) Then
                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
            LoadGrid()
            LoadData()

            If (Session("IsTemp").ToString().Equals("1")) Then
                grdusers.Enabled = False
            End If

            If (Session("User_Role_ID") = "999") Then
                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (Session("User_Role_ID").ToString().Contains("999")) Then
                    If (menuItems.Count > 15) Then
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                    End If
                ElseIf (Session("User_Role_ID").ToString().Contains("888")) Then
                    If (menuItems.Count > 5) Then
                        menuItems.RemoveAt(0)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                    End If
                End If
            Else
                Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                Menuhide.Value = "close"
            End If

        End If
    End Sub
    Protected Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Response.Redirect("Add_Building.aspx")
    End Sub
    Protected Sub LoadGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New System.Data.DataTable
        dt = objSqlConfig.GetAllUsers(Session("CompanyID"))
        If (dt.Rows.Count > 0) Then
            grdusers.DataSource = dt
            grdusers.DataBind()
        Else
            grdusers.DataSource = Nothing
            grdusers.DataBind()
        End If
    End Sub
    Protected Sub LoadData()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim arrUsers As String()
        arrUsers = objSqlConfig.GetProjectUsers(Session("ProjectName").ToString(), Session("BuildingName").ToString(), Session("CompanyID").ToString())
        If (arrUsers.Length > 0) Then
            For i As Integer = 0 To grdusers.Rows.Count - 1
                For j As Integer = 0 To arrUsers.Length - 1
                    If (arrUsers(j).ToString() = grdusers.DataKeys(i).Value.ToString()) Then
                        Dim cbox As CheckBox = grdusers.Rows(i).FindControl("CheckBox1")
                        cbox.Checked = True
                    End If
                Next
            Next
        End If
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim strUserIDs As String = String.Empty
        For i As Integer = 0 To grdusers.Rows.Count - 1
            Dim cbox As CheckBox = grdusers.Rows(i).FindControl("CheckBox1")
            If (cbox.Checked) Then
                strUserIDs = strUserIDs + grdusers.DataKeys(i).Value.ToString() + ","
            End If
        Next
        If (strUserIDs.Length > 0) Then
            strUserIDs = strUserIDs + Session("UserID")
            'strUserIDs = strUserIDs.Substring(0, strUserIDs.Length - 1)
            If (objSqlConfig.InsertProjectUsers(strUserIDs, Session("ProjectName").ToString(), Session("BuildingName").ToString()) = 0) Then
                Session("ProjectSaved") = "1"
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
        End If
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Button1_Click(sender, Nothing)
        If (Session("IsTemp").ToString().Equals("1")) Then
            If (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
                Response.Redirect("ClientBOQ.aspx")
            Else
                Response.Redirect("Add_Categories.aspx")
            End If
        Else
            If (Session("ProjectSaved") = "0") Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
            ElseIf (Session("ISBOQ") IsNot Nothing And Session("ISBOQ") = "YES") Then
                Response.Redirect("ClientBOQ.aspx")
            Else
                Response.Redirect("Add_Categories.aspx")
            End If
        End If
        
    End Sub
    Protected Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs)
        Session("ProjectSaved") = "0"
    End Sub
End Class