﻿Public Class BOQ_Mapping
    Inherits FMS.Culture.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then

                Button2.OnClientClick = "window.opener.document.getElementById('ctl00_MainContent_Button4').click(); window.close();"
                ViewState("UnMappedData") = Nothing

                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                End If
                LoadGridView()
                LoadTreeView()

            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
    End Sub
    Private Sub LoadGridView()
        Dim strDiv As String = String.Empty
        Dim strNo As String = String.Empty
        Dim strNameE As String = String.Empty
        Dim strNameA As String = String.Empty
        Dim strUnit As String = String.Empty
        Dim strQty As String = String.Empty

        If (Session("Div") IsNot Nothing) Then
            strDiv = Session("Div")
        End If
        If (Session("No") IsNot Nothing) Then
            strNo = Session("No")
        End If
        If (Session("NameE") IsNot Nothing) Then
            strNameE = Session("NameE")
        End If
        If (Session("NameA") IsNot Nothing) Then
            strNameA = Session("NameA")
        End If
        If (Session("Unit") IsNot Nothing) Then
            strUnit = Session("Unit")
        End If
        If (Session("Qty") IsNot Nothing) Then
            strQty = Session("Qty")
        End If

        Dim dt As New System.Data.DataTable
        dt.Columns.Add("Div")
        dt.Columns.Add("No")
        dt.Columns.Add("NameE")
        dt.Columns.Add("NameA")
        dt.Columns.Add("Unit")
        dt.Columns.Add("Qty")
        Dim dr As DataRow = dt.NewRow()
        dr("Div") = strDiv
        dr("No") = strNo
        dr("NameE") = strNameE
        dr("NameA") = strNameA
        dr("Unit") = strUnit
        dr("Qty") = strQty

        dt.Rows.Add(dr)
        grdBOQ.DataSource = dt
        grdBOQ.DataBind()

        grdBOQ.Height = 300
    End Sub
    Private Sub LoadTreeView()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New System.Data.DataTable
        Dim node As TreeNode = Nothing
        dt = objSqlConfig.Get_BOQ_Mappings(Session("ProjectID").ToString(), Session("BOQLineNo").ToString())
        If (dt.Rows.Count > 0) Then
            For k As Integer = 0 To dt.Rows.Count - 1
                For i As Integer = 0 To 31 Step 4
                    If (dt.Rows(k).Item(i).ToString().Length > 0) Then

                        Dim strValuePath As String = String.Empty

                        For h As Integer = 0 To (i + 3) Step 4
                            If (dt.Rows(k).Item(h + 3).ToString().Length > 0) Then
                                strValuePath = strValuePath + dt.Rows(k).Item(h + 3).ToString() + "/"
                            End If
                        Next

                        If (strValuePath.Length > 0) Then
                            strValuePath = strValuePath.Substring(0, strValuePath.Length - 1)
                        End If

                        node = tvLevels.FindNode(strValuePath)

                        If (node Is Nothing) Then

                            Dim count As Integer = 0
                            For m As Integer = 0 To tvLevels.Nodes.Count - 1
                                If (tvLevels.Nodes(m).Value = dt.Rows(k).Item(i).ToString()) Then
                                    count = 9
                                End If
                            Next

                            If (count = 0) Then
                                Dim newNode As New TreeNode(dt.Rows(k).Item(i + 1).ToString(), dt.Rows(k).Item(i).ToString())
                                tvLevels.Nodes.Add(newNode)
                            End If

                        Else

                            Dim count As Integer = 0
                            For m As Integer = 0 To node.ChildNodes.Count - 1
                                If (node.ChildNodes(m).Value = dt.Rows(k).Item(i).ToString()) Then
                                    count = 9
                                End If
                            Next

                            If (count = 0) Then
                                Dim newNode As New TreeNode(dt.Rows(k).Item(i + 1).ToString(), dt.Rows(k).Item(i).ToString())
                                node.ChildNodes.Add(newNode)
                            End If

                        End If
                    End If
                Next
            Next
        End If
    End Sub
    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If (tvLevels.SelectedNode Is Nothing) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
            Return
        End If
        If (tvLevels.SelectedNode.ChildNodes.Count > 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
            Return
        End If

        Dim dt As System.Data.DataTable = ViewState("UnMappedData")
        If (dt Is Nothing) Then
            dt = New System.Data.DataTable
            dt.Columns.Add("BOQLineNo")
            dt.Columns.Add("ItemCode")
            dt.Columns.Add("NodeValuePath")
        End If

        'Dim strNodeText As String = tvLevels.SelectedNode.Text
        'tvLevels.SelectedNode.Text = "<span style='background-color:red'> '" + strNodeText + "' </span>"
        tvLevels.SelectedNode.SelectAction = TreeNodeSelectAction.None

        Dim dr As DataRow = dt.NewRow()
        dr("BOQLineNo") = Session("BOQLineNo").ToString()
        dr("ItemCode") = tvLevels.SelectedNode.Value
        dr("NodeValuePath") = tvLevels.SelectedNode.ValuePath
        dt.Rows.Add(dr)

        ViewState("UnMappedData") = dt

        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
    End Sub
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dim dt As System.Data.DataTable = ViewState("UnMappedData")
        If (dt IsNot Nothing) Then
            Dim strValuePath As String = ""
            Dim node As TreeNode
            For i As Integer = 0 To dt.Rows.Count - 1
                strValuePath = dt.Rows(i).Item(2).ToString()
                node = tvLevels.FindNode(strValuePath)
                If (node IsNot Nothing) Then
                    node.SelectAction = TreeNodeSelectAction.Select
                End If
            Next
            ViewState("UnMappedData") = Nothing
        End If
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim dt As System.Data.DataTable = ViewState("UnMappedData")
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim arrValues() As String = Session("SuppliersValues").ToString().Split(",")

        If (dt IsNot Nothing) Then

            Dim dtMappedData As System.Data.DataTable = Session("MappedData")
            Dim strLineNo = Session("BOQLineNo")

            For i As Integer = 0 To dt.Rows.Count - 1
                Dim strLineItemCode As String = dt.Rows(i).Item(1).ToString()
                'dr = dtMappedData.Select("LineItemCode = " + strLineItemCode + " , BOQLineNo = " + strLineNo)

                For j As Integer = 0 To dtMappedData.Rows.Count - 1
                    If (j <= dtMappedData.Rows.Count - 1) Then
                        If (dtMappedData.Rows(j).Item("BOQLineNo").ToString() = strLineNo And dtMappedData.Rows(j).Item("LineItemCode").ToString() = strLineItemCode) Then
                            dtMappedData.Rows.RemoveAt(j)
                            j = j - 1
                        End If
                    End If
                Next

                For k As Integer = 0 To arrValues.Length - 1
                    If (arrValues(k) IsNot Nothing) Then
                        If (arrValues(k).ToString().Length > 0 And dt.Rows(i).ItemArray(1).ToString() = arrValues(k).Split(":")(0)) Then
                            System.Array.Clear(arrValues, k, 1)
                        End If
                    End If
                Next

                objSqlConfig.Delete_BOQ_Mappings(Session("ProjectID"), dt.Rows(i).Item(0).ToString(), dt.Rows(i).Item(1).ToString())
                ViewState("UnMappedData") = Nothing
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
            Next

            Session("MappedData") = dtMappedData

            Dim strFinalValue = ""
            For i As Integer = 0 To arrValues.Length - 1
                If (arrValues(i) IsNot Nothing) Then
                    strFinalValue = strFinalValue + arrValues(i) + ","
                End If
            Next

            Session("SuppliersValues") = strFinalValue
        End If
    End Sub
End Class
