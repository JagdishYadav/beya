﻿Public Class ClientType
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            Dim objSqlConfig As New ConfigClassSqlServer()
            ddlClientType.DataSource = objSqlConfig.GetClientTypes()
            If (UICulture = "Arabic") Then
                ddlClientType.DataTextField = "NameA"
            Else
                ddlClientType.DataTextField = "NameE"
            End If
            ddlClientType.DataValueField = "ID"
            ddlClientType.DataBind()
        End If
    End Sub
    Protected Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Session("ClientTypeID") = ddlClientType.SelectedValue
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "window.opener.document.getElementById('ctl00_Button1').click(); window.close();", True)
    End Sub
End Class