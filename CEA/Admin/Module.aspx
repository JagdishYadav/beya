﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Module.aspx.vb" Inherits="CEA._Module" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
Module Selection
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="moduleLanding">
        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="AdminButton">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/admin.png" />
            <asp:Label ID="Label5" runat="server" Text="Administrator" Width="100%"></asp:Label>
        </asp:LinkButton>
        <asp:LinkButton ID="LinkButton2" runat="server" CssClass="AdminButton">
            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/data.png" />
            <asp:Label ID="Label6" runat="server" Text="Cost Estimation System" Width="100%"></asp:Label>
        </asp:LinkButton>
    </div>

    <style>
        #simple-menu {
            display: none !important;
        }

        .moduleLanding {
            display: grid;
            grid-template-columns: 1fr 1fr;
            grid-gap: 20px;
        }

        .moduleLanding .AdminButton {
            margin: 0;
        }

        @media (max-width: 767px) {
            .moduleLanding {
                grid-template-columns: 1fr;
            }
        }
    </style>

    <script>
        $(document).ready(function () {
            $(".bodyWrapper .bodyContainer").addClass('p-0');
            $("#headerWrapper .toggleNavButton").hide();
            $(".mainContentWrapper > .card > .card-header").hide();
        });
    </script>
</asp:Content>
