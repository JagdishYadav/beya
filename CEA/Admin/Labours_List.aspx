﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Labours_List.aspx.vb" Inherits="CEA.Labours_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        function ShowConfirm() {
            var strConfirm = '<%=GetGlobalResourceObject("Resource", "ConfirmDelete") %>';
            return confirm(strConfirm);
        }
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Success") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "UpdateSuccess") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteSuccess") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorAlreadyExist") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorNotExist") %>';
            }

            alert(msgstring);
        }
    </script>
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
User Management
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="">

                <div class="row">
                    <div class="col-12 col-md-7 col-lg-5">
                        <%--<asp:Label ID="Label15" runat="server" Text="Search" Width="100%"></asp:Label>--%>
                        <div class="input-group mb-4 commanSearchGroup">
                            <asp:TextBox ID="txtSearch" runat="server" SkinID="FormControl" CssClass="form-control" placeholder="Search"></asp:TextBox>
                            <div class="input-group-append">
                                <asp:Button ID="Button5" runat="server" Text="Search" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-responsive tableLayoutWrap altboarder">
                    <asp:GridView ID="grdLabours" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="10" DataKeyNames="Code" HeaderStyle-HorizontalAlign="Center">
                        <Columns>
                            <asp:BoundField DataField="ProfE" SortExpression="ProfE" HeaderText='<%$ Resources:Resource, ProfE %>'
                                ItemStyle-Width="45px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="ProfA" SortExpression="ProfA" HeaderText='<%$ Resources:Resource, ProfA %>'
                                ItemStyle-Width="45px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="GenExp" SortExpression="GenExp" HeaderText='<%$ Resources:Resource, GenExp %>'
                                ItemStyle-Width="25px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="AvgSalary" SortExpression="AvgSalary" HeaderText='<%$ Resources:Resource, AvgSal %>'
                                ItemStyle-Width="25px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Housing" SortExpression="Housing" HeaderText='<%$ Resources:Resource, Housing %>'
                                ItemStyle-Width="25px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Food" SortExpression="Food" HeaderText='<%$ Resources:Resource, Food %>'
                                ItemStyle-Width="25px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="MedInsurance" SortExpression="MedInsurance" HeaderText='<%$ Resources:Resource, MedIns %>'
                                ItemStyle-Width="25px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="SecInsurance" SortExpression="SecInsurance" HeaderText='<%$ Resources:Resource, SecIns %>'
                                ItemStyle-Width="25px" ItemStyle-Wrap="true" Visible="false" />
                            <asp:BoundField DataField="Ticket" SortExpression="Ticket" HeaderText='<%$ Resources:Resource, Ticket %>'
                                ItemStyle-Width="25px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="EOContract" SortExpression="EOContract" HeaderText='<%$ Resources:Resource, EOFCont %>'
                                ItemStyle-Width="25px" ItemStyle-Wrap="true" Visible="false" />
                            <asp:BoundField DataField="Iqama" SortExpression="Iqama" HeaderText='<%$ Resources:Resource, Iqama %>'
                                ItemStyle-Width="25px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="NameE" SortExpression="NameE" HeaderText='<%$ Resources:Resource, Nat %>'
                                ItemStyle-Width="25px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="NameA" SortExpression="NameA" HeaderText='<%$ Resources:Resource, Nat %>'
                                ItemStyle-Width="25px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="HourlyRate" SortExpression="HourlyRate" HeaderText='<%$ Resources:Resource, HourlyRate %>'
                                ItemStyle-Width="25px" ItemStyle-Wrap="true" />
                            <asp:CommandField ShowCancelButton="false" ShowDeleteButton="false" ShowEditButton="true"
                                ShowInsertButton="false" ItemStyle-Width="25px" ShowSelectButton="false" EditImageUrl="../Images/tableedit.png" ButtonType="Image" />
                            <asp:CommandField ShowCancelButton="false" ShowDeleteButton="true" ShowEditButton="false"
                                ShowInsertButton="false" ItemStyle-Width="25px" ShowSelectButton="false" DeleteImageUrl="../Images/tabledelete.png" ButtonType="Image" />
                        </Columns>
                    </asp:GridView>
                </div>
                
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>$(document).ready(function () { $("#sidr").show(); });</script>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="PageTitle">

    <asp:Label ID="lblHeader" runat="server" Text='<%$ Resources:Resource, LaboursList %>'></asp:Label>
</asp:Content>

