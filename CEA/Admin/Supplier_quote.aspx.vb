﻿Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging

Public Class Supplier_quote
    Inherits FMS.Culture.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                Menuhide.Value = "close"
                Button3.Attributes.Add("style", "display:none")

                Button2.Enabled = False
                Button4.Enabled = False
                Button5.Enabled = False
                If (Session("User_Role_ID") = "99") Then
                    Button2.Enabled = True
                    Button4.Enabled = True
                    Button5.Enabled = True
                End If
                'If (UICulture = "Arabic") Then
                '    Panel1.Style.Add("direction", "rtl")
                '    Panel1.Style.Add("text-align", "right")
                '    grdProducts.Columns(10).Visible = False
                'Else
                '    grdProducts.Columns(11).Visible = False
                'End If
            Else
                Response.Redirect("../Login.aspx")
            End If

            If (Session("Path") IsNot Nothing) Then
                lblPath.Text = Session("Path")
            End If

            Dim objSqlConfig As New ConfigClassSqlServer()
            ddlCrew.DataSource = objSqlConfig.GetCrewStructureCodesAll(Session("CompanyID"))
            ddlCrew.DataBind()
            If (ddlCrew.Items.Count > 0) Then
                ddlCrew.SelectedIndex = 0
            End If

            Dim listItem As New ListItem("None", "0")
            ddlCrew.Items.Insert(0, listItem)
            ddlCrew.SelectedIndex = 0

            If (Request.QueryString.Get("ID") IsNot Nothing) Then
                Dim dt As New DataTable()
                dt = objSqlConfig.Get_Supplier_Quotation_ByID(Request.QueryString.Get("ID").ToString())
                If (dt.Rows.Count > 0) Then
                    txtUnit.Text = dt.Rows(0).ItemArray(6).ToString()
                    txtCost.Text = dt.Rows(0).ItemArray(4).ToString()
                    txtMatCost.Text = dt.Rows(0).ItemArray(5).ToString()
                    txtDO.Text = dt.Rows(0).ItemArray(7).ToString()
                    ddlCrew.SelectedValue = dt.Rows(0).ItemArray(2).ToString()
                    chkPercent.Checked = dt.Rows(0).ItemArray(3).ToString()
                    imgCrew.ImageUrl = "~/Upload/CrewImages/" + dt.Rows(0).ItemArray(12).ToString()
                End If
            ElseIf (Session("MapID") IsNot Nothing) Then
                Dim dt As New DataTable()
                dt = objSqlConfig.Get_Supplier_Quotation(Session("MapID").ToString(), Session("UserName").ToString())
                If (dt.Rows.Count > 0) Then
                    txtUnit.Text = dt.Rows(0).ItemArray(6).ToString()
                    txtCost.Text = dt.Rows(0).ItemArray(4).ToString()
                    txtMatCost.Text = dt.Rows(0).ItemArray(5).ToString()
                    txtDO.Text = dt.Rows(0).ItemArray(7).ToString()
                    ddlCrew.SelectedValue = dt.Rows(0).ItemArray(2).ToString()
                    chkPercent.Checked = dt.Rows(0).ItemArray(3).ToString()
                    imgCrew.ImageUrl = "~/Upload/CrewImages/" + dt.Rows(0).ItemArray(12).ToString()
                End If
            End If

            If (Request.QueryString.Get("NodeNo") IsNot Nothing) Then
                lblPath.Text = Request.QueryString.Get("NodeNo").ToString() + " --- " + Request.QueryString.Get("NodeName").ToString()
            End If

            txtCost.ReadOnly = False
            If (ddlCrew.SelectedIndex > 0) Then
                txtCost.ReadOnly = True
            End If
        End If
    End Sub
    Protected Sub ddlCrew_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCrew.SelectedIndexChanged
        txtCost.ReadOnly = False
        If (ddlCrew.SelectedIndex > 0) Then
            txtCost.ReadOnly = True
            txtCost.Text = "0"
        End If
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If (ddlCrew.SelectedIndex = 0 And (txtCost.Text.Length = 0 Or txtCost.Text = "0" Or txtCost.Text = "0.00")) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('25');", True)
            txtCost.Focus()
            Return
        End If
        If (chkPercent.Checked) Then
            If (Convert.ToDouble(txtCost.Text) < 0 Or Convert.ToDouble(txtCost.Text) > 100) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('20');", True)
                txtCost.Focus()
                Return
            End If
        End If

        Dim strCost As String = "0"
        Dim strMaterialCost As String = "0"

        If (txtCost.Text.Length > 0) Then
            strCost = txtCost.Text.Trim()
        End If
        If (txtMatCost.Text.Length > 0) Then
            strMaterialCost = txtMatCost.Text.Trim()
        End If

        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim strNodeNumber As String = ""
        If (Session("MapID") IsNot Nothing) Then
            strNodeNumber = Session("MapID").ToString()
        Else
            strNodeNumber = Request.QueryString.Get("ID").ToString()
        End If
        Dim strFileName As String = ""
        If (ViewState("ImageFileName") IsNot Nothing) Then
            strFileName = ViewState("ImageFileName").ToString()
        Else
            strFileName = ""
        End If
        If (objSqlConfig.UpdateCostLineItemByCompany_BySupplier(strNodeNumber, ddlCrew.SelectedValue, (Convert.ToInt32(chkPercent.Checked)).ToString(), strCost.Trim(), strMaterialCost.Trim(), txtUnit.Text.Trim(), txtDO.Text.Trim(), 0, Session("UserName").ToString(), strFileName) = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('23');", True)
        End If
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        txtUnit.Text = ""
        txtMatCost.Text = ""
        ddlCrew.SelectedIndex = 0
        txtCost.Text = ""
        txtDO.Text = ""
        chkPercent.Checked = False
        txtCost.ReadOnly = False
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If (Session("User_Role_ID").ToString() = "99") Then
            Response.Redirect("Supplier_Products_View.aspx")
        Else
            Response.Redirect("Supplier_Quot_View.aspx")
        End If

    End Sub
    Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "Showopoup('Supplier_CrewStructure.aspx');", True)
    End Sub
    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click


        If (Session("Supplier_CS") IsNot Nothing) Then
            Dim objSqlConfig As New ConfigClassSqlServer()
            ddlCrew.DataSource = objSqlConfig.GetCrewStructureCodesAll(Session("CompanyID"))
            ddlCrew.DataBind()

            Dim listItem As New ListItem("None", "0")
            ddlCrew.Items.Insert(0, listItem)

            If (ddlCrew.Items.Count > 0) Then
                ddlCrew.SelectedIndex = 0
            End If

            ddlCrew.SelectedValue = Session("Supplier_CS").ToString()
            'txtDO.Text = Session("Supplier_CS_DO").ToString()
            txtCost.ReadOnly = True
            txtCost.Text = "0"
            'Else
            'ddlCrew.SelectedIndex = 0
            'txtDO.Text = ""
            'txtCost.ReadOnly = False
            'txtCost.Text = ""
        End If
    End Sub
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpload.Click
        If (uplFile.HasFile) Then
            Dim fileExt As String = System.IO.Path.GetExtension(uplFile.FileName).ToLower()
            If (fileExt = ".ico" Or fileExt = ".png" Or fileExt = ".jpeg" Or fileExt = ".img" Or fileExt = ".gif" Or fileExt = ".bmp") Then
                Dim sDateTimeNow As String = DateTime.Now.ToString("yyyyMMddHHmmss")
                Dim strFileName As String = uplFile.FileName.Split(".")(0).ToString().Trim() + "_" + sDateTimeNow + fileExt
                uplFile.SaveAs(Server.MapPath("../Upload/CrewImages/" + strFileName))
                imgCrew.ImageUrl = "~/Upload/CrewImages/" + strFileName
                ViewState("ImageFileName") = strFileName
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
                Return
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('6');", True)
        End If
    End Sub
    Function ResizeBitmap(ByVal bitmapToResize As Bitmap, ByVal width As Integer, ByVal height As Integer)

        Dim NewBitmap As New Bitmap(width, height)
        Dim BitmpGraphics As Graphics = Graphics.FromImage(NewBitmap)
        Dim scaleFactorX As Integer = bitmapToResize.Width / width
        Dim scaleFactorY As Integer = bitmapToResize.Height / width
        BitmpGraphics.ScaleTransform(scaleFactorX, scaleFactorY)
        BitmpGraphics.DrawImage(bitmapToResize, 0, 0)

        Return NewBitmap
    End Function
End Class
