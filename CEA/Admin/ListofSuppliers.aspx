﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master" CodeBehind="ListofSuppliers.aspx.vb" Inherits="CEA.ListofSuppliers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        function ShowMessage(msgno) {
            if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorSelectSupp") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SuppAlreadyAdded") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SuppAdded") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "MinMaxValueMissing") %>';
            }
            alert(msgstring);
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="large-12 columns">
                    <asp:Panel ID="Panel1" runat="server" CssClass="panel">
                      
                        <div class="row">
                         <div class="large-6 medium-6 column">
                   <asp:Label ID="Label15" runat="server" Text="Search" Width="100%"></asp:Label>                            
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="left" Width="70%"></asp:TextBox><asp:Button ID="Button1" runat="server" Text="Search" CssClass="BTNSearch" />
                </div>
                        </div>
                        <div style="width: 100%; height: 300px; overflow: scroll">
            
                            <asp:GridView ID="grdSuppliers" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="5">
                                <Columns>
                                    <asp:BoundField DataField="ID" SortExpression="ID" HeaderText='<%$ Resources:Resource, Code %>'
                                        ItemStyle-Width="20%" />
                                    <asp:BoundField DataField="NameE" SortExpression="NameE" HeaderText='<%$ Resources:Resource, NameE %>'
                                        ItemStyle-Width="35%" />
                                    <asp:BoundField DataField="NameA" SortExpression="NameA" HeaderText='<%$ Resources:Resource, NameA %>'
                                        ItemStyle-Width="35%" />
                                    <asp:CommandField ShowCancelButton="false" ShowDeleteButton="false" ShowEditButton="false"
                                        ShowInsertButton="false" ItemStyle-Width="10%" ShowSelectButton="true" SelectText='<%$ Resources:Resource, Select %>' />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            
            <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
                ShowMessageBox="true" ValidationGroup="Add" DisplayMode="BulletList" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" runat="server" contentplaceholderid="MiddlePart">
<div class="row">
                <div class="large-8 medium-8 columns">
                    <asp:Panel ID="Panel2" runat="server" >
                        <div >
                            <asp:Label ID="lblUser" runat="server" Text='<%$ Resources:Resource, NameE %>' CssClass="label1"></asp:Label>
                            <asp:TextBox ID="txtNameE" runat="server" ReadOnly="true"></asp:TextBox>
                            <asp:Label ID="lblPassword" runat="server" Text='<%$ Resources:Resource, NameA %>'
                                CssClass="label1"></asp:Label>
                            <asp:TextBox ID="txtNameA" runat="server" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div  id = "div1" runat = "server">
                            <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, MinPrice %>'
                                CssClass="label1"></asp:Label>
                            <asp:TextBox ID="txtMinPrice" runat="server" Text="0.00" MaxLength="18"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reMinPrice" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtMinPrice" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Add">
                            </asp:RegularExpressionValidator>
                            <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, SuppID %>' CssClass="label1"></asp:Label>
                            <asp:TextBox ID="txtID" runat="server" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div  id = "div2" runat = "server">
                            <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, MaxPrice %>'
                                CssClass="label1"></asp:Label>
                            <asp:TextBox ID="txtMaxPrice" runat="server" Text="0.00" MaxLength="18"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reMaxPrice" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtMaxPrice" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Add">
                            </asp:RegularExpressionValidator>
                            <asp:Label ID="Label4" runat="server" Text="&nbsp;" CssClass="label1"></asp:Label>
                            <asp:Button ID="Button5" runat="server" Text='<%$ Resources:Resource, AddSupp %>'
                                CssClass="BTNAdd" ValidationGroup="Add" />
                        </div>
                    </asp:Panel>
                </div>
                <div >
                 
                    <asp:Label ID="Label5" runat="server" Text='<%$ Resources:Resource, TotSupp %>' CssClass="label"></asp:Label>
                    <asp:Label ID="lblTotSupp" runat="server" Text="0" CssClass="label"></asp:Label>
                    <br />
                    <br />
                    <asp:Button ID="Button4" runat="server" Text='<%$ Resources:Resource, Close %>' CssClass="BTNclose "
                       />
                </div>
            </div>
</asp:Content>

<asp:Content ID="Content5" runat="server" contentplaceholderid="PageTitle">
 <asp:Label ID="lblHeader" runat="server" Text='<%$ Resources:Resource, SuppliersList %>'></asp:Label>
</asp:Content>


