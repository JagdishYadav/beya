﻿Public Class CrewStructure_List
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (Session("User_Role_ID").ToString().Contains("999")) Then
                    If (menuItems.Count > 15) Then
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                    End If
                ElseIf (Session("User_Role_ID").ToString().Contains("888")) Then
                    If (menuItems.Count > 5) Then
                        menuItems.RemoveAt(0)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                    End If
                End If
                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    'header1.Style.Add("float", "right")
                    'header2.Style.Add("float", "right")
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
        LoadDataGrid()
        Panel1.Width = grdCrew.Width
    End Sub
    Private Sub LoadDataGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        dt = objSqlConfig.GetCrewStrucrureAll(txtSearch.Text)
        If (dt.Rows.Count > 0) Then
            grdCrew.DataSource = dt
            grdCrew.DataBind()
        Else
            grdCrew.DataSource = Nothing
            grdCrew.DataBind()
        End If
    End Sub
    Protected Sub grdCrew_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdCrew.Sorting
        If (ViewState("SortState") IsNot Nothing And ViewState("SortState") = SortDirection.Ascending) Then
            ViewState("SortDirection") = "asc"
            ViewState("SortState") = 1
        Else
            ViewState("SortDirection") = "desc"
            ViewState("SortState") = 0
        End If
        ViewState("SortExp") = e.SortExpression
        FillGrid()
    End Sub
    Protected Sub grdCrew_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdCrew.PageIndexChanging
        grdCrew.PageIndex = e.NewPageIndex
        grdCrew.DataBind()
    End Sub
    Private Sub FillGrid()
        Dim Data As DataTable = grdCrew.DataSource
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp") Is Nothing) Then
                If ViewState("SortDirection").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp") & " " & "desc"
                End If

                grdCrew.DataSource = dv
                grdCrew.DataBind()
            End If
        End If
    End Sub
    Protected Sub grdCrew_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdCrew.RowEditing
        Dim objSqlConfig As New ConfigClassSqlServer()
        Response.Redirect("Add_CrewStructure.aspx?Code=" + grdCrew.Rows(e.NewEditIndex).Cells(0).Text)
    End Sub
    Protected Sub grdCrew_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdCrew.RowDeleting
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim retVal As Integer = objSqlConfig.DeleteCrewStructure(grdCrew.Rows(e.RowIndex).Cells(0).Text)
        If (retVal = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
            LoadDataGrid()
        End If
    End Sub
    Protected Sub grdCrew_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdCrew.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            For Each link As ImageButton In e.Row.Cells(6).Controls.OfType(Of ImageButton)()
                If link.CommandName = "Delete" Then
                    link.Attributes("onclick") = "if (ShowConfirm() == false) { return false; };"
                End If
            Next
        End If
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        LoadDataGrid()
    End Sub
End Class