﻿Imports System.Data
Imports System.Data.SqlClient
Public Class AssociatedSuppliers
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                Menuhide.Value = "close"
                Dim logout As Button = Master.FindControl("Logout")
                logout.Visible = False

                Button2.OnClientClick = "window.opener.document.getElementById('ctl00_MainContent_Button4').click(); window.close();"

                Session("Supplierdt") = Nothing

                If (Session("ProductCode") IsNot Nothing) Then
                    lblProdCode.Text = Session("ProductCode")
                End If
                If (Session("ProductName") IsNot Nothing) Then
                    lblProdName.Text = Session("ProductName").ToString()
                    lblProdName2.Text = Session("ProductNameAra").ToString()
                End If
                If (Session("ProductUnit") IsNot Nothing) Then
                    lblProdUnit.Text = Session("ProductUnit")
                End If

                If (UICulture = "Arabic") Then
                    abc.Style.Add("direction", "rtl")
                    abc.Style.Add("text-align", "right")
                End If
                Dim dt As New DataTable()

                LoadGrids()
                LoadDropDowns()
                SetEnvironmentAccordingtoLang()
            Else
                Response.Redirect("../Login.aspx")
            End If
            
        End If
    End Sub
    Private Sub LoadGrids()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        If (Session("Supplierdt") IsNot Nothing) Then
            grdLabour.DataSource = Session("Supplierdt")
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False
        Else
            dt = objSqlConfig.GetAssociatedSuppliers(Session("ProductCode"))
            Dim dr As DataRow = dt.NewRow()
            dr("SupplierID") = "-1"
            dr("NameE") = ""
            dr("NameA") = ""
            dr("MinPrice") = 0
            dr("MaxPrice") = 0

            dt.Rows.InsertAt(dr, 0)
            grdLabour.DataSource = dt
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False
            Session("Supplierdt") = dt
        End If
    End Sub
    Private Sub LoadDropDowns()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim ddlSupplier As DropDownList = TryCast(grdLabour.FooterRow.FindControl("ddlSuppF"), DropDownList)
        If (ddlSupplier IsNot Nothing) Then
            ddlSupplier.DataSource = objSqlConfig.GetSuppliersList("")
            ddlSupplier.DataValueField = "ID"
            If (UICulture = "Arabic") Then
                ddlSupplier.DataTextField = "NameA"
            Else
                ddlSupplier.DataTextField = "NameE"
            End If
            ddlSupplier.DataBind()
        End If
    End Sub
    Private Sub SetEnvironmentAccordingtoLang()
        If (UICulture = "Arabic") Then
            For i As Integer = 0 To grdLabour.Rows.Count - 1
                If (grdLabour.Rows(i).RowState = DataControlRowState.Normal Or grdLabour.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdLabour.Rows(i).FindControl("l1"), Label).Visible = False
                End If
            Next
        Else
            For i As Integer = 0 To grdLabour.Rows.Count - 1
                If (grdLabour.Rows(i).RowState = DataControlRowState.Normal Or grdLabour.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdLabour.Rows(i).FindControl("l2"), Label).Visible = False
                End If
            Next
        End If
    End Sub
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs)
        Dim dt As DataTable = Session("Supplierdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (dt.Rows(i).Item(0).ToString() = TryCast(grdLabour.FooterRow.FindControl("ddlSuppF"), DropDownList).SelectedValue) Then
                iCount = 1
                Exit For
            End If
        Next

        If (iCount = 0) Then
            Dim dr As DataRow = dt.NewRow()
            dr("SupplierID") = TryCast(grdLabour.FooterRow.FindControl("ddlSuppF"), DropDownList).SelectedValue
            dr("NameE") = TryCast(grdLabour.FooterRow.FindControl("ddlSuppF"), DropDownList).SelectedItem.Text
            dr("NameA") = TryCast(grdLabour.FooterRow.FindControl("ddlSuppF"), DropDownList).SelectedItem.Text
            dr("MinPrice") = TryCast(grdLabour.FooterRow.FindControl("txtMinPrice"), TextBox).Text.Trim()
            dr("MaxPrice") = TryCast(grdLabour.FooterRow.FindControl("txtMaxPrice"), TextBox).Text.Trim()

            dt.Rows.Add(dr)
            grdLabour.DataSource = dt
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False

            Session("Supplierdt") = dt
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(2);", True)
        End If
    End Sub
    Protected Sub grdLabour_RowCancelingEdit(sender As Object, e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdLabour.RowCancelingEdit
        e.Cancel = True
        grdLabour.EditIndex = -1
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdLabour_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdLabour.RowEditing
        grdLabour.EditIndex = e.NewEditIndex
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdLabour_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdLabour.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow And grdLabour.EditIndex = e.Row.RowIndex) Then
            Dim ddlSupplier As DropDownList = DirectCast(e.Row.FindControl("ddlSuppE"), DropDownList)
            Dim objSqlConfig As New ConfigClassSqlServer()
            ddlSupplier.DataSource = objSqlConfig.GetSuppliersList("")
            ddlSupplier.DataValueField = "ID"
            If (UICulture = "Arabic") Then
                ddlSupplier.DataTextField = "NameA"
            Else
                ddlSupplier.DataTextField = "NameE"
            End If
            ddlSupplier.DataBind()
            ddlSupplier.SelectedValue = DirectCast(e.Row.FindControl("lblSuppE"), Label).Text
        End If
    End Sub
    Protected Sub grdLabour_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdLabour.RowUpdating
        Dim dt As DataTable = Session("Supplierdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (i <> e.RowIndex) Then
                If (dt.Rows(i).Item(0).ToString() = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlSuppE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next

        If (iCount = 0) Then
            dt.Rows(e.RowIndex).Item(0) = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlSuppE"), DropDownList).SelectedValue
            dt.Rows(e.RowIndex).Item(1) = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlSuppE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(2) = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlSuppE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(3) = CType(grdLabour.Rows(e.RowIndex).FindControl("txtMinPriceE"), TextBox).Text.Trim()
            dt.Rows(e.RowIndex).Item(4) = CType(grdLabour.Rows(e.RowIndex).FindControl("txtMaxPriceE"), TextBox).Text.Trim()

            grdLabour.EditIndex = -1
            Session("Supplierdt") = dt
            LoadGrids()
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(2);", True)
        End If
    End Sub
    Protected Sub grdLabour_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdLabour.RowDeleting
        Dim dt As DataTable = Session("Supplierdt")
        dt.Rows.RemoveAt(e.RowIndex)
        Session("Supplierdt") = dt
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdLabour_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdLabour.PageIndexChanging
        grdLabour.PageIndex = e.NewPageIndex
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Session("Supplierdt") = Nothing
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim dt As DataTable = Session("Supplierdt")
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "DELETE FROM AssociatedSuppliers WHERE ProductID = '" + lblProdCode.Text + "' "
        sqlCommand.ExecuteNonQuery()

        For i As Integer = 1 To dt.Rows.Count - 1
            If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                sqlCommand.CommandText = "SELECT COUNT(*) FROM AssociatedSuppliers WHERE ProductID = '" + lblProdCode.Text + "' AND SupplierID = '" + dt.Rows(i).Item(0).ToString() + "' "
                If (sqlCommand.ExecuteScalar().ToString() = "0") Then
                    sqlCommand.CommandText = "INSERT INTO AssociatedSuppliers(ProductID,SupplierID,NameE,NameA,MinPrice, MaxPrice) VALUES('" + lblProdCode.Text.Trim() + "', '" + dt.Rows(i).Item(0).ToString() + "', '" + dt.Rows(i).Item(1).ToString() + "', '" + dt.Rows(i).Item(2).ToString() + "', '" + dt.Rows(i).Item(3).ToString() + "', '" + dt.Rows(i).Item(4).ToString() + "')"
                    sqlCommand.ExecuteNonQuery()
                End If
            End If
        Next

        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(1);", True)
    End Sub
End Class