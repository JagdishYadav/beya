﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Supplier_Products_View.aspx.vb" Inherits="CEA.GridView1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 <style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        function ShowConfirm() {
            var strConfirm = '<%=GetGlobalResourceObject("Resource", "ConfirmDelete") %>';
            return confirm(strConfirm);
        }
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorImageFile") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdAlreadyAdded") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdAdded") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorProdNotFound") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdDeleted") %>';
            }
            else if (msgno == 6) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdUpdated") %>';
            }
            else if (msgno == 7) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorOtherMissing") %>';
            }
            else if (msgno == 8) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "QuotDeleted")%>';
            }
            alert(msgstring);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, MyQuots %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="large-12 medium-12 column">
                    <asp:Panel ID="Panel1" runat="server" CssClass="panel">
                     <div class="row">
                        <div class="large-6 medium-6 column">
                            <asp:Label ID="Label15" runat="server" Text='<%$ Resources:Resource, Search %>' Width="100%" CssClass="label1"></asp:Label>
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="left" Width="70%"></asp:TextBox><asp:Button
                                ID="Button5" runat="server" Text='<%$ Resources:Resource, Search %>' CssClass="BTNSearch" />
                        </div>
                       <%-- <div class="large-6 medium-6 column">
                            <asp:Label ID="Label1" runat="server" Text="View" Width="100%" CssClass="label1"></asp:Label>
                            <asp:DropDownList ID="DropDownList1" runat="server">
                            </asp:DropDownList>
                        </div>--%>
                         <div class="large-6 medium-6 column">
                             <asp:Label ID="Label1" runat="server" Width="100%" Text = "." CssClass="label1"></asp:Label>
                             <asp:Button ID="btnAdd" runat="server" Text='<%$ Resources:Resource, AddQuote %>'  CssClass="BTNAdd right" />
                        </div>
                        </div>
                        <div class="row">
                <div class="large-12 medium-12 column">
                        <div style="width: 100%; height: 600px; overflow: scroll">
                            <asp:GridView ID="grdProducts" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="20" DataKeyNames="ID" HeaderStyle-HorizontalAlign ="Center">
                                <Columns>
                                    <asp:BoundField DataField="CostLineItemNo" SortExpression="CostLineItemNo" HeaderText='<%$ Resources:Resource, CostLineItemNo %>'
                                        ItemStyle-Width="50px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="NameE" SortExpression="NameE" HeaderText='<%$ Resources:Resource, Name %>'
                                        ItemStyle-Width="200px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="NameA" SortExpression="NameA" HeaderText='<%$ Resources:Resource, Name %>'
                                        ItemStyle-Width="200px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="Unit" SortExpression="Unit" HeaderText='<%$ Resources:Resource, Unit %>'
                                        ItemStyle-Width="50px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="CrewCode" SortExpression="CrewCode" HeaderText='<%$ Resources:Resource, CrewCode %>'
                                        ItemStyle-Width="50px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="Cost" SortExpression="Cost" HeaderText='<%$ Resources:Resource, CostOfLineItem %>'
                                        ItemStyle-Width="50px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="MaterialCost" SortExpression="MaterialCost" HeaderText='<%$ Resources:Resource, MaterialCost %>'
                                        ItemStyle-Width="50px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="DO" SortExpression="DO" HeaderText='<%$ Resources:Resource, DO %>'
                                        ItemStyle-Width="50px" ItemStyle-Wrap="true" />
                                    <%--<asp:BoundField DataField="Unit" SortExpression="Unit" HeaderText='<%$ Resources:Resource, Unit %>'
                                        ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="ManHour" SortExpression="ManHour" HeaderText='<%$ Resources:Resource, ManHour %>'
                                        ItemStyle-Width="30px" ItemStyle-Wrap="true" Visible = "false"/>
                                    <asp:BoundField DataField="EquipHour" SortExpression="EquipHour" HeaderText='<%$ Resources:Resource, EquipHour %>'
                                        ItemStyle-Width="30px" ItemStyle-Wrap="true" Visible = "false" />
                                    <asp:BoundField DataField="ApprovedByE" SortExpression="ApprovedByE" HeaderText='<%$ Resources:Resource, ApprovedBy %>'
                                        ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="ApprovedByA" SortExpression="ApprovedByA" HeaderText='<%$ Resources:Resource, ApprovedBy %>'
                                        ItemStyle-Width="30px" ItemStyle-Wrap="true" />--%>
                                    <asp:CommandField ShowSelectButton="true" ItemStyle-Width="25px" SelectImageUrl ="../Images/tableedit.png" ButtonType ="Image" />
                                    <asp:CommandField ShowDeleteButton="true" ItemStyle-Width="25px" DeleteImageUrl ="../Images/tabledelete.png" ButtonType ="Image" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        </div></div>
                    </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
