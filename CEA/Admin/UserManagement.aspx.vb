﻿Public Class UserManagment
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items

                If (Session("User_Role_ID").ToString().Contains("999")) Then
                    If (menuItems.Count > 15) Then
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                    End If
                ElseIf (Session("User_Role_ID").ToString().Contains("888")) Then
                    If (Session("IsTemp").ToString().Equals("0")) Then
                        If (menuItems.Count > 5) Then
                            menuItems.RemoveAt(0)
                            menuItems.RemoveAt(1)
                            menuItems.RemoveAt(2)
                            menuItems.RemoveAt(2)
                            menuItems.RemoveAt(5)
                            menuItems.RemoveAt(5)
                            menuItems.RemoveAt(5)
                            menuItems.RemoveAt(5)
                            menuItems.RemoveAt(5)
                            menuItems.RemoveAt(5)
                            menuItems.RemoveAt(5)
                        End If
                    Else
                        menuItems.RemoveAt(0)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(1)
                    End If
                End If

                'If (Session("User_Role_ID").ToString().Contains("888")) Then
                '    Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                '    Menuhide.Value = "close"
                'End If
                Dim objSqlConfig As New ConfigClassSqlServer()
                If (UICulture = "Arabic") Then
                    grdRoles.Columns(0).Visible = False
                    ' header1.Style.Add("float", "right")
                    '  header2.Style.Add("float", "right")
                Else
                    grdRoles.Columns(1).Visible = False
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If

            LoadDataGrid()
        End If
    End Sub
    Private Sub LoadDataGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt, dt1 As New DataTable()
        If (Session("User_Role_ID").ToString().Contains("999")) Then
            dt = objSqlConfig.SearchUsers(txtSearch.Text)
        Else
            dt = objSqlConfig.SearchLimitedUsers(txtSearch.Text, Session("CompanyID").ToString())
        End If
        If (dt.Rows.Count > 0) Then
            grdUsers.DataSource = dt
            grdUsers.DataBind()
        Else
            grdUsers.DataSource = Nothing
            grdUsers.DataBind()
        End If

        If (Session("User_Role_ID").ToString().Contains("888")) Then
            dt1 = objSqlConfig.GetLimitedUserRoles()
        Else
            dt1 = objSqlConfig.GetUserRoles()
        End If

        If (dt.Rows.Count > 0) Then
            grdRoles.DataSource = dt1
            grdRoles.DataBind()
        Else
            grdRoles.DataSource = Nothing
            grdRoles.DataBind()
        End If
    End Sub
    Private Sub LoadUsersGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        If (Session("User_Role_ID").ToString().Contains("999")) Then
            dt = objSqlConfig.SearchUsers(txtSearch.Text)
        Else
            dt = objSqlConfig.SearchLimitedUsers(txtSearch.Text, Session("CompanyID").ToString())
        End If

        If (dt.Rows.Count > 0) Then
            grdUsers.DataSource = dt
            grdUsers.DataBind()
        Else
            grdUsers.DataSource = Nothing
            grdUsers.DataBind()
        End If
    End Sub
    Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        TextBox1.Text = ""
        TextBox3.Text = ""
        txtFName.Text = ""
        txtLName.Text = ""
        txtEmail.Text = ""
        LoadDataGrid()
        TextBox3.ReadOnly = False
        grdUsers.SelectedIndex = -1
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim objSqlConfig As New ConfigClassSqlServer()
        If (grdUsers.SelectedRow.RowIndex >= 0) Then
            If (objSqlConfig.DeleteUser(grdUsers.SelectedDataKey.Value.ToString(), TextBox3.Text.Trim()) = 1) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('5');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
                Button4_Click(sender, Nothing)
            End If
            LoadDataGrid()
        End If
    End Sub
    Protected Sub grdUsers_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdUsers.Sorting
        If (ViewState("SortState") IsNot Nothing And ViewState("SortState") = SortDirection.Ascending) Then
            ViewState("SortDirection") = "asc"
            ViewState("SortState") = 1
        Else
            ViewState("SortDirection") = "desc"
            ViewState("SortState") = 0
        End If
        ViewState("SortExp") = e.SortExpression
        LoadDataGrid()
        FillGrid()
    End Sub
    Protected Sub grdUsers_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdUsers.PageIndexChanging
        LoadDataGrid()
        grdUsers.SelectedIndex = -1
        grdUsers.PageIndex = e.NewPageIndex
        grdUsers.DataBind()
    End Sub
    Private Sub FillGrid()
        Dim Data As DataTable = grdUsers.DataSource
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp") Is Nothing) Then
                If ViewState("SortDirection").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp") & " " & "desc"
                End If

                grdUsers.DataSource = dv
                grdUsers.DataBind()
            End If
        End If
    End Sub
    Protected Sub grdUsers_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdUsers.SelectedIndexChanged
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As DataTable = objSqlConfig.GetUserByID(grdUsers.Rows(grdUsers.SelectedIndex).Cells(2).Text)
        If (dt.Rows.Count > 0) Then
            TextBox3.Text = dt.Rows(0).Item(0).ToString()
            TextBox1.Text = dt.Rows(0).Item(1).ToString()
            txtFName.Text = dt.Rows(0).Item(5).ToString()
            txtLName.Text = dt.Rows(0).Item(6).ToString()
            txtEmail.Text = dt.Rows(0).Item(7).ToString()

            For i As Integer = 0 To grdRoles.Rows.Count - 1
                Dim cbox As CheckBox = grdRoles.Rows(i).FindControl("CheckBox1")
                cbox.Checked = False
            Next

            For i As Integer = 0 To dt.Rows.Count - 1
                For j As Integer = 0 To grdRoles.Rows.Count - 1
                    If (dt.Rows(i).Item(2).ToString() = grdRoles.DataKeys(j).Value.ToString()) Then
                        Dim cbox As CheckBox = grdRoles.Rows(j).FindControl("CheckBox1")
                        cbox.Checked = True
                    End If
                Next
            Next
            TextBox3.ReadOnly = True
        End If
    End Sub
    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim role_selected As Boolean = False
        For i As Integer = 0 To grdRoles.Rows.Count - 1
            Dim cbox As CheckBox = grdRoles.Rows(i).FindControl("CheckBox1")
            If (cbox.Checked) Then
                role_selected = True
                Exit For
            End If
        Next
        If (role_selected = True) Then
            Dim objSqlConfig As New ConfigClassSqlServer()
            Dim retVal As Integer = objSqlConfig.UpdateUser(grdUsers.SelectedDataKey.Value.ToString(), TextBox1.Text.Trim(), txtFName.Text.Trim(), txtLName.Text.Trim(), txtEmail.Text.Trim())
            For i As Integer = 0 To grdRoles.Rows.Count - 1
                Dim cbox As CheckBox = grdRoles.Rows(i).FindControl("CheckBox1")
                If (cbox.Checked) Then
                    objSqlConfig.InsertUserRoles(grdUsers.SelectedDataKey.Value.ToString(), grdRoles.DataKeys(i).Value.ToString())
                End If
            Next
            If (retVal = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
                LoadUsersGrid()
            ElseIf (retVal = 1) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('5');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('-1');", True)
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('6');", True)
        End If
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim role_selected As Boolean = False
        For i As Integer = 0 To grdRoles.Rows.Count - 1
            Dim cbox As CheckBox = grdRoles.Rows(i).FindControl("CheckBox1")
            If (cbox.Checked) Then
                role_selected = True
                Exit For
            End If
        Next
        If (role_selected = True) Then
            Dim objSqlConfig As New ConfigClassSqlServer()
            Dim retVal As Integer = objSqlConfig.CreateUser(TextBox3.Text.Trim(), TextBox1.Text.Trim(), txtFName.Text.Trim(), txtLName.Text.Trim(), txtEmail.Text.Trim(), Session("CompanyID").ToString())
            If (retVal > 1) Then
                objSqlConfig.DeleteUserRoles(retVal.ToString())
                For i As Integer = 0 To grdRoles.Rows.Count - 1
                    Dim cbox As CheckBox = grdRoles.Rows(i).FindControl("CheckBox1")
                    If (cbox.Checked) Then
                        objSqlConfig.InsertUserRoles(retVal.ToString(), grdRoles.DataKeys(i).Value.ToString())
                    End If
                Next
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
                LoadDataGrid()
                Button4_Click(sender, Nothing)
            ElseIf (retVal = -1) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('-1');", True)
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('6');", True)
        End If
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        LoadDataGrid()
    End Sub
    Protected Sub grdRoles_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdRoles.PageIndexChanging
        If (grdUsers.SelectedIndex >= 0) Then
            LoadDataGrid()
            grdRoles.PageIndex = e.NewPageIndex
            grdRoles.DataBind()

            Dim objSqlConfig As New ConfigClassSqlServer()
            Dim dt As DataTable = objSqlConfig.GetUserByID(grdUsers.Rows(grdUsers.SelectedIndex).Cells(2).Text)

            For i As Integer = 0 To grdRoles.Rows.Count - 1
                Dim cbox As CheckBox = grdRoles.Rows(i).FindControl("CheckBox1")
                cbox.Checked = False
            Next
            For i As Integer = 0 To dt.Rows.Count - 1
                For j As Integer = 0 To grdRoles.Rows.Count - 1
                    If (dt.Rows(i).Item(2).ToString() = grdRoles.DataKeys(j).Value.ToString()) Then
                        Dim cbox As CheckBox = grdRoles.Rows(j).FindControl("CheckBox1")
                        cbox.Checked = True
                    End If
                Next
            Next
        Else
            LoadDataGrid()
            grdRoles.PageIndex = e.NewPageIndex
            grdRoles.DataBind()
        End If
    End Sub
    Protected Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        TextBox1.Text = ""
        TextBox3.Text = ""
        txtFName.Text = ""
        txtLName.Text = ""
        txtEmail.Text = ""

        Dim cbox As CheckBox
        For i As Integer = 0 To grdRoles.Rows.Count - 1
            cbox = grdRoles.Rows(i).FindControl("CheckBox1")
            cbox.Checked = False
        Next

        TextBox3.ReadOnly = False
        grdUsers.SelectedIndex = -1
        txtFName.Focus()
    End Sub
End Class