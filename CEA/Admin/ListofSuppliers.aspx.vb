﻿Public Class ListofSuppliers
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
            Menuhide.Value = "close"
            Dim logout As Button = Master.FindControl("Logout")
            logout.Visible = False
           
            Button4.OnClientClick = "window.opener.document.getElementById('ctl00_MainContent_Button4').click(); window.close();"

            If (UICulture = "Arabic") Then
                Panel1.Style.Add("direction", "rtl")
                Panel1.Style.Add("text-align", "right")
                Panel2.Style.Add("direction", "rtl")
                Panel2.Style.Add("text-align", "right")
                'header1.Style.Add("float", "right")
                'header2.Style.Add("float", "right")
                div1.Style.Add("float", "right")
                div2.Style.Add("float", "right")
            End If

            Dim dt As New DataTable()
            If (Session("SupplierList") IsNot Nothing) Then

                dt = Session("SupplierList")
                Dim arrID As String() = Request.QueryString.Get("ID").Split(",")
                For k As Integer = 0 To arrID.Length - 2
                    Dim index As Integer = Integer.Parse(arrID(k))
                    If (dt.Rows.Count > index) Then
                        dt.Rows(index).Delete()
                    End If
                Next
                dt.AcceptChanges()
                Session("SupplierList") = dt
            End If
            
            LoadDataGrid()
            Panel1.Width = grdSuppliers.Width
        End If
    End Sub
    Private Sub LoadDataGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        dt = objSqlConfig.GetSuppliersList(txtSearch.Text)
        If (dt.Rows.Count > 0) Then
            grdSuppliers.DataSource = dt
            grdSuppliers.DataBind()
            lblTotSupp.Text = dt.Rows.Count
        Else
            grdSuppliers.DataSource = Nothing
            grdSuppliers.DataBind()
            lblTotSupp.Text = "0"
        End If
    End Sub
    Protected Sub grdSuppliers_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdSuppliers.Sorting
        If (ViewState("SortState") IsNot Nothing And ViewState("SortState") = SortDirection.Ascending) Then
            ViewState("SortDirection") = "asc"
            ViewState("SortState") = 1
        Else
            ViewState("SortDirection") = "desc"
            ViewState("SortState") = 0
        End If
        ViewState("SortExp") = e.SortExpression
        FillGrid()
    End Sub
    Private Sub FillGrid()
        Dim Data As DataTable = grdSuppliers.DataSource
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp") Is Nothing) Then
                If ViewState("SortDirection").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp") & " " & "desc"
                End If

                grdSuppliers.DataSource = dv
                grdSuppliers.DataBind()
            End If
        End If
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim d As Double
        If (txtID.Text.Length = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
            Return
        End If
        If (txtMinPrice.Text.Length = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
            txtMinPrice.Focus()
            Return
        ElseIf (Double.TryParse(txtMinPrice.Text, d) = False Or Double.Parse(txtMinPrice.Text) = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
            txtMinPrice.Focus()
            Return
        End If
        If (txtMaxPrice.Text.Length = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
            txtMaxPrice.Focus()
            Return
        ElseIf (Double.TryParse(txtMaxPrice.Text, d) = False Or Double.Parse(txtMaxPrice.Text) = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
            txtMaxPrice.Focus()
            Return
        End If

        'check if selected supplier is already added
        Dim dt As New DataTable()
        If (Session("SupplierList") IsNot Nothing) Then
            dt = Session("SupplierList")
            For i As Integer = 0 To dt.Rows.Count - 1
                If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                    If (txtID.Text.Equals(dt.Rows(i).Item(0).ToString())) Then
                        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
                        Return
                    End If
                End If
            Next
        End If

        If (txtMinPrice.Text.Length = 0) Then
            txtMinPrice.Text = "0.00"
        End If
        If (txtMaxPrice.Text.Length = 0) Then
            txtMaxPrice.Text = "0.00"
        End If

        Dim dr As DataRow = dt.NewRow()
        If (dt.Columns.Count = 0) Then
            dt.Columns.Add("SupplierID")
            dt.Columns.Add("NameE")
            dt.Columns.Add("NameA")
            dt.Columns.Add("MinPrice")
            dt.Columns.Add("MaxPrice")
        End If
        dr(0) = txtID.Text
        dr(1) = txtNameE.Text
        dr(2) = txtNameA.Text
        dr(3) = txtMinPrice.Text
        dr(4) = txtMaxPrice.Text

        dt.Rows.Add(dr)

        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
        txtID.Text = ""
        txtNameE.Text = ""
        txtNameA.Text = ""
        txtMinPrice.Text = "0.00"
        txtMaxPrice.Text = "0.00"

        Session("SupplierList") = dt
    End Sub
    Protected Sub grdSuppliers_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdSuppliers.SelectedIndexChanged
        txtID.Text = grdSuppliers.Rows(grdSuppliers.SelectedIndex).Cells(0).Text
        txtNameE.Text = grdSuppliers.Rows(grdSuppliers.SelectedIndex).Cells(1).Text
        txtNameA.Text = grdSuppliers.Rows(grdSuppliers.SelectedIndex).Cells(2).Text
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        LoadDataGrid()
    End Sub
    Protected Sub grdSuppliers_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdSuppliers.PageIndexChanging
        grdSuppliers.PageIndex = e.NewPageIndex
        LoadDataGrid()
    End Sub
End Class