﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="CrewStructure_New.aspx.vb" Inherits="CEA.CrewStructure_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        function DoPostBack() {
            document.getElementById('<%= ddlCountry.ClientID %>').focus();
            document.getElementById('<%= txtDO.ClientID %>').focus();
            //alert(txtDO.value);
            //document.getElementById('<%= hdnDoValue.ClientID %>').value = txtDO.value;
            //document.getElementById('<%= Button9999.ClientID %>').click();
            return false;
        }
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DataSaved") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "LabourAlreadyExist") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "EquipAlreadyExist") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorFileFormat") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DocAdded") %>';
            }
            else if (msgno == 6) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NoFile") %>';
            }
            alert(msgstring);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label14" runat="server" Text='<%$ Resources:Resource, CrewStructure %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <Triggers>
            <asp:PostBackTrigger ControlID="Button3" />
        </Triggers>
        <ContentTemplate>
        <table width = "100%" border = "0" id = "options" runat = "server">
            <tr>
                <td>
                    <asp:RadioButton ID="rbtn4" runat="server" Text='<%$ Resources:Resource, ShowProd %>' GroupName = "option" AutoPostBack = "true" Checked = "true"  />
                </td>
                <td>
                    <asp:RadioButton ID="rbtn1" runat="server" Text='<%$ Resources:Resource, ShowDoc %>' GroupName = "option" AutoPostBack = "true"  />
                </td>
                <td>
                    <asp:RadioButton ID="rbtn2" runat="server" Text='<%$ Resources:Resource, ShowHist %>' GroupName = "option" AutoPostBack = "true"/>
                </td>
                <td>
                    <asp:RadioButton ID="rbtn3" runat="server" Text='<%$ Resources:Resource, ShowRefData %>' GroupName = "option" AutoPostBack = "true"/>
                </td>
            </tr>
        </table>
        <asp:Panel ID="Panel4" runat="server" CssClass="panelDiv" Visible = "true">
            <div class="row">
            <div class="large-6 medium-6 columns">
                    <div class="large-2 medium-2 columns">
                        &nbsp;
                    </div>
                    <div class="large-3 medium-3 columns bold">
                        <asp:Label ID="Label11" runat="server" CssClass="left" Text='<%$ Resources:Resource, SelectProductivity %>'></asp:Label>
                    </div>
                    <div class="large-6 medium-6 columns">
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="large-6 medium-6 columns">
                    <div class="large-2 medium-2 columns">
                        &nbsp;
                    </div>
                    <div class="large-3 medium-3 columns bold">
                        <asp:Label ID="Label2" runat="server" CssClass="left" Text='<%$ Resources:Resource, SelectProductivity %>'></asp:Label>
                    </div>
                    <div class="large-6 medium-6 columns">
                        <asp:DropDownList ID="ddlProd" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="large-6 medium-6 columns">
                    <div class="panelDiv">
                        <div class="row">
                            <div class="large-3 medium-3 columns bold">
                                <asp:Label ID="Label15" runat="server" Text='<%$ Resources:Resource, SelectNat %>'
                                    Font-Bold="True" ForeColor="Red"></asp:Label>
                            </div>
                            <div class="large-9 medium-9 column">
                                <asp:DropDownList ID="ddlCountry" runat="server" Style="max-width: 180px" AutoPostBack="True">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-3 medium-3 columns bold">
                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, ItemCode %>'
                                    Font-Bold="True" ForeColor="Red"></asp:Label>
                            </div>
                            <div class="large-9 medium-9 column">
                                <asp:Label ID="lblProdCode" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-3 medium-3 columns bold">
                                <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, ItemName %>'
                                    Font-Bold="True" ForeColor="Red"></asp:Label>
                            </div>
                            <div class="large-9 medium-9 column">
                                <asp:Label ID="lblProdName" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                <asp:Label ID="lblProdName2" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-3 medium-3 columns bold">
                                <asp:Label ID="Label5" runat="server" Text='<%$ Resources:Resource, ItemUnit %>'
                                    Font-Bold="True" ForeColor="Red"></asp:Label>
                            </div>
                            <div class="large-9 medium-9 column">
                                <asp:Label ID="lblProdUnit" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="large-6 medium-6 columns">
                    <div class="panelDiv">
                        <div class="row">
                            <div class="large-3 medium-3 columns bold">
                                <asp:Label ID="Label8" runat="server" Text='<%$ Resources:Resource, DailyOutput %>'
                                    Font-Bold="True"></asp:Label>
                            </div>
                            <div class="large-9 medium-9 columns">
                                <asp:TextBox ID="txtDO" runat="server" MaxLength="10" Width="80px" AutoPostBack="True"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="frvDO" runat="server" ErrorMessage='<%$ Resources:Resource, MissingDailyOutput %>'
                                    ControlToValidate="txtDO" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="reAvgSal" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                    SetFocusOnError="true" ControlToValidate="txtDO" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                    Display="None" ValidationGroup="Save">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-3 medium-3 columns bold">
                                <asp:Label ID="Label7" runat="server" Text='<%$ Resources:Resource, HourlyOutput %>'></asp:Label>
                            </div>
                            <div class="large-9 medium-9 columns">
                                <asp:Label ID="lblHO" runat="server" Text="Value" Font-Bold="True"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-3 medium-3 columns bold">
                                <asp:Label ID="Label10" runat="server" Text='<%$ Resources:Resource, ManHour %>'></asp:Label>
                            </div>
                            <div class="large-9 medium-9 columns">
                                <asp:Label ID="lblMO" runat="server" Text="Value" Font-Bold="True"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-3 medium-3 columns bold">
                                <asp:Label ID="Label12" runat="server" Text='<%$ Resources:Resource, EquipHour %>'></asp:Label>
                            </div>
                            <div class="large-9 medium-9 columns">
                                <asp:Label ID="lblEH" runat="server" Text="Value" Font-Bold="True"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--<div class="row">
        <div class="large-6 medium-6 column">
                        <asp:Label ID="Label18" runat="server" Text="Search" Width="100%"></asp:Label>
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="left" Width="60%"></asp:TextBox><asp:Button
                            ID="Button5" runat="server" Text="Search" CssClass="BTNSearch" />
                    </div>
                     <div class="large-6 medium-6 column">
                        <asp:Label ID="Label19" runat="server" Text="Search" Width="100%"></asp:Label>
                        <asp:TextBox ID="TextBox1" runat="server" CssClass="left" Width="60%"></asp:TextBox><asp:Button
                            ID="Button1" runat="server" Text="Search" CssClass="BTNSearch" />
                    </div>
    </div>--%>
            <div class="row" id="abc" runat="server">
                <div class="large-6 medium-6 columns" id="grd1" runat="server">
                    <asp:Label ID="Label16" runat="server" Text='<%$ Resources:Resource, LaboursList %>'
                        CssClass="bold"></asp:Label>
                    <asp:GridView ID="grdLabour" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        EmptyDataText="No Data" Width="100%" PageSize="10" DataKeyNames="Code" ShowFooter="True"
                        HeaderStyle-Height="30px" RowStyle-Height="25px" FooterStyle-BackColor="ActiveBorder"
                        FooterStyle-Height="25px" AllowSorting="false" HeaderStyle-HorizontalAlign ="Center">
                        <Columns>
                            <asp:TemplateField HeaderText='<%$ Resources:Resource, Prof %>'>
                                <ItemTemplate>
                                    <asp:Label ID="l1" runat="server" Text='<% # Eval("ProfE") %>' />
                                    <asp:Label ID="l2" runat="server" Text='<% # Eval("ProfA") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlLabE" runat="server" Style="max-width: 300px">
                                    </asp:DropDownList>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblLabEdit" runat="server" Text='<% # Eval("Code") %>' Visible="false" />
                                    <asp:DropDownList ID="ddlLabFE" runat="server" Style="max-width: 300px">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText='<%$ Resources:Resource, Man %>'>
                                <ItemTemplate>
                                    <asp:Label ID="l3" runat="server" Text='<% # Eval("Man_Day") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtMDF" runat="server" Style="max-width: 150px" MaxLength="10"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv114" runat="server" ErrorMessage='<%$ Resources:Resource, MissingMan %>'
                                        ControlToValidate="txtMDF" SetFocusOnError="true" ValidationGroup="SaveLab" Display="None"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="reAvgSal1" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                        SetFocusOnError="true" ControlToValidate="txtMDF" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                        Display="None" ValidationGroup="SaveLab">
                                    </asp:RegularExpressionValidator>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtMD" runat="server" Style="max-width: 150px" MaxLength="10" Text='<% # Eval("Man_Day") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv119" runat="server" ErrorMessage='<%$ Resources:Resource, MissingMan %>'
                                        ControlToValidate="txtMD" SetFocusOnError="true" ValidationGroup="SaveLabE" Display="None"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="re2345" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                        SetFocusOnError="true" ControlToValidate="txtMD" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                        Display="None" ValidationGroup="SaveLabE"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField >
                                <FooterTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" Text='<%$ Resources:Resource, Save %>'
                                       OnClick="LinkButton1_Click"  ValidationGroup="SaveLab"></asp:LinkButton>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="LinkButton8" runat="server" CommandName="Update" Text='<%$ Resources:Resource, Update %>'
                                        ItemStyle-Width="25px" ValidationGroup="SaveLabE"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton8" runat="server" CommandName="Edit" Text='<%$ Resources:Resource, Edit %>'
                                        ItemStyle-Width="25px"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="LinkButton9" runat="server" CommandName="Cancel" Text='<%$ Resources:Resource, Cancel %>'
                                        ItemStyle-Width="25px"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton9" runat="server" CommandName="Delete" Text='<%$ Resources:Resource, Delete %>'
                                        ItemStyle-Width="25px"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="ActiveBorder" Height="30px"></FooterStyle>
                        <HeaderStyle Height="30px"></HeaderStyle>
                        <RowStyle Height="25px"></RowStyle>
                    </asp:GridView>
                </div>
                <div class="large-6 medium-6 columns">
                    <asp:Label ID="Label17" runat="server" Text='<%$ Resources:Resource, EquipmentList %>'
                        CssClass="bold"></asp:Label>
                    <asp:GridView ID="grdEquip" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        EmptyDataText="No Data" Width="100%" PageSize="10" DataKeyNames="Code" ShowFooter="True"
                        HeaderStyle-Height="30px" RowStyle-Height="25px" FooterStyle-BackColor="ActiveBorder"
                        FooterStyle-Height="25px" AllowSorting="false" HeaderStyle-HorizontalAlign ="Center">
                        <Columns>
                            <asp:TemplateField HeaderText='<%$ Resources:Resource, Desc %>'>
                                <ItemTemplate>
                                    <asp:Label ID="l11" runat="server" Text='<% # Eval("DescE") %>' />
                                    <asp:Label ID="l12" runat="server" Text='<% # Eval("DescA") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlEquipE" runat="server" Style="max-width: 300px">
                                    </asp:DropDownList>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblEqEdit" runat="server" Text='<% # Eval("Code") %>' Visible="false" />
                                    <asp:DropDownList ID="ddlEquipFE" runat="server" Style="max-width: 300px">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText='<%$ Resources:Resource, Day %>'>
                                <ItemTemplate>
                                    <asp:Label ID="l13" runat="server" Text='<% # Eval("Man_Day") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtMDF1" runat="server" Style="max-width: 150px" MaxLength="10"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv1134" runat="server" ErrorMessage='<%$ Resources:Resource, MissingDay %>'
                                        ControlToValidate="txtMDF1" SetFocusOnError="true" ValidationGroup="SaveEq" Display="None"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="reAvgSal2" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                        SetFocusOnError="true" ControlToValidate="txtMDF1" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                        Display="None" ValidationGroup="SaveEq">
                                    </asp:RegularExpressionValidator>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtMD1" runat="server" Style="max-width: 150px" MaxLength="50" Text='<% # Eval("Man_Day") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv1319" runat="server" ErrorMessage='<%$ Resources:Resource, MissingDay %>'
                                        ControlToValidate="txtMD1" SetFocusOnError="true" ValidationGroup="SaveEqE" Display="None"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rgexp897" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                        SetFocusOnError="true" ControlToValidate="txtMD1" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                        Display="None" ValidationGroup="SaveEqE">
                                    </asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign ="Center">
                                <FooterTemplate>
                                    <asp:LinkButton ID="LinkButton11" runat="server" Text='<%$ Resources:Resource, Save %>'
                                    OnClick="LinkButton11_Click"    ValidationGroup="SaveEq" ></asp:LinkButton>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="LinkButton81" runat="server" CommandName="Update" Text='<%$ Resources:Resource, Update %>'
                                        ItemStyle-Width="25px" ValidationGroup="SaveEqE"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton82" runat="server" CommandName="Edit" Text='<%$ Resources:Resource, Edit %>'
                                        ItemStyle-Width="25px"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="LinkButton91" runat="server" CommandName="Cancel" Text='<%$ Resources:Resource, Cancel %>'
                                        ItemStyle-Width="25px"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton92" runat="server" CommandName="Delete" Text='<%$ Resources:Resource, Delete %>'
                                        ItemStyle-Width="25px"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" CssClass="panelDiv" Visible = "false">
                <div class="row">
                    <div class="large-12 medium-12 columns">
                     <asp:Label ID="Label4" runat="server" Text='<%$ Resources:Resource, ProdHistory %>'
                        CssClass="bold"></asp:Label>
                       
                       <div class="large-12 medium-12 columns">
                            <asp:GridView ID = "grdHistory" runat="server" AutoGenerateColumns="False"
                        EmptyDataText="No Data" Width="100%" PageSize="5" DataKeyNames="ID" ShowFooter="True"
                        HeaderStyle-Height="30px" RowStyle-Height="25px" FooterStyle-BackColor="ActiveBorder"
                        FooterStyle-Height="25px" AllowSorting="false" EditRowStyle-HorizontalAlign = "Center" HeaderStyle-HorizontalAlign = "Center" RowStyle-HorizontalAlign = "Center">
                        <Columns>
                            
                            <asp:TemplateField HeaderText='<%$ Resources:Resource, DateTime %>' FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblDateTime" runat="server" Text='<% # DateTime.ParseExact(Eval("DateTime"), "yyyyMMddHHmmss", Nothing).ToString() %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblDateTimeF" runat="server" Text='<% # DateTime.ParseExact(Now.ToString("yyyyMMddHHmmss"), "yyyyMMddHHmmss", Nothing).ToString() %>' />
                                </FooterTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblDateTimeE" runat="server" Text='<% # DateTime.ParseExact(Eval("DateTime"), "yyyyMMddHHmmss", Nothing).ToString() %>' />
                                </EditItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText='<%$ Resources:Resource, DailyOutput %>' FooterStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblCode" runat="server" Text='<% # Eval("DO") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtDOF" runat="server" Style="max-width: 150px" MaxLength="20" Text = ""></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvDOF" runat="server" ErrorMessage='<%$ Resources:Resource, MissingDailyOutput %>'
                                        ControlToValidate="txtDOF" SetFocusOnError="true" ValidationGroup="SaveHistoryF" Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="reDOF" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                        SetFocusOnError="true" ControlToValidate="txtDOF" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                        Display="None" ValidationGroup="SaveHistoryF">
                                    </asp:RegularExpressionValidator>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDOE" runat="server" Style="max-width: 150px" MaxLength="20" Text='<% # Eval("DO") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvDOE" runat="server" ErrorMessage='<%$ Resources:Resource, MissingDailyOutput %>'
                                        ControlToValidate="txtDOE" SetFocusOnError="true" ValidationGroup="SaveHistoryE" Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="reDOE" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                        SetFocusOnError="true" ControlToValidate="txtDOE" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                        Display="None" ValidationGroup="SaveHistoryE">
                                    </asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField FooterStyle-HorizontalAlign="Center">
                                <FooterTemplate>
                                    <asp:LinkButton ID="btnSaveHistory" runat="server" Text='<%$ Resources:Resource, Save %>'
                                      OnClick = "btnSaveHistory_Click"   ValidationGroup="SaveHistoryF" ></asp:LinkButton> 
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="btnUpdateHistory" runat="server" CommandName="Update" Text='<%$ Resources:Resource, Update %>'
                                        ItemStyle-Width="25px" ValidationGroup="SaveHistoryE"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnEditHistory" runat="server" CommandName="Edit" Text='<%$ Resources:Resource, Edit %>'
                                        ItemStyle-Width="25px"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="btnCancelHistory" runat="server" CommandName="Cancel" Text='<%$ Resources:Resource, Cancel %>'
                                        ItemStyle-Width="25px"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDeleteHistory" runat="server" CommandName="Delete" Text='<%$ Resources:Resource, Delete %>'
                                        ItemStyle-Width="25px"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="ActiveBorder" Height="30px"></FooterStyle>
                        <HeaderStyle Height="30px"></HeaderStyle>
                        <RowStyle Height="25px"></RowStyle>
                    </asp:GridView>
                       </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="Panel2" runat="server" CssClass="panelDiv" Visible = "false">
                <div class="row">
                    <div class="large-12 medium-12 columns" style=overflow:auto>
                     <asp:Label ID="Label6" runat="server" Text='<%$ Resources:Resource, ShowRefData %>'
                        CssClass="bold"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="left" Width="30%"></asp:TextBox>
                        <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Search %>' CssClass="BTNSearch"/>
                       <div class="large-12 medium-12 columns">
                       <asp:GridView ID="grdRef" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        AllowSorting="false" EmptyDataText="No Data" Width="100%" PageSize="15" 
                               DataKeyNames="ID" HeaderStyle-HorizontalAlign ="Center">
                        <Columns>
                            <asp:BoundField DataField="LineNum" SortExpression="LineNum" HeaderText='<%$ Resources:Resource, LineNum %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Source" SortExpression="Source" HeaderText='<%$ Resources:Resource, Source %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Code" SortExpression="Code" HeaderText='<%$ Resources:Resource, Code %>'
                                ItemStyle-Width="35px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="DescE" SortExpression="DescE" HeaderText='<%$ Resources:Resource, DescriptionE %>'
                                ItemStyle-Width="40px" ItemStyle-Wrap="true" Visible="false" />
                            <asp:BoundField DataField="DescA" SortExpression="DescA" HeaderText='<%$ Resources:Resource, DescriptionA %>'
                                ItemStyle-Width="40px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Crew" SortExpression="Crew" HeaderText='<%$ Resources:Resource, Crew %>'
                                ItemStyle-Width="50px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="DO" SortExpression="DO" HeaderText='<%$ Resources:Resource, DO %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="LH" SortExpression="LH" HeaderText='<%$ Resources:Resource, LH %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Unit" SortExpression="Unit" HeaderText='<%$ Resources:Resource, Unit %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Material" SortExpression="Material" HeaderText='<%$ Resources:Resource, Material %>'
                                ItemStyle-Width="40px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Labour" SortExpression="Labour" HeaderText='<%$ Resources:Resource, Labour %>'
                                ItemStyle-Width="40px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Equipment" SortExpression="Equipment" HeaderText='<%$ Resources:Resource, Equipment %>'
                                ItemStyle-Width="40px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Total" SortExpression="Total" HeaderText='<%$ Resources:Resource, Total %>'
                                ItemStyle-Width="35px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="MatOP" SortExpression="MatOP" HeaderText='<%$ Resources:Resource, MatOP %>'
                                ItemStyle-Width="40px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="LabourOP" SortExpression="LabourOP" HeaderText='<%$ Resources:Resource, LabourOP %>'
                                ItemStyle-Width="40px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="EquipOP" SortExpression="EquipOP" HeaderText='<%$ Resources:Resource, EquipOP %>'
                                ItemStyle-Width="40px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="TotalOP" SortExpression="TotalOP" HeaderText='<%$ Resources:Resource, TotalOP %>'
                                ItemStyle-Width="40px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="LabourType" SortExpression="LabourType" HeaderText='<%$ Resources:Resource, LabourType %>'
                                ItemStyle-Width="40px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="DataRelease" SortExpression="DataRelease" HeaderText='<%$ Resources:Resource, DataRelease %>'
                                ItemStyle-Width="40px" ItemStyle-Wrap="true" />
                             <asp:BoundField DataField="Location" SortExpression="Location" HeaderText='<%$ Resources:Resource, Location %>'
                                ItemStyle-Width="40px" ItemStyle-Wrap="true" />
                              <asp:BoundField DataField="Notes" SortExpression="Notes" HeaderText='<%$ Resources:Resource, Notes %>'
                                ItemStyle-Width="40px" ItemStyle-Wrap="true" />
                        </Columns>
                    </asp:GridView>
                       </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="Panel3" runat="server" CssClass="panelDiv" Visible = "false">
                <div class="row">
                    <div class="large-12 medium-12 columns">
                     <asp:Label ID="Label9" runat="server" Text='<%$ Resources:Resource, AttachedDoc %>'   
                        CssClass="bold"></asp:Label>
                        <br />
                        <div class="large-12 medium-12 columns">
                       <asp:GridView ID="grdDoc" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                             EmptyDataText="No Data" Width="100%" PageSize="8" DataKeyNames="ID,Code,Ext" SelectedRowStyle-BackColor = "Yellow" HeaderStyle-HorizontalAlign = "Center" RowStyle-HorizontalAlign = "Center">
                            <Columns>
                                <asp:BoundField DataField="Name" HeaderText='<%$ Resources:Resource, DocName %>'
                                    ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                                <asp:BoundField DataField="DateTime" HeaderText='<%$ Resources:Resource, DateAdded %>'
                                    ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                                <asp:CommandField ShowSelectButton="true" ItemStyle-Width="25px" SelectText='<%$ Resources:Resource, View %>' />
                            </Columns>
                        </asp:GridView>
                       </div>
                        <asp:FileUpload ID="FileUpload1" runat="server" Width="30%" CssClass="small button " />
                        <div class="large-4 medium-4 column middle" style="width: 250px;">
                    <asp:Button ID="Button3" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave"
                        ValidationGroup="Save" />
                    <asp:Button ID="Button4" runat="server" Text='<%$ Resources:Resource, Cancel %>'
                        CssClass="BTNCancel" />
                   
                </div>
                    </div>
                </div>
            </asp:Panel>
            
            <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
                ShowMessageBox="true" ValidationGroup="Save" DisplayMode="BulletList" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveLab" DisplayMode="BulletList" />
            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveLabE" DisplayMode="BulletList" />
            <asp:ValidationSummary ID="ValidationSummary3" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveEq" DisplayMode="BulletList" />
            <asp:ValidationSummary ID="ValidationSummary4" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveEqE" DisplayMode="BulletList" />

                <asp:ValidationSummary ID="ValidationSummary5" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveHistoryE" DisplayMode="BulletList" />
                <asp:ValidationSummary ID="ValidationSummary6" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveHistoryF" DisplayMode="BulletList" />




            <asp:Button ID="Button9999" runat="server" Text="" />
            <asp:HiddenField ID="hdnDoValue" runat="server" />
        </ContentTemplate>
        
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" runat="server" contentplaceholderid="MiddlePart">
    <div class="row"><div class="large-12 medium-12 column">
<div class="outerDivBtn">
                <div class="BTNdiv" >
                    <asp:Button ID="btnSave" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave"
                        ValidationGroup="Save" />
                    <asp:Button ID="Button8" runat="server" Text='<%$ Resources:Resource, Cancel %>'
                        CssClass="BTNCancel" />
                    <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Close %>' CssClass="BTNclose" />
                </div>
            </div></div></div>
</asp:Content>

