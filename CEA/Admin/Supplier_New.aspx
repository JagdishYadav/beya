﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Supplier_New.aspx.vb" Inherits="CEA.Supplier_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">

        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Success") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "UpdateSuccess") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteSuccess") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorAlreadyExist") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorNotExist") %>';
            }
            else if (msgno == 6) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorOtherMissing") %>';
            }
            else if (msgno == 7) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorSuppAlreadyExist") %>';
            }
            else if (msgno == 8) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorPassMatch")%>';
            }

            alert(msgstring);
        }

        function checkBoxListOnClick(elementRef) {
            var textBoxRef = document.getElementById('<%= txtOther.ClientID %>');
            if (elementRef.rows.length > 0) {
                checkboxCell = elementRef.rows[elementRef.rows.length - 1].cells[1];
                for (j = 0; j < checkboxCell.childNodes.length; j++) {
                    if (checkboxCell.childNodes[j].type == "checkbox") {

                        if (checkboxCell.childNodes[j].checked == true) {
                            textBoxRef.style.display = '';
                        }
                        else {
                            textBoxRef.style.display = 'None';
                        }
                    }
                }
            }

            return true;
        }
        function checkOtherValue(source, args) {
            var textBoxRef = document.getElementById('<%= txtOther.ClientID %>');
            if (textBoxRef.style.display == '' && textBoxRef.value == '') {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }

    </script>
</asp:Content>
<%--<asp:Content ID="Content3" runat="server" contentplaceholderid="PageTitle">
    SupplierAUD
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="addNewSupplierWrapper">

                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, NameE %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtNameE"></asp:Label>
                            <asp:TextBox ID="txtNameE" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvNameE" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNameE %>'
                                ControlToValidate="txtNameE" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, NameA %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtNameA"></asp:Label>
                            <asp:TextBox ID="txtNameA" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvNameA" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNameA %>'
                                ControlToValidate="txtNameA" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        </div>
                    </div>


                </div>

                <div id="div2" runat="server">
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, Address %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtAddress"></asp:Label>
                                <asp:TextBox ID="txtAddress" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <asp:Label ID="Label4" runat="server" Text='<%$ Resources:Resource, POBox %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtPOBox"></asp:Label>
                                <asp:TextBox ID="txtPOBox" runat="server" MaxLength="20" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <asp:Label ID="Label5" runat="server" Text='<%$ Resources:Resource, City %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtCity"></asp:Label>
                                <asp:TextBox ID="txtCity" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <asp:Label ID="Label6" runat="server" Text='<%$ Resources:Resource, ZipCode %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtZipCode"></asp:Label>
                                <asp:TextBox ID="txtZipCode" runat="server" MaxLength="20" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <asp:Label ID="Label7" runat="server" Text='<%$ Resources:Resource, Country %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtCountry"></asp:Label>
                                <asp:TextBox ID="txtCountry" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <asp:Label ID="Label8" runat="server" Text='<%$ Resources:Resource, PhoneNo %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtPhone"></asp:Label>
                                <asp:TextBox ID="txtPhone" runat="server" MaxLength="20" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <asp:Label ID="Label9" runat="server" Text='<%$ Resources:Resource, FaxNo %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtFax"></asp:Label>
                                <asp:TextBox ID="txtFax" runat="server" MaxLength="20" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <asp:Label ID="Label10" runat="server" Text='<%$ Resources:Resource, Email %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtEmail"></asp:Label>
                                <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="reEmail" ControlToValidate="txtEmail" runat="server"
                                    ErrorMessage='<%$ Resources:Resource, ErrorInValidEmail %>' ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ValidationGroup="Save" Display="Dynamic"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <asp:Label ID="lblUserID" runat="server" Text='<%$ Resources:Resource, UserNameForLogin %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtUserID"></asp:Label>
                                <asp:TextBox ID="txtUserID" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorUserName %>'
                                    ControlToValidate="txtUserID" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <asp:Label ID="Label12" runat="server" Text='<%$ Resources:Resource, Password %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtPswd"></asp:Label>
                                <asp:TextBox ID="txtPswd" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorPassword %>'
                                    ControlToValidate="txtPswd" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <asp:Label ID="Label13" runat="server" Text='<%$ Resources:Resource, ReTypePass %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtPassword"></asp:Label>
                                <asp:TextBox ID="txtPassword" runat="server" MaxLength="30" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvPass" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorReEnterPass %>'
                                    ControlToValidate="txtPassword" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <asp:Label ID="Label11" runat="server" Text='<%$ Resources:Resource, ApprovedBy %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="grdApprovedBy"></asp:Label>
                                <div class="table-responsive tableLayoutWrap altboarder">
                                    <asp:GridView ID="grdApprovedBy" runat="server" Font-Size="Smaller" Width="100%"
                                        AutoGenerateColumns="false" DataKeyNames="ID">
                                        <Columns>
                                            <asp:TemplateField HeaderText='<%$ Resources:Resource, ApprovedBy %>'>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNameE" runat="server" Text='<%# Eval("NameE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText='<%$ Resources:Resource, ApprovedBy %>'>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNameA" runat="server" Text='<%# Eval("NameA")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkApproved" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText='<%$ Resources:Resource, ReferenceNo %>'>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("RefNo")%>' MaxLength="15" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:TextBox ID="txtOther" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                    <asp:CustomValidator ID="cvmodulelist" runat="server" Display="None" ErrorMessage='<%$ Resources:Resource, ErrorOtherMissing %>'
                                        ClientValidationFunction="checkOtherValue" ValidationGroup="Save"></asp:CustomValidator>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-right mt-4 buttonActionGroup">
                    <div class="btn-group commanButtonGroup mb-3">
                        <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="btn btn-primary btn-min-120" ValidationGroup="Save" />
                        <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Cancel %>' CssClass="btn btn-primary btn-min-120" />
                        <asp:Button ID="btnNew" runat="server" Text='<%$ Resources:Resource, New %>' CssClass="btn btn-primary btn-min-120" />
                        <asp:Button ID="btnLogin" runat="server" Text='<%$ Resources:Resource, Login %>' CssClass="btn btn-primary btn-min-120" />
                    </div>
                </div>

            </asp:Panel>
            <div class="large-9 columns">
                <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
                    ShowMessageBox="true" ValidationGroup="Save" DisplayMode="BulletList" />
            </div>
            <div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script>$(document).ready(function () { $("#sidr").show(); });</script>
</asp:Content>
<%--<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MiddlePart">
    <div class="row">
        <div class="large-12 medium-12 column">
            <div class="outerDivBtn">
                <div class="BTNdiv">
                </div>
            </div>
        </div>
    </div>
</asp:Content>--%>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="PageTitle">
    <asp:Label ID="lblHeader" runat="server" Text='<%$ Resources:Resource, CreateSupplier %>'></asp:Label>
</asp:Content>

