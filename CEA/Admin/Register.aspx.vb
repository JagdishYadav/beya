﻿Public Class Register
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            'Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
            'Menuhide.Value = "close"
            'Dim logout As Button = Master.FindControl("Logout")
            'logout.Visible = False
            'Dim lbl As Label = Master.FindControl("loginAs")
            'lbl.Visible = False
            'lbl = Master.FindControl("Welcome")
            'lbl.Visible = False
            'Dim lnlButton As LinkButton = Master.FindControl("LinkButton1")
            'lnlButton.Visible = False

            Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
            Dim menuItems As MenuItemCollection = Menu1.Items
            If (menuItems.Count > 15) Then
                menuItems.RemoveAt(12)
                menuItems.RemoveAt(12)
                menuItems.RemoveAt(12)
                menuItems.RemoveAt(12)
            End If

            Dim objSqlConfig As New ConfigClassSqlServer()
            DropDownList1.DataSource = objSqlConfig.GetCountries()
            DropDownList1.DataValueField = "ID"
            DropDownList1.DataTextField = "NameE"
            DropDownList1.DataBind()
            DropDownList1.SelectedIndex = 0
            txtDate.Text = Format(Today.AddDays(182).Day, "00") + "/" + Format(Today.AddDays(182).Month, "00") + "/" + Format(Today.AddDays(182).Year, "0000")

            ddlClientType.DataSource = objSqlConfig.GetClientTypes()
            ddlClientType.DataTextField = "NameE"
            ddlClientType.DataValueField = "ID"
            ddlClientType.DataBind()

            txtCompName.ReadOnly = False
            ddlClientType.Enabled = True
            DropDownList1.Enabled = True
            'chkTemp.Checked = False
            rdoList.Items(1).Selected = False
            rdoList.Items(2).Selected = False
            rdoList.Items(3).Selected = False
            If (Request.QueryString("ID") IsNot Nothing) Then
                rfvName.ValidationGroup = "Abc"
                rfvPass.ValidationGroup = "Abc"
                rfvPass1.ValidationGroup = "Abc"
                LoadCompanyData(Request.QueryString("ID").ToString())
                txtCompName.ReadOnly = True
                ddlClientType.Enabled = False
                DropDownList1.Enabled = False
            Else
                rfvName.ValidationGroup = "Save"
                rfvPass.ValidationGroup = "Save"
                rfvPass1.ValidationGroup = "Save"
            End If
        End If
    End Sub
    Private Sub LoadCompanyData(strID As String)
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dtData As New DataTable()
        dtData = objSqlConfig.GetCompaniesByID(strID)
        If (dtData.Rows.Count > 0) Then
            txtCompName.Text = dtData.Rows(0).Item(1).ToString()
            DropDownList1.SelectedValue = dtData.Rows(0).Item(10).ToString()
            txtEmail.Text = dtData.Rows(0).Item(12).ToString()
            txtPhone.Text = dtData.Rows(0).Item(4).ToString()
            txtFax.Text = dtData.Rows(0).Item(5).ToString()
            txtPOBox.Text = dtData.Rows(0).Item(6).ToString()
            txtZipCode.Text = dtData.Rows(0).Item(7).ToString()
            txtWebsite.Text = dtData.Rows(0).Item(8).ToString()
            rdoStatus.SelectedValue = dtData.Rows(0).Item(9).ToString()
            txtUsers.Text = dtData.Rows(0).Item(2).ToString()
            ddlClientType.SelectedValue = dtData.Rows(0).Item(11).ToString()
            If (dtData.Rows(0).Item(3).ToString().Length > 0) Then
                Dim dt As DateTime = DateTime.ParseExact(dtData.Rows(0).Item(3).ToString(), "yyyyMMddHHmmss", Nothing)
                txtDate.Text = dt.ToString("dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            End If
            'chkTemp.Checked = dtData.Rows(0).Item(13).ToString()
            Dim strCompanyType As String = dtData.Rows(0).Item(13).ToString()

            If (strCompanyType = "0") Then
                rdoList.Items(0).Selected = True
            ElseIf (strCompanyType = "1") Then
                rdoList.Items(1).Selected = True
            ElseIf (strCompanyType = "2") Then
                rdoList.Items(2).Selected = True
            ElseIf (strCompanyType = "3") Then
                rdoList.Items(3).Selected = True
            End If
            Dim strPassword As String = Encryption.Decrypt(dtData.Rows(0).Item(15).ToString())
            txtName.Text = dtData.Rows(0).Item(14).ToString()
            txtPass.Text = strPassword
            txtPass1.Text = strPassword
        End If
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim strExpiryDate As String = Now.AddDays(182).ToString("yyyyMMddHHmmss")
        Dim strFileName As String = ""


        If (txtPass.Text.Trim() <> txtPass1.Text.Trim()) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
            txtPass1.Focus()
            Return
        End If

        If (uplFile.HasFile) Then
            Dim fileExt As String = System.IO.Path.GetExtension(uplFile.FileName).ToLower()
            If (fileExt = ".ico" Or fileExt = ".png" Or fileExt = ".jpeg" Or fileExt = ".img" Or fileExt = ".gif" Or fileExt = ".bmp") Then
                Dim sDateTimeNow As String = DateTime.Now.ToString("yyyyMMddHHmmss")
                strFileName = txtCompName.Text.Trim() + "_" + sDateTimeNow + fileExt
                uplFile.SaveAs(Server.MapPath("../Upload/CompanyLogo/" + strFileName))
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('5');", True)
                Return
            End If
        End If

        If (Request.QueryString("ID") IsNot Nothing) Then
            If (objSqlConfig.UpdateCompany(Request.QueryString("ID").ToString(), txtCompName.Text.Trim(), DropDownList1.SelectedValue, txtEmail.Text.Trim(), strExpiryDate, ddlClientType.SelectedValue.ToCharArray(), txtPhone.Text.Trim(), txtFax.Text.Trim(), txtPOBox.Text.Trim(), txtZipCode.Text.Trim(), txtWebsite.Text.Trim(), rdoStatus.SelectedValue, strFileName, rdoList.SelectedValue.ToString(), txtName.Text.Trim(), txtPass.Text.Trim()) = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
            End If
        Else
            If (objSqlConfig.Create_Company_User(txtName.Text.Trim(), txtCompName.Text.Trim(), DropDownList1.SelectedValue, txtPass.Text.Trim(), txtEmail.Text.Trim(), strExpiryDate, ddlClientType.SelectedValue.ToCharArray(), txtPhone.Text.Trim(), txtFax.Text.Trim(), txtPOBox.Text.Trim(), txtZipCode.Text.Trim(), txtWebsite.Text.Trim(), rdoStatus.SelectedValue, strFileName, rdoList.SelectedValue.ToString()) = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
            End If
        End If
    End Sub
    'Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
    '    Response.Redirect("../Login.aspx")
    'End Sub
End Class