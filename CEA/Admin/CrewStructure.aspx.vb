﻿Public Class CrewStructure
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then

                Session("Selected") = "-1"

                Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                Menuhide.Value = "close"
                Dim logout As Button = Master.FindControl("Logout")
                logout.Visible = False

                Button4.OnClientClick = " window.opener.document.getElementById('ctl00_MainContent_Button3').click(); window.close();"

                Dim dt As New DataTable()
                If (Session("CrewList") IsNot Nothing) Then

                    dt = Session("CrewList")
                    Dim arrID As String() = Request.QueryString.Get("ID").Split(",")
                    For k As Integer = 0 To arrID.Length - 2
                        Dim index As Integer = Integer.Parse(arrID(k))
                        If (dt.Rows.Count > index) Then
                            dt.Rows(index).Delete()
                        End If
                    Next
                    dt.AcceptChanges()
                    Session("CrewList") = dt
                End If

                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    Panel2.Style.Add("direction", "rtl")
                    Panel2.Style.Add("text-align", "right")
                    'Panel3.Style.Add("direction", "rtl")
                    'Panel3.Style.Add("text-align", "right")
                    div1.Style.Add("float", "right")
                    div2.Style.Add("float", "right")
                    div3.Style.Add("float", "right")
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
            LoadDataGrid_Labour()
            LoadDataGrid_Equipment()
            Panel1.Width = grdEquipmet.Width
            Panel2.Width = grdEquipmet.Width
    End Sub
    Private Sub LoadDataGrid_Labour()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        dt = objSqlConfig.GetLabours(txtSearch1.Text)
        lblTotLabour.Text = "0"
        If (dt.Rows.Count > 0) Then
            grdLabours.DataSource = dt
            grdLabours.DataBind()
            lblTotLabour.Text = dt.Rows.Count
        Else
            grdLabours.DataSource = Nothing
            grdLabours.DataBind()
        End If
    End Sub
    Private Sub LoadDataGrid_Equipment()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        dt = objSqlConfig.GetEquipments(txtSearch.Text, Session("CompanyID"))
        lblTotEquip.Text = "0"
        If (dt.Rows.Count > 0) Then
            grdEquipmet.DataSource = dt
            grdEquipmet.DataBind()
            lblTotEquip.Text = dt.Rows.Count
        Else
            grdEquipmet.DataSource = Nothing
            grdEquipmet.DataBind()
        End If
    End Sub
    Protected Sub grdLabours_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdLabours.Sorting
        If (ViewState("SortState") IsNot Nothing And ViewState("SortState") = SortDirection.Ascending) Then
            ViewState("SortDirection") = "asc"
            ViewState("SortState") = 1
        Else
            ViewState("SortDirection") = "desc"
            ViewState("SortState") = 0
        End If
        ViewState("SortExp") = e.SortExpression
        FillGrid_Labours()
    End Sub
    Protected Sub grdLabours_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdLabours.PageIndexChanging
        grdLabours.PageIndex = e.NewPageIndex
        grdLabours.DataBind()
    End Sub
    Private Sub FillGrid_Labours()
        Dim Data As DataTable = grdLabours.DataSource
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp") Is Nothing) Then
                If ViewState("SortDirection").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp") & " " & "desc"
                End If

                grdLabours.DataSource = dv
                grdLabours.DataBind()
            End If
        End If
    End Sub
    Protected Sub grdEquipmet_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdEquipmet.Sorting
        If (ViewState("SortState1") IsNot Nothing And ViewState("SortState1") = SortDirection.Ascending) Then
            ViewState("SortDirection1") = "asc"
            ViewState("SortState1") = 1
        Else
            ViewState("SortDirection1") = "desc"
            ViewState("SortState1") = 0
        End If
        ViewState("SortExp1") = e.SortExpression
        FillGrid_Equipment()
    End Sub
    Protected Sub grdEquipmet_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdEquipmet.PageIndexChanging
        grdEquipmet.PageIndex = e.NewPageIndex
        grdEquipmet.DataBind()
    End Sub
    Private Sub FillGrid_Equipment()
        Dim Data As DataTable = grdEquipmet.DataSource
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp1") Is Nothing) Then
                If ViewState("SortDirection1").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp1") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp1") & " " & "desc"
                End If

                grdEquipmet.DataSource = dv
                grdEquipmet.DataBind()
            End If
        End If
    End Sub
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim d As Double
        If (txtCode.Text.Length = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
            Return
        End If
        If (txtProdRate.Text.Length = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
            txtProdRate.Focus()
            Return
        ElseIf (Double.TryParse(txtProdRate.Text, d) = False Or Double.Parse(txtProdRate.Text) = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
            txtProdRate.Focus()
            Return
        End If

        'check if selected crew is already added
        Dim dt As New DataTable()
        If (Session("CrewList") IsNot Nothing) Then
            dt = Session("CrewList")
            'For i As Integer = 0 To dt.Rows.Count - 1
            '    If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
            '        'If (txtCode.Text.Equals(dt.Rows(i).Item(0).ToString()) And Session("Selected").ToString().Equals(dt.Rows(i).Item(5).ToString())) Then
            '        If (txtCode.Text.Equals(dt.Rows(i).Item(0).ToString())) Then
            '            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
            '            Return
            '        End If
            '    End If
            'Next
        End If

        'If (txtProdRate.Text.Length = 0) Then
        '    txtProdRate.Text = "0.00"
        'End If
        If (txtHourlyRate.Text.Length = 0) Then
            txtHourlyRate.Text = "0.00"
        End If

        Dim dr As DataRow = dt.NewRow()
        If (dt.Columns.Count = 0) Then
            dt.Columns.Add("Code")
            dt.Columns.Add("DescE")
            dt.Columns.Add("DescA")
            dt.Columns.Add("ProductionRate")
            dt.Columns.Add("HourlyRate")
            'dt.Columns.Add("CatID")
        End If

        dr(0) = txtCode.Text
        dr(1) = txtDescE.Text
        dr(2) = txtDescA.Text
        dr(3) = txtProdRate.Text
        dr(4) = txtHourlyRate.Text
        'dr(5) = Session("Selected").ToString()

        dt.Rows.Add(dr)

        Session("CrewList") = dt

        If (txtProdRate.Text.Length = 0) Then
            txtProdRate.Text = "0.00"
        End If
        If (txtHourlyRate.Text.Length = 0) Then
            txtHourlyRate.Text = "0.00"
        End If

        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
        'txtCode.Text = ""
        'txtDescE.Text = ""
        'txtDescA.Text = ""
        'txtHourlyRate.Text = ""
        'txtProdRate.Text = "0.00"
        Session("Selected") = "-1"
    End Sub
    Protected Sub grdLabours_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdLabours.SelectedIndexChanged
        txtCode.Text = grdLabours.Rows(grdLabours.SelectedIndex).Cells(0).Text
        txtDescE.Text = grdLabours.Rows(grdLabours.SelectedIndex).Cells(1).Text
        txtDescA.Text = grdLabours.Rows(grdLabours.SelectedIndex).Cells(2).Text
        txtHourlyRate.Text = grdLabours.Rows(grdLabours.SelectedIndex).Cells(13).Text
        txtProdRate.Text = "0.00"
        Session("Selected") = "1"
    End Sub
    Protected Sub grdEquipmet_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdEquipmet.SelectedIndexChanged
        txtCode.Text = grdEquipmet.Rows(grdEquipmet.SelectedIndex).Cells(0).Text
        txtDescE.Text = grdEquipmet.Rows(grdEquipmet.SelectedIndex).Cells(1).Text
        txtDescA.Text = grdEquipmet.Rows(grdEquipmet.SelectedIndex).Cells(2).Text
        txtHourlyRate.Text = grdEquipmet.Rows(grdEquipmet.SelectedIndex).Cells(9).Text
        txtProdRate.Text = "0.00"
        Session("Selected") = "2"
    End Sub
End Class