﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master" CodeBehind="Supplier_quote.aspx.vb" Inherits="CEA.Supplier_quote" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function Showopoup(url) {

            params = 'width=' + screen.width;
            params += ', height=' + screen.height;
            params += ', top=0, left=0'
            params += ', fullscreen=yes';
            params += ', menubar=no, status=no, scrollbars=yes,titlebar=no, toolbar=no,directories=no';
            var win = window.open('', 'CrewStructure', params);
            win.location.href = url
            win = null;
            return false;
        }

        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 20) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "AllowedRange") %>';
            }
            else if (msgno == 23) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "QuotSaved")%>';
            }
            else if (msgno == 25) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "MissingCost") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorFileFormat") %>';
            }
            else if (msgno == 6) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NoFile") %>';
            }
            alert(msgstring);
        }
    </script>
    <script>$(document).ready(function () { $(".bodyWrapper .bodyContainer").addClass('p-0'); });</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MiddlePart" runat="server">
    <%--<div class="row">
        <div class="large-12 medium-12 column">
            <div class="outerDivBtn">
                <div class="BTNdiv">
                    <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Save %>'
                        CssClass="backBTN" ValidationGroup="Save" />
                    <asp:Button ID="Button5" runat="server" Text='<%$ Resources:Resource, Cancel %>' CssClass="BTNSave" />
                    <asp:Button ID="Button4" runat="server" Text='<%$ Resources:Resource, CreateCS %>' CssClass="BTNnext" />
                    <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, ViewQuote %>' CssClass="BTNnext" />
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="lblHeader" runat="server" Text='<%$ Resources:Resource, SupplierQuote %>'></asp:Label>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" class="supplierQuoteWrapper">
        <ContentTemplate>
            <div class="row mb-4">
                <div class="col-12 col-lg-auto order-lg-12">
                    <div class="btn-group commanButtonGroup mb-3">
                        <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="btn btn-primary btn-min-120" ValidationGroup="Save" />
                        <asp:Button ID="Button5" runat="server" Text='<%$ Resources:Resource, Cancel %>' CssClass="btn btn-primary btn-min-120" />
                        <asp:Button ID="Button4" runat="server" Text='<%$ Resources:Resource, CreateCS %>' CssClass="btn btn-primary btn-min-120" />
                        <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, ViewQuote %>' CssClass="btn btn-primary btn-min-120" />
                    </div>
                </div>
                <div class="col-12 col-lg order-lg-1">
                    <h2 class="title">
                        <asp:Label ID="Label7" runat="server" Text='<%$ Resources:Resource, AddingQuot %>' SkinID="Controllabel" CssClass="mb-2"></asp:Label>
                        <asp:Label ID="lblPath" runat="server" Text="" Font-Size="Smaller" SkinID="Controllabel" CssClass="small"></asp:Label>
                    </h2>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-auto">
                    <div class="form-group">
                        <asp:Image ID="imgCrew" runat="server" Height="225px" Width="225px" />
                    </div>
                </div>
                <div class="col-12 col-md">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, Unit %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtUnit"></asp:Label>
                                <asp:TextBox ID="txtUnit" runat="server" Width="100%" MaxLength="10"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvUnit" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorUnit %>' ControlToValidate="txtUnit" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, DO %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtDO"></asp:Label>
                                <asp:TextBox ID="txtDO" runat="server" Width="100%" MaxLength="7"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvDO" runat="server" ErrorMessage='<%$ Resources:Resource, MissingDailyOutput %>'
                                    ControlToValidate="txtDO" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="reDO" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                    SetFocusOnError="true" ControlToValidate="txtDO" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                    Display="None" ValidationGroup="Save"> </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, CrewStructure %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="ddlCrew"></asp:Label>
                                <asp:DropDownList ID="ddlCrew" runat="server" Width="100%" DataTextField="ProductID" DataValueField="ProductID" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <asp:Label ID="Label4" runat="server" Text='<%$ Resources:Resource, MaterialCost %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtMatCost"></asp:Label>
                                <asp:RegularExpressionValidator ID="reCost" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                    SetFocusOnError="true" ControlToValidate="txtCost" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                    Display="None" ValidationGroup="Save"> </asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtMatCost" runat="server" Width="100%" MaxLength="12"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="reMatCost" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                    SetFocusOnError="true" ControlToValidate="txtMatCost" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                    Display="None" ValidationGroup="Save"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <asp:Label ID="Label5" runat="server" Text='<%$ Resources:Resource, CostOfLineItem %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtCost"></asp:Label>
                                <asp:TextBox ID="txtCost" runat="server" Width="100%" MaxLength="12"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <asp:Label ID="Label6" runat="server" Text='<%$ Resources:Resource, IsPercent %>' SkinID="Controllabel" CssClass="control-label mb-3 d-block" AssociatedControlID="chkPercent"></asp:Label>
                                <asp:CheckBox ID="chkPercent" runat="server" Text='<%$ Resources:Resource, YES %>' Checked="false" />
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <asp:Label ID="Label9" runat="server" Text='<%$ Resources:Resource, UploadImage %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="uplFile"></asp:Label>
                                <asp:FileUpload ID="uplFile" runat="server" CssClass="btn btn-primary" />
                                <asp:Button ID="btnUpload" runat="server" Text='<%$ Resources:Resource, Upload %>' CssClass="btn btn-primary btn-min-120 mt-3" />

                                <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
                                    ShowMessageBox="true" ValidationGroup="Save" DisplayMode="BulletList" />
                                <asp:Button ID="Button3" runat="server" Text="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>

    <%--<script>$(document).ready(function () { $("#sidr").show(); });</script>--%>
</asp:Content>
