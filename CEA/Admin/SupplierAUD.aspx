﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="SupplierAUD.aspx.vb" Inherits="CEA.Screen1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">

        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Success") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "UpdateSuccess") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteSuccess") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorAlreadyExist") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorNotExist") %>';
            }
            else if (msgno == 6) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorOtherMissing") %>';
            }
            else if (msgno == 7) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorSuppAlreadyExist") %>';
            }
            else if (msgno == 8) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorPassword") %>';
            }
            else if (msgno == 9) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorUserName") %>';
            }
            alert(msgstring);
        }

        function checkBoxListOnClick(elementRef) {
            var textBoxRef = document.getElementById('<%= txtOther.ClientID %>');
            if (elementRef.rows.length > 0) {
                checkboxCell = elementRef.rows[elementRef.rows.length - 1].cells[1];
                for (j = 0; j < checkboxCell.childNodes.length; j++) {
                    if (checkboxCell.childNodes[j].type == "checkbox") {

                        if (checkboxCell.childNodes[j].checked == true) {
                            textBoxRef.style.display = '';
                        }
                        else {
                            textBoxRef.style.display = 'None';
                        }
                    }
                }
            }

            return true;
        }
        function checkOtherValue(source, args) {
            var textBoxRef = document.getElementById('<%= txtOther.ClientID %>');
            if (textBoxRef.style.display == '' && textBoxRef.value == '') {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }

    </script>
</asp:Content>
<%--<asp:Content ID="Content3" runat="server" contentplaceholderid="PageTitle">
    SupplierAUD
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="panel">
              
                <div class="row">
                    <div class="large-12 columns">
                        <asp:Label ID="lblSupplierID" runat="server" Text='<%$ Resources:Resource, SupplierID %>'></asp:Label>
                        <asp:TextBox ID="txtSupplierID" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvID" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorID %>'
                            ControlToValidate="txtSupplierID" SetFocusOnError="true" ValidationGroup="Save"
                            Display="None"></asp:RequiredFieldValidator>
                  
                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, NameE %>'></asp:Label>
                        <asp:TextBox ID="txtNameE" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNameE" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNameE %>'
                            ControlToValidate="txtNameE" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                    </div>
                    <div id="div2" runat="server">
                        <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, NameA %>'></asp:Label>
                        <asp:TextBox ID="txtNameA" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNameA" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNameA %>'
                            ControlToValidate="txtNameA" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                   
                        <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, Address %>'></asp:Label>
                        <asp:TextBox ID="txtAddress" runat="server" MaxLength="50"></asp:TextBox>
                    
                        <asp:Label ID="lblUserID" runat="server" Text='<%$ Resources:Resource, UserName %>'></asp:Label>
                        <asp:TextBox ID="txtUserID" runat="server" MaxLength="50"></asp:TextBox>
                  
                        <asp:Label ID="Label12" runat="server" Text='<%$ Resources:Resource, Password %>'></asp:Label>
                        <asp:TextBox ID="txtPswd" runat="server" MaxLength="50"></asp:TextBox>
                   
                        <asp:Label ID="Label4" runat="server" Text='<%$ Resources:Resource, POBox %>'></asp:Label>
                        <asp:TextBox ID="txtPOBox" runat="server" MaxLength="20"></asp:TextBox>
                        <asp:Label ID="Label5" runat="server" Text='<%$ Resources:Resource, City %>'></asp:Label>
                        <asp:TextBox ID="txtCity" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="Label6" runat="server" Text='<%$ Resources:Resource, ZipCode %>'></asp:Label>
                        <asp:TextBox ID="txtZipCode" runat="server" MaxLength="20"></asp:TextBox>
                        <asp:Label ID="Label7" runat="server" Text='<%$ Resources:Resource, Country %>'></asp:Label>
                        <asp:TextBox ID="txtCountry" runat="server" MaxLength="50"></asp:TextBox>
                        
                       
                        <%--<asp:CheckBoxList ID="chkList" runat="server" Font-Size="Smaller" Width="100%">
                        </asp:CheckBoxList>--%>
                        
                            <asp:Label ID="Label8" runat="server" Text='<%$ Resources:Resource, PhoneNo %>'></asp:Label>
                            <asp:TextBox ID="txtPhone" runat="server" MaxLength="20"></asp:TextBox>
                            <asp:Label ID="Label9" runat="server" Text='<%$ Resources:Resource, FaxNo %>'></asp:Label>
                            <asp:TextBox ID="txtFax" runat="server" MaxLength="20"></asp:TextBox>
                            <asp:Label ID="Label10" runat="server" Text='<%$ Resources:Resource, Email %>'></asp:Label>
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="50"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reEmail" ControlToValidate="txtEmail" runat="server"
                                ErrorMessage='<%$ Resources:Resource, ErrorInValidEmail %>' ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ValidationGroup="Save"></asp:RegularExpressionValidator>
                       <div class="clear"></div>
                       <asp:Label ID="Label11" runat="server" Text='<%$ Resources:Resource, ApprovedBy %>'></asp:Label>
                        <div class="clear"></div>
                        <br />
                        <asp:GridView ID="grdApprovedBy" runat="server" Font-Size="Smaller" Width="100%"
                            AutoGenerateColumns="false" DataKeyNames="ID">
                            <Columns>
                                <asp:TemplateField HeaderText='<%$ Resources:Resource, ApprovedBy %>'>
                                    <ItemTemplate>
                                        <asp:Label ID="lblNameE" runat="server" Text='<%# Eval("NameE")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:Resource, ApprovedBy %>'>
                                    <ItemTemplate>
                                        <asp:Label ID="lblNameA" runat="server" Text='<%# Eval("NameA")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkApproved" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:Resource, ReferenceNo %>'>
                                    <ItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("RefNo")%>' MaxLength="15"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:TextBox ID="txtOther" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:CustomValidator ID="cvmodulelist" runat="server" Display="None" ErrorMessage='<%$ Resources:Resource, ErrorOtherMissing %>'
                            ClientValidationFunction="checkOtherValue" ValidationGroup="Save"></asp:CustomValidator>
                    </div>
                   
                    
                </div>
            </asp:Panel>
            <div class="large-9 columns">
                <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
                    ShowMessageBox="true" ValidationGroup="Save" DisplayMode="BulletList" />
            </div>
            <div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MiddlePart">
    <div class="row">
        <div class="large-12 medium-12 column">
            <div class="outerDivBtn">
                <div class="BTNdiv">
                    <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave"
                        ValidationGroup="Save" />
                    <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Cancel %>'
                        CssClass="BTNCancel" />
                    <asp:Button ID="btnNew" runat="server" Text='<%$ Resources:Resource, New %>' CssClass="BTNEdit" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" runat="server" contentplaceholderid="PageTitle">
 <asp:Label ID="lblHeader" runat="server" Text='<%$ Resources:Resource, CreateSupplier %>'></asp:Label>
</asp:Content>

