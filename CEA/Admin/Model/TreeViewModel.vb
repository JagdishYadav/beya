﻿Public Class TreeViewModel
    Private _nameA As String
    Public Property NameA() As String
        Get
            Return _nameA
        End Get
        Set(ByVal value As String)
            _nameA = value
        End Set
    End Property


    Private _nameE As String
    Public Property NameE() As String
        Get
            Return _nameE
        End Get
        Set(ByVal value As String)
            _nameE = value
        End Set
    End Property

    Private _id As Long

    Public Property Id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            _id = value
        End Set
    End Property

    Private _no As String

    Public Property No() As String

        Get
            Return _no
        End Get
        Set(ByVal value As String)
            _no = value
        End Set
    End Property

    Private _parentNo As String

    Public Property ParentNo() As String
        Get
            Return _parentNo
        End Get
        Set(ByVal value As String)
            _parentNo = value
        End Set
    End Property

    Private _chields As List(Of TreeViewModel)

    Public Property Chields() As List(Of TreeViewModel)
        Get
            Return _chields
        End Get
        Set(ByVal value As List(Of TreeViewModel))
            _chields = value
        End Set
    End Property

    Private _crewCode As String

    Public Property CrewCode() As String
        Get
            Return _crewCode
        End Get
        Set(ByVal value As String)
            _crewCode = value
        End Set
    End Property

    Private _isPercent As String

    Public Property IsPercent() As String
        Get
            Return _isPercent
        End Get
        Set(ByVal value As String)
            _isPercent = value
        End Set
    End Property

    Private _cost As String

    Public Property Cost() As String
        Get
            Return _cost
        End Get
        Set(ByVal value As String)
            _cost = value
        End Set
    End Property

    Private _materialCost As String

    Public Property MaterialCost() As String
        Get
            Return _materialCost
        End Get
        Set(ByVal value As String)
            _materialCost = value
        End Set
    End Property

    Private _unit As String

    Public Property Unit() As String
        Get
            Return _unit
        End Get
        Set(ByVal value As String)
            _unit = value
        End Set
    End Property

    Private _dailyOutput As String

    Public Property DailyOutput() As String
        Get
            Return _dailyOutput
        End Get
        Set(ByVal value As String)
            _dailyOutput = value
        End Set
    End Property

End Class
