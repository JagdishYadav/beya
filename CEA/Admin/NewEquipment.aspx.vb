﻿Public Class NewEquipment
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (Session("User_Role_ID").ToString().Contains("999")) Then
                    If (menuItems.Count > 15) Then
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                    End If
                ElseIf (Session("User_Role_ID").ToString().Contains("888")) Then
                    If (menuItems.Count > 5) Then
                        menuItems.RemoveAt(0)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                    End If
                End If
                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    'LastRow.Style.Add("direction", "rtl")
                    'LastRow.Style.Add("text-align", "right")
                    'tdsave.Align = "left"
                    'tdcancel.Align = "right"
                    'header1.Style.Add("float", "right")
                    'header2.Style.Add("float", "right")
                    'div1.Style.Add("float", "right")
                    'div2.Style.Add("float", "right")
                    'div3.Style.Add("float", "right")
                    'div4.Style.Add("float", "right")
                    'div5.Style.Add("float", "right")
                    'div6.Style.Add("float", "right")
                    'div7.Style.Add("float", "right")
                    'div8.Style.Add("float", "right")
                End If

                txtCode.Enabled = True
                If (Request.QueryString.Get("ID") IsNot Nothing) Then
                    Button1.Text = GetGlobalResourceObject("Resource", "Update").ToString()
                    FillEquipmentData(Request.QueryString.Get("ID").ToString())
                    txtCode.Enabled = False
                Else
                    Button1.Text = GetGlobalResourceObject("Resource", "Save").ToString()
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        txtCode.Text = ""
        txtDescE.Text = ""
        txtDescA.Text = ""
        txtUnit.Text = ""
        txtOpCost.Text = "0.00"
        txtDepCost.Text = "0.00"
        txtDailyCost.Text = "0.00"
        txtRentD.Text = "0.00"
        txtRentM.Text = "0.00"
    End Sub
    Private Sub FillEquipmentData(strEquipmentCode As String)
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        dt = objSqlConfig.GetEquipmentByCodeAndCompany(strEquipmentCode, Session("CompanyID"))
        If (dt.Rows.Count > 0) Then
            txtCode.Text = dt.Rows(0).Item(0).ToString()
            txtDescE.Text = dt.Rows(0).Item(1).ToString()
            txtDescA.Text = dt.Rows(0).Item(2).ToString()
            txtUnit.Text = dt.Rows(0).Item(3).ToString()
            txtOpCost.Text = dt.Rows(0).Item(4).ToString()
            txtDepCost.Text = dt.Rows(0).Item(5).ToString()
            txtDailyCost.Text = dt.Rows(0).Item(6).ToString()
            txtRentD.Text = dt.Rows(0).Item(7).ToString()
            txtRentM.Text = dt.Rows(0).Item(8).ToString()
        End If
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim objSqlConfig As New ConfigClassSqlServer()

        If (txtOpCost.Text.Length = 0) Then
            txtOpCost.Text = "0.00"
        End If
        If (txtDepCost.Text.Length = 0) Then
            txtDepCost.Text = "0.00"
        End If
        If (txtDailyCost.Text.Length = 0) Then
            txtDailyCost.Text = "0.00"
        End If
        If (txtRentD.Text.Length = 0) Then
            txtRentD.Text = "0.00"
        End If
        If (txtRentM.Text.Length = 0) Then
            txtRentM.Text = "0.00"
        End If

        If (Request.QueryString.Get("ID") Is Nothing) Then
            Dim retVal As Integer = objSqlConfig.CreateEquipment(txtCode.Text.Trim(), txtDescE.Text.Trim(), txtDescA.Text.Trim(), txtUnit.Text.Trim(), txtOpCost.Text.Trim(), txtDepCost.Text.Trim(), txtDailyCost.Text.Trim(), txtRentD.Text.Trim(), txtRentM.Text.Trim(), Session("CompanyID"))
            If (retVal = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
                Button2_Click(sender, Nothing)
            ElseIf (retVal = 1) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('-1');", True)
            End If
        Else
            Dim retVal As Integer = objSqlConfig.UpdateEquipment(txtCode.Text.Trim(), txtDescE.Text.Trim(), txtDescA.Text.Trim(), txtUnit.Text.Trim(), txtOpCost.Text.Trim(), txtDepCost.Text.Trim(), txtDailyCost.Text.Trim(), txtRentD.Text.Trim(), txtRentM.Text.Trim(), Session("CompanyID"))
            If (retVal = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
                'Button2_Click(sender, Nothing)
            ElseIf (retVal = 1) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('5');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('-1');", True)
            End If
        End If
    End Sub
    Protected Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        Response.Redirect("NewEquipment.aspx")
    End Sub
End Class