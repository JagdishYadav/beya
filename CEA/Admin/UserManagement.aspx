﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="UserManagement.aspx.vb" Inherits="CEA.UserManagment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        function ShowConfirm() {
            var userName = document.getElementById('<%=TextBox3.ClientID%>').value;
            var pass = document.getElementById('<%=TextBox1.ClientID%>').value;

            if (userName != "" && pass != "") {
                var strConfirm = '<%=GetGlobalResourceObject("Resource", "ConfirmDelete") %>';
                return confirm(strConfirm);
            }
            else {
                var strMsg = '<%=GetGlobalResourceObject("Resource", "PleaseSelect") %>';
                alert(strMsg);
                return false;
            }
        }
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Success") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "UpdateSuccess") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteSuccess") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorAlreadyExist") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorNotExist") %>';
            }
            else if (msgno == 6) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorRoleSelection") %>';
            }
    alert(msgstring);
}
    </script>
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
User Management
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-12">
                    <asp:Panel ID="Panel1" runat="server" CssClass="mb-5">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-5 col-xl-4">
                                <%--<asp:Label ID="Label15" runat="server" Text='<%$ Resources:Resource, Search %>' Width="100%"></asp:Label>--%>
                                <div class="input-group mb-3 commanSearchGroup">
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control"></asp:TextBox>
                                  <div class="input-group-append">
                                    <asp:Button ID="Button5" runat="server" Text='<%$ Resources:Resource, Search %>' CssClass="btn btn-primary" />
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive tableLayoutWrap altboarder">
                            <asp:GridView ID="grdUsers" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="5" DataKeyNames="ID" SelectedRowStyle-BackColor="Yellow" CssClass="">
                                <Columns>
                                    <asp:BoundField DataField="Name" SortExpression="Name" HeaderText='<%$ Resources:Resource, Name %>'
                                        ItemStyle-Width="23%" />
                                    <asp:BoundField DataField="Email" SortExpression="Email" HeaderText='<%$ Resources:Resource, Email %>'
                                        ItemStyle-Width="23%" />
                                    <asp:BoundField DataField="UserID" SortExpression="UserID" HeaderText='<%$ Resources:Resource, UserName %>'
                                        ItemStyle-Width="22%" />
                                    <asp:BoundField DataField="Password" SortExpression="Password" HeaderText='<%$ Resources:Resource, Password %>'
                                        ItemStyle-Width="22%" Visible="false" />
                                    <asp:BoundField DataField="User_Role_ID" SortExpression="User_Role_ID" Visible="false"
                                        HeaderText="Role_ID" />
                                    <asp:CommandField ShowCancelButton="false" ShowDeleteButton="false" ShowEditButton="false"
                                        ShowInsertButton="false" ItemStyle-Width="10%" ShowSelectButton="true" SelectText='<%$ Resources:Resource, Select %>' />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <asp:Panel ID="Panel2" runat="server" CssClass="addUserPanel">
                <div class="row justify-content-between">
                    <div class="col-12 col-sm-auto">
                        <h2 class="headingTitle mb-3">Add User</h2>
                    </div>
                    <div class="col-12 col-sm-auto">
                        <div class="btn-group commanButtonGroup mb-3">
                            <asp:Button ID="Button4" runat="server" Text='<%$ Resources:Resource, Cancel %>'
                            CssClass="btn btn-primary btn-min-120" />
                            <asp:Button ID="btnNew" runat="server" Text='<%$ Resources:Resource, New %>'
                            CssClass="btn btn-primary btn-min-120" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, FirstName %>'></asp:Label>
                                    <asp:TextBox ID="txtFName" runat="server" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvFName" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorFirstName %>'
                                        ControlToValidate="txtFName" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label4" runat="server" Text='<%$ Resources:Resource, LastName %>'></asp:Label>
                                    <asp:TextBox ID="txtLName" runat="server" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorLastName %>'
                                        ControlToValidate="txtLName" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label5" runat="server" Text='<%$ Resources:Resource, Email %>'></asp:Label>
                                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="50"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="reEmail" runat="server" ErrorMessage='<%$ Resources:Resource, InvalidEmail %>'
                                        SetFocusOnError="true" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        Display="None" ValidationGroup="Save"> </asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, UserName %>'></asp:Label>
                                    <asp:TextBox ID="TextBox3" runat="server" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorUserName %>'
                                        ControlToValidate="TextBox3" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, Password %>'></asp:Label>
                                    <asp:TextBox ID="TextBox1" runat="server" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvPass" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorPassword %>'
                                        ControlToValidate="TextBox1" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-6">
                        <div class="table-responsive tableLayoutWrap">
                            <asp:GridView ID="grdRoles" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                AllowSorting="false" EmptyDataText="No Data" Width="100%" PageSize="10" DataKeyNames="ID">
                                <Columns>
                                    <asp:BoundField DataField="NameE" SortExpression="NameE" HeaderText='<%$ Resources:Resource, UserRole %>'
                                        ItemStyle-Width="90%" />
                                    <asp:BoundField DataField="NameA" SortExpression="NameA" HeaderText='<%$ Resources:Resource, UserRole %>'
                                        ItemStyle-Width="90%" />
                                    <asp:TemplateField HeaderText='<%$ Resources:Resource, Select %>' ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="actionButtonGroup py-4 mt-4 d-md-flex justify-content-md-end">
                            <div class="btn-group commanButtonGroup">
                                <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Add %>' CssClass="btn btn-primary btn-min-120"
                                ValidationGroup="Save" />
                                <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Delete %>' CssClass="btn btn-primary btn-min-120"
                                    OnClientClick="return ShowConfirm()" />
                                <asp:Button ID="Button3" runat="server" Text='<%$ Resources:Resource, Update %>'
                                    CssClass="btn btn-primary btn-min-120" ValidationGroup="Save" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
                ShowMessageBox="true" ValidationGroup="Save" DisplayMode="BulletList" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<%--<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MiddlePart">
    <div class="row">

        <div class="large-12 medium-12 column">
            <div class="outerDivBtn">
                <div class="BTNdiv">
                    <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Add %>' CssClass="BTNAdd"
                        ValidationGroup="Save" />

                    <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Delete %>' CssClass="BTNDelete"
                        OnClientClick="return ShowConfirm()" />
                    <asp:Button ID="Button3" runat="server" Text='<%$ Resources:Resource, Update %>'
                        CssClass="BTNUpdate" ValidationGroup="Save" />
                    <asp:Button ID="Button4" runat="server" Text='<%$ Resources:Resource, Cancel %>'
                        CssClass="BTNCancel" />
                    <asp:Button ID="btnNew" runat="server" Text='<%$ Resources:Resource, New %>'
                        CssClass="BTNCancel" />
                </div>
            </div>
        </div>

    </div>
</asp:Content>--%>

<asp:Content ID="Content5" runat="server" contentplaceholderid="PageTitle">
 <asp:Label ID="lblHeader" runat="server" Text='<%$ Resources:Resource, UserManagement %>'></asp:Label></h3>
        
    <script>$(document).ready(function () { $("#sidr").show(); });</script>    
    
</asp:Content>


