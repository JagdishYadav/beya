﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="AssociatedSuppliers.aspx.vb" Inherits="CEA.AssociatedSuppliers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DataSaved") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SuppAlreadyExist") %>';
            }
            alert(msgstring);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label14" runat="server" Text='<%$ Resources:Resource, Supplier %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <center>
                <div class="row">
                      <div class="large-12 medium-12 column">
                    <div class="outerDivBtn">
                        <div class="BTNdiv">
                        <div class="panelDiv" style="width:500px;">
                            <div class="row">
                                <div class="large-6 medium-6 columns bold">
                                    <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, ItemCode %>'
                                        Font-Bold="True" ForeColor="Red"></asp:Label>
                                </div>
                                <div class="large-6 medium-6 column">
                                    <asp:Label ID="lblProdCode" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-6 medium-6 columns bold">
                                    <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, ItemName %>'
                                        Font-Bold="True" ForeColor="Red"></asp:Label>
                                </div>
                                <div class="large-6 medium-6 column">
                                    <asp:Label ID="lblProdName" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                    <asp:Label ID="lblProdName2" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-6 medium-6 columns bold">
                                    <asp:Label ID="Label5" runat="server" Text='<%$ Resources:Resource, ItemUnit %>'
                                        Font-Bold="True" ForeColor="Red"></asp:Label>
                                </div>
                                <div class="large-6 medium-6 column">
                                    <asp:Label ID="lblProdUnit" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                        </div></div>
                    </div>
                </div>
                <br />
                <br />
                <div class="row" id="abc" runat="server">
                    <div class="large-12 medium-12 columns " id="grd1" runat="server">
                   
                    <div class="outerDivBtn">
                        <div class="BTNdiv">
                        <asp:Label ID="Label16" runat="server" Text='<%$ Resources:Resource, SupplierList %>'
                            CssClass="bold"></asp:Label>
                        <asp:GridView ID="grdLabour" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                            EmptyDataText="No Data" Width="100%" PageSize="10" DataKeyNames="SupplierID"
                            ShowFooter="True" HeaderStyle-Height="30px" RowStyle-Height="25px" FooterStyle-BackColor="ActiveBorder"
                            FooterStyle-Height="25px" AllowSorting="false" HeaderStyle-HorizontalAlign ="Center">
                            <Columns>
                                <asp:TemplateField HeaderText='<%$ Resources:Resource, SuppName %>' ItemStyle-Width="300px">
                                    <ItemTemplate>
                                        <asp:Label ID="l1" runat="server" Text='<% # Eval("NameE") %>' />
                                        <asp:Label ID="l2" runat="server" Text='<% # Eval("NameA") %>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:DropDownList ID="ddlSuppF" runat="server" Style="max-width: 300px">
                                        </asp:DropDownList>
                                    </FooterTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblSuppE" runat="server" Text='<% # Eval("SupplierID") %>' Visible="false" />
                                        <asp:DropDownList ID="ddlSuppE" runat="server" Style="max-width: 300px">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:Resource, MinPrice %>'>
                                    <ItemTemplate>
                                        <asp:Label ID="lblMinPrice" runat="server" Text='<% # Eval("MinPrice") %>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtMinPrice" runat="server" Style="max-width: 150px" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfv114" runat="server" ErrorMessage='<%$ Resources:Resource, MissingMinPrice %>'
                                            ControlToValidate="txtMinPrice" SetFocusOnError="true" ValidationGroup="SaveSupp"
                                            Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="reAvgSal1" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                            SetFocusOnError="true" ControlToValidate="txtMinPrice" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                            Display="None" ValidationGroup="SaveSupp">
                                        </asp:RegularExpressionValidator>
                                    </FooterTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtMinPriceE" runat="server" Style="max-width: 150px" MaxLength="10"
                                            Text='<% # Eval("MinPrice") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfv119" runat="server" ErrorMessage='<%$ Resources:Resource, MissingMinPrice %>'
                                            ControlToValidate="txtMinPriceE" SetFocusOnError="true" ValidationGroup="SaveSuppE"
                                            Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="re2345" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                            SetFocusOnError="true" ControlToValidate="txtMinPriceE" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                            Display="None" ValidationGroup="SaveSuppE"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:Resource, MaxPrice %>'>
                                    <ItemTemplate>
                                        <asp:Label ID="lblMaxPrice" runat="server" Text='<% # Eval("MaxPrice") %>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtMaxPrice" runat="server" Style="max-width: 150px" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfv1114" runat="server" ErrorMessage='<%$ Resources:Resource, MissingMaxPrice %>'
                                            ControlToValidate="txtMaxPrice" SetFocusOnError="true" ValidationGroup="SaveSupp"
                                            Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="reAvgSa5l1" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                            SetFocusOnError="true" ControlToValidate="txtMaxPrice" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                            Display="None" ValidationGroup="SaveSupp">
                                        </asp:RegularExpressionValidator>
                                    </FooterTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtMaxPriceE" runat="server" Style="max-width: 150px" MaxLength="10"
                                            Text='<% # Eval("MaxPrice") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfv1519" runat="server" ErrorMessage='<%$ Resources:Resource, MissingMaxPrice %>'
                                            ControlToValidate="txtMaxPriceE" SetFocusOnError="true" ValidationGroup="SaveSuppE"
                                            Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="re2314d5" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                            SetFocusOnError="true" ControlToValidate="txtMaxPriceE" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                            Display="None" ValidationGroup="SaveSuppE"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign ="Center">
                                    <FooterTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" Text='<%$ Resources:Resource, Save %>'
                                            OnClick="LinkButton1_Click" ValidationGroup="SaveSupp"></asp:LinkButton>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="LinkButton8" runat="server" CommandName="Update" Text='<%$ Resources:Resource, Update %>'
                                            ItemStyle-Width="25px" ValidationGroup="SaveSuppE"></asp:LinkButton>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton8" runat="server" CommandName="Edit" Text='<%$ Resources:Resource, Edit %>'
                                            ItemStyle-Width="25px"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="LinkButton9" runat="server" CommandName="Cancel" Text='<%$ Resources:Resource, Cancel %>'
                                            ItemStyle-Width="25px"></asp:LinkButton>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton9" runat="server" CommandName="Delete" Text='<%$ Resources:Resource, Delete %>'
                                            ItemStyle-Width="25px"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="ActiveBorder" Height="30px"></FooterStyle>
                            <HeaderStyle Height="30px"></HeaderStyle>
                            <RowStyle Height="25px"></RowStyle>
                        </asp:GridView>
                    </div></div></div>
                </div>
                <div class="row">
                     <div class="large-12 medium-12 column">
                   </div>
                </div>
            </center>
            <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
                ShowMessageBox="true" ValidationGroup="Save" DisplayMode="BulletList" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveSupp" DisplayMode="BulletList" />
            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveSuppE" DisplayMode="BulletList" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" runat="server" contentplaceholderid="MiddlePart">
 <div class="outerDivBtn">
                        <div class="BTNdiv">
                        <asp:Button ID="btnSave" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave"
                            ValidationGroup="Save" />
                        <asp:Button ID="Button8" runat="server" Text='<%$ Resources:Resource, Cancel %>'
                            CssClass="BTNCancel" />
                        <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Close %>' CssClass="BTNclose" />
                    </div>
                    </div>
</asp:Content>

