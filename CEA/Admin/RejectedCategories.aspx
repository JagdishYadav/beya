﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="RejectedCategories.aspx.vb" Inherits="CEA.RejectedCategories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        var _isInitialLoad = true;
        function pageLoad(sender, args) {
            if (_isInitialLoad) {
                _isInitialLoad = false;
                document.getElementById("<%= Button8.ClientID %>").click();
                //document.getElementById("<%= hdnInitialLoad.ClientID %>").value = "0";
            }
        }

        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorSelectNode") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteChild") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteSuccess") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorProdNotFound") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "HierarchyAlreadyExist") %>';
            }
            else if (msgno == 6) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorAddHierarchy") %>';
            }
            else if (msgno == 7) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "HierarchyAdded") %>';
            }
            else if (msgno == 8) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorCantAdd") %>';
            }
            else if (msgno == 9) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NonLeafNode") %>';
            }
            else if (msgno == 10) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "HasChildren") %>';
            }
            else if (msgno == 11) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NoRecFound") %>';
            }
            else if (msgno == 12) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ValueMissing") %>';
            }
            else if (msgno == 13) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NotExcel") %>';
            }
            else if (msgno == 14) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Imported") %>';
            }
            else if (msgno == 15) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ImportFailure") %>';
            }
            else if (msgno == 16) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NumberAlreadyExist") %>';
            }
            else if (msgno == 17) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NumberMissing") %>';
            }
            else if (msgno == 18) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "EngNameMissing") %>';
            }
            else if (msgno == 19) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "AraNameMissing") %>';
            }
            alert(msgstring);
        }

        function ShowConfirm() {
            var strConfirm = '<%=GetGlobalResourceObject("Resource", "ConfirmDelete") %>';
            if (confirm(strConfirm) == true) {
                //DeleteProductListConfirmation()
                return true;
            }
            else {
                return false;
            }
        }

        function DeleteProductListConfirmation() {
            var strConfirm = '<%=GetGlobalResourceObject("Resource", "ProdDeleteConfirm") %>';
            if (confirm(strConfirm) == true) {
                document.getElementById("<%= hdnResult.ClientID %>").value = "1";
            }
            else {
                document.getElementById("<%= hdnResult.ClientID %>").value = "0";
            }
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <%--<div class="update" id = "abc" runat = "server"> </div>--%>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <%--<div class="row">
                <div class="large-7 medium-7 columns" id="div3" runat="server">
                    <asp:LinkButton ID="LinkButton1" runat="server" Text='<%$ Resources:Resource, AddDiv %>'
                        CssClass="BTNAdd"></asp:LinkButton>
                <%--</div>
            </div>
            <div class="row">
                <div class="large-7 medium-7 columns">
                    <asp:LinkButton ID="LinkButton2" runat="server" CssClass="BTNImport" Text='<%$ Resources:Resource, ImportFromExcel %>'></asp:LinkButton>
                    <asp:FileUpload ID="uplImage" runat="server" CssClass="small button " Text='<%$ Resources:Resource, Browse %>' />
                </div>
            </div>--%>
            <div class="row">
                <div class="large-7 medium-7 columns" id="div1" runat="server">
                    <asp:Panel ID="Panel3" runat="server" CssClass="panel">
                    <asp:TreeView ID="tvLevels" runat="server" Width="100%" LineImagesFolder="~/TreeLineImages"
                        Visible="false" ShowLines="True" NodeWrap = "true" Height ="523px">
                    </asp:TreeView>
                    </asp:Panel>
                </div>
                <div class="large-5 medium-5 columns" id="div2" runat="server">
                    <asp:Panel ID="Panel1" runat="server" CssClass="panel">
                        <asp:Label ID="lblDivision" runat="server" Text='<%$ Resources:Resource, Div %>'
                            CssClass="label1 left" Width="50%"></asp:Label>
                        <asp:Label ID="Label8" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                        <asp:TextBox ID="txtNo1" runat="server" Width="15%" CssClass="left" ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel1E" runat="server" Width="35%" CssClass="left" MaxLength="70" SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtLevel1A" runat="server" Width="35%" CssClass="left" MaxLength="70" SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:Button ID="Button0" runat="server" Text="E" CssClass="small button large-1 left" />
                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, Sec1 %>' Width="50%"
                            CssClass="label1 left"></asp:Label>
                        <asp:Label ID="Label9" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                            <asp:TextBox ID="txtNo2" runat="server" Width="15%" CssClass="left" ReadOnly="true"  SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel2E" runat="server" Width="35%" CssClass="left" MaxLength="70"  SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtLevel2A" runat="server" Width="35%" CssClass="left" MaxLength="70"  SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:Button ID="Button1" runat="server" Text="E" CssClass="small button large-1 left" />
                        <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, Sec2 %>' Width="50%"
                            CssClass="label1 left"></asp:Label>
                        <asp:Label ID="Label10" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                                                        <asp:TextBox ID="txtNo3" runat="server" Width="15%" CssClass="left"  SkinID="txtMinwidth" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtLevel3E" runat="server" Width="35%" CssClass="left" MaxLength="70" SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtLevel3A" runat="server" Width="35%" CssClass="left" MaxLength="70" SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:Button ID="Button2" runat="server" Text="E" CssClass="small button large-1 left" />
                        <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, Sec3 %>' Width="50%"
                            CssClass="label1 left"></asp:Label>
                        <asp:Label ID="Label11" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                                                        <asp:TextBox ID="txtNo4" runat="server" Width="15%" CssClass="left" SkinID="txtMinwidth" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtLevel4E" runat="server" Width="35%" CssClass="left" MaxLength="70" SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtLevel4A" runat="server" Width="35%" CssClass="left" MaxLength="70" SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:Button ID="Button3" runat="server" Text="E" CssClass="small button large-1 left" />
                        <asp:Label ID="Label4" runat="server" Text='<%$ Resources:Resource, Sec4 %>' Width="50%"
                            CssClass="label1 left"></asp:Label>
                        <asp:Label ID="Label12" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                                                        <asp:TextBox ID="txtNo5" runat="server" Width="15%" CssClass="left" SkinID="txtMinwidth" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtLevel5E" runat="server" Width="35%" CssClass="left" MaxLength="70" SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtLevel5A" runat="server" Width="35%" CssClass="left" MaxLength="70" SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:Button ID="Button4" runat="server" Text="E" CssClass="small button large-1 left" />
                        <asp:Label ID="Label5" runat="server" Text='<%$ Resources:Resource, Sec5 %>' Width="50%"
                            CssClass="label1 left"></asp:Label>
                        <asp:Label ID="Label13" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                                                        <asp:TextBox ID="txtNo6" runat="server" Width="15%" CssClass="left" SkinID="txtMinwidth" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtLevel6E" runat="server" Width="35%" CssClass="left" MaxLength="70" SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtLevel6A" runat="server" Width="35%" CssClass="left" MaxLength="70" SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:Button ID="Button5" runat="server" Text="E" CssClass="small button large-1 left" />
                        <asp:Label ID="Label6" runat="server" Text='<%$ Resources:Resource, Sec6 %>' Width="50%"
                            CssClass="label1 left"></asp:Label>
                        <asp:Label ID="Label14" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                                                        <asp:TextBox ID="txtNo7" runat="server" Width="15%" CssClass="left" SkinID="txtMinwidth" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtLevel7E" runat="server" Width="35%" CssClass="left" MaxLength="70" SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtLevel7A" runat="server" Width="35%" CssClass="left" MaxLength="70" SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:Button ID="Button6" runat="server" Text="E" CssClass="small button large-1 left" />
                        <asp:Label ID="Label7" runat="server" Text='<%$ Resources:Resource, Sec7 %>' Width="50%"
                            CssClass="label1 left"></asp:Label>
                        <asp:Label ID="Label15" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                                                        <asp:TextBox ID="txtNo8" runat="server" Width="15%" CssClass="left" SkinID="txtMinwidth" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtLevel8E" runat="server" Width="35%" CssClass="left" MaxLength="70" SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtLevel8A" runat="server" Width="35%" CssClass="left" MaxLength="70" SkinID="txtMinwidth"
                            ReadOnly="true"></asp:TextBox>
                        <asp:Button ID="Button7" runat="server" Text="E" CssClass="small button large-1  left" />
                       <%-- <asp:Button ID="Button9" runat="server" Text='<%$ Resources:Resource, Add %>' CssClass="small button"
                            Width="48%" />--%>
                        <asp:Button ID="Button10" runat="server" Text='<%$ Resources:Resource, Delete %>'
                            CssClass="small button alert" Width="96%" />
                    </asp:Panel>
                    <%--<asp:Panel ID="Panel2" runat="server" CssClass="panel">
                        <%--<asp:Button ID="Button13" runat="server" Text="Show Non-Mapped Levels" CssClass="small button "
                            Width="97%" />--%>
                        <%-- <asp:Button ID="Button14" runat="server" Text="Reports" CssClass="small button success"
                            Width="32%" />--%>
                       <%-- <asp:Button ID="Button15" runat="server" Text='<%$ Resources:Resource, AddProduct %>'
                            CssClass="small button success" Width="97%" />--%>
                        <%-- <asp:Button ID="Button16" runat="server" Text="Browser" CssClass="small button success"
                            Width="32%" />
                        <asp:Button ID="Button17" runat="server" Text="Mapping" CssClass="small button" Width="48%" />--%>
                        <%--<asp:Button ID="Button18" runat="server" Text="Exit " CssClass="small button secondary"
                            Width="48%" />
                    </asp:Panel>--%>
                    <asp:Button ID="Button8" runat="server" Text="" />
                </div>
            </div>
        </ContentTemplate>
        
    </asp:UpdatePanel>
    <asp:HiddenField ID="hdnResult" runat="server" Value="-1" />
    <asp:HiddenField ID="hdnInitialLoad" runat="server" Value="1" />
    <script>$(document).ready(function () { $("#sidr").show(); });</script>
</asp:Content>
