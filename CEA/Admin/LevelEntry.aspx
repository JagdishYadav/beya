﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="LevelEntry.aspx.vb" Inherits="CEA.LevelEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }

        .style2 {
            margin-bottom: 1.25rem;
            padding: 10px 20px 10px 20px;
            margin: 0 0 10px 0;
            background: #fff;
            direction: ltr;
        }
    </style>
    <script type="text/javascript">
        var _isInitialLoad = true;
        function pageLoad(sender, args) {
            if (_isInitialLoad) {
                _isInitialLoad = false;
                document.getElementById("<%= Button8.ClientID %>").click();
                //document.getElementById("<%= hdnInitialLoad.ClientID %>").value = "0";
            }
        }


        function getItems() {
            $.ajax({
                url: "LevelEntry.aspx/GetMasterList",
                contentType: "Application/json; charset=utf-8",
                responseType: "json",
                method: "GET",
                success: function (response) {
                    getChields(response.d[0].No, 0)
                    console.log(response.d);
                    $.each(response.d, function (index, item) {
                        $('#treeMainList').append('<li><a href="javascript:void(0);" id="link_' + item.No + '" onclick="getChields(' + item.No + ', 1)"> ' + item.NameE + ' <em class= "fa fa-angle-right"></em></a></li>')
                        //$('#treeMainList').append('HELLO WORLD');
                    });
                    $("#treeMainList li:first-child a").addClass('active');
                },
                error: function (xhr) {
                    alert(xhr.status);
                },
                Failure: function (response) {
                    alert(response);
                }
            });
        }

        function getChields(parentNo, type) {
            if (type == 1) {
                $('#treeMainList li a').removeClass('active');
                setTimeout(function () {
                    $('#link_' + parentNo).addClass('active');
                }, 1000);
            }
            $('#ctl00_MainContent_updProgress').show();
            $.ajax({
                url: "LevelEntry.aspx/GetChields",
                contentType: "Application/json; charset=utf-8",
                data: { parentNo: parentNo },
                responseType: "json",
                method: "GET",
                success: function (response) {
                    console.log(response.d);
                    $('#dataTreeWrap').empty();
                    recurse(response.d, 'dataTreeWrap');
                    $('#ctl00_MainContent_updProgress').hide();
                },
                error: function (xhr) {
                    alert(xhr.status);
                    $('#ctl00_MainContent_updProgress').hide();
                },
                Failure: function (response) {
                    alert(response);
                    $('#ctl00_MainContent_updProgress').hide();
                }
            });
        }

        function recurse(node, id) {
            for (var i = 0; i < node.length; i++) {
                $('#' + id).append('<div class="dataTreeList" id="' + node[i].No + '">'
                    + '<div class="dataTreeRow">'
                    + '<div class="idBlock">' + node[i].No + '</div>'
                    + '<div class="titleBlock">'
                    + '<span class="spaceLeft">' + node[i].NameE + ''
                    + '<div class="actionBlock">'
                    + '<div class="dropdown actionDropdown">'
                    + '<a href="javascript:void(0);" class="dropdown-toggle" role="button" type="button" id="dropdownMenuButton_'+ id + i +'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><em class="fas fa-ellipsis-v"></em></a>'
                + '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton_' + id+'">'
                    + '<a class="dropdown-item" href="javascript:void(0);" data-toggle="modal" data-target="#addDivisionModal">Add New Nested Node</a>'
                    + '<a class="dropdown-item" href="javascript:void(0);" data-toggle="modal" data-target="#addDivisionModal">Update Node</a>'
                    + '<a class="dropdown-item" href="javascript:void(0);">Delete Node</a>'
                    + '<a class="dropdown-item" href="javascript:void(0);">Mark as Non-leaf Node</a>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</span>'
                    + '</div>'
                    + '</div>'
                    + '</div>');
                if (node[i].Chields != null && node[i].Chields.length > 0) {
                    // recurse(node[i].Chields)
                    var _id = node[i].No + '_ch';
                    $('#' + node[i].No).append('<div class="dataNestedTreeList" id="' + _id + '">'

                        + '</div>');
                    recurse(node[i].Chields, _id)
                } else {
                    //$('#' + node[i].No).find('.titleBlock').append('hello');
                    $('#' + node[i].No).find('.actionBlock').prepend('<a href="javascript:void(0);" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#costLineItemModal">Add Cost Line</a>')
                    $('#' + node[i].No).find('.titleBlock').append('<div class="spaceLeft table-responsive tableLayoutWrap altboarder w-100 mt-4">'
                        + '<table class="table mb-0" >'
                        + '<thead>'
                        + '<tr class="HeaderStyle">'
                        + '<th>Unit</th>'
                        + '<th>Daily Output</th>'
                        + '<th>Crew Structure</th>'
                        + '<th>Material Cost</th>'
                        + '<th>Cost of Line Item</th>'
                        + '<th>Is Percent</th>'
                        + '</tr>'
                        + '</thead>'
                        + '<tbody>'
                        + '<tr>'
                        + '<td>' + node[i].Unit+'</td>'
                        + '<td>' + node[i].DailyOutput +'</td>'
                        + '<td>' + node[i].CrewCode +'</td>'
                        + '<td>' + node[i].MaterialCost +'</td>'
                        + '<td>' + node[i].Cost +'</td>'
                        + '<td>' + node[i].IsPercent +'</td>'
                        + '</tr>'
                        + '</tbody>'
                        + '</table>'
                        + '</div>');
                }
            }


        }

        function NodeClicked(evt) {


            var treeViewData = window["<%=tvLevels.ClientID%>" + "_Data"];
            var tree = document.getElementById("<%= tvLevels.ClientID %>");
            if (treeViewData.selectedNodeID.value != "") {
                var selectedNode = document.getElementById(treeViewData.selectedNodeID.value);
                //                var value = selectedNode.href.substring(selectedNode.href.indexOf(",") + 3, selectedNode.href.length - 2);

                //                var text = selectedNode.innerHTML;
                //                alert("Text: " + text + "\r\n" + "Value: " + value);

                if (selectedNode) {
                    var nodes = [{
                        "id": 13,
                        "text": "Raspberry"
                    }, {
                        "id": 14,
                        "text": "Cantaloupe"
                    }];
                    $('#div1').tvlevels('append', {
                        parent: node.target,
                        data: nodes
                    });
                }
            } else {
                alert("No node selected.")
            }
            return false;
        }

        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorSelectNode") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteChild") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteSuccess") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorProdNotFound") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "HierarchyAlreadyExist") %>';
            }
            else if (msgno == 6) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorAddHierarchy") %>';
            }
            else if (msgno == 7) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "HierarchyAdded") %>';
            }
            else if (msgno == 8) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorCantAdd") %>';
            }
            else if (msgno == 9) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NonLeafNode") %>';
            }
            else if (msgno == 10) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "HasChildren") %>';
            }
            else if (msgno == 11) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NoRecFound") %>';
            }
            else if (msgno == 12) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ValueMissing") %>';
            }
            else if (msgno == 13) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NotExcel") %>';
            }
            else if (msgno == 14) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Imported") %>';
            }
            else if (msgno == 15) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ImportFailure") %>';
            }
            else if (msgno == 16) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NumberAlreadyExist") %>';
            }
            else if (msgno == 17) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NumberMissing") %>';
            }
            else if (msgno == 18) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "EngNameMissing") %>';
            }
            else if (msgno == 19) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "AraNameMissing") %>';
            }
            else if (msgno == 20) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "AllowedRange") %>';
            }
            else if (msgno == 22) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NoCrewStructure") %>';
            }
            else if (msgno == 23) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "CostLineUpdated") %>';
            }
            else if (msgno == 24) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SelectLeafNodeToUpdate") %>';
            }
            else if (msgno == 25) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "MissingCost") %>';
            }
            else if (msgno == 26) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NumericNodeNo") %>';
            }
            else if (msgno == 27) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NodeNameStartWith") %>';
            }
            alert(msgstring);
        }

        function ShowConfirm() {
            var strConfirm = '<%=GetGlobalResourceObject("Resource", "ConfirmDelete") %>';
            if (confirm(strConfirm) == true) {
                DeleteProductListConfirmation()
                return true;
            }
            else {
                return false;
            }
        }

        function DeleteProductListConfirmation() {
            var strConfirm = '<%=GetGlobalResourceObject("Resource", "ProdDeleteConfirm") %>';
            if (confirm(strConfirm) == true) {
                document.getElementById("<%= hdnResult.ClientID %>").value = "1";
            }
            else {
                document.getElementById("<%= hdnResult.ClientID %>").value = "0";
            }
        }

        function ShowConfirmDelete() {
            var strConfirm = '<%=GetGlobalResourceObject("Resource", "ConfirmDelete") %>';
            if (confirm(strConfirm) == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function Showopoup(url) {

            params = 'width=' + screen.width;
            params += ', height=' + screen.height;
            params += ', top=0, left=0'
            params += ', fullscreen=yes';
            params += ', menubar=no, status=no, scrollbars=yes,titlebar=no, toolbar=no,directories=no';
            //var win = window.open('', 'Levels', 'left=0,top=0px,menubar=no, status=no, scrollbars=yes,titlebar=no, toolbar=no,directories=no,width,height');
            var win = window.open('', 'Levels', params);
            win.location.href = url
            win = null;
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="lblHeader" runat="server" Text='<%$ Resources:Resource, CostLineItems %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <script type="text/javascript">
        var xPos, yPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xPos = $get('divTreeViewScrollTo').scrollLeft;
            yPos = $get('divTreeViewScrollTo').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('divTreeViewScrollTo').scrollLeft = xPos;
            $get('divTreeViewScrollTo').scrollTop = yPos;
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" CssClass="levelEntryWrapper">
        <ContentTemplate>

            <div class="levelEntryWrapper__header">
                <div class="row justify-content-between">
                    <div class="col-12 col-lg-4">
                        <div class="form-group mb-0">
                            <asp:FileUpload ID="uplImage" runat="server" CssClass="btn btn-primary button-block" Text='<%$ Resources:Resource, Browse %>' />
                        </div>
                    </div>
                    <div class="col-12 col-lg-auto">
                        <div class="text-right mb-0 buttonActionGroup">
                            <div class="btn-group commanButtonGroup mb-0">
                                <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-primary btn-min-120" Text='<%$ Resources:Resource, ImportFromExcel %>'></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton1" runat="server" Text='<%$ Resources:Resource, AddDiv %>' CssClass="btn btn-primary btn-min-120" data-toggle="modal" data-target="#addDivisionModal"></asp:LinkButton>
                                <asp:Button ID="btnDeleteAll" runat="server" Text="DELETE ALL" CssClass="btn btn-danger btn-min-120" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--<div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-xl-3">
                    <div class="form-group">
                        <asp:Label ID="Label17" runat="server" SkinID="Controllabel" AssociatedControlID="txtUnit" Text='<%$ Resources:Resource, Unit %>' CssClass="control-label"></asp:Label>
                        <asp:TextBox ID="txtUnit" runat="server" CssClass="smalltextbox" MaxLength="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvUnit" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorUnit %>' ControlToValidate="txtUnit" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-xl-3">
                    <div class="form-group">
                        <asp:Label ID="Label19" runat="server" SkinID="Controllabel" AssociatedControlID="txtDO" Text='<%$ Resources:Resource, DO %>' CssClass="control-label"></asp:Label>
                        <asp:TextBox ID="txtDO" runat="server" CssClass="smalltextbox" MaxLength="7"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDO" runat="server" ErrorMessage='<%$ Resources:Resource, MissingDailyOutput %>' ControlToValidate="txtDO" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="reDO" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>' SetFocusOnError="true" ControlToValidate="txtDO" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$" Display="None" ValidationGroup="Save"> </asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-xl-3">
                    <div class="form-group">
                        <asp:Label ID="Label16" SkinID="Controllabel" AssociatedControlID="ddlCrew" runat="server" Text='<%$ Resources:Resource, CrewStructure %>' CssClass="control-label"></asp:Label>
                        <asp:DropDownList ID="ddlCrew" runat="server" CssClass="smalltextbox" DataTextField="ProductID" DataValueField="ProductID" AutoPostBack="true"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-xl-3">
                    <div class="form-group">
                        <asp:Label ID="lblCost" SkinID="Controllabel" AssociatedControlID="txtMatCost" runat="server" CssClass="control-label" Text='<%$ Resources:Resource, MaterialCost %>'></asp:Label>
                        <asp:RegularExpressionValidator ID="reCost" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>' SetFocusOnError="true" ControlToValidate="txtCost" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$" Display="None" ValidationGroup="Save"> </asp:RegularExpressionValidator>
                        <asp:TextBox ID="txtMatCost" runat="server" CssClass="smalltextbox" MaxLength="12"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="reMatCost" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>' SetFocusOnError="true" ControlToValidate="txtMatCost" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$" Display="None" ValidationGroup="Save"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-xl-3">
                    <div class="form-group">
                        <asp:Label ID="Label18" SkinID="Controllabel" AssociatedControlID="txtCost" runat="server" Text='<%$ Resources:Resource, CostOfLineItem %>' CssClass="control-label"></asp:Label>
                        <asp:TextBox ID="txtCost" runat="server" CssClass="smalltextbox" MaxLength="12"></asp:TextBox>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-xl-3">
                    <div class="form-group pt-md-4">
                        <asp:CheckBox ID="chkPercent" runat="server" CssClass="control-label d-inline-block mt-2" Text='<%$ Resources:Resource, IsPercent %>' Checked="false" />
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-xl-6">
                    <div class="form-group text-right pt-md-4">
                        <asp:LinkButton ID="lnkSet" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="btn btn-primary" ValidationGroup="Save"></asp:LinkButton>
                    </div>
                </div>
            </div>--%>

            <div class="row">
                <div class="col-12">
                    <div class="treeViewWrapper">
                        <div class="row no-gutters">
                            <div class="col-12 col-lg-auto">
                                <div class="treeMainList">
                                    <ul id="treeMainList"></ul>
                                </div>
                            </div>
                            <div class="col-12 col-lg">
                                <div class="dataTreeWrap" id="dataTreeWrap">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row d-none">
                <div class="col-12 col-lg-6">
                    <div id="divTreeViewScrollTo" style="overflow: auto; max-height: 750px;">
                        <asp:TreeView ID="tvLevels" runat="server" Width="100%" LineImagesFolder="~/TreeLineImages"
                            Style="margin-top: 7PX;" Visible="false" ShowLines="True" NodeWrap="true">
                        </asp:TreeView>
                    </div>
                </div>
                <div class="col-12 col-lg-6" id="div2" runat="server">
                    <asp:Panel ID="Panel1" runat="server" CssClass="">
                        <asp:Label ID="lblDivision" runat="server" Text='<%$ Resources:Resource, Div %>'
                            CssClass="label1 left" Width="50%"></asp:Label>
                        <asp:Label ID="Label8" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                        <asp:TextBox ID="txtNo1" runat="server" Width="15%" CssClass="left"
                            ReadOnly="true" MaxLength="15" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel1E" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel1A" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:Button ID="Button0" runat="server" Text="E" CssClass="small button large-1 left" />
                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, Sec1 %>' Width="50%"
                            CssClass="label1 left"></asp:Label>
                        <asp:Label ID="Label9" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                        <asp:TextBox ID="txtNo2" runat="server" Width="15%" CssClass="left"
                            ReadOnly="true" MaxLength="15" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel2E" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel2A" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:Button ID="Button1" runat="server" Text="E" CssClass="small button large-1 left" />
                        <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, Sec2 %>' Width="50%"
                            CssClass="label1 left"></asp:Label>
                        <asp:Label ID="Label10" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                        <asp:TextBox ID="txtNo3" runat="server" Width="15%" CssClass="left"
                            ReadOnly="true" MaxLength="15" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel3E" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel3A" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:Button ID="Button2" runat="server" Text="E" CssClass="small button large-1 left" />
                        <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, Sec3 %>' Width="50%"
                            CssClass="label1 left"></asp:Label>
                        <asp:Label ID="Label11" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                        <asp:TextBox ID="txtNo4" runat="server" Width="15%" CssClass="left"
                            ReadOnly="true" MaxLength="15" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel4E" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel4A" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:Button ID="Button3" runat="server" Text="E" CssClass="small button large-1 left" />
                        <asp:Label ID="Label4" runat="server" Text='<%$ Resources:Resource, Sec4 %>' Width="50%"
                            CssClass="label1 left"></asp:Label>
                        <asp:Label ID="Label12" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                        <asp:TextBox ID="txtNo5" runat="server" Width="15%" CssClass="left"
                            ReadOnly="true" MaxLength="15" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel5E" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel5A" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:Button ID="Button4" runat="server" Text="E" CssClass="small button large-1 left" />
                        <asp:Label ID="Label5" runat="server" Text='<%$ Resources:Resource, Sec5 %>' Width="50%"
                            CssClass="label1 left"></asp:Label>
                        <asp:Label ID="Label13" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                        <asp:TextBox ID="txtNo6" runat="server" Width="15%" CssClass="left"
                            ReadOnly="true" MaxLength="15" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel6E" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel6A" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:Button ID="Button5" runat="server" Text="E" CssClass="small button large-1 left" />
                        <asp:Label ID="Label6" runat="server" Text='<%$ Resources:Resource, Sec6 %>' Width="50%"
                            CssClass="label1 left"></asp:Label>
                        <asp:Label ID="Label14" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                        <asp:TextBox ID="txtNo7" runat="server" Width="15%" CssClass="left"
                            ReadOnly="true" MaxLength="15" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel7E" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel7A" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:Button ID="Button6" runat="server" Text="E" CssClass="small button large-1 left" />
                        <asp:Label ID="Label7" runat="server" Text='<%$ Resources:Resource, Sec7 %>' Width="50%"
                            CssClass="label1 left"></asp:Label>
                        <asp:Label ID="Label15" runat="server" Text='<%$ Resources:Resource, Arabic %>' CssClass="label1 left"
                            Width="50%"></asp:Label>
                        <asp:TextBox ID="txtNo8" runat="server" Width="15%" CssClass="left"
                            ReadOnly="true" MaxLength="15" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel8E" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:TextBox ID="txtLevel8A" runat="server" Width="35%" CssClass="left" MaxLength="499"
                            ReadOnly="true" SkinID="txtMinwidth"></asp:TextBox>
                        <asp:Button ID="Button7" runat="server" Text="E" CssClass="small button large-1  left" />
                        <div class="clear">
                        </div>
                        <div class="row gutters-5 mt-2">
                            <div class="col">
                                <asp:Button ID="Button9" runat="server" Text='<%$ Resources:Resource, Add %>' CssClass="btn btn-primary btn-block mb-3" />
                            </div>
                            <div class="col">
                                <asp:Button ID="Button10" runat="server" Text='<%$ Resources:Resource, Delete %>' CssClass="btn btn-danger btn-block mb-3" />
                            </div>
                            <div class="col-12">
                                <asp:Button ID="Button11" runat="server" Text='<%$ Resources:Resource, Nonleaf %>' CssClass="btn btn-success btn-block mb-3" />
                            </div>
                            <div class="col-12">
                                <asp:Button ID="Button12" runat="server" Text='<%$ Resources:Resource, Nonlinked %>' CssClass="btn btn-success btn-block mb-3" />
                            </div>
                            <%--<div class="col-12">
                                <asp:Button ID="btnDeleteAll" runat="server" Text="DELETE ALL" CssClass="btn btn-danger btn-block mb-3" />
                            </div>--%>
                            <div class="col-12">
                                <asp:Button ID="Button15" runat="server" Text='<%$ Resources:Resource, AddProduct %>' CssClass="btn btn-success btn-block mb-3" />
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Button ID="Button8" runat="server" Text="" />
                </div>
            </div>


            <!-- Add & Update Cost line Item Modal -->
            <div class="modal fade" id="costLineItemModal" tabindex="-1" aria-labelledby="costLineItemModalLable" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="costLineItemModalLable">Add Cost Line</h5>
                            <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </a>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Label17" runat="server" SkinID="Controllabel" AssociatedControlID="txtUnit" Text='<%$ Resources:Resource, Unit %>' CssClass="control-label"></asp:Label>
                                        <asp:TextBox ID="txtUnit" runat="server" CssClass="smalltextbox" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvUnit" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorUnit %>' ControlToValidate="txtUnit" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Label19" runat="server" SkinID="Controllabel" AssociatedControlID="txtDO" Text='<%$ Resources:Resource, DO %>' CssClass="control-label"></asp:Label>
                                        <asp:TextBox ID="txtDO" runat="server" CssClass="smalltextbox" MaxLength="7"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvDO" runat="server" ErrorMessage='<%$ Resources:Resource, MissingDailyOutput %>' ControlToValidate="txtDO" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="reDO" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>' SetFocusOnError="true" ControlToValidate="txtDO" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$" Display="None" ValidationGroup="Save"> </asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Label16" SkinID="Controllabel" AssociatedControlID="ddlCrew" runat="server" Text='<%$ Resources:Resource, CrewStructure %>' CssClass="control-label"></asp:Label>
                                        <asp:DropDownList ID="ddlCrew" runat="server" CssClass="smalltextbox" DataTextField="ProductID" DataValueField="ProductID" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblCost" SkinID="Controllabel" AssociatedControlID="txtMatCost" runat="server" CssClass="control-label" Text='<%$ Resources:Resource, MaterialCost %>'></asp:Label>
                                        <asp:RegularExpressionValidator ID="reCost" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>' SetFocusOnError="true" ControlToValidate="txtCost" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$" Display="None" ValidationGroup="Save"> </asp:RegularExpressionValidator>
                                        <asp:TextBox ID="txtMatCost" runat="server" CssClass="smalltextbox" MaxLength="12"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="reMatCost" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>' SetFocusOnError="true" ControlToValidate="txtMatCost" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$" Display="None" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Label18" SkinID="Controllabel" AssociatedControlID="txtCost" runat="server" Text='<%$ Resources:Resource, CostOfLineItem %>' CssClass="control-label"></asp:Label>
                                        <asp:TextBox ID="txtCost" runat="server" CssClass="smalltextbox" MaxLength="12"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group pt-md-4">
                                        <asp:CheckBox ID="chkPercent" runat="server" CssClass="control-label d-inline-block mt-2" Text='<%$ Resources:Resource, IsPercent %>' Checked="false" />
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-end pt-4">
                                <div class="col-auto">
                                    <div class="form-group">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                         </div>
                                </div>
                                <div class="col-auto">
                                    <div class="form-group">
                                        <asp:LinkButton ID="lnkSet" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="btn btn-primary" ValidationGroup="Save"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%-- Add New Node Modal --%>
            <div class="modal fade" id="addDivisionModal" tabindex="-1" aria-labelledby="addDivisionModalLable" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addDivisionModalLable">Add Division</h5>
                            <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </a>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">Section Id</label>
                                <input type="text" value="" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Name</label>
                                <input type="text" value="" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Arabic Name</label>
                                <input type="text" value="" class="form-control" />
                            </div>
                            <div class="row justify-content-end pt-4">
                                <div class="col-auto">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                </div>
                                <div class="col-auto">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <%-- <Triggers>
            <asp:PostBackTrigger ControlID="LinkButton2"></asp:PostBackTrigger>
        </Triggers>--%>
    </asp:UpdatePanel>

    <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
        ShowMessageBox="true" ValidationGroup="Save" DisplayMode="BulletList" />
    <asp:HiddenField ID="hdnResult" runat="server" Value="-1" />
    <asp:HiddenField ID="hdnInitialLoad" runat="server" Value="1" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MiddlePart">
    <%--<div class="row">
        <div class="large-12 medium-12 columns" id="div3" runat="server">
            <asp:FileUpload ID="uplImage" runat="server" CssClass="small button left " Text='<%$ Resources:Resource, Browse %>'
                Width="400px" />
            <asp:LinkButton ID="LinkButton2" runat="server" CssClass="BTNImport" Text='<%$ Resources:Resource, ImportFromExcel %>'></asp:LinkButton>
            <asp:LinkButton ID="LinkButton1" runat="server" Text='<%$ Resources:Resource, AddDiv %>'
                CssClass="BTNAdd rightbtn"></asp:LinkButton>
        </div>
    </div>--%>
    <script>
        $(document).ready(function () {
            $("#sidr").show();
            $('.mainCardWrapperBody').addClass('p-0');
            getItems();

            //$('#treeMainList li a').click(function () {
            //    $('#treeMainList li a').removeClass('active');
            //    setTimeout(function () {
            //        $(this).addClass('active');
            //    }, 1000);
            //});
        });

    </script>
</asp:Content>



