﻿Public Class GridView1
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                Menuhide.Value = "close"

                grdProducts.Columns(1).Visible = True
                grdProducts.Columns(2).Visible = True
                'grdProducts.Columns(11).Visible = True

                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    grdProducts.Columns(1).Visible = False
                Else
                    grdProducts.Columns(2).Visible = False
                End If

                LoadDataGrid_Products()
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
    End Sub
    Private Sub LoadDataGrid_Products()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        
        'dt = objSqlConfig.GetProductBySupplierID(Session("UserID").ToString(), txtSearch.Text)
        dt = objSqlConfig.Get_Supplier_Quotes(Session("UserName").ToString(), txtSearch.Text)
        If (dt.Rows.Count > 0) Then
            grdProducts.DataSource = dt
            grdProducts.DataBind()
            Session("ProductList") = dt
        Else
            grdProducts.DataSource = Nothing
            grdProducts.DataBind()
            Session("ProductList") = Nothing
        End If
    End Sub
    Private Sub FillGrid_Products()
        Dim Data As DataTable = Session("ProductList")
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp") Is Nothing) Then
                If ViewState("SortDirection").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp") & " " & "desc"
                End If

                grdProducts.DataSource = dv
                grdProducts.DataBind()
            End If
        End If
    End Sub
    Protected Sub grdProducts_DataBound(sender As Object, e As EventArgs) Handles grdProducts.DataBound
        For Each row As GridViewRow In grdProducts.Rows
            If (row.Cells(2).Text.Equals("&nbsp;") Or row.Cells(2).Text.Equals("&amp;nbsp;") Or row.Cells(2).Text.Equals("")) Then
                row.Cells(2).Text = String.Empty
            End If
        Next
    End Sub
    Protected Sub grdProducts_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdProducts.SelectedIndexChanged
        'Response.Redirect("Supplier_Products.aspx?ID=" + grdProducts.SelectedDataKey("ID").ToString())
        Dim strNodeNo As String = grdProducts.Rows(grdProducts.SelectedIndex).Cells(0).Text
        Dim strNodeName As String = grdProducts.Rows(grdProducts.SelectedIndex).Cells(1).Text
        Session("MapID") = grdProducts.Rows(grdProducts.SelectedIndex).Cells(0).Text
        Response.Redirect("Supplier_quote.aspx?ID=" + grdProducts.SelectedDataKey("ID").ToString() + "&NodeNo=" + strNodeNo + "&NodeName=" + strNodeName)
    End Sub
    Protected Sub grdProducts_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdProducts.Sorting
        If (ViewState("SortState") IsNot Nothing And ViewState("SortState") = SortDirection.Ascending) Then
            ViewState("SortDirection") = "asc"
            ViewState("SortState") = 1
        Else
            ViewState("SortDirection") = "desc"
            ViewState("SortState") = 0
        End If
        ViewState("SortExp") = e.SortExpression
        FillGrid_Products()
    End Sub
    Protected Sub grdProducts_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdProducts.PageIndexChanging
        grdProducts.PageIndex = e.NewPageIndex
        grdProducts.DataSource = Session("ProductList")
        grdProducts.DataBind()
    End Sub
    Protected Sub grdProducts_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdProducts.RowDeleting
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim retVal As Integer = objSqlConfig.Delete_Supplier_Quotation(grdProducts.DataKeys(e.RowIndex).Value.ToString())
        If (retVal = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('8');", True)
            LoadDataGrid_Products()
        End If
    End Sub
    Protected Sub grdProducts_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdProducts.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            For Each link As ImageButton In e.Row.Cells(9).Controls.OfType(Of ImageButton)()
                If link.CommandName = "Delete" Then
                    link.Attributes("onclick") = "if (ShowConfirm() == false) { return false; };"
                End If
            Next
        End If
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        LoadDataGrid_Products()
    End Sub
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("CategoriesTree.aspx")
    End Sub
End Class