﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" CodeBehind="ClientType.aspx.vb"
    Inherits="CEA.ClientType" MasterPageFile="~/Master/PopUpMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">

//        function GotoRegister() { window.location = 'UserProfileNew.aspx'; return false; }




    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, ClientType %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div class="row">
                <div class="large-12 medium-12 column">
                    <div class="large-4 medium-4 columns bold">
                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, SelectClientType %>'>
                        </asp:Label><asp:DropDownList ID="ddlClientType" runat="server" Width="300px">
                        </asp:DropDownList>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="large-12 medium-12 column">
                    <div class="outerDivBtn">
                        <div class="BTNdiv">
                            <asp:Button ID="Button8" runat="server" Text='<%$ Resources:Resource, Proceed %>' CssClass="BTNSave" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
