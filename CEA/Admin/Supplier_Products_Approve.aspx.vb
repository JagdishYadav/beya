﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Public Class FORM3
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (Session("User_Role_ID").ToString().Contains("999")) Then
                    If (menuItems.Count > 15) Then
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                    End If
                ElseIf (Session("User_Role_ID").ToString().Contains("777")) Then
                    If (menuItems.Count > 14) Then
                        For i As Integer = 0 To 13
                            menuItems.RemoveAt(0)
                        Next
                    End If
                End If
                txtOther.Attributes.Add("style", "display:none")
                chkList.Attributes.Add("onclick", "checkBoxListOnClick(this);")
                txtOther1.Attributes.Add("style", "display:none")
                chkSpec.Attributes.Add("onclick", "checkBoxListOnClick1(this);")
                Dim objSqlConfig As New ConfigClassSqlServer()
                chkList.DataSource = objSqlConfig.GetApproved_Agencies()
                chkList.DataValueField = "ID"
                chkSpec.DataSource = objSqlConfig.GetProduct_Specification()
                chkSpec.DataValueField = "ID"

                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    chkList.DataTextField = "NameA"
                    chkList.DataBind()
                    chkSpec.DataTextField = "NameA"
                    chkSpec.DataBind()
                Else
                    chkList.DataTextField = "NameE"
                    chkList.DataBind()
                    chkSpec.DataTextField = "NameE"
                    chkSpec.DataBind()
                End If

                FillProductData(Request.QueryString.Get("ID").ToString())
                txtProdID.ReadOnly = True
                DisableControls()
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
    End Sub
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        FillProductData(Request.QueryString.Get("ID").ToString())
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim imgFileName As String = ""
        Dim docFileName As String = ""
        Dim dt As New DataTable()

        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "UPDATE ProductDetails SET Status = '" + RadioButtonList1.SelectedValue + "', Comments = '" + txtComments.Text.Trim() + "', ManHour = '" + txtManHour.Text.Trim() + "', EquipHour = '" + txtEquipHour.Text.Trim() + "', MapID = (SELECT TempMapID FROM ProductDetails WHERE ID = '" + txtProdID.Text.Trim() + "') WHERE ID = '" + txtProdID.Text.Trim() + "' "
        sqlCommand.ExecuteNonQuery()
        If (RadioButtonList1.SelectedValue.ToString().Equals("1")) Then
            Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
            sqlCommand.CommandText = "SELECT ID, NameE, NameA FROM Suppliers WHERE UserID = '" + ViewState("SupplierID") + "' "
            sqlDataAdapter.Fill(dt)

            If (dt.Rows.Count > 0) Then
                sqlCommand.CommandText = "INSERT INTO AssociatedSuppliers(ProductID, SupplierID, NameE, NameA, MinPrice, MaxPrice) VALUES('" + txtProdID.Text.Trim() + "', '" + dt.Rows(0).Item(0).ToString() + "', '" + dt.Rows(0).Item(1).ToString() + "', '" + dt.Rows(0).Item(2).ToString() + "', 0, 0)"
                sqlCommand.ExecuteNonQuery()
            End If
        End If

        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('6');", True)

        sqlConnection.Close()
    End Sub
    Private Sub FillProductData(strID As String)
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        dt = objSqlConfig.GetProductByID(strID)
        If (dt.Rows.Count > 0) Then
            txtProdID.Text = dt.Rows(0).Item(0).ToString()
            txtNameE.Text = dt.Rows(0).Item(2).ToString()
            txtNameA.Text = dt.Rows(0).Item(3).ToString()
            txtFeatE.Text = dt.Rows(0).Item(4).ToString()
            txtFeatA.Text = dt.Rows(0).Item(5).ToString()
            txtDescE.Text = dt.Rows(0).Item(6).ToString()
            txtDescA.Text = dt.Rows(0).Item(7).ToString()
            txtUnit.Text = dt.Rows(0).Item(10).ToString()
            txtManHour.Text = dt.Rows(0).Item(11).ToString()
            txtEquipHour.Text = dt.Rows(0).Item(12).ToString()
            txtOther.Text = dt.Rows(0).Item(14).ToString()
            txtOther1.Text = dt.Rows(0).Item(19).ToString()
            RadioButtonList1.Items.FindByValue(dt.Rows(0).Item(16).ToString()).Selected = True
            txtComments.Text = dt.Rows(0).Item(17).ToString()
            Image1.ImageUrl = ResolveClientUrl("~/Upload/Images/" + dt.Rows(0).Item(8).ToString())
            ViewState("SupplierID") = dt.Rows(0).Item(15).ToString()

            Dim arrValues As String()
            arrValues = dt.Rows(0).Item(13).ToString().Split(",")

            If (arrValues.Length > 0 And arrValues(0).Length > 0) Then
                For i As Integer = 0 To arrValues.Length - 1
                    Dim listItem As ListItem = chkList.Items.FindByValue(arrValues(i))
                    If (listItem IsNot Nothing) Then
                        chkList.Items.FindByValue(arrValues(i)).Selected = True
                    Else
                        chkList.Items(chkList.Items.Count - 1).Selected = True
                        'txtOther.Text = arrValues(i)
                    End If
                Next
            End If
            If (chkList.Items(chkList.Items.Count - 1).Selected) Then
                txtOther.Visible = True
                txtOther.Attributes.Add("style", "display:''")
            End If

            arrValues = dt.Rows(0).Item(18).ToString().Split(",")
            If (arrValues.Length > 0 And arrValues(0).Length > 0) Then
                For i As Integer = 0 To arrValues.Length - 1
                    Dim listItem As ListItem = chkSpec.Items.FindByValue(arrValues(i))
                    If (listItem IsNot Nothing) Then
                        chkSpec.Items.FindByValue(arrValues(i)).Selected = True
                    Else
                        chkSpec.Items(chkSpec.Items.Count - 1).Selected = True
                        'txtOther1.Text = arrValues(i)
                    End If
                Next
            End If
            If (chkSpec.Items(chkSpec.Items.Count - 1).Selected) Then
                txtOther1.Visible = True
                txtOther1.Attributes.Add("style", "display:''")
            End If

            RadioButtonList1.SelectedIndex = Integer.Parse(dt.Rows(0).Item(16).ToString())
            txtComments.Text = dt.Rows(0).Item(17).ToString()
        End If
    End Sub
    Private Sub DisableControls()
        txtProdID.ReadOnly = True
        txtNameE.ReadOnly = True
        txtNameA.ReadOnly = True
        txtDescE.ReadOnly = True
        txtDescA.ReadOnly = True
        txtFeatE.ReadOnly = True
        txtFeatA.ReadOnly = True
        uplImage.Enabled = False
        uplFile.Enabled = False
        txtUnit.ReadOnly = True
        'txtManHour.ReadOnly = True
        'txtEquipHour.ReadOnly = True
        chkList.Enabled = False
        chkSpec.Enabled = False
        RadioButtonList1.Enabled = True
        txtComments.ReadOnly = False
        txtOther.ReadOnly = True
        txtOther1.ReadOnly = True
    End Sub
End Class