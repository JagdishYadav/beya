﻿Public Class Suppliers_List
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (menuItems.Count > 15) Then
                    menuItems.RemoveAt(12)
                    menuItems.RemoveAt(12)
                    menuItems.RemoveAt(12)
                    menuItems.RemoveAt(12)
                End If
                grdSupplier.Columns(10).Visible = True
                grdSupplier.Columns(11).Visible = True
                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    grdSupplier.Columns(0).Visible = False
                    'header1.Style.Add("float", "right")
                    'header2.Style.Add("float", "right")
                    grdSupplier.Columns(11).Visible = False
                Else
                    grdSupplier.Columns(10).Visible = False
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
        LoadDataGrid()
        Panel1.Width = grdSupplier.Width
    End Sub
    Private Sub LoadDataGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        dt = objSqlConfig.GetSuppliers(txtSearch.Text)
        If (dt.Rows.Count > 0) Then
            grdSupplier.DataSource = dt
            grdSupplier.DataBind()
        Else
            grdSupplier.DataSource = Nothing
            grdSupplier.DataBind()
        End If
    End Sub
    Protected Sub grdSupplier_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdSupplier.Sorting
        If (ViewState("SortState") IsNot Nothing And ViewState("SortState") = SortDirection.Ascending) Then
            ViewState("SortDirection") = "asc"
            ViewState("SortState") = 1
        Else
            ViewState("SortDirection") = "desc"
            ViewState("SortState") = 0
        End If
        ViewState("SortExp") = e.SortExpression
        FillGrid()
    End Sub
    Protected Sub grdSupplier_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdSupplier.PageIndexChanging
        grdSupplier.PageIndex = e.NewPageIndex
        grdSupplier.DataBind()
    End Sub
    Private Sub FillGrid()
        Dim Data As DataTable = grdSupplier.DataSource
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp") Is Nothing) Then
                If ViewState("SortDirection").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp") & " " & "desc"
                End If

                grdSupplier.DataSource = dv
                grdSupplier.DataBind()
            End If
        End If
    End Sub
    Protected Sub grdSupplier_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdSupplier.RowEditing
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As DataTable = objSqlConfig.GetSupplierByID(grdSupplier.DataKeys(e.NewEditIndex).Values(0).ToString())
        If (dt.Rows.Count > 0) Then
            Response.Redirect("Supplier_New.aspx?ID=" + dt.Rows(0).Item(0).ToString())
        End If
    End Sub
    Protected Sub grdSupplier_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdSupplier.RowDeleting
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim retVal As Integer = objSqlConfig.DeleteSupplier(grdSupplier.DataKeys(e.RowIndex).Values(1).ToString())
        If (retVal = 0) Then
            objSqlConfig.DeleteSupplierUser(grdSupplier.DataKeys(e.RowIndex).Values(0).ToString())
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
            LoadDataGrid()
        End If
    End Sub
    Protected Sub grdSupplier_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdSupplier.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            For Each link As ImageButton In e.Row.Cells(13).Controls.OfType(Of ImageButton)()
                If link.CommandName = "Delete" Then
                    link.Attributes("onclick") = "if (ShowConfirm() == false) { return false; };"
                End If
            Next
        End If
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        LoadDataGrid()
    End Sub
End Class