﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Productivity_New.aspx.vb" Inherits="CEA.Productivity_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">

        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DataSaved") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "LabourAlreadyExist") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "EquipAlreadyExist") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProductivityAlreadyExist") %>';
            }
            else if (msgno == 9) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "CrewMissing") %>';
            }
            alert(msgstring);
        }

    </script>
    <style>
        input[type="text"], input[type="password"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="month"], input[type="week"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="time"], input[type="url"], textarea, select
        {
            min-height: 1.4125rem;
            height: 1.4125rem;
            margin: 0 0 0.7rem 0;
        }
        .panelDiv
        {
            min-height: 64px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, CreateProductivity %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="large-6 medium-6 columns"> 
                    <div class="panelDiv" Style="display: none">
                        <div class="row" >
                            <div class="large-6 medium-6 columns bold">
                                <asp:Label ID="Label17" runat="server" Text='<%$ Resources:Resource, HourlyOutput %>'></asp:Label>
                            </div>
                            <div class="large-6 medium-6 columns">
                                <asp:Label ID="lblHO" runat="server" Text="0" Font-Bold="True"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-6 medium-6 columns bold">
                                <asp:Label ID="Label18" runat="server" Text='<%$ Resources:Resource, ManHour %>'></asp:Label>
                            </div>
                            <div class="large-6 medium-6 columns">
                                <asp:Label ID="lblMO" runat="server" Text="0" Font-Bold="True" ></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-6 medium-6 columns bold">
                                <asp:Label ID="Label19" runat="server" Text='<%$ Resources:Resource, EquipHour %>'></asp:Label>
                            </div>
                            <div class="large-6 medium-6 columns">
                                <asp:Label ID="lblEH" runat="server" Text="0" Font-Bold="True"></asp:Label>
                            </div>
                        </div>
                    </div>
                    &nbsp;
                </div>
                
            </div>
            <asp:Panel ID="Panel2" runat="server" CssClass="panelDiv">
                <div class="row">
                    <div >
                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, Nat %>'></asp:Label>
                        <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                    <div >
                        <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, DailyOutput %>'></asp:Label>
                        <asp:TextBox ID="txtDO" runat="server" MaxLength="10" AutoPostBack="True" Enabled = "false" Text = "0"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="frvDO" runat="server" ErrorMessage='<%$ Resources:Resource, MissingDailyOutput %>'
                            ControlToValidate="txtDO" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="reDo" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                            SetFocusOnError="true" ControlToValidate="txtDO" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                            Display="None" ValidationGroup="Save">
                        </asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="row">
                    <div >
                        <asp:Label ID="Label8" runat="server" Text='<%$ Resources:Resource, Code %>'></asp:Label>
                        <asp:TextBox ID="txtCode" runat="server" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage='<%$ Resources:Resource, MissingCode %>'
                            ControlToValidate="txtCode" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                    </div>
                    <div >
                        <asp:Label ID="Label9" runat="server" Text='<%$ Resources:Resource, Unit %>'></asp:Label>
                        <asp:TextBox ID="txtUnit" runat="server" MaxLength="10" Enabled = "false"></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="rfvUnit" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorUnit %>'
                            ControlToValidate="txtUnit" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>--%>
                    </div>
                </div>
                <div class="row">
                    <div >
                        <asp:Label ID="Label10" runat="server" Text='<%$ Resources:Resource, NameE %>'></asp:Label>
                        <asp:TextBox ID="txtNameE" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNameE" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNameE %>'
                            ControlToValidate="txtNameE" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                    </div>
                    <div >
                        <asp:Label ID="Label11" runat="server" Text='<%$ Resources:Resource, NameA %>'></asp:Label>
                        <asp:TextBox ID="txtNameA" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNameA" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNameA %>'
                            ControlToValidate="txtNameE" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div >
                        <asp:Label ID="Label12" runat="server" Text='<%$ Resources:Resource, DescriptionE %>'></asp:Label>
                        <asp:TextBox ID="txtDescE" runat="server" TextMode="MultiLine" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDescE" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorDescE %>'
                            ControlToValidate="txtDescE" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                    </div>
                    <div >
                        <asp:Label ID="Label13" runat="server" Text='<%$ Resources:Resource, DescriptionA %>'></asp:Label>
                        <asp:TextBox ID="txtDescA" runat="server" TextMode="MultiLine" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDescA" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorDescA %>'
                            ControlToValidate="txtDescA" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 medium-6 columns">
                        <asp:Label ID="Label14" runat="server" Text='<%$ Resources:Resource, LaboursList %>'
                            Font-Bold="true"></asp:Label>
                        <br />
                        <asp:GridView ID="grdLabour" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                            EmptyDataText="No Data" Width="100%" PageSize="10" DataKeyNames="Code" ShowFooter="True"
                            HeaderStyle-Height="30px" RowStyle-Height="25px" FooterStyle-BackColor="ActiveBorder"
                            FooterStyle-Height="25px" AllowSorting="false" HeaderStyle-HorizontalAlign ="Center">
                            <Columns>
                                <asp:TemplateField HeaderText='<%$ Resources:Resource, Prof %>'>
                                    <ItemTemplate>
                                        <asp:Label ID="l1" runat="server" Text='<% # Eval("ProfE") %>' />
                                        <asp:Label ID="l2" runat="server" Text='<% # Eval("ProfA") %>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:DropDownList ID="ddlLabE" runat="server" Style="max-width: 300px">
                                        </asp:DropDownList>
                                    </FooterTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblLabEdit" runat="server" Text='<% # Eval("Code") %>' Visible="false" />
                                        <asp:DropDownList ID="ddlLabFE" runat="server" Style="max-width: 300px">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:Resource, Man %>'>
                                    <ItemTemplate>
                                        <asp:Label ID="l3" runat="server" Text='<% # Eval("Man_Day") %>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtMDF" runat="server" Style="max-width: 150px" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfv114" runat="server" ErrorMessage='<%$ Resources:Resource, MissingMan %>'
                                            ControlToValidate="txtMDF" SetFocusOnError="true" ValidationGroup="SaveLab" Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="reAvgSal1" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                            SetFocusOnError="true" ControlToValidate="txtMDF" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                            Display="None" ValidationGroup="SaveLab">
                                        </asp:RegularExpressionValidator>
                                    </FooterTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtMD" runat="server" Style="max-width: 150px" MaxLength="10" Text='<% # Eval("Man_Day") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfv119" runat="server" ErrorMessage='<%$ Resources:Resource, MissingMan %>'
                                            ControlToValidate="txtMD" SetFocusOnError="true" ValidationGroup="SaveLabE" Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="re2345" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                            SetFocusOnError="true" ControlToValidate="txtMD" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                            Display="None" ValidationGroup="SaveLabE"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign ="Center">
                                    <FooterTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" Text='<%$ Resources:Resource, Save %>'
                                            ValidationGroup="SaveLab" OnClick="LinkButton1_Click"></asp:LinkButton>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="LinkButton8" runat="server" CommandName="Update" Text='<%$ Resources:Resource, Update %>'
                                            ItemStyle-Width="25px" ValidationGroup="SaveLabE"></asp:LinkButton>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton8" runat="server" CommandName="Edit" Text='<%$ Resources:Resource, Edit %>'
                                            ItemStyle-Width="25px"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="LinkButton9" runat="server" CommandName="Cancel" Text='<%$ Resources:Resource, Cancel %>'
                                            ItemStyle-Width="25px"></asp:LinkButton>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton9" runat="server" CommandName="Delete" Text='<%$ Resources:Resource, Delete %>'
                                            ItemStyle-Width="25px"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="ActiveBorder" Height="30px"></FooterStyle>
                            <HeaderStyle Height="30px"></HeaderStyle>
                            <RowStyle Height="25px"></RowStyle>
                        </asp:GridView>
                    </div>
                    <div class="large-6 medium-6 columns" style="padding:0">
                        <asp:Label ID="Label15" runat="server" Text='<%$ Resources:Resource, EquipmentList %>'
                            Font-Bold="true"></asp:Label>
                        <br />
                        <asp:GridView ID="grdEquip" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                            EmptyDataText="No Data" Width="100%" PageSize="10" DataKeyNames="Code" ShowFooter="True"
                            HeaderStyle-Height="30px" RowStyle-Height="25px" FooterStyle-BackColor="ActiveBorder"
                            FooterStyle-Height="25px" AllowSorting="false" HeaderStyle-HorizontalAlign ="Center">
                            <Columns>
                                <asp:TemplateField HeaderText='<%$ Resources:Resource, Desc %>'>
                                    <ItemTemplate>
                                        <asp:Label ID="l11" runat="server" Text='<% # Eval("DescE") %>' />
                                        <asp:Label ID="l12" runat="server" Text='<% # Eval("DescA") %>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:DropDownList ID="ddlEquipE" runat="server" Style="max-width: 300px">
                                        </asp:DropDownList>
                                    </FooterTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEqEdit" runat="server" Text='<% # Eval("Code") %>' Visible="false" />
                                        <asp:DropDownList ID="ddlEquipFE" runat="server" Style="max-width: 300px">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:Resource, Day %>'>
                                    <ItemTemplate>
                                        <asp:Label ID="l13" runat="server" Text='<% # Eval("Man_Day") %>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtMDF1" runat="server" Style="max-width: 150px" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfv1134" runat="server" ErrorMessage='<%$ Resources:Resource, MissingDay %>'
                                            ControlToValidate="txtMDF1" SetFocusOnError="true" ValidationGroup="SaveEq" Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="reAvgSal2" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                            SetFocusOnError="true" ControlToValidate="txtMDF1" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                            Display="None" ValidationGroup="SaveEq">
                                        </asp:RegularExpressionValidator>
                                    </FooterTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtMD1" runat="server" Style="max-width: 150px" MaxLength="50" Text='<% # Eval("Man_Day") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfv1319" runat="server" ErrorMessage='<%$ Resources:Resource, MissingDay %>'
                                            ControlToValidate="txtMD1" SetFocusOnError="true" ValidationGroup="SaveEqE" Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="rgexp897" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                            SetFocusOnError="true" ControlToValidate="txtMD1" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                            Display="None" ValidationGroup="SaveEqE">
                                        </asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign ="Center">
                                    <FooterTemplate>
                                        <asp:LinkButton ID="LinkButton11" runat="server" Text='<%$ Resources:Resource, Save %>'
                                            ValidationGroup="SaveEq" OnClick="LinkButton11_Click"></asp:LinkButton>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="LinkButton81" runat="server" CommandName="Update" Text='<%$ Resources:Resource, Update %>'
                                            ItemStyle-Width="25px" ValidationGroup="SaveEqE"></asp:LinkButton>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton82" runat="server" CommandName="Edit" Text='<%$ Resources:Resource, Edit %>'
                                            ItemStyle-Width="25px"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="LinkButton91" runat="server" CommandName="Cancel" Text='<%$ Resources:Resource, Cancel %>'
                                            ItemStyle-Width="25px"></asp:LinkButton>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton92" runat="server" CommandName="Delete" Text='<%$ Resources:Resource, Delete %>'
                                            ItemStyle-Width="25px"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>
            
            <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
                ShowMessageBox="true" ValidationGroup="Save" DisplayMode="BulletList" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveLab" DisplayMode="BulletList" />
            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveLabE" DisplayMode="BulletList" />
            <asp:ValidationSummary ID="ValidationSummary3" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveEq" DisplayMode="BulletList" />
            <asp:ValidationSummary ID="ValidationSummary4" runat="server" EnableClientScript="true"
                ShowSummary="false" ShowMessageBox="true" ValidationGroup="SaveEqE" DisplayMode="BulletList" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" runat="server" contentplaceholderid="MiddlePart">

                <div class="row">
               <div class="large-12 medium-12 column">
                    <div class="outerDivBtn">
                        <div class="BTNdiv">
                    <asp:Button ID="btnAdd" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave rightbtn"
                        ValidationGroup="Save" />
                       
                    <asp:Button ID="Button4" runat="server" Text='<%$ Resources:Resource, Cancel %>'
                        CssClass="BTNCancel " />
                        <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, ViewProductivity %>'
                        CssClass="BTNView" />
                </div></div></div>
            </div>
</asp:Content>

