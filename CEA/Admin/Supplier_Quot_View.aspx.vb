﻿Public Class Supplier_Quot_View
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim objSqlConfig As New ConfigClassSqlServer()
                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (Session("User_Role_ID").ToString().Contains("999")) Then
                    If (menuItems.Count > 15) Then
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                    End If
                ElseIf (Session("User_Role_ID").ToString().Contains("777")) Then
                    If (menuItems.Count > 14) Then
                        For i As Integer = 0 To 13
                            menuItems.RemoveAt(0)
                        Next
                    End If
                End If

                grdProducts.Columns(1).Visible = True
                grdProducts.Columns(2).Visible = True
                'grdProducts.Columns(11).Visible = True

                'DropDownList1.DataSource = objSqlConfig.GetAllSuppliers()
                DropDownList1.DataSource = objSqlConfig.Get_AllSuppliers()
                DropDownList1.DataValueField = "ID"
                DropDownList1.DataTextField = "NameE"
                DropDownList1.DataBind()

                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    DropDownList1.DataTextField = "NameA"
                    grdProducts.Columns(1).Visible = False

                    Dim item As New ListItem("الكل", 0)
                    DropDownList1.Items.Insert(0, item)
                Else
                    grdProducts.Columns(2).Visible = False
                    Dim item As New ListItem("ALL", 0)
                    DropDownList1.Items.Insert(0, item)
                End If

                DropDownList1.SelectedIndex = 0
                LoadDataGrid_Products()
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
    End Sub
    Private Sub LoadDataGrid_Products()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()

        Dim fromID As String = "0"
        Dim toID As String = "9999999"

        If (DropDownList1.SelectedIndex > 0) Then
            fromID = DropDownList1.SelectedValue.ToString()
            toID = DropDownList1.SelectedValue.ToString()
        End If

        'dt = objSqlConfig.GetProductBySupplier(fromID, toID, txtSearch.Text)
        dt = objSqlConfig.Get_Supplier_Quotes_ByRange(fromID, toID, txtSearch.Text)
        If (dt.Rows.Count > 0) Then
            grdProducts.DataSource = dt
            grdProducts.DataBind()
            Session("ProductList") = dt
        Else
            grdProducts.DataSource = Nothing
            grdProducts.DataBind()
            Session("ProductList") = Nothing
        End If
    End Sub
    Private Sub FillGrid_Products()
        Dim Data As DataTable = Session("ProductList")
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp") Is Nothing) Then
                If ViewState("SortDirection").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp") & " " & "desc"
                End If

                grdProducts.DataSource = dv
                grdProducts.DataBind()
            End If
        End If
    End Sub
    Protected Sub grdProducts_DataBound(sender As Object, e As EventArgs) Handles grdProducts.DataBound
        For Each row As GridViewRow In grdProducts.Rows
            If (row.Cells(2).Text.Equals("&nbsp;") Or row.Cells(2).Text.Equals("&amp;nbsp;") Or row.Cells(2).Text.Equals("")) Then
                row.Cells(2).Text = String.Empty
            End If
        Next
    End Sub
    Protected Sub grdProducts_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdProducts.SelectedIndexChanged
        'Response.Redirect("Supplier_Products_Approve.aspx?ID=" + grdProducts.SelectedDataKey("ID").ToString())
        Dim strNodeNo As String = grdProducts.Rows(grdProducts.SelectedIndex).Cells(0).Text
        Dim strNodeName As String = grdProducts.Rows(grdProducts.SelectedIndex).Cells(1).Text
        Session("MapID") = Nothing
        Response.Redirect("Supplier_quote.aspx?ID=" + grdProducts.SelectedDataKey("ID").ToString() + "&NodeNo=" + strNodeNo + "&NodeName=" + strNodeName)
    End Sub
    Protected Sub grdProducts_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdProducts.Sorting
        If (ViewState("SortState") IsNot Nothing And ViewState("SortState") = SortDirection.Ascending) Then
            ViewState("SortDirection") = "asc"
            ViewState("SortState") = 1
        Else
            ViewState("SortDirection") = "desc"
            ViewState("SortState") = 0
        End If
        ViewState("SortExp") = e.SortExpression
        FillGrid_Products()
    End Sub
    Protected Sub grdProducts_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdProducts.PageIndexChanging
        grdProducts.PageIndex = e.NewPageIndex
        grdProducts.DataSource = Session("ProductList")
        grdProducts.DataBind()
    End Sub
    'Protected Sub grdProducts_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdProducts.RowDeleting
    '    Dim objSqlConfig As New ConfigClassSqlServer()
    '    Dim retVal As Integer = objSqlConfig.DeleteProductByID(grdProducts.DataKeys(e.RowIndex).Value.ToString())
    '    If (retVal = 0) Then
    '        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('5');", True)
    '        LoadDataGrid_Products()
    '    End If
    'End Sub
    'Protected Sub grdProducts_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdProducts.RowDataBound
    '    If (e.Row.RowType = DataControlRowType.DataRow) Then
    '        For Each link As LinkButton In e.Row.Cells(13).Controls.OfType(Of LinkButton)()
    '            If link.CommandName = "Delete" Then
    '                link.Attributes("onclick") = "if (ShowConfirm() == false) { return false; };"
    '            End If
    '        Next
    '    End If
    'End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        LoadDataGrid_Products()
    End Sub
    Protected Sub DropDownList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownList1.SelectedIndexChanged
        LoadDataGrid_Products()
    End Sub
End Class