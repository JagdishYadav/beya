﻿Public Class NewLabourCategory
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (Session("User_Role_ID").ToString().Contains("999")) Then
                    If (menuItems.Count > 15) Then
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                    End If
                ElseIf (Session("User_Role_ID").ToString().Contains("888")) Then
                    If (menuItems.Count > 5) Then
                        menuItems.RemoveAt(0)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                    End If
                End If
                lblTotSal.Text = GetGlobalResourceObject("Resource", "TotSal").ToString() + " : " + "0.00"
                Dim objSqlConfig As New ConfigClassSqlServer()
                ddlNat.DataSource = objSqlConfig.GetCountries()
                ddlNat.DataValueField = "ID"
                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    'LastRow.Style.Add("direction", "rtl")
                    'LastRow.Style.Add("text-align", "right")
                    'tdsave.Align = "left"
                    'tdcancel.Align = "right"
                    'header1.Style.Add("float", "right")
                    'header2.Style.Add("float", "right")
                    'div1.Style.Add("float", "right")
                    'div2.Style.Add("float", "right")
                    'div3.Style.Add("float", "right")
                    'div4.Style.Add("float", "right")
                    'div5.Style.Add("float", "right")
                    'div6.Style.Add("float", "right")
                    'div7.Style.Add("float", "right")
                    'div8.Style.Add("float", "right")
                    'div9.Style.Add("float", "right")
                    'div10.Style.Add("float", "right")
                    'div11.Style.Add("float", "right")
                    'div12.Style.Add("float", "right")
                    ddlNat.DataTextField = "NameA"
                Else
                    ddlNat.DataTextField = "NameE"
                End If
                ddlNat.DataBind()
                txtCode.Enabled = True
                If (Request.QueryString.Get("ID") IsNot Nothing) Then
                    Button1.Text = GetGlobalResourceObject("Resource", "Update").ToString()
                    FillLabourData(Request.QueryString.Get("ID").ToString())
                    txtCode.Enabled = False
                Else
                    Button1.Text = GetGlobalResourceObject("Resource", "Save").ToString()
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
    End Sub
    Private Sub FillLabourData(strLabourCode As String)
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        dt = objSqlConfig.GetLabourByCode(strLabourCode, Session("CompanyID"))
        If (dt.Rows.Count > 0) Then
            txtCode.Text = dt.Rows(0).Item(0).ToString()
            txtProfE.Text = dt.Rows(0).Item(1).ToString()
            txtProfA.Text = dt.Rows(0).Item(2).ToString()
            txtGenExp.Text = dt.Rows(0).Item(3).ToString()
            txtAvgSal.Text = dt.Rows(0).Item(4).ToString()
            txtHousing.Text = dt.Rows(0).Item(5).ToString()
            txtFood.Text = dt.Rows(0).Item(6).ToString()
            txtMedIns.Text = dt.Rows(0).Item(7).ToString()
            txtSecIns.Text = dt.Rows(0).Item(8).ToString()
            txtTicket.Text = dt.Rows(0).Item(9).ToString()
            txtEOCont.Text = dt.Rows(0).Item(10).ToString()
            txtIqama.Text = dt.Rows(0).Item(11).ToString()
            'txtNat.Text = dt.Rows(0).Item(12).ToString()
            ddlNat.SelectedValue = dt.Rows(0).Item(12).ToString()

            CalculateCost()
        End If
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        txtCode.Text = ""
        txtProfE.Text = ""
        txtProfA.Text = ""
        txtGenExp.Text = "0.00"
        txtAvgSal.Text = "0.00"
        txtHousing.Text = "0.00"
        txtFood.Text = "0.00"
        txtMedIns.Text = "0.00"
        txtSecIns.Text = "0.00"
        txtTicket.Text = "0.00"
        txtEOCont.Text = "0.00"
        txtIqama.Text = "0.00"
        'txtNat.Text = ""
        ddlNat.SelectedIndex = 0
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim objSqlConfig As New ConfigClassSqlServer()

        If (txtGenExp.Text.Length = 0) Then
            txtGenExp.Text = "0.00"
        End If
        If (txtAvgSal.Text.Length = 0) Then
            txtAvgSal.Text = "0.00"
        End If
        If (txtHousing.Text.Length = 0) Then
            txtHousing.Text = "0.00"
        End If
        If (txtFood.Text.Length = 0) Then
            txtFood.Text = "0.00"
        End If
        If (txtMedIns.Text.Length = 0) Then
            txtMedIns.Text = "0.00"
        End If
        If (txtSecIns.Text.Length = 0) Then
            txtSecIns.Text = "0.00"
        End If
        If (txtEOCont.Text.Length = 0) Then
            txtEOCont.Text = "0.00"
        End If
        If (txtTicket.Text.Length = 0) Then
            txtTicket.Text = "0.00"
        End If
        If (txtIqama.Text.Length = 0) Then
            txtIqama.Text = "0.00"
        End If

        Dim HourlyRate As String = (Double.Parse(hdnTotCost.Value) / 208).ToString()

        If (Request.QueryString.Get("ID") Is Nothing) Then
            Dim retVal As Integer = objSqlConfig.CreateLabour(txtCode.Text.Trim(), txtProfE.Text.Trim(), txtProfA.Text.Trim(), txtGenExp.Text.Trim(), txtAvgSal.Text.Trim(), txtHousing.Text.Trim(), txtFood.Text.Trim(), txtMedIns.Text.Trim(), txtSecIns.Text.Trim(), txtTicket.Text.Trim(), txtEOCont.Text.Trim(), txtIqama.Text.Trim(), ddlNat.SelectedValue, HourlyRate.Trim(), Session("CompanyID"))
            If (retVal = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
                Button2_Click(sender, Nothing)
            ElseIf (retVal = 1) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('-1');", True)
            End If
        Else
            Dim retVal As Integer = objSqlConfig.UpdateLabour(txtCode.Text.Trim(), txtProfE.Text.Trim(), txtProfA.Text.Trim(), txtGenExp.Text.Trim(), txtAvgSal.Text.Trim(), txtHousing.Text.Trim(), txtFood.Text.Trim(), txtMedIns.Text.Trim(), txtSecIns.Text.Trim(), txtTicket.Text.Trim(), txtEOCont.Text.Trim(), txtIqama.Text.Trim(), ddlNat.SelectedValue, HourlyRate.Trim(), Session("CompanyID"))
            If (retVal = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
            ElseIf (retVal = 1) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('5');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('-1');", True)
            End If
        End If
    End Sub
    Private Sub CalculateCost()
        Dim totalCost = 0
        totalCost = totalCost + Double.Parse(txtGenExp.Text) + Double.Parse(txtAvgSal.Text) + Double.Parse(txtHousing.Text) + Double.Parse(txtFood.Text) + Double.Parse(txtMedIns.Text) + Double.Parse(txtSecIns.Text) + Double.Parse(txtTicket.Text) + Double.Parse(txtEOCont.Text) + Double.Parse(txtIqama.Text)
        lblTotSal.Text = GetGlobalResourceObject("Resource", "TotSal").ToString() + " : " + totalCost.ToString()
        hdnTotCost.Value = totalCost
    End Sub
    Protected Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        Response.Redirect("NewLabourCategory.aspx")
    End Sub
End Class