﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Office
Imports Microsoft.Office.Interop.Word
Imports System.IO
Public Class CrewStructure_New
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                Menuhide.Value = "close"
                Dim logout As Button = Master.FindControl("Logout")
                logout.Visible = False

                Button9999.Attributes.Add("style", "display:none")

                If (Session("CameFromCES") Is Nothing) Then
                    Button2.OnClientClick = "window.opener.document.getElementById('ctl00_MainContent_Button3').click(); window.close();"
                Else
                    Button2.Text = GetGlobalResourceObject("Resource", "Previous")
                End If

                Dim objSqlConfig As New ConfigClassSqlServer()
                ddlCountry.DataSource = objSqlConfig.GetCountries()
                ddlCountry.DataValueField = "ID"

                ddlProd.DataSource = objSqlConfig.GetProductivities()
                ddlProd.DataValueField = "Code"

                Session("Labourdt") = Nothing
                Session("Equipdt") = Nothing

                If (Session("ProductCode") IsNot Nothing) Then
                    lblProdCode.Text = Session("ProductCode")
                End If
                If (Session("ProductName") IsNot Nothing) Then
                    lblProdName.Text = Session("ProductName").ToString()
                    lblProdName2.Text = Session("ProductNameAra").ToString()
                End If
                If (Session("ProductUnit") IsNot Nothing) Then
                    lblProdUnit.Text = Session("ProductUnit")
                End If

                If (UICulture = "Arabic") Then
                    ddlCountry.DataTextField = "NameA"
                    abc.Style.Add("direction", "rtl")
                    abc.Style.Add("text-align", "right")
                    ddlProd.DataTextField = "NameA"
                    ddlProd.DataBind()
                    ddlProd.Items.Insert(0, "Select")
                    options.Style.Add("float", "right")
                    rbtn1.Style.Add("float", "right")
                    rbtn2.Style.Add("float", "right")
                    rbtn3.Style.Add("float", "right")
                    rbtn4.Style.Add("float", "right")

                    rbtn1.Style.Add("direction", "rtl")
                    rbtn1.Style.Add("text-align", "right")
                    rbtn2.Style.Add("direction", "rtl")
                    rbtn2.Style.Add("text-align", "right")
                    rbtn3.Style.Add("direction", "rtl")
                    rbtn3.Style.Add("text-align", "right")
                    rbtn4.Style.Add("direction", "rtl")
                    rbtn4.Style.Add("text-align", "right")
                    options.Style.Add("direction", "rtl")
                    options.Style.Add("text-align", "right")

                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    Panel2.Style.Add("direction", "rtl")
                    Panel2.Style.Add("text-align", "right")
                    Panel3.Style.Add("direction", "rtl")
                    Panel3.Style.Add("text-align", "right")
                    Panel4.Style.Add("direction", "rtl")
                    Panel4.Style.Add("text-align", "right")
                Else
                    ddlCountry.DataTextField = "NameE"
                    ddlProd.DataTextField = "NameE"
                    ddlProd.DataBind()
                    ddlProd.Items.Insert(0, "Select")
                End If
                ddlCountry.DataBind()
                ddlCountry.SelectedIndex = 0
                ddlProd.SelectedIndex = 0

                LoadGrids()
                LoadHistoryGrid()
                LoadDocGrid()
                LoadDropDowns()
                SetEnvironmentAccordingtoLang()
                LoadOtherData()
                LoadRefData()
            Else
                Response.Redirect("../Login.aspx")
            End If

        End If
    End Sub
    Private Sub LoadGrids()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt, dt1 As New System.Data.DataTable()
        If (Session("Labourdt") IsNot Nothing) Then
            grdLabour.DataSource = Session("Labourdt")
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False

            dt = Session("Labourdt")
            If (dt.Rows.Count > 1) Then
                ddlCountry.SelectedValue = dt.Rows(1).Item(9).ToString()
            End If
        Else
            dt = objSqlConfig.GetProductCrewStructure_Labour(Session("ProductCode"))
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = "-1"
            dr("Code") = "000"
            dr("ProfE") = ""
            dr("ProfA") = ""
            dr("Man_Day") = 0

            dt.Rows.InsertAt(dr, 0)
            grdLabour.DataSource = dt
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False
            Session("Labourdt") = dt

            If (dt.Rows.Count > 1) Then
                ddlCountry.SelectedValue = dt.Rows(1).Item(9).ToString()
            End If
        End If

        If (Session("Equipdt") IsNot Nothing) Then
            grdEquip.DataSource = Session("Equipdt")
            grdEquip.DataBind()
            grdEquip.Rows(0).Visible = False
        Else
            dt1 = objSqlConfig.GetProductCrewStructure_Equipment(Session("ProductCode"))
            Dim dr1 As DataRow = dt1.NewRow()
            dr1("ID") = "-1"
            dr1("Code") = "000"
            dr1("DescE") = ""
            dr1("DescA") = ""
            dr1("Man_Day") = 0

            dt1.Rows.InsertAt(dr1, 0)
            grdEquip.DataSource = dt1
            grdEquip.DataBind()
            grdEquip.Rows(0).Visible = False
            Session("Equipdt") = dt1
        End If
    End Sub
    Private Sub LoadHistoryGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New System.Data.DataTable()
        
        dt = objSqlConfig.GetProductivityHistory(lblProdCode.Text.Trim())
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = "-1"
            dr("Code") = "000"
        dr("DateTime") = Now.ToString("yyyyMMddHHmmss")
        dr("DO") = 0

            dt.Rows.InsertAt(dr, 0)
        grdHistory.DataSource = dt
        grdHistory.DataBind()
        grdHistory.Rows(0).Visible = False
    End Sub
    Private Sub LoadDropDowns()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim ddlLabour As DropDownList = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList)
        If (ddlLabour IsNot Nothing) Then
            ddlLabour.DataSource = objSqlConfig.GetLaboursByCountry("", ddlCountry.SelectedValue, Session("CompanyID"))
            ddlLabour.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlLabour.DataTextField = "ProfA"
            Else
                ddlLabour.DataTextField = "ProfE"
            End If
            ddlLabour.DataBind()
        End If


        Dim ddlEquip As DropDownList = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList)
        If (ddlEquip IsNot Nothing) Then
            ddlEquip.DataSource = objSqlConfig.GetEquipments("", Session("CompanyID"))
            ddlEquip.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlEquip.DataTextField = "DescA"
            Else
                ddlEquip.DataTextField = "DescE"
            End If
            ddlEquip.DataBind()
        End If
    End Sub
    Protected Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCountry.SelectedIndexChanged
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim ddlLabour As DropDownList = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList)
        If (ddlLabour IsNot Nothing) Then
            ddlLabour.DataSource = objSqlConfig.GetLaboursByCountry("", ddlCountry.SelectedValue, Session("CompanyID"))
            ddlLabour.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlLabour.DataTextField = "ProfA"
            Else
                ddlLabour.DataTextField = "ProfE"
            End If
            ddlLabour.DataBind()
        End If
    End Sub
    Private Sub SetEnvironmentAccordingtoLang()
        If (UICulture = "Arabic") Then
            For i As Integer = 0 To grdLabour.Rows.Count - 1
                If (grdLabour.Rows(i).RowState = DataControlRowState.Normal Or grdLabour.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdLabour.Rows(i).FindControl("l1"), Label).Visible = False
                End If
            Next
            For i As Integer = 0 To grdEquip.Rows.Count - 1
                If (grdEquip.Rows(i).RowState = DataControlRowState.Normal Or grdEquip.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdEquip.Rows(i).FindControl("l11"), Label).Visible = False
                End If
            Next
        Else
            For i As Integer = 0 To grdLabour.Rows.Count - 1
                If (grdLabour.Rows(i).RowState = DataControlRowState.Normal Or grdLabour.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdLabour.Rows(i).FindControl("l2"), Label).Visible = False
                End If
            Next
            For i As Integer = 0 To grdEquip.Rows.Count - 1
                If (grdEquip.Rows(i).RowState = DataControlRowState.Normal Or grdEquip.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdEquip.Rows(i).FindControl("l12"), Label).Visible = False
                End If
            Next
        End If
    End Sub
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs)
        Dim dt As System.Data.DataTable = Session("Labourdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                If (dt.Rows(i).Item(1).ToString() = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next

        If (iCount = 0) Then
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = "-1"
            dr("Code") = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedValue
            dr("ProfE") = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedItem.Text
            dr("ProfA") = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedItem.Text
            dr("Man_Day") = TryCast(grdLabour.FooterRow.FindControl("txtMDF"), TextBox).Text.Trim()

            dt.Rows.Add(dr)
            grdLabour.DataSource = dt
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False

            Session("Labourdt") = dt
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(2);", True)
        End If
    End Sub
    Protected Sub LinkButton11_Click(sender As Object, e As EventArgs)
        Dim dt As System.Data.DataTable = Session("Equipdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                If (dt.Rows(i).Item(1).ToString() = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next

        If (iCount = 0) Then
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = "-1"
            dr("Code") = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedValue
            dr("DescE") = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedItem.Text
            dr("DescA") = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedItem.Text
            dr("Man_Day") = TryCast(grdEquip.FooterRow.FindControl("txtMDF1"), TextBox).Text.Trim()

            dt.Rows.Add(dr)
            grdEquip.DataSource = dt
            grdEquip.DataBind()
            grdEquip.Rows(0).Visible = False

            Session("Equipdt") = dt
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(3);", True)
        End If
    End Sub
    Protected Sub grdLabour_RowCancelingEdit(sender As Object, e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdLabour.RowCancelingEdit
        e.Cancel = True
        grdLabour.EditIndex = -1
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdLabour_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdLabour.RowEditing
        grdLabour.EditIndex = e.NewEditIndex
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdLabour_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdLabour.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow And grdLabour.EditIndex = e.Row.RowIndex) Then
            Dim ddlLabour As DropDownList = DirectCast(e.Row.FindControl("ddlLabFE"), DropDownList)
            Dim objSqlConfig As New ConfigClassSqlServer()
            ddlLabour.DataSource = objSqlConfig.GetLaboursByCountry("", ddlCountry.SelectedValue, Session("CompanyID"))
            ddlLabour.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlLabour.DataTextField = "ProfA"
            Else
                ddlLabour.DataTextField = "ProfE"
            End If
            ddlLabour.DataBind()
            ddlLabour.SelectedValue = DirectCast(e.Row.FindControl("lblLabEdit"), Label).Text
        End If
    End Sub
    Protected Sub grdLabour_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdLabour.RowUpdating
        Dim dt As System.Data.DataTable = Session("Labourdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (i <> e.RowIndex) Then
                If (dt.Rows(i).Item(1).ToString() = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next

        If (iCount = 0) Then
            dt.Rows(e.RowIndex).Item(1) = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedValue
            dt.Rows(e.RowIndex).Item(2) = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(3) = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(4) = CType(grdLabour.Rows(e.RowIndex).FindControl("txtMD"), TextBox).Text.Trim()

            grdLabour.EditIndex = -1
            Session("Labourdt") = dt
            LoadGrids()
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(2);", True)
        End If
    End Sub
    Protected Sub grdLabour_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdLabour.RowDeleting
        Dim dt As System.Data.DataTable = Session("Labourdt")
        dt.Rows.RemoveAt(e.RowIndex)
        Session("Labourdt") = dt
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
        CalculateValues()
    End Sub
    Protected Sub grdLabour_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdLabour.PageIndexChanging
        grdLabour.PageIndex = e.NewPageIndex
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdEquip_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdEquip.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow And grdEquip.EditIndex = e.Row.RowIndex) Then
            Dim ddlEquip As DropDownList = DirectCast(e.Row.FindControl("ddlEquipFE"), DropDownList)
            Dim objSqlConfig As New ConfigClassSqlServer()
            ddlEquip.DataSource = objSqlConfig.GetEquipments("", Session("CompanyID"))
            ddlEquip.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlEquip.DataTextField = "DescA"
            Else
                ddlEquip.DataTextField = "DescE"
            End If
            ddlEquip.DataBind()
            ddlEquip.SelectedValue = DirectCast(e.Row.FindControl("lblEqEdit"), Label).Text
        End If
    End Sub
    Protected Sub grdEquip_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdEquip.RowEditing
        grdEquip.EditIndex = e.NewEditIndex
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdEquip_RowCancelingEdit(sender As Object, e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdEquip.RowCancelingEdit
        e.Cancel = True
        grdEquip.EditIndex = -1
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdEquip_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdEquip.RowDeleting
        Dim dt As System.Data.DataTable = Session("Equipdt")
        dt.Rows.RemoveAt(e.RowIndex)
        Session("Equipdt") = dt
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
        CalculateValues()
    End Sub
    Protected Sub grdEquip_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdEquip.RowUpdating
        Dim dt As System.Data.DataTable = Session("Equipdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (i <> e.RowIndex) Then
                If (dt.Rows(i).Item(1).ToString() = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next
        If (iCount = 0) Then
            dt.Rows(e.RowIndex).Item(1) = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedValue
            dt.Rows(e.RowIndex).Item(2) = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(3) = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(4) = CType(grdEquip.Rows(e.RowIndex).FindControl("txtMD1"), TextBox).Text.Trim()

            grdEquip.EditIndex = -1
            Session("Equipdt") = dt
            LoadGrids()
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(2);", True)
        End If
    End Sub
    Protected Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        ddlCountry.SelectedIndex = 0
        'txtDO.Text = ""
        LoadOtherData()
        Session("Labourdt") = Nothing
        Session("Equipdt") = Nothing

        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim dt As System.Data.DataTable = Session("Labourdt")
        Dim dt1 As System.Data.DataTable = Session("Equipdt")
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "DELETE FROM ProductCrewStructure WHERE ProductID = '" + lblProdCode.Text + "' "
        sqlCommand.ExecuteNonQuery()

        For i As Integer = 1 To dt.Rows.Count - 1
            If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                sqlCommand.CommandText = "SELECT COUNT(*) FROM ProductCrewStructure WHERE ProductID = '" + lblProdCode.Text + "' AND Code = '" + dt.Rows(i).Item(1).ToString() + "' "
                If (sqlCommand.ExecuteScalar().ToString() = "0") Then
                    sqlCommand.CommandText = "INSERT INTO ProductCrewStructure(ProductID,Code,Man_Day,DailyOutput,HourlyOutput, ManHour, EquipHour, Lab_Equip) VALUES('" + lblProdCode.Text.Trim() + "', '" + dt.Rows(i).Item(1).ToString() + "', '" + dt.Rows(i).Item(4).ToString() + "', '" + txtDO.Text.Trim() + "', '" + lblHO.Text.Trim() + "', '" + lblMO.Text.Trim() + "', '" + lblEH.Text.Trim() + "', 1)"
                    sqlCommand.ExecuteNonQuery()
                End If
            End If
        Next

        For i As Integer = 1 To dt1.Rows.Count - 1
            If (dt1.Rows(i).RowState <> DataRowState.Deleted) Then
                sqlCommand.CommandText = "SELECT COUNT(*) FROM ProductCrewStructure WHERE ProductID = '" + lblProdCode.Text + "' AND Code = '" + dt1.Rows(i).Item(1).ToString() + "' "
                If (sqlCommand.ExecuteScalar().ToString() = "0") Then
                    sqlCommand.CommandText = "INSERT INTO ProductCrewStructure(ProductID,Code,Man_Day,DailyOutput,HourlyOutput, ManHour, EquipHour, Lab_Equip) VALUES('" + lblProdCode.Text.Trim() + "', '" + dt1.Rows(i).Item(1).ToString() + "', '" + dt1.Rows(i).Item(4).ToString() + "', '" + txtDO.Text.Trim() + "', '" + lblHO.Text.Trim() + "', '" + lblMO.Text.Trim() + "', '" + lblEH.Text.Trim() + "', 2)"
                    sqlCommand.ExecuteNonQuery()
                End If
            End If

        Next

        sqlCommand.CommandText = "UPDATE ProductDetails SET ManHour = '" + lblMO.Text.Trim() + "', EquipHour ='" + lblEH.Text.Trim() + "' WHERE ID = '" + lblProdCode.Text + "' "
        sqlCommand.ExecuteNonQuery()

        Session("ManHours") = lblMO.Text.Trim()
        Session("EquipHours") = lblEH.Text.Trim()

        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(1);", True)
    End Sub
    Protected Sub txtDO_TextChanged(sender As Object, e As EventArgs) Handles txtDO.TextChanged
        If (IsNumeric(txtDO.Text)) Then
            CalculateValues()
        End If
    End Sub
    Private Sub CalculateValues()
        Dim dt As System.Data.DataTable = Session("Labourdt")
        Dim dt1 As System.Data.DataTable = Session("Equipdt")
        Dim dSumCSMan As Double = 0
        Dim dSumCSEquip As Double = 0

        If (IsNumeric(txtDO.Text)) Then
            lblHO.Text = Math.Round((Double.Parse(txtDO.Text) / 8), 4)
            For i As Integer = 1 To dt.Rows.Count - 1
                If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                    If (dt.Rows(i).Item(4).ToString().Length > 0 And IsNumeric(dt.Rows(i).Item(4).ToString())) Then
                        dSumCSMan = dSumCSMan + Double.Parse(dt.Rows(i).Item(4).ToString())
                    End If
                End If
            Next
            lblMO.Text = Math.Round((dSumCSMan / Double.Parse(lblHO.Text)), 4)

            For i As Integer = 1 To dt1.Rows.Count - 1

                If (dt1.Rows(i).RowState <> DataRowState.Deleted) Then
                    If (dt1.Rows(i).Item(4).ToString().Length > 0 And IsNumeric(dt1.Rows(i).Item(4).ToString())) Then
                        dSumCSEquip = dSumCSEquip + Double.Parse(dt1.Rows(i).Item(4).ToString())
                    End If
                End If
            Next
            lblEH.Text = Math.Round((dSumCSEquip / Double.Parse(lblHO.Text)), 4)
        End If
    End Sub
    Private Sub LoadOtherData()
        Dim dt As System.Data.DataTable = Session("Labourdt")
        Dim dt1 As System.Data.DataTable = Session("Equipdt")

        If (dt.Rows.Count > 1) Then
            txtDO.Text = dt.Rows(1).Item(5).ToString()
            lblHO.Text = dt.Rows(1).Item(6).ToString()
            lblMO.Text = dt.Rows(1).Item(7).ToString()
            lblEH.Text = dt.Rows(1).Item(8).ToString()
        ElseIf dt1.Rows.Count > 1 Then
            txtDO.Text = dt1.Rows(1).Item(5).ToString()
            lblHO.Text = dt1.Rows(1).Item(6).ToString()
            lblMO.Text = dt1.Rows(1).Item(7).ToString()
            lblEH.Text = dt1.Rows(1).Item(8).ToString()
        End If
    End Sub
    Protected Sub ddlProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlProd.SelectedIndexChanged
        If (ddlProd.SelectedIndex > 0) Then
            Dim objSqlConfig As New ConfigClassSqlServer()
            Dim dt, dt1 As New System.Data.DataTable()

            dt = objSqlConfig.GetProductivityLaboursByCode(ddlProd.SelectedValue, Session("CompanyID"))
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = "-1"
            dr("Code") = "000"
            dr("ProfE") = ""
            dr("ProfA") = ""
            dr("Man_Day") = 0

            dt.Rows.InsertAt(dr, 0)
            grdLabour.DataSource = dt
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False
            Session("Labourdt") = dt

            If (dt.Rows.Count > 1) Then
                ddlCountry.SelectedValue = dt.Rows(1).Item(9).ToString()
                txtDO.Text = dt.Rows(1).Item(5).ToString()
            End If

            dt1 = objSqlConfig.GetProductivityEquipmentsByCode(ddlProd.SelectedValue, Session("CompanyID"))
            Dim dr1 As DataRow = dt1.NewRow()
            dr1("ID") = "-1"
            dr1("Code") = "000"
            dr1("DescE") = ""
            dr1("DescA") = ""
            dr1("Man_Day") = 0

            dt1.Rows.InsertAt(dr1, 0)
            grdEquip.DataSource = dt1
            grdEquip.DataBind()
            grdEquip.Rows(0).Visible = False

            If (dt1.Rows.Count > 1) Then
                txtDO.Text = dt1.Rows(1).Item(5).ToString()
            End If
            Session("Equipdt") = dt1

            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            CalculateValues()
        End If
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If (Session("CameFromCES") IsNot Nothing) Then
            If (Session("CameFromCES") = "1") Then
                Session("CameFromCES") = Nothing
                Response.Redirect("../CES/Final_ProductList.aspx")
            End If
        End If
    End Sub
    Protected Sub rbtn1_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rbtn1.CheckedChanged
        Panel1.Visible = False
        Panel2.Visible = False
        Panel3.Visible = False
        Panel4.Visible = False
        btnSave.Visible = False
        Button8.Visible = False
        Button2.Visible = False
        If (rbtn1.Checked = True) Then
            Panel3.Visible = True
        End If
    End Sub
    Protected Sub rbtn4_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rbtn4.CheckedChanged
        Panel1.Visible = False
        Panel2.Visible = False
        Panel3.Visible = False
        Panel4.Visible = False
        If (rbtn4.Checked = True) Then
            Panel4.Visible = True
            btnSave.Visible = True
            Button8.Visible = True
            Button2.Visible = True
        End If
    End Sub

    Protected Sub rbtn2_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rbtn2.CheckedChanged
        Panel1.Visible = False
        Panel2.Visible = False
        Panel3.Visible = False
        Panel4.Visible = False
        btnSave.Visible = False
        Button8.Visible = False
        Button2.Visible = False
        If (rbtn2.Checked = True) Then
            Panel1.Visible = True
        End If
    End Sub

    Protected Sub rbtn3_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rbtn3.CheckedChanged
        Panel1.Visible = False
        Panel2.Visible = False
        Panel3.Visible = False
        Panel4.Visible = False
        btnSave.Visible = False
        Button8.Visible = False
        Button2.Visible = False
        If (rbtn3.Checked = True) Then
            Panel2.Visible = True
        End If
    End Sub
    Protected Sub btnSaveHistory_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "INSERT INTO ProductivityHistory(Code,DateTime,DO) VALUES('" + lblProdCode.Text.Trim() + "', '" + Now.ToString("yyyyMMddHHmmss") + "', '" + TryCast(grdHistory.FooterRow.FindControl("txtDOF"), TextBox).Text + "')"
        sqlCommand.ExecuteNonQuery()

        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(1);", True)
        LoadHistoryGrid()
    End Sub
    Protected Sub grdHistory_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdHistory.RowDeleting
        Dim objSqlConfig As New ConfigClassSqlServer()
        objSqlConfig.DeleteHistory(grdHistory.DataKeys(e.RowIndex).Value.ToString())
        LoadHistoryGrid()
    End Sub
    Protected Sub grdHistory_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdHistory.RowEditing
        grdHistory.EditIndex = e.NewEditIndex
        LoadHistoryGrid()
    End Sub
    Protected Sub grdHistory_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdHistory.RowCancelingEdit
        e.Cancel = True
        grdHistory.EditIndex = -1
        LoadHistoryGrid()
    End Sub
    Protected Sub grdHistory_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdHistory.RowUpdating
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "UPDATE ProductivityHistory Set DateTime = '" + Now.ToString("yyyyMMddHHmmss") + "', DO = '" + CType(grdHistory.Rows(e.RowIndex).FindControl("txtDOE"), TextBox).Text.Trim() + "' WHERE ID = '" + grdHistory.DataKeys(e.RowIndex).Value.ToString() + "' "
        sqlCommand.ExecuteNonQuery()

        grdHistory.EditIndex = -1
        LoadHistoryGrid()
    End Sub
    Private Sub LoadDocGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New System.Data.DataTable()
        dt = objSqlConfig.GetProductivityDocs(lblProdCode.Text.Trim())
        
        grdDoc.DataSource = dt
        grdDoc.DataBind()
    End Sub
    Protected Sub Button4_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button4.Click
        FileUpload1.Attributes.Clear()
    End Sub
    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button3.Click
        Dim fileName As String = ""
        If (FileUpload1.HasFile) Then
            Dim fileExt As String = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower()
            Dim sDateTimeNow As String = DateTime.Now.ToString("yyyyMMddHHmmss")
            If (fileExt = ".doc" Or fileExt = ".docx" Or fileExt = ".pdf" Or fileExt = ".xls" Or fileExt = ".xlsx") Then
                fileName = System.IO.Path.GetFileNameWithoutExtension(FileUpload1.FileName)
                fileName = fileName + "_" + sDateTimeNow + fileExt

                FileUpload1.SaveAs(Server.MapPath("../Upload/Docs/" + fileName))
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
                Return
            End If

            Dim sqlConnection As New SqlConnection()
            Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
            sqlConnection.Open()

            sqlCommand.CommandText = "INSERT INTO ProductivityDocuments(Code, Name, DateTime, Ext) VALUES('" + lblProdCode.Text.Trim() + "', '" + fileName + "',  '" + sDateTimeNow + "', '" + fileExt + "' ) "
            sqlCommand.ExecuteNonQuery()
            LoadDocGrid()

            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('5');", True)
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('6');", True)
        End If
    End Sub
    Protected Sub grdDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles grdDoc.SelectedIndexChanged
        If (grdDoc.DataKeys(grdDoc.SelectedIndex).Values(2).ToString() = ".doc" Or grdDoc.DataKeys(grdDoc.SelectedIndex).Values(2).ToString() = ".docx") Then
            Dim File = New FileInfo(MapPath("../Upload/Docs/" + grdDoc.Rows(grdDoc.SelectedIndex).Cells(0).Text))
            Response.ClearContent()
            Response.AddHeader("Content-Disposition", "inline;filename=" + File.Name)
            Response.AddHeader("Content-Length", File.Length.ToString())
            Response.ContentType = "application/msword"
            Response.TransmitFile(File.FullName)
            Response.End()
        ElseIf (grdDoc.DataKeys(grdDoc.SelectedIndex).Values(2).ToString() = ".xls" Or grdDoc.DataKeys(grdDoc.SelectedIndex).Values(2).ToString() = ".xlsx") Then
            Dim File = New FileInfo(MapPath("../Upload/Docs/" + grdDoc.Rows(grdDoc.SelectedIndex).Cells(0).Text))
            Response.ClearContent()
            Response.AddHeader("Content-Disposition", "inline;filename=" + File.Name)
            Response.AddHeader("Content-Length", File.Length.ToString())
            Response.ContentType = "application/msexcel"
            Response.TransmitFile(File.FullName)
            Response.End()
        End If
    End Sub
    Private Sub LoadRefData()
        Dim objSqlConfig As New ConfigClassSqlServer()
        grdRef.DataSource = objSqlConfig.GetReferenceData(txtSearch.Text.Trim(), lblProdCode.Text.Trim())
        grdRef.DataBind()
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        LoadRefData()
    End Sub
    Protected Sub grdDoc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdDoc.PageIndexChanging
        grdDoc.PageIndex = e.NewPageIndex
        LoadDocGrid()
    End Sub
    Protected Sub grdRef_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdRef.PageIndexChanging
        grdRef.PageIndex = e.NewPageIndex
        LoadRefData()
    End Sub
End Class