﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Equipment_List.aspx.vb" Inherits="CEA.Equipment_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        function ShowConfirm() {
            var strConfirm = '<%=GetGlobalResourceObject("Resource", "ConfirmDelete") %>';
            return confirm(strConfirm);
        }
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Success") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "UpdateSuccess") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteSuccess") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorAlreadyExist") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorNotExist") %>';
            }

            alert(msgstring);
        }
    </script>
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
User Management
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="panel">

                <div class="row">
                    <div class="col-12 col-md-7 col-lg-5">
                        <%--<asp:Label ID="Label15" runat="server" Text="Search" Width="100%"></asp:Label>--%>
                        <div class="input-group mb-4 commanSearchGroup">
                            <asp:TextBox ID="txtSearch" runat="server" SkinID="FormControl" CssClass="form-control" placeholder="Search"></asp:TextBox>
                            <div class="input-group-append">
                                <asp:Button ID="Button5" runat="server" Text="Search" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                </div>


                <div class="table-responsive tableLayoutWrap altboarder">
                    <asp:GridView ID="grdEquipmet" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="10" DataKeyNames="Code" HeaderStyle-HorizontalAlign="Center">
                        <Columns>
                            <asp:BoundField DataField="Code" SortExpression="Code" HeaderText='<%$ Resources:Resource, Code %>'
                                ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="DescE" SortExpression="DescE" HeaderText='<%$ Resources:Resource, DescriptionE %>'
                                ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="DescA" SortExpression="DescA" HeaderText='<%$ Resources:Resource, DescriptionA %>'
                                ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Unit" SortExpression="Unit" HeaderText='<%$ Resources:Resource, Unit %>'
                                ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="OperationCost" SortExpression="OperationCost" HeaderText='<%$ Resources:Resource, OperationCost %>'
                                ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="EquipmentDepCost" SortExpression="EquipmentDepCost" HeaderText='<%$ Resources:Resource, DepCost %>'
                                ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="EquipmentDailyCost" SortExpression="EquipmentDailyCost"
                                HeaderText='<%$ Resources:Resource, DailyCost %>' ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="RentalCostD" SortExpression="RentalCostD" HeaderText='<%$ Resources:Resource, RentalCostDay %>'
                                ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="RentalCostM" SortExpression="RentalCostM" HeaderText='<%$ Resources:Resource, RentalCostMonth %>'
                                ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:CommandField ShowCancelButton="false" ShowDeleteButton="false" ShowEditButton="true"
                                ShowInsertButton="false" ItemStyle-Width="25px" ShowSelectButton="false" EditImageUrl="../Images/tableedit.png" ButtonType="Image" />
                            <asp:CommandField ShowCancelButton="false" ShowDeleteButton="true" ShowEditButton="false"
                                ShowInsertButton="false" ItemStyle-Width="25px" ShowSelectButton="false" DeleteImageUrl="../Images/tabledelete.png" ButtonType="Image" />
                        </Columns>
                    </asp:GridView>
                </div>
                
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>$(document).ready(function () { $("#sidr").show(); });</script>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="PageTitle">

    <asp:Label ID="lblHeader" runat="server" Text='<%$ Resources:Resource, EquipmentList %>'></asp:Label>
</asp:Content>

