﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Office
Imports Microsoft.Office.Interop.Word
Imports System.IO
Imports System.Data.OleDb

Public Class Add_CrewStructure
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                'Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                'Menuhide.Value = "close"
                'Dim logout As Button = Master.FindControl("Logout")
                'logout.Visible = False

                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (Session("User_Role_ID").ToString().Contains("999")) Then
                    If (menuItems.Count > 15) Then
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                    End If
                ElseIf (Session("User_Role_ID").ToString().Contains("888")) Then
                    If (menuItems.Count > 5) Then
                        menuItems.RemoveAt(0)
                        menuItems.RemoveAt(1)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(2)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                        menuItems.RemoveAt(5)
                    End If
                End If

                Button9999.Attributes.Add("style", "display:none")
                Button100.Attributes.Add("style", "display:none")
                Panel3.Attributes.Add("style", "display:none")

                'If (Session("CameFromCES") Is Nothing) Then
                '    Button2.OnClientClick = "window.opener.document.getElementById('ctl00_MainContent_Button3').click(); window.close();"
                'Else
                '    Button2.Text = GetGlobalResourceObject("Resource", "Previous")
                'End If

                Dim objSqlConfig As New ConfigClassSqlServer()
                ddlCountry.DataSource = objSqlConfig.GetCountries()
                ddlCountry.DataValueField = "ID"

                ddlProd.DataSource = objSqlConfig.GetProductivities()
                ddlProd.DataValueField = "Code"

                Session("Labourdt") = Nothing
                Session("Equipdt") = Nothing

                'If (Session("ProductCode") IsNot Nothing) Then
                '    lblProdCode.Text = Session("ProductCode")
                'End If
                'If (Session("ProductName") IsNot Nothing) Then
                '    lblProdName.Text = Session("ProductName").ToString()
                '    lblProdName2.Text = Session("ProductNameAra").ToString()
                'End If
                'If (Session("ProductUnit") IsNot Nothing) Then
                '    lblProdUnit.Text = Session("ProductUnit")
                'End If
                'txtCode.ReadOnly = False

                If (Request.QueryString("Code") IsNot Nothing) Then
                    Session("ProductCode") = Request.QueryString.Get("Code")
                    txtCode.Text = Request.QueryString.Get("Code")
                    'txtCode.ReadOnly = True
                    ViewState("CS_CREATED") = "YES"
                Else
                    Session("ProductCode") = "-1"
                    ViewState("CS_CREATED") = "NO"
                End If

                If (UICulture = "Arabic") Then
                    ddlCountry.DataTextField = "NameA"
                    abc.Style.Add("direction", "rtl")
                    abc.Style.Add("text-align", "right")
                    ddlProd.DataTextField = "NameA"
                    ddlProd.DataBind()
                    ddlProd.Items.Insert(0, "Select")
                    'options.Style.Add("float", "right")
                    rbtn1.Style.Add("float", "right")
                    rbtn2.Style.Add("float", "right")
                    rbtn3.Style.Add("float", "right")
                    rbtn4.Style.Add("float", "right")

                    rbtn1.Style.Add("direction", "rtl")
                    rbtn1.Style.Add("text-align", "right")
                    rbtn2.Style.Add("direction", "rtl")
                    rbtn2.Style.Add("text-align", "right")
                    rbtn3.Style.Add("direction", "rtl")
                    rbtn3.Style.Add("text-align", "right")
                    rbtn4.Style.Add("direction", "rtl")
                    rbtn4.Style.Add("text-align", "right")
                    'options.Style.Add("direction", "rtl")
                    'options.Style.Add("text-align", "right")

                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    Panel2.Style.Add("direction", "rtl")
                    Panel2.Style.Add("text-align", "right")
                    Panel3.Style.Add("direction", "rtl")
                    Panel3.Style.Add("text-align", "right")
                    Panel4.Style.Add("direction", "rtl")
                    Panel4.Style.Add("text-align", "right")
                Else
                    ddlCountry.DataTextField = "NameE"
                    ddlProd.DataTextField = "NameE"
                    ddlProd.DataBind()
                    ddlProd.Items.Insert(0, "Select")
                End If
                ddlCountry.DataBind()
                ddlCountry.SelectedIndex = 0
                ddlProd.SelectedIndex = 0

                LoadGrids()
                LoadHistoryGrid()
                LoadDocGrid()
                LoadDropDowns()
                SetEnvironmentAccordingtoLang()
                'LoadOtherData()
                LoadRefData()

                Dim strCode As String = String.Empty
                If (Request.QueryString("Code") IsNot Nothing) Then
                    strCode = Request.QueryString.Get("Code")

                    Dim sqlConnection As New SqlConnection()
                    Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
                    sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                    sqlConnection.Open()

                    sqlCommand.CommandText = "SELECT FileName FROM ProductCrewStructure WHERE ProductID = '" + strCode + "' "
                    Dim strFileName As String = sqlCommand.ExecuteScalar().ToString()
                    imgCrew.ImageUrl = "~/Upload/CrewImages/" + strFileName

                    sqlConnection.Close()
                End If

            Else
                Response.Redirect("../Login.aspx")
            End If

            ViewState("ImageFileName") = ""
        End If
    End Sub
    Private Sub LoadGrids()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt, dt1 As New System.Data.DataTable()
        If (Session("Labourdt") IsNot Nothing) Then
            grdLabour.DataSource = Session("Labourdt")
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False

            dt = Session("Labourdt")
            If (dt.Rows.Count > 1) Then
                If (dt.Rows(1).Item(9).ToString().Length > 0) Then
                    ddlCountry.SelectedValue = dt.Rows(1).Item(9).ToString()
                End If
                'txtMatCost.Text = dt.Rows(1).Item(10).ToString()
            End If
        Else
            dt = objSqlConfig.GetProductCrewStructure_LabourByCompany(Session("ProductCode"), Session("CompanyID"))
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = "-1"
            dr("Code") = "000"
            dr("ProfE") = ""
            dr("ProfA") = ""
            dr("Man_Day") = 0

            dt.Rows.InsertAt(dr, 0)
            grdLabour.DataSource = dt
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False
            Session("Labourdt") = dt

            If (dt.Rows.Count > 1) Then
                ddlCountry.SelectedValue = dt.Rows(1).Item(9).ToString()
                'txtMatCost.Text = dt.Rows(1).Item(10).ToString()
            End If
        End If

        If (Session("Equipdt") IsNot Nothing) Then
            grdEquip.DataSource = Session("Equipdt")
            grdEquip.DataBind()
            grdEquip.Rows(0).Visible = False

            If (dt1.Rows.Count > 1) Then
                'txtMatCost.Text = dt1.Rows(1).Item(9).ToString()
            End If
        Else
            dt1 = objSqlConfig.GetProductCrewStructure_EquipmentByCompany(Session("ProductCode"), Session("CompanyID"))
            Dim dr1 As DataRow = dt1.NewRow()
            dr1("ID") = "-1"
            dr1("Code") = "000"
            dr1("DescE") = ""
            dr1("DescA") = ""
            dr1("Man_Day") = 0

            dt1.Rows.InsertAt(dr1, 0)
            grdEquip.DataSource = dt1
            grdEquip.DataBind()
            grdEquip.Rows(0).Visible = False
            Session("Equipdt") = dt1

            'If (dt1.Rows.Count > 1) Then
            '    txtMatCost.Text = dt1.Rows(1).Item(9).ToString()
            'End If
        End If
    End Sub
    Private Sub LoadHistoryGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New System.Data.DataTable()

        dt = objSqlConfig.GetProductivityHistoryByCompany(txtCode.Text.Trim(), Session("CompanyID"))
        Dim dr As DataRow = dt.NewRow()
        dr("ID") = "-1"
        dr("Code") = "000"
        dr("DateTime") = Now.ToString("yyyyMMddHHmmss")
        dr("DO") = 0

        dt.Rows.InsertAt(dr, 0)
        grdHistory.DataSource = dt
        grdHistory.DataBind()
        grdHistory.Rows(0).Visible = False
    End Sub
    Private Sub LoadDropDowns()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim ddlLabour As DropDownList = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList)
        If (ddlLabour IsNot Nothing) Then
            ddlLabour.DataSource = objSqlConfig.GetLaboursByCountryAndCompany("", ddlCountry.SelectedValue, Session("CompanyID"))
            ddlLabour.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlLabour.DataTextField = "ProfA"
            Else
                ddlLabour.DataTextField = "ProfE"
            End If
            ddlLabour.DataBind()
        End If


        Dim ddlEquip As DropDownList = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList)
        If (ddlEquip IsNot Nothing) Then
            ddlEquip.DataSource = objSqlConfig.GetEquipmentsByCompany2("", Session("CompanyID"))
            ddlEquip.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlEquip.DataTextField = "DescA"
            Else
                ddlEquip.DataTextField = "DescE"
            End If
            ddlEquip.DataBind()
        End If
    End Sub
    Protected Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCountry.SelectedIndexChanged
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim ddlLabour As DropDownList = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList)
        If (ddlLabour IsNot Nothing) Then
            ddlLabour.DataSource = objSqlConfig.GetLaboursByCountryAndCompany("", ddlCountry.SelectedValue, Session("CompanyID"))
            ddlLabour.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlLabour.DataTextField = "ProfA"
            Else
                ddlLabour.DataTextField = "ProfE"
            End If
            ddlLabour.DataBind()
        End If
    End Sub
    Private Sub SetEnvironmentAccordingtoLang()
        If (UICulture = "Arabic") Then
            For i As Integer = 0 To grdLabour.Rows.Count - 1
                If (grdLabour.Rows(i).RowState = DataControlRowState.Normal Or grdLabour.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdLabour.Rows(i).FindControl("l1"), Label).Visible = False
                End If
            Next
            For i As Integer = 0 To grdEquip.Rows.Count - 1
                If (grdEquip.Rows(i).RowState = DataControlRowState.Normal Or grdEquip.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdEquip.Rows(i).FindControl("l11"), Label).Visible = False
                End If
            Next
        Else
            For i As Integer = 0 To grdLabour.Rows.Count - 1
                If (grdLabour.Rows(i).RowState = DataControlRowState.Normal Or grdLabour.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdLabour.Rows(i).FindControl("l2"), Label).Visible = False
                End If
            Next
            For i As Integer = 0 To grdEquip.Rows.Count - 1
                If (grdEquip.Rows(i).RowState = DataControlRowState.Normal Or grdEquip.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdEquip.Rows(i).FindControl("l12"), Label).Visible = False
                End If
            Next
        End If
    End Sub
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs)
        Dim dt As System.Data.DataTable = Session("Labourdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                If (dt.Rows(i).Item(1).ToString() = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next

        If (iCount = 0) Then
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = "-1"
            dr("Code") = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedValue
            dr("ProfE") = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedItem.Text
            dr("ProfA") = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedItem.Text
            dr("Man_Day") = TryCast(grdLabour.FooterRow.FindControl("txtMDF"), TextBox).Text.Trim()
            dr("Nationality") = ddlCountry.SelectedValue.ToString()

            dt.Rows.Add(dr)
            grdLabour.DataSource = dt
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False

            Session("Labourdt") = dt
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            'CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(2);", True)
        End If
    End Sub
    Protected Sub LinkButton11_Click(sender As Object, e As EventArgs)
        Dim dt As System.Data.DataTable = Session("Equipdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                If (dt.Rows(i).Item(1).ToString() = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next

        If (iCount = 0) Then
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = "-1"
            dr("Code") = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedValue
            dr("DescE") = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedItem.Text
            dr("DescA") = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedItem.Text
            dr("Man_Day") = TryCast(grdEquip.FooterRow.FindControl("txtMDF1"), TextBox).Text.Trim()

            dt.Rows.Add(dr)
            grdEquip.DataSource = dt
            grdEquip.DataBind()
            grdEquip.Rows(0).Visible = False

            Session("Equipdt") = dt
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            'CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(3);", True)
        End If
    End Sub
    Protected Sub grdLabour_RowCancelingEdit(sender As Object, e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdLabour.RowCancelingEdit
        e.Cancel = True
        grdLabour.EditIndex = -1
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdLabour_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdLabour.RowEditing
        grdLabour.EditIndex = e.NewEditIndex
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdLabour_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdLabour.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow And grdLabour.EditIndex = e.Row.RowIndex) Then
            Dim ddlLabour As DropDownList = DirectCast(e.Row.FindControl("ddlLabFE"), DropDownList)
            Dim objSqlConfig As New ConfigClassSqlServer()
            ddlLabour.DataSource = objSqlConfig.GetLaboursByCountryAndCompany("", ddlCountry.SelectedValue, Session("CompanyID"))
            ddlLabour.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlLabour.DataTextField = "ProfA"
            Else
                ddlLabour.DataTextField = "ProfE"
            End If
            ddlLabour.DataBind()
            ddlLabour.SelectedValue = DirectCast(e.Row.FindControl("lblLabEdit"), Label).Text
        End If
    End Sub
    Protected Sub grdLabour_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdLabour.RowUpdating
        Dim dt As System.Data.DataTable = Session("Labourdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (i <> e.RowIndex) Then
                If (dt.Rows(i).Item(1).ToString() = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next

        If (iCount = 0) Then
            dt.Rows(e.RowIndex).Item(1) = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedValue
            dt.Rows(e.RowIndex).Item(2) = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(3) = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(4) = CType(grdLabour.Rows(e.RowIndex).FindControl("txtMD"), TextBox).Text.Trim()

            grdLabour.EditIndex = -1
            Session("Labourdt") = dt
            LoadGrids()
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            'CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(2);", True)
        End If
    End Sub
    Protected Sub grdLabour_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdLabour.RowDeleting
        Dim dt As System.Data.DataTable = Session("Labourdt")
        dt.Rows.RemoveAt(e.RowIndex)
        Session("Labourdt") = dt
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
        'CalculateValues()
    End Sub
    Protected Sub grdLabour_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdLabour.PageIndexChanging
        grdLabour.PageIndex = e.NewPageIndex
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdEquip_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdEquip.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow And grdEquip.EditIndex = e.Row.RowIndex) Then
            Dim ddlEquip As DropDownList = DirectCast(e.Row.FindControl("ddlEquipFE"), DropDownList)
            Dim objSqlConfig As New ConfigClassSqlServer()
            ddlEquip.DataSource = objSqlConfig.GetEquipmentsByCompany2("", Session("CompanyID"))
            ddlEquip.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlEquip.DataTextField = "DescA"
            Else
                ddlEquip.DataTextField = "DescE"
            End If
            ddlEquip.DataBind()
            ddlEquip.SelectedValue = DirectCast(e.Row.FindControl("lblEqEdit"), Label).Text
        End If
    End Sub
    Protected Sub grdEquip_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdEquip.RowEditing
        grdEquip.EditIndex = e.NewEditIndex
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdEquip_RowCancelingEdit(sender As Object, e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdEquip.RowCancelingEdit
        e.Cancel = True
        grdEquip.EditIndex = -1
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdEquip_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdEquip.RowDeleting
        Dim dt As System.Data.DataTable = Session("Equipdt")
        dt.Rows.RemoveAt(e.RowIndex)
        Session("Equipdt") = dt
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
        'CalculateValues()
    End Sub
    Protected Sub grdEquip_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdEquip.RowUpdating
        Dim dt As System.Data.DataTable = Session("Equipdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (i <> e.RowIndex) Then
                If (dt.Rows(i).Item(1).ToString() = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next
        If (iCount = 0) Then
            dt.Rows(e.RowIndex).Item(1) = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedValue
            dt.Rows(e.RowIndex).Item(2) = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(3) = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(4) = CType(grdEquip.Rows(e.RowIndex).FindControl("txtMD1"), TextBox).Text.Trim()

            grdEquip.EditIndex = -1
            Session("Equipdt") = dt
            LoadGrids()
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            'CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(2);", True)
        End If
    End Sub
    Protected Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        If (ddlCountry.Items.Count > 0) Then
            ddlCountry.SelectedIndex = 0
        End If

        'txtDO.Text = ""
        '        LoadOtherData()
        Session("Labourdt") = Nothing
        Session("Equipdt") = Nothing

        'If (ddlProd.SelectedIndex > 0) Then
        '    'txtDO.Text = ""
        '    lblDO.Text = ""
        '    lblMO.Text = ""
        '    lblHO.Text = ""
        '    lblEH.Text = ""
        'End If

        Session("ProductCode") = txtCode.Text.Trim()

        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
        'LoadOtherData()

        ddlProd.SelectedIndex = 0
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim dt As System.Data.DataTable = Session("Labourdt")
        Dim dt1 As System.Data.DataTable = Session("Equipdt")
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        If (dt.Rows.Count = 1 And dt1.Rows.Count = 1) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(9);", True)
            Return
        End If

        Dim strCode As String = String.Empty
        If (Request.QueryString("Code") IsNot Nothing) Then
            strCode = Request.QueryString.Get("Code")
        Else
            strCode = txtCode.Text.Trim()
        End If

        sqlCommand.CommandText = "DELETE FROM ProductCrewStructure WHERE ProductID = '" + strCode + "' AND CompID = '" + Session("CompanyID") + "' "
        sqlCommand.ExecuteNonQuery()

        For i As Integer = 1 To dt.Rows.Count - 1
            If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                sqlCommand.CommandText = "SELECT COUNT(*) FROM ProductCrewStructure WHERE ProductID = '" + strCode + "' AND Code = '" + dt.Rows(i).Item(1).ToString() + "' AND CompID = '" + Session("CompanyID") + "' "
                If (sqlCommand.ExecuteScalar().ToString() = "0") Then
                    'sqlCommand.CommandText = "INSERT INTO ProductCrewStructure(ProductID,Code,Man_Day,DailyOutput,HourlyOutput, ManHour, EquipHour, Lab_Equip) VALUES('" + txtCode.Text.Trim() + "', '" + dt.Rows(i).Item(1).ToString() + "', '" + dt.Rows(i).Item(4).ToString() + "', '" + lblDO.Text.Trim() + "', '" + lblHO.Text.Trim() + "', '" + lblMO.Text.Trim() + "', '" + lblEH.Text.Trim() + "', 1)"
                    sqlCommand.CommandText = "INSERT INTO ProductCrewStructure(ProductID,Code,Man_Day,DailyOutput,HourlyOutput, ManHour, EquipHour, Lab_Equip, FileName,CompID) VALUES('" + txtCode.Text.Trim() + "', '" + dt.Rows(i).Item(1).ToString() + "', '" + dt.Rows(i).Item(4).ToString() + "', 0, 0, 0, 0, 1, '" + ViewState("ImageFileName").ToString() + "', '" + Session("CompanyID") + "' )"
                    sqlCommand.ExecuteNonQuery()
                    ViewState("CS_CREATED") = "YES"
                End If
            End If
        Next

        For i As Integer = 1 To dt1.Rows.Count - 1
            If (dt1.Rows(i).RowState <> DataRowState.Deleted) Then
                sqlCommand.CommandText = "SELECT COUNT(*) FROM ProductCrewStructure WHERE ProductID = '" + strCode + "' AND Code = '" + dt1.Rows(i).Item(1).ToString() + "' AND CompID = '" + Session("CompanyID") + "' "
                If (sqlCommand.ExecuteScalar().ToString() = "0") Then
                    'sqlCommand.CommandText = "INSERT INTO ProductCrewStructure(ProductID,Code,Man_Day,DailyOutput,HourlyOutput, ManHour, EquipHour, Lab_Equip) VALUES('" + txtCode.Text.Trim() + "', '" + dt1.Rows(i).Item(1).ToString() + "', '" + dt1.Rows(i).Item(4).ToString() + "', '" + lblDO.Text.Trim() + "', '" + lblHO.Text.Trim() + "', '" + lblMO.Text.Trim() + "', '" + lblEH.Text.Trim() + "', 2)"
                    sqlCommand.CommandText = "INSERT INTO ProductCrewStructure(ProductID,Code,Man_Day,DailyOutput,HourlyOutput, ManHour, EquipHour, Lab_Equip, FileName,CompID) VALUES('" + txtCode.Text.Trim() + "', '" + dt1.Rows(i).Item(1).ToString() + "', '" + dt1.Rows(i).Item(4).ToString() + "', 0, 0, 0, 0, 2, '" + ViewState("ImageFileName").ToString() + "', '" + Session("CompanyID") + "' )"
                    sqlCommand.ExecuteNonQuery()
                    ViewState("CS_CREATED") = "YES"
                End If
            End If
        Next

        If (strCode <> txtCode.Text.Trim()) Then
            sqlCommand.CommandText = "UPDATE MainHierarchy1 SET CrewCode = '" + txtCode.Text.Trim() + "' WHERE CrewCode = '" + strCode + "' AND CompID = '" + Session("CompanyID") + "' "
            sqlCommand.ExecuteNonQuery()
        End If

        'sqlCommand.CommandText = "UPDATE ProductDetails SET ManHour = '" + lblMO.Text.Trim() + "', EquipHour ='" + lblEH.Text.Trim() + "' WHERE ID = '" + txtCode.Text + "' "
        'sqlCommand.ExecuteNonQuery()

        'Session("ManHours") = lblMO.Text.Trim()
        'Session("EquipHours") = lblEH.Text.Trim()

        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(1);", True)
    End Sub
    'Protected Sub txtDO_TextChanged(sender As Object, e As EventArgs) Handles txtDO.TextChanged
    '    If (IsNumeric(lblDO.Text)) Then
    '        CalculateValues()
    '    End If
    'End Sub
    'Private Sub CalculateValues()
    '    Dim dt As DataTable = Session("Labourdt")
    '    Dim dt1 As DataTable = Session("Equipdt")
    '    Dim dSumCSMan As Double = 0
    '    Dim dSumCSEquip As Double = 0

    '    If (IsNumeric(lblDO.Text)) Then
    '        lblHO.Text = Math.Round((Double.Parse(lblDO.Text) / 8), 4)
    '        For i As Integer = 1 To dt.Rows.Count - 1
    '            If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
    '                If (dt.Rows(i).Item(4).ToString().Length > 0 And IsNumeric(dt.Rows(i).Item(4).ToString())) Then
    '                    dSumCSMan = dSumCSMan + Double.Parse(dt.Rows(i).Item(4).ToString())
    '                End If
    '            End If
    '        Next
    '        lblMO.Text = Math.Round((dSumCSMan / Double.Parse(lblHO.Text)), 4)

    '        For i As Integer = 1 To dt1.Rows.Count - 1

    '            If (dt1.Rows(i).RowState <> DataRowState.Deleted) Then
    '                If (dt1.Rows(i).Item(4).ToString().Length > 0 And IsNumeric(dt1.Rows(i).Item(4).ToString())) Then
    '                    dSumCSEquip = dSumCSEquip + Double.Parse(dt1.Rows(i).Item(4).ToString())
    '                End If
    '            End If
    '        Next
    '        lblEH.Text = Math.Round((dSumCSEquip / Double.Parse(lblHO.Text)), 4)
    '    End If
    'End Sub
    'Private Sub LoadOtherData()
    '    Dim dt As DataTable = Session("Labourdt")
    '    Dim dt1 As DataTable = Session("Equipdt")

    '    If (dt.Rows.Count > 1) Then
    '        lblDO.Text = dt.Rows(1).Item(5).ToString()
    '        lblHO.Text = dt.Rows(1).Item(6).ToString()
    '        lblMO.Text = dt.Rows(1).Item(7).ToString()
    '        lblEH.Text = dt.Rows(1).Item(8).ToString()
    '    ElseIf dt1.Rows.Count > 1 Then
    '        lblDO.Text = dt1.Rows(1).Item(5).ToString()
    '        lblHO.Text = dt1.Rows(1).Item(6).ToString()
    '        lblMO.Text = dt1.Rows(1).Item(7).ToString()
    '        lblEH.Text = dt1.Rows(1).Item(8).ToString()
    '    End If
    'End Sub
    Protected Sub ddlProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlProd.SelectedIndexChanged
        If (ddlProd.SelectedIndex > 0) Then
            Dim objSqlConfig As New ConfigClassSqlServer()
            Dim dt, dt1 As New System.Data.DataTable()

            dt = objSqlConfig.GetProductivityLaboursByCode(ddlProd.SelectedValue, Session("CompanyID"))
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = "-1"
            dr("Code") = "000"
            dr("ProfE") = ""
            dr("ProfA") = ""
            dr("Man_Day") = 0

            dt.Rows.InsertAt(dr, 0)
            grdLabour.DataSource = dt
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False
            Session("Labourdt") = dt

            If (dt.Rows.Count > 1) Then
                If (dt.Rows(1).Item(9).ToString().Length > 0) Then
                    ddlCountry.SelectedValue = dt.Rows(1).Item(9).ToString()
                End If
                'lblDO.Text = dt.Rows(1).Item(5).ToString()
                'txtMatCost.Text = dt.Rows(1).Item(10).ToString()
            End If

            dt1 = objSqlConfig.GetProductivityEquipmentsByCode(ddlProd.SelectedValue, Session("CompanyID"))
            Dim dr1 As DataRow = dt1.NewRow()
            dr1("ID") = "-1"
            dr1("Code") = "000"
            dr1("DescE") = ""
            dr1("DescA") = ""
            dr1("Man_Day") = 0

            dt1.Rows.InsertAt(dr1, 0)
            grdEquip.DataSource = dt1
            grdEquip.DataBind()
            grdEquip.Rows(0).Visible = False

            If (dt1.Rows.Count > 1) Then
                'lblDO.Text = dt1.Rows(1).Item(5).ToString()
                'txtMatCost.Text = dt.Rows(1).Item(6).ToString()
            End If
            Session("Equipdt") = dt1

            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            'CalculateValues()
        End If
    End Sub
    'Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
    '    If (Session("CameFromCES") IsNot Nothing) Then
    '        If (Session("CameFromCES") = "1") Then
    '            Session("CameFromCES") = Nothing
    '            Response.Redirect("../CES/Final_ProductList.aspx")
    '        End If
    '    End If
    'End Sub
    Protected Sub rbtn1_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rbtn1.CheckedChanged
        Panel1.Visible = False
        Panel2.Visible = False
        Panel3.Visible = False
        Panel4.Visible = False
        btnSave.Visible = False
        btnNew.Visible = False
        Button8.Visible = False
        'Button2.Visible = False
        If (rbtn1.Checked = True) Then
            Panel3.Visible = True
            Panel3.Attributes.Add("style", "display:''")
        End If
    End Sub
    Protected Sub rbtn4_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rbtn4.CheckedChanged
        Panel1.Visible = False
        Panel2.Visible = False
        Panel3.Visible = False
        Panel4.Visible = False
        If (rbtn4.Checked = True) Then
            Panel4.Visible = True
            btnSave.Visible = True
            btnNew.Visible = True
            Button8.Visible = True
            'Button2.Visible = True
        End If
    End Sub
    Protected Sub rbtn2_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rbtn2.CheckedChanged
        Panel1.Visible = False
        Panel2.Visible = False
        Panel3.Visible = False
        Panel4.Visible = False
        btnSave.Visible = False
        Button8.Visible = False
        btnNew.Visible = False
        'Button2.Visible = False
        If (rbtn2.Checked = True) Then
            Panel1.Visible = True
        End If
    End Sub
    Protected Sub rbtn3_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rbtn3.CheckedChanged
        Panel1.Visible = False
        Panel2.Visible = False
        Panel3.Visible = False
        Panel4.Visible = False
        btnSave.Visible = False
        Button8.Visible = False
        btnNew.Visible = False
        'Button2.Visible = False
        If (rbtn3.Checked = True) Then
            Panel2.Visible = True
        End If
    End Sub
    Protected Sub btnSaveHistory_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        If (rbtn2.Checked And ViewState("CS_CREATED") = "YES") Then
            Dim sqlConnection As New SqlConnection()
            Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
            sqlConnection.Open()

            sqlCommand.CommandText = "INSERT INTO ProductivityHistory(Code,DateTime,DO,CompID) VALUES('" + txtCode.Text.Trim() + "', '" + Now.ToString("yyyyMMddHHmmss") + "', '" + TryCast(grdHistory.FooterRow.FindControl("txtDOF"), TextBox).Text + "', '" + Session("CompanyID") + "' )"
            sqlCommand.ExecuteNonQuery()

            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(1);", True)
            LoadHistoryGrid()
        ElseIf (rbtn2.Checked = True) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(7);", True)
        End If
    End Sub
    Protected Sub grdHistory_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdHistory.RowDeleting
        Dim objSqlConfig As New ConfigClassSqlServer()
        objSqlConfig.DeleteHistoryByCompany(grdHistory.DataKeys(e.RowIndex).Value.ToString(), Session("CompanyID"))
        LoadHistoryGrid()
    End Sub
    Protected Sub grdHistory_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdHistory.RowEditing
        grdHistory.EditIndex = e.NewEditIndex
        LoadHistoryGrid()
    End Sub
    Protected Sub grdHistory_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdHistory.RowCancelingEdit
        e.Cancel = True
        grdHistory.EditIndex = -1
        LoadHistoryGrid()
    End Sub
    Protected Sub grdHistory_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdHistory.RowUpdating
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        sqlCommand.CommandText = "UPDATE ProductivityHistory Set DateTime = '" + Now.ToString("yyyyMMddHHmmss") + "', DO = '" + CType(grdHistory.Rows(e.RowIndex).FindControl("txtDOE"), TextBox).Text.Trim() + "' WHERE ID = '" + grdHistory.DataKeys(e.RowIndex).Value.ToString() + "' "
        sqlCommand.ExecuteNonQuery()

        grdHistory.EditIndex = -1
        LoadHistoryGrid()
    End Sub
    Private Sub LoadDocGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New System.Data.DataTable()
        dt = objSqlConfig.GetProductivityDocsByCompany(txtCode.Text.Trim(), Session("CompanyID"))

        grdDoc.DataSource = dt
        grdDoc.DataBind()
    End Sub
    Protected Sub Button4_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button4.Click
        FileUpload1.Attributes.Clear()
    End Sub
    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button3.Click
        If (rbtn1.Checked And ViewState("CS_CREATED") = "YES") Then
            Dim fileName As String = ""
            If (FileUpload1.HasFile) Then
                Dim fileExt As String = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower()
                Dim sDateTimeNow As String = DateTime.Now.ToString("yyyyMMddHHmmss")
                If (fileExt = ".doc" Or fileExt = ".docx" Or fileExt = ".pdf" Or fileExt = ".xls" Or fileExt = ".xlsx" Or fileExt = ".png" Or fileExt = ".jpeg" Or fileExt = ".img" Or fileExt = ".gif" Or fileExt = ".bmp") Then
                    fileName = System.IO.Path.GetFileNameWithoutExtension(FileUpload1.FileName)
                    fileName = fileName + "_" + sDateTimeNow + fileExt

                    FileUpload1.SaveAs(Server.MapPath("../Upload/Docs/" + fileName))
                Else
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
                    Return
                End If

                'ViewDocument(Server.MapPath("../Upload/Docs/" + fileName))

                Dim sqlConnection As New SqlConnection()
                Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
                sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                sqlConnection.Open()

                sqlCommand.CommandText = "INSERT INTO ProductivityDocuments(Code, Name, DateTime, Ext, CompID) VALUES('" + txtCode.Text.Trim() + "', '" + fileName + "',  '" + sDateTimeNow + "', '" + fileExt + "', '" + Session("CompanyID") + "' ) "
                sqlCommand.ExecuteNonQuery()
                LoadDocGrid()

                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('5');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('6');", True)
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('7');", True)
        End If
    End Sub
    Protected Sub grdDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles grdDoc.SelectedIndexChanged

        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "document.getElementById('ctl00_MainContent_Button100').click();", True)
        'ViewDocument(Server.MapPath("../Upload/Docs/" + grdDoc.Rows(grdDoc.SelectedIndex).Cells(0).Text))
        'If (grdDoc.DataKeys(grdDoc.SelectedIndex).Values(2).ToString() = ".doc" Or grdDoc.DataKeys(grdDoc.SelectedIndex).Values(2).ToString() = ".docx") Then
        '    Dim docApp As Microsoft.Office.Interop.Word.Application
        '    Dim docBook As Microsoft.Office.Interop.Word.Document
        '    docApp = CType(CreateObject("Word.Application"), Microsoft.Office.Interop.Word.Application)
        '    docApp.Visible = True
        '    docBook = docApp.Documents.Open(MapPath("../Upload/Docs/" + grdDoc.Rows(grdDoc.SelectedIndex).Cells(0).Text))
        '    'Dim File = New FileInfo(MapPath("../Upload/Docs/" + grdDoc.Rows(grdDoc.SelectedIndex).Cells(0).Text))
        '    'Response.ClearContent()
        '    'Response.AddHeader("Content-Disposition", "inline;filename=" + File.Name)
        '    'Response.AddHeader("Content-Length", File.Length.ToString())
        '    'Response.ContentType = "application/msword"
        '    'Response.TransmitFile(File.FullName)
        '    'Response.End()
        'ElseIf (grdDoc.DataKeys(grdDoc.SelectedIndex).Values(2).ToString() = ".xls" Or grdDoc.DataKeys(grdDoc.SelectedIndex).Values(2).ToString() = ".xlsx") Then
        '    'Dim File = New FileInfo(MapPath("../Upload/Docs/" + grdDoc.Rows(grdDoc.SelectedIndex).Cells(0).Text))
        '    'Response.ClearContent()
        '    'Response.AddHeader("Content-Disposition", "inline;filename=" + File.Name)
        '    'Response.AddHeader("Content-Length", File.Length.ToString())
        '    'Response.ContentType = "application/msexcel"
        '    'Response.TransmitFile(File.FullName)
        '    'Response.End()
        '    Dim xlApp As Microsoft.Office.Interop.Excel.Application
        '    Dim xlBook As Microsoft.Office.Interop.Excel.Workbook
        '    xlApp = CType(CreateObject("Excel.Application"), Microsoft.Office.Interop.Excel.Application)
        '    xlApp.Visible = True
        '    xlBook = xlApp.Workbooks.Open(MapPath("../Upload/Docs/" + grdDoc.Rows(grdDoc.SelectedIndex).Cells(0).Text))
        'End If
    End Sub
    Private Sub LoadRefData()
        Dim objSqlConfig As New ConfigClassSqlServer()
        grdRef.DataSource = objSqlConfig.GetReferenceDataByCompany(txtSearch.Text.Trim(), txtCode.Text.Trim(), Session("CompanyID"))
        grdRef.DataBind()
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        LoadRefData()
    End Sub
    Protected Sub grdDoc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdDoc.PageIndexChanging
        grdDoc.PageIndex = e.NewPageIndex
        LoadDocGrid()
    End Sub
    Protected Sub grdRef_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdRef.PageIndexChanging
        grdRef.PageIndex = e.NewPageIndex
        LoadRefData()
    End Sub
    Protected Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        'Session("Equipdt") = Nothing
        'Session("Labourdt") = Nothing
        'Session("ProductCode") = "-11"

        'ViewState("CS_CREATED") = "NO"

        'If (ddlCountry.Items.Count > 0) Then
        '    ddlCountry.SelectedIndex = 0
        'End If
        'ddlProd.SelectedIndex = 0
        'txtCode.Text = ""


        'txtDO.Text = ""
        'lblHO.Text = ""
        'lblMO.Text = ""
        'lblEH.Text = ""

        Response.Redirect("Add_CrewStructure.aspx")

        'LoadGrids()
        'LoadHistoryGrid()
        'LoadDocGrid()
        'LoadDropDowns()
        'SetEnvironmentAccordingtoLang()
        ''LoadOtherData()
        'LoadRefData()
        'txtCode.Focus()

        'txtCode.ReadOnly = False
    End Sub
    Private Sub ViewDocument(strFile As String)
        Dim content As Byte() = File.ReadAllBytes(strFile)
        Dim context As HttpContext = HttpContext.Current
        Dim strAttachment As String = "attachment; filename=" + strFile
        Dim fileExt As String = System.IO.Path.GetExtension(strFile).ToLower()
        If (fileExt = ".xls" Or fileExt = ".xlsx") Then
            context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        ElseIf (fileExt = ".doc" Or fileExt = ".docx") Then
            context.Response.ContentType = "application/ms-word"
        ElseIf (fileExt = ".pdf") Then
            context.Response.ContentType = "application/pdf"
        ElseIf (fileExt = ".png") Then
            context.Response.ContentType = "image/png"
        ElseIf (fileExt = ".gif") Then
            context.Response.ContentType = "image/gif"
        ElseIf (fileExt = ".bmp") Then
            context.Response.ContentType = "image/bmp"
        End If

        context.Response.BinaryWrite(content)
        context.Response.AppendHeader("Content-Disposition", strAttachment)
        context.Response.End()
    End Sub
    'Protected Sub Upload(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim missingType As Object = Type.Missing
    '    Dim readOnlyObject As Object = True
    '    Dim isVisible As Object = False
    '    Dim documentFormat As Object = 8
    '    Dim randomName As String = DateTime.Now.Ticks.ToString
    '    Dim htmlFilePath As Object = (Server.MapPath("~/Upload/Docs") _
    '                & (randomName + ".htm"))
    '    Dim directoryPath As String = (Server.MapPath("~/Upload/Docs") _
    '                & (randomName + "_files"))

    '    'Upload the word document and save to Temp folder
    '    FileUpload1.SaveAs((Server.MapPath("~/Upload/Docs/") + Path.GetFileName(FileUpload1.PostedFile.FileName)))
    '    Dim fileName As Object = FileUpload1.PostedFile.FileName


    '    Dim applicationclass As Microsoft.Office.Interop.Word.Application = New Microsoft.Office.Interop.Word.Application
    '    applicationclass.Documents.Open(Server.MapPath("~/Upload/Docs/") + fileName, readOnlyObject, missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType, isVisible, missingType, missingType, missingType, missingType, missingType)



    '    'Dim applicationclass As Microsoft.Office.Interop.Word.Application
    '    'applicationclass = CType(CreateObject("Word.Application"), Microsoft.Office.Interop.Word.Application) 'applicationclass.Documents.Open(fileName, readOnlyObject, missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType, isVisible, missingType, missingType, missingType, missingType, missingType)
    '    'applicationclass.Documents.Open(fileName, readOnlyObject, missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType, isVisible, missingType, missingType, missingType, missingType, missingType)
    '    applicationclass.Visible = False
    '    Dim document As Document = applicationclass.ActiveDocument

    '    'Save the word document as HTML file
    '    document.SaveAs(htmlFilePath, documentFormat, missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType)

    '    'Close the word document
    '    document.Close(missingType, missingType, missingType)

    '    'Delete the Uploaded Word File
    '    File.Delete((Server.MapPath("~/Upload/Docs/") + Path.GetFileName(FileUpload1.PostedFile.FileName)))
    '    Dim bytes() As Byte
    '    Dim fs As FileStream = New FileStream(htmlFilePath.ToString, FileMode.Open, FileAccess.Read)
    '    Dim reader As BinaryReader = New BinaryReader(fs)
    '    bytes = reader.ReadBytes(CType(fs.Length, Integer))
    '    fs.Close()
    '    Response.BinaryWrite(bytes)
    '    Response.Flush()
    '    System.IO.File.Delete(htmlFilePath.ToString)
    '    Response.End()
    'End Sub
    'Protected Sub UploadExcel(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim missingType As Object = Type.Missing
    '    Dim readOnlyObject As Object = True
    '    Dim isVisible As Object = False
    '    Dim documentFormat As Object = 8
    '    Dim randomName As String = DateTime.Now.Ticks.ToString
    '    Dim htmlFilePath As Object = (Server.MapPath("~/Upload/Docs") _
    '                & (randomName + ".htm"))
    '    Dim directoryPath As String = (Server.MapPath("~/Upload/Docs") _
    '                & (randomName + "_files"))

    '    'Upload the word document and save to Temp folder
    '    FileUpload1.SaveAs((Server.MapPath("~/Upload/Docs/") + Path.GetFileName(FileUpload1.PostedFile.FileName)))
    '    Dim fileName As String = FileUpload1.PostedFile.FileName


    '    'Dim applicationclass As Microsoft.Office.Interop.Excel.Application = New Microsoft.Office.Interop.Excel.Application
    '    'applicationclass.Workbooks.Open(Server.MapPath("~/Upload/Docs/") + fileName, readOnlyObject, missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType, isVisible, missingType, missingType, missingType, missingType)



    '    ''Dim applicationclass As Microsoft.Office.Interop.Word.Application
    '    ''applicationclass = CType(CreateObject("Word.Application"), Microsoft.Office.Interop.Word.Application) 'applicationclass.Documents.Open(fileName, readOnlyObject, missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType, isVisible, missingType, missingType, missingType, missingType, missingType)
    '    ''applicationclass.Documents.Open(fileName, readOnlyObject, missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType, isVisible, missingType, missingType, missingType, missingType, missingType)
    '    'applicationclass.Visible = False
    '    'Dim document As Microsoft.Office.Interop.Excel.Workbook = applicationclass.ActiveWorkbook

    '    'Dim sheet As Microsoft.Office.Interop.Excel.Worksheet = document.Worksheets(1)
    '    'sheet.SaveAs(htmlFilePath, )

    '    ''Save the word document as HTML file
    '    ''document.SaveAs(htmlFilePath, Microsoft.Office.Interop.Excel.XlFileFormat.xlHtml) ', missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType, missingType)

    '    ''Close the word document
    '    'document.Close(missingType, missingType, missingType)

    '    ''Delete the Uploaded Word File
    '    'File.Delete((Server.MapPath("~/Upload/Docs/") + Path.GetFileName(FileUpload1.PostedFile.FileName)))
    '    'Dim bytes() As Byte
    '    'Dim fs As FileStream = New FileStream(htmlFilePath.ToString, FileMode.Open, FileAccess.Read)
    '    'Dim reader As BinaryReader = New BinaryReader(fs)
    '    'bytes = reader.ReadBytes(CType(fs.Length, Integer))
    '    'fs.Close()
    '    'Response.BinaryWrite(bytes)
    '    'Response.Flush()
    '    'System.IO.File.Delete(htmlFilePath.ToString)
    '    'Response.End()




    '    Dim content As Byte() = File.ReadAllBytes((Server.MapPath("~/Upload/Docs/") + Path.GetFileName(FileUpload1.PostedFile.FileName)))
    '    Dim context As HttpContext = HttpContext.Current

    '    context.Response.BinaryWrite(content)
    '    context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    '    context.Response.AppendHeader("Content-Disposition", "attachment; filename=EmployeeData.xlsx")
    '    context.Response.End()
    'End Sub
    Protected Sub Button100_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button100.Click
        ViewDocument(Server.MapPath("../Upload/Docs/" + grdDoc.Rows(grdDoc.SelectedIndex).Cells(0).Text))
    End Sub
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpload.Click
        If (uplFile.HasFile) Then
            Dim fileExt As String = System.IO.Path.GetExtension(uplFile.FileName).ToLower()
            If (fileExt = ".ico" Or fileExt = ".png" Or fileExt = ".jpeg" Or fileExt = ".img" Or fileExt = ".gif" Or fileExt = ".bmp") Then
                Dim sDateTimeNow As String = DateTime.Now.ToString("yyyyMMddHHmmss")
                Dim strFileName As String = uplFile.FileName.Split(".")(0).ToString().Trim() + "_" + sDateTimeNow + fileExt
                uplFile.SaveAs(Server.MapPath("../Upload/CrewImages/" + strFileName))
                imgCrew.ImageUrl = "~/Upload/CrewImages/" + strFileName
                ViewState("ImageFileName") = strFileName
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
                Return
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('6');", True)
        End If
    End Sub
End Class