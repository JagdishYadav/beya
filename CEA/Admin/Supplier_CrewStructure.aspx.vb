﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Office
Imports Microsoft.Office.Interop.Word
Imports System.IO
Public Class Supplier_CrewStructure
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                'Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                'Menuhide.Value = "close"
                Dim logout As LinkButton = Master.FindControl("Logout")
                logout.Visible = False

                Button9999.Attributes.Add("style", "display:none")
                Button1.OnClientClick = "window.opener.document.getElementById('ctl00_MainContent_Button3').click(); window.close();"

                Dim objSqlConfig As New ConfigClassSqlServer()
                ddlCountry.DataSource = objSqlConfig.GetCountries()
                ddlCountry.DataValueField = "ID"

                Session("Labourdt") = Nothing
                Session("Equipdt") = Nothing

                Session("Supplier_CS") = Nothing
                'Session("Supplier_CS_DO") = Nothing

                If (UICulture = "Arabic") Then
                    ddlCountry.DataTextField = "NameA"
                    abc.Style.Add("direction", "rtl")
                    abc.Style.Add("text-align", "right")

                    Panel4.Style.Add("direction", "rtl")
                    Panel4.Style.Add("text-align", "right")
                Else
                    ddlCountry.DataTextField = "NameE"
                End If
                ddlCountry.DataBind()
                ddlCountry.SelectedIndex = 0

                LoadGrids()
                LoadDropDowns()
                SetEnvironmentAccordingtoLang()
                LoadOtherData()
            Else
                Response.Redirect("../Login.aspx")
            End If

        End If
    End Sub
    Private Sub LoadGrids()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt, dt1 As New System.Data.DataTable
        If (Session("Labourdt") IsNot Nothing) Then
            grdLabour.DataSource = Session("Labourdt")
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False

            dt = Session("Labourdt")
            If (dt.Rows.Count > 1) Then
                If (dt.Rows(1).Item(9).ToString().Length > 0) Then
                    ddlCountry.SelectedValue = dt.Rows(1).Item(9).ToString()
                End If
                'txtMatCost.Text = dt.Rows(1).Item(10).ToString()
            End If
        Else
            dt = objSqlConfig.GetProjectCrewStructure_Labour(Session("ProjectID"), Session("LineItemCrewCode"), Session("CompanyID"))
            If (dt.Rows.Count > 0) Then
                txtCode.Text = dt.Rows(0).Item(1).ToString()
            End If
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = "-1"
            dr("Code") = "000"
            dr("ProfE") = ""
            dr("ProfA") = ""
            dr("Man_Day") = 0

            dt.Rows.InsertAt(dr, 0)

            grdLabour.DataSource = dt
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False
            Session("Labourdt") = dt

            If (dt.Rows.Count > 1) Then
                ddlCountry.SelectedValue = dt.Rows(1).Item(9).ToString()
                'txtMatCost.Text = dt.Rows(1).Item(10).ToString()
            End If
        End If

        If (Session("Equipdt") IsNot Nothing) Then
            grdEquip.DataSource = Session("Equipdt")
            grdEquip.DataBind()
            grdEquip.Rows(0).Visible = False

            If (dt1.Rows.Count > 1) Then
                'txtMatCost.Text = dt1.Rows(1).Item(9).ToString()
            End If
        Else
            dt1 = objSqlConfig.GetProjectCrewStructure_Equipment(Session("ProjectID"), Session("LineItemCrewCode"), Session("CompanyID"))
            If (dt1.Rows.Count > 0) Then
                txtCode.Text = dt1.Rows(0).Item(1).ToString()
            End If
            Dim dr1 As DataRow = dt1.NewRow()
            dr1("ID") = "-1"
            dr1("Code") = "000"
            dr1("DescE") = ""
            dr1("DescA") = ""
            dr1("Man_Day") = 0

            dt1.Rows.InsertAt(dr1, 0)
            grdEquip.DataSource = dt1
            grdEquip.DataBind()
            grdEquip.Rows(0).Visible = False
            Session("Equipdt") = dt1

            'If (dt1.Rows.Count > 1) Then
            '    txtMatCost.Text = dt1.Rows(1).Item(9).ToString()
            'End If
        End If
    End Sub
    Private Sub LoadDropDowns()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim ddlLabour As DropDownList = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList)
        If (ddlLabour IsNot Nothing) Then
            ddlLabour.DataSource = objSqlConfig.GetLaboursByCountry("", ddlCountry.SelectedValue, Session("CompanyID"))
            ddlLabour.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlLabour.DataTextField = "ProfA"
            Else
                ddlLabour.DataTextField = "ProfE"
            End If
            ddlLabour.DataBind()
        End If


        Dim ddlEquip As DropDownList = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList)
        If (ddlEquip IsNot Nothing) Then
            ddlEquip.DataSource = objSqlConfig.GetEquipments("", Session("CompanyID"))
            ddlEquip.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlEquip.DataTextField = "DescA"
            Else
                ddlEquip.DataTextField = "DescE"
            End If
            ddlEquip.DataBind()
        End If
    End Sub
    Protected Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCountry.SelectedIndexChanged
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim ddlLabour As DropDownList = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList)
        If (ddlLabour IsNot Nothing) Then
            ddlLabour.DataSource = objSqlConfig.GetLaboursByCountry("", ddlCountry.SelectedValue, Session("CompanyID"))
            ddlLabour.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlLabour.DataTextField = "ProfA"
            Else
                ddlLabour.DataTextField = "ProfE"
            End If
            ddlLabour.DataBind()
        End If
    End Sub
    Private Sub SetEnvironmentAccordingtoLang()
        If (UICulture = "Arabic") Then
            For i As Integer = 0 To grdLabour.Rows.Count - 1
                If (grdLabour.Rows(i).RowState = DataControlRowState.Normal Or grdLabour.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdLabour.Rows(i).FindControl("l1"), Label).Visible = False
                End If
            Next
            For i As Integer = 0 To grdEquip.Rows.Count - 1
                If (grdEquip.Rows(i).RowState = DataControlRowState.Normal Or grdEquip.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdEquip.Rows(i).FindControl("l11"), Label).Visible = False
                End If
            Next
        Else
            For i As Integer = 0 To grdLabour.Rows.Count - 1
                If (grdLabour.Rows(i).RowState = DataControlRowState.Normal Or grdLabour.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdLabour.Rows(i).FindControl("l2"), Label).Visible = False
                End If
            Next
            For i As Integer = 0 To grdEquip.Rows.Count - 1
                If (grdEquip.Rows(i).RowState = DataControlRowState.Normal Or grdEquip.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdEquip.Rows(i).FindControl("l12"), Label).Visible = False
                End If
            Next
        End If
    End Sub
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs)
        Dim dt As System.Data.DataTable = Session("Labourdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                If (dt.Rows(i).Item(10).ToString() = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next

        If (iCount = 0) Then
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = "-1"
            dr("CrewCode") = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedValue
            dr("ProfE") = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedItem.Text
            dr("ProfA") = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedItem.Text
            dr("Man_Day") = TryCast(grdLabour.FooterRow.FindControl("txtMDF"), TextBox).Text.Trim()

            dt.Rows.Add(dr)
            grdLabour.DataSource = dt
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False

            Session("Labourdt") = dt
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(2);", True)
        End If
    End Sub
    Protected Sub LinkButton11_Click(sender As Object, e As EventArgs)
        Dim dt As System.Data.DataTable = Session("Equipdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                If (dt.Rows(i).Item(9).ToString() = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next

        If (iCount = 0) Then
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = "-1"
            dr("CrewCode") = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedValue
            dr("DescE") = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedItem.Text
            dr("DescA") = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedItem.Text
            dr("Man_Day") = TryCast(grdEquip.FooterRow.FindControl("txtMDF1"), TextBox).Text.Trim()

            dt.Rows.Add(dr)
            grdEquip.DataSource = dt
            grdEquip.DataBind()
            grdEquip.Rows(0).Visible = False

            Session("Equipdt") = dt
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(3);", True)
        End If
    End Sub
    Protected Sub grdLabour_RowCancelingEdit(sender As Object, e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdLabour.RowCancelingEdit
        e.Cancel = True
        grdLabour.EditIndex = -1
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdLabour_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdLabour.RowEditing
        grdLabour.EditIndex = e.NewEditIndex
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdLabour_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdLabour.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow And grdLabour.EditIndex = e.Row.RowIndex) Then
            Dim ddlLabour As DropDownList = DirectCast(e.Row.FindControl("ddlLabFE"), DropDownList)
            Dim objSqlConfig As New ConfigClassSqlServer()
            ddlLabour.DataSource = objSqlConfig.GetLaboursByCountry("", ddlCountry.SelectedValue, Session("CompanyID"))
            ddlLabour.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlLabour.DataTextField = "ProfA"
            Else
                ddlLabour.DataTextField = "ProfE"
            End If
            ddlLabour.DataBind()
            ddlLabour.SelectedValue = DirectCast(e.Row.FindControl("lblLabEdit"), Label).Text
        End If
    End Sub
    Protected Sub grdLabour_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdLabour.RowUpdating
        Dim dt As System.Data.DataTable = Session("Labourdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (i <> e.RowIndex) Then
                If (dt.Rows(i).Item(10).ToString() = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next

        If (iCount = 0) Then
            dt.Rows(e.RowIndex).Item(10) = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedValue
            dt.Rows(e.RowIndex).Item(2) = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(3) = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(4) = CType(grdLabour.Rows(e.RowIndex).FindControl("txtMD"), TextBox).Text.Trim()

            grdLabour.EditIndex = -1
            Session("Labourdt") = dt
            LoadGrids()
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(2);", True)
        End If
    End Sub
    Protected Sub grdLabour_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdLabour.RowDeleting
        Dim dt As System.Data.DataTable = Session("Labourdt")
        dt.Rows.RemoveAt(e.RowIndex)
        Session("Labourdt") = dt
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
        CalculateValues()
    End Sub
    Protected Sub grdLabour_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdLabour.PageIndexChanging
        grdLabour.PageIndex = e.NewPageIndex
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdEquip_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdEquip.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow And grdEquip.EditIndex = e.Row.RowIndex) Then
            Dim ddlEquip As DropDownList = DirectCast(e.Row.FindControl("ddlEquipFE"), DropDownList)
            Dim objSqlConfig As New ConfigClassSqlServer()
            ddlEquip.DataSource = objSqlConfig.GetEquipments("", Session("CompanyID"))
            ddlEquip.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlEquip.DataTextField = "DescA"
            Else
                ddlEquip.DataTextField = "DescE"
            End If
            ddlEquip.DataBind()
            ddlEquip.SelectedValue = DirectCast(e.Row.FindControl("lblEqEdit"), Label).Text
        End If
    End Sub
    Protected Sub grdEquip_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdEquip.RowEditing
        grdEquip.EditIndex = e.NewEditIndex
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdEquip_RowCancelingEdit(sender As Object, e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdEquip.RowCancelingEdit
        e.Cancel = True
        grdEquip.EditIndex = -1
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdEquip_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdEquip.RowDeleting
        Dim dt As System.Data.DataTable = Session("Equipdt")
        dt.Rows.RemoveAt(e.RowIndex)
        Session("Equipdt") = dt
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
        CalculateValues()
    End Sub
    Protected Sub grdEquip_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdEquip.RowUpdating
        Dim dt As System.Data.DataTable = Session("Equipdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (i <> e.RowIndex) Then
                If (dt.Rows(i).Item(9).ToString() = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next
        If (iCount = 0) Then
            dt.Rows(e.RowIndex).Item(9) = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedValue
            dt.Rows(e.RowIndex).Item(2) = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(3) = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(4) = CType(grdEquip.Rows(e.RowIndex).FindControl("txtMD1"), TextBox).Text.Trim()

            grdEquip.EditIndex = -1
            Session("Equipdt") = dt
            LoadGrids()
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(2);", True)
        End If
    End Sub
    Protected Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        ddlCountry.SelectedIndex = 0
        'txtDO.Text = ""
        LoadOtherData()
        Session("Labourdt") = Nothing
        Session("Equipdt") = Nothing
        Session("Supplier_CS") = Nothing
        'Session("Supplier_CS_DO") = Nothing

        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim dt As System.Data.DataTable = Session("Labourdt")
        Dim dt1 As System.Data.DataTable = Session("Equipdt")
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        If (dt.Rows.Count = 1 And dt1.Rows.Count = 1) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(9);", True)
            Return
        End If

        sqlCommand.CommandText = "SELECT COUNT(*) FROM ProductCrewStructure WHERE ProductID = '" + txtCode.Text.Trim() + "' AND CompID = 0 "
        If (sqlCommand.ExecuteScalar().ToString() <> "0") Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(10);", True)
            Return
        End If

        For i As Integer = 1 To dt.Rows.Count - 1
            If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                sqlCommand.CommandText = "SELECT COUNT(*) FROM ProductCrewStructure WHERE ProductID = '" + txtCode.Text.Trim() + "' AND Code = '" + dt.Rows(i).Item(1).ToString() + "' AND CompID = 0 "
                If (sqlCommand.ExecuteScalar().ToString() = "0") Then
                    sqlCommand.CommandText = "INSERT INTO ProductCrewStructure(ProductID,Code,Man_Day,DailyOutput,HourlyOutput, ManHour, EquipHour, Lab_Equip, FileName,CompID) VALUES('" + txtCode.Text.Trim() + "', '" + dt.Rows(i).Item(1).ToString() + "', '" + dt.Rows(i).Item(4).ToString() + "', 0, 0, 0, 0, 1, '" + DBNull.Value + "', 0)"
                    sqlCommand.ExecuteNonQuery()
                    ViewState("CS_CREATED") = "YES"
                End If
            End If
        Next

        For i As Integer = 1 To dt1.Rows.Count - 1
            If (dt1.Rows(i).RowState <> DataRowState.Deleted) Then
                sqlCommand.CommandText = "SELECT COUNT(*) FROM ProductCrewStructure WHERE ProductID = '" + txtCode.Text.Trim() + "' AND Code = '" + dt1.Rows(i).Item(1).ToString() + "' AND CompID = '" + Session("CompanyID") + "' "
                If (sqlCommand.ExecuteScalar().ToString() = "0") Then
                    sqlCommand.CommandText = "INSERT INTO ProductCrewStructure(ProductID,Code,Man_Day,DailyOutput,HourlyOutput, ManHour, EquipHour, Lab_Equip, FileName,CompID) VALUES('" + txtCode.Text.Trim() + "', '" + dt1.Rows(i).Item(1).ToString() + "', '" + dt1.Rows(i).Item(4).ToString() + "', 0, 0, 0, 0, 2, '" + DBNull.Value + "', 0)"
                    sqlCommand.ExecuteNonQuery()
                    ViewState("CS_CREATED") = "YES"
                End If
            End If
        Next

        Session("ManHours") = lblMO.Text.Trim()
        Session("EquipHours") = lblEH.Text.Trim()
        Session("Supplier_CS") = txtCode.Text.Trim()
        'Session("Supplier_CS_DO") = txtDO.Text.Trim()

        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(1);", True)
    End Sub
    Protected Sub txtDO_TextChanged(sender As Object, e As EventArgs) Handles txtDO.TextChanged
        If (IsNumeric(txtDO.Text)) Then
            CalculateValues()
        End If
    End Sub
    Private Sub CalculateValues()
        Dim dt As System.Data.DataTable = Session("Labourdt")
        Dim dt1 As System.Data.DataTable = Session("Equipdt")
        Dim dSumCSMan As Double = 0
        Dim dSumCSEquip As Double = 0

        If (IsNumeric(txtDO.Text)) Then
            lblHO.Text = Math.Round((Double.Parse(txtDO.Text) / 8), 4)
            For i As Integer = 1 To dt.Rows.Count - 1
                If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                    If (dt.Rows(i).Item(4).ToString().Length > 0 And IsNumeric(dt.Rows(i).Item(4).ToString())) Then
                        dSumCSMan = dSumCSMan + Double.Parse(dt.Rows(i).Item(4).ToString())
                    End If
                End If
            Next
            lblMO.Text = Math.Round((dSumCSMan / Double.Parse(lblHO.Text)), 4)

            For i As Integer = 1 To dt1.Rows.Count - 1

                If (dt1.Rows(i).RowState <> DataRowState.Deleted) Then
                    If (dt1.Rows(i).Item(4).ToString().Length > 0 And IsNumeric(dt1.Rows(i).Item(4).ToString())) Then
                        dSumCSEquip = dSumCSEquip + Double.Parse(dt1.Rows(i).Item(4).ToString())
                    End If
                End If
            Next
            lblEH.Text = Math.Round((dSumCSEquip / Double.Parse(lblHO.Text)), 4)
        End If
    End Sub
    Private Sub LoadOtherData()
        Dim dt As System.Data.DataTable = Session("Labourdt")
        Dim dt1 As System.Data.DataTable = Session("Equipdt")

        If (dt.Rows.Count > 1) Then
            txtDO.Text = dt.Rows(1).Item(5).ToString()
            lblHO.Text = dt.Rows(1).Item(6).ToString()
            lblMO.Text = dt.Rows(1).Item(7).ToString()
            lblEH.Text = dt.Rows(1).Item(8).ToString()
        ElseIf dt1.Rows.Count > 1 Then
            txtDO.Text = dt1.Rows(1).Item(5).ToString()
            lblHO.Text = dt1.Rows(1).Item(6).ToString()
            lblMO.Text = dt1.Rows(1).Item(7).ToString()
            lblEH.Text = dt1.Rows(1).Item(8).ToString()
        End If
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

    End Sub
End Class