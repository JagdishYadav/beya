﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Public Class FORM2
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            txtProdID.ReadOnly = False
            If (Request.IsAuthenticated) Then
                Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                Menuhide.Value = "close"
                txtOther.Attributes.Add("style", "display:none")
                grdApprovedBy.Attributes.Add("onclick", "checkBoxListOnClick(this);")
                txtOther1.Attributes.Add("style", "display:none")
                chkSpec.Attributes.Add("onclick", "checkBoxListOnClick1(this);")
                Dim objSqlConfig As New ConfigClassSqlServer()
                grdApprovedBy.DataSource = objSqlConfig.GetApproved_Agencies()
                grdApprovedBy.DataBind()
                chkSpec.DataSource = objSqlConfig.GetProduct_Specification()
                chkSpec.DataValueField = "ID"

                grdProducts.Columns(0).Visible = False
                grdProducts.Columns(10).Visible = True
                grdProducts.Columns(11).Visible = True
                grdProducts.Columns(12).Visible = True
                grdProducts.Columns(13).Visible = True

                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    chkSpec.DataTextField = "NameA"
                    chkSpec.DataBind()
                    grdApprovedBy.Columns(0).Visible = False
                    grdApprovedBy.Columns(1).Visible = True
                    grdProducts.Columns(10).Visible = False
                    grdProducts.Columns(12).Visible = False
                Else
                    grdProducts.Columns(11).Visible = False
                    grdProducts.Columns(13).Visible = False
                    grdApprovedBy.Columns(1).Visible = False
                    grdApprovedBy.Columns(0).Visible = True
                    chkSpec.DataTextField = "NameE"
                    chkSpec.DataBind()
                End If

                If (Session("Path") IsNot Nothing) Then
                    lblPath.Text = Session("Path")
                End If

                RadioButtonList1.Enabled = False
                txtComments.ReadOnly = True
                If (Request.QueryString.Get("ID") IsNot Nothing) Then
                    Dim dt As New DataTable()
                    Dim strPath As String = ""
                    dt = objSqlConfig.GetProductPath(Request.QueryString.Get("ID").ToString())
                    If (dt.Rows.Count > 0) Then
                        Session("MapID") = dt.Rows(0).Item(0).ToString()
                        If (UICulture = "Arabic") Then
                            For i As Integer = 0 To dt.Rows.Count - 1
                                strPath = strPath + dt.Rows(i).Item(0).ToString() + " " + dt.Rows(i).Item(2).ToString() + "/"
                            Next
                        Else
                            For i As Integer = 0 To dt.Rows.Count - 1
                                strPath = strPath + dt.Rows(i).Item(0).ToString() + " " + dt.Rows(i).Item(1).ToString() + "/"
                            Next
                        End If
                        strPath = strPath.Substring(0, strPath.Length - 1)
                        lblPath.Text = strPath
                    End If
                    btnSave.Text = GetGlobalResourceObject("Resource", "Update").ToString()
                    FillProductData(Request.QueryString.Get("ID").ToString())
                    txtProdID.ReadOnly = True
                Else
                    btnSave.Text = GetGlobalResourceObject("Resource", "Save").ToString()
                    RadioButtonList1.Items(0).Selected = True
                    txtProdID.Focus()
                End If

                LoadDataGrid_Products()

            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
    End Sub
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        txtProdID.Text = ""
        txtDescA.Text = ""
        txtDescE.Text = ""
        txtProdID.Text = ""
        txtNameE.Text = ""
        txtNameA.Text = ""
        txtFeatA.Text = ""
        txtFeatE.Text = ""
        txtUnit.Text = ""
        'txtManHour.Text = "0.00"
        'txtEquipHour.Text = "0.00"
        txtComments.Text = ""
        RadioButtonList1.Items(0).Selected = True
        Image1.ImageUrl = ""

        Dim checkBox As CheckBox
        Dim textBox As TextBox
        For i As Integer = 0 To grdApprovedBy.Rows.Count - 1
            checkBox = grdApprovedBy.Rows(i).FindControl("chkApproved")
            textBox = grdApprovedBy.Rows(i).FindControl("TextBox1")
            checkBox.Checked = False
            textBox.Text = ""
        Next

        'chkList.ClearSelection()
        chkSpec.ClearSelection()
        txtOther.Text = ""
        txtOther1.Text = ""
        'txtOther.Visible = False
        'txtOther1.Visible = False
        txtOther.Attributes.Add("style", "display:none")
        txtOther1.Attributes.Add("style", "display:none")
        txtProdID.ReadOnly = False

        Session("DocFile") = Nothing
        Session("ImageFile") = Nothing

        btnSave.Text = GetGlobalResourceObject("Resource", "Save").ToString()
        txtProdID.Focus()
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim imgFileName As String = ""
        Dim docFileName As String = ""

        If (uplImage.HasFile) Then
            Dim fileExt As String = System.IO.Path.GetExtension(uplImage.FileName).ToLower()
            If (fileExt = ".jpg" Or fileExt = ".jpeg" Or fileExt = ".bmp" Or fileExt = ".gif" Or fileExt = ".png" Or fileExt = ".ico") Then
                imgFileName = System.IO.Path.GetFileNameWithoutExtension(uplImage.FileName)
                Dim sDateTimeNow As String = DateTime.Now.ToString("yyyyMMddHHmmss")
                imgFileName = imgFileName + "_" + sDateTimeNow + fileExt
                uplImage.SaveAs(Server.MapPath("../Upload/Images/" + imgFileName))
                Image1.ImageUrl = ResolveClientUrl("~/Upload/Images/" + imgFileName)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
                Return
            End If
        ElseIf (Session("ImageFile") IsNot Nothing) Then
            If (Session("ImageFile").ToString().Length > 0) Then
                imgFileName = Session("ImageFile").ToString()
            End If
        End If

        If (uplFile.HasFile) Then
            Dim fileExt As String = System.IO.Path.GetExtension(uplFile.FileName).ToLower()
            docFileName = System.IO.Path.GetFileNameWithoutExtension(uplFile.FileName)
            Dim sDateTimeNow As String = DateTime.Now.ToString("yyyyMMddHHmmss")
            docFileName = docFileName + "_" + sDateTimeNow + fileExt

            uplFile.SaveAs(Server.MapPath("../Upload/Files/" + docFileName))

        ElseIf (Session("DocFile") IsNot Nothing) Then
            If (Session("DocFile").ToString().Length > 0) Then
                docFileName = Session("DocFile").ToString()
            End If
        End If

        Session("ImageFile") = imgFileName
        Session("DocFile") = docFileName

        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        Dim strChkIDs As String = String.Empty
        Dim strrefNos As String = String.Empty
        Dim checkBox As CheckBox
        Dim textBox As TextBox
        For i As Integer = 0 To grdApprovedBy.Rows.Count - 1
            checkBox = grdApprovedBy.Rows(i).FindControl("chkApproved")
            textBox = grdApprovedBy.Rows(i).FindControl("TextBox1")

            If (checkBox.Checked) Then
                strChkIDs = strChkIDs + grdApprovedBy.DataKeys(i).Value.ToString() + ","
                strrefNos = strrefNos + textBox.Text + ","
            Else
                strrefNos = strrefNos + ","
            End If
        Next
        If (strChkIDs.Length > 0) Then
            strChkIDs = strChkIDs.Substring(0, strChkIDs.Length - 1)
        End If

        Dim strSpecIDs As String = String.Empty
        For i As Integer = 0 To chkSpec.Items.Count - 1
            If (chkSpec.Items(i).Selected) Then
                strSpecIDs = strSpecIDs + chkSpec.Items(i).Value + ","
            End If
        Next
        If (strSpecIDs.Length > 0) Then
            strSpecIDs = strSpecIDs.Substring(0, strSpecIDs.Length - 1)
        End If

        If (btnSave.Text = GetGlobalResourceObject("Resource", "Update").ToString()) Then
            sqlCommand.CommandText = "UPDATE ProductDetails SET NameE = '" + txtNameE.Text.Trim() + "', NameA = '" + txtNameA.Text.Trim() + "', PropE = '" + txtFeatE.Text.Trim() + "', PropA = '" + txtFeatA.Text.Trim() + "', DescE = '" + txtDescE.Text.Trim() + "', DescA = '" + txtDescA.Text.Trim() + "', ImageFile = '" + imgFileName + "', DocFile = '" + docFileName + "', Unit = '" + txtUnit.Text.Trim() + "', ApprovedBy = '" + strChkIDs + "', Other = '" + txtOther.Text.Trim() + "', Specification = '" + strSpecIDs + "', Other_Spec = '" + txtOther1.Text.Trim() + "', Status = 0, RefNo = '" + strrefNos + "' WHERE ID = '" + txtProdID.Text.Trim() + "' "
            sqlCommand.ExecuteNonQuery()
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('6');", True)
        Else
            sqlCommand.CommandText = "SELECT ID FROM ProductDetails WHERE ID = '" + txtProdID.Text.Trim() + "' "
            If (sqlCommand.ExecuteScalar() IsNot Nothing) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
                Return
            Else

                'If (txtManHour.Text.Length = 0) Then
                '    txtManHour.Text = "0.00"
                'End If
                'If (txtEquipHour.Text.Length = 0) Then
                '    txtEquipHour.Text = "0.00"
                'End If

                sqlCommand.CommandText = "INSERT INTO ProductDetails(ID,MapID,NameE,NameA,PropE,PropA,DescE,DescA,ImageFile,DocFile,Unit, ApprovedBy, Other, SupplierID, Status, Comments, Specification, Other_Spec, TempMapID, RefNo) VALUES('" + txtProdID.Text.Trim() + "', 0, '" + txtNameE.Text.Trim() + "', '" + txtNameA.Text.Trim() + "', '" + txtFeatE.Text.Trim() + "', '" + txtFeatA.Text.Trim() + "', '" + txtDescE.Text.Trim() + "', '" + txtDescA.Text.Trim() + "', '" + imgFileName + "', '" + docFileName + "', '" + txtUnit.Text.Trim() + "', '" + strChkIDs + "', '" + txtOther.Text.Trim() + "', '" + Session("UserID").ToString() + "', '" + RadioButtonList1.SelectedValue + "', '" + txtComments.Text.Trim() + "', '" + strSpecIDs + "', '" + txtOther1.Text.Trim() + "', '" + Session("MapID").ToString() + "', '" + strrefNos + "') "
                sqlCommand.ExecuteNonQuery()

                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
            End If
        End If
        LoadDataGrid_Products()
        sqlConnection.Close()
    End Sub
    Private Sub FillProductData(strID As String)
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()

        Dim arrValues As String()
        Dim arrRefNos As String()
        Dim strOther As String = ""

        dt = objSqlConfig.GetProductByID(strID)
        If (dt.Rows.Count > 0) Then
            txtProdID.Text = dt.Rows(0).Item(0).ToString()
            txtNameE.Text = dt.Rows(0).Item(2).ToString()
            txtNameA.Text = dt.Rows(0).Item(3).ToString()
            txtFeatE.Text = dt.Rows(0).Item(4).ToString()
            txtFeatA.Text = dt.Rows(0).Item(5).ToString()
            txtDescE.Text = dt.Rows(0).Item(6).ToString()
            txtDescA.Text = dt.Rows(0).Item(7).ToString()
            txtUnit.Text = dt.Rows(0).Item(10).ToString()
            'txtManHour.Text = dt.Rows(0).Item(11).ToString()
            'txtEquipHour.Text = dt.Rows(0).Item(12).ToString()
            txtOther.Text = dt.Rows(0).Item(14).ToString()
            txtOther1.Text = dt.Rows(0).Item(19).ToString()
            RadioButtonList1.Items.FindByValue(dt.Rows(0).Item(16).ToString()).Selected = True
            txtComments.Text = dt.Rows(0).Item(17).ToString()
            Image1.ImageUrl = ResolveClientUrl("~/Upload/Images/" + dt.Rows(0).Item(8).ToString())

            Session("ImageFile") = dt.Rows(0).Item(8).ToString()
            Session("DocFile") = dt.Rows(0).Item(9).ToString()

            arrValues = dt.Rows(0).Item(13).ToString().Split(",")

            'If (arrValues.Length > 0 And arrValues(0).Length > 0) Then
            '    For i As Integer = 0 To arrValues.Length - 1
            '        Dim listItem As ListItem = chkList.Items.FindByValue(arrValues(i))
            '        If (listItem IsNot Nothing) Then
            '            chkList.Items.FindByValue(arrValues(i)).Selected = True
            '        Else
            '            chkList.Items(chkList.Items.Count - 1).Selected = True
            '            'txtOther.Text = arrValues(i)
            '        End If
            '    Next
            'End If
            'If (chkList.Items(chkList.Items.Count - 1).Selected) Then
            '    txtOther.Visible = True
            '    txtOther.Attributes.Add("style", "display:''")
            'End If

            Dim checkBox As CheckBox
            Dim textBox As TextBox
            For i As Integer = 0 To grdApprovedBy.Rows.Count - 1

                checkBox = grdApprovedBy.Rows(i).FindControl("chkApproved")
                textBox = grdApprovedBy.Rows(i).FindControl("TextBox1")

                checkBox.Checked = False
                textBox.Text = ""
                For j As Integer = 0 To arrValues.Length - 1
                    If (grdApprovedBy.DataKeys(i).Value.ToString() = arrValues(j)) Then
                        checkBox.Checked = True
                    End If
                Next
            Next

            arrRefNos = dt.Rows(0).Item(21).ToString().Split(",")

            For i As Integer = 0 To grdApprovedBy.Rows.Count - 1
                textBox = grdApprovedBy.Rows(i).FindControl("TextBox1")
                textBox.Text = arrRefNos(i)
            Next

            checkBox = grdApprovedBy.Rows(grdApprovedBy.Rows.Count - 1).FindControl("chkApproved")
            If (checkBox.Checked = True) Then
                txtOther.Visible = True
                txtOther.Attributes.Add("style", "display:''")
            End If

            arrValues = dt.Rows(0).Item(18).ToString().Split(",")
            If (arrValues.Length > 0 And arrValues(0).Length > 0) Then
                For i As Integer = 0 To arrValues.Length - 1
                    Dim listItem As ListItem = chkSpec.Items.FindByValue(arrValues(i))
                    If (listItem IsNot Nothing) Then
                        chkSpec.Items.FindByValue(arrValues(i)).Selected = True
                    Else
                        chkSpec.Items(chkSpec.Items.Count - 1).Selected = True
                        'txtOther1.Text = arrValues(i)
                    End If
                Next
            End If
            If (chkSpec.Items(chkSpec.Items.Count - 1).Selected) Then
                txtOther1.Visible = True
                txtOther1.Attributes.Add("style", "display:''")
            End If

            RadioButtonList1.SelectedIndex = Integer.Parse(dt.Rows(0).Item(16).ToString())
            txtComments.Text = dt.Rows(0).Item(17).ToString()
        End If
    End Sub
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        Response.Redirect("Supplier_Products_View.aspx")
    End Sub
    Private Sub LoadDataGrid_Products()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        Dim strMapID As String = "-1"
        If (Session("MapID") IsNot Nothing) Then
            strMapID = Session("MapID").ToString()
        End If
        dt = objSqlConfig.GetProducts_BySupplier(strMapID, txtSearch.Text, Session("UserID"))
        If (dt.Rows.Count > 0) Then
            grdProducts.DataSource = dt
            grdProducts.DataBind()
            Session("ProductList") = dt
        Else
            grdProducts.DataSource = Nothing
            grdProducts.DataBind()
            Session("ProductList") = Nothing
        End If
    End Sub
    Private Sub FillGrid_Products()
        Dim Data As DataTable = Session("ProductList")
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp") Is Nothing) Then
                If ViewState("SortDirection").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp") & " " & "desc"
                End If

                grdProducts.DataSource = dv
                grdProducts.DataBind()
            End If
        End If
    End Sub
    Protected Sub grdProducts_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdProducts.SelectedIndexChanged
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        dt = objSqlConfig.GetProduct_Image_Data(grdProducts.SelectedDataKey("ID").ToString())

        If (dt.Rows.Count > 0) Then
            Session("ImageFile") = dt.Rows(0).Item(0).ToString()
            Session("DocFile") = dt.Rows(0).Item(1).ToString()
        End If

        txtProdID.Text = grdProducts.SelectedDataKey("ID").ToString()
        txtNameE.Text = grdProducts.Rows(grdProducts.SelectedIndex).Cells(1).Text.Trim()
        txtNameA.Text = grdProducts.Rows(grdProducts.SelectedIndex).Cells(2).Text.Trim()
        txtDescE.Text = grdProducts.Rows(grdProducts.SelectedIndex).Cells(5).Text.Replace("&nbsp;", "")
        txtDescA.Text = grdProducts.Rows(grdProducts.SelectedIndex).Cells(6).Text.Replace("&nbsp;", "")
        txtFeatE.Text = grdProducts.Rows(grdProducts.SelectedIndex).Cells(3).Text.Replace("&nbsp;", "")
        txtFeatA.Text = grdProducts.Rows(grdProducts.SelectedIndex).Cells(4).Text.Replace("&nbsp;", "")
        txtUnit.Text = grdProducts.Rows(grdProducts.SelectedIndex).Cells(7).Text.Replace("&nbsp;", "")
        'txtManHour.Text = grdProducts.Rows(grdProducts.SelectedIndex).Cells(8).Text.Replace("&nbsp;", "")
        'txtEquipHour.Text = grdProducts.Rows(grdProducts.SelectedIndex).Cells(9).Text.Replace("&nbsp;", "")
        grdProducts.Rows(grdProducts.SelectedIndex).Cells(10).Text = grdProducts.Rows(grdProducts.SelectedIndex).Cells(10).Text.Replace("&nbsp;", "")
        grdProducts.Rows(grdProducts.SelectedIndex).Cells(11).Text = grdProducts.Rows(grdProducts.SelectedIndex).Cells(11).Text.Replace("&nbsp;", "")

        Image1.ImageUrl = ResolveClientUrl("~/Upload/Images/" + dt.Rows(0).Item(0).ToString())

        txtOther.Attributes.Add("style", "display:none")
        txtOther1.Attributes.Add("style", "display:none")

        Dim arrValues As String()
        Dim arrRefNos As String()
        Dim strOther As String = ""

        If (UICulture = "Arabic") Then
            arrValues = grdProducts.Rows(grdProducts.SelectedIndex).Cells(11).Text.Replace("&nbsp;", "").Split(",")
            strOther = arrValues(arrValues.Length - 1)
        Else
            arrValues = grdProducts.Rows(grdProducts.SelectedIndex).Cells(10).Text.Replace("&nbsp;", "").Split(",")
            strOther = arrValues(arrValues.Length - 1)
        End If

        arrValues = grdProducts.DataKeys(grdProducts.SelectedIndex).Values(2).ToString().Split(",")
        
        Dim checkBox As CheckBox
        Dim textBox As TextBox
        For i As Integer = 0 To grdApprovedBy.Rows.Count - 1

            checkBox = grdApprovedBy.Rows(i).FindControl("chkApproved")
            textBox = grdApprovedBy.Rows(i).FindControl("TextBox1")

            checkBox.Checked = False
            textBox.Text = ""
            For j As Integer = 0 To arrValues.Length - 1
                If (grdApprovedBy.DataKeys(i).Value.ToString() = arrValues(j)) Then
                    checkBox.Checked = True
                End If
            Next
        Next

        arrRefNos = grdProducts.DataKeys(grdProducts.SelectedIndex).Values(1).Replace("&nbsp;", "").Split(",")

        For i As Integer = 0 To grdApprovedBy.Rows.Count - 1
            textBox = grdApprovedBy.Rows(i).FindControl("TextBox1")
            textBox.Text = arrRefNos(i)
        Next

        If (UICulture = "Arabic") Then
            arrValues = grdProducts.Rows(grdProducts.SelectedIndex).Cells(13).Text.Replace("&nbsp;", "").Split(",")
        Else
            arrValues = grdProducts.Rows(grdProducts.SelectedIndex).Cells(12).Text.Replace("&nbsp;", "").Split(",")
        End If

        If (arrValues.Length > 0 And arrValues(0).Length > 0) Then
            For i As Integer = 0 To arrValues.Length - 1
                Dim listItem As ListItem = chkSpec.Items.FindByText(arrValues(i))
                If (listItem IsNot Nothing) Then
                    chkSpec.Items.FindByText(arrValues(i)).Selected = True
                Else
                    chkSpec.Items(chkSpec.Items.Count - 1).Selected = True
                    txtOther1.Text = arrValues(i)
                End If
            Next
        End If
        If (chkSpec.Items(chkSpec.Items.Count - 1).Selected) Then
            txtOther1.Visible = True
            txtOther1.Attributes.Add("style", "display:''")
        End If
        
        checkBox = grdApprovedBy.Rows(grdApprovedBy.Rows.Count - 1).FindControl("chkApproved")
        If (checkBox.Checked) Then
            txtOther.Text = strOther
            txtOther.Visible = True
            txtOther.Attributes.Add("style", "display:''")
        End If
        txtProdID.ReadOnly = True
        If (UICulture = "Arabic") Then
            btnSave.Text = "التحديث"
        Else
            btnSave.Text = "Update"
        End If
    End Sub
    Protected Sub grdProducts_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdProducts.Sorting
        If (ViewState("SortState") IsNot Nothing And ViewState("SortState") = SortDirection.Ascending) Then
            ViewState("SortDirection") = "asc"
            ViewState("SortState") = 1
        Else
            ViewState("SortDirection") = "desc"
            ViewState("SortState") = 0
        End If
        ViewState("SortExp") = e.SortExpression
        FillGrid_Products()
    End Sub
    Protected Sub grdProducts_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdProducts.PageIndexChanging
        grdProducts.PageIndex = e.NewPageIndex
        grdProducts.DataSource = Session("ProductList")
        grdProducts.DataBind()
    End Sub
    Protected Sub grdProducts_DataBound(sender As Object, e As EventArgs) Handles grdProducts.DataBound
        For Each row As GridViewRow In grdProducts.Rows
            If (row.Cells(2).Text.Equals("&nbsp;") Or row.Cells(2).Text.Equals("&amp;nbsp;") Or row.Cells(2).Text.Equals("")) Then
                row.Cells(2).Text = String.Empty
            End If
        Next
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        LoadDataGrid_Products()
    End Sub
End Class