﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.IO
Public Class Supplier_New
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                btnLogin.Visible = False
                Dim ul As HtmlGenericControl = Me.Page.Master.FindControl("ulGlobalDropdown")
                Dim lblWelcome As Label = Me.Page.Master.FindControl("Welcome")
                ul.Visible = True
                lblWelcome.Visible = True
                If (Session("UserID") = "-99") Then
                    Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                    Menu1.Visible = False
                    btnLogin.Visible = True
                    ul.Visible = False
                    lblWelcome.Visible = False
                Else
                    Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                    Dim menuItems As MenuItemCollection = Menu1.Items
                    If (menuItems.Count > 15) Then
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                        menuItems.RemoveAt(12)
                    End If
                End If

                txtOther.Attributes.Add("style", "display:none")
                'chkList.Attributes.Add("onclick", "checkBoxListOnClick(this);")
                grdApprovedBy.Attributes.Add("onclick", "checkBoxListOnClick(this);")
                Dim objSqlConfig As New ConfigClassSqlServer()
                'chkList.DataSource = objSqlConfig.GetApproved_Agencies()
                'chkList.DataValueField = "ID"

                grdApprovedBy.DataSource = objSqlConfig.GetApproved_Agencies()
                grdApprovedBy.DataBind()

                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    'LastRow.Style.Add("direction", "rtl")
                    'LastRow.Style.Add("text-align", "right")
                    'tdsave.Align = "left"
                    'tdcancel.Align = "right"
                    'header1.Style.Add("float", "right")
                    'header2.Style.Add("float", "right")
                    'div1.Style.Add("float", "right")
                    div2.Style.Add("float", "right")
                    'div3.Style.Add("float", "right")
                    'div4.Style.Add("float", "right")
                    'div5.Style.Add("float", "right")
                    'chkList.DataTextField = "NameA"
                    'chkList.DataBind()
                    grdApprovedBy.Columns(0).Visible = False
                    grdApprovedBy.Columns(1).Visible = True
                Else
                    'chkList.DataTextField = "NameE"
                    'chkList.DataBind()
                    grdApprovedBy.Columns(1).Visible = False
                    grdApprovedBy.Columns(0).Visible = True
                End If

                txtUserID.Enabled = True
                If (Request.QueryString.Get("ID") IsNot Nothing) Then
                    Button1.Text = GetGlobalResourceObject("Resource", "Update").ToString()
                    FillSupplierData(Request.QueryString.Get("ID").ToString())
                    txtUserID.Enabled = False
                Else
                    Button1.Text = GetGlobalResourceObject("Resource", "Save").ToString()
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        txtNameE.Text = ""
        txtNameA.Text = ""
        txtAddress.Text = ""
        txtCity.Text = ""
        txtCountry.Text = ""
        txtPhone.Text = ""
        txtPOBox.Text = ""
        txtEmail.Text = ""
        txtFax.Text = ""
        txtZipCode.Text = ""
        txtUserID.Text = ""
        txtPswd.Text = ""
        txtPassword.Text = ""
        Dim checkBox As CheckBox
        Dim textBox As TextBox
        For i As Integer = 0 To grdApprovedBy.Rows.Count - 1
            checkBox = grdApprovedBy.Rows(i).FindControl("chkApproved")
            textBox = grdApprovedBy.Rows(i).FindControl("TextBox1")
            checkBox.Checked = False
            textBox.Text = ""
        Next
        'ddlApprovedBy.SelectedIndex = 0
        'chkList.ClearSelection()
        txtOther.Text = ""
        'txtOther.Visible = False
        txtOther.Attributes.Add("style", "display:none")
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim objSqlConfig As New ConfigClassSqlServer()
        If (Request.QueryString.Get("ID") Is Nothing) Then
            If (txtPswd.Text.Trim() <> txtPassword.Text.Trim()) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('8');", True)
                txtPswd.Focus()
                Return
            End If
            If (txtOther.Style.Item("Display") = "" And txtOther.Text.Length = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('6');", True)
                txtOther.Focus()
                Return
            End If

            Dim strChkIDs As String = String.Empty
            Dim strrefNos As String = String.Empty
            Dim checkBox As CheckBox
            Dim textBox As TextBox
            For i As Integer = 0 To grdApprovedBy.Rows.Count - 1
                checkBox = grdApprovedBy.Rows(i).FindControl("chkApproved")
                textBox = grdApprovedBy.Rows(i).FindControl("TextBox1")

                If (checkBox.Checked) Then
                    strChkIDs = strChkIDs + grdApprovedBy.DataKeys(i).Value.ToString() + ","
                    strrefNos = strrefNos + textBox.Text + ","
                Else
                    strrefNos = strrefNos + ","
                End If
            Next
            If (strChkIDs.Length > 0) Then
                strChkIDs = strChkIDs.Substring(0, strChkIDs.Length - 1)
            End If

            Dim retVal As Integer = objSqlConfig.CreateSupplier(txtUserID.Text.Trim(), txtNameE.Text.Trim(), txtNameA.Text.Trim(), txtAddress.Text.Trim(), txtPOBox.Text.Trim(), txtCity.Text.Trim(), txtZipCode.Text.Trim(), txtCountry.Text.Trim(), txtPhone.Text.Trim(), txtFax.Text.Trim(), txtEmail.Text.Trim(), strChkIDs, txtOther.Text.Trim(), strrefNos.Trim(), Encryption.Encrypt(txtPswd.Text.Trim()))
            Dim userID As Integer = -9
            If (retVal = 0) Then
                If (txtUserID.Text.Length > 0 And txtPswd.Text.Length > 0) Then
                    userID = objSqlConfig.CreateUser(txtUserID.Text.Trim(), txtPswd.Text.Trim(), txtNameE.Text.Trim(), "", txtEmail.Text.Trim(), "0")
                    If (userID > 0) Then
                        objSqlConfig.InsertUserRoles(userID.ToString(), "99")
                        'objSqlConfig.UpdateSupplierUserID(txtSupplierID.Text.Trim(), userID)
                        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
                        Button2_Click(sender, Nothing)
                    ElseIf (userID = -1) Then
                        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('7');", True)
                        txtUserID.Focus()
                    Else
                        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('-1');", True)
                    End If
                Else
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
                    Button2_Click(sender, Nothing)
                End If
            ElseIf (retVal = 1) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('-1');", True)
            End If

        Else
            If (txtOther.Style.Item("Display") = "" And txtOther.Text.Length = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('6');", True)
                txtOther.Focus()
                Return
            End If

            Dim strChkIDs As String = String.Empty
            Dim strrefNos As String = String.Empty
            Dim checkBox As CheckBox
            Dim textBox As TextBox
            For i As Integer = 0 To grdApprovedBy.Rows.Count - 1
                checkBox = grdApprovedBy.Rows(i).FindControl("chkApproved")
                textBox = grdApprovedBy.Rows(i).FindControl("TextBox1")

                If (checkBox.Checked) Then
                    strChkIDs = strChkIDs + grdApprovedBy.DataKeys(i).Value.ToString() + ","
                    strrefNos = strrefNos + textBox.Text + ","
                Else
                    strrefNos = strrefNos + ","
                End If
            Next
            If (strChkIDs.Length > 0) Then
                strChkIDs = strChkIDs.Substring(0, strChkIDs.Length - 1)
            End If

            Dim retVal As Integer = objSqlConfig.UpdateSupplier(txtUserID.Text.Trim(), txtNameE.Text.Trim(), txtNameA.Text.Trim(), txtAddress.Text.Trim(), txtPOBox.Text.Trim(), txtCity.Text.Trim(), txtZipCode.Text.Trim(), txtCountry.Text.Trim(), txtPhone.Text.Trim(), txtFax.Text.Trim(), txtEmail.Text.Trim(), strChkIDs, txtOther.Text.Trim(), strrefNos.Trim(), Encryption.Encrypt(txtPswd.Text.Trim()))
            If (retVal = 0) Then
                'If (txtUserID.Text.Length > 0 And txtPswd.Text.Length > 0) Then
                '    retVal = objSqlConfig.UpdateUserByID(txtSupplierID.Text.Trim(), txtUserID.Text.Trim(), txtPswd.Text.Trim(), txtNameE.Text.Trim(), "", txtEmail.Text.Trim())
                '    If (retVal = 0) Then
                '        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
                '    End If
                'Else
                '    retVal = objSqlConfig.DeleteUserBySupplier(txtSupplierID.Text.Trim())
                '    If (retVal = 0) Then
                '        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
                '    End If
                'End If
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)

            ElseIf (retVal = 1) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('5');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('-1');", True)
            End If
        End If
    End Sub
    Private Sub FillSupplierData(strSupplierID As String)
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        dt = objSqlConfig.GetSupplierByUserID(strSupplierID)
        If (dt.Rows.Count > 0) Then
            'txtSupplierID.Text = dt.Rows(0).Item(0).ToString()
            txtNameE.Text = dt.Rows(0).Item(1).ToString()
            txtNameA.Text = dt.Rows(0).Item(2).ToString()
            txtAddress.Text = dt.Rows(0).Item(3).ToString()
            txtPOBox.Text = dt.Rows(0).Item(4).ToString()
            txtCity.Text = dt.Rows(0).Item(5).ToString()
            txtZipCode.Text = dt.Rows(0).Item(6).ToString()
            txtCountry.Text = dt.Rows(0).Item(7).ToString()
            txtPhone.Text = dt.Rows(0).Item(8).ToString()
            txtFax.Text = dt.Rows(0).Item(9).ToString()
            txtEmail.Text = dt.Rows(0).Item(10).ToString()
            txtOther.Text = dt.Rows(0).Item(12).ToString()
            txtUserID.Text = dt.Rows(0).Item(14).ToString()
            If (dt.Rows(0).Item(15).ToString().Length > 0) Then
                txtPswd.Text = Encryption.Decrypt(dt.Rows(0).Item(15).ToString())
                txtPassword.Text = txtPswd.Text
            End If
            'chkList.ClearSelection()
            'txtOther.Visible = False

            Dim arrRefNos As String()
            Dim arrValues As String() = dt.Rows(0).Item(11).ToString().Split(",")
            Dim checkBox As CheckBox
            Dim textBox As TextBox
            Dim strOther As String = ""

            For i As Integer = 0 To grdApprovedBy.Rows.Count - 1
                checkBox = grdApprovedBy.Rows(i).FindControl("chkApproved")
                textBox = grdApprovedBy.Rows(i).FindControl("TextBox1")

                checkBox.Checked = False
                textBox.Text = ""
                For j As Integer = 0 To arrValues.Length - 1
                    If (grdApprovedBy.DataKeys(i).Value.ToString() = arrValues(j)) Then
                        checkBox.Checked = True
                    End If
                Next
            Next

            arrRefNos = dt.Rows(0).Item(13).ToString().Split(",")

            For i As Integer = 0 To grdApprovedBy.Rows.Count - 1
                textBox = grdApprovedBy.Rows(i).FindControl("TextBox1")
                textBox.Text = arrRefNos(i)
            Next
            'If (arrValues.Length > 0 And arrValues(0).Length > 0) Then
            '    For i As Integer = 0 To arrValues.Length - 1
            '        If (arrValues(i).Equals("99")) Then
            '            chkList.Items.FindByValue(arrValues(i)).Selected = True
            '        Else
            '            chkList.Items.FindByValue(arrValues(i)).Selected = True
            '        End If
            '    Next
            'End If
            'If (chkList.Items(chkList.Items.Count - 1).Selected) Then
            '    txtOther.Visible = True
            'End If
            checkBox = grdApprovedBy.Rows(grdApprovedBy.Rows.Count - 1).FindControl("chkApproved")
            If (checkBox.Checked) Then
                'txtOther.Visible = True
                txtOther.Attributes.Add("style", "display:''")
            End If
        End If
    End Sub
    Protected Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        Response.Redirect("Supplier_New.aspx")
    End Sub
    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        Dim strURL As String = "../Supplier"
        Session.Clear()
        Session.RemoveAll()
        Session.Abandon()
        Response.Redirect(strURL)
    End Sub
End Class
