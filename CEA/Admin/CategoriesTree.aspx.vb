﻿Imports System.Data
Imports System.Data.SqlClient
Public Class CategoriesTree
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                Menuhide.Value = "close"
                Button8.Attributes.Add("style", "display:none")
                Dim languageSelect As HiddenField = Me.Page.Master.FindControl("hfMenuLanguage")
                If languageSelect.Value = "Arabic" Then
                    tvLevels.LineImagesFolder = "~/TreeLineImagesAr"
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
    End Sub
    'Private Sub LoadTreeViewData()
    '    'If (hdnInitialLoad.Value.ToString().Equals("1")) Then
    '    Dim sqlConnection As New SqlConnection()
    '    Dim node As TreeNode = Nothing
    '    'Dim NodeLevel1, NodeLevel2, NodeLevel3, NodeLevel4, NodeLevel5, NodeLevel6, NodeLevel7 As TreeNode
    '    tvLevels.Nodes.Clear()

    '    Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
    '    sqlCommand.CommandTimeout = 120000
    '    'Dim r1, r2, r3, r4, r5, r6, r7, r8 As SqlDataReader
    '    'Dim dt1, dt2, dt3, dt4, dt5, dt6, dt7, dt8 As New DataTable()
    '    Dim r1 As SqlDataReader
    '    Dim dt1 As New DataTable()

    '    sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
    '    sqlConnection.Open()

    '    If (UICulture = "Arabic") Then
    '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = 1 AND CompID = 0 ORDER BY No ASC"
    '        r1 = sqlCommand.ExecuteReader()
    '        dt1.Load(r1)
    '        r1.Close()

    '        For Each dr1 As DataRow In dt1.Rows
    '            If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
    '                node = New TreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
    '                node.Target = dr1.Item(2).ToString()
    '                'node.ImageUrl = dr1.Item(3).ToString()
    '                node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
    '                tvLevels.Nodes.Add(node)
    '                'NodeLevel1 = node

    '                'sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = 1 ORDER BY No ASC"
    '                'r2 = sqlCommand.ExecuteReader()
    '                'dt2.Clear()
    '                'dt2.Load(r2)
    '                'r2.Close()
    '                'For Each dr2 As DataRow In dt2.Rows
    '                '    If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
    '                '        node = New TreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
    '                '        node.Target = dr2.Item(2).ToString()
    '                '        'node.ImageUrl = dr2.Item(3).ToString() 
    '                '        node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
    '                '        NodeLevel1.ChildNodes.Add(node)
    '                '        NodeLevel2 = node

    '                '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = 1 ORDER BY No ASC"
    '                '        r3 = sqlCommand.ExecuteReader()
    '                '        dt3.Clear()
    '                '        dt3.Load(r3)
    '                '        r3.Close()
    '                '        For Each dr3 As DataRow In dt3.Rows
    '                '            If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
    '                '                node = New TreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
    '                '                node.Target = dr3.Item(2).ToString()
    '                '                'node.ImageUrl = dr3.Item(3).ToString()
    '                '                node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
    '                '                NodeLevel2.ChildNodes.Add(node)
    '                '                NodeLevel3 = node

    '                '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = 1 ORDER BY No ASC"
    '                '                r4 = sqlCommand.ExecuteReader()
    '                '                dt4.Clear()
    '                '                dt4.Load(r4)
    '                '                r4.Close()

    '                '                For Each dr4 As DataRow In dt4.Rows
    '                '                    If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
    '                '                        node = New TreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
    '                '                        node.Target = dr4.Item(2).ToString()
    '                '                        'node.ImageUrl = dr4.Item(3).ToString()
    '                '                        node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
    '                '                        NodeLevel3.ChildNodes.Add(node)
    '                '                        NodeLevel4 = node

    '                '                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = 1 ORDER BY No ASC"
    '                '                        r5 = sqlCommand.ExecuteReader()
    '                '                        dt5.Clear()
    '                '                        dt5.Load(r5)
    '                '                        r5.Close()
    '                '                        For Each dr5 As DataRow In dt5.Rows
    '                '                            If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
    '                '                                node = New TreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
    '                '                                node.Target = dr5.Item(2).ToString()
    '                '                                'node.ImageUrl = dr5.Item(3).ToString()
    '                '                                node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
    '                '                                NodeLevel4.ChildNodes.Add(node)
    '                '                                NodeLevel5 = node

    '                '                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = 1 ORDER BY No ASC"
    '                '                                r6 = sqlCommand.ExecuteReader()
    '                '                                dt6.Clear()
    '                '                                dt6.Load(r6)
    '                '                                r6.Close()
    '                '                                For Each dr6 As DataRow In dt6.Rows
    '                '                                    If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
    '                '                                        node = New TreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
    '                '                                        node.Target = dr6.Item(2).ToString()
    '                '                                        'node.ImageUrl = dr6.Item(3).ToString()
    '                '                                        node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
    '                '                                        NodeLevel5.ChildNodes.Add(node)
    '                '                                        NodeLevel6 = node

    '                '                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = 1 ORDER BY No ASC"
    '                '                                        r7 = sqlCommand.ExecuteReader()
    '                '                                        dt7.Clear()
    '                '                                        dt7.Load(r7)
    '                '                                        r7.Close()
    '                '                                        For Each dr7 As DataRow In dt7.Rows
    '                '                                            If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
    '                '                                                node = New TreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
    '                '                                                node.Target = dr7.Item(2).ToString()
    '                '                                                'node.ImageUrl = dr7.Item(3).ToString()
    '                '                                                node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
    '                '                                                NodeLevel6.ChildNodes.Add(node)
    '                '                                                NodeLevel7 = node

    '                '                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = 1 ORDER BY No ASC"
    '                '                                                r8 = sqlCommand.ExecuteReader()
    '                '                                                dt8.Clear()
    '                '                                                dt8.Load(r8)
    '                '                                                r8.Close()
    '                '                                                For Each dr8 As DataRow In dt8.Rows
    '                '                                                    If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
    '                '                                                        node = New TreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
    '                '                                                        node.Target = dr8.Item(2).ToString()
    '                '                                                        'node.ImageUrl = dr8.Item(3).ToString()
    '                '                                                        node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
    '                '                                                        NodeLevel7.ChildNodes.Add(node)
    '                '                                                    End If
    '                '                                                Next
    '                '                                            End If
    '                '                                        Next
    '                '                                    End If
    '                '                                Next
    '                '                            End If
    '                '                        Next
    '                '                    End If
    '                '                Next
    '                '            End If
    '                '        Next
    '                '    End If
    '                'Next
    '            End If
    '        Next

    '    Else
    '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = 1 AND CompID = 0 ORDER BY No ASC"
    '        r1 = sqlCommand.ExecuteReader()
    '        dt1.Load(r1)
    '        r1.Close()

    '        For Each dr1 As DataRow In dt1.Rows
    '            If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
    '                node = New TreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
    '                node.Target = dr1.Item(2).ToString()
    '                'node.ImageUrl = dr1.Item(3).ToString()
    '                node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
    '                tvLevels.Nodes.Add(node)
    '                'NodeLevel1 = node

    '                'sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = 1 ORDER BY No ASC"
    '                'r2 = sqlCommand.ExecuteReader()
    '                'dt2.Clear()
    '                'dt2.Load(r2)
    '                'r2.Close()
    '                'For Each dr2 As DataRow In dt2.Rows
    '                '    If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
    '                '        node = New TreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
    '                '        node.Target = dr2.Item(2).ToString()
    '                '        'node.ImageUrl = dr2.Item(3).ToString()
    '                '        node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
    '                '        NodeLevel1.ChildNodes.Add(node)
    '                '        NodeLevel2 = node

    '                '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = 1 ORDER BY No ASC"
    '                '        r3 = sqlCommand.ExecuteReader()
    '                '        dt3.Clear()
    '                '        dt3.Load(r3)
    '                '        r3.Close()
    '                '        For Each dr3 As DataRow In dt3.Rows
    '                '            If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
    '                '                node = New TreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
    '                '                node.Target = dr3.Item(2).ToString()
    '                '                'node.ImageUrl = dr3.Item(3).ToString()
    '                '                node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
    '                '                NodeLevel2.ChildNodes.Add(node)
    '                '                NodeLevel3 = node

    '                '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = 1 ORDER BY No ASC"
    '                '                r4 = sqlCommand.ExecuteReader()
    '                '                dt4.Clear()
    '                '                dt4.Load(r4)
    '                '                r4.Close()

    '                '                For Each dr4 As DataRow In dt4.Rows
    '                '                    If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
    '                '                        node = New TreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
    '                '                        node.Target = dr4.Item(2).ToString()
    '                '                        'node.ImageUrl = dr4.Item(3).ToString()
    '                '                        node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
    '                '                        NodeLevel3.ChildNodes.Add(node)
    '                '                        NodeLevel4 = node

    '                '                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = 1 ORDER BY No ASC"
    '                '                        r5 = sqlCommand.ExecuteReader()
    '                '                        dt5.Clear()
    '                '                        dt5.Load(r5)
    '                '                        r5.Close()
    '                '                        For Each dr5 As DataRow In dt5.Rows
    '                '                            If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
    '                '                                node = New TreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
    '                '                                node.Target = dr5.Item(2).ToString()
    '                '                                'node.ImageUrl = dr5.Item(3).ToString()
    '                '                                node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
    '                '                                NodeLevel4.ChildNodes.Add(node)
    '                '                                NodeLevel5 = node

    '                '                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = 1 ORDER BY No ASC"
    '                '                                r6 = sqlCommand.ExecuteReader()
    '                '                                dt6.Clear()
    '                '                                dt6.Load(r6)
    '                '                                r6.Close()
    '                '                                For Each dr6 As DataRow In dt6.Rows
    '                '                                    If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
    '                '                                        node = New TreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
    '                '                                        node.Target = dr6.Item(2).ToString()
    '                '                                        'node.ImageUrl = dr6.Item(3).ToString()
    '                '                                        node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
    '                '                                        NodeLevel5.ChildNodes.Add(node)
    '                '                                        NodeLevel6 = node

    '                '                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = 1 ORDER BY No ASC"
    '                '                                        r7 = sqlCommand.ExecuteReader()
    '                '                                        dt7.Clear()
    '                '                                        dt7.Load(r7)
    '                '                                        r7.Close()
    '                '                                        For Each dr7 As DataRow In dt7.Rows
    '                '                                            If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
    '                '                                                node = New TreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
    '                '                                                node.Target = dr7.Item(2).ToString()
    '                '                                                'node.ImageUrl = dr7.Item(3).ToString()
    '                '                                                node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
    '                '                                                NodeLevel6.ChildNodes.Add(node)
    '                '                                                NodeLevel7 = node

    '                '                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = 1 ORDER BY No ASC"
    '                '                                                r8 = sqlCommand.ExecuteReader()
    '                '                                                dt8.Clear()
    '                '                                                dt8.Load(r8)
    '                '                                                r8.Close()
    '                '                                                For Each dr8 As DataRow In dt8.Rows
    '                '                                                    If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
    '                '                                                        node = New TreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
    '                '                                                        node.Target = dr8.Item(2).ToString()
    '                '                                                        'node.ImageUrl = dr8.Item(3).ToString()
    '                '                                                        node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
    '                '                                                        NodeLevel7.ChildNodes.Add(node)
    '                '                                                    End If
    '                '                                                Next
    '                '                                            End If
    '                '                                        Next
    '                '                                    End If
    '                '                                Next
    '                '                            End If
    '                '                        Next
    '                '                    End If
    '                '                Next
    '                '            End If
    '                '        Next
    '                '    End If
    '                'Next
    '            End If
    '        Next
    '    End If

    '    tvLevels.Visible = True
    '    sqlConnection.Close()
    '    tvLevels.CollapseAll()
    '    'End If
    '    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "HideDiv();", True)
    'End Sub
    Private Sub LoadTreeViewData()
        Dim strClientTypeID As String = "1"
        If (Session("ClientTypeID") IsNot Nothing) Then
            strClientTypeID = Session("ClientTypeID")
        End If
        If (hdnInitialLoad.Value.ToString().Equals("1")) Then
            Dim sqlConnection As New SqlConnection()
            Dim node As TreeNode = Nothing
            tvLevels.Nodes.Clear()

            Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
            sqlCommand.CommandTimeout = 120000
            Dim r1 As SqlDataReader
            Dim dt1 As New DataTable()

            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
            sqlConnection.Open()

            If (UICulture = "Arabic") Then
                sqlCommand.CommandText = "SELECT DISTINCT ID,  NameA, NameE, CAST(No As Numeric(25))No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                r1 = sqlCommand.ExecuteReader()
                dt1.Load(r1)
                r1.Close()

                For Each dr1 As DataRow In dt1.Rows
                    If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
                        node = New TreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
                        node.Target = dr1.Item(2).ToString()
                        node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
                        tvLevels.Nodes.Add(node)
                    End If
                Next

            Else
                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                r1 = sqlCommand.ExecuteReader()
                dt1.Load(r1)
                r1.Close()

                For Each dr1 As DataRow In dt1.Rows
                    If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
                        node = New TreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
                        node.Target = dr1.Item(2).ToString()
                        node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
                        tvLevels.Nodes.Add(node)
                        
                    End If
                Next
            End If

            tvLevels.Visible = True
            sqlConnection.Close()
            tvLevels.ExpandAll()

        End If
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "HideDiv();", True)
    End Sub
    Protected Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click
        Dim currentnode As TreeNode
        If (tvLevels.SelectedNode IsNot Nothing) Then
            If tvLevels.SelectedNode.ChildNodes.Count = 0 Then
                currentnode = tvLevels.SelectedNode
                Session("MapID") = currentnode.ToolTip.Split(",")(2)
                Session("Path") = currentnode.ValuePath
                Response.Redirect("Supplier_quote.aspx")
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('8');", True)
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
        End If
    End Sub
    Protected Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        If (Request.IsAuthenticated) Then
            LoadTreeViewData()
        Else
            Response.Redirect("../Login.aspx")
        End If
        If (UICulture = "Arabic") Then
            tvLevels.Style.Add("float", "right")
            tvLevels.Style.Add("direction", "rtl")
            tvLevels.Style.Add("text-align", "right")
            div1.Style.Add("float", "right")
            div2.Style.Add("float", "right")
            div2.Style.Add("direction", "rtl")
            div2.Style.Add("text-align", "right")
        End If
    End Sub
    Protected Sub tvLevels_SelectedNodeChanged(sender As Object, e As EventArgs) Handles tvLevels.SelectedNodeChanged
        Dim node As TreeNode
        Dim selectedNode As TreeNode
        Dim textvalue As String = ""
        Dim index As Integer = 0
        Dim myChars() As Char
        Dim ch As Char

        If (tvLevels.Nodes.Count > 0 And tvLevels.SelectedNode.Text <> "" And tvLevels.SelectedNode.ChildNodes.Count = 0) Then

            Dim strClientTypeID As String = "1"
            If (Session("ClientTypeID") IsNot Nothing) Then
                strClientTypeID = Session("ClientTypeID")
            End If

            Dim dt As New DataTable()
            Dim arrList() As String = tvLevels.SelectedNode.ToolTip.Split(",")
            Dim strNodeNumber As String = arrList(arrList.Length - 1)
            Dim strLevelNo As Integer = Integer.Parse(arrList(0))
            Dim sqlConnection As New SqlConnection()

            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
            sqlConnection.Open()
            Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()

            If (UICulture = "Arabic") Then
                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Numeric(25))No FROM MainHierarchy1 WHERE ParentNo = '" + strNodeNumber + "' AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
            Else
                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Numeric(25))No FROM MainHierarchy1 WHERE ParentNo = '" + strNodeNumber + "' AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
            End If
            Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
            sqlDataAdapter.Fill(dt)

            strLevelNo = strLevelNo + 1
            For i As Integer = 0 To dt.Rows.Count - 1
                If Not (IsDBNull(dt.Rows(i).Item(0).ToString())) And Not (Trim(dt.Rows(i).Item(0).ToString())) = "" Then
                    node = New TreeNode(dt.Rows(i).Item(3).ToString() + " " + dt.Rows(i).Item(1).ToString())
                    node.Target = dt.Rows(i).Item(2).ToString()
                    node.ToolTip = strLevelNo.ToString() + "," + dt.Rows(i).Item(0).ToString() + "," + dt.Rows(i).Item(3).ToString()
                    tvLevels.SelectedNode.ChildNodes.Add(node)
                End If
            Next
            'If (dt.Rows.Count = 0) Then
            '    tvLevels.SelectedNode.ShowCheckBox = True
            'End If
            sqlConnection.Close()
            tvLevels.SelectedNode.ExpandAll()

        End If
    End Sub
End Class