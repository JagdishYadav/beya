﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Register.aspx.vb" Inherits="CEA.Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorPassMatch") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "AccountSuccess") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorAlreadyExist") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProfileUpdated") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorFileFormat") %>';
            }
            alert(msgstring);
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Register Company
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" class="registerCompanyWrapper">
        <ContentTemplate>
            <div class="row">

                <div class="col-12">

                    <div class="formWrap">
                        <div class="formBlk mb-5">
                            <h3>Enter Company Details</h3>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Label2" runat="server" Text="Company Name:" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtCompName"></asp:Label>
                                        <asp:TextBox ID="txtCompName" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorUserName %>'
                                            ControlToValidate="txtName" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Label3" runat="server" Text="Country:" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="DropDownList1"></asp:Label>
                                        <asp:DropDownList ID="DropDownList1" runat="server" SkinID="FormControl" CssClass="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvComp" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorCompName %>'
                                            ControlToValidate="txtCompName" SetFocusOnError="true" ValidationGroup="Save"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="formBlk">
                            <h3>Create Company Admin Account</h3>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Label4" runat="server" Text="Email:" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtEmail"></asp:Label>
                                        <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorEmail %>'
                                            ControlToValidate="txtEmail" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="reEmail" runat="server" ErrorMessage='<%$ Resources:Resource, InvalidEmail %>'
                                            SetFocusOnError="true" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            Display="None" ValidationGroup="Save"> </asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Phone" runat="server" Text="Phone" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtPhone"></asp:Label>
                                        <asp:TextBox ID="txtPhone" runat="server" MaxLength="20" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ErrorMessage="Please enter phone no to save..." ControlToValidate="txtPhone" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Fax" runat="server" Text="Fax" MaxLength="20" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtFax"></asp:Label>
                                        <asp:TextBox ID="txtFax" runat="server" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvFax" runat="server" ErrorMessage="Please enter fax no to save..." ControlToValidate="txtFax" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="PObox" runat="server" Text="PO box" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtPOBox"></asp:Label>
                                        <asp:TextBox ID="txtPOBox" runat="server" MaxLength="20" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPOBox" runat="server" ErrorMessage="Please enter PO Box to save..." ControlToValidate="txtPOBox" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="ZipCode" runat="server" Text="Zip Code" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtZipCode"></asp:Label>
                                        <asp:TextBox ID="txtZipCode" runat="server" MaxLength="20" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvZipCode" runat="server" ErrorMessage="Please enter Zip Code to save..." ControlToValidate="txtZipCode" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Website" runat="server" Text="Website" MaxLength="60" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtWebsite"></asp:Label>
                                        <asp:TextBox ID="txtWebsite" runat="server" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="File" runat="server" Text="Company Logo" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="uplFile"></asp:Label>
                                        <asp:FileUpload ID="uplFile" runat="server" CssClass="btn btn-primary" />
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group statusGroup">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="rdoStatus"></asp:Label>
                                        <asp:RadioButtonList ID="rdoStatus" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Activate" Value="0" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="De Activate" Value="1"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Label1" runat="server" Text="User Name:" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtName"></asp:Label>
                                        <asp:TextBox ID="txtName" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Label5" runat="server" Text="Password:" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtPass"></asp:Label>
                                        <asp:TextBox ID="txtPass" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPass" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorPassword %>' ControlToValidate="txtPass" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Label6" runat="server" Text="Re-enter password:" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtPass1"></asp:Label>
                                        <asp:TextBox ID="txtPass1" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPass1" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorPassword %>' ControlToValidate="txtPass1" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Label7" runat="server" Text="No. Users:" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtUsers"></asp:Label>
                                        <asp:TextBox ID="txtUsers" runat="server" Width="100px" Text="5" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="reusers" runat="server" ErrorMessage="Please enter numeric value in No of Users fields..." SetFocusOnError="true" ControlToValidate="txtUsers" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$" Display="None" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Label10" runat="server" Text="Client Type:" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="ddlClientType"></asp:Label>
                                        <asp:DropDownList ID="ddlClientType" runat="server" SkinID="FormControl" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="Label9" runat="server" Text="Expiry Date :" ForeColor="Red" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtDate"></asp:Label>
                                        <asp:TextBox ID="txtDate" runat="server" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvDate" runat="server" ErrorMessage="Please enter date..." ControlToValidate="txtDate" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regexpDate" runat="server" ControlToValidate="txtDate" ValidationGroup="Save" Display="None" ErrorMessage="Please enter date in dd/mm/yyyy format..." ValidationExpression="^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((1[6-9]|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group companyTypeGroup">
                                        <asp:Label ID="Label8" runat="server" Text="Company Type :" SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="rdoList"></asp:Label>
                                        <asp:RadioButtonList ID="rdoList" runat="server" RepeatColumns="2" CssClass="table table-borderless">
                                            <asp:ListItem Text="Normal Company" Value="0" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Temporary Company" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Limited Access Company" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Read Only Company" Value="3"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-right mt-4 buttonActionGroup">
                            <asp:Button ID="Button5" runat="server" Text="Save" CssClass="btn btn-primary btn-min-200" ValidationGroup="Save" />
                            <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false" ShowMessageBox="true" ValidationGroup="Save" DisplayMode="BulletList" />
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
        <%-- <Triggers>
            <asp:PostBackTrigger ControlID="Button5"></asp:PostBackTrigger>
        </Triggers>--%>
    </asp:UpdatePanel>

    <script>$(document).ready(function () { $("#sidr").show(); });</script>
</asp:Content>
<%--<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MiddlePart">
    <div class="row">
        <div class="large-12 medium-12 column">
        </div>
    </div>
</asp:Content>--%>

