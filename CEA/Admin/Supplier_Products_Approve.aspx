﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Supplier_Products_Approve.aspx.vb" Inherits="CEA.FORM3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 <style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">

        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorImageFile") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdAlreadyAdded") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdAdded") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorProdNotFound") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdDeleted") %>';
            }
            else if (msgno == 6) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdUpdated") %>';
            }
            else if (msgno == 7) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorOtherMissing") %>';
            }
            alert(msgstring);
        }
        //        function ValidateList(source, args) {
        //            var chkListModules = document.getElementById('<%= chkList.ClientID %>');
        //            var chkListinputs = chkListModules.getElementsByTagName("input");
        //            for (var i = 0; i < chkListinputs.length; i++) {
        //                if (chkListinputs[i].checked) {
        //                    args.IsValid = true;
        //                    return;
        //                }
        //            }
        //            args.IsValid = false;
        //        }

        function checkBoxListOnClick(elementRef) {
            var textBoxRef = document.getElementById('<%= txtOther.ClientID %>');
            var checkBoxArray = elementRef.getElementsByTagName('input');

            var checkBoxRef = checkBoxArray[checkBoxArray.length - 1];
            if (checkBoxRef.checked == true) {
                textBoxRef.style.display = '';
            }
            else {
                textBoxRef.style.display = 'None';
            }
            return true;
        }
        function checkOtherValue(source, args) {
            var textBoxRef = document.getElementById('<%= txtOther.ClientID %>');
            if (textBoxRef.style.display == '' && textBoxRef.value == '') {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }

        function checkBoxListOnClick1(elementRef) {
            var textBoxRef = document.getElementById('<%= txtOther1.ClientID %>');
            var checkBoxArray = elementRef.getElementsByTagName('input');

            var checkBoxRef = checkBoxArray[checkBoxArray.length - 1];
            if (checkBoxRef.checked == true) {
                textBoxRef.style.display = '';
            }
            else {
                textBoxRef.style.display = 'None';
            }
            return true;
        }
        function checkOtherValue1(source, args) {
            var textBoxRef = document.getElementById('<%= txtOther1.ClientID %>');
            if (textBoxRef.style.display == '' && textBoxRef.value == '') {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }
        
    </script>
    <style>
        input[type="text"], button, .button
        {
            min-height: 1.9125rem;
            padding: 0.1rem;
            margin: 0;
            font-size: 0.7925rem !important;
        }
        .label, .label1, td, td label, td input, table
        {
            font-size: 0.7925rem !important;
            line-height: normal !important;
            padding: 0 !important;
            margin: 0 !important;
        }
        button, .button
        {
            padding-top: 0.375rem !important;
            padding-right: 1.0rem !important;
            padding-bottom: 0.3375rem !important;
        }
        button, .button</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label13" runat="server" Text='<%$ Resources:Resource, ProductDetail %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="panel">
                <%--                <div class="row">
                    <div class="large-6 medium-6 column" id="div3" runat="server">
                        <asp:LinkButton ID="LinkButton1" runat="server" Text='<%$ Resources:Resource, ViewProduct %>'></asp:LinkButton>
                    </div>
                </div>--%>
                <div class="row">
                    <div  id="div1" runat="server">
                        <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, ProductID %>'
                            CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtProdID" runat="server" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvID" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorID %>'
                            ControlToValidate="txtProdID" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, NameE %>' CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtNameE" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNameE" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNameE %>'
                            ControlToValidate="txtNameE" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, NameA %>' CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtNameA" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNameA" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNameA %>'
                            ControlToValidate="txtNameA" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        <asp:Label ID="Label4" runat="server" Text='<%$ Resources:Resource, DescriptionE %>'
                            CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtDescE" runat="server" TextMode="MultiLine" Rows="2" MaxLength="500"></asp:TextBox>
                        <asp:Label ID="Label5" runat="server" Text='<%$ Resources:Resource, DescriptionA %>'
                            CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtDescA" runat="server" TextMode="MultiLine" Rows="2" MaxLength="500"></asp:TextBox>
                        <asp:Label ID="Label6" runat="server" Text="File of Specifications" CssClass="label1"></asp:Label>
                        <asp:FileUpload ID="uplFile" runat="server" CssClass="small button " Text='<%$ Resources:Resource, Browse %>' />
                        <asp:Label ID="Label7" runat="server" Text='<%$ Resources:Resource, FeatE %>' CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtFeatE" runat="server" MaxLength="100"></asp:TextBox>
                        <asp:Label ID="Label8" runat="server" Text='<%$ Resources:Resource, FeatA %>' CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtFeatA" runat="server" MaxLength="100"></asp:TextBox>
                        <asp:Label ID="Label9" runat="server" Text='<%$ Resources:Resource, Image %>' CssClass="label1"></asp:Label>
                        <asp:FileUpload ID="uplImage" runat="server" CssClass="small button " Text='<%$ Resources:Resource, Browse %>' />
                        <asp:Label ID="Label10" runat="server" Text='<%$ Resources:Resource, Unit %>' CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtUnit" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="Label14" runat="server" Text='<%$ Resources:Resource, ManHour %>'
                            CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtManHour" runat="server" Text="0.00" MaxLength="15"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvManHour" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorManHour %>'
                            ControlToValidate="txtManHour" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="reManHour" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                            SetFocusOnError="true" ControlToValidate="txtManHour" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                            Display="None" ValidationGroup="Save"> </asp:RegularExpressionValidator>
                        <asp:Label ID="Label15" runat="server" Text='<%$ Resources:Resource, EquipHour %>'
                            CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtEquipHour" runat="server" Text="0.00" MaxLength="15"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEquip" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorEquip %>'
                            ControlToValidate="txtEquipHour" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="reEquip" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                            SetFocusOnError="true" ControlToValidate="txtEquipHour" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                            Display="None" ValidationGroup="Save"> </asp:RegularExpressionValidator>
                        <asp:Label ID="Label16" runat="server" Text='<%$ Resources:Resource, ApprovedBy %>'
                            CssClass="label1"></asp:Label>
                        <asp:CheckBoxList ID="chkList" runat="server" Font-Size="Smaller" Width="100%">
                        </asp:CheckBoxList>
                        <asp:TextBox ID="txtOther" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:CustomValidator ID="cvmodulelist" runat="server" Display="None" ErrorMessage='<%$ Resources:Resource, ErrorOtherMissing %>'
                            ClientValidationFunction="checkOtherValue" ValidationGroup="Save"></asp:CustomValidator>
                        <asp:Label ID="Label17" runat="server" Text='<%$ Resources:Resource, Specification %>'
                            CssClass="label1"></asp:Label>
                        <asp:CheckBoxList ID="chkSpec" runat="server" Font-Size="Smaller" Width="100%">
                        </asp:CheckBoxList>
                        <asp:TextBox ID="txtOther1" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:CustomValidator ID="chkOther1" runat="server" Display="None" ErrorMessage='<%$ Resources:Resource, ErrorOtherMissing1 %>'
                            ClientValidationFunction="checkOtherValue1" ValidationGroup="Save"></asp:CustomValidator>
                        <asp:Label ID="Label11" runat="server" Text='<%$ Resources:Resource, Status %>' CssClass="label1"></asp:Label>
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text='<%$ Resources:Resource, Pending %>' Value="0"></asp:ListItem>
                            <asp:ListItem Text='<%$ Resources:Resource, Approve %>' Value="1"></asp:ListItem>
                            <asp:ListItem Text='<%$ Resources:Resource, Reject %>' Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:Label ID="Label12" runat="server" Text='<%$ Resources:Resource, Comments %>'
                            CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </div>
                    <div class="large-6 medium-6 column" id="div2" runat="server">
                        <%--<asp:LinkButton ID="LinkButton1" runat="server" Text='<%$ Resources:Resource, ViewProduct %>'></asp:LinkButton>--%>
                        <asp:Image ID="Image1" runat="server" Width="90%" CssClass="ImageBox" />
                    </div>
            </asp:Panel>
            
            
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>
    </asp:UpdatePanel>

    <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
                ShowMessageBox="true" ValidationGroup="Save" DisplayMode="BulletList" />

</asp:Content>

<asp:Content ID="Content4" runat="server" contentplaceholderid="MiddlePart">

<div class="row">
           
                 <div class="large-12 medium-12 column" id="buttonsrow" runat="server">
                    <div class="outerDivBtn">
                        <div class="BTNdiv">
                    <asp:Button ID="btnSave" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave"
                        ValidationGroup="Save" />
                    <asp:Button ID="btnCancel" runat="server" Text='<%$ Resources:Resource, Cancel %>'
                        CssClass="BTNCancel" />
                </div></div>
            </div>
            </div>
</asp:Content>

