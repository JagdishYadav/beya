﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Template_Project.aspx.vb" Inherits="CEA.Template_Project" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProjSaved") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProjAlreadyExist") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SaveFirst") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorProjName") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "PleaseSelectOption") %>';
            }

            alert(msgstring);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, Project %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="">
                <div class="row">
                    <div class="col-12">
                        <div class="BTNdiv mb-4">
                            <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="btn btn-primary btn-min-120" Visible="false" />
                            <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Next %>' CssClass="btn btn-primary btn-min-120" />
                        </div>
                    </div>
                    <div class="col-12 col-lg col-xl order-lg-12">
                        <div class="text-right mt-lg-3 buttonActionGroup">
                            <div class="btn-group commanButtonGroup mb-3">
                                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-primary btn-min-120" Text='<%$ Resources:Resource, Open %>'></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-primary btn-min-120" Text='<%$ Resources:Resource, Create %>'></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn btn-primary btn-min-120" Text='<%$ Resources:Resource, CreateFromExisting %>'></asp:LinkButton>
                                <%--<asp:LinkButton ID="LinkButton4" runat="server" CssClass="BTNClient"
                            Text='Client Boq'></asp:LinkButton>--%>
                                <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn btn-primary btn-min-120" Text='<%$ Resources:Resource, FromClientBOQ %>'></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-5 col-xl-4 order-lg-1">
                        <div class="form-group mb-4">
                            <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, ProjectName %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtProjName"></asp:Label>
                            <asp:TextBox ID="txtProjName" runat="server" MaxLength="50" ReadOnly="true" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-7 col-lg-5" runat="server" id="searchbox">
                        <%--<asp:Label ID="Label15" runat="server" Text='<%$ Resources:Resource, Search %>' Width="100%"></asp:Label>--%>
                        <div class="input-group mb-4 commanSearchGroup">
                            <asp:TextBox ID="txtSearch" runat="server" SkinID="FormControl" CssClass="form-control" placeholder="Search"></asp:TextBox>
                            <div class="input-group-append">
                                <asp:Button ID="Button5" runat="server" Text='<%$ Resources:Resource, Search %>' CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 medium-12 column">
                        <div class="table-responsive tableLayoutWrap altboarder">
                            <asp:GridView ID="grdProjects" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="10" DataKeyNames="ID,Status,LangID,ProjectType"
                                SelectedRowStyle-BackColor="Yellow" HeaderStyle-HorizontalAlign="Center">
                                <Columns>
                                    <asp:BoundField DataField="Name" SortExpression="Name" HeaderText='<%$ Resources:Resource, ProjectName %>'
                                        ItemStyle-Width="30%" />
                                    <asp:BoundField DataField="CreationDate" SortExpression="CreationDate" HeaderText='<%$ Resources:Resource, CreationDate %>'
                                        ItemStyle-Width="20%" />
                                    <asp:BoundField DataField="CreatedBy" SortExpression="CreatedBy" HeaderText='<%$ Resources:Resource, CreatedBy %>'
                                        ItemStyle-Width="20%" />
                                    <asp:BoundField DataField="IsComplete" SortExpression="IsComplete" HeaderText='<%$ Resources:Resource, Status %>'
                                        ItemStyle-Width="20%" />
                                    <asp:CommandField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%" ShowSelectButton="true" SelectText='<%$ Resources:Resource, Select %>' />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <%--<div class="row">
                    <div class="large-2 medium-2 column">
                        <asp:Label ID="Label11" runat="server" CssClass="label" Text='<%$ Resources:Resource, MarkProject %>'></asp:Label>
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal"
                            Width="100%">
                            <asp:ListItem Text='<%$ Resources:Resource, Private %>' Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text='<%$ Resources:Resource, Public %>' Value="1"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>--%>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script>$(document).ready(function () { $("#sidr").show(); });</script>
</asp:Content>

<%--<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MiddlePart">
    <div class="row">
        <div class="large-12 medium-12 column">
            <div class="outerDivBtn">
                <div class="BTNdiv">
                    <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave" Visible="false" />
                    <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Next %>' CssClass="BTNnext" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>--%>

