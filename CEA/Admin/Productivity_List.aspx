﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Productivity_List.aspx.vb" Inherits="CEA.Productivity_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        function ShowConfirm() {
            var strConfirm = '<%=GetGlobalResourceObject("Resource", "ConfirmDelete") %>';
            return confirm(strConfirm);
        }
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Success") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "UpdateSuccess") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteSuccess") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorAlreadyExist") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorNotExist") %>';
            }

            alert(msgstring);
        }
    </script>
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
User Management
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="panel">
                <%--<div class="row">
            <div class="large-12 columns">--%>
               
                <div class="row">
                    <div class="large-6 medium-6 column">
                        <asp:Label ID="Label15" runat="server" Text="Search" Width="100%"></asp:Label>
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="left" Width="70%"></asp:TextBox><asp:Button
                            ID="Button5" runat="server" Text="Search" CssClass="BTNSearch" />
                    </div>
                     <div  >
                    
                </div>
                </div>
                <div style="width: 100%; height: 600px; overflow: scroll">
                        <asp:GridView ID="grdLabours" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                            AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="10" DataKeyNames="Code" HeaderStyle-HorizontalAlign ="Center">
                            <Columns>
                                <asp:BoundField DataField="Code" SortExpression="Code" HeaderText='<%$ Resources:Resource, Code %>'
                                    ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                                <asp:BoundField DataField="NameE" SortExpression="NameE" HeaderText='<%$ Resources:Resource, NameE %>'
                                    ItemStyle-Width="100px" ItemStyle-Wrap="true" />
                                <asp:BoundField DataField="NameA" SortExpression="NameA" HeaderText='<%$ Resources:Resource, NameA %>'
                                    ItemStyle-Width="100px" ItemStyle-Wrap="true" />

                                <asp:BoundField DataField="DescE" SortExpression="DescE" HeaderText='<%$ Resources:Resource, DescriptionE %>'
                                    ItemStyle-Width="200px" ItemStyle-Wrap="true" />
                                <asp:BoundField DataField="DescA" SortExpression="DescA" HeaderText='<%$ Resources:Resource, DescriptionA %>'
                                    ItemStyle-Width="200px" ItemStyle-Wrap="true" />
                                <asp:BoundField DataField="DailyOutput" SortExpression="DailyOutput" HeaderText='<%$ Resources:Resource, DailyOutput %>'
                                    ItemStyle-Width="50px" ItemStyle-Wrap="true" />
                                <asp:CommandField ShowCancelButton="false" ShowDeleteButton="false" ShowEditButton="false"
                                    ShowInsertButton="false" ItemStyle-Width="25px" ShowSelectButton="true" SelectText='<%$ Resources:Resource, ViewDetails %>' />
                            </Columns>
                        </asp:GridView>
                    </div>
                <%-- </div>
        </div>--%>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" runat="server" contentplaceholderid="MiddlePart">
 <div class="row">
                    <div class="large-10 medium-10 column" >
<asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, AddProductivity %>' CssClass="BTNAdd" />
</div>
</div>
</asp:Content>

<asp:Content ID="Content5" runat="server" contentplaceholderid="PageTitle">
  <asp:Label ID="lblHeader" runat="server" Text='<%$ Resources:Resource, ProductivityList %>'></asp:Label>
</asp:Content>


