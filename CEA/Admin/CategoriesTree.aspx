﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="CategoriesTree.aspx.vb" Inherits="CEA.CategoriesTree" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        var _isInitialLoad = true;
        function pageLoad(sender, args) {
            if (_isInitialLoad) {
                _isInitialLoad = false;
                document.getElementById("<%= Button8.ClientID %>").click();
                
                
            }
        }

        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorSelectNode") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteChild") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteSuccess") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorProdNotFound") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "HierarchyAlreadyExist") %>';
            }
            else if (msgno == 6) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorAddHierarchy") %>';
            }
            else if (msgno == 7) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "HierarchyAdded") %>';
            }
            else if (msgno == 8) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorCantAdd") %>';
            }
            else if (msgno == 9) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NonLeafNode") %>';
            }
            else if (msgno == 10) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "HasChildren") %>';
            }
            else if (msgno == 11) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NoRecFound") %>';
            }
            else if (msgno == 12) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ValueMissing") %>';
            }
            else if (msgno == 13) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "NotExcel") %>';
            }
            else if (msgno == 14) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Imported") %>';
            }
            else if (msgno == 15) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ImportFailure") %>';
            }
            alert(msgstring);
        }

    </script>
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <%--<div class="update" id = "abc" runat = "server"></div>--%>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="large-7 medium-7 columns" id="div1" runat="server" style="width: 100%;">
                    <asp:TreeView ID="tvLevels" runat="server" Width="100%" LineImagesFolder="~/TreeLineImages"
                        Visible="false" ShowLines="True" NodeWrap = "true" ShowCheckBoxes="None">
                    </asp:TreeView>
                </div>
            </div>
            <div class="row">
                <div class="large-7 medium-7 columns" id="div2" runat="server" style="width: 100%;">
                    <asp:Button ID="Button15" runat="server" Text='<%$ Resources:Resource, AddQuote %>'
                        CssClass="small button success" Width="100%" />
                </div>
            </div>
            <asp:Button ID="Button8" runat="server" Text="" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hdnResult" runat="server" Value="-1" />
    <asp:HiddenField ID="hdnInitialLoad" runat="server" Value="1" />
</asp:Content>
