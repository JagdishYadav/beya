﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Company_List.aspx.vb" Inherits="CEA.Company_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        function ShowConfirm() {
            var strConfirm = '<%=GetGlobalResourceObject("Resource", "ConfirmDelete") %>';
            return confirm(strConfirm);
        }
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Success") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "UpdateSuccess") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteSuccess") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorAlreadyExist") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorNotExist") %>';
            }

            alert(msgstring);
        }
    </script>
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
User Management
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server">
                <%--<div class="row">
            <div class="large-12 columns">--%>

                <div class="row">
                    <div class="col-12 col-md-7 col-lg-5">
                        <%--<asp:Label ID="Label15" runat="server" Text="Search" Width="100%"></asp:Label>--%>
                        <div class="input-group mb-5 commanSearchGroup">
                            <asp:TextBox ID="txtSearch" runat="server" SkinID="FormControl" CssClass="form-control" placeholder="Search"></asp:TextBox>
                            <div class="input-group-append">
                                <asp:Button ID="Button5" runat="server" Text="Search" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div style="width: 100%; height: 600px; overflow: scroll">--%>
                <div class="table-responsive tableLayoutWrap altboarder">
                    <asp:GridView ID="grdEquipmet" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="15" DataKeyNames="ID" HeaderStyle-HorizontalAlign="Center">
                        <Columns>
                            <asp:BoundField DataField="Name" SortExpression="Name" HeaderText='<%$ Resources:Resource, Name %>'
                                ItemStyle-Width="120px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="UsersCount" SortExpression="UsersCount" HeaderText='<%$ Resources:Resource, NoofUsers %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="ExpiryDate" SortExpression="ExpiryDate" HeaderText='<%$ Resources:Resource, ExpiryDate %>'
                                ItemStyle-Width="50px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Phone" SortExpression="Phone" HeaderText='<%$ Resources:Resource, Phone %>'
                                ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Fax" SortExpression="Fax"
                                HeaderText='<%$ Resources:Resource, Fax %>' ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="POBox" SortExpression="POBox" HeaderText='<%$ Resources:Resource, POBox %>'
                                ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="ZipCode" SortExpression="ZipCode" HeaderText='<%$ Resources:Resource, ZipCode %>'
                                ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Email" SortExpression="Email" HeaderText='<%$ Resources:Resource, Email %>'
                                ItemStyle-Width="100px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Status" SortExpression="Status" HeaderText='<%$ Resources:Resource, Status %>'
                                ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:CommandField ShowCancelButton="false" ShowDeleteButton="false" ShowEditButton="true"
                                ShowInsertButton="false" ItemStyle-Width="25px" ShowSelectButton="false" EditImageUrl="../Images/tableedit.png" ButtonType="Image"  />
                            <asp:CommandField ShowCancelButton="false" ShowDeleteButton="true" ShowEditButton="false"
                                ShowInsertButton="false" ItemStyle-Width="25px" ShowSelectButton="false" DeleteImageUrl="../Images/tabledelete.png" ButtonType="Image" />
                        </Columns>
                    </asp:GridView>
                </div>
                <%-- </div>
        </div>--%>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

<%--<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MiddlePart">
</asp:Content>--%>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="PageTitle">
    <asp:Label ID="lblHeader" runat="server" Text='<%$ Resources:Resource, CompanyList %>'></asp:Label>
    <script>$(document).ready(function () { $("#sidr").show(); });</script>
</asp:Content>


