﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="ClientType_New.aspx.vb" Inherits="CEA.ClientType_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">

        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Success") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "UpdateSuccess") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteSuccess") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "TypeAlreadyExist") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorNotExist") %>';
            }
            else if (msgno == 6) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorOtherMissing") %>';
            }
            else if (msgno == 7) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorSuppAlreadyExist") %>';
            }
            else if (msgno == 8) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorPassword") %>';
            }
            else if (msgno == 9) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorUserName") %>';
            }
            alert(msgstring);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, AddClientType %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="form-group mb-4">
                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, ClientTypeNameE %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtNameE"></asp:Label>
                        <asp:TextBox ID="txtNameE" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvID" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNameE %>'
                            ControlToValidate="txtNameE" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="form-group mb-4">
                        <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, ClientTypeNameA %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtNameA"></asp:Label>
                        <asp:TextBox ID="txtNameA" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNameA %>'
                            ControlToValidate="txtNameA" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>

            <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
                ShowMessageBox="true" ValidationGroup="Save" DisplayMode="BulletList" />

            <div class="text-right mt-4 buttonActionGroup">
                <div class="btn-group commanButtonGroup mb-3">
                    <asp:Button ID="btnSave" runat="server" Text='<%$ Resources:Resource, Save %>' ValidationGroup="Save" CssClass="btn btn-primary btn-min-120" />
                    <asp:Button ID="Button8" runat="server" Text='<%$ Resources:Resource, Cancel %>' CssClass="btn btn-primary btn-min-120" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>$(document).ready(function () { $("#sidr").show(); });</script>
</asp:Content>
<%--<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MiddlePart">
    <div class="row">
        <div class="large-12 medium-12 column">
            <div class="outerDivBtn">
                <div class="BTNdiv">
                    <asp:Button ID="btnSave" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave"
                        ValidationGroup="Save" />
                    <asp:Button ID="Button8" runat="server" Text='<%$ Resources:Resource, Cancel %>'
                        CssClass="BTNCancel" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>--%>

