﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="NewLabourCategory.aspx.vb" Inherits="CEA.NewLabourCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">

        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Success") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "UpdateSuccess") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteSuccess") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorAlreadyExist") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorNotExist") %>';
            }

            alert(msgstring);
        }

        function CalculateTotalCost() {
            var totCost = 0;

            if (!isNaN(parseFloat(document.getElementById('<%=txtGenExp.ClientID%>').value))) {
                totCost = totCost + parseFloat(document.getElementById('<%=txtGenExp.ClientID%>').value);
            }
            if (!isNaN(parseFloat(document.getElementById('<%=txtAvgSal.ClientID%>').value))) {
                totCost = totCost + parseFloat(document.getElementById('<%=txtAvgSal.ClientID%>').value);
            }
            if (!isNaN(parseFloat(document.getElementById('<%=txtHousing.ClientID%>').value))) {
                totCost = totCost + parseFloat(document.getElementById('<%=txtHousing.ClientID%>').value);
            }
            if (!isNaN(parseFloat(document.getElementById('<%=txtFood.ClientID%>').value))) {
                totCost = totCost + parseFloat(document.getElementById('<%=txtFood.ClientID%>').value);
            }
            if (!isNaN(parseFloat(document.getElementById('<%=txtMedIns.ClientID%>').value))) {
                totCost = totCost + parseFloat(document.getElementById('<%=txtMedIns.ClientID%>').value);
            }
            if (!isNaN(parseFloat(document.getElementById('<%=txtSecIns.ClientID%>').value))) {
                totCost = totCost + parseFloat(document.getElementById('<%=txtSecIns.ClientID%>').value);
            }
            if (!isNaN(parseFloat(document.getElementById('<%=txtTicket.ClientID%>').value))) {
                totCost = totCost + parseFloat(document.getElementById('<%=txtTicket.ClientID%>').value);
            }
            if (!isNaN(parseFloat(document.getElementById('<%=txtEOCont.ClientID%>').value))) {
                totCost = totCost + parseFloat(document.getElementById('<%=txtEOCont.ClientID%>').value);
            }
            if (!isNaN(parseFloat(document.getElementById('<%=txtIqama.ClientID%>').value))) {
                totCost = totCost + parseFloat(document.getElementById('<%=txtIqama.ClientID%>').value);
            }

            document.getElementById('<%=hdnTotCost.ClientID%>').value = totCost;
            document.getElementById('<%=lblTotSal.ClientID%>').innerHTML = '<%=GetGlobalResourceObject("Resource", "TotSal") %>' + " : " + totCost;
        }
    </script>
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
New Labour Category
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="newLabourWrapper">

                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="form-group mb-4">
                            <asp:Label ID="lblCode" runat="server" Text='<%$ Resources:Resource, Code %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtCode"></asp:Label>
                            <asp:TextBox ID="txtCode" runat="server" MaxLength="20" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorCode %>'
                                ControlToValidate="txtCode" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group mb-4">
                            <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, ProfE %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtProfE"></asp:Label>
                            <asp:TextBox ID="txtProfE" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvProfE" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorProfE %>'
                                ControlToValidate="txtProfE" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group mb-4">
                            <asp:Label ID="Label11" runat="server" Text='<%$ Resources:Resource, ProfA %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtProfA"></asp:Label>
                            <asp:TextBox ID="txtProfA" runat="server" MaxLength="50" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvProfA" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorProfA %>'
                                ControlToValidate="txtProfA" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group mb-4">
                            <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, GenExp %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtGenExp"></asp:Label>
                            <asp:TextBox ID="txtGenExp" runat="server" MaxLength="18" Text="0.00" onblur="CalculateTotalCost(); return false;" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reGenExp" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtGenExp" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Save">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group mb-4">
                            <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, AvgSal %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtAvgSal"></asp:Label>
                            <asp:TextBox ID="txtAvgSal" runat="server" MaxLength="18" Text="0.00" onblur="CalculateTotalCost(); return false;" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reAvgSal" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtAvgSal" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Save">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group mb-4">
                            <asp:Label ID="Label4" runat="server" Text='<%$ Resources:Resource, Housing %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtHousing"></asp:Label>
                            <asp:TextBox ID="txtHousing" runat="server" MaxLength="18" Text="0.00" onblur="CalculateTotalCost(); return false;" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reHousing" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtHousing" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Save">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group mb-4">
                            <asp:Label ID="Label5" runat="server" Text='<%$ Resources:Resource, Food %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtFood"></asp:Label>
                            <asp:TextBox ID="txtFood" runat="server" MaxLength="18" Text="0.00" onblur="CalculateTotalCost(); return false;" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reFood" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtFood" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Save">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group mb-4">
                            <asp:Label ID="Label6" runat="server" Text='<%$ Resources:Resource, MedIns %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtMedIns"></asp:Label>
                            <asp:TextBox ID="txtMedIns" runat="server" MaxLength="18" Text="0.00" onblur="CalculateTotalCost(); return false;" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reMedIns" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtMedIns" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Save">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group mb-4">
                            <asp:Label ID="Label7" runat="server" Text='<%$ Resources:Resource, SecIns %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtSecIns"></asp:Label>
                            <asp:TextBox ID="txtSecIns" runat="server" MaxLength="18" Text="0.00" onblur="CalculateTotalCost(); return false;" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reSecInx" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtSecIns" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Save">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group mb-4">
                            <asp:Label ID="Label8" runat="server" Text='<%$ Resources:Resource, Ticket %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtTicket"></asp:Label>
                            <asp:TextBox ID="txtTicket" runat="server" MaxLength="18" Text="0.00" onblur="CalculateTotalCost(); return false;" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reTicket" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtTicket" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Save">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group mb-4">
                            <asp:Label ID="Label9" runat="server" Text='<%$ Resources:Resource, EOFCont %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtEOCont"></asp:Label>
                            <asp:TextBox ID="txtEOCont" runat="server" MaxLength="18" Text="0.00" onblur="CalculateTotalCost(); return false;" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reEOCont" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtEOCont" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Save">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group mb-4">
                            <asp:Label ID="Label10" runat="server" Text='<%$ Resources:Resource, Iqama %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtIqama"></asp:Label>
                            <asp:TextBox ID="txtIqama" runat="server" MaxLength="18" Text="0.00" onblur="CalculateTotalCost(); return false;" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reIqama" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtIqama" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Save">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group mb-4">
                            <asp:Label ID="Label12" runat="server" Text='<%$ Resources:Resource, Nat %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="ddlNat"></asp:Label>
                            <asp:DropDownList ID="ddlNat" runat="server" SkinID="FormControl" CssClass="form-control"></asp:DropDownList>
                            <%--<asp:RequiredFieldValidator ID="rfvNat" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNat %>'
                    ControlToValidate="txtNat" SetFocusOnError="true" ValidationGroup="Save"
                    Display="None"></asp:RequiredFieldValidator>--%>
                        </div>
                    </div>
                    <div class="col-12 ">
                        <div class="form-group mb-4">
                            <asp:Label ID="lblTotSal" runat="server" Text="" CssClass="label1" Font-Bold="true"
                                Font-Size="Larger" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>

                <div class="text-right mt-4 buttonActionGroup">
                    <div class="btn-group commanButtonGroup mb-3">
                        <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Save %>' ValidationGroup="Save" CssClass="btn btn-primary btn-min-120" />
                        <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Cancel %>' CssClass="btn btn-primary btn-min-120" />
                        <asp:HiddenField ID="hdnTotCost" runat="server" Value="0" />
                        <asp:Button ID="btnNew" runat="server" Text='<%$ Resources:Resource, New %>' CssClass="btn btn-primary btn-min-120" />
                    </div>
                </div>

            </asp:Panel>
            <div class="large-9 columns">
                <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
                    ShowMessageBox="true" ValidationGroup="Save" DisplayMode="BulletList" />
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    <script>$(document).ready(function () { $("#sidr").show(); });</script>
</asp:Content>
<%--<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MiddlePart">
    <div class="row">
        <div class="large-12 medium-12 column">
            <div class="outerDivBtn">
                <div class="BTNdiv">
                    <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave" ValidationGroup="Save" />
                    <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Cancel %>' CssClass="BTNCancel" />
                    <asp:HiddenField ID="hdnTotCost" runat="server" Value="0" />
                    <asp:Button ID="btnNew" runat="server" Text='<%$ Resources:Resource, New %>' CssClass="BTNEdit" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>--%>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="PageTitle">
    <asp:Label ID="lblHeader" runat="server" Text='<%$ Resources:Resource, CreateLabour %>'></asp:Label>
</asp:Content>


