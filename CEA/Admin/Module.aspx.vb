﻿Public Class _Module
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                LinkButton1.Visible = False
                LinkButton2.Visible = False
                Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                Menuhide.Value = "close"

                Dim linkButton As LinkButton = Master.FindControl("LinkButton1")
                linkButton.Visible = False

                Dim dt As DataTable
                Dim configClass As New ConfigClassSqlServer()
                If (Session("UserName") IsNot Nothing) Then
                    dt = configClass.GetUserModules(Session("UserName").ToString())
                End If
                Dim lblWelcome As Label = Master.FindControl("Welcome")
                If (lblWelcome IsNot Nothing And Session("UserName") IsNot Nothing) Then
                    lblWelcome.Text = "Welcome " + Session("UserName").ToString()
                End If
                If (dt.Rows.Count > 0) Then
                    If (dt.Rows(0).Item(1).ToString().Equals("1")) Then
                        LinkButton1.Visible = True
                    ElseIf (dt.Rows(0).Item(1).ToString().Equals("2")) Then
                        LinkButton2.Visible = True
                    End If

                    Dim lblloginAs As Label = Master.FindControl("loginAs")
                    If (lblloginAs IsNot Nothing) Then
                        lblloginAs.Text = "You are login as " + dt.Rows(0).Item(0).ToString()
                    End If
                End If
                If (dt.Rows.Count > 1) Then
                    For i As Integer = 0 To dt.Rows.Count - 1
                        If (dt.Rows(i).Item(1).ToString().Equals("1")) Then
                            LinkButton1.Visible = True
                        ElseIf (dt.Rows(i).Item(1).ToString().Equals("2")) Then
                            LinkButton2.Visible = True
                        End If
                    Next
                    
                End If

                'If (Session("IsTemp").ToString().Equals("1")) Then
                '    LinkButton1.Visible = False
                'End If
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
    End Sub
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        Response.Redirect("~/Admin/UserManagement.aspx")
    End Sub
    Protected Sub LinkButton2_Click(sender As Object, e As EventArgs) Handles LinkButton2.Click
        Response.Redirect("~/CES/Project_Home.aspx")
    End Sub
End Class