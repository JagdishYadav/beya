﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="CrewStructure.aspx.vb" Inherits="CEA.CrewStructure" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
     <style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        function ShowMessage(msgno) {
            if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorSelectProf") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProfAlreadyAdded") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProfAdded") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdRateMissing") %>';
            }

            alert(msgstring);
        }
        
    </script>
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Crew Structure
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        <div class="large-6 medium-6 columns">
            <div class="row">
                <div class="large-12 medium-12 columns">
                    <h5>
                        <asp:Label ID="lblManpower" runat="server" Text='<%$ Resources:Resource, Manpower %>'></asp:Label>
                         <asp:Label ID="Label6" runat="server" Text='<%$ Resources:Resource, TotLabCat %>'
                        CssClass="label"></asp:Label>
                    <asp:Label ID="lblTotLabour" runat="server" Text="0" CssClass="label"></asp:Label>
                        </h5>
                </div>
               
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <asp:Panel ID="Panel1" runat="server" CssClass="panel">
                    <div class="row">
                     <div class="large-12 medium-12 column">
                   <asp:Label ID="Label15" runat="server" Text="Search" Width="100%"></asp:Label>                            
                            <asp:TextBox ID="txtSearch1" runat="server" CssClass="left" Width="70%"></asp:TextBox><asp:Button ID="Button5" runat="server" Text="Search" CssClass="BTNSearch" />
                </div>
                    </div>
                        <div style="width: 100%; height: 400px; overflow: scroll">
                       
                            <asp:GridView ID="grdLabours" runat="server" AutoGenerateColumns="false" AllowPaging="true" style="min-height:375px"
                                AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="5" DataKeyNames="Code" HeaderStyle-HorizontalAlign ="Center">
                                <Columns>
                                    <asp:BoundField DataField="Code" SortExpression="Code" HeaderText='<%$ Resources:Resource, Code %>'
                                        ItemStyle-Width="40px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="ProfE" SortExpression="ProfE" HeaderText='<%$ Resources:Resource, ProfE %>'
                                        ItemStyle-Width="45px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="ProfA" SortExpression="ProfA" HeaderText='<%$ Resources:Resource, ProfA %>'
                                        ItemStyle-Width="45px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="GenExp" SortExpression="GenExp" HeaderText='<%$ Resources:Resource, GenExp %>'
                                        ItemStyle-Width="25px" ItemStyle-Wrap="true" Visible = "false" />
                                    <asp:BoundField DataField="AvgSalary" SortExpression="AvgSalary" HeaderText='<%$ Resources:Resource, AvgSal %>'
                                        ItemStyle-Width="25px" ItemStyle-Wrap="true" Visible = "false"/>
                                    <asp:BoundField DataField="Housing" SortExpression="Housing" HeaderText='<%$ Resources:Resource, Housing %>'
                                        ItemStyle-Width="25px" ItemStyle-Wrap="true" Visible = "false"/>
                                    <asp:BoundField DataField="Food" SortExpression="Food" HeaderText='<%$ Resources:Resource, Food %>'
                                        ItemStyle-Width="25px" ItemStyle-Wrap="true" Visible = "false"/>
                                    <asp:BoundField DataField="MedInsurance" SortExpression="MedInsurance" HeaderText='<%$ Resources:Resource, MedIns %>'
                                        ItemStyle-Width="25px" ItemStyle-Wrap="true" Visible = "false" />
                                    <asp:BoundField DataField="SecInsurance" SortExpression="SecInsurance" HeaderText='<%$ Resources:Resource, SecIns %>'
                                        ItemStyle-Width="25px" ItemStyle-Wrap="true" Visible = "false"/>
                                    <asp:BoundField DataField="Ticket" SortExpression="Ticket" HeaderText='<%$ Resources:Resource, Ticket %>'
                                        ItemStyle-Width="25px" ItemStyle-Wrap="true" Visible = "false"/>
                                    <asp:BoundField DataField="EOContract" SortExpression="EOContract" HeaderText='<%$ Resources:Resource, EOFCont %>'
                                        ItemStyle-Width="25px" ItemStyle-Wrap="true" Visible = "false"/>
                                    <asp:BoundField DataField="Iqama" SortExpression="Iqama" HeaderText='<%$ Resources:Resource, Iqama %>'
                                        ItemStyle-Width="25px" ItemStyle-Wrap="true" Visible = "false"/>
                                    <asp:BoundField DataField="Nationality" SortExpression="Nationality" HeaderText='<%$ Resources:Resource, Nat %>'
                                        ItemStyle-Width="25px" ItemStyle-Wrap="true" Visible = "false"/>
                                    <asp:BoundField DataField="HourlyRate" SortExpression="HourlyRate" HeaderText='<%$ Resources:Resource, HourlyRate %>'
                                        ItemStyle-Width="25px" ItemStyle-Wrap="true" />
                                    <asp:CommandField ItemStyle-HorizontalAlign ="Center" ShowCancelButton="false" ShowDeleteButton="false" ShowEditButton="false"
                                        ShowSelectButton="true" ShowInsertButton="false" ItemStyle-Width="25px" SelectText='<%$ Resources:Resource, Select %>' />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            </div>
            <div class="large-6 medium-6 columns">
            <div class="row">
                <div class="large-12 medium-12 columns">
                    <h5>
                        <asp:Label ID="lblEquip" runat="server" Text='<%$ Resources:Resource, Equipment %>'> </asp:Label>
                         <asp:Label ID="Label7" runat="server" Text='<%$ Resources:Resource, TotEquip %>'
                        CssClass="label"></asp:Label>
                    <asp:Label ID="lblTotEquip" runat="server" Text="0" CssClass="label"></asp:Label>
                        </h5>
                </div>
               
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <asp:Panel ID="Panel2" runat="server" CssClass="panel">
                       
                      <div class="row">
                       <div class="large-6 medium-6 column">
<asp:Label ID="Label4" runat="server" Text="Search" Width="100%"></asp:Label>                            
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="left" Width="70%"></asp:TextBox><asp:Button ID="Button1" runat="server" Text="Search" CssClass="BTNSearch" />
                </div>
                      </div>
                       <div style="width: 100%; height: 400px; overflow: scroll">
                            <asp:GridView ID="grdEquipmet" runat="server" AutoGenerateColumns="false" AllowPaging="true" style="min-height:375px"
                                AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="5" DataKeyNames="Code" HeaderStyle-HorizontalAlign ="Center">
                                <Columns>
                                    <asp:BoundField DataField="Code" SortExpression="Code" HeaderText='<%$ Resources:Resource, Code %>'
                                        ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="DescE" SortExpression="DescE" HeaderText='<%$ Resources:Resource, DescriptionE %>'
                                        ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="DescA" SortExpression="DescA" HeaderText='<%$ Resources:Resource, DescriptionA %>'
                                        ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="Unit" SortExpression="Unit" HeaderText='<%$ Resources:Resource, Unit %>'
                                        ItemStyle-Width="60px" ItemStyle-Wrap="true" Visible = "false" />
                                    <asp:BoundField DataField="OperationCost" SortExpression="OperationCost" HeaderText='<%$ Resources:Resource, OperationCost %>'
                                        ItemStyle-Width="60px" ItemStyle-Wrap="true" Visible = "false"/>
                                    <asp:BoundField DataField="EquipmentDepCost" SortExpression="EquipmentDepCost" HeaderText='<%$ Resources:Resource, DepCost %>'
                                        ItemStyle-Width="60px" ItemStyle-Wrap="true" Visible = "false"/>
                                    <asp:BoundField DataField="EquipmentDailyCost" SortExpression="EquipmentDailyCost"
                                        HeaderText='<%$ Resources:Resource, DailyCost %>' ItemStyle-Width="60px" ItemStyle-Wrap="true" Visible = "false"/>
                                    <asp:BoundField DataField="RentalCostD" SortExpression="RentalCostD" HeaderText='<%$ Resources:Resource, RentalCostDay %>'
                                        ItemStyle-Width="60px" ItemStyle-Wrap="true" Visible = "false"/>
                                    <asp:BoundField DataField="RentalCostM" SortExpression="RentalCostM" HeaderText='<%$ Resources:Resource, RentalCostMonth %>'
                                        ItemStyle-Width="60px" ItemStyle-Wrap="true" Visible = "false"/>
                                    <asp:BoundField DataField="HourlyRate" SortExpression="HourlyRate" HeaderText='<%$ Resources:Resource, HourlyRate %>'
                                        ItemStyle-Width="30px" ItemStyle-Wrap="true"/>
                                    <asp:CommandField ItemStyle-HorizontalAlign ="Center" ShowCancelButton="false" ShowDeleteButton="false" ShowEditButton="false"
                                        ShowInsertButton="false" ItemStyle-Width="25px" ShowSelectButton="true" SelectText='<%$ Resources:Resource, Select %>' />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            </div>
            <div class="row">
                <div class="large-10 medium-10 columns">
                    
                </div>
                
            </div>
            <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
                ShowMessageBox="true" ValidationGroup="Add" DisplayMode="BulletList" />
            <asp:HiddenField ID="hdnSelection" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" runat="server" contentplaceholderid="MiddlePart">
    <div class="clear"></div>


                        <div  id = "div1" runat = "server">
                            <asp:Label ID="lblUser" runat="server" Text='<%$ Resources:Resource, Code %>' CssClass="label1"></asp:Label>
                            <asp:TextBox ID="txtCode" runat="server" ReadOnly="true"></asp:TextBox>
                        </div>

                        <div  id = "div2" runat = "server">
                            <asp:Label ID="lblPassword" runat="server" Text='<%$ Resources:Resource, HourlyRate %>'
                                CssClass="label1"></asp:Label>
                            <asp:TextBox ID="txtHourlyRate" runat="server" ReadOnly="true"></asp:TextBox>
                        </div>
                       
                        <div>
                            <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, DescriptionE %>'
                                CssClass="label1"></asp:Label>
                            <asp:TextBox ID="txtDescE" runat="server" ReadOnly="true"></asp:TextBox>
                            <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, DescriptionA %>'
                                CssClass="label1"></asp:Label>
                            <asp:TextBox ID="txtDescA" runat="server" ReadOnly="true"></asp:TextBox>
                        </div>
                      
                        <div  id = "div3" runat = "server">
                            <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, ProductionRate %>'
                                CssClass="label1"></asp:Label>
                            <asp:TextBox ID="txtProdRate" runat="server" Text="0.00" MaxLength="18"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reProdrate" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtProdRate" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Add">
                            </asp:RegularExpressionValidator>
                        </div>
                        <div >
                  
                    <asp:Button ID="btnAdd" runat="server" Text='<%$ Resources:Resource, Add %>' CssClass="BTNAdd "
                        ValidationGroup="Add"  />
                        
                    <asp:Button ID="Button4" runat="server" Text='<%$ Resources:Resource, Close %>' CssClass="BTNclose "
                         />
                </div>
              
                  
                    <div class="clear"></div>
</asp:Content>

