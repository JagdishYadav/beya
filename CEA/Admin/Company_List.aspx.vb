﻿Public Class Company_List
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (menuItems.Count > 15) Then
                    menuItems.RemoveAt(12)
                    menuItems.RemoveAt(12)
                    menuItems.RemoveAt(12)
                    menuItems.RemoveAt(12)
                End If
                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    'header1.Style.Add("float", "right")
                    'header2.Style.Add("float", "right")
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
        LoadDataGrid()
        Panel1.Width = grdEquipmet.Width
    End Sub
    Private Sub LoadDataGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        dt = objSqlConfig.GetCompaniesAll(txtSearch.Text)
        If (dt.Rows.Count > 0) Then
            grdEquipmet.DataSource = dt
            grdEquipmet.DataBind()
        Else
            grdEquipmet.DataSource = Nothing
            grdEquipmet.DataBind()
        End If
    End Sub
    Protected Sub grdSupplier_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdEquipmet.Sorting
        If (ViewState("SortState") IsNot Nothing And ViewState("SortState") = SortDirection.Ascending) Then
            ViewState("SortDirection") = "asc"
            ViewState("SortState") = 1
        Else
            ViewState("SortDirection") = "desc"
            ViewState("SortState") = 0
        End If
        ViewState("SortExp") = e.SortExpression
        FillGrid()
    End Sub
    Protected Sub grdSupplier_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdEquipmet.PageIndexChanging
        grdEquipmet.PageIndex = e.NewPageIndex
        grdEquipmet.DataBind()
    End Sub
    Private Sub FillGrid()
        Dim Data As DataTable = grdEquipmet.DataSource
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp") Is Nothing) Then
                If ViewState("SortDirection").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp") & " " & "desc"
                End If

                grdEquipmet.DataSource = dv
                grdEquipmet.DataBind()
            End If
        End If
    End Sub
    Protected Sub grdSupplier_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdEquipmet.RowEditing
        'Dim objSqlConfig As New ConfigClassSqlServer()
        'Dim dt As DataTable = objSqlConfig.GetEquipmentByCode(grdEquipmet.DataKeys(e.NewEditIndex).Value.ToString())
        'If (dt.Rows.Count > 0) Then
        '    Response.Redirect("Register.aspx?ID=" + dt.Rows(0).Item(0).ToString())
        'End If
        Response.Redirect("Register.aspx?ID=" + grdEquipmet.DataKeys(e.NewEditIndex).Value.ToString())
    End Sub
    Protected Sub grdSupplier_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdEquipmet.RowDeleting
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim retVal As Integer = objSqlConfig.DeleteCompany(grdEquipmet.DataKeys(e.RowIndex).Value.ToString())
        If (retVal = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
            LoadDataGrid()
        End If
    End Sub
    Protected Sub grdSupplier_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdEquipmet.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            For Each link As ImageButton In e.Row.Cells(10).Controls.OfType(Of ImageButton)()
                If link.CommandName = "Delete" Then
                    link.Attributes("onclick") = "if (ShowConfirm() == false) { return false; };"
                End If
            Next

            e.Row.Cells(2).Text = DateTime.ParseExact(e.Row.Cells(2).Text, "yyyyMMddHHmmss", Nothing).ToShortDateString()

            If (e.Row.Cells(8).Text = "0") Then
                e.Row.Cells(8).Text = "Activated"
            Else
                e.Row.Cells(8).Text = "De Activated"
            End If
        End If
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        LoadDataGrid()
    End Sub
End Class