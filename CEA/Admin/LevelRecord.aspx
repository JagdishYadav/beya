﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="LevelRecord.aspx.vb" Inherits="CEA.LevelRocord" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function ShowConfirm() {
            var strConfirm = '<%=GetGlobalResourceObject("Resource", "ConfirmDelete") %>';
            return confirm(strConfirm);
        }
        function ChangeRowColor(rowID) {
            var grid = document.getElementById("<%= grdProducts.ClientID %>");
            for (i = 1; i < grid.rows.length; i++) {
                grid.rows[i].style.backgroundColor = 'White';
            }

            document.getElementById(rowID).style.backgroundColor = 'yellow';
            return false;
        }
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "AttachSuccess") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SelectRow") %>';
            }

            alert(msgstring);
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
 <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, CurrentLocation %>'></asp:Label>
                    <asp:Label ID="lblPath" runat="server" Text="" Font-Bold="True" Font-Size="Smaller"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
               
                <div class="large-12 columns">
                    <asp:Panel ID="Panel1" runat="server" CssClass="panel">
<div class="row">
                   <div class="large-6 medium-6 column">
                    <asp:Label ID="Label15" runat="server" Text="Search" Width="100%"></asp:Label>                            
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="left" Width="70%"></asp:TextBox><asp:Button ID="Button5" runat="server" Text="Search" CssClass="BTNSearch" />
                </div>
                </div>
                        
                        <div style="width: 100%; height: 300px; overflow: scroll">
                            <asp:GridView ID="grdProducts" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="10" DataKeyNames="ID">
                                <Columns>
                                    <asp:BoundField DataField="MapID" HeaderText="MapID" ItemStyle-Width="0px" ItemStyle-Wrap="true"
                                        Visible="false" />
                                    <asp:BoundField DataField="NameE" SortExpression="NameE" HeaderText='<%$ Resources:Resource, NameE %>'
                                        ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="NameA" SortExpression="NameA" HeaderText='<%$ Resources:Resource, NameA %>'
                                        ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="PropE" SortExpression="PropE" HeaderText='<%$ Resources:Resource, FeatE %>'
                                        ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="PropA" SortExpression="PropA" HeaderText='<%$ Resources:Resource, FeatA %>'
                                        ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="DescE" SortExpression="DescE" HeaderText='<%$ Resources:Resource, DescriptionE %>'
                                        ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="DescA" SortExpression="DescA" HeaderText='<%$ Resources:Resource, DescriptionA %>'
                                        ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="Unit" SortExpression="Unit" HeaderText='<%$ Resources:Resource, Unit %>'
                                        ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="ManHour" SortExpression="ManHour" HeaderText='<%$ Resources:Resource, ManHour %>'
                                        ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="EquipHour" SortExpression="EquipHour" HeaderText='<%$ Resources:Resource, EquipHour %>'
                                        ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                                    <asp:CommandField ShowSelectButton="true" ItemStyle-Width="25px" SelectText='<%$ Resources:Resource, Select %>' />
                                    <asp:CommandField ShowDeleteButton="true" ItemStyle-Width="25px" DeleteText='<%$ Resources:Resource, Delete %>' />
                                </Columns>
                            </asp:GridView>
</div>
                    </asp:Panel>
                </div>
                </div>
                
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" runat="server" contentplaceholderid="MiddlePart">
<div class="row">
               
                <div class="large-12 medium-12 column" id = "buttonsrow" runat = "server">
                    <div class="outerDivBtn">
                        <div class="BTNdiv">
                    <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Attach %>' CssClass="BTNAttach"  />
                    <asp:Button ID="btnClose" runat="server" Text='<%$ Resources:Resource, Close %>' CssClass="BTNclose"  />
                </div></div></div>
            </div>
</asp:Content>

