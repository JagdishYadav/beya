﻿Imports System.Data
Imports System.Data.SqlClient
Public Class LevelRocord
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
            Menuhide.Value = "close"
            Dim logout As Button = Master.FindControl("Logout")
            logout.Visible = False

            btnClose.OnClientClick = "window.close();"

            If (UICulture = "Arabic") Then
                Panel1.Style.Add("direction", "rtl")
                Panel1.Style.Add("text-align", "right")
                buttonsrow.Style.Add("direction", "rtl")
                buttonsrow.Style.Add("text-align", "right")
            End If
            LoadDataGrid_Products()
            If (Session("Path") IsNot Nothing) Then
                lblPath.Text = Session("Path")
            End If
        End If
    End Sub
    Private Sub LoadDataGrid_Products()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        dt = objSqlConfig.GetProducts_2("0", txtSearch.Text, Session("CompanyID"))
        If (dt.Rows.Count > 0) Then
            grdProducts.DataSource = dt
            grdProducts.DataBind()
        Else
            grdProducts.DataSource = Nothing
            grdProducts.DataBind()
        End If
    End Sub
    Protected Sub grdProducts_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdProducts.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            For Each link As LinkButton In e.Row.Cells(11).Controls.OfType(Of LinkButton)()
                If link.CommandName = "Delete" Then
                    link.Attributes("onclick") = "if (ShowConfirm() == false) { return false; };"
                End If
            Next
        End If
    End Sub
    Protected Sub grdProducts_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdProducts.RowDeleting
        Dim objSqlConfig As New ConfigClassSqlServer()
        objSqlConfig.DeleteProductByIDAndCompany(e.Keys(0).ToString(), Session("CompanyID"))
        LoadDataGrid_Products()
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim i As Integer
        Dim strPath, strCode As String
        Dim sqlTrans As SqlTransaction = Nothing

        If grdProducts.SelectedIndex >= 0 Then
            If (Session("Path") IsNot Nothing) Then
                strPath = Session("Path")
            End If
            If (Session("ID") IsNot Nothing) Then
                strCode = Session("ID")
            End If

            Dim sqlConnection As New SqlConnection()
            Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
            sqlConnection.Open()

            Dim arrPath As String() = strPath.Split("/")

            sqlTrans = sqlConnection.BeginTransaction()
            sqlCommand.Transaction = sqlTrans

            'sqlCommand.CommandText = "UPDATE MainHierarchy SET LastLevel='" & UBound(arrPath) & "' WHERE IDCode=" & CLng(strCode)
            'sqlCommand.ExecuteNonQuery()

            sqlCommand.CommandText = "UPDATE ProductDetails SET MapId= '" + strCode + "' WHERE MapID=0 AND ID='" + grdProducts.DataKeys(grdProducts.SelectedIndex).Value.ToString() + "' AND CompID = '" + Session("CompanyID") + "' "
            sqlCommand.ExecuteNonQuery()

            grdProducts.SelectedIndex = -1

            sqlTrans.Commit()
            LoadDataGrid_Products()
            sqlConnection.Close()

            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "Message", "ShowMessage('1');", True)
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "Message", "ShowMessage('2');", True)
        End If
    End Sub
    Protected Sub grdProducts_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdProducts.SelectedIndexChanged
        grdProducts.Rows(grdProducts.SelectedIndex).Attributes.Add("ID", grdProducts.SelectedIndex)
        Dim strFunction As String = "ChangeRowColor(" + grdProducts.SelectedIndex.ToString() + ");"
        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "", strFunction, True)
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        LoadDataGrid_Products()
    End Sub
End Class