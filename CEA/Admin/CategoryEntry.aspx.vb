﻿Imports System.Data
Imports System.Data.SqlClient
Public Class CategoryEntry
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Button8.Attributes.Add("style", "display:none")
                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (menuItems.Count > 14) Then
                    For i As Integer = 0 To 11
                        menuItems.RemoveAt(0)
                    Next
                    menuItems.RemoveAt(2)
                    menuItems.RemoveAt(2)
                End If

                If (UICulture = "Arabic") Then
                    tvLevels.LineImagesFolder = "~/TreeLineImagesAr"
                    tvLevels.Attributes.Add("style", "margin-top: 13px;")
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
    End Sub
    Private Sub LoadTreeViewData()
        Dim strClientTypeID As String = "1"
        If (hdnInitialLoad.Value.ToString().Equals("1")) Then
            Dim sqlConnection As New SqlConnection()
            Dim node As TreeNode = Nothing
            'Dim NodeLevel1, NodeLevel2, NodeLevel3, NodeLevel4, NodeLevel5, NodeLevel6, NodeLevel7 As TreeNode
            tvLevels.Nodes.Clear()

            Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
            sqlCommand.CommandTimeout = 120000
            'Dim r1, r2, r3, r4, r5, r6, r7, r8 As SqlDataReader
            'Dim dt1, dt2, dt3, dt4, dt5, dt6, dt7, dt8 As New DataTable()
            Dim r1 As SqlDataReader
            Dim dt1 As New DataTable()

            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
            sqlConnection.Open()

            If (UICulture = "Arabic") Then
                sqlCommand.CommandText = "SELECT DISTINCT ID,  NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = 0 ORDER BY No ASC"
                r1 = sqlCommand.ExecuteReader()
                dt1.Load(r1)
                r1.Close()

                For Each dr1 As DataRow In dt1.Rows
                    If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
                        node = New TreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
                        node.Target = dr1.Item(2).ToString()
                        'node.ImageUrl = dr1.Item(3).ToString()
                        node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
                        tvLevels.Nodes.Add(node)
                        'NodeLevel1 = node

                        'sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        'r2 = sqlCommand.ExecuteReader()
                        'dt2.Clear()
                        'dt2.Load(r2)
                        'r2.Close()
                        'For Each dr2 As DataRow In dt2.Rows
                        '    If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
                        '        node = New TreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
                        '        node.Target = dr2.Item(2).ToString()
                        '        'node.ImageUrl = dr2.Item(3).ToString() 
                        '        node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
                        '        NodeLevel1.ChildNodes.Add(node)
                        '        NodeLevel2 = node

                        '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '        r3 = sqlCommand.ExecuteReader()
                        '        dt3.Clear()
                        '        dt3.Load(r3)
                        '        r3.Close()
                        '        For Each dr3 As DataRow In dt3.Rows
                        '            If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
                        '                node = New TreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
                        '                node.Target = dr3.Item(2).ToString()
                        '                'node.ImageUrl = dr3.Item(3).ToString()
                        '                node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
                        '                NodeLevel2.ChildNodes.Add(node)
                        '                NodeLevel3 = node

                        '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                r4 = sqlCommand.ExecuteReader()
                        '                dt4.Clear()
                        '                dt4.Load(r4)
                        '                r4.Close()

                        '                For Each dr4 As DataRow In dt4.Rows
                        '                    If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
                        '                        node = New TreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
                        '                        node.Target = dr4.Item(2).ToString()
                        '                        'node.ImageUrl = dr4.Item(3).ToString()
                        '                        node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
                        '                        NodeLevel3.ChildNodes.Add(node)
                        '                        NodeLevel4 = node

                        '                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                        r5 = sqlCommand.ExecuteReader()
                        '                        dt5.Clear()
                        '                        dt5.Load(r5)
                        '                        r5.Close()
                        '                        For Each dr5 As DataRow In dt5.Rows
                        '                            If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
                        '                                node = New TreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
                        '                                node.Target = dr5.Item(2).ToString()
                        '                                'node.ImageUrl = dr5.Item(3).ToString()
                        '                                node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
                        '                                NodeLevel4.ChildNodes.Add(node)
                        '                                NodeLevel5 = node

                        '                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                                r6 = sqlCommand.ExecuteReader()
                        '                                dt6.Clear()
                        '                                dt6.Load(r6)
                        '                                r6.Close()
                        '                                For Each dr6 As DataRow In dt6.Rows
                        '                                    If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
                        '                                        node = New TreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
                        '                                        node.Target = dr6.Item(2).ToString()
                        '                                        'node.ImageUrl = dr6.Item(3).ToString()
                        '                                        node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
                        '                                        NodeLevel5.ChildNodes.Add(node)
                        '                                        NodeLevel6 = node

                        '                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                                        r7 = sqlCommand.ExecuteReader()
                        '                                        dt7.Clear()
                        '                                        dt7.Load(r7)
                        '                                        r7.Close()
                        '                                        For Each dr7 As DataRow In dt7.Rows
                        '                                            If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
                        '                                                node = New TreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
                        '                                                node.Target = dr7.Item(2).ToString()
                        '                                                'node.ImageUrl = dr7.Item(3).ToString()
                        '                                                node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
                        '                                                NodeLevel6.ChildNodes.Add(node)
                        '                                                NodeLevel7 = node

                        '                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                                                r8 = sqlCommand.ExecuteReader()
                        '                                                dt8.Clear()
                        '                                                dt8.Load(r8)
                        '                                                r8.Close()
                        '                                                For Each dr8 As DataRow In dt8.Rows
                        '                                                    If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
                        '                                                        node = New TreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
                        '                                                        node.Target = dr8.Item(2).ToString()
                        '                                                        'node.ImageUrl = dr8.Item(3).ToString()
                        '                                                        node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
                        '                                                        NodeLevel7.ChildNodes.Add(node)
                        '                                                    End If
                        '                                                Next
                        '                                            End If
                        '                                        Next
                        '                                    End If
                        '                                Next
                        '                            End If
                        '                        Next
                        '                    End If
                        '                Next
                        '            End If
                        '        Next
                        '    End If
                        'Next
                    End If
                Next

            Else
                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = 0 ORDER BY No ASC"
                r1 = sqlCommand.ExecuteReader()
                dt1.Load(r1)
                r1.Close()

                For Each dr1 As DataRow In dt1.Rows
                    If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
                        node = New TreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
                        node.Target = dr1.Item(2).ToString()
                        'node.ImageUrl = dr1.Item(3).ToString()
                        node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
                        tvLevels.Nodes.Add(node)
                        'NodeLevel1 = node

                        'sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        'r2 = sqlCommand.ExecuteReader()
                        'dt2.Clear()
                        'dt2.Load(r2)
                        'r2.Close()
                        'For Each dr2 As DataRow In dt2.Rows
                        '    If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
                        '        node = New TreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
                        '        node.Target = dr2.Item(2).ToString()
                        '        'node.ImageUrl = dr2.Item(3).ToString()
                        '        node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
                        '        NodeLevel1.ChildNodes.Add(node)
                        '        NodeLevel2 = node

                        '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '        r3 = sqlCommand.ExecuteReader()
                        '        dt3.Clear()
                        '        dt3.Load(r3)
                        '        r3.Close()
                        '        For Each dr3 As DataRow In dt3.Rows
                        '            If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
                        '                node = New TreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
                        '                node.Target = dr3.Item(2).ToString()
                        '                'node.ImageUrl = dr3.Item(3).ToString()
                        '                node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
                        '                NodeLevel2.ChildNodes.Add(node)
                        '                NodeLevel3 = node

                        '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                r4 = sqlCommand.ExecuteReader()
                        '                dt4.Clear()
                        '                dt4.Load(r4)
                        '                r4.Close()

                        '                For Each dr4 As DataRow In dt4.Rows
                        '                    If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
                        '                        node = New TreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
                        '                        node.Target = dr4.Item(2).ToString()
                        '                        'node.ImageUrl = dr4.Item(3).ToString()
                        '                        node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
                        '                        NodeLevel3.ChildNodes.Add(node)
                        '                        NodeLevel4 = node

                        '                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                        r5 = sqlCommand.ExecuteReader()
                        '                        dt5.Clear()
                        '                        dt5.Load(r5)
                        '                        r5.Close()
                        '                        For Each dr5 As DataRow In dt5.Rows
                        '                            If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
                        '                                node = New TreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
                        '                                node.Target = dr5.Item(2).ToString()
                        '                                'node.ImageUrl = dr5.Item(3).ToString()
                        '                                node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
                        '                                NodeLevel4.ChildNodes.Add(node)
                        '                                NodeLevel5 = node

                        '                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                                r6 = sqlCommand.ExecuteReader()
                        '                                dt6.Clear()
                        '                                dt6.Load(r6)
                        '                                r6.Close()
                        '                                For Each dr6 As DataRow In dt6.Rows
                        '                                    If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
                        '                                        node = New TreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
                        '                                        node.Target = dr6.Item(2).ToString()
                        '                                        'node.ImageUrl = dr6.Item(3).ToString()
                        '                                        node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
                        '                                        NodeLevel5.ChildNodes.Add(node)
                        '                                        NodeLevel6 = node

                        '                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                                        r7 = sqlCommand.ExecuteReader()
                        '                                        dt7.Clear()
                        '                                        dt7.Load(r7)
                        '                                        r7.Close()
                        '                                        For Each dr7 As DataRow In dt7.Rows
                        '                                            If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
                        '                                                node = New TreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
                        '                                                node.Target = dr7.Item(2).ToString()
                        '                                                'node.ImageUrl = dr7.Item(3).ToString()
                        '                                                node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
                        '                                                NodeLevel6.ChildNodes.Add(node)
                        '                                                NodeLevel7 = node

                        '                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND STATUS NOT IN(2) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                                                r8 = sqlCommand.ExecuteReader()
                        '                                                dt8.Clear()
                        '                                                dt8.Load(r8)
                        '                                                r8.Close()
                        '                                                For Each dr8 As DataRow In dt8.Rows
                        '                                                    If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
                        '                                                        node = New TreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
                        '                                                        node.Target = dr8.Item(2).ToString()
                        '                                                        'node.ImageUrl = dr8.Item(3).ToString()
                        '                                                        node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
                        '                                                        NodeLevel7.ChildNodes.Add(node)
                        '                                                    End If
                        '                                                Next
                        '                                            End If
                        '                                        Next
                        '                                    End If
                        '                                Next
                        '                            End If
                        '                        Next
                        '                    End If
                        '                Next
                        '            End If
                        '        Next
                        '    End If
                        'Next
                    End If
                Next
            End If

            tvLevels.Visible = True
            sqlConnection.Close()
            tvLevels.ExpandAll()

        End If
        'ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "HideDiv();", True)
    End Sub
    Protected Sub tvLevels_SelectedNodeChanged(sender As Object, e As EventArgs) Handles tvLevels.SelectedNodeChanged
        Dim node As TreeNode
        Dim selectedNode As TreeNode
        Dim textvalue As String = ""
        Dim index As Integer = 0
        Dim myChars() As Char
        Dim ch As Char

        If (tvLevels.Nodes.Count > 0 And tvLevels.SelectedNode.Text <> "") Then

            Dim strClientTypeID As String = "1"
            If (Session("ClientTypeID") IsNot Nothing) Then
                strClientTypeID = Session("ClientTypeID")
            End If

            Dim dt As New DataTable()
            Dim arrList() As String = tvLevels.SelectedNode.ToolTip.Split(",")
            Dim strNodeNumber As String = arrList(arrList.Length - 1)
            Dim strLevelNo As Integer = Integer.Parse(arrList(0))
            Dim sqlConnection As New SqlConnection()

            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
            sqlConnection.Open()
            Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()

            If (UICulture = "Arabic") Then
                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Numeric(25))No FROM MainHierarchy1 WHERE ParentNo = '" + strNodeNumber + "' AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
            Else
                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Numeric(25))No FROM MainHierarchy1 WHERE ParentNo = '" + strNodeNumber + "' AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
            End If
            Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
            sqlDataAdapter.Fill(dt)

            strLevelNo = strLevelNo + 1
            If (tvLevels.SelectedNode.ChildNodes.Count = 0) Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    If Not (IsDBNull(dt.Rows(i).Item(0).ToString())) And Not (Trim(dt.Rows(i).Item(0).ToString())) = "" Then
                        node = New TreeNode(dt.Rows(i).Item(3).ToString() + " " + dt.Rows(i).Item(1).ToString())
                        node.Target = dt.Rows(i).Item(2).ToString()
                        node.ToolTip = strLevelNo.ToString() + "," + dt.Rows(i).Item(0).ToString() + "," + dt.Rows(i).Item(3).ToString()
                        tvLevels.SelectedNode.ChildNodes.Add(node)
                    End If
                Next
            End If
            sqlConnection.Close()

            For Each ctrl In Panel1.Controls
                If TypeOf (ctrl) Is TextBox Then
                    CType(ctrl, TextBox).Text = ""
                    CType(ctrl, TextBox).ReadOnly = True
                End If
            Next

            node = tvLevels.SelectedNode
            selectedNode = tvLevels.SelectedNode
            While node IsNot Nothing
                myChars = node.Text
                textvalue = ""
                index = 0
                For i As Integer = 0 To myChars.Length - 1
                    ch = myChars(i)
                    If Char.IsDigit(ch) Or Char.IsWhiteSpace(ch) Then
                        index = index + 1
                    Else
                        textvalue = node.Text.Substring(index, node.Text.Length - index)
                        Exit For
                    End If
                Next

                If (UICulture = "Arabic") Then
                    If (node.ToolTip.Split(",")(0) = "8") Then
                        txtNo8.Text = node.ToolTip.Split(",")(2)
                        txtLevel8A.Text = textvalue
                        txtLevel8E.Text = node.Target
                    ElseIf (node.ToolTip.Split(",")(0) = "7") Then
                        txtNo7.Text = node.ToolTip.Split(",")(2)
                        txtLevel7A.Text = textvalue
                        txtLevel7E.Text = node.Target

                        If (selectedNode.ToolTip.Split(",")(0) = "7") Then
                            txtLevel8E.ReadOnly = False
                            txtLevel8A.ReadOnly = False
                            txtNo8.ReadOnly = False
                        End If
                    ElseIf (node.ToolTip.Split(",")(0) = "6") Then
                        txtNo6.Text = node.ToolTip.Split(",")(2)
                        txtLevel6A.Text = textvalue
                        txtLevel6E.Text = node.Target

                        If (selectedNode.ToolTip.Split(",")(0) = "6") Then
                            txtLevel7E.ReadOnly = False
                            txtLevel7A.ReadOnly = False
                            txtNo7.ReadOnly = False
                        End If
                    ElseIf (node.ToolTip.Split(",")(0) = "5") Then
                        txtNo5.Text = node.ToolTip.Split(",")(2)
                        txtLevel5A.Text = textvalue
                        txtLevel5E.Text = node.Target

                        If (selectedNode.ToolTip.Split(",")(0) = "5") Then
                            txtLevel6E.ReadOnly = False
                            txtLevel6A.ReadOnly = False
                            txtNo6.ReadOnly = False
                        End If
                    ElseIf (node.ToolTip.Split(",")(0) = "4") Then
                        txtNo4.Text = node.ToolTip.Split(",")(2)
                        txtLevel4A.Text = textvalue
                        txtLevel4E.Text = node.Target

                        If (selectedNode.ToolTip.Split(",")(0) = "4") Then
                            txtLevel5E.ReadOnly = False
                            txtLevel5A.ReadOnly = False
                            txtNo5.ReadOnly = False
                        End If
                    ElseIf (node.ToolTip.Split(",")(0) = "3") Then
                        txtNo3.Text = node.ToolTip.Split(",")(2)
                        txtLevel3A.Text = textvalue
                        txtLevel3E.Text = node.Target

                        If (selectedNode.ToolTip.Split(",")(0) = "3") Then
                            txtLevel4E.ReadOnly = False
                            txtLevel4A.ReadOnly = False
                            txtNo4.ReadOnly = False
                        End If
                    ElseIf (node.ToolTip.Split(",")(0) = "2") Then
                        txtNo2.Text = node.ToolTip.Split(",")(2)
                        txtLevel2A.Text = textvalue
                        txtLevel2E.Text = node.Target

                        If (selectedNode.ToolTip.Split(",")(0) = "2") Then
                            txtLevel3E.ReadOnly = False
                            txtLevel3A.ReadOnly = False
                            txtNo3.ReadOnly = False
                        End If
                    ElseIf (node.ToolTip.Split(",")(0) = "1") Then
                        txtNo1.Text = node.ToolTip.Split(",")(2)
                        txtLevel1A.Text = textvalue
                        txtLevel1E.Text = node.Target

                        If (selectedNode.ToolTip.Split(",")(0) = "1") Then
                            txtLevel2E.ReadOnly = False
                            txtLevel2A.ReadOnly = False
                            txtNo2.ReadOnly = False
                        End If
                    End If
                Else
                    If (node.ToolTip.Split(",")(0) = "8") Then
                        txtNo8.Text = node.ToolTip.Split(",")(2)
                        txtLevel8E.Text = textvalue
                        txtLevel8A.Text = node.Target
                    ElseIf (node.ToolTip.Split(",")(0) = "7") Then
                        txtNo7.Text = node.ToolTip.Split(",")(2)
                        txtLevel7E.Text = textvalue
                        txtLevel7A.Text = node.Target

                        If (selectedNode.ToolTip.Split(",")(0) = "7") Then
                            txtLevel8E.ReadOnly = False
                            txtLevel8A.ReadOnly = False
                            txtNo8.ReadOnly = False
                        End If
                    ElseIf (node.ToolTip.Split(",")(0) = "6") Then
                        txtNo6.Text = node.ToolTip.Split(",")(2)
                        txtLevel6E.Text = textvalue
                        txtLevel6A.Text = node.Target

                        If (selectedNode.ToolTip.Split(",")(0) = "6") Then
                            txtLevel7E.ReadOnly = False
                            txtLevel7A.ReadOnly = False
                            txtNo7.ReadOnly = False
                        End If
                    ElseIf (node.ToolTip.Split(",")(0) = "5") Then
                        txtNo5.Text = node.ToolTip.Split(",")(2)
                        txtLevel5E.Text = textvalue
                        txtLevel5A.Text = node.Target

                        If (selectedNode.ToolTip.Split(",")(0) = "5") Then
                            txtLevel6E.ReadOnly = False
                            txtLevel6A.ReadOnly = False
                            txtNo6.ReadOnly = False
                        End If
                    ElseIf (node.ToolTip.Split(",")(0) = "4") Then
                        txtNo4.Text = node.ToolTip.Split(",")(2)
                        txtLevel4E.Text = textvalue
                        txtLevel4A.Text = node.Target

                        If (selectedNode.ToolTip.Split(",")(0) = "4") Then
                            txtLevel5E.ReadOnly = False
                            txtLevel5A.ReadOnly = False
                            txtNo5.ReadOnly = False
                        End If
                    ElseIf (node.ToolTip.Split(",")(0) = "3") Then
                        txtNo3.Text = node.ToolTip.Split(",")(2)
                        txtLevel3E.Text = textvalue
                        txtLevel3A.Text = node.Target

                        If (selectedNode.ToolTip.Split(",")(0) = "3") Then
                            txtLevel4E.ReadOnly = False
                            txtLevel4A.ReadOnly = False
                            txtNo4.ReadOnly = False
                        End If
                    ElseIf (node.ToolTip.Split(",")(0) = "2") Then
                        txtNo2.Text = node.ToolTip.Split(",")(2)
                        txtLevel2E.Text = textvalue
                        txtLevel2A.Text = node.Target

                        If (selectedNode.ToolTip.Split(",")(0) = "2") Then
                            txtLevel3E.ReadOnly = False
                            txtLevel3A.ReadOnly = False
                            txtNo3.ReadOnly = False
                        End If
                    ElseIf (node.ToolTip.Split(",")(0) = "1") Then
                        txtNo1.Text = node.ToolTip.Split(",")(2)
                        txtLevel1E.Text = textvalue
                        txtLevel1A.Text = node.Target

                        If (selectedNode.ToolTip.Split(",")(0) = "1") Then
                            txtLevel2E.ReadOnly = False
                            txtLevel2A.ReadOnly = False
                            txtNo2.ReadOnly = False
                        End If
                    End If
                End If
                node = node.Parent
            End While
        End If

        'selectedNode.Select()

        'Dim collection As New Collection
        'Dim textboxcollection As New Collection
        'Dim node As TreeNode
        'Dim strSelectedPath As String = ""
        'Dim sqlConnection As New SqlConnection()
        'Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()

        'If (tvLevels.Nodes.Count > 0 And tvLevels.SelectedNode.Text <> "") Then

        '    node = tvLevels.SelectedNode

        '    For Each ctrl In Panel1.Controls
        '        If TypeOf (ctrl) Is TextBox Then
        '            CType(ctrl, TextBox).Text = ""
        '            CType(ctrl, TextBox).ReadOnly = True
        '            'textboxcollection.Add(ctrl)
        '        End If
        '    Next

        '    While node IsNot Nothing
        '        Dim textvalue As String = ""
        '        Dim index As Integer = 0
        '        textvalue = node.Text
        '        If (node.ImageUrl = "0") Then
        '            textvalue = ""
        '            Dim myChars() As Char = node.Text
        '            Dim ch As Char
        '            For i As Integer = 0 To myChars.Length - 1
        '                ch = myChars(i)
        '                If Char.IsDigit(ch) Or ch = " " Then
        '                    index = index + 1
        '                Else
        '                    textvalue = node.Text.Substring(index, node.Text.Length - index)
        '                    Exit For
        '                End If
        '            Next
        '        End If

        '        collection.Add(textvalue)
        '        collection.Add(node.Target)
        '        collection.Add(node.ToolTip)
        '        collection.Add(node.ImageUrl)
        '        node = node.Parent
        '    End While

        '    Dim levelIndex As Integer = 1
        '    Dim startingIndex As Integer = collection.Count - 1

        '    If (tvLevels.SelectedNode.ImageUrl = "1") Then
        '        For i = startingIndex To 1 Step -4
        '            If (UICulture = "Arabic") Then
        '                strSelectedPath = strSelectedPath & "Level" & levelIndex & "A" & "='" & Trim(collection.Item(i - 1)) & "' and "
        '            Else
        '                strSelectedPath = strSelectedPath & "Level" & levelIndex & "='" & Trim(collection.Item(i - 2)) & "' and "
        '            End If
        '            levelIndex = levelIndex + 1
        '        Next i
        '    Else
        '        For i = startingIndex To 1 Step -4
        '            If (UICulture = "Arabic") Then
        '                strSelectedPath = strSelectedPath & "Level" & levelIndex & "A" & "='" & Trim(collection.Item(i - 1)) & "' and "
        '            Else
        '                strSelectedPath = strSelectedPath & "Level" & levelIndex & "='" & Trim(collection.Item(i - 2)) & "' and "
        '            End If
        '            strSelectedPath = strSelectedPath & "No" & levelIndex & "='" & Trim(collection.Item(i)) & "' and "
        '            levelIndex = levelIndex + 1
        '        Next i
        '    End If

        '    If Trim(strSelectedPath) <> "" Then
        '        Dim dt As New DataTable()
        '        strSelectedPath = Mid$(strSelectedPath, 1, Len(strSelectedPath) - 4)

        '        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        '        sqlConnection.Open()
        '        sqlCommand.CommandText = "Select * from MainHierarchy where " & strSelectedPath

        '        Dim SqlDataAdapter As New SqlDataAdapter(sqlCommand)
        '        SqlDataAdapter.Fill(dt)

        '        If (tvLevels.SelectedNode.ImageUrl = "1") Then
        '            If (dt.Rows.Count > 0) Then
        '                If (collection.Count >= 2) Then
        '                    Dim myChars() As Char = dt.Rows(0).Item(1).ToString().ToCharArray()
        '                    Dim numericval As String = ""
        '                    Dim ch As Char
        '                    Dim ch1 As Char
        '                    For i As Integer = 0 To myChars.Length - 1
        '                        ch = myChars(i)
        '                        ch1 = myChars(i + 1)
        '                        If Char.IsDigit(ch) Or (ch = " " And Char.IsDigit(ch1)) Then
        '                            numericval = numericval + ch
        '                        Else
        '                            Exit For
        '                        End If
        '                    Next
        '                    txtLevel0.Text = dt.Rows(0).Item(1).ToString().Substring(numericval.Length, dt.Rows(0).Item(1).ToString().Length - numericval.Length)
        '                    txtLevelA0.Text = dt.Rows(0).Item(10).ToString().Substring(numericval.Length, dt.Rows(0).Item(10).ToString().Length - numericval.Length)
        '                    txtNo1.Text = numericval
        '                End If

        '                If (collection.Count >= 5) Then
        '                    Dim myChars() As Char = dt.Rows(0).Item(2).ToString().ToCharArray()
        '                    Dim numericval As String = ""
        '                    Dim ch As Char
        '                    Dim ch1 As Char
        '                    For i As Integer = 0 To myChars.Length - 1
        '                        ch = myChars(i)
        '                        ch1 = myChars(i + 1)
        '                        If Char.IsDigit(ch) Or (ch = " " And Char.IsDigit(ch1)) Then
        '                            numericval = numericval + ch
        '                        Else
        '                            Exit For
        '                        End If
        '                    Next
        '                    txtLevel1.Text = dt.Rows(0).Item(2).ToString().Substring(numericval.Length, dt.Rows(0).Item(2).ToString().Length - numericval.Length)
        '                    txtLevelA1.Text = dt.Rows(0).Item(11).ToString().Substring(numericval.Length, dt.Rows(0).Item(11).ToString().Length - numericval.Length)
        '                    txtNo2.Text = numericval
        '                End If

        '                If (collection.Count >= 9) Then
        '                    Dim myChars() As Char = dt.Rows(0).Item(3).ToString().ToCharArray()
        '                    Dim numericval As String = ""
        '                    Dim ch As Char
        '                    Dim ch1 As Char
        '                    For i As Integer = 0 To myChars.Length - 1
        '                        ch = myChars(i)
        '                        ch1 = myChars(i + 1)
        '                        If Char.IsDigit(ch) Or (ch = " " And Char.IsDigit(ch1)) Then
        '                            numericval = numericval + ch
        '                        Else
        '                            Exit For
        '                        End If
        '                    Next
        '                    txtLevel2.Text = dt.Rows(0).Item(3).ToString().Substring(numericval.Length, dt.Rows(0).Item(3).ToString().Length - numericval.Length)
        '                    txtLevelA2.Text = dt.Rows(0).Item(12).ToString().Substring(numericval.Length, dt.Rows(0).Item(12).ToString().Length - numericval.Length)
        '                    txtNo3.Text = numericval
        '                End If

        '                If (collection.Count >= 13) Then
        '                    Dim myChars() As Char = dt.Rows(0).Item(4).ToString().ToCharArray()
        '                    Dim numericval As String = ""
        '                    Dim ch As Char
        '                    Dim ch1 As Char
        '                    For i As Integer = 0 To myChars.Length - 1
        '                        ch = myChars(i)
        '                        ch1 = myChars(i + 1)
        '                        If Char.IsDigit(ch) Or (ch = " " And Char.IsDigit(ch1)) Then
        '                            numericval = numericval + ch
        '                        Else
        '                            Exit For
        '                        End If
        '                    Next
        '                    txtLevel3.Text = dt.Rows(0).Item(4).ToString().Substring(numericval.Length, dt.Rows(0).Item(4).ToString().Length - numericval.Length)
        '                    txtLevelA3.Text = dt.Rows(0).Item(13).ToString().Substring(numericval.Length, dt.Rows(0).Item(13).ToString().Length - numericval.Length)
        '                    txtNo4.Text = numericval
        '                End If

        '                If (collection.Count >= 17) Then
        '                    Dim myChars() As Char = dt.Rows(0).Item(5).ToString().ToCharArray()
        '                    Dim numericval As String = ""
        '                    Dim ch As Char
        '                    Dim ch1 As Char
        '                    For i As Integer = 0 To myChars.Length - 1
        '                        ch = myChars(i)
        '                        ch1 = myChars(i + 1)
        '                        If Char.IsDigit(ch) Or (ch = " " And Char.IsDigit(ch1)) Then
        '                            numericval = numericval + ch
        '                        Else
        '                            Exit For
        '                        End If
        '                    Next
        '                    txtLevel4.Text = dt.Rows(0).Item(5).ToString().Substring(numericval.Length, dt.Rows(0).Item(5).ToString().Length - numericval.Length)
        '                    txtLevelA4.Text = dt.Rows(0).Item(14).ToString().Substring(numericval.Length, dt.Rows(0).Item(14).ToString().Length - numericval.Length)
        '                    txtNo5.Text = numericval
        '                End If

        '                If (collection.Count >= 21) Then
        '                    txtLevel5.Text = dt.Rows(0).Item(6).ToString()
        '                    txtLevelA5.Text = dt.Rows(0).Item(15).ToString()
        '                End If

        '                If (collection.Count >= 25) Then
        '                    txtLevel6.Text = dt.Rows(0).Item(7).ToString()
        '                    txtLevelA6.Text = dt.Rows(0).Item(16).ToString()
        '                End If

        '                If (collection.Count >= 29) Then
        '                    txtLevel7.Text = dt.Rows(0).Item(8).ToString()
        '                    txtLevelA7.Text = dt.Rows(0).Item(17).ToString()
        '                End If
        '            End If
        '        Else
        '            If (dt.Rows.Count > 0) Then
        '                If (collection.Count >= 2) Then
        '                    txtLevel0.Text = dt.Rows(0).Item(1).ToString()
        '                    txtLevelA0.Text = dt.Rows(0).Item(10).ToString()
        '                    txtNo1.Text = dt.Rows(0).Item(19).ToString()
        '                End If
        '                If (collection.Count >= 5) Then
        '                    txtLevel1.Text = dt.Rows(0).Item(2).ToString()
        '                    txtLevelA1.Text = dt.Rows(0).Item(11).ToString()
        '                    txtNo2.Text = dt.Rows(0).Item(20).ToString()
        '                End If
        '                If (collection.Count >= 9) Then
        '                    txtLevel2.Text = dt.Rows(0).Item(3).ToString()
        '                    txtLevelA2.Text = dt.Rows(0).Item(12).ToString()
        '                    txtNo3.Text = dt.Rows(0).Item(21).ToString()
        '                End If
        '                If (collection.Count >= 13) Then
        '                    txtLevel3.Text = dt.Rows(0).Item(4).ToString()
        '                    txtLevelA3.Text = dt.Rows(0).Item(13).ToString()
        '                    txtNo4.Text = dt.Rows(0).Item(22).ToString()
        '                End If
        '                If (collection.Count >= 17) Then
        '                    txtLevel4.Text = dt.Rows(0).Item(5).ToString()
        '                    txtLevelA4.Text = dt.Rows(0).Item(14).ToString()
        '                    txtNo5.Text = dt.Rows(0).Item(23).ToString()
        '                End If
        '                If (collection.Count >= 21) Then
        '                    txtLevel5.Text = dt.Rows(0).Item(6).ToString()
        '                    txtLevelA5.Text = dt.Rows(0).Item(15).ToString()
        '                    txtNo6.Text = dt.Rows(0).Item(24).ToString()
        '                End If
        '                If (collection.Count >= 25) Then
        '                    txtLevel6.Text = dt.Rows(0).Item(7).ToString()
        '                    txtLevelA6.Text = dt.Rows(0).Item(16).ToString()
        '                    txtNo7.Text = dt.Rows(0).Item(25).ToString()
        '                End If
        '                If (collection.Count >= 29) Then
        '                    txtLevel7.Text = dt.Rows(0).Item(8).ToString()
        '                    txtLevelA7.Text = dt.Rows(0).Item(17).ToString()
        '                    txtNo8.Text = dt.Rows(0).Item(26).ToString()
        '                End If
        '            End If
        '        End If


        '        'If (collection.Count < 8) Then
        '        '    CType(textboxcollection((collection.Count * 2) + 1), TextBox).ReadOnly = False
        '        '    CType(textboxcollection((collection.Count * 2) + 2), TextBox).ReadOnly = False
        '        'End If

        '        'If (collection.Count < 23) Then

        '        '    CType(textboxcollection((collection.Count)), TextBox).ReadOnly = False
        '        '    CType(textboxcollection((collection.Count + 1)), TextBox).ReadOnly = False
        '        '    CType(textboxcollection((collection.Count + 2)), TextBox).ReadOnly = False
        '        'End If
        '        If (collection.Count = 4) Then
        '            txtNo2.ReadOnly = False
        '            txtLevel1.ReadOnly = False
        '            txtLevelA1.ReadOnly = False
        '        ElseIf (collection.Count = 8) Then
        '            txtNo3.ReadOnly = False
        '            txtLevel2.ReadOnly = False
        '            txtLevelA2.ReadOnly = False
        '        ElseIf (collection.Count = 12) Then
        '            txtNo4.ReadOnly = False
        '            txtLevel3.ReadOnly = False
        '            txtLevelA3.ReadOnly = False
        '        ElseIf (collection.Count = 16) Then
        '            txtNo5.ReadOnly = False
        '            txtLevel4.ReadOnly = False
        '            txtLevelA4.ReadOnly = False
        '        ElseIf (collection.Count = 20) Then
        '            txtNo6.ReadOnly = False
        '            txtLevel5.ReadOnly = False
        '            txtLevelA5.ReadOnly = False
        '        ElseIf (collection.Count = 24) Then
        '            txtNo7.ReadOnly = False
        '            txtLevel6.ReadOnly = False
        '            txtLevelA6.ReadOnly = False
        '        ElseIf (collection.Count = 28) Then
        '            txtNo8.ReadOnly = False
        '            txtLevel7.ReadOnly = False
        '            txtLevelA7.ReadOnly = False
        '        End If

        '    End If
        'End If

        'sqlConnection.Close()
        'collection = Nothing
        'textboxcollection = Nothing
    End Sub
    Protected Sub Button0_Click(sender As Object, e As EventArgs) Handles Button0.Click
        ViewState("EditNodeLevel") = "1"
        UpdateSingleLevel(sender)
    End Sub
    Private Sub UpdateSingleLevel(button As Button)
        If (tvLevels.SelectedNode IsNot Nothing) Then
            Dim clickedButton As Button = CType(button, Button)
            Dim node As TreeNode

            node = tvLevels.SelectedNode
            If (clickedButton.Text = "E") Then

                For Each ctrl In Panel1.Controls
                    If TypeOf (ctrl) Is TextBox Then
                        CType(ctrl, TextBox).ReadOnly = True
                    End If
                Next

                Button0.Enabled = False
                Button1.Enabled = False
                Button2.Enabled = False
                Button3.Enabled = False
                Button4.Enabled = False
                Button5.Enabled = False
                Button6.Enabled = False
                Button7.Enabled = False

                clickedButton.Text = "U"
                clickedButton.Enabled = True

                If (ViewState("EditNodeLevel") = "1") Then
                    txtNo1.ReadOnly = False
                    txtLevel1E.ReadOnly = False
                    txtLevel1A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo1.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "2") Then
                    txtNo2.ReadOnly = False
                    txtLevel2E.ReadOnly = False
                    txtLevel2A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo2.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "3") Then
                    txtNo3.ReadOnly = False
                    txtLevel3E.ReadOnly = False
                    txtLevel3A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo3.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "4") Then
                    txtNo4.ReadOnly = False
                    txtLevel4E.ReadOnly = False
                    txtLevel4A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo4.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "5") Then
                    txtNo5.ReadOnly = False
                    txtLevel5E.ReadOnly = False
                    txtLevel5A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo5.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "6") Then
                    txtNo6.ReadOnly = False
                    txtLevel6E.ReadOnly = False
                    txtLevel6A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo6.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "7") Then
                    txtNo7.ReadOnly = False
                    txtLevel7E.ReadOnly = False
                    txtLevel7A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo7.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "8") Then
                    txtNo8.ReadOnly = False
                    txtLevel8E.ReadOnly = False
                    txtLevel8A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo8.Text.Trim()
                End If
            ElseIf (clickedButton.Text = "U") Then
                Dim nodeID As String = ""
                Dim nodeNo As String = ""
                Dim nodeNameE As String = ""
                Dim nodeNameA As String = ""

                While node IsNot Nothing
                    If (node.ToolTip.Split(",")(0) = ViewState("EditNodeLevel")) Then
                        nodeID = node.ToolTip.Split(",")(1)
                        Exit While
                    End If
                    node = node.Parent
                End While

                If (ViewState("EditNodeLevel") = "1") Then
                    nodeNo = txtNo1.Text.Trim()
                    nodeNameE = txtLevel1E.Text.Trim()
                    nodeNameA = txtLevel1A.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "2") Then
                    nodeNo = txtNo2.Text.Trim()
                    nodeNameE = txtLevel2E.Text.Trim()
                    nodeNameA = txtLevel2A.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "3") Then
                    nodeNo = txtNo3.Text.Trim()
                    nodeNameE = txtLevel3E.Text.Trim()
                    nodeNameA = txtLevel3A.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "4") Then
                    nodeNo = txtNo4.Text.Trim()
                    nodeNameE = txtLevel4E.Text.Trim()
                    nodeNameA = txtLevel4A.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "5") Then
                    nodeNo = txtNo5.Text.Trim()
                    nodeNameE = txtLevel5E.Text.Trim()
                    nodeNameA = txtLevel5A.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "6") Then
                    nodeNo = txtNo6.Text.Trim()
                    nodeNameE = txtLevel6E.Text.Trim()
                    nodeNameA = txtLevel6A.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "7") Then
                    nodeNo = txtNo7.Text.Trim()
                    nodeNameE = txtLevel7E.Text.Trim()
                    nodeNameA = txtLevel7A.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "8") Then
                    nodeNo = txtNo8.Text.Trim()
                    nodeNameE = txtLevel8E.Text.Trim()
                    nodeNameA = txtLevel8A.Text.Trim()
                End If

                If (nodeNo.Length = 0) Then
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('17');", True)
                ElseIf (nodeNameE.Length = 0) Then
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('18');", True)
                ElseIf (nodeNameA.Length = 0) Then
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('19');", True)
                Else
                    Dim sqlConnection As New SqlConnection()
                    Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
                    sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                    sqlConnection.Open()

                    sqlCommand.CommandText = "SELECT COUNT(*) FROM MainHierarchy1 WHERE No = '" + nodeNo + "' AND ID <> '" + nodeID + "' "
                    If (sqlCommand.ExecuteScalar().ToString() = "0") Then

                        sqlCommand.CommandText = "UPDATE MainHierarchy1 SET No = '" + nodeNo + "', NameE = N'" + nodeNameE + "', NameA = N'" + nodeNameA + "', Status = 0 WHERE ID = '" + nodeID + "' "
                        sqlCommand.ExecuteNonQuery()

                        sqlCommand.CommandText = "UPDATE MainHierarchy1 SET ParentNo = '" + nodeNo + "' WHERE ParentNo = '" + ViewState("PrevNumber") + "' "
                        sqlCommand.ExecuteNonQuery()

                        Dim currentNode As TreeNode
                        currentNode = tvLevels.SelectedNode
                        Dim currentNodeID As String = ""
                        While (currentNode IsNot Nothing)
                            currentNodeID = currentNode.ToolTip.Split(",")(1)
                            sqlCommand.CommandText = "UPDATE MainHierarchy1 SET Status = 3 WHERE ID = '" + currentNodeID + "' "
                            sqlCommand.ExecuteNonQuery()
                            currentNode = currentNode.Parent
                        End While

                        node.ToolTip = node.ToolTip + "," + nodeNo
                        If (UICulture = "Arabic") Then
                            node.Text = nodeNo + " " + nodeNameA
                            node.Target = nodeNameE
                        Else
                            node.Text = nodeNo + " " + nodeNameE
                            node.Target = nodeNameA
                        End If


                        clickedButton.Text = "E"
                        Button0.Enabled = True
                        Button1.Enabled = True
                        Button2.Enabled = True
                        Button3.Enabled = True
                        Button4.Enabled = True
                        Button5.Enabled = True
                        Button6.Enabled = True
                        Button7.Enabled = True

                    Else
                        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('16');", True)
                    End If
                End If

            End If
        End If
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ViewState("EditNodeLevel") = "2"
        UpdateSingleLevel(sender)
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        ViewState("EditNodeLevel") = "3"
        UpdateSingleLevel(sender)
    End Sub
    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        ViewState("EditNodeLevel") = "4"
        UpdateSingleLevel(sender)
    End Sub
    Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        ViewState("EditNodeLevel") = "5"
        UpdateSingleLevel(sender)
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        ViewState("EditNodeLevel") = "6"
        UpdateSingleLevel(sender)
    End Sub
    Protected Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        ViewState("EditNodeLevel") = "7"
        UpdateSingleLevel(sender)
    End Sub
    Protected Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        ViewState("EditNodeLevel") = "8"
        UpdateSingleLevel(sender)
    End Sub
    Protected Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        Dim sqlConnection As New SqlConnection()
        Dim i As Integer = 0
        Dim valuePath As String = ""
        Dim currentNode As TreeNode

        If (tvLevels.SelectedNode IsNot Nothing) Then
            If (tvLevels.SelectedNode.ChildNodes.Count = 0) Then

                Dim strID As String = ""
                Dim dt As New DataTable()
                Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()

                sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                sqlConnection.Open()

                strID = tvLevels.SelectedNode.ToolTip.Split(",")(1)
                Dim strNodenumber As String = tvLevels.SelectedNode.ToolTip.Split(",")(2)

                sqlCommand.CommandText = "DELETE FROM Mainhierarchy1 WHERE ID = '" + strID + "' "
                sqlCommand.ExecuteNonQuery()

                dt.Clear()

                sqlCommand.CommandText = "SELECT DISTINCT MapID FROM ProductDetails WHERE MapID =  '" + strNodenumber + "' "
                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)

                If (dt.Rows.Count > 0) Then
                    If (hdnResult.Value.Equals("1")) Then
                        sqlCommand.CommandText = "DELETE FROM ProductDetails WHERE MapID = '" + strNodenumber + "' "
                        sqlCommand.ExecuteNonQuery()
                    End If
                End If

                hdnResult.Value = "-1"

                If (tvLevels.SelectedNode.Parent IsNot Nothing) Then
                    tvLevels.SelectedNode.Parent.ChildNodes.Remove(tvLevels.SelectedNode)
                Else
                    tvLevels.Nodes.Remove(tvLevels.SelectedNode)
                End If

                If (valuePath.Length > 0) Then
                    currentNode = tvLevels.FindNode(valuePath)
                    currentNode.Select()
                    While (currentNode.Parent IsNot Nothing)
                        currentNode = currentNode.Parent
                    End While
                    currentNode.ExpandAll()
                    tvLevels_SelectedNodeChanged(sender, Nothing)
                End If

                For Each ctrl In Panel1.Controls
                    If TypeOf (ctrl) Is TextBox Then
                        CType(ctrl, TextBox).Text = ""
                    End If
                Next

                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
        End If
        sqlConnection.Close()
    End Sub
    Protected Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        If (tvLevels.SelectedNode IsNot Nothing) Then
            Dim strNodeLevel As String = tvLevels.SelectedNode.ToolTip.Split(",")(0)
            Dim strSelectedNodeID As String = tvLevels.SelectedNode.ToolTip.Split(",")(1)
            Dim strSelectedNodeNumber As String = tvLevels.SelectedNode.ToolTip.Split(",")(2)
            Dim strNewLevel As String = ""
            Dim nodeNo As String = ""
            Dim nodeNameE As String = ""
            Dim nodeNameA As String = ""
            Dim currentNode As TreeNode

            If (strNodeLevel = "1") Then
                nodeNo = txtNo2.Text.Trim()
                nodeNameE = txtLevel2E.Text.Trim()
                nodeNameA = txtLevel2A.Text.Trim()
                strNewLevel = "2"
            ElseIf (strNodeLevel = "2") Then
                nodeNo = txtNo3.Text.Trim()
                nodeNameE = txtLevel3E.Text.Trim()
                nodeNameA = txtLevel3A.Text.Trim()
                strNewLevel = "3"
            ElseIf (strNodeLevel = "3") Then
                nodeNo = txtNo4.Text.Trim()
                nodeNameE = txtLevel4E.Text.Trim()
                nodeNameA = txtLevel4A.Text.Trim()
                strNewLevel = "4"
            ElseIf (strNodeLevel = "4") Then
                nodeNo = txtNo5.Text.Trim()
                nodeNameE = txtLevel5E.Text.Trim()
                nodeNameA = txtLevel5A.Text.Trim()
                strNewLevel = "5"
            ElseIf (strNodeLevel = "5") Then
                nodeNo = txtNo6.Text.Trim()
                nodeNameE = txtLevel6E.Text.Trim()
                nodeNameA = txtLevel6A.Text.Trim()
                strNewLevel = "6"
            ElseIf (strNodeLevel = "6") Then
                nodeNo = txtNo7.Text.Trim()
                nodeNameE = txtLevel7E.Text.Trim()
                nodeNameA = txtLevel7A.Text.Trim()
                strNewLevel = "7"
            ElseIf (strNodeLevel = "7") Then
                nodeNo = txtNo8.Text.Trim()
                nodeNameE = txtLevel8E.Text.Trim()
                nodeNameA = txtLevel8A.Text.Trim()
                strNewLevel = "8"
            End If

            If (nodeNo.Length = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('17');", True)
            ElseIf (nodeNameE.Length = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('18');", True)
            ElseIf (nodeNameA.Length = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('19');", True)
            ElseIf (IsNumeric(nodeNo) = False) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('20');", True)
            Else
                Dim dt As New DataTable()
                Dim sqlConnection As New SqlConnection()
                Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
                sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                sqlConnection.Open()

                sqlCommand.CommandText = "SELECT COUNT(*) FROM MainHierarchy1 WHERE No = '" + nodeNo + "' AND CompID = 0"
                If (sqlCommand.ExecuteScalar().ToString() = "0") Then

                    sqlCommand.CommandText = "SELECT COUNT(*) FROM ProductDetails WHERE MapID = '" + strSelectedNodeNumber + "' "
                    If (sqlCommand.ExecuteScalar().ToString() = "0") Then
                        sqlCommand.CommandText = "INSERT INTO MainHierarchy1(NameE,NameA,Status,No,ParentNo, ClientTypeID) VALUES(N'" + nodeNameE + "', N'" + nodeNameA + "', 0,  '" + nodeNo + "', '" + strSelectedNodeNumber + "', 1 )"
                        sqlCommand.ExecuteNonQuery()
                        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('7');", True)

                        currentNode = tvLevels.SelectedNode
                        Dim currentNodeID As String = ""
                        While (currentNode IsNot Nothing)
                            currentNodeID = currentNode.ToolTip.Split(",")(1)
                            sqlCommand.CommandText = "UPDATE MainHierarchy1 SET Status = 3 WHERE ID = '" + currentNodeID + "' "
                            sqlCommand.ExecuteNonQuery()
                            currentNode = currentNode.Parent
                        End While

                        If (UICulture = "Arabic") Then
                            sqlCommand.CommandText = "SELECT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ID = (SELECT MAX(ID) FROM MainHierarchy1) "
                        Else
                            sqlCommand.CommandText = "SELECT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ID = (SELECT MAX(ID) FROM MainHierarchy1) "
                        End If

                        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                        sqlDataAdapter.Fill(dt)

                        If (dt.Rows.Count > 0) Then
                            Dim node As TreeNode
                            node = New TreeNode(dt.Rows(0).Item(3).ToString() + " " + dt.Rows(0).Item(1).ToString())
                            node.Target = dt.Rows(0).Item(2).ToString()
                            'node.ImageUrl = dt.Rows(0).Item(3).ToString()
                            node.ToolTip = strNewLevel + "," + dt.Rows(0).Item(0).ToString() + "," + dt.Rows(0).Item(3).ToString()
                            Dim index As Integer = 0
                            For Each n As TreeNode In tvLevels.SelectedNode.ChildNodes
                                If (n.Text < node.Text) Then
                                    index = index + 1
                                End If
                            Next
                            tvLevels.SelectedNode.ChildNodes.AddAt(index, node)
                            node.Select()
                            If (node.Parent IsNot Nothing) Then
                                node.Parent.Expand()
                            End If
                        End If
                    Else
                        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('6');", True)
                    End If
                Else
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('16');", True)
                End If
                tvLevels.SelectedNode.ExpandAll()

                sqlConnection.Close()
            End If

        Else
            Dim nodeNo As String = ""
            Dim nodeNameE As String = ""
            Dim nodeNameA As String = ""

            nodeNo = txtNo1.Text.Trim()
            nodeNameE = txtLevel1E.Text.Trim()
            nodeNameA = txtLevel1A.Text.Trim()

            If (nodeNo.Length = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('17');", True)
            ElseIf (nodeNameE.Length = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('18');", True)
            ElseIf (nodeNameA.Length = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('19');", True)
            ElseIf (IsNumeric(nodeNo) = False) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('20');", True)
            Else
                Dim dt As New DataTable()
                Dim sqlConnection As New SqlConnection()
                Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
                sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                sqlConnection.Open()

                sqlCommand.CommandText = "SELECT COUNT(*) FROM MainHierarchy1 WHERE No = '" + nodeNo + "' AND CompID = 0 "
                If (sqlCommand.ExecuteScalar().ToString() = "0") Then

                    sqlCommand.CommandText = "INSERT INTO MainHierarchy1(NameE,NameA,Status,No, ClientTypeID) VALUES(N'" + nodeNameE + "', N'" + nodeNameA + "', 0,  '" + nodeNo + "', 1)"
                    sqlCommand.ExecuteNonQuery()
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('7');", True)

                    sqlCommand.CommandText = "SELECT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ID = (SELECT MAX(ID) FROM MainHierarchy1) "
                    Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                    sqlDataAdapter.Fill(dt)

                    If (dt.Rows.Count > 0) Then
                        Dim node As TreeNode
                        node = New TreeNode(dt.Rows(0).Item(3).ToString() + " " + dt.Rows(0).Item(1).ToString())
                        node.Target = dt.Rows(0).Item(2).ToString()
                        'node.ImageUrl = dt.Rows(0).Item(3).ToString()
                        node.ToolTip = "1" + "," + dt.Rows(0).Item(0).ToString() + "," + dt.Rows(0).Item(3).ToString()
                        Dim index As Integer = 0
                        For Each n As TreeNode In tvLevels.Nodes
                            If (n.Text < node.Text) Then
                                index = index + 1
                            End If
                        Next
                        tvLevels.Nodes.AddAt(index, node)
                        node.Select()

                    End If
                Else
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('16');", True)
                End If

                sqlConnection.Close()
            End If

        End If
    End Sub
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        txtLevel1E.ReadOnly = False
        txtLevel1A.ReadOnly = False
        txtNo1.ReadOnly = False

        txtNo1.Text = ""
        txtNo2.Text = ""
        txtNo3.Text = ""
        txtNo4.Text = ""
        txtNo5.Text = ""
        txtNo6.Text = ""
        txtNo7.Text = ""
        txtNo8.Text = ""

        txtLevel1E.Text = ""
        txtLevel2E.Text = ""
        txtLevel3E.Text = ""
        txtLevel4E.Text = ""
        txtLevel5E.Text = ""
        txtLevel6E.Text = ""
        txtLevel7E.Text = ""
        txtLevel8E.Text = ""

        txtLevel1A.Text = ""
        txtLevel2A.Text = ""
        txtLevel3A.Text = ""
        txtLevel4A.Text = ""
        txtLevel5A.Text = ""
        txtLevel6A.Text = ""
        txtLevel7A.Text = ""
        txtLevel8A.Text = ""

        If (tvLevels.SelectedNode IsNot Nothing) Then
            tvLevels.SelectedNode.Selected = False
        End If

        txtNo1.Focus()
    End Sub
    Protected Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        If (Request.IsAuthenticated) Then
            LoadTreeViewData()
            Button10.Attributes("onclick") = "if(ShowConfirm() == false){ return false; }"
        Else
            Response.Redirect("../Login.aspx")
        End If
        If (UICulture = "Arabic") Then
            Panel1.Style.Add("direction", "rtl")
            Panel1.Style.Add("text-align", "right")
            'Panel2.Style.Add("direction", "rtl")
            'Panel2.Style.Add("text-align", "right")
            tvLevels.Style.Add("float", "right")
            tvLevels.Style.Add("direction", "rtl")
            tvLevels.Style.Add("text-align", "right")
            'div1.Style.Add("float", "right")
            div2.Style.Add("float", "right")
            div3.Style.Add("float", "right")
            div2.Style.Add("direction", "rtl")
            div2.Style.Add("text-align", "right")
            LinkButton1.Style.Add("float", "right")
        End If

        txtLevel1A.Style.Add("text-align", "right")
        txtLevel2A.Style.Add("text-align", "right")
        txtLevel3A.Style.Add("text-align", "right")
        txtLevel4A.Style.Add("text-align", "right")
        txtLevel5A.Style.Add("text-align", "right")
        txtLevel6A.Style.Add("text-align", "right")
        txtLevel7A.Style.Add("text-align", "right")
        txtLevel8A.Style.Add("text-align", "right")

        txtLevel1E.Style.Add("text-align", "left")
        txtLevel2E.Style.Add("text-align", "left")
        txtLevel3E.Style.Add("text-align", "left")
        txtLevel4E.Style.Add("text-align", "left")
        txtLevel5E.Style.Add("text-align", "left")
        txtLevel6E.Style.Add("text-align", "left")
        txtLevel7E.Style.Add("text-align", "left")
        txtLevel8E.Style.Add("text-align", "left")
    End Sub
    Protected Sub LinkButton2_Click(sender As Object, e As EventArgs) Handles LinkButton2.Click
        Dim strFileName As String = "ExcelFile"
        If (uplImage.HasFile) Then
            Dim fileExt As String = System.IO.Path.GetExtension(uplImage.FileName).ToLower()
            If (fileExt = ".xls" Or fileExt = ".xlsx") Then
                strFileName = strFileName + fileExt

                If (System.IO.File.Exists("../Upload/Excel/" + strFileName)) Then
                    System.IO.File.Delete("../Upload/Excel/" + strFileName)
                End If
                uplImage.SaveAs(Server.MapPath("../Upload/Excel/" + strFileName))

                Dim xlApp As New Microsoft.Office.Interop.Excel.Application
                Dim xlWorkbook As Microsoft.Office.Interop.Excel.Workbook
                Dim xlWorksheet As Microsoft.Office.Interop.Excel.Worksheet
                Dim xlRange As Microsoft.Office.Interop.Excel.Range

                xlWorkbook = xlApp.Workbooks.Open(Server.MapPath("../Upload/Excel/" + strFileName))
                xlWorksheet = xlWorkbook.Sheets(1)
                xlRange = xlWorksheet.UsedRange

                Dim rowCount As Integer = xlRange.Rows.Count
                Dim strQuery As String = String.Empty
                'Dim strWhereCondition As String = String.Empty
                'Dim strColumnNames As String = String.Empty
                'Dim strValues As String = String.Empty
                Dim strNodeNumber As String = String.Empty
                Dim strEngName As String = String.Empty
                Dim strAraName As String = String.Empty
                Dim strParentNodeNumber As String = String.Empty

                Dim sqlConnection As New SqlConnection()
                Dim sqlCommand As SqlCommand = SqlConnection.CreateCommand()
                SqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                SqlConnection.Open()
                Try

                    For i As Integer = 1 To rowCount

                        strNodeNumber = ""
                        strEngName = ""
                        strAraName = ""
                        strParentNodeNumber = ""

                        'strWhereCondition = ""
                        'strColumnNames = ""
                        'strValues = ""

                        'If (CType(xlRange.Cells(i, 1), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level1 =" + "'" + CType(xlRange.Cells(i, 1), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level1,"
                        '    strValues = strValues + "'" + CType(xlRange.Cells(i, 1), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If
                        'If (CType(xlRange.Cells(i, 2), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level1A =" + "'" + CType(xlRange.Cells(i, 2), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level1A,"
                        '    strValues = strValues + "N" + "'" + CType(xlRange.Cells(i, 2), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If
                        'If (CType(xlRange.Cells(i, 3), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level2 =" + "'" + CType(xlRange.Cells(i, 3), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level2,"
                        '    strValues = strValues + "'" + CType(xlRange.Cells(i, 3), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If
                        'If (CType(xlRange.Cells(i, 4), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level2A =" + "'" + CType(xlRange.Cells(i, 4), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level2A,"
                        '    strValues = strValues + "N" + "'" + CType(xlRange.Cells(i, 4), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If
                        'If (CType(xlRange.Cells(i, 5), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level3 =" + "'" + CType(xlRange.Cells(i, 5), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level3,"
                        '    strValues = strValues + "'" + CType(xlRange.Cells(i, 5), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If
                        'If (CType(xlRange.Cells(i, 6), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level3A =" + "'" + CType(xlRange.Cells(i, 6), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level3A,"
                        '    strValues = strValues + "N" + "'" + CType(xlRange.Cells(i, 6), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If
                        'If (CType(xlRange.Cells(i, 7), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level4 =" + "'" + CType(xlRange.Cells(i, 7), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level4,"
                        '    strValues = strValues + "'" + CType(xlRange.Cells(i, 7), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If
                        'If (CType(xlRange.Cells(i, 8), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level4A =" + "'" + CType(xlRange.Cells(i, 8), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level4A,"
                        '    strValues = strValues + "N" + "'" + CType(xlRange.Cells(i, 8), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If
                        'If (CType(xlRange.Cells(i, 9), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level5 =" + "'" + CType(xlRange.Cells(i, 9), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level5,"
                        '    strValues = strValues + "'" + CType(xlRange.Cells(i, 9), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If
                        'If (CType(xlRange.Cells(i, 10), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level5A =" + "'" + CType(xlRange.Cells(i, 10), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level5A,"
                        '    strValues = strValues + "N" + "'" + CType(xlRange.Cells(i, 10), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If
                        'If (CType(xlRange.Cells(i, 11), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level6 =" + "'" + CType(xlRange.Cells(i, 11), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level6,"
                        '    strValues = strValues + "'" + CType(xlRange.Cells(i, 11), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If
                        'If (CType(xlRange.Cells(i, 12), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level6A =" + "'" + CType(xlRange.Cells(i, 12), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level6A,"
                        '    strValues = strValues + "N" + "'" + CType(xlRange.Cells(i, 12), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If
                        'If (CType(xlRange.Cells(i, 13), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level7 =" + "'" + CType(xlRange.Cells(i, 13), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level7,"
                        '    strValues = strValues + "'" + CType(xlRange.Cells(i, 13), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If
                        'If (CType(xlRange.Cells(i, 14), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level7A =" + "'" + CType(xlRange.Cells(i, 14), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level7A,"
                        '    strValues = strValues + "N" + "'" + CType(xlRange.Cells(i, 14), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If
                        'If (CType(xlRange.Cells(i, 15), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level8 =" + "'" + CType(xlRange.Cells(i, 15), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level8,"
                        '    strValues = strValues + "'" + CType(xlRange.Cells(i, 15), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If
                        'If (CType(xlRange.Cells(i, 16), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                        '    strWhereCondition = strWhereCondition + "Level8A =" + "'" + CType(xlRange.Cells(i, 16), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + " AND "
                        '    strColumnNames = strColumnNames + "Level8A,"
                        '    strValues = strValues + "N" + "'" + CType(xlRange.Cells(i, 16), Microsoft.Office.Interop.Excel.Range).Value2 + "'" + ","
                        'End If

                        If (CType(xlRange.Cells(i, 1), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                            strNodeNumber = CType(xlRange.Cells(i, 1), Microsoft.Office.Interop.Excel.Range).Value2

                            If (CType(xlRange.Cells(i, 2), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                                strEngName = CType(xlRange.Cells(i, 2), Microsoft.Office.Interop.Excel.Range).Value2
                            End If
                            If (CType(xlRange.Cells(i, 4), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                                strAraName = CType(xlRange.Cells(i, 4), Microsoft.Office.Interop.Excel.Range).Value2
                            End If
                            If (CType(xlRange.Cells(i, 3), Microsoft.Office.Interop.Excel.Range).Value2 IsNot Nothing) Then
                                strParentNodeNumber = CType(xlRange.Cells(i, 3), Microsoft.Office.Interop.Excel.Range).Value2
                            End If

                            strQuery = "SELECT COUNT(*) FROM MainHierarchy1 WHERE No = '" + strNodeNumber + "' "
                            sqlCommand.CommandText = strQuery

                            If (sqlCommand.ExecuteScalar().ToString() = "0") Then
                                'If (strColumnNames.Length > 0) Then
                                '    strColumnNames = strColumnNames + "Status"
                                '    strValues = strValues + "1"
                                '    'strColumnNames = Mid$(strColumnNames, 1, Len(strColumnNames) - 1)
                                '    'strValues = Mid$(strValues, 1, Len(strValues) - 1)
                                'End If
                                strQuery = "INSERT INTO Mainhierarchy1(NameE,NameA,Status,No,ParentNo) VALUES (N'" + strEngName + "', N'" + strAraName + "', 0, '" + strNodeNumber + "', '" + strParentNodeNumber + "')"
                                sqlCommand.CommandText = strQuery
                                sqlCommand.ExecuteNonQuery()
                            End If

                        End If

                        'If (strWhereCondition.Length > 4) Then
                        '    strWhereCondition = Mid$(strWhereCondition, 1, Len(strWhereCondition) - 4)

                        '    strQuery = "SELECT * FROM MainHierarchy WHERE " & strWhereCondition
                        '    sqlCommand.CommandText = strQuery

                        '    If (sqlCommand.ExecuteScalar() Is Nothing) Then
                        '        If (strColumnNames.Length > 0) Then
                        '            strColumnNames = strColumnNames + "Status"
                        '            strValues = strValues + "1"
                        '            'strColumnNames = Mid$(strColumnNames, 1, Len(strColumnNames) - 1)
                        '            'strValues = Mid$(strValues, 1, Len(strValues) - 1)
                        '        End If
                        '        strQuery = "INSERT INTO Mainhierarchy(" & strColumnNames & ") values (" & strValues & ")"
                        '        sqlCommand.CommandText = strQuery
                        '        sqlCommand.ExecuteNonQuery()
                        '    End If
                        'End If
                    Next
                    hdnInitialLoad.Value = "1"
                Catch ex As Exception
                    xlWorkbook.Close()
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp)
                    xlApp = Nothing
                    xlWorksheet = Nothing
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('15');", True)
                End Try

                xlWorkbook.Close()
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp)
                xlApp = Nothing
                xlWorksheet = Nothing
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('14');", True)
                'ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "HideDiv();", True)
            Else
                hdnInitialLoad.Value = "0"
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('13');", True)
                'ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "HideDiv();", True)
                Return
            End If
        Else
            hdnInitialLoad.Value = "0"
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('13');", True)
            'ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "HideDiv();", True)
            Return
        End If
    End Sub
End Class