﻿Public Class Productivity_List
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
                Menuhide.Value = "close"

                grdLabours.Columns(1).Visible = True
                grdLabours.Columns(2).Visible = True
                grdLabours.Columns(3).Visible = True
                grdLabours.Columns(4).Visible = True

                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    'header1.Style.Add("float", "right")
                    'header2.Style.Add("float", "right")
                    grdLabours.Columns(1).Visible = False
                    grdLabours.Columns(3).Visible = False
                Else
                    grdLabours.Columns(2).Visible = False
                    grdLabours.Columns(4).Visible = False
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
        LoadDataGrid()
        Panel1.Width = grdLabours.Width
    End Sub
    Private Sub LoadDataGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        dt = objSqlConfig.GetAllProductivities(txtSearch.Text)
        If (dt.Rows.Count > 0) Then
            grdLabours.DataSource = dt
            grdLabours.DataBind()
        Else
            grdLabours.DataSource = Nothing
            grdLabours.DataBind()
        End If
    End Sub
    Protected Sub grdSupplier_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdLabours.Sorting
        If (ViewState("SortState") IsNot Nothing And ViewState("SortState") = SortDirection.Ascending) Then
            ViewState("SortDirection") = "asc"
            ViewState("SortState") = 1
        Else
            ViewState("SortDirection") = "desc"
            ViewState("SortState") = 0
        End If
        ViewState("SortExp") = e.SortExpression
        FillGrid()
    End Sub
    Protected Sub grdSupplier_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdLabours.PageIndexChanging
        grdLabours.PageIndex = e.NewPageIndex
        grdLabours.DataBind()
    End Sub
    Private Sub FillGrid()
        Dim Data As DataTable = grdLabours.DataSource
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp") Is Nothing) Then
                If ViewState("SortDirection").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp") & " " & "desc"
                End If

                grdLabours.DataSource = dv
                grdLabours.DataBind()
            End If
        End If
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        LoadDataGrid()
    End Sub
    Protected Sub grdLabours_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdLabours.SelectedIndexChanged
        If (grdLabours.SelectedIndex >= 0) Then
            Response.Redirect("Productivity_New.aspx?ID=" + grdLabours.Rows(grdLabours.SelectedIndex).Cells(0).Text)
        End If
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Response.Redirect("Productivity_New.aspx")
    End Sub
End Class