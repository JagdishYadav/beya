﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Supplier_Products.aspx.vb" Inherits="CEA.FORM2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
        .update
        {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">

        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorImageFile") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdAlreadyAdded") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdAdded") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorProdNotFound") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdDeleted") %>';
            }
            else if (msgno == 6) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ProdUpdated") %>';
            }
            else if (msgno == 7) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorOtherMissing") %>';
            }
            alert(msgstring);
        }

        function checkBoxListOnClick(elementRef) {
            var textBoxRef = document.getElementById('<%= txtOther.ClientID %>');
            if (elementRef.rows.length > 0) {
                checkboxCell = elementRef.rows[elementRef.rows.length - 1].cells[1];
                for (j = 0; j < checkboxCell.childNodes.length; j++) {
                    if (checkboxCell.childNodes[j].type == "checkbox") {

                        if (checkboxCell.childNodes[j].checked == true) {
                            textBoxRef.style.display = '';
                        }
                        else {
                            textBoxRef.style.display = 'None';
                        }
                    }
                }
            }

            return true;
        }
        function checkOtherValue(source, args) {
            var textBoxRef = document.getElementById('<%= txtOther.ClientID %>');
            if (textBoxRef.style.display == '' && textBoxRef.value == '') {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }


        function checkBoxListOnClick1(elementRef) {
            var textBoxRef = document.getElementById('<%= txtOther1.ClientID %>');
            var checkBoxArray = elementRef.getElementsByTagName('input');

            var checkBoxRef = checkBoxArray[checkBoxArray.length - 1];
            if (checkBoxRef.checked == true) {
                textBoxRef.style.display = '';
            }
            else {
                textBoxRef.style.display = 'None';
            }
            return true;
        }
        function checkOtherValue1(source, args) {
            var textBoxRef = document.getElementById('<%= txtOther1.ClientID %>');
            if (textBoxRef.style.display == '' && textBoxRef.value == '') {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }

    </script>
    <style>
        input[type="text"], button, .button
        {
            min-height: 1.9125rem;
            padding: 0.1rem;
            margin: 0;
            font-size: 0.7925rem !important;
        }
        .label, .label1, td, td label, td input, table
        {
            font-size: 0.7925rem !important;
            line-height: normal !important;
            padding: 0 !important;
            margin: 0 !important;
        }
        button, .button
        {
            padding-top: 0.375rem !important;
            padding-right: 1.0rem !important;
            padding-bottom: 0.3375rem !important;
        }
        button, .button</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label13" runat="server" Text='<%$ Resources:Resource, ProductDetail %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <h3>
                <asp:Label ID="lblHeader" runat="server" Text='<%$ Resources:Resource, AddingProd %>'></asp:Label>
                <asp:Label ID="lblPath" runat="server" Text="" Font-Bold="True" Font-Size="Smaller"></asp:Label>
            </h3>
            <div class="clear"></div>
            <asp:Panel ID="Panel2" runat="server" CssClass="panel">
                <div class="row">
                    <div class="large-6 medium-6 column">
                        <asp:Label ID="Label18" runat="server" Text="Search" Width="100%"></asp:Label>
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="left" Width="70%"></asp:TextBox><asp:Button
                            ID="Button5" runat="server" Text="Search" CssClass="BTNSearch" />
                    </div>
                     <asp:Label ID="Label19" runat="server" Text="" Width="10%"></asp:Label>
                      <div class="large-6 medium-6 column" id="div3" runat="server">
                      
                            </div>
                </div>
                <div style="width: 100%; height: 300px; overflow: scroll">
                    <asp:GridView ID="grdProducts" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        AllowSorting="true" EmptyDataText="No Data" Width="100%" PageSize="10" DataKeyNames="ID,RefNo,ApprovedBy" HeaderStyle-HorizontalAlign ="Center">
                        <Columns>
                            <asp:BoundField DataField="MapID" HeaderText="MapID" ItemStyle-Width="0px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="NameE" SortExpression="NameE" HeaderText='<%$ Resources:Resource, NameE %>'
                                ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="NameA" SortExpression="NameA" HeaderText='<%$ Resources:Resource, NameA %>'
                                ItemStyle-Width="60px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="PropE" SortExpression="PropE" HeaderText='<%$ Resources:Resource, FeatE %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="PropA" SortExpression="PropA" HeaderText='<%$ Resources:Resource, FeatA %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="DescE" SortExpression="DescE" HeaderText='<%$ Resources:Resource, DescriptionE %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="DescA" SortExpression="DescA" HeaderText='<%$ Resources:Resource, DescriptionA %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="Unit" SortExpression="Unit" HeaderText='<%$ Resources:Resource, Unit %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="ManHour" SortExpression="ManHour" HeaderText='<%$ Resources:Resource, ManHour %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" Visible = "false"/>
                            <asp:BoundField DataField="EquipHour" SortExpression="EquipHour" HeaderText='<%$ Resources:Resource, EquipHour %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" Visible = "false"/>
                            <asp:BoundField DataField="ApprovedByE" SortExpression="ApprovedByE" HeaderText='<%$ Resources:Resource, ApprovedBy %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="ApprovedByA" SortExpression="ApprovedByA" HeaderText='<%$ Resources:Resource, ApprovedBy %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="SpecificationE" SortExpression="SpecificationE" HeaderText='<%$ Resources:Resource, Specification %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="SpecificationA" SortExpression="SpecificationA" HeaderText='<%$ Resources:Resource, Specification %>'
                                ItemStyle-Width="30px" ItemStyle-Wrap="true" />
                            <asp:CommandField ItemStyle-HorizontalAlign ="Center" ShowSelectButton="true" ItemStyle-Width="25px" SelectText='<%$ Resources:Resource, Select %>' />
                        </Columns>
                    </asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" CssClass="panel">
                <div class="row">
                    <div  id="div1" runat="server">
                        <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, ProductID %>'
                            CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtProdID" runat="server" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvID" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorID %>'
                            ControlToValidate="txtProdID" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, NameE %>' CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtNameE" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNameE" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNameE %>'
                            ControlToValidate="txtNameE" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, NameA %>' CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtNameA" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNameA" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNameA %>'
                            ControlToValidate="txtNameA" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        <asp:Label ID="Label4" runat="server" Text='<%$ Resources:Resource, DescriptionE %>'
                            CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtDescE" runat="server" TextMode="MultiLine" Rows="2" MaxLength="500"></asp:TextBox>
                        <asp:Label ID="Label5" runat="server" Text='<%$ Resources:Resource, DescriptionA %>'
                            CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtDescA" runat="server" TextMode="MultiLine" Rows="2" MaxLength="500"></asp:TextBox>
                        <asp:Label ID="Label6" runat="server" Text="File of Specifications" CssClass="label1"></asp:Label>
                        <asp:FileUpload ID="uplFile" runat="server" CssClass="small button " Text='<%$ Resources:Resource, Browse %>' />
                        <asp:Label ID="Label7" runat="server" Text='<%$ Resources:Resource, FeatE %>' CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtFeatE" runat="server" MaxLength="100"></asp:TextBox>
                        <asp:Label ID="Label8" runat="server" Text='<%$ Resources:Resource, FeatA %>' CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtFeatA" runat="server" MaxLength="100"></asp:TextBox>
                        <asp:Label ID="Label9" runat="server" Text='<%$ Resources:Resource, Image %>' CssClass="label1"></asp:Label>
                        <asp:FileUpload ID="uplImage" runat="server" CssClass="small button " Text='<%$ Resources:Resource, Browse %>' />
                        <asp:Label ID="Label10" runat="server" Text='<%$ Resources:Resource, Unit %>' CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtUnit" runat="server" MaxLength="50"></asp:TextBox>
                        <%--<asp:Label ID="Label14" runat="server" Text='<%$ Resources:Resource, ManHour %>'
                            CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtManHour" runat="server" Text="0.00" MaxLength="15"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="reManHour" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                            SetFocusOnError="true" ControlToValidate="txtManHour" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                            Display="None" ValidationGroup="Save"> </asp:RegularExpressionValidator>
                        <asp:Label ID="Label15" runat="server" Text='<%$ Resources:Resource, EquipHour %>'
                            CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtEquipHour" runat="server" Text="0.00" MaxLength="15"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="reEquip" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                            SetFocusOnError="true" ControlToValidate="txtEquipHour" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                            Display="None" ValidationGroup="Save"> </asp:RegularExpressionValidator>--%>
                        <asp:Label ID="Label16" runat="server" Text='<%$ Resources:Resource, ApprovedBy %>'
                            CssClass="label1"></asp:Label>
                        
                        <asp:GridView ID="grdApprovedBy" runat="server" Font-Size="Smaller" Width="100%"
                            AutoGenerateColumns="false" DataKeyNames="ID" HeaderStyle-HorizontalAlign ="Center">
                            <Columns>
                                <asp:TemplateField HeaderText='<%$ Resources:Resource, ApprovedBy %>'>
                                    <ItemTemplate>
                                        <asp:Label ID="lblNameE" runat="server" Text='<%# Eval("NameE")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:Resource, ApprovedBy %>'>
                                    <ItemTemplate>
                                        <asp:Label ID="lblNameA" runat="server" Text='<%# Eval("NameA")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkApproved" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:Resource, ReferenceNo %>'>
                                    <ItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("RefNo")%>' MaxLength="15"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:TextBox ID="txtOther" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:CustomValidator ID="cvmodulelist" runat="server" Display="None" ErrorMessage='<%$ Resources:Resource, ErrorOtherMissing %>'
                            ClientValidationFunction="checkOtherValue" ValidationGroup="Save"></asp:CustomValidator>
                        <asp:Label ID="Label17" runat="server" Text='<%$ Resources:Resource, Specification %>'
                            CssClass="label1"></asp:Label>
                        <asp:CheckBoxList ID="chkSpec" runat="server" Font-Size="Smaller" Width="100%">
                        </asp:CheckBoxList>
                        <asp:TextBox ID="txtOther1" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:CustomValidator ID="chkOther1" runat="server" Display="None" ErrorMessage='<%$ Resources:Resource, ErrorOtherMissing1 %>'
                            ClientValidationFunction="checkOtherValue1" ValidationGroup="Save"></asp:CustomValidator>
                        <asp:Label ID="Label11" runat="server" Text='<%$ Resources:Resource, Status %>' CssClass="label1"></asp:Label>
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text='<%$ Resources:Resource, Pending %>' Value="0"></asp:ListItem>
                            <asp:ListItem Text='<%$ Resources:Resource, Approve %>' Value="1"></asp:ListItem>
                            <asp:ListItem Text='<%$ Resources:Resource, Reject %>' Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:Label ID="Label12" runat="server" Text='<%$ Resources:Resource, Comments %>'
                            CssClass="label1"></asp:Label>
                        <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </div>
                    <div class="large-6 medium-6 column" id="div2" runat="server">
                        
                        <asp:Image ID="Image1" runat="server" Width="90%" CssClass="ImageBox" />
                    </div>
            </asp:Panel>
            
            <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
                ShowMessageBox="true" ValidationGroup="Save" DisplayMode="BulletList" />
        </ContentTemplate>
        <%--<Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>--%>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" runat="server" contentplaceholderid="MiddlePart">
<div class="row">
                
                 <div class="large-12 medium-12 column" id="buttonsrow" runat="server">
                    <div class="outerDivBtn">
                        <div class="BTNdiv">
                    <asp:Button ID="btnSave" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNUpdate"
                        ValidationGroup="Save" />
                    <asp:Button ID="btnCancel" runat="server" Text='<%$ Resources:Resource, New %>' CssClass="BTNAdd" />
                      <asp:LinkButton ID="LinkButton1" runat="server" Text='<%$ Resources:Resource, ViewProduct %>'
                            CssClass="BTNView right"></asp:LinkButton>
                </div></div>
            </div>
            </div>
</asp:Content>

