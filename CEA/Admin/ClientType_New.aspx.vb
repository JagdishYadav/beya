﻿Public Class ClientType_New
    Inherits FMS.Culture.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (menuItems.Count > 15) Then
                    menuItems.RemoveAt(12)
                    menuItems.RemoveAt(12)
                    menuItems.RemoveAt(12)
                    menuItems.RemoveAt(12)
                End If
                
                If (UICulture = "Arabic") Then
                    
                Else
                   
                End If

                
            Else
                Response.Redirect("../Login.aspx")
            End If
        End If
    End Sub
    Protected Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        txtNameA.Text = ""
        txtNameE.Text = ""
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim objSqlConfig As New ConfigClassSqlServer()
        If (objSqlConfig.Insert_Clinet_Type(txtNameE.Text.Trim(), txtNameA.Text.Trim()) = "0") Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
        End If
    End Sub
End Class