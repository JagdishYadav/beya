﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="NewEquipment.aspx.vb" Inherits="CEA.NewEquipment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">

        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Success") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "UpdateSuccess") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteSuccess") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorAlreadyExist") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorNotExist") %>';
            }

            alert(msgstring);
        }
    </script>
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    New Equipment
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="addNewEquipmentWrapper">

                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <asp:Label ID="lblCode" runat="server" Text='<%$ Resources:Resource, Code %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtCode"></asp:Label>
                            <asp:TextBox ID="txtCode" runat="server" MaxLength="20" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorCode %>'
                                ControlToValidate="txtCode" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, DescriptionE %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtDescE"></asp:Label>
                            <asp:TextBox ID="txtDescE" runat="server" MaxLength="60" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvDescE" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorDescE %>'
                                ControlToValidate="txtDescE" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <asp:Label ID="Label11" runat="server" Text='<%$ Resources:Resource, DescriptionA %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtDescA"></asp:Label>
                            <asp:TextBox ID="txtDescA" runat="server" MaxLength="60" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvDescA" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorDescA %>'
                                ControlToValidate="txtDescA" SetFocusOnError="true" ValidationGroup="Save" Display="None"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, Unit %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtUnit"></asp:Label>
                            <asp:TextBox ID="txtUnit" runat="server" MaxLength="25" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <asp:Label ID="Label3" runat="server" Text='<%$ Resources:Resource, OperationCost %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="reCost1"></asp:Label>
                            <asp:TextBox ID="txtOpCost" runat="server" MaxLength="18" Text="0.00" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reCost1" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtOpCost" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Save">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <asp:Label ID="Label4" runat="server" Text='<%$ Resources:Resource, DepCost %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtDepCost"></asp:Label>
                            <asp:TextBox ID="txtDepCost" runat="server" MaxLength="18" Text="0.00" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reCost2" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtDepCost" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Save">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <asp:Label ID="Label5" runat="server" Text='<%$ Resources:Resource, DailyCost %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtDailyCost"></asp:Label>
                            <asp:TextBox ID="txtDailyCost" runat="server" MaxLength="18" Text="0.00" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reCost3" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtDailyCost" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Save">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <asp:Label ID="Label6" runat="server" Text='<%$ Resources:Resource, RentalCostDay %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtRentD"></asp:Label>
                            <asp:TextBox ID="txtRentD" runat="server" MaxLength="18" Text="0.00" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reCost4" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtRentD" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Save">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <asp:Label ID="Label7" runat="server" Text='<%$ Resources:Resource, RentalCostMonth %>' SkinID="Controllabel" CssClass="control-label mb-2" AssociatedControlID="txtRentM"></asp:Label>
                            <asp:TextBox ID="txtRentM" runat="server" MaxLength="18" Text="0.00" SkinID="FormControl" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="reCost5" runat="server" ErrorMessage='<%$ Resources:Resource, ErrorNumeric %>'
                                SetFocusOnError="true" ControlToValidate="txtRentM" ValidationExpression="^(([0-9]*)|(([0-9]*).([0-9]*)))$"
                                Display="None" ValidationGroup="Save">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                </div>

                <div class="text-right mt-4 buttonActionGroup">
                    <div class="btn-group commanButtonGroup mb-3">
                        <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="btn btn-primary btn-min-120" ValidationGroup="Save" />
                        <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Cancel %>' CssClass="btn btn-primary btn-min-120" />
                        <asp:Button ID="btnNew" runat="server" Text='<%$ Resources:Resource, New %>' CssClass="btn btn-primary btn-min-120" />
                    </div>
                </div>

            </asp:Panel>
            <%--<div class="row">
        <div class="large-9 columns">
        </div>
        <div class="large-3 columns">
            <asp:Button ID="BtnDone" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="small button" />
            <asp:Button ID="BtnCencel" runat="server" Text='<%$ Resources:Resource, Cancel %>' CssClass="small button alert" />
        </div>
        </div>--%>
            <div class="large-9 columns">
                <asp:ValidationSummary ID="vsError" runat="server" EnableClientScript="true" ShowSummary="false"
                    ShowMessageBox="true" ValidationGroup="Save" DisplayMode="BulletList" />
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    <script>$(document).ready(function () { $("#sidr").show(); });</script>
</asp:Content>
<%--<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MiddlePart">
    <div class="row">
        <div class="large-12 medium-12 column">
            <div class="outerDivBtn">
                <div class="BTNdiv">

                    <asp:Button ID="Button1" runat="server" Text='<%$ Resources:Resource, Save %>' CssClass="BTNSave"
                        ValidationGroup="Save" />

                    <asp:Button ID="Button2" runat="server" Text='<%$ Resources:Resource, Cancel %>'
                        CssClass="BTNCancel" />

                    <asp:Button ID="btnNew" runat="server" Text='<%$ Resources:Resource, New %>' CssClass="BTNEdit" />

                </div>
            </div>
        </div>
    </div>
</asp:Content>--%>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="PageTitle">
    <asp:Label ID="lblHeader" runat="server" Text='<%$ Resources:Resource, CreateEquipment %>'></asp:Label></h3>
</asp:Content>


