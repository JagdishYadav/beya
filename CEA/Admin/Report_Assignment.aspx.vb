﻿Public Class Report_Assignment
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            If (Request.IsAuthenticated) Then
                Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
                Dim menuItems As MenuItemCollection = Menu1.Items
                If (menuItems.Count > 15) Then
                    menuItems.RemoveAt(12)
                    menuItems.RemoveAt(12)
                    menuItems.RemoveAt(12)
                    menuItems.RemoveAt(12)
                End If
                Dim objSqlConfig As New ConfigClassSqlServer()
                DropDownList1.DataSource = objSqlConfig.GetAllRoles()
                DropDownList1.DataValueField = "ID"

                If (UICulture = "Arabic") Then
                    DropDownList1.DataTextField = "NameA"
                    DropDownList1.DataBind()
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    Dim item As New ListItem("اختر", 0)
                    DropDownList1.Items.Insert(0, item)
                Else
                    DropDownList1.DataTextField = "NameE"
                    DropDownList1.DataBind()
                    Dim item As New ListItem("Select", 0)
                    DropDownList1.Items.Insert(0, item)
                End If
                DropDownList1.SelectedIndex = 0
            Else
                Response.Redirect("../Login.aspx")
            End If
            LoadDataGrid()
        End If
    End Sub
    Private Sub LoadDataGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        dt = objSqlConfig.GetAllReports()
        If (dt.Rows.Count > 0) Then
            grdCategories.DataSource = dt
            grdCategories.DataBind()
            ViewState("dt") = dt
        Else
            grdCategories.DataSource = Nothing
            grdCategories.DataBind()
        End If

    End Sub
    Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        DropDownList1.SelectedIndex = 0
        For i As Integer = 0 To grdCategories.Rows.Count - 1
            Dim cbox As CheckBox = grdCategories.Rows(i).FindControl("CheckBox1")
            If (cbox IsNot Nothing) Then
                cbox.Checked = False
            End If
        Next
    End Sub
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        If (DropDownList1.SelectedIndex = 0) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('6');", True)
            Return
        End If
        Dim cat_selected As Boolean = False
        Dim strCategories As String = String.Empty
        Dim dt As New DataTable
        If (ViewState("dt") IsNot Nothing) Then
            dt = ViewState("dt")
        End If
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim cbox As CheckBox = grdCategories.Rows(i).FindControl("CheckBox1")
            If (cbox.Checked) Then
                strCategories = strCategories + dt.Rows(i).Item(0).ToString() + ","
                cat_selected = True
            End If
        Next
        If (cat_selected = True) Then
            strCategories = strCategories.Substring(0, strCategories.Length - 1)
            Dim objSqlConfig As New ConfigClassSqlServer()
            Dim retVal As Integer = objSqlConfig.Insert_Report_Role_Mapping(DropDownList1.SelectedValue.ToString(), strCategories.Trim())
            If (retVal = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('5');", True)
        End If
    End Sub
    Protected Sub DropDownList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownList1.SelectedIndexChanged
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt, dt1 As New DataTable()

        If (ViewState("dt") IsNot Nothing) Then
            dt1 = ViewState("dt")
        End If

        For j As Integer = 0 To grdCategories.Rows.Count - 1
            Dim cbox As CheckBox = grdCategories.Rows(j).FindControl("CheckBox1")
            cbox.Checked = False
        Next
        dt = objSqlConfig.GetReport_Role_Mapping(DropDownList1.SelectedValue)
        If (dt.Rows.Count > 0) Then
            Dim catArray As String() = dt.Rows(0).Item(0).ToString().Split(",")
            For i As Integer = 0 To catArray.Length - 1
                For j As Integer = 0 To dt1.Rows.Count - 1
                    If (dt1.Rows(j).Item(0).ToString() = catArray(i)) Then
                        Dim cbox As CheckBox = grdCategories.Rows(j).FindControl("CheckBox1")
                        cbox.Checked = True
                    End If
                Next
            Next
        End If
    End Sub
End Class