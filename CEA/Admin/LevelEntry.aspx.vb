﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Imports System.Web.Script.Services

Public Class LevelEntry
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then

            tvLevels.Attributes.Add("onclick", "NodeClicked(event)")

            Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
            Dim menuItems As MenuItemCollection = Menu1.Items
            If (Session("User_Role_ID").ToString().Contains("999")) Then
                If (menuItems.Count > 15) Then
                    menuItems.RemoveAt(12)
                    menuItems.RemoveAt(12)
                    menuItems.RemoveAt(12)
                    menuItems.RemoveAt(12)
                End If
            ElseIf (Session("User_Role_ID").ToString().Contains("888")) Then
                If (menuItems.Count > 5) Then
                    menuItems.RemoveAt(0)
                    menuItems.RemoveAt(1)
                    menuItems.RemoveAt(2)
                    menuItems.RemoveAt(2)
                    menuItems.RemoveAt(5)
                    menuItems.RemoveAt(5)
                    menuItems.RemoveAt(5)
                    menuItems.RemoveAt(5)
                    menuItems.RemoveAt(5)
                    menuItems.RemoveAt(5)
                    menuItems.RemoveAt(5)
                End If
            End If
            Button8.Attributes.Add("style", "display:none")

            'Dim languageSelect As HiddenField = Me.Page.Master.FindControl("hfMenuLanguage")
            'If languageSelect.Value = "Arabic" Then
            If (UICulture = "Arabic") Then
                tvLevels.LineImagesFolder = "~/TreeLineImagesAr"
                lnkSet.Attributes.Add("style", "margin-right:121px !important;")
                tvLevels.Attributes.Add("style", "margin-top: 20px;")
            End If
            'load all crew structures in dropdown
            Dim objSqlConfig As New ConfigClassSqlServer()
            ddlCrew.DataSource = objSqlConfig.GetCrewStructureCodesAll(Session("CompanyID"))
            ddlCrew.DataBind()

            Dim listItem As New ListItem("None", "0")
            ddlCrew.Items.Insert(0, listItem)

            If (ddlCrew.Items.Count > 0) Then
                ddlCrew.SelectedIndex = 0
            End If

            btnDeleteAll.Attributes("onclick") = "if(ShowConfirmDelete() == false){ return false; }"
        End If
    End Sub
    'Private Sub LoadTreeViewData()
    '    Dim strClientTypeID As String = "1"
    '    If (Session("ClientTypeID") IsNot Nothing) Then
    '        strClientTypeID = Session("ClientTypeID")
    '    End If
    '    If (hdnInitialLoad.Value.ToString().Equals("1")) Then
    '        Dim sqlConnection As New SqlConnection()
    '        Dim node As TreeNode = Nothing
    '        'Dim NodeLevel1, NodeLevel2, NodeLevel3, NodeLevel4, NodeLevel5, NodeLevel6, NodeLevel7 As TreeNode
    '        'Dim NodeLevel1 As TreeNode
    '        tvLevels.Nodes.Clear()

    '        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
    '        sqlCommand.CommandTimeout = 120000
    '        'Dim r1, r2, r3, r4, r5, r6, r7, r8 As SqlDataReader
    '        Dim r1 As SqlDataReader
    '        'Dim dt1, dt2, dt3, dt4, dt5, dt6, dt7, dt8 As New DataTable()
    '        Dim dt1 As New DataTable()

    '        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
    '        sqlConnection.Open()

    '        If (UICulture = "Arabic") Then
    '            sqlCommand.CommandText = "SELECT DISTINCT ID,  NameA, NameE, CAST(No As Numeric(25))No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
    '            r1 = sqlCommand.ExecuteReader()
    '            dt1.Load(r1)
    '            r1.Close()

    '            For Each dr1 As DataRow In dt1.Rows
    '                If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
    '                    node = New TreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
    '                    node.Target = dr1.Item(2).ToString()
    '                    'node.ImageUrl = dr1.Item(3).ToString()
    '                    node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
    '                    tvLevels.Nodes.Add(node)
    '                    'NodeLevel1 = node

    '                    'sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                    'r2 = sqlCommand.ExecuteReader()
    '                    'dt2.Clear()
    '                    'dt2.Load(r2)
    '                    'r2.Close()
    '                    'For Each dr2 As DataRow In dt2.Rows
    '                    '    If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
    '                    '        node = New TreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
    '                    '        node.Target = dr2.Item(2).ToString()
    '                    '        'node.ImageUrl = dr2.Item(3).ToString() 
    '                    '        node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
    '                    '        NodeLevel1.ChildNodes.Add(node)
    '                    '        NodeLevel2 = node

    '                    '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                    '        r3 = sqlCommand.ExecuteReader()
    '                    '        dt3.Clear()
    '                    '        dt3.Load(r3)
    '                    '        r3.Close()
    '                    '        For Each dr3 As DataRow In dt3.Rows
    '                    '            If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
    '                    '                node = New TreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
    '                    '                node.Target = dr3.Item(2).ToString()
    '                    '                'node.ImageUrl = dr3.Item(3).ToString()
    '                    '                node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
    '                    '                NodeLevel2.ChildNodes.Add(node)
    '                    '                NodeLevel3 = node

    '                    '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                    '                r4 = sqlCommand.ExecuteReader()
    '                    '                dt4.Clear()
    '                    '                dt4.Load(r4)
    '                    '                r4.Close()

    '                    '                For Each dr4 As DataRow In dt4.Rows
    '                    '                    If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
    '                    '                        node = New TreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
    '                    '                        node.Target = dr4.Item(2).ToString()
    '                    '                        'node.ImageUrl = dr4.Item(3).ToString()
    '                    '                        node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
    '                    '                        NodeLevel3.ChildNodes.Add(node)
    '                    '                        NodeLevel4 = node

    '                    '                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                    '                        r5 = sqlCommand.ExecuteReader()
    '                    '                        dt5.Clear()
    '                    '                        dt5.Load(r5)
    '                    '                        r5.Close()
    '                    '                        For Each dr5 As DataRow In dt5.Rows
    '                    '                            If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
    '                    '                                node = New TreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
    '                    '                                node.Target = dr5.Item(2).ToString()
    '                    '                                'node.ImageUrl = dr5.Item(3).ToString()
    '                    '                                node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
    '                    '                                NodeLevel4.ChildNodes.Add(node)
    '                    '                                NodeLevel5 = node

    '                    '                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                    '                                r6 = sqlCommand.ExecuteReader()
    '                    '                                dt6.Clear()
    '                    '                                dt6.Load(r6)
    '                    '                                r6.Close()
    '                    '                                For Each dr6 As DataRow In dt6.Rows
    '                    '                                    If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
    '                    '                                        node = New TreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
    '                    '                                        node.Target = dr6.Item(2).ToString()
    '                    '                                        'node.ImageUrl = dr6.Item(3).ToString()
    '                    '                                        node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
    '                    '                                        NodeLevel5.ChildNodes.Add(node)
    '                    '                                        NodeLevel6 = node

    '                    '                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                    '                                        r7 = sqlCommand.ExecuteReader()
    '                    '                                        dt7.Clear()
    '                    '                                        dt7.Load(r7)
    '                    '                                        r7.Close()
    '                    '                                        For Each dr7 As DataRow In dt7.Rows
    '                    '                                            If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
    '                    '                                                node = New TreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
    '                    '                                                node.Target = dr7.Item(2).ToString()
    '                    '                                                'node.ImageUrl = dr7.Item(3).ToString()
    '                    '                                                node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
    '                    '                                                NodeLevel6.ChildNodes.Add(node)
    '                    '                                                NodeLevel7 = node

    '                    '                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                    '                                                r8 = sqlCommand.ExecuteReader()
    '                    '                                                dt8.Clear()
    '                    '                                                dt8.Load(r8)
    '                    '                                                r8.Close()
    '                    '                                                For Each dr8 As DataRow In dt8.Rows
    '                    '                                                    If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
    '                    '                                                        node = New TreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
    '                    '                                                        node.Target = dr8.Item(2).ToString()
    '                    '                                                        'node.ImageUrl = dr8.Item(3).ToString()
    '                    '                                                        node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
    '                    '                                                        NodeLevel7.ChildNodes.Add(node)
    '                    '                                                    End If
    '                    '                                                Next
    '                    '                                            End If
    '                    '                                        Next
    '                    '                                    End If
    '                    '                                Next
    '                    '                            End If
    '                    '                        Next
    '                    '                    End If
    '                    '                Next
    '                    '            End If
    '                    '        Next
    '                    '    End If
    '                    'Next
    '                End If
    '            Next

    '        Else
    '            sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
    '            r1 = sqlCommand.ExecuteReader()
    '            dt1.Load(r1)
    '            r1.Close()

    '            For Each dr1 As DataRow In dt1.Rows
    '                If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
    '                    node = New TreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
    '                    node.Target = dr1.Item(2).ToString()
    '                    'node.ImageUrl = dr1.Item(3).ToString()
    '                    node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
    '                    tvLevels.Nodes.Add(node)
    '                    'NodeLevel1 = node

    '                    'sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                    'r2 = sqlCommand.ExecuteReader()
    '                    'dt2.Clear()
    '                    'dt2.Load(r2)
    '                    'r2.Close()
    '                    'For Each dr2 As DataRow In dt2.Rows
    '                    '    If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
    '                    '        node = New TreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
    '                    '        node.Target = dr2.Item(2).ToString()
    '                    '        'node.ImageUrl = dr2.Item(3).ToString()
    '                    '        node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
    '                    '        NodeLevel1.ChildNodes.Add(node)
    '                    '        NodeLevel2 = node

    '                    '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                    '        r3 = sqlCommand.ExecuteReader()
    '                    '        dt3.Clear()
    '                    '        dt3.Load(r3)
    '                    '        r3.Close()
    '                    '        For Each dr3 As DataRow In dt3.Rows
    '                    '            If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
    '                    '                node = New TreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
    '                    '                node.Target = dr3.Item(2).ToString()
    '                    '                'node.ImageUrl = dr3.Item(3).ToString()
    '                    '                node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
    '                    '                NodeLevel2.ChildNodes.Add(node)
    '                    '                NodeLevel3 = node

    '                    '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                    '                r4 = sqlCommand.ExecuteReader()
    '                    '                dt4.Clear()
    '                    '                dt4.Load(r4)
    '                    '                r4.Close()

    '                    '                For Each dr4 As DataRow In dt4.Rows
    '                    '                    If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
    '                    '                        node = New TreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
    '                    '                        node.Target = dr4.Item(2).ToString()
    '                    '                        'node.ImageUrl = dr4.Item(3).ToString()
    '                    '                        node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
    '                    '                        NodeLevel3.ChildNodes.Add(node)
    '                    '                        NodeLevel4 = node

    '                    '                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                    '                        r5 = sqlCommand.ExecuteReader()
    '                    '                        dt5.Clear()
    '                    '                        dt5.Load(r5)
    '                    '                        r5.Close()
    '                    '                        For Each dr5 As DataRow In dt5.Rows
    '                    '                            If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
    '                    '                                node = New TreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
    '                    '                                node.Target = dr5.Item(2).ToString()
    '                    '                                'node.ImageUrl = dr5.Item(3).ToString()
    '                    '                                node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
    '                    '                                NodeLevel4.ChildNodes.Add(node)
    '                    '                                NodeLevel5 = node

    '                    '                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                    '                                r6 = sqlCommand.ExecuteReader()
    '                    '                                dt6.Clear()
    '                    '                                dt6.Load(r6)
    '                    '                                r6.Close()
    '                    '                                For Each dr6 As DataRow In dt6.Rows
    '                    '                                    If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
    '                    '                                        node = New TreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
    '                    '                                        node.Target = dr6.Item(2).ToString()
    '                    '                                        'node.ImageUrl = dr6.Item(3).ToString()
    '                    '                                        node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
    '                    '                                        NodeLevel5.ChildNodes.Add(node)
    '                    '                                        NodeLevel6 = node

    '                    '                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                    '                                        r7 = sqlCommand.ExecuteReader()
    '                    '                                        dt7.Clear()
    '                    '                                        dt7.Load(r7)
    '                    '                                        r7.Close()
    '                    '                                        For Each dr7 As DataRow In dt7.Rows
    '                    '                                            If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
    '                    '                                                node = New TreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
    '                    '                                                node.Target = dr7.Item(2).ToString()
    '                    '                                                'node.ImageUrl = dr7.Item(3).ToString()
    '                    '                                                node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
    '                    '                                                NodeLevel6.ChildNodes.Add(node)
    '                    '                                                NodeLevel7 = node

    '                    '                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
    '                    '                                                r8 = sqlCommand.ExecuteReader()
    '                    '                                                dt8.Clear()
    '                    '                                                dt8.Load(r8)
    '                    '                                                r8.Close()
    '                    '                                                For Each dr8 As DataRow In dt8.Rows
    '                    '                                                    If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
    '                    '                                                        node = New TreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
    '                    '                                                        node.Target = dr8.Item(2).ToString()
    '                    '                                                        'node.ImageUrl = dr8.Item(3).ToString()
    '                    '                                                        node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
    '                    '                                                        NodeLevel7.ChildNodes.Add(node)
    '                    '                                                    End If
    '                    '                                                Next
    '                    '                                            End If
    '                    '                                        Next
    '                    '                                    End If
    '                    '                                Next
    '                    '                            End If
    '                    '                        Next
    '                    '                    End If
    '                    '                Next
    '                    '            End If
    '                    '        Next
    '                    '    End If
    '                    'Next
    '                End If
    '            Next
    '        End If

    '        tvLevels.Visible = True
    '        sqlConnection.Close()
    '        tvLevels.ExpandAll()


    '    End If
    '    'ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "HideDiv();", True)
    'End Sub
    Protected Sub tvLevels_SelectedNodeChanged(sender As Object, e As EventArgs) Handles tvLevels.SelectedNodeChanged
        Dim node As TreeNode
        Dim selectedNode As TreeNode
        Dim textvalue As String = ""
        Dim index As Integer = 0
        Dim myChars() As Char
        Dim ch As Char

        If (tvLevels.Nodes.Count > 0 And tvLevels.SelectedNode.Text <> "" And tvLevels.SelectedNode.ChildNodes.Count = 0) Then

            Dim strClientTypeID As String = "1"
            If (Session("ClientTypeID") IsNot Nothing) Then
                strClientTypeID = Session("ClientTypeID")
            End If

            Dim dt As New DataTable()
            Dim arrList() As String = tvLevels.SelectedNode.ToolTip.Split(",")
            Dim strNodeNumber As String = arrList(arrList.Length - 1)
            Dim strLevelNo As Integer = Integer.Parse(arrList(0))
            Dim sqlConnection As New SqlConnection()

            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
            sqlConnection.Open()
            Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()

            If (UICulture = "Arabic") Then
                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Numeric(25))No FROM MainHierarchy1 WHERE ParentNo = '" + strNodeNumber + "' AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
            Else
                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Numeric(25))No FROM MainHierarchy1 WHERE ParentNo = '" + strNodeNumber + "' AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
            End If
            Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
            sqlDataAdapter.Fill(dt)

            strLevelNo = strLevelNo + 1
            For i As Integer = 0 To dt.Rows.Count - 1
                If Not (IsDBNull(dt.Rows(i).Item(0).ToString())) And Not (Trim(dt.Rows(i).Item(0).ToString())) = "" Then
                    node = New TreeNode(dt.Rows(i).Item(3).ToString() + " " + dt.Rows(i).Item(1).ToString())
                    node.Target = dt.Rows(i).Item(2).ToString()
                    node.ToolTip = strLevelNo.ToString() + "," + dt.Rows(i).Item(0).ToString() + "," + dt.Rows(i).Item(3).ToString()
                    tvLevels.SelectedNode.ChildNodes.Add(node)
                End If
            Next
            sqlConnection.Close()
            tvLevels.SelectedNode.ExpandAll()

        Else
            If (ddlCrew.Items.Count > 0) Then
                ddlCrew.SelectedIndex = 0
            End If
            chkPercent.Checked = False
            txtCost.Text = ""
            txtMatCost.Text = ""
            txtUnit.Text = ""
            txtDO.Text = ""


        End If

        For Each ctrl In Panel1.Controls
            If TypeOf (ctrl) Is TextBox Then
                CType(ctrl, TextBox).Text = ""
                CType(ctrl, TextBox).ReadOnly = True
            End If
        Next

        node = tvLevels.SelectedNode
        selectedNode = tvLevels.SelectedNode
        While node IsNot Nothing
            myChars = node.Text
            textvalue = ""
            index = 0
            For i As Integer = 0 To myChars.Length - 1
                ch = myChars(i)
                If Char.IsDigit(ch) Or Char.IsWhiteSpace(ch) Then
                    index = index + 1
                Else
                    textvalue = node.Text.Substring(index, node.Text.Length - index)
                    Exit For
                End If
            Next

            If (UICulture = "Arabic") Then
                If (node.ToolTip.Split(",")(0) = "8") Then
                    txtNo8.Text = node.ToolTip.Split(",")(2)
                    txtLevel8A.Text = textvalue
                    txtLevel8E.Text = node.Target
                ElseIf (node.ToolTip.Split(",")(0) = "7") Then
                    txtNo7.Text = node.ToolTip.Split(",")(2)
                    txtLevel7A.Text = textvalue
                    txtLevel7E.Text = node.Target

                    If (selectedNode.ToolTip.Split(",")(0) = "7") Then
                        txtLevel8E.ReadOnly = False
                        txtLevel8A.ReadOnly = False
                        txtNo8.ReadOnly = False
                    End If
                ElseIf (node.ToolTip.Split(",")(0) = "6") Then
                    txtNo6.Text = node.ToolTip.Split(",")(2)
                    txtLevel6A.Text = textvalue
                    txtLevel6E.Text = node.Target

                    If (selectedNode.ToolTip.Split(",")(0) = "6") Then
                        txtLevel7E.ReadOnly = False
                        txtLevel7A.ReadOnly = False
                        txtNo7.ReadOnly = False
                    End If
                ElseIf (node.ToolTip.Split(",")(0) = "5") Then
                    txtNo5.Text = node.ToolTip.Split(",")(2)
                    txtLevel5A.Text = textvalue
                    txtLevel5E.Text = node.Target

                    If (selectedNode.ToolTip.Split(",")(0) = "5") Then
                        txtLevel6E.ReadOnly = False
                        txtLevel6A.ReadOnly = False
                        txtNo6.ReadOnly = False
                    End If
                ElseIf (node.ToolTip.Split(",")(0) = "4") Then
                    txtNo4.Text = node.ToolTip.Split(",")(2)
                    txtLevel4A.Text = textvalue
                    txtLevel4E.Text = node.Target

                    If (selectedNode.ToolTip.Split(",")(0) = "4") Then
                        txtLevel5E.ReadOnly = False
                        txtLevel5A.ReadOnly = False
                        txtNo5.ReadOnly = False
                    End If
                ElseIf (node.ToolTip.Split(",")(0) = "3") Then
                    txtNo3.Text = node.ToolTip.Split(",")(2)
                    txtLevel3A.Text = textvalue
                    txtLevel3E.Text = node.Target

                    If (selectedNode.ToolTip.Split(",")(0) = "3") Then
                        txtLevel4E.ReadOnly = False
                        txtLevel4A.ReadOnly = False
                        txtNo4.ReadOnly = False
                    End If
                ElseIf (node.ToolTip.Split(",")(0) = "2") Then
                    txtNo2.Text = node.ToolTip.Split(",")(2)
                    txtLevel2A.Text = textvalue
                    txtLevel2E.Text = node.Target

                    If (selectedNode.ToolTip.Split(",")(0) = "2") Then
                        txtLevel3E.ReadOnly = False
                        txtLevel3A.ReadOnly = False
                        txtNo3.ReadOnly = False
                    End If
                ElseIf (node.ToolTip.Split(",")(0) = "1") Then
                    txtNo1.Text = node.ToolTip.Split(",")(2)
                    txtLevel1A.Text = textvalue
                    txtLevel1E.Text = node.Target

                    If (selectedNode.ToolTip.Split(",")(0) = "1") Then
                        txtLevel2E.ReadOnly = False
                        txtLevel2A.ReadOnly = False
                        txtNo2.ReadOnly = False
                    End If
                End If
            Else
                If (node.ToolTip.Split(",")(0) = "8") Then
                    txtNo8.Text = node.ToolTip.Split(",")(2)
                    txtLevel8E.Text = textvalue
                    txtLevel8A.Text = node.Target
                ElseIf (node.ToolTip.Split(",")(0) = "7") Then
                    txtNo7.Text = node.ToolTip.Split(",")(2)
                    txtLevel7E.Text = textvalue
                    txtLevel7A.Text = node.Target

                    If (selectedNode.ToolTip.Split(",")(0) = "7") Then
                        txtLevel8E.ReadOnly = False
                        txtLevel8A.ReadOnly = False
                        txtNo8.ReadOnly = False
                    End If
                ElseIf (node.ToolTip.Split(",")(0) = "6") Then
                    txtNo6.Text = node.ToolTip.Split(",")(2)
                    txtLevel6E.Text = textvalue
                    txtLevel6A.Text = node.Target

                    If (selectedNode.ToolTip.Split(",")(0) = "6") Then
                        txtLevel7E.ReadOnly = False
                        txtLevel7A.ReadOnly = False
                        txtNo7.ReadOnly = False
                    End If
                ElseIf (node.ToolTip.Split(",")(0) = "5") Then
                    txtNo5.Text = node.ToolTip.Split(",")(2)
                    txtLevel5E.Text = textvalue
                    txtLevel5A.Text = node.Target

                    If (selectedNode.ToolTip.Split(",")(0) = "5") Then
                        txtLevel6E.ReadOnly = False
                        txtLevel6A.ReadOnly = False
                        txtNo6.ReadOnly = False
                    End If
                ElseIf (node.ToolTip.Split(",")(0) = "4") Then
                    txtNo4.Text = node.ToolTip.Split(",")(2)
                    txtLevel4E.Text = textvalue
                    txtLevel4A.Text = node.Target

                    If (selectedNode.ToolTip.Split(",")(0) = "4") Then
                        txtLevel5E.ReadOnly = False
                        txtLevel5A.ReadOnly = False
                        txtNo5.ReadOnly = False
                    End If
                ElseIf (node.ToolTip.Split(",")(0) = "3") Then
                    txtNo3.Text = node.ToolTip.Split(",")(2)
                    txtLevel3E.Text = textvalue
                    txtLevel3A.Text = node.Target

                    If (selectedNode.ToolTip.Split(",")(0) = "3") Then
                        txtLevel4E.ReadOnly = False
                        txtLevel4A.ReadOnly = False
                        txtNo4.ReadOnly = False
                    End If
                ElseIf (node.ToolTip.Split(",")(0) = "2") Then
                    txtNo2.Text = node.ToolTip.Split(",")(2)
                    txtLevel2E.Text = textvalue
                    txtLevel2A.Text = node.Target

                    If (selectedNode.ToolTip.Split(",")(0) = "2") Then
                        txtLevel3E.ReadOnly = False
                        txtLevel3A.ReadOnly = False
                        txtNo3.ReadOnly = False
                    End If
                ElseIf (node.ToolTip.Split(",")(0) = "1") Then
                    txtNo1.Text = node.ToolTip.Split(",")(2)
                    txtLevel1E.Text = textvalue
                    txtLevel1A.Text = node.Target

                    If (selectedNode.ToolTip.Split(",")(0) = "1") Then
                        txtLevel2E.ReadOnly = False
                        txtLevel2A.ReadOnly = False
                        txtNo2.ReadOnly = False
                    End If
                End If
            End If
            node = node.Parent
        End While

        If (tvLevels.SelectedNode.ChildNodes.Count = 0) Then
            Dim objSqlConfig As New ConfigClassSqlServer()
            Dim strNodeNo As String = tvLevels.SelectedNode.ToolTip.Split(",")(2).ToString()
            Dim dtData As DataTable = objSqlConfig.GetCostLineItemData(strNodeNo)
            If (dtData.Rows.Count > 0) Then
                If (ddlCrew.Items.FindByValue(dtData.Rows(0).Item(0).ToString()) IsNot Nothing) Then
                    ddlCrew.SelectedValue = dtData.Rows(0).Item(0).ToString()
                Else
                    ddlCrew.SelectedIndex = 0
                End If

                chkPercent.Checked = dtData.Rows(0).Item(1).ToString()
                txtCost.Text = dtData.Rows(0).Item(2).ToString()
                txtMatCost.Text = dtData.Rows(0).Item(3).ToString()
                txtUnit.Text = dtData.Rows(0).Item(4).ToString()
                txtDO.Text = dtData.Rows(0).Item(5).ToString()
            Else
                If (ddlCrew.Items.Count > 0) Then
                    ddlCrew.SelectedIndex = 0
                End If
                chkPercent.Checked = False
                txtCost.Text = ""
                txtMatCost.Text = ""
                txtUnit.Text = ""
                txtDO.Text = ""
            End If
        Else
            If (ddlCrew.Items.Count > 0) Then
                ddlCrew.SelectedIndex = 0
            End If
            chkPercent.Checked = False
            txtCost.Text = ""
            txtMatCost.Text = ""
            txtUnit.Text = ""
            txtDO.Text = ""
        End If

        If (ddlCrew.SelectedIndex > 0) Then
            txtCost.ReadOnly = True
            txtCost.Text = "0"
        Else
            txtCost.ReadOnly = False
        End If
        tvLevels.SelectedNode.ExpandAll()
    End Sub
    Protected Sub Button0_Click(sender As Object, e As EventArgs) Handles Button0.Click
        ViewState("EditNodeLevel") = "1"
        UpdateSingleLevel(sender)
    End Sub
    Private Sub UpdateSingleLevel(button As Button)
        If (tvLevels.SelectedNode IsNot Nothing) Then
            Dim clickedButton As Button = CType(button, Button)
            Dim node As TreeNode

            node = tvLevels.SelectedNode
            If (clickedButton.Text = "E") Then

                For Each ctrl In Panel1.Controls
                    If TypeOf (ctrl) Is TextBox Then
                        CType(ctrl, TextBox).ReadOnly = True
                    End If
                Next

                Button0.Enabled = False
                Button1.Enabled = False
                Button2.Enabled = False
                Button3.Enabled = False
                Button4.Enabled = False
                Button5.Enabled = False
                Button6.Enabled = False
                Button7.Enabled = False

                clickedButton.Text = "U"
                clickedButton.Enabled = True

                If (ViewState("EditNodeLevel") = "1") Then
                    txtNo1.ReadOnly = False
                    txtLevel1E.ReadOnly = False
                    txtLevel1A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo1.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "2") Then
                    txtNo2.ReadOnly = False
                    txtLevel2E.ReadOnly = False
                    txtLevel2A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo2.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "3") Then
                    txtNo3.ReadOnly = False
                    txtLevel3E.ReadOnly = False
                    txtLevel3A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo3.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "4") Then
                    txtNo4.ReadOnly = False
                    txtLevel4E.ReadOnly = False
                    txtLevel4A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo4.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "5") Then
                    txtNo5.ReadOnly = False
                    txtLevel5E.ReadOnly = False
                    txtLevel5A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo5.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "6") Then
                    txtNo6.ReadOnly = False
                    txtLevel6E.ReadOnly = False
                    txtLevel6A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo6.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "7") Then
                    txtNo7.ReadOnly = False
                    txtLevel7E.ReadOnly = False
                    txtLevel7A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo7.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "8") Then
                    txtNo8.ReadOnly = False
                    txtLevel8E.ReadOnly = False
                    txtLevel8A.ReadOnly = False
                    ViewState("PrevNumber") = txtNo8.Text.Trim()
                End If
            ElseIf (clickedButton.Text = "U" And ViewState("PrevNumber").ToString().Length > 0) Then
                Dim nodeID As String = ""
                Dim nodeNo As String = ""
                Dim nodeNameE As String = ""
                Dim nodeNameA As String = ""

                While node IsNot Nothing
                    If (node.ToolTip.Split(",")(0) = ViewState("EditNodeLevel")) Then
                        nodeID = node.ToolTip.Split(",")(1)
                        Exit While
                    End If
                    node = node.Parent
                End While

                If (ViewState("EditNodeLevel") = "1") Then
                    nodeNo = txtNo1.Text.Trim()
                    nodeNameE = txtLevel1E.Text.Trim()
                    nodeNameA = txtLevel1A.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "2") Then
                    nodeNo = txtNo2.Text.Trim()
                    nodeNameE = txtLevel2E.Text.Trim()
                    nodeNameA = txtLevel2A.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "3") Then
                    nodeNo = txtNo3.Text.Trim()
                    nodeNameE = txtLevel3E.Text.Trim()
                    nodeNameA = txtLevel3A.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "4") Then
                    nodeNo = txtNo4.Text.Trim()
                    nodeNameE = txtLevel4E.Text.Trim()
                    nodeNameA = txtLevel4A.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "5") Then
                    nodeNo = txtNo5.Text.Trim()
                    nodeNameE = txtLevel5E.Text.Trim()
                    nodeNameA = txtLevel5A.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "6") Then
                    nodeNo = txtNo6.Text.Trim()
                    nodeNameE = txtLevel6E.Text.Trim()
                    nodeNameA = txtLevel6A.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "7") Then
                    nodeNo = txtNo7.Text.Trim()
                    nodeNameE = txtLevel7E.Text.Trim()
                    nodeNameA = txtLevel7A.Text.Trim()
                ElseIf (ViewState("EditNodeLevel") = "8") Then
                    nodeNo = txtNo8.Text.Trim()
                    nodeNameE = txtLevel8E.Text.Trim()
                    nodeNameA = txtLevel8A.Text.Trim()
                End If

                If (nodeNo.Length = 0) Then
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('17');", True)
                ElseIf (IsNumeric(nodeNo) = False) Then
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('26');", True)
                ElseIf (nodeNameE.Length = 0) Then
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('18');", True)
                ElseIf (Char.IsDigit(nodeNameE.Substring(0, 1)) Or Char.IsWhiteSpace(nodeNameE.Substring(0, 1))) Then
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('27');", True)
                ElseIf (nodeNameA.Length = 0) Then
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('19');", True)
                ElseIf (Char.IsDigit(nodeNameA.Substring(0, 1)) Or Char.IsWhiteSpace(nodeNameA.Substring(0, 1))) Then
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('27');", True)
                Else
                    Dim sqlConnection As New SqlConnection()
                    Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
                    sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                    sqlConnection.Open()

                    sqlCommand.CommandText = "SELECT COUNT(*) FROM MainHierarchy1 WHERE No = '" + nodeNo + "' AND ID <> '" + nodeID + "' AND CompID = '" + Session("CompanyID") + "' "
                    If (sqlCommand.ExecuteScalar().ToString() = "0") Then

                        sqlCommand.CommandText = "UPDATE MainHierarchy1 SET No = '" + nodeNo + "', NameE = N'" + nodeNameE + "', NameA = N'" + nodeNameA + "' WHERE ID = '" + nodeID + "' AND CompID = '" + Session("CompanyID") + "' "
                        sqlCommand.ExecuteNonQuery()

                        sqlCommand.CommandText = "UPDATE MainHierarchy1 SET ParentNo = '" + nodeNo + "' WHERE ParentNo = '" + ViewState("PrevNumber") + "' AND CompID = '" + Session("CompanyID") + "' "
                        sqlCommand.ExecuteNonQuery()

                        node.ToolTip = node.ToolTip + "," + nodeNo
                        If (UICulture = "Arabic") Then
                            node.Text = nodeNo + " " + nodeNameA
                            node.Target = nodeNameE
                        Else
                            node.Text = nodeNo + " " + nodeNameE
                            node.Target = nodeNameA
                        End If


                        clickedButton.Text = "E"
                        Button0.Enabled = True
                        Button1.Enabled = True
                        Button2.Enabled = True
                        Button3.Enabled = True
                        Button4.Enabled = True
                        Button5.Enabled = True
                        Button6.Enabled = True
                        Button7.Enabled = True

                    Else
                        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('16');", True)
                    End If
                End If
            Else
                clickedButton.Text = "E"
                Button0.Enabled = True
                Button1.Enabled = True
                Button2.Enabled = True
                Button3.Enabled = True
                Button4.Enabled = True
                Button5.Enabled = True
                Button6.Enabled = True
                Button7.Enabled = True
            End If
        End If




        'Dim collection As New Collection
        'Dim textboxcollection As New Collection
        'Dim buttoncollection As New Collection
        'Dim sqlConnection As New SqlConnection()
        'Dim button As Button
        'Dim txtBox, txtBox1, txtBox2 As TextBox
        'Dim localIndex As Integer
        'Dim strSelectedPath As String = ""
        'Dim node As TreeNode

        'If (tvLevels.SelectedNode IsNot Nothing) Then

        '    For Each ctrl In Panel1.Controls
        '        If TypeOf (ctrl) Is TextBox Then
        '            textboxcollection.Add(ctrl)
        '        End If
        '    Next

        '    txtBox = CType(textboxcollection(index), TextBox)
        '    txtBox.ReadOnly = False
        '    txtBox1 = CType(textboxcollection(index + 1), TextBox)
        '    txtBox1.ReadOnly = False
        '    txtBox2 = CType(textboxcollection(index + 2), TextBox)
        '    txtBox2.ReadOnly = False

        '    If (txtBox.Text.Length > 0) Then
        '        For Each ctrl In Panel1.Controls
        '            If TypeOf (ctrl) Is Button Then
        '                CType(ctrl, Button).Enabled = False
        '                buttoncollection.Add(ctrl)
        '                If (buttoncollection.Count = 8) Then
        '                    Exit For
        '                End If
        '            End If
        '        Next

        '        If (index = 1) Then
        '            button = CType(buttoncollection(1), Button)
        '            localIndex = 1
        '        ElseIf (index = 4) Then
        '            button = CType(buttoncollection(2), Button)
        '            localIndex = 2
        '        ElseIf (index = 7) Then
        '            button = CType(buttoncollection(3), Button)
        '            localIndex = 3
        '        ElseIf (index = 10) Then
        '            button = CType(buttoncollection(4), Button)
        '            localIndex = 4
        '        ElseIf (index = 13) Then
        '            button = CType(buttoncollection(5), Button)
        '            localIndex = 5
        '        ElseIf (index = 16) Then
        '            button = CType(buttoncollection(6), Button)
        '            localIndex = 6
        '        ElseIf (index = 19) Then
        '            button = CType(buttoncollection(7), Button)
        '            localIndex = 7
        '        ElseIf (index = 22) Then
        '            button = CType(buttoncollection(8), Button)
        '            localIndex = 8
        '        End If

        '        If (button.Text.Equals("E")) Then
        '            button.Enabled = True
        '            txtBox.Focus()
        '            button.Text = "U"

        '        ElseIf (button.Text.Equals("U")) Then

        '            Dim valuePath As String = tvLevels.SelectedNode.ValuePath
        '            node = tvLevels.SelectedNode

        '            'While node IsNot Nothing
        '            '    collection.Add(node.Text)
        '            '    node = node.Parent
        '            'End While

        '            While node IsNot Nothing
        '                Dim textvalue As String = ""
        '                Dim numericvalue As String = ""
        '                Dim iIndex As Integer = 0
        '                textvalue = node.Text
        '                If (node.ImageUrl = "0") Then
        '                    textvalue = ""
        '                    Dim myChars() As Char = node.Text
        '                    Dim ch As Char
        '                    For i As Integer = 0 To myChars.Length - 1
        '                        ch = myChars(i)
        '                        If Char.IsDigit(ch) Or ch = " " Then
        '                            iIndex = iIndex + 1
        '                        Else
        '                            textvalue = node.Text.Substring(iIndex, node.Text.Length - iIndex)
        '                            numericvalue = node.Text.Substring(0, iIndex - 1)
        '                            Exit For
        '                        End If
        '                    Next
        '                End If

        '                collection.Add(textvalue)
        '                collection.Add(numericvalue)
        '                node = node.Parent
        '            End While


        '            Dim levelIndex As Integer = 1
        '            For i = collection.Count To 1 Step -2
        '                If (UICulture = "Arabic") Then
        '                    strSelectedPath = strSelectedPath & "Level" & levelIndex & "A" & "='" & Trim(collection.Item(i - 1)) & "' and "
        '                Else
        '                    strSelectedPath = strSelectedPath & "Level" & levelIndex & "='" & Trim(collection.Item(i - 1)) & "' and "
        '                End If

        '                If (tvLevels.SelectedNode.ImageUrl = "0") Then
        '                    strSelectedPath = strSelectedPath & "No" & levelIndex & "='" & Trim(collection.Item(i)) & "' and "
        '                End If
        '                levelIndex = levelIndex + 1
        '            Next i



        '            If Trim(strSelectedPath) <> "" Then
        '                strSelectedPath = Mid$(strSelectedPath, 1, Len(strSelectedPath) - 4)
        '                Dim strNewValue As String
        '                If (tvLevels.SelectedNode.ImageUrl = "1") Then
        '                    strNewValue = "Level" & localIndex & "=" & "'" & Trim(CType(textboxcollection(index), TextBox).Text) & " " & Trim(CType(textboxcollection(index + 1), TextBox).Text) & "'" & "," & "Level" & localIndex & "A" & "= N" & "'" & Trim(CType(textboxcollection(index + 2), TextBox).Text) & "'"
        '                Else
        '                    strNewValue = "No" & localIndex & "=" & "'" & Trim(CType(textboxcollection(index), TextBox).Text) & "'" & ", Level" & localIndex & "=" & "'" & Trim(CType(textboxcollection(index + 1), TextBox).Text) & "'" & "," & "Level" & localIndex & "A" & "= N" & "'" & Trim(CType(textboxcollection(index + 2), TextBox).Text) & "'"
        '                End If

        '                Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        '                sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        '                sqlConnection.Open()

        '                sqlCommand.CommandText = "Update MainHierarchy set " & strNewValue & " where " & strSelectedPath & " "
        '                sqlCommand.ExecuteNonQuery()

        '                button.Text = "E"

        '                Button0.Enabled = True
        '                Button1.Enabled = True
        '                Button2.Enabled = True
        '                Button3.Enabled = True
        '                Button4.Enabled = True
        '                Button5.Enabled = True
        '                Button6.Enabled = True
        '                Button7.Enabled = True

        '                'LoadTreeViewData()
        '                Dim arrValuePath() As String = valuePath.Split("/")
        '                'If (UICulture = "Arabic") Then
        '                '    arrValuePath(localIndex - 1) = Trim(CType(textboxcollection(index + 1), TextBox).Text)
        '                'Else
        '                '    arrValuePath(localIndex - 1) = Trim(CType(textboxcollection(index), TextBox).Text)
        '                'End If
        '                'valuePath = ""
        '                Dim strLastUpdateNodePath As String = ""
        '                For i As Integer = 0 To localIndex - 1
        '                    strLastUpdateNodePath = strLastUpdateNodePath + arrValuePath(i) + "/"
        '                Next
        '                strLastUpdateNodePath = strLastUpdateNodePath.Substring(0, strLastUpdateNodePath.Length - 1)

        '                node = tvLevels.FindNode(strLastUpdateNodePath)

        '                If (UICulture = "Arabic") Then
        '                    node.Text = txtBox.Text + " " + txtBox2.Text
        '                    node.Target = txtBox1.Text
        '                Else
        '                    node.Text = txtBox.Text + " " + txtBox1.Text
        '                    node.Target = txtBox2.Text
        '                End If

        '                Dim nodeIndex As Integer = 0
        '                'Dim prevNode As New TreeNode
        '                'prevNode = node
        '                For Each n As TreeNode In tvLevels.Nodes
        '                    If (n.Text < node.Text) Then
        '                        nodeIndex = nodeIndex + 1
        '                    End If
        '                Next
        '                tvLevels.Nodes.AddAt(nodeIndex, node)
        '                'tvLevels.Nodes.Remove(prevNode)
        '                node.Select()

        '                'node.Select()
        '                'While (node.Parent IsNot Nothing)
        '                '    node = node.Parent
        '                'End While
        '                'node.ExpandAll()

        '                CType(textboxcollection(index), TextBox).ReadOnly = True
        '                CType(textboxcollection(index + 1), TextBox).ReadOnly = True
        '                CType(textboxcollection(index + 2), TextBox).ReadOnly = True
        '            End If
        '        End If
        '    End If
        'Else
        '    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
        'End If
        'sqlConnection.Close()
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ViewState("EditNodeLevel") = "2"
        UpdateSingleLevel(sender)
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        ViewState("EditNodeLevel") = "3"
        UpdateSingleLevel(sender)
    End Sub
    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        ViewState("EditNodeLevel") = "4"
        UpdateSingleLevel(sender)
    End Sub
    Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        ViewState("EditNodeLevel") = "5"
        UpdateSingleLevel(sender)
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        ViewState("EditNodeLevel") = "6"
        UpdateSingleLevel(sender)
    End Sub
    Protected Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        ViewState("EditNodeLevel") = "7"
        UpdateSingleLevel(sender)
    End Sub
    Protected Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        ViewState("EditNodeLevel") = "8"
        UpdateSingleLevel(sender)
    End Sub
    Protected Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        Dim sqlConnection As New SqlConnection()
        Dim i As Integer = 0
        Dim valuePath As String = ""
        Dim currentNode As TreeNode

        If (tvLevels.SelectedNode IsNot Nothing) Then
            If (tvLevels.SelectedNode.ChildNodes.Count = 0) Then

                Dim strNo As String = ""
                'Dim dt1, dt2, dt3, dt4, dt5, dt6, dt7, dt8 As New DataTable()
                Dim dt As New DataTable()
                Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
                'Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter()

                sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                sqlConnection.Open()

                strNo = tvLevels.SelectedNode.ToolTip.Split(",")(1)
                Dim strNodenumber As String = tvLevels.SelectedNode.ToolTip.Split(",")(2)

                'strNo = tvLevels.SelectedNode.ToolTip.Split(",")(2)
                'sqlCommand.CommandText = "SELECT No FROM Mainhierarchy1 WHERE No = '" + strNo + "' "
                'sqlDataAdapter.SelectCommand = sqlCommand
                'sqlDataAdapter.Fill(dt1)

                'For t As Integer = 0 To dt1.Rows.Count - 1
                '    strNo = dt1.Rows(t).Item(0).ToString()
                '    sqlCommand.CommandText = "SELECT No FROM Mainhierarchy1 WHERE ParentNo = '" + strNo + "' "
                '    sqlDataAdapter.SelectCommand = sqlCommand
                '    sqlDataAdapter.Fill(dt2)

                '    For u As Integer = 0 To dt2.Rows.Count - 1
                '        strNo = dt2.Rows(t).Item(0).ToString()
                '        sqlCommand.CommandText = "SELECT No FROM Mainhierarchy1 WHERE ParentNo = '" + strNo + "' "
                '        sqlDataAdapter.SelectCommand = sqlCommand
                '        sqlDataAdapter.Fill(dt3)

                '        For v As Integer = 0 To dt3.Rows.Count - 1
                '            strNo = dt3.Rows(t).Item(0).ToString()
                '            sqlCommand.CommandText = "SELECT No FROM Mainhierarchy1 WHERE ParentNo = '" + strNo + "' "
                '            sqlDataAdapter.SelectCommand = sqlCommand
                '            sqlDataAdapter.Fill(dt4)

                '            For w As Integer = 0 To dt4.Rows.Count - 1
                '                strNo = dt4.Rows(t).Item(0).ToString()
                '                sqlCommand.CommandText = "SELECT No FROM Mainhierarchy1 WHERE ParentNo = '" + strNo + "' "
                '                sqlDataAdapter.SelectCommand = sqlCommand
                '                sqlDataAdapter.Fill(dt5)

                '                For x As Integer = 0 To dt5.Rows.Count - 1
                '                    strNo = dt5.Rows(t).Item(0).ToString()
                '                    sqlCommand.CommandText = "SELECT No FROM Mainhierarchy1 WHERE ParentNo = '" + strNo + "' "
                '                    sqlDataAdapter.SelectCommand = sqlCommand
                '                    sqlDataAdapter.Fill(dt6)

                '                    For y As Integer = 0 To dt6.Rows.Count - 1
                '                        strNo = dt6.Rows(t).Item(0).ToString()
                '                        sqlCommand.CommandText = "SELECT No FROM Mainhierarchy1 WHERE ParentNo = '" + strNo + "' "
                '                        sqlDataAdapter.SelectCommand = sqlCommand
                '                        sqlDataAdapter.Fill(dt7)

                '                        For z As Integer = 0 To dt7.Rows.Count - 1
                '                            strNo = dt7.Rows(t).Item(0).ToString()
                '                            sqlCommand.CommandText = "SELECT No FROM Mainhierarchy1 WHERE ParentNo = '" + strNo + "' "
                '                            sqlDataAdapter.SelectCommand = sqlCommand
                '                            sqlDataAdapter.Fill(dt8)

                '                            For a As Integer = 0 To dt8.Rows.Count - 1
                '                                sqlCommand.CommandText = "DELETE FROM Mainhierarchy1 WHERE No = '" + dt8.Rows(a).Item(0).ToString() + "' "
                '                                sqlCommand.ExecuteNonQuery()
                '                            Next

                '                            sqlCommand.CommandText = "DELETE FROM Mainhierarchy1 WHERE No = '" + dt7.Rows(z).Item(0).ToString() + "' "
                '                            sqlCommand.ExecuteNonQuery()
                '                        Next

                '                        sqlCommand.CommandText = "DELETE FROM Mainhierarchy1 WHERE No = '" + dt6.Rows(y).Item(0).ToString() + "' "
                '                        sqlCommand.ExecuteNonQuery()
                '                    Next

                '                    sqlCommand.CommandText = "DELETE FROM Mainhierarchy1 WHERE No = '" + dt5.Rows(x).Item(0).ToString() + "' "
                '                    sqlCommand.ExecuteNonQuery()
                '                Next

                '                sqlCommand.CommandText = "DELETE FROM Mainhierarchy1 WHERE No = '" + dt4.Rows(w).Item(0).ToString() + "' "
                '                sqlCommand.ExecuteNonQuery()
                '            Next

                '            sqlCommand.CommandText = "DELETE FROM Mainhierarchy1 WHERE No = '" + dt3.Rows(v).Item(0).ToString() + "' "
                '            sqlCommand.ExecuteNonQuery()
                '        Next

                '        sqlCommand.CommandText = "DELETE FROM Mainhierarchy1 WHERE No = '" + dt2.Rows(u).Item(0).ToString() + "' "
                '        sqlCommand.ExecuteNonQuery()
                '    Next

                '    sqlCommand.CommandText = "DELETE FROM Mainhierarchy1 WHERE No = '" + dt1.Rows(t).Item(0).ToString() + "' "
                '    sqlCommand.ExecuteNonQuery()
                'Next

                sqlCommand.CommandText = "DELETE FROM Mainhierarchy1 WHERE ID = '" + strNo + "' AND CompID = '" + Session("CompanyID") + "' "
                sqlCommand.ExecuteNonQuery()

                dt.Clear()

                sqlCommand.CommandText = "SELECT DISTINCT MapID FROM ProductDetails WHERE MapID =  '" + strNodenumber + "' AND CompID = '" + Session("CompanyID") + "' "
                Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                sqlDataAdapter.Fill(dt)

                If (dt.Rows.Count > 0) Then
                    If (hdnResult.Value.Equals("1")) Then
                        sqlCommand.CommandText = "DELETE FROM ProductDetails WHERE MapID = '" + strNodenumber + "' AND CompID = '" + Session("CompanyID") + "' "
                        sqlCommand.ExecuteNonQuery()
                    End If
                End If

                hdnResult.Value = "-1"

                If (tvLevels.SelectedNode.Parent IsNot Nothing) Then
                    tvLevels.SelectedNode.Parent.ChildNodes.Remove(tvLevels.SelectedNode)
                Else
                    tvLevels.Nodes.Remove(tvLevels.SelectedNode)
                End If

                If (valuePath.Length > 0) Then
                    currentNode = tvLevels.FindNode(valuePath)
                    currentNode.Select()
                    While (currentNode.Parent IsNot Nothing)
                        currentNode = currentNode.Parent
                    End While
                    currentNode.ExpandAll()
                    tvLevels_SelectedNodeChanged(sender, Nothing)
                End If

                For Each ctrl In Panel1.Controls
                    If TypeOf (ctrl) Is TextBox Then
                        CType(ctrl, TextBox).Text = ""
                    End If
                Next

                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
        End If

        sqlConnection.Close()
    End Sub
    Protected Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Dim strClientTypeID As String = "1"
        If (Session("ClientTypeID") IsNot Nothing) Then
            strClientTypeID = Session("ClientTypeID")
        End If

        If (tvLevels.SelectedNode IsNot Nothing) Then
            Dim strNodeLevel As String = tvLevels.SelectedNode.ToolTip.Split(",")(0)
            Dim strSelectedNodeID As String = tvLevels.SelectedNode.ToolTip.Split(",")(1)
            Dim strSelectedNodeNumber As String = tvLevels.SelectedNode.ToolTip.Split(",")(2)
            Dim strNewLevel As String = ""
            Dim nodeNo As String = ""
            Dim nodeNameE As String = ""
            Dim nodeNameA As String = ""

            If (strNodeLevel = "1") Then
                nodeNo = txtNo2.Text.Trim()
                nodeNameE = txtLevel2E.Text.Trim()
                nodeNameA = txtLevel2A.Text.Trim()
                strNewLevel = "2"
            ElseIf (strNodeLevel = "2") Then
                nodeNo = txtNo3.Text.Trim()
                nodeNameE = txtLevel3E.Text.Trim()
                nodeNameA = txtLevel3A.Text.Trim()
                strNewLevel = "3"
            ElseIf (strNodeLevel = "3") Then
                nodeNo = txtNo4.Text.Trim()
                nodeNameE = txtLevel4E.Text.Trim()
                nodeNameA = txtLevel4A.Text.Trim()
                strNewLevel = "4"
            ElseIf (strNodeLevel = "4") Then
                nodeNo = txtNo5.Text.Trim()
                nodeNameE = txtLevel5E.Text.Trim()
                nodeNameA = txtLevel5A.Text.Trim()
                strNewLevel = "5"
            ElseIf (strNodeLevel = "5") Then
                nodeNo = txtNo6.Text.Trim()
                nodeNameE = txtLevel6E.Text.Trim()
                nodeNameA = txtLevel6A.Text.Trim()
                strNewLevel = "6"
            ElseIf (strNodeLevel = "6") Then
                nodeNo = txtNo7.Text.Trim()
                nodeNameE = txtLevel7E.Text.Trim()
                nodeNameA = txtLevel7A.Text.Trim()
                strNewLevel = "7"
            ElseIf (strNodeLevel = "7") Then
                nodeNo = txtNo8.Text.Trim()
                nodeNameE = txtLevel8E.Text.Trim()
                nodeNameA = txtLevel8A.Text.Trim()
                strNewLevel = "8"
            End If

            If (nodeNo.Length = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('17');", True)
            ElseIf (IsNumeric(nodeNo) = False) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('26');", True)
            ElseIf (nodeNameE.Length = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('18');", True)
            ElseIf (Char.IsDigit(nodeNameE.Substring(0, 1)) Or Char.IsWhiteSpace(nodeNameE.Substring(0, 1))) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('27');", True)
            ElseIf (nodeNameA.Length = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('19');", True)
            ElseIf (Char.IsDigit(nodeNameA.Substring(0, 1)) Or Char.IsWhiteSpace(nodeNameA.Substring(0, 1))) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('27');", True)
            Else
                Dim dt As New DataTable()
                Dim sqlConnection As New SqlConnection()
                Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
                sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                sqlConnection.Open()

                sqlCommand.CommandText = "SELECT COUNT(*) FROM MainHierarchy1 WHERE No = '" + nodeNo + "' AND CompID = '" + Session("CompanyID") + "' "
                If (sqlCommand.ExecuteScalar().ToString() = "0") Then

                    sqlCommand.CommandText = "SELECT COUNT(*) FROM ProductDetails WHERE MapID = '" + strSelectedNodeNumber + "' AND CompID = '" + Session("CompanyID") + "' "
                    If (sqlCommand.ExecuteScalar().ToString() = "0") Then
                        sqlCommand.CommandText = "INSERT INTO MainHierarchy1(NameE,NameA,Status,No,ParentNo, ClientTypeID, CompID) VALUES(N'" + nodeNameE + "', N'" + nodeNameA + "', 1,  '" + nodeNo + "', '" + strSelectedNodeNumber + "', '" + strClientTypeID + "', '" + Session("CompanyID") + "' )"
                        sqlCommand.ExecuteNonQuery()
                        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('7');", True)

                        If (UICulture = "Arabic") Then
                            sqlCommand.CommandText = "SELECT ID, NameA, NameE, CAST(No As Float)No FROM MainHierarchy1 WHERE ID = (SELECT MAX(ID) FROM MainHierarchy1 WHERE CompID = '" + Session("CompanyID") + "')  "
                        Else
                            sqlCommand.CommandText = "SELECT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ID = (SELECT MAX(ID) FROM MainHierarchy1 WHERE CompID = '" + Session("CompanyID") + "') "
                        End If

                        Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                        sqlDataAdapter.Fill(dt)

                        If (dt.Rows.Count > 0) Then
                            Dim node As TreeNode
                            node = New TreeNode(dt.Rows(0).Item(3).ToString() + " " + dt.Rows(0).Item(1).ToString())
                            node.Target = dt.Rows(0).Item(2).ToString()
                            'node.ImageUrl = dt.Rows(0).Item(3).ToString()
                            node.ToolTip = strNewLevel + "," + dt.Rows(0).Item(0).ToString() + "," + dt.Rows(0).Item(3).ToString()
                            Dim index As Integer = 0
                            For Each n As TreeNode In tvLevels.SelectedNode.ChildNodes
                                If (n.Text < node.Text) Then
                                    index = index + 1
                                End If
                            Next
                            tvLevels.SelectedNode.ChildNodes.AddAt(index, node)
                            node.Select()
                            If (node.Parent IsNot Nothing) Then
                                node.Parent.Expand()
                            End If
                        End If
                    Else
                        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('6');", True)
                    End If
                Else
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('16');", True)
                End If

                sqlConnection.Close()
            End If

        Else
            Dim nodeNo As String = ""
            Dim nodeNameE As String = ""
            Dim nodeNameA As String = ""

            nodeNo = txtNo1.Text.Trim()
            nodeNameE = txtLevel1E.Text.Trim()
            nodeNameA = txtLevel1A.Text.Trim()

            If (nodeNo.Length = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('17');", True)
            ElseIf (IsNumeric(nodeNo) = False) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('26');", True)
            ElseIf (nodeNameE.Length = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('18');", True)
            ElseIf (Char.IsDigit(nodeNameE.Substring(0, 1)) Or Char.IsWhiteSpace(nodeNameE.Substring(0, 1))) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('27');", True)
            ElseIf (nodeNameA.Length = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('19');", True)
            ElseIf (Char.IsDigit(nodeNameA.Substring(0, 1)) Or Char.IsWhiteSpace(nodeNameA.Substring(0, 1))) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('27');", True)
            Else
                Dim dt As New DataTable()
                Dim sqlConnection As New SqlConnection()
                Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
                sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                sqlConnection.Open()

                sqlCommand.CommandText = "SELECT COUNT(*) FROM MainHierarchy1 WHERE No = '" + nodeNo + "' "
                If (sqlCommand.ExecuteScalar().ToString() = "0") Then

                    sqlCommand.CommandText = "INSERT INTO MainHierarchy1(NameE,NameA,Status,No, ClientTypeID) VALUES(N'" + nodeNameE + "', N'" + nodeNameA + "', 1,  '" + nodeNo + "', '" + strClientTypeID + "' )"
                    sqlCommand.ExecuteNonQuery()
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('7');", True)

                    sqlCommand.CommandText = "SELECT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE ID = (SELECT MAX(ID) FROM MainHierarchy1) "
                    Dim sqlDataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)
                    sqlDataAdapter.Fill(dt)

                    If (dt.Rows.Count > 0) Then
                        Dim node As TreeNode
                        node = New TreeNode(dt.Rows(0).Item(3).ToString() + " " + dt.Rows(0).Item(1).ToString())
                        node.Target = dt.Rows(0).Item(2).ToString()
                        'node.ImageUrl = dt.Rows(0).Item(3).ToString()
                        node.ToolTip = "1" + "," + dt.Rows(0).Item(0).ToString() + "," + dt.Rows(0).Item(3).ToString()
                        Dim index As Integer = 0
                        For Each n As TreeNode In tvLevels.Nodes
                            If (n.Text < node.Text) Then
                                index = index + 1
                            End If
                        Next
                        tvLevels.Nodes.AddAt(index, node)
                        node.Select()

                    End If
                Else
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('16');", True)
                End If

                sqlConnection.Close()
            End If

        End If
    End Sub
    Protected Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click
        Dim currentnode As TreeNode
        Dim strNodeID As String = ""
        Dim Path As String = ""

        If (tvLevels.SelectedNode IsNot Nothing) Then
            If tvLevels.SelectedNode.ChildNodes.Count = 0 Then
                currentnode = tvLevels.SelectedNode

                Session("LastLevel") = currentnode.ToolTip.Split(",")(0)
                strNodeID = currentnode.ToolTip.Split(",")(2)
                Path = currentnode.ValuePath

                Session("MapID") = strNodeID
                Session("Path") = Path
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "Showopoup('AddProducts.aspx');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('8');", True)
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
        End If
    End Sub
    Protected Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Dim currentnode As TreeNode
        Dim sqlConnection As New SqlConnection()
        Dim strNodeID As String = ""

        If (tvLevels.SelectedNode IsNot Nothing) Then
            If tvLevels.SelectedNode.ChildNodes.Count = 0 Then
                currentnode = tvLevels.SelectedNode
                Session("LastLevel") = currentnode.ToolTip.Split(",")(0)
                strNodeID = currentnode.ToolTip.Split(",")(2)

                Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
                sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                sqlConnection.Open()

                sqlCommand.CommandText = "UPDATE ProductDetails SET MapID=0 WHERE MapID = '" + strNodeID + "' AND CompID = '" + Session("CompanyID") + "' "
                sqlCommand.ExecuteNonQuery()
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('9');", True)

                sqlConnection.Close()
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('10');", True)
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
        End If
    End Sub
    Protected Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        Dim currentnode As TreeNode
        Dim strNodeID As String = ""
        Dim Path As String = ""

        If (tvLevels.SelectedNode IsNot Nothing) Then
            If tvLevels.SelectedNode.ChildNodes.Count = 0 Then
                currentnode = tvLevels.SelectedNode
                strNodeID = currentnode.ToolTip.Split(",")(2)
                Path = currentnode.ValuePath

                Session("ID") = strNodeID
                Session("Path") = Path
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "Showopoup('LevelRecord.aspx');", True)
            Else
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('10');", True)
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
        End If
    End Sub
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        txtLevel1E.ReadOnly = False
        txtLevel1A.ReadOnly = False
        txtNo1.ReadOnly = False

        txtNo1.Text = ""
        txtNo2.Text = ""
        txtNo3.Text = ""
        txtNo4.Text = ""
        txtNo5.Text = ""
        txtNo6.Text = ""
        txtNo7.Text = ""
        txtNo8.Text = ""

        txtLevel1E.Text = ""
        txtLevel2E.Text = ""
        txtLevel3E.Text = ""
        txtLevel4E.Text = ""
        txtLevel5E.Text = ""
        txtLevel6E.Text = ""
        txtLevel7E.Text = ""
        txtLevel8E.Text = ""

        txtLevel1A.Text = ""
        txtLevel2A.Text = ""
        txtLevel3A.Text = ""
        txtLevel4A.Text = ""
        txtLevel5A.Text = ""
        txtLevel6A.Text = ""
        txtLevel7A.Text = ""
        txtLevel8A.Text = ""

        If (tvLevels.SelectedNode IsNot Nothing) Then
            tvLevels.SelectedNode.Selected = False
        End If

        txtNo1.Focus()
    End Sub
    Protected Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        If (Request.IsAuthenticated) Then
            'LoadTreeViewData()
            Button10.Attributes("onclick") = "if(ShowConfirm() == false){ return false; }"
        Else
            Response.Redirect("../Login.aspx")
        End If
        If (UICulture = "Arabic") Then
            Panel1.Style.Add("direction", "rtl")
            Panel1.Style.Add("text-align", "right")
            'Panel2.Style.Add("direction", "rtl")
            'Panel2.Style.Add("text-align", "right")
            tvLevels.Style.Add("float", "right")
            tvLevels.Style.Add("direction", "rtl")
            tvLevels.Style.Add("text-align", "right")
            'div1.Style.Add("float", "right")
            div2.Style.Add("float", "right")
            'div3.Style.Add("float", "right")
            div2.Style.Add("direction", "rtl")
            div2.Style.Add("text-align", "right")
            LinkButton1.Style.Add("float", "right")
        End If

        txtLevel1A.Style.Add("text-align", "right")
        txtLevel2A.Style.Add("text-align", "right")
        txtLevel3A.Style.Add("text-align", "right")
        txtLevel4A.Style.Add("text-align", "right")
        txtLevel5A.Style.Add("text-align", "right")
        txtLevel6A.Style.Add("text-align", "right")
        txtLevel7A.Style.Add("text-align", "right")
        txtLevel8A.Style.Add("text-align", "right")

        txtLevel1E.Style.Add("text-align", "left")
        txtLevel2E.Style.Add("text-align", "left")
        txtLevel3E.Style.Add("text-align", "left")
        txtLevel4E.Style.Add("text-align", "left")
        txtLevel5E.Style.Add("text-align", "left")
        txtLevel6E.Style.Add("text-align", "left")
        txtLevel7E.Style.Add("text-align", "left")
        txtLevel8E.Style.Add("text-align", "left")
    End Sub
    Protected Sub LinkButton2_Click(sender As Object, e As EventArgs) Handles LinkButton2.Click
        Dim strFileName As String = "ExcelFile"
        If (uplImage.HasFile) Then
            Dim fileExt As String = System.IO.Path.GetExtension(uplImage.FileName).ToLower()
            If (fileExt = ".xls" Or fileExt = ".xlsx") Then
                'show here

                strFileName = strFileName + fileExt

                If (System.IO.File.Exists("../Upload/Excel/" + strFileName)) Then
                    System.IO.File.Delete("../Upload/Excel/" + strFileName)
                End If
                uplImage.SaveAs(Server.MapPath("../Upload/Excel/" + strFileName))

                Dim strQuery As String = String.Empty
                Dim strNodeNumber As String = String.Empty
                Dim strEngName As String = String.Empty
                Dim strAraName As String = String.Empty
                Dim strParentNodeNumber As String = String.Empty
                Dim strCrewCode As String = String.Empty
                Dim strDO As String = String.Empty
                Dim strUnit As String = String.Empty
                Dim strMaterialCost As String = String.Empty
                Dim isPercent As String = String.Empty
                Dim strItemCost As String = String.Empty

                Dim sqlConnection As New SqlConnection()
                Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
                sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
                sqlConnection.Open()

                Try
                    Dim strClientTypeID As String = "1"
                    If (Session("ClientTypeID") IsNot Nothing) Then
                        strClientTypeID = Session("ClientTypeID")
                    End If

                    Dim conStr As String = ""
                    Dim Extension As String = Path.GetExtension(Server.MapPath("../Upload/Excel/" + strFileName))

                    Select Case Extension
                        Case ".xls"
                            conStr = ConfigurationManager.ConnectionStrings("Excel03ConString").ConnectionString()
                            Exit Select
                        Case ".xlsx"
                            conStr = ConfigurationManager.ConnectionStrings("Excel07ConString").ConnectionString()
                            Exit Select
                    End Select

                    conStr = String.Format(conStr, Server.MapPath("../Upload/Excel/" + strFileName), "Yes")

                    Dim connExcel As New OleDbConnection(conStr)
                    Dim cmdExcel As New OleDbCommand()
                    Dim oda As New OleDbDataAdapter()
                    Dim dt, dt1 As New DataTable()

                    cmdExcel.Connection = connExcel
                    connExcel.Open()

                    Dim dtExcelSchema As DataTable

                    dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                    Dim SheetName As String = ""

                    For i As Integer = 0 To dtExcelSchema.Rows.Count - 1
                        'Dim index As Integer = dtExcelSchema.Rows(i)("TABLE_NAME").ToString().IndexOf("$")
                        'Dim strName As String = dtExcelSchema.Rows(i)("TABLE_NAME").ToString().Substring(index, dtExcelSchema.Rows(i)("TABLE_NAME").ToString().Length - index)
                        If (dtExcelSchema.Rows(i)("TABLE_NAME").ToString().Contains("$") = False) Then
                            'If (strName.Length > 2) Then
                            dtExcelSchema.Rows(i).Delete()
                        End If
                    Next

                    For i As Integer = 0 To dtExcelSchema.Rows.Count - 1
                        If (dtExcelSchema.Rows(i).RowState <> DataRowState.Deleted) Then
                            SheetName = dtExcelSchema.Rows(i)("TABLE_NAME").ToString()
                            cmdExcel.CommandText = "SELECT * From [" & SheetName & "]"
                            oda.SelectCommand = cmdExcel
                            If (dt.Rows.Count = 0) Then
                                oda.Fill(dt)
                            Else
                                dt1.Columns.Clear()
                                dt1.Clear()
                                oda.Fill(dt1)
                                If (dt1.Rows.Count > 0) Then
                                    dt = dt1.Copy()
                                End If
                            End If
                        End If
                    Next

                    connExcel.Close()

                    If (dt.Columns.Count = 10) Then
                        For i As Integer = 0 To dt.Rows.Count - 1

                            strNodeNumber = ""
                            strEngName = ""
                            strAraName = ""
                            strParentNodeNumber = ""
                            strCrewCode = ""
                            strUnit = ""
                            strDO = ""
                            strMaterialCost = ""
                            strItemCost = ""
                            isPercent = ""

                            If dt.Rows(i).Item(0).ToString() IsNot Nothing And dt.Rows(i).Item(0).ToString().Length > 0 Then

                                strNodeNumber = dt.Rows(i).Item(0).ToString()

                                If dt.Rows(i).Item(1).ToString() IsNot Nothing And dt.Rows(i).Item(1).ToString().Length > 0 Then
                                    strEngName = dt.Rows(i).Item(1).ToString()
                                End If
                                If dt.Rows(i).Item(2).ToString() IsNot Nothing And dt.Rows(i).Item(2).ToString().Length > 0 Then
                                    strAraName = dt.Rows(i).Item(2).ToString()
                                End If
                                If dt.Rows(i).Item(3).ToString() IsNot Nothing And dt.Rows(i).Item(3).ToString().Length > 0 Then
                                    strParentNodeNumber = dt.Rows(i).Item(3).ToString()
                                End If
                                If dt.Rows(i).Item(4).ToString() IsNot Nothing And dt.Rows(i).Item(4).ToString().Length > 0 Then
                                    strCrewCode = dt.Rows(i).Item(4).ToString()
                                End If
                                If dt.Rows(i).Item(5).ToString() IsNot Nothing And dt.Rows(i).Item(5).ToString().Length > 0 Then
                                    strDO = dt.Rows(i).Item(5).ToString()
                                End If
                                If dt.Rows(i).Item(6).ToString() IsNot Nothing And dt.Rows(i).Item(6).ToString().Length > 0 Then
                                    strUnit = dt.Rows(i).Item(6).ToString()
                                End If
                                If dt.Rows(i).Item(7).ToString() IsNot Nothing And dt.Rows(i).Item(7).ToString().Length > 0 Then
                                    strMaterialCost = dt.Rows(i).Item(7).ToString()
                                    strMaterialCost = strMaterialCost.Replace("SAR", "")
                                    strMaterialCost = strMaterialCost.Replace(" ", "")
                                    strMaterialCost = strMaterialCost.Replace("-", "0")
                                End If
                                If dt.Rows(i).Item(8).ToString() IsNot Nothing And dt.Rows(i).Item(8).ToString().Length > 0 Then
                                    isPercent = dt.Rows(i).Item(8).ToString()
                                    isPercent = isPercent.Replace("0.00", "0")
                                    isPercent = isPercent.Replace("1.00", "1")
                                End If
                                If dt.Rows(i).Item(9).ToString() IsNot Nothing And dt.Rows(i).Item(9).ToString().Length > 0 Then
                                    strItemCost = dt.Rows(i).Item(9).ToString()
                                End If

                                strQuery = "SELECT COUNT(*) FROM MainHierarchy1 WHERE No = '" + strNodeNumber + "' AND CompID = '" + Session("CompanyID") + "' "
                                sqlCommand.CommandText = strQuery

                                If (sqlCommand.ExecuteScalar().ToString() = "0") Then

                                    strEngName = strEngName.Replace("'", " ")
                                    strAraName = strAraName.Replace("'", " ")

                                    strQuery = "INSERT INTO Mainhierarchy1(NameE,NameA,Status,No,ParentNo,ClientTypeID, CompID) VALUES (N'" + strEngName + "', N'" + strAraName + "', 1, '" + strNodeNumber + "', '" + strParentNodeNumber + "', '" + strClientTypeID + "', '" + Session("CompanyID") + "' )"
                                    sqlCommand.CommandText = strQuery
                                    sqlCommand.ExecuteNonQuery()

                                    If (strCrewCode.Length > 0 And strDO.Length > 0) Then
                                        Dim objSqlConfig As New ConfigClassSqlServer()
                                        objSqlConfig.UpdateCostLineItem(strNodeNumber, strCrewCode, isPercent, strItemCost, strMaterialCost, strUnit, strDO)
                                    End If

                                End If

                            End If


                        Next
                        hdnInitialLoad.Value = "1"
                        'hide here
                    Else
                        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "alert('Invalid file format...');", True)
                    End If

                Catch ex As Exception
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('15');", True)
                    'hide here
                End Try
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('14');", True)
            Else
                hdnInitialLoad.Value = "0"
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('13');", True)
                Return
            End If
        Else
            hdnInitialLoad.Value = "0"
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('13');", True)
            Return
        End If
    End Sub
    Protected Sub lnkSet_Click(sender As Object, e As EventArgs) Handles lnkSet.Click
        If (tvLevels.SelectedNode IsNot Nothing) Then
            If (tvLevels.SelectedNode.ChildNodes.Count > 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('24');", True)
                Return
            End If
            If (ddlCrew.SelectedIndex = 0 And (txtCost.Text.Length = 0 Or txtCost.Text = "0" Or txtCost.Text = "0.00")) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('25');", True)
                txtCost.Focus()
                Return
            End If
            If (chkPercent.Checked) Then
                If (Convert.ToDouble(txtCost.Text) < 0 Or Convert.ToDouble(txtCost.Text) > 100) Then
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('20');", True)
                    txtCost.Focus()
                    Return
                End If
            End If

            Dim strCost As String = "0"
            Dim strMaterialCost As String = "0"

            If (txtCost.Text.Length > 0) Then
                strCost = txtCost.Text.Trim()
            End If
            If (txtMatCost.Text.Length > 0) Then
                strMaterialCost = txtMatCost.Text.Trim()
            End If

            Dim objSqlConfig As New ConfigClassSqlServer()
            Dim strNodeNumber As String = tvLevels.SelectedNode.ToolTip.Split(",")(2).ToString()
            If (objSqlConfig.UpdateCostLineItemByCompany(strNodeNumber, ddlCrew.SelectedValue, (Convert.ToInt32(chkPercent.Checked)).ToString(), strCost.Trim(), strMaterialCost.Trim(), txtUnit.Text.Trim(), txtDO.Text.Trim(), Session("CompanyID")) = 0) Then
                ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('23');", True)
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
        End If
    End Sub
    Protected Sub ddlCrew_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCrew.SelectedIndexChanged
        txtCost.ReadOnly = False
        If (ddlCrew.SelectedIndex > 0) Then
            txtCost.ReadOnly = True
            txtCost.Text = "0"
        End If
    End Sub
    Protected Sub btnDeleteAll_Click(sender As Object, e As EventArgs) Handles btnDeleteAll.Click
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        'sqlCommand.CommandText = "DELETE FROM MainHierarchy1 WHERE CompId = 0 DELETE FROM Project  DELETE FROM Project_BOQ_Data  DELETE FROM Project_CostLineItems  DELETE FROM Project_CrewStructure DELETE FROM Role_Cat_Assignment "
        sqlCommand.CommandText = "DELETE FROM MainHierarchy1 WHERE CompId = 0 DELETE FROM Role_Cat_Assignment "
        sqlCommand.ExecuteNonQuery()

        tvLevels.Nodes.Clear()

        sqlConnection.Close()
    End Sub

    Public Sub TreeData()
        Dim strClientTypeID As String = "1"
        If (Session("ClientTypeID") IsNot Nothing) Then
            strClientTypeID = Session("ClientTypeID")
        End If
        If (hdnInitialLoad.Value.ToString().Equals("1")) Then
            Dim sqlConnection As New SqlConnection()
            Dim node As TreeNode = Nothing
            'Dim NodeLevel1, NodeLevel2, NodeLevel3, NodeLevel4, NodeLevel5, NodeLevel6, NodeLevel7 As TreeNode
            'Dim NodeLevel1 As TreeNode
            tvLevels.Nodes.Clear()

            Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
            sqlCommand.CommandTimeout = 120000
            'Dim r1, r2, r3, r4, r5, r6, r7, r8 As SqlDataReader
            Dim r1 As SqlDataReader
            'Dim dt1, dt2, dt3, dt4, dt5, dt6, dt7, dt8 As New DataTable()
            Dim dt1 As New DataTable()

            sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
            sqlConnection.Open()

            If (UICulture = "Arabic") Then
                sqlCommand.CommandText = "SELECT DISTINCT ID,  NameA, NameE, CAST(No As Numeric(25))No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                r1 = sqlCommand.ExecuteReader()
                dt1.Load(r1)
                r1.Close()

                For Each dr1 As DataRow In dt1.Rows
                    If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
                        node = New TreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
                        node.Target = dr1.Item(2).ToString()
                        'node.ImageUrl = dr1.Item(3).ToString()
                        node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
                        tvLevels.Nodes.Add(node)
                        'NodeLevel1 = node

                        'sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        'r2 = sqlCommand.ExecuteReader()
                        'dt2.Clear()
                        'dt2.Load(r2)
                        'r2.Close()
                        'For Each dr2 As DataRow In dt2.Rows
                        '    If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
                        '        node = New TreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
                        '        node.Target = dr2.Item(2).ToString()
                        '        'node.ImageUrl = dr2.Item(3).ToString() 
                        '        node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
                        '        NodeLevel1.ChildNodes.Add(node)
                        '        NodeLevel2 = node

                        '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '        r3 = sqlCommand.ExecuteReader()
                        '        dt3.Clear()
                        '        dt3.Load(r3)
                        '        r3.Close()
                        '        For Each dr3 As DataRow In dt3.Rows
                        '            If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
                        '                node = New TreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
                        '                node.Target = dr3.Item(2).ToString()
                        '                'node.ImageUrl = dr3.Item(3).ToString()
                        '                node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
                        '                NodeLevel2.ChildNodes.Add(node)
                        '                NodeLevel3 = node

                        '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                r4 = sqlCommand.ExecuteReader()
                        '                dt4.Clear()
                        '                dt4.Load(r4)
                        '                r4.Close()

                        '                For Each dr4 As DataRow In dt4.Rows
                        '                    If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
                        '                        node = New TreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
                        '                        node.Target = dr4.Item(2).ToString()
                        '                        'node.ImageUrl = dr4.Item(3).ToString()
                        '                        node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
                        '                        NodeLevel3.ChildNodes.Add(node)
                        '                        NodeLevel4 = node

                        '                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                        r5 = sqlCommand.ExecuteReader()
                        '                        dt5.Clear()
                        '                        dt5.Load(r5)
                        '                        r5.Close()
                        '                        For Each dr5 As DataRow In dt5.Rows
                        '                            If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
                        '                                node = New TreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
                        '                                node.Target = dr5.Item(2).ToString()
                        '                                'node.ImageUrl = dr5.Item(3).ToString()
                        '                                node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
                        '                                NodeLevel4.ChildNodes.Add(node)
                        '                                NodeLevel5 = node

                        '                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                                r6 = sqlCommand.ExecuteReader()
                        '                                dt6.Clear()
                        '                                dt6.Load(r6)
                        '                                r6.Close()
                        '                                For Each dr6 As DataRow In dt6.Rows
                        '                                    If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
                        '                                        node = New TreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
                        '                                        node.Target = dr6.Item(2).ToString()
                        '                                        'node.ImageUrl = dr6.Item(3).ToString()
                        '                                        node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
                        '                                        NodeLevel5.ChildNodes.Add(node)
                        '                                        NodeLevel6 = node

                        '                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                                        r7 = sqlCommand.ExecuteReader()
                        '                                        dt7.Clear()
                        '                                        dt7.Load(r7)
                        '                                        r7.Close()
                        '                                        For Each dr7 As DataRow In dt7.Rows
                        '                                            If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
                        '                                                node = New TreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
                        '                                                node.Target = dr7.Item(2).ToString()
                        '                                                'node.ImageUrl = dr7.Item(3).ToString()
                        '                                                node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
                        '                                                NodeLevel6.ChildNodes.Add(node)
                        '                                                NodeLevel7 = node

                        '                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameA, NameE, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                                                r8 = sqlCommand.ExecuteReader()
                        '                                                dt8.Clear()
                        '                                                dt8.Load(r8)
                        '                                                r8.Close()
                        '                                                For Each dr8 As DataRow In dt8.Rows
                        '                                                    If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
                        '                                                        node = New TreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
                        '                                                        node.Target = dr8.Item(2).ToString()
                        '                                                        'node.ImageUrl = dr8.Item(3).ToString()
                        '                                                        node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
                        '                                                        NodeLevel7.ChildNodes.Add(node)
                        '                                                    End If
                        '                                                Next
                        '                                            End If
                        '                                        Next
                        '                                    End If
                        '                                Next
                        '                            End If
                        '                        Next
                        '                    End If
                        '                Next
                        '            End If
                        '        Next
                        '    End If
                        'Next
                    End If
                Next

            Else
                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
                r1 = sqlCommand.ExecuteReader()
                dt1.Load(r1)
                r1.Close()

                For Each dr1 As DataRow In dt1.Rows
                    If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then
                        node = New TreeNode(dr1.Item(3).ToString() + " " + dr1.Item(1).ToString())
                        node.Target = dr1.Item(2).ToString()
                        'node.ImageUrl = dr1.Item(3).ToString()
                        node.ToolTip = "1," + dr1.Item(0).ToString() + "," + dr1.Item(3).ToString()
                        tvLevels.Nodes.Add(node)
                        'NodeLevel1 = node

                        'sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr1.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        'r2 = sqlCommand.ExecuteReader()
                        'dt2.Clear()
                        'dt2.Load(r2)
                        'r2.Close()
                        'For Each dr2 As DataRow In dt2.Rows
                        '    If Not (IsDBNull(dr2.Item(0).ToString().ToString())) And Not (Trim(dr2.Item(0).ToString().ToString())) = "" Then
                        '        node = New TreeNode(dr2.Item(3).ToString() + " " + dr2.Item(1).ToString())
                        '        node.Target = dr2.Item(2).ToString()
                        '        'node.ImageUrl = dr2.Item(3).ToString()
                        '        node.ToolTip = "2," + dr2.Item(0).ToString() + "," + dr2.Item(3).ToString()
                        '        NodeLevel1.ChildNodes.Add(node)
                        '        NodeLevel2 = node

                        '        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr2.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '        r3 = sqlCommand.ExecuteReader()
                        '        dt3.Clear()
                        '        dt3.Load(r3)
                        '        r3.Close()
                        '        For Each dr3 As DataRow In dt3.Rows
                        '            If Not (IsDBNull(dr3.Item(0).ToString())) And Not (Trim(dr3.Item(0).ToString())) = "" Then
                        '                node = New TreeNode(dr3.Item(3).ToString() + " " + dr3.Item(1).ToString())
                        '                node.Target = dr3.Item(2).ToString()
                        '                'node.ImageUrl = dr3.Item(3).ToString()
                        '                node.ToolTip = "3," + dr3.Item(0).ToString() + "," + dr3.Item(3).ToString()
                        '                NodeLevel2.ChildNodes.Add(node)
                        '                NodeLevel3 = node

                        '                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr3.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                r4 = sqlCommand.ExecuteReader()
                        '                dt4.Clear()
                        '                dt4.Load(r4)
                        '                r4.Close()

                        '                For Each dr4 As DataRow In dt4.Rows
                        '                    If Not (IsDBNull(dr4.Item(0).ToString())) And Not (Trim(dr4.Item(0).ToString())) = "" Then
                        '                        node = New TreeNode(dr4.Item(3).ToString() + " " + dr4.Item(1).ToString())
                        '                        node.Target = dr4.Item(2).ToString()
                        '                        'node.ImageUrl = dr4.Item(3).ToString()
                        '                        node.ToolTip = "4," + dr4.Item(0).ToString() + "," + dr4.Item(3).ToString()
                        '                        NodeLevel3.ChildNodes.Add(node)
                        '                        NodeLevel4 = node

                        '                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr4.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                        r5 = sqlCommand.ExecuteReader()
                        '                        dt5.Clear()
                        '                        dt5.Load(r5)
                        '                        r5.Close()
                        '                        For Each dr5 As DataRow In dt5.Rows
                        '                            If Not (IsDBNull(dr5.Item(0).ToString())) And Not (Trim(dr5.Item(0).ToString())) = "" Then
                        '                                node = New TreeNode(dr5.Item(3).ToString() + " " + dr5.Item(1).ToString())
                        '                                node.Target = dr5.Item(2).ToString()
                        '                                'node.ImageUrl = dr5.Item(3).ToString()
                        '                                node.ToolTip = "5," + dr5.Item(0).ToString() + "," + dr5.Item(3).ToString()
                        '                                NodeLevel4.ChildNodes.Add(node)
                        '                                NodeLevel5 = node

                        '                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr5.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                                r6 = sqlCommand.ExecuteReader()
                        '                                dt6.Clear()
                        '                                dt6.Load(r6)
                        '                                r6.Close()
                        '                                For Each dr6 As DataRow In dt6.Rows
                        '                                    If Not (IsDBNull(dr6.Item(0).ToString())) And Not (Trim(dr6.Item(0).ToString())) = "" Then
                        '                                        node = New TreeNode(dr6.Item(3).ToString() + " " + dr6.Item(1).ToString())
                        '                                        node.Target = dr6.Item(2).ToString()
                        '                                        'node.ImageUrl = dr6.Item(3).ToString()
                        '                                        node.ToolTip = "6," + dr6.Item(0).ToString() + "," + dr6.Item(3).ToString()
                        '                                        NodeLevel5.ChildNodes.Add(node)
                        '                                        NodeLevel6 = node

                        '                                        sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr6.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                                        r7 = sqlCommand.ExecuteReader()
                        '                                        dt7.Clear()
                        '                                        dt7.Load(r7)
                        '                                        r7.Close()
                        '                                        For Each dr7 As DataRow In dt7.Rows
                        '                                            If Not (IsDBNull(dr7.Item(0).ToString())) And Not (Trim(dr7.Item(0).ToString())) = "" Then
                        '                                                node = New TreeNode(dr7.Item(3).ToString() + " " + dr7.Item(1).ToString())
                        '                                                node.Target = dr7.Item(2).ToString()
                        '                                                'node.ImageUrl = dr7.Item(3).ToString()
                        '                                                node.ToolTip = "7," + dr7.Item(0).ToString() + "," + dr7.Item(3).ToString()
                        '                                                NodeLevel6.ChildNodes.Add(node)
                        '                                                NodeLevel7 = node

                        '                                                sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)CAST(No As Float)No FROM MainHierarchy1 WHERE ParentNo ='" & Trim(dr7.Item(3).ToString()) & "' AND Status IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' ORDER BY No ASC"
                        '                                                r8 = sqlCommand.ExecuteReader()
                        '                                                dt8.Clear()
                        '                                                dt8.Load(r8)
                        '                                                r8.Close()
                        '                                                For Each dr8 As DataRow In dt8.Rows
                        '                                                    If Not (IsDBNull(dr8.Item(0).ToString())) And Not (Trim(dr8.Item(0).ToString())) = "" Then
                        '                                                        node = New TreeNode(dr8.Item(3).ToString() + " " + dr8.Item(1).ToString())
                        '                                                        node.Target = dr8.Item(2).ToString()
                        '                                                        'node.ImageUrl = dr8.Item(3).ToString()
                        '                                                        node.ToolTip = "8," + dr8.Item(0).ToString() + "," + dr8.Item(3).ToString()
                        '                                                        NodeLevel7.ChildNodes.Add(node)
                        '                                                    End If
                        '                                                Next
                        '                                            End If
                        '                                        Next
                        '                                    End If
                        '                                Next
                        '                            End If
                        '                        Next
                        '                    End If
                        '                Next
                        '            End If
                        '        Next
                        '    End If
                        'Next
                    End If
                Next
            End If

            tvLevels.Visible = True
            sqlConnection.Close()
            tvLevels.ExpandAll()


        End If
        'ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "HideDiv();", True)
    End Sub

    <System.Web.Services.WebMethod()>
    <ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Json)>
    Public Shared Function GetMasterList As List(Of TreeViewModel)

        Dim Session = HttpContext.Current.Session
        Dim strClientTypeID As String = "1"


        If (Session("ClientTypeID") IsNot Nothing) Then
            strClientTypeID = Session("ClientTypeID")
        End If
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlCommand.CommandTimeout = 120000
        Dim r1 As SqlDataReader
        Dim dt1 As New DataTable()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        'If (UICulture = "Arabic") Then
        sqlCommand.CommandText = "SELECT DISTINCT ID,  NameA, NameE, CAST(No As Numeric(25))No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
        r1 = sqlCommand.ExecuteReader()
        dt1.Load(r1)
        r1.Close()

        Dim treeViewModel As New List(Of TreeViewModel)
        For Each dr1 As DataRow In dt1.Rows
            Dim inputModel As New TreeViewModel
            If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then

                inputModel.Id = dr1.Item(0)
                inputModel.NameA = dr1.Item(1)
                inputModel.NameE = dr1.Item(2)
                inputModel.No = dr1.Item(3)
                treeViewModel.Add(inputModel)

            End If
        Next


        Return treeViewModel

        'Else
        '    sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
        '        r1 = sqlCommand.ExecuteReader()
        '        dt1.Load(r1)
        '        r1.Close()
        '    End If
        'End If
    End Function


    <System.Web.Services.WebMethod()>
    <ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Json)>
    Public Shared Function GetChields(parentNo As String) As List(Of TreeViewModel)


        Dim Session = HttpContext.Current.Session
        Dim strClientTypeID As String = "1"


        If (Session("ClientTypeID") IsNot Nothing) Then
            strClientTypeID = Session("ClientTypeID")
        End If
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlCommand.CommandTimeout = 120000
        Dim r1 As SqlDataReader
        Dim dt1 As New DataTable()

        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        'If (UICulture = "Arabic") Then
        sqlCommand.CommandText = "WITH cte AS 
             (
              SELECT  a.ID,a.NameA, a.NameE,a.No,a.ParentNo,a.CrewCode, a.IsPercent, a.Cost, a.MaterialCost, a.Unit, a.Do
              FROM MainHierarchy1 a
              WHERE STATUS IN(1,3) AND ClientTypeID = '1' AND CompID = '0' and No = '" + parentNo + "'
              UNION ALL
              SELECT a.ID,a.NameA, a.NameE,a.No,a.ParentNo,a.CrewCode, a.IsPercent, a.Cost, a.MaterialCost, a.Unit, a.Do
              FROM MainHierarchy1 a JOIN cte c ON a.ParentNo = c.No 
              where STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' 
              )
              SELECT  ID,NameA, NameE,No,ParentNo,CrewCode, IsPercent, Cost, MaterialCost, Unit, Do
              FROM cte"
        r1 = sqlCommand.ExecuteReader()
        dt1.Load(r1)
        r1.Close()

        Dim treeViewModel As New List(Of TreeViewModel)
        For Each dr1 As DataRow In dt1.Rows
            Dim inputModel As New TreeViewModel
            If Not (IsDBNull(dr1.Item(0).ToString())) And Not (Trim(dr1.Item(0).ToString())) = "" Then

                inputModel.Id = dr1.Item(0)
                inputModel.NameA = dr1.Item(1)
                inputModel.NameE = dr1.Item(2)
                inputModel.No = dr1.Item(3)
                inputModel.ParentNo = dr1.Item(4)
                If Not IsDBNull(dr1.Item(5)) Then
                    inputModel.CrewCode = dr1.Item(5)
                    inputModel.IsPercent = dr1.Item(6)
                    If inputModel.IsPercent = "1" Then
                        inputModel.IsPercent = "true"
                    Else
                        inputModel.IsPercent = "false"
                    End If
                    inputModel.Cost = dr1.Item(7)
                    inputModel.MaterialCost = dr1.Item(8)
                    inputModel.Unit = dr1.Item(9)
                    inputModel.DailyOutput = dr1.Item(10)
                Else
                        inputModel.CrewCode = "-"
                    inputModel.IsPercent = "-"
                    inputModel.Cost = "-"
                    inputModel.MaterialCost = "-"
                    inputModel.Unit = "-"
                    inputModel.DailyOutput = "-"
                End If


                treeViewModel.Add(inputModel)

            End If
        Next

        Dim groupedResult = treeViewModel.GroupBy(Function(x)
                                                      Return x.ParentNo
                                                  End Function)

        Dim finalResult = GetChield(groupedResult, parentNo)
        Return finalResult

        'Else
        '    sqlCommand.CommandText = "SELECT DISTINCT ID, NameE, NameA, CAST(No As Float)No FROM MainHierarchy1 WHERE (ParentNo iS NULL OR Len(ParentNo) = 0) AND STATUS IN(1,3) AND ClientTypeID = '" + strClientTypeID + "' AND CompID = '" + Session("CompanyID") + "' ORDER BY No ASC"
        '        r1 = sqlCommand.ExecuteReader()
        '        dt1.Load(r1)
        '        r1.Close()
        '    End If
        'End If
    End Function


    Public Shared Function GetChield(input As IEnumerable(Of IGrouping(Of String, TreeViewModel)), parantNo As String) As List(Of TreeViewModel)

        Dim myChields = input.Where(Function(x)
                                        Return x.Key = parantNo
                                    End Function).FirstOrDefault
        If myChields Is Nothing Then
            Return Nothing
        End If



        Dim result As New List(Of TreeViewModel)

        Dim chield As TreeViewModel
        Dim ch As TreeViewModel
        For Each chield In myChields
            ch = New TreeViewModel With {
                .Id = chield.Id,
                .NameA = chield.NameA,
                .NameE = chield.NameE,
                .No = chield.No,
                .ParentNo = chield.ParentNo,
                .CrewCode = chield.CrewCode,
                .IsPercent = chield.IsPercent,
                .Cost = chield.Cost,
                .MaterialCost = chield.MaterialCost,
                .Unit = chield.Unit,
                .DailyOutput = chield.DailyOutput
            }
            ch.Chields = GetChield(input, ch.No)

            result.Add(ch)
        Next

        Return result

    End Function




End Class