﻿Public Class Template_Project
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            Session("BuildingName") = Nothing
            'Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
            'Menuhide.Value = "close"

            Dim Menu1 As Menu = Me.Page.Master.FindControl("Menu1")
            Dim menuItems As MenuItemCollection = Menu1.Items
            If (menuItems.Count > 15) Then
                menuItems.RemoveAt(12)
                menuItems.RemoveAt(12)
                menuItems.RemoveAt(12)
                menuItems.RemoveAt(12)
            End If

            searchbox.Visible = False
            Session("ProjectSaved") = "0"
            Session("Project_Type") = Nothing
            Session("Proj_Status") = Nothing
            Session("ProjectName") = Nothing
            Session("ProjectID") = Nothing
            If (Request.IsAuthenticated) Then
                If (UICulture = "Arabic") Then
                    Panel1.Style.Add("direction", "rtl")
                    Panel1.Style.Add("text-align", "right")
                    LinkButton1.Style.Add("direction", "rtl")
                    LinkButton1.Style.Add("float", "right")
                    LinkButton2.Style.Add("direction", "rtl")
                    LinkButton2.Style.Add("float", "right")
                    LinkButton3.Style.Add("direction", "rtl")
                    LinkButton3.Style.Add("float", "right")
                    LinkButton5.Style.Add("direction", "rtl")
                    LinkButton5.Style.Add("float", "right")
                    txtProjName.Width = 198
                End If
            Else
                Response.Redirect("../Login.aspx")
            End If


            Dim objSqlConfig As New ConfigClassSqlServer()
            Dim dt As New DataTable()
            Dim strCategories As String = ""
            dt = objSqlConfig.GetAllCategories(Session("CompanyID"))

            For i As Integer = 0 To dt.Rows.Count - 1
                strCategories = strCategories + dt.Rows(i).Item(0).ToString() + "}"
            Next

            If (strCategories.Length > 0) Then
                strCategories = strCategories.Substring(0, strCategories.Length - 1)
                objSqlConfig.InsertRole_Categories("999", strCategories.Trim())
            End If

            'Dim objSqlConfig As New ConfigClassSqlServer()
            'Dim dt As New DataTable()

            'LinkButton2.Visible = False
            'LinkButton3.Visible = False
            'LinkButton5.Visible = False

            'dt = objSqlConfig.GetUserRolesByID(Session("UserID").ToString())
            'If (dt.Rows.Count > 0) Then
            '    For i As Integer = 0 To dt.Rows.Count - 1
            '        If (dt.Rows(i).Item(0).ToString() = "4") Then
            '            LinkButton2.Visible = True
            '            LinkButton3.Visible = True
            '            LinkButton5.Visible = True
            '        End If
            '    Next
            'End If

            If (Session("Project_Type") IsNot Nothing) Then
                If (Session("Project_Type").ToString().Equals("New")) Then
                    LinkButton2_Click(sender, Nothing)
                    txtProjName.Text = Session("ProjectName")
                ElseIf (Session("Project_Type").ToString().Equals("Open")) Then
                    LinkButton1_Click(sender, Nothing)
                    txtProjName.Text = Session("ProjectName")
                Else
                    LinkButton3_Click(sender, Nothing)
                    txtProjName.Text = Session("ProjectName")
                End If
                Session("ProjectSaved") = "1"
            End If
        End If
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim strCreationDate As String = Now.ToString("yyyyMMddHHmmss")
        Dim strProjectID = ""
        Dim strlangID As String = "1"
        If (UICulture = "Arabic") Then
            strlangID = 2
        End If
        If (Session("Project_Type") IsNot Nothing) Then
            If (Session("Project_Type").Equals("New") Or Session("Project_Type").Equals("Existing") Or Session("Project_Type").Equals("BOQ")) Then
                If (txtProjName.Text.Trim.Length = 0) Then
                    ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('4');", True)
                    txtProjName.Focus()
                Else
                    Dim strProjectType As String = "-1"
                    If (Session("Project_Type").Equals("BOQ")) Then
                        strProjectType = "9"
                    End If
                    If (Session("ProjectID") IsNot Nothing) Then
                        strProjectID = objSqlConfig.Save_Update_Project(Session("ProjectID"), txtProjName.Text.Trim(), strCreationDate, Session("UserName"), "1", Session("CompanyID").ToString(), strlangID, strProjectType)
                    Else
                        strProjectID = objSqlConfig.Save_Update_Project("", txtProjName.Text.Trim(), strCreationDate, Session("UserName"), "1", Session("CompanyID").ToString(), strlangID, strProjectType)
                    End If
                    If (strProjectID = "-1") Then
                        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('2');", True)
                        Session("ProjectSaved") = "0"
                    Else
                        If (Session("Project_Type").Equals("Existing")) Then
                            strProjectID = objSqlConfig.CreateProjectFromExisting(grdProjects.SelectedRow.Cells(0).Text, txtProjName.Text, Session("UserName"), Session("CompanyID"), strProjectID, "1", strlangID, strProjectType)
                        End If
                        ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('1');", True)
                        Session("ProjectID") = strProjectID
                        Session("ProjectName") = txtProjName.Text.Trim()
                        Session("Proj_Status") = "1"
                        Session("ProjectSaved") = "1"
                    End If
                End If
            End If
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('5');", True)
        End If
    End Sub
    Protected Sub txtProjName_TextChanged(sender As Object, e As EventArgs) Handles txtProjName.TextChanged
        Session("ProjectSaved") = "0"
    End Sub
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Button1_Click(sender, Nothing)
        If (Session("ProjectSaved").Equals("0")) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage('3');", True)
        Else
            'If (Session("Project_Type") = "BOQ") Then
            '    Response.Redirect("ClientBOQ.aspx")
            'Else
            '    Response.Redirect("Add_Building.aspx")
            'End If
            Response.Redirect("../CES/Add_Building.aspx")
        End If
    End Sub
    Protected Sub LinkButton2_Click(sender As Object, e As EventArgs) Handles LinkButton2.Click
        Session("Project_Type") = "New"
        Session("ISBOQ") = Nothing
        Button1.Enabled = True
        txtProjName.Text = ""
        txtProjName.ReadOnly = False
        txtProjName.Focus()
        grdProjects.Visible = False
        searchbox.Visible = False
    End Sub
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        Session("ProjectSaved") = "0"
        Session("Project_Type") = "Open"
        txtProjName.Text = ""
        txtProjName.ReadOnly = True
        Button1.Enabled = False
        grdProjects.Visible = True
        searchbox.Visible = True
        LoadGrid()
        txtSearch.Focus()
    End Sub
    Protected Sub LinkButton3_Click(sender As Object, e As EventArgs) Handles LinkButton3.Click
        Session("ProjectSaved") = "0"
        Session("Project_Type") = "Existing"
        txtProjName.Text = ""
        txtProjName.ReadOnly = True
        grdProjects.Visible = True
        searchbox.Visible = True
        Button1.Enabled = True

        LoadGrid()
    End Sub
    Protected Sub grdProjects_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdProjects.SelectedIndexChanged
        txtProjName.Text = grdProjects.SelectedRow.Cells(0).Text
        'RadioButtonList1.SelectedIndex = grdProjects.SelectedDataKey(1).ToString()

        Session("ISBOQ") = Nothing
        If (grdProjects.SelectedDataKey(3).ToString().Equals("9")) Then
            Session("ISBOQ") = "YES"
        End If

        If (Session("Project_Type").Equals("Open")) Then
            Session("ProjectSaved") = "1"
            Session("ProjectID") = grdProjects.SelectedDataKey(0).ToString()
            Session("ProjectName") = grdProjects.SelectedRow.Cells(0).Text
            Session("Proj_Status") = grdProjects.SelectedDataKey(1).ToString()
        ElseIf (Session("Project_Type").Equals("Existing")) Then
            Session("ProjectSaved") = "0"
            Session("ProjectID") = Nothing
            Session("ProjectName") = Nothing
            txtProjName.ReadOnly = False
            Session("Proj_Status") = grdProjects.SelectedDataKey(1).ToString()
        Else
            Session("ProjectSaved") = "0"
        End If

        'Dim linkButton As LinkButton = Master.FindControl("LinkButton1")
        'linkButton.Visible = False

        Dim senderLink As New LinkButton
        If (grdProjects.SelectedDataKey(2).ToString() = "2") Then
            senderLink.CommandArgument = "ar"
        Else
            senderLink.CommandArgument = "en-US"
        End If

        If (UICulture = "Arabic" And grdProjects.SelectedDataKey(2).ToString() = "1") Then
            Response.Cookies.Add(New HttpCookie("Culture", senderLink.CommandArgument))
            Response.Redirect(Request.Url.AbsolutePath)
        ElseIf (UICulture <> "Arabic" And grdProjects.SelectedDataKey(2).ToString() = "2") Then
            Response.Cookies.Add(New HttpCookie("Culture", senderLink.CommandArgument))
            Response.Redirect(Request.Url.AbsolutePath)
        End If

    End Sub
    Private Sub LoadGrid()
        Dim objSqlConfig As New ConfigClassSqlServer()
        grdProjects.DataSource = Nothing
        grdProjects.DataBind()

        grdProjects.DataSource = objSqlConfig.GetAllTemplateProjects(txtSearch.Text)
        grdProjects.DataBind()
    End Sub
    Protected Sub grdProjects_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdProjects.PageIndexChanging
        grdProjects.PageIndex = e.NewPageIndex
        LoadGrid()
    End Sub
    Protected Sub grdProjects_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdProjects.Sorting
        If (ViewState("SortState") IsNot Nothing And ViewState("SortState") = SortDirection.Ascending) Then
            ViewState("SortDirection") = "asc"
            ViewState("SortState") = 1
        Else
            ViewState("SortDirection") = "desc"
            ViewState("SortState") = 0
        End If
        ViewState("SortExp") = e.SortExpression
        FillGrid()
    End Sub
    Private Sub FillGrid()
        LoadGrid()
        Dim Data As DataTable = grdProjects.DataSource
        If (Data IsNot Nothing) Then
            Dim dv As DataView = New DataView(Data)
            If Not (ViewState("SortExp") Is Nothing) Then
                If ViewState("SortDirection").ToString() = "asc" Then
                    dv.Sort = ViewState("SortExp") & " " & "asc"
                Else
                    dv.Sort = ViewState("SortExp") & " " & "desc"
                End If

                grdProjects.DataSource = dv
                grdProjects.DataBind()
            End If
        End If
    End Sub
    Protected Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        LoadGrid()
    End Sub
    Protected Sub grdProjects_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdProjects.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            If (e.Row.Cells(3).Text.Equals("0")) Then
                e.Row.Cells(3).Text = "Pending"
            Else
                e.Row.Cells(3).Text = "Complete"
            End If
        End If
    End Sub
    Protected Sub LinkButton5_Click(sender As Object, e As EventArgs) Handles LinkButton5.Click
        Session("Project_Type") = "BOQ"
        Session("ISBOQ") = "YES"
        Button1.Enabled = True
        txtProjName.Text = ""
        txtProjName.ReadOnly = False
        txtProjName.Focus()
        grdProjects.Visible = False
        searchbox.Visible = False
    End Sub
End Class