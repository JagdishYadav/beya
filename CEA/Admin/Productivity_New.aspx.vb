﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Productivity_New
    Inherits FMS.Culture.BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            Dim Menuhide As HiddenField = Me.Page.Master.FindControl("hfHideMenu")
            Menuhide.Value = "close"

            Dim objSqlConfig As New ConfigClassSqlServer()
            ddlCountry.DataSource = objSqlConfig.GetCountries()
            ddlCountry.DataValueField = "ID"

            Session("Labourdt") = Nothing
            Session("Equipdt") = Nothing

            If (UICulture = "Arabic") Then
                ddlCountry.DataTextField = "NameA"
                'abc.Style.Add("direction", "rtl")
                'abc.Style.Add("text-align", "right")
            Else
                ddlCountry.DataTextField = "NameE"
            End If
            ddlCountry.DataBind()
            ddlCountry.SelectedIndex = 0

            txtCode.Enabled = True
            If (Request.QueryString.Get("ID") IsNot Nothing) Then
                btnAdd.Text = GetGlobalResourceObject("Resource", "Update").ToString()
                txtCode.Enabled = False
                FillData(Request.QueryString.Get("ID").ToString())
            Else
                btnAdd.Text = GetGlobalResourceObject("Resource", "Save").ToString()
            End If

            LoadGrids()
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
        End If
    End Sub
    Private Sub LoadGrids()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt, dt1 As New DataTable()

        Dim strCode As String = ""
        If (Request.QueryString.Get("ID") IsNot Nothing) Then
            strCode = Request.QueryString.Get("ID").ToString()
        Else
            strCode = txtCode.Text.Trim()
        End If

        If (Session("Labourdt") IsNot Nothing) Then
            grdLabour.DataSource = Session("Labourdt")
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False

            dt = Session("Labourdt")
            If (dt.Rows.Count > 1) Then
                ddlCountry.SelectedValue = dt.Rows(1).Item(9).ToString()
            End If
        Else
            dt = objSqlConfig.GetCrewStructureLabour(strCode)
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = "-1"
            dr("Code") = "000"
            dr("ProfE") = ""
            dr("ProfA") = ""
            dr("Man_Day") = 0

            dt.Rows.InsertAt(dr, 0)
            grdLabour.DataSource = dt
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False
            Session("Labourdt") = dt

            If (dt.Rows.Count > 1) Then
                ddlCountry.SelectedValue = dt.Rows(1).Item(9).ToString()
            End If
        End If

        If (Session("Equipdt") IsNot Nothing) Then
            grdEquip.DataSource = Session("Equipdt")
            grdEquip.DataBind()
            grdEquip.Rows(0).Visible = False
        Else
            dt1 = objSqlConfig.GetCreStructureEquipment(strCode)
            Dim dr1 As DataRow = dt1.NewRow()
            dr1("ID") = "-1"
            dr1("Code") = "000"
            dr1("DescE") = ""
            dr1("DescA") = ""
            dr1("Man_Day") = 0

            dt1.Rows.InsertAt(dr1, 0)
            grdEquip.DataSource = dt1
            grdEquip.DataBind()
            grdEquip.Rows(0).Visible = False
            Session("Equipdt") = dt1
        End If
    End Sub
    Private Sub LoadDropDowns()
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim ddlLabour As DropDownList = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList)
        If (ddlLabour IsNot Nothing) Then
            ddlLabour.DataSource = objSqlConfig.GetLaboursByCountry("", ddlCountry.SelectedValue, Session("CompanyID"))
            ddlLabour.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlLabour.DataTextField = "ProfA"
            Else
                ddlLabour.DataTextField = "ProfE"
            End If
            ddlLabour.DataBind()
        End If


        Dim ddlEquip As DropDownList = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList)
        If (ddlEquip IsNot Nothing) Then
            ddlEquip.DataSource = objSqlConfig.GetEquipments("", Session("CompanyID"))
            ddlEquip.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlEquip.DataTextField = "DescA"
            Else
                ddlEquip.DataTextField = "DescE"
            End If
            ddlEquip.DataBind()
        End If
    End Sub
    Protected Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCountry.SelectedIndexChanged
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim ddlLabour As DropDownList = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList)
        If (ddlLabour IsNot Nothing) Then
            ddlLabour.DataSource = objSqlConfig.GetLaboursByCountry("", ddlCountry.SelectedValue, Session("CompanyID"))
            ddlLabour.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlLabour.DataTextField = "ProfA"
            Else
                ddlLabour.DataTextField = "ProfE"
            End If
            ddlLabour.DataBind()
        End If
    End Sub
    Private Sub SetEnvironmentAccordingtoLang()
        If (UICulture = "Arabic") Then
            For i As Integer = 0 To grdLabour.Rows.Count - 1
                If (grdLabour.Rows(i).RowState = DataControlRowState.Normal Or grdLabour.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdLabour.Rows(i).FindControl("l1"), Label).Visible = False
                End If
            Next
            For i As Integer = 0 To grdEquip.Rows.Count - 1
                If (grdEquip.Rows(i).RowState = DataControlRowState.Normal Or grdEquip.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdEquip.Rows(i).FindControl("l11"), Label).Visible = False
                End If
            Next
        Else
            For i As Integer = 0 To grdLabour.Rows.Count - 1
                If (grdLabour.Rows(i).RowState = DataControlRowState.Normal Or grdLabour.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdLabour.Rows(i).FindControl("l2"), Label).Visible = False
                End If
            Next
            For i As Integer = 0 To grdEquip.Rows.Count - 1
                If (grdEquip.Rows(i).RowState = DataControlRowState.Normal Or grdEquip.Rows(i).RowState = DataControlRowState.Alternate) Then
                    TryCast(grdEquip.Rows(i).FindControl("l12"), Label).Visible = False
                End If
            Next
        End If
    End Sub
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs)
        Dim dt As DataTable = Session("Labourdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                If (dt.Rows(i).Item(1).ToString() = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next

        If (iCount = 0) Then
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = "-1"
            dr("Code") = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedValue
            dr("ProfE") = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedItem.Text
            dr("ProfA") = TryCast(grdLabour.FooterRow.FindControl("ddlLabE"), DropDownList).SelectedItem.Text
            dr("Man_Day") = TryCast(grdLabour.FooterRow.FindControl("txtMDF"), TextBox).Text.Trim()
            dr("Nationality") = ddlCountry.SelectedValue.ToString()

            dt.Rows.Add(dr)
            grdLabour.DataSource = dt
            grdLabour.DataBind()
            grdLabour.Rows(0).Visible = False

            Session("Labourdt") = dt
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            'CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(2);", True)
        End If
    End Sub
    Protected Sub LinkButton11_Click(sender As Object, e As EventArgs)
        Dim dt As DataTable = Session("Equipdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                If (dt.Rows(i).Item(1).ToString() = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next

        If (iCount = 0) Then
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = "-1"
            dr("Code") = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedValue
            dr("DescE") = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedItem.Text
            dr("DescA") = TryCast(grdEquip.FooterRow.FindControl("ddlEquipE"), DropDownList).SelectedItem.Text
            dr("Man_Day") = TryCast(grdEquip.FooterRow.FindControl("txtMDF1"), TextBox).Text.Trim()

            dt.Rows.Add(dr)
            grdEquip.DataSource = dt
            grdEquip.DataBind()
            grdEquip.Rows(0).Visible = False

            Session("Equipdt") = dt
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            'CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(3);", True)
        End If
    End Sub
    Protected Sub grdLabour_RowCancelingEdit(sender As Object, e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdLabour.RowCancelingEdit
        e.Cancel = True
        grdLabour.EditIndex = -1
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdLabour_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdLabour.RowEditing
        grdLabour.EditIndex = e.NewEditIndex
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdLabour_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdLabour.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow And grdLabour.EditIndex = e.Row.RowIndex) Then
            Dim ddlLabour As DropDownList = DirectCast(e.Row.FindControl("ddlLabFE"), DropDownList)
            Dim objSqlConfig As New ConfigClassSqlServer()
            ddlLabour.DataSource = objSqlConfig.GetLaboursByCountry("", ddlCountry.SelectedValue, Session("CompanyID"))
            ddlLabour.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlLabour.DataTextField = "ProfA"
            Else
                ddlLabour.DataTextField = "ProfE"
            End If
            ddlLabour.DataBind()
            ddlLabour.SelectedValue = DirectCast(e.Row.FindControl("lblLabEdit"), Label).Text
        End If
    End Sub
    Protected Sub grdLabour_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdLabour.RowUpdating
        Dim dt As DataTable = Session("Labourdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (i <> e.RowIndex) Then
                If (dt.Rows(i).Item(1).ToString() = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next

        If (iCount = 0) Then
            dt.Rows(e.RowIndex).Item(1) = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedValue
            dt.Rows(e.RowIndex).Item(2) = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(3) = CType(grdLabour.Rows(e.RowIndex).FindControl("ddlLabFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(4) = CType(grdLabour.Rows(e.RowIndex).FindControl("txtMD"), TextBox).Text.Trim()

            grdLabour.EditIndex = -1
            Session("Labourdt") = dt
            LoadGrids()
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            'CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(2);", True)
        End If
    End Sub
    Protected Sub grdLabour_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdLabour.RowDeleting
        Dim dt As DataTable = Session("Labourdt")
        dt.Rows.RemoveAt(e.RowIndex)
        Session("Labourdt") = dt
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
        'CalculateValues()
    End Sub
    Protected Sub grdLabour_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdLabour.PageIndexChanging
        grdLabour.PageIndex = e.NewPageIndex
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdEquip_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdEquip.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow And grdEquip.EditIndex = e.Row.RowIndex) Then
            Dim ddlEquip As DropDownList = DirectCast(e.Row.FindControl("ddlEquipFE"), DropDownList)
            Dim objSqlConfig As New ConfigClassSqlServer()
            ddlEquip.DataSource = objSqlConfig.GetEquipments("", Session("CompanyID"))
            ddlEquip.DataValueField = "Code"
            If (UICulture = "Arabic") Then
                ddlEquip.DataTextField = "DescA"
            Else
                ddlEquip.DataTextField = "DescE"
            End If
            ddlEquip.DataBind()
            ddlEquip.SelectedValue = DirectCast(e.Row.FindControl("lblEqEdit"), Label).Text
        End If
    End Sub
    Protected Sub grdEquip_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdEquip.RowEditing
        grdEquip.EditIndex = e.NewEditIndex
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdEquip_RowCancelingEdit(sender As Object, e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdEquip.RowCancelingEdit
        e.Cancel = True
        grdEquip.EditIndex = -1
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Protected Sub grdEquip_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdEquip.RowDeleting
        Dim dt As DataTable = Session("Equipdt")
        dt.Rows.RemoveAt(e.RowIndex)
        Session("Equipdt") = dt
        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
        'CalculateValues()
    End Sub
    Protected Sub grdEquip_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdEquip.RowUpdating
        Dim dt As DataTable = Session("Equipdt")
        Dim iCount As Integer = 0

        For i As Integer = 0 To dt.Rows.Count - 1
            If (i <> e.RowIndex) Then
                If (dt.Rows(i).Item(1).ToString() = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedValue) Then
                    iCount = 1
                    Exit For
                End If
            End If
        Next
        If (iCount = 0) Then
            dt.Rows(e.RowIndex).Item(1) = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedValue
            dt.Rows(e.RowIndex).Item(2) = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(3) = CType(grdEquip.Rows(e.RowIndex).FindControl("ddlEquipFE"), DropDownList).SelectedItem.Text
            dt.Rows(e.RowIndex).Item(4) = CType(grdEquip.Rows(e.RowIndex).FindControl("txtMD1"), TextBox).Text.Trim()

            grdEquip.EditIndex = -1
            Session("Equipdt") = dt
            LoadGrids()
            LoadDropDowns()
            SetEnvironmentAccordingtoLang()
            'CalculateValues()
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(2);", True)
        End If
    End Sub
    Protected Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        ddlCountry.SelectedIndex = 0
        Dim strCode As String = ""
        If (Request.QueryString.Get("ID") IsNot Nothing) Then
            strCode = Request.QueryString.Get("ID").ToString()
        Else
            strCode = txtCode.Text.Trim()
        End If

        FillData(strCode)
        Session("Labourdt") = Nothing
        Session("Equipdt") = Nothing

        LoadGrids()
        LoadDropDowns()
        SetEnvironmentAccordingtoLang()
    End Sub
    Private Sub FillData(strCode As String)
        Dim objSqlConfig As New ConfigClassSqlServer()
        Dim dt As New DataTable()
        dt = objSqlConfig.GetCrewStructureByID(strCode)
        If (dt.Rows.Count > 0) Then
            txtCode.Text = dt.Rows(0).Item(1).ToString()
            txtNameE.Text = dt.Rows(0).Item(2).ToString()
            txtNameA.Text = dt.Rows(0).Item(3).ToString()
            txtDescE.Text = dt.Rows(0).Item(4).ToString()
            txtDescA.Text = dt.Rows(0).Item(5).ToString()
            txtUnit.Text = dt.Rows(0).Item(6).ToString()
            If (dt.Rows(0).Item(7) IsNot Nothing And dt.Rows(0).Item(7).ToString().Length > 0) Then
                ddlCountry.SelectedValue = dt.Rows(0).Item(7).ToString()
            End If

            txtDO.Text = dt.Rows(0).Item(8).ToString()
            lblHO.Text = dt.Rows(0).Item(9).ToString()
            lblMO.Text = dt.Rows(0).Item(10).ToString()
            lblEH.Text = dt.Rows(0).Item(11).ToString()

        Else
            txtCode.Text = ""
            txtNameE.Text = ""
            txtNameA.Text = ""
            txtDescE.Text = ""
            txtDescA.Text = ""
            txtUnit.Text = ""
            ddlCountry.SelectedIndex = 0
            txtDO.Text = ""

            lblHO.Text = ""
            lblMO.Text = ""
            lblEH.Text = ""
        End If
    End Sub
    'Private Sub CalculateValues()
    '    Dim dt As DataTable = Session("Labourdt")
    '    Dim dt1 As DataTable = Session("Equipdt")
    '    Dim dSumCSMan As Double = 0
    '    Dim dSumCSEquip As Double = 0

    '    If (IsNumeric(txtDO.Text)) Then
    '        lblHO.Text = Math.Round((Double.Parse(txtDO.Text) / 8), 4)
    '        For i As Integer = 1 To dt.Rows.Count - 1
    '            If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
    '                If (dt.Rows(i).Item(4).ToString().Length > 0 And IsNumeric(dt.Rows(i).Item(4).ToString())) Then
    '                    dSumCSMan = dSumCSMan + Double.Parse(dt.Rows(i).Item(4).ToString())
    '                End If
    '            End If
    '        Next
    '        lblMO.Text = Math.Round((dSumCSMan / Double.Parse(lblHO.Text)), 4)

    '        For i As Integer = 1 To dt1.Rows.Count - 1

    '            If (dt1.Rows(i).RowState <> DataRowState.Deleted) Then
    '                If (dt1.Rows(i).Item(4).ToString().Length > 0 And IsNumeric(dt1.Rows(i).Item(4).ToString())) Then
    '                    dSumCSEquip = dSumCSEquip + Double.Parse(dt1.Rows(i).Item(4).ToString())
    '                End If
    '            End If
    '        Next
    '        lblEH.Text = Math.Round((dSumCSEquip / Double.Parse(lblHO.Text)), 4)
    '    End If
    'End Sub
    Protected Sub txtDO_TextChanged(sender As Object, e As EventArgs) Handles txtDO.TextChanged
        'If (IsNumeric(txtDO.Text)) Then
        '    'CalculateValues()
        'End If
    End Sub
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim dt As DataTable = Session("Labourdt")
        Dim dt1 As DataTable = Session("Equipdt")
        Dim sqlConnection As New SqlConnection()
        Dim sqlCommand As SqlCommand = sqlConnection.CreateCommand()
        sqlConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("CES").ConnectionString
        sqlConnection.Open()

        If (dt.Rows.Count = 1 And dt1.Rows.Count = 1) Then
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(9);", True)
            Return
        End If

        Dim iCount As Integer = 0
        Dim bDontInsertMasterRecord As Boolean = False

        If (Request.QueryString.Get("ID") Is Nothing And ViewState("IsUpdate") Is Nothing) Then
            sqlCommand.CommandText = "SELECT COUNT(*) FROM CrewStructure WHERE Code = '" + txtCode.Text + "' "
            iCount = sqlCommand.ExecuteScalar().ToString()
        ElseIf (ViewState("IsUpdate") IsNot Nothing) Then
            iCount = 0
            bDontInsertMasterRecord = True
        End If

        If (iCount = 0) Then
            sqlCommand.CommandText = "DELETE FROM CrewStructure WHERE Code = '" + txtCode.Text.Trim() + "' "
            sqlCommand.ExecuteNonQuery()

            If (bDontInsertMasterRecord = False) Then
                sqlCommand.CommandText = "INSERT INTO CrewStructure(Code, NameE, NameA, DescE, DescA, Unit, DailyOutput, HourlyOutput, ManHour, EquipHour) VALUES('" + txtCode.Text.Trim() + "', '" + txtNameE.Text.Trim() + "', '" + txtNameA.Text.Trim() + "', '" + txtDescE.Text.Trim() + "', '" + txtDescA.Text.Trim() + "', '" + txtUnit.Text.Trim() + "', '" + txtDO.Text.Trim() + "', '" + lblHO.Text.Trim() + "', '" + lblMO.Text.Trim() + "', '" + lblEH.Text.Trim() + "')"
                sqlCommand.ExecuteNonQuery()
            End If

            For i As Integer = 1 To dt.Rows.Count - 1
                If (dt.Rows(i).RowState <> DataRowState.Deleted) Then
                    sqlCommand.CommandText = "SELECT COUNT(*) FROM CrewStructure WHERE Code = '" + txtCode.Text + "' AND CrewCode = '" + dt.Rows(i).Item(1).ToString() + "' "
                    If (sqlCommand.ExecuteScalar().ToString() = "0") Then
                        sqlCommand.CommandText = "INSERT INTO CrewStructure(Code, NameE, NameA, DescE, DescA, Unit, Man_Day, DailyOutput, HourlyOutput, ManHour, EquipHour, Lab_Equip, CrewCode) VALUES('" + txtCode.Text.Trim() + "', '" + txtNameE.Text.Trim() + "', '" + txtNameA.Text.Trim() + "', '" + txtDescE.Text.Trim() + "', '" + txtDescA.Text.Trim() + "', '" + txtUnit.Text.Trim() + "', '" + dt.Rows(i).Item(4).ToString() + "', '" + txtDO.Text.Trim() + "', '" + lblHO.Text.Trim() + "', '" + lblMO.Text.Trim() + "', '" + lblEH.Text.Trim() + "', 1, '" + dt.Rows(i).Item(1).ToString() + "' )"
                        sqlCommand.ExecuteNonQuery()
                    End If
                End If
            Next

            For i As Integer = 1 To dt1.Rows.Count - 1
                If (dt1.Rows(i).RowState <> DataRowState.Deleted) Then
                    sqlCommand.CommandText = "SELECT COUNT(*) FROM CrewStructure WHERE Code = '" + txtCode.Text + "' AND CrewCode = '" + dt1.Rows(i).Item(1).ToString() + "' "
                    If (sqlCommand.ExecuteScalar().ToString() = "0") Then
                        sqlCommand.CommandText = "INSERT INTO CrewStructure(Code, NameE, NameA, DescE, DescA, Unit, Man_Day, DailyOutput, HourlyOutput, ManHour, EquipHour, Lab_Equip, CrewCode) VALUES('" + txtCode.Text.Trim() + "', '" + txtNameE.Text.Trim() + "', '" + txtNameA.Text.Trim() + "', '" + txtDescE.Text.Trim() + "', '" + txtDescA.Text.Trim() + "', '" + txtUnit.Text.Trim() + "', '" + dt1.Rows(i).Item(4).ToString() + "', '" + txtDO.Text.Trim() + "', '" + lblHO.Text.Trim() + "', '" + lblMO.Text.Trim() + "', '" + lblEH.Text.Trim() + "', 2, '" + dt1.Rows(i).Item(1).ToString() + "' )"
                        sqlCommand.ExecuteNonQuery()
                    End If
                End If

            Next

            ViewState("IsUpdate") = "True"
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(1);", True)
        Else
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.[GetType](), "showAlert", "ShowMessage(4);", True)
        End If
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Response.Redirect("Productivity_List.aspx")
    End Sub
End Class