﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Site.Master"
    CodeBehind="Report_Assignment.aspx.vb" Inherits="CEA.Report_Assignment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .update {
            position: fixed;
            top: 0px;
            left: 0px;
            min-height: 100%;
            min-width: 100%;
            background-image: url("../Images/Loading1.gif");
            background-position: center center;
            background-repeat: no-repeat;
            background-color: #e4e4e6;
            z-index: 500 !important;
            opacity: 0.8;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        function ShowMessage(msgno) {
            if (msgno == -1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Failure") %>';
            }
            else if (msgno == 1) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "Success") %>';
            }
            else if (msgno == 2) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "UpdateSuccess") %>';
            }
            else if (msgno == 3) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "DeleteSuccess") %>';
            }
            else if (msgno == 4) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorAlreadyExist") %>';
            }
            else if (msgno == 5) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "ErrorCategorySelection") %>';
            }
            else if (msgno == 6) {
                var msgstring = '<%=GetGlobalResourceObject("Resource", "SelectRole") %>';
            }
            alert(msgstring);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="Label2" runat="server" Text='<%$ Resources:Resource, ReportAssignment %>'></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0"
        runat="server">
        <ProgressTemplate>
            <div class="update">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="">
                <div class="row justify-content-between">
                    <div class="col-12 col-sm-6 col-md-5">
                        <div class="form-group">
                            <%--<asp:Label ID="Label1" runat="server" Text='<%$ Resources:Resource, SelectUser %>'></asp:Label>--%>
                            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-12 col-sm-auto">
                        <div class="btn-group commanButtonGroup mb-4">
                            <asp:Button ID="btnAdd" runat="server" Text="Save" CssClass="btn btn-primary btn-min-120" ValidationGroup="Add" />
                            <asp:Button ID="Button4" runat="server" Text="Cancel" CssClass="btn btn-primary btn-min-120" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive tableLayoutWrap altboarder">
                            <asp:GridView ID="grdCategories" runat="server" AutoGenerateColumns="false" EmptyDataText="No Data"
                                Width="100%" PageSize="20" HeaderStyle-HorizontalAlign="Center">
                                <Columns>
                                    <asp:BoundField DataField="NameE" HeaderText='<%$ Resources:Resource, ReportNameE %>'
                                        ItemStyle-Width="45%" />
                                    <asp:BoundField DataField="NameA" HeaderText='<%$ Resources:Resource, ReportNameA %>' ItemStyle-Width="45%" />
                                    <asp:TemplateField HeaderText='<%$ Resources:Resource, Select %>'>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>

            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>$(document).ready(function () { $("#sidr").show(); });</script>
</asp:Content>


<%--<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MiddlePart">
    <div class="row">
        <div class="large-12 medium-12 column">
            <div class="outerDivBtn">
                <div class="BTNdiv">
                    <asp:Button ID="btnAdd" runat="server" Text="Save" CssClass="BTNAdd" ValidationGroup="Add" />
                    <asp:Button ID="Button4" runat="server" Text="Cancel" CssClass="BTNCancel" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>--%>

