﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Error.aspx.vb" Inherits="CEA._Error" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="lblErrorMessage" runat="server" 
            Text="SOME ERROR OCCURED OR SESSION IS EXPIRED. PLEASE LOGIN AGAIN...                 " 
            Font-Bold="True" Font-Size="Large" ForeColor="Red"></asp:Label>

        <asp:Button ID="Button1" runat="server" Text="LOGIN" Font-Bold="True" 
            Font-Size="Large" ForeColor="#669900" />
    </div>
    </form>
</body>
</html>
