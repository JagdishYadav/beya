﻿Public Class Login1
    Inherits System.Web.UI.Page
    Dim strConnectionString As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            Session("CameFromCES") = Nothing
            'Session("LoginCompanyID") = Nothing

            Dim configClass As New ConfigClassSqlServer()
            LinkButton1.Visible = False

            Dim url = Request.Path.ToLower
            url = url.Trim("/")
            Dim Vals = url.Split("/")

            If Vals.Length > 0 And Vals(Vals.Count - 1).Contains(".aspx") = True Then
                Session("IS_SUPPLIER") = Nothing
            End If

            If Vals.Length > 0 And Vals(Vals.Count - 1).Contains(".aspx") = False And Vals(Vals.Count - 1).Contains(".axd") = False And Vals(Vals.Count - 1).Contains(".ico") = False Then
                Dim strLastString As String = Vals(Vals.Count - 1)
                If (strLastString.ToLower() = "supplier") Then
                    Session("IS_SUPPLIER") = "YES"
                    LinkButton1.Visible = True
                Else
                    Session("IS_SUPPLIER") = Nothing
                    Session("LoginCompanyID") = configClass.GetCompanyByName(strLastString)
                End If
            End If

            If (Request.QueryString.Get("CompID") IsNot Nothing) Then
                Session("LoginCompanyID") = configClass.GetCompanyByID(Request.QueryString.Get("CompID").ToString())
                Session("IS_SUPPLIER") = Nothing
            Else
                'Session("LoginCompanyID") = "0"
                url = Request.Path.ToLower
                url = url.Trim("/")
                Vals = url.Split("/")

                If Vals.Length > 0 And Vals(Vals.Count - 1).Contains(".aspx") = False And Vals(Vals.Count - 1).Contains(".axd") = False And Vals(Vals.Count - 1).Contains(".ico") = False Then
                    Dim strCompanyName As String = Vals(Vals.Count - 1)
                    If (strCompanyName.ToLower() = "supplier") Then
                        Session("IS_SUPPLIER") = "YES"
                        LinkButton1.Visible = True
                    Else
                        Session("IS_SUPPLIER") = Nothing
                        Session("LoginCompanyID") = configClass.GetCompanyByName(strCompanyName)
                    End If
                End If
            End If

            If (Session("LoginCompanyID") Is Nothing) Then
                Session("LoginCompanyID") = "0"
            End If
            Username.Focus()
        End If
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        lblErrorMessage.Text = String.Empty
        strConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionString").ToString()
        If (CommonFunctions.ValidateInput(Username.Text.Trim()) = False Or Username.Text.Trim().Length = 0) Then
            lblErrorMessage.Text = "Invalid Username.Please check..."
            Username.Text = String.Empty
            Username.Focus()
            Return
        End If
        If (CommonFunctions.ValidateInput(Password.Text.Trim()) = False Or Password.Text.Trim().Length = 0) Then
            lblErrorMessage.Text = "Invalid Password.Please check..."
            Password.Text = String.Empty
            Password.Focus()
            Return
        End If

        Dim sTicketData As String = String.Empty
        Dim sIP As String = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        If (sIP = Nothing) Then
            sIP = Request.ServerVariables("REMOTE_ADDR")
        End If
        Dim sArr As String() = Nothing
        Dim sUserDisplayName As String = ""
        Dim configClass As New ConfigClassSqlServer()

        If (Session("IS_SUPPLIER") IsNot Nothing And Session("IS_SUPPLIER") = "YES") Then
            sTicketData = configClass.Login(Username.Text.Trim(), Password.Text.Trim(), strConnectionString, lblErrorMessage, Session("LoginCompanyID"), "YES")
        Else
            sTicketData = configClass.Login(Username.Text.Trim(), Password.Text.Trim(), strConnectionString, lblErrorMessage, Session("LoginCompanyID"), "NO")
        End If

        If sTicketData = "NotFound" Then
            lblErrorMessage.Text = " Invalid Username or password, please try again or contact your administrator"
            Password.Focus()
        ElseIf (sTicketData <> "" And sTicketData <> "NotFound") Then
            sArr = sTicketData.Split("|")
            sUserDisplayName = sArr(1).ToString()
            Dim authCookie As HttpCookie = FormsAuthentication.GetAuthCookie(sUserDisplayName, False)
            Dim ticket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)
            Dim newTicket As FormsAuthenticationTicket = New FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate, ticket.Expiration, ticket.IsPersistent, sTicketData)
            authCookie.Value = FormsAuthentication.Encrypt(newTicket)
            Response.Cookies.Add(authCookie)
            Dim redirUrl As String = FormsAuthentication.GetRedirectUrl(sUserDisplayName, False)
            Session("UserName") = sArr(1).ToString()
            Session("User_Role_ID") = sArr(2).ToString()
            Session("UserID") = sArr(3).ToString()
            Session("CompanyID") = sArr(4).ToString()
            Session("ClientTypeID") = sArr(5).ToString()
            Session("IsTemp") = sArr(6).ToString()
            If (sArr(2).ToString().Equals("99")) Then
                Response.Redirect("~/Admin/Supplier_Products_View.aspx")
            ElseIf (sArr(2).ToString().Equals("555")) Then
                Response.Redirect("~/Admin/CategoryEntry.aspx")
            ElseIf (sArr(2).ToString().Equals("777")) Then
                Response.Redirect("~/Admin/CategoryApprove.aspx")
            ElseIf (sArr(2).ToString().Equals("666")) Then
                Response.Redirect("~/Admin/Productivity_List.aspx")
            Else
                Response.Redirect("~/Admin/Module.aspx")
            End If
        End If
    End Sub
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        Session("UserID") = "-99"
        Response.Redirect("~/Admin/Supplier_New.aspx")
    End Sub
End Class